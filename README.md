Before your development, please check wiki and 'WARNING' label issue to get more information.

It must be pointed out that all functions or changes must be developed together with the test. You need to make sure that all the logical branches are covered by the test.

* [Install to front-end project](#install-to-front-end-project)
* [Available Scripts](#available-scripts)
  * [Build](#build)
  * [test](#test)
* [Develop Core-API](#develop-core-api)
* [Update front end project Core-API module](#update-front-end-project-core-api-module)
* [Maintain & update bz files](#maintain-update-bz-files)

## Install to front-end project
1. Before installing, you may need to confirm you have uploaded the ssh key to your gitlab account settings.  
link: http://192.168.222.60:8001/profile/keys

2. Add record to package.json dependencies field  
```
# Latest master
git+ssh://git@192.168.222.60:eab-shared-module/eab-core-api.git
# By branch
git+ssh://git@192.168.222.60:eab-shared-module/eab-core-api.git#<branch name>
# By tag
git+ssh://git@192.168.222.60:eab-shared-module/eab-core-api.git@<tag name>
```

## Available Scripts
### Build
`npm run build`

Build Web-API module to 'lib' folder.

#### Test
`npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### Smart Build
`npm run smartBuild`

Build Web-API module to 'lib' folder and auto deploy to your front-end project. It need you to place the front-end project into the same folder level of your web-api.

## Develop Core-API
After the development work is completed, you must build the project before the push to incorporate the changes into the module. At the same time, you need to update the Core-API module of the front-end project.

## Update front end project Core-API module
If you are try to test the front end project with your Core-API development branch, please edit your package.json `eab-core-api` record to your branch.
1. delete package lock file(package-lock.json etc.)
2. re-install module  
`npm install eab-core-api`
3. re bundle  
`npm run reset-start`

## Maintain & update bz files
When you have latest bz files to be replaced into the existing files in eab-core-api, 
the following files need to be handled specially.

the following files must not be replaced:
* `package.json`
* `src/index.js`
* `src/bz/cbDaoFactory/index.js`
* `src/bz/cbDaoFactory/localCB.js`
* `src/bz/cbDaoFactory/remoteCB.js`
* `src/bz/index.js`
* `src/bz/nativeRunner.js`
* `src/bz/InvalidationHandler.js`

the following files in which some method(s) & line(s) must not be replaced:
* `src/bz/cbDao/couchbaseMgr.js`

keep codes
```
const viewVersionNum = 1;
const init = function (callback) {
  if (!cbDao) {
    cbDao = require('../cbDaoFactory').create();
  }
  cbDao.init((resp) => {
    updateViews(callback);
  });
};
```

keep codes
```
const updateViews = function (callback) {
  cbDao.getDoc('_design/main', (doc) => {
    if (!doc || !doc.views || hasDiffView(doc.views)) {
      const views = generateViewJson();
      logger.log('CouchbaseMgr :: updateViews :: updating view');
      cbDao.createView('main', views, (res) => {
        logger.log('CouchbaseMgr :: updateViews :: complete:', res);
        if (typeof callback === "function") {
          callback(res);
        }
      });
    } else {
      if (typeof callback === "function") {
        callback(null);
      }
    }
  });
};
```

keep code
```
module.exports = {
  viewVersionNum,
```

* `src/bz/cbDao/application.js`

keep code
```
module.exports.getApplicationPromise = function(appId) {...}
module.exports.upsertApplicationPromise = (appId, newApp, skipUpdateView) => {...}
```

* `src/bz/cbDao/bundle/common.js`

keep code
```
module.exports.updateBundlePromise = bundle => {...}
```

* `src/bz/cbDao/bundle/index.js`

keep code
```
module.exports.updateBundlePromise = _c.updateBundlePromise;
```


* `src/bz/utils/RemoteUtils.js`

remove code
```
var request = require('request');
```

keep codes
```
var requestWithInternetProxy = function(uri, method, headers, data, callback, skipProxy) {
  callback({});
}
```

* `src/bz/handler/QuotationHandler.js`

keep code
```
// in function saveQuotation
if (session.platform && isNewQuotation) {
    if (quotation._rev) {
      delete quotation._rev;
    }
    if (quotation._id) {
      delete quotation._id;
    }
    if (quotation._attachments) {
      delete quotation._attachments;
    }
}
```

Add 3 fields when send email with emailUtils in order to set password  for pdf
* userType
* agentCode
* dob

```
EmailUtils.sendEmail({
                from: companyInfo.fromEmail, // TODO
                userType: email.id,
                agentCode: email.agentCode,
                dob: email.dob,
                to: _.join(email.to, ','),
                title: email.title,
                content: email.content,
                attachments: attArr
                }, resolve);

```

Added adjustedModel handle customization of fund allocation

At the function:
```
module.exports.allocFunds = function(data, session, callback) {
```
from
```
  let {invOpt, portfolio, fundAllocs, hasTopUpAlloc, topUpAllocs} = data;
```
...
```
        quotation.fund = {
          invOpt: invOpt,
          portfolio: invOpt === 'buildPortfolio' ? portfolio : null,
          funds: selectedFunds
        };
```
to
```
  let {invOpt, portfolio, fundAllocs, hasTopUpAlloc, topUpAllocs, adjustedModel} = data;
```
...
```
        quotation.fund = {
          invOpt: invOpt,
          portfolio: invOpt === 'buildPortfolio' ? portfolio : null,
          funds: selectedFunds,
          adjustedModel
        };
```


* `src/bz/handler/ClientChoiceHandler.js`

added:
```
const rspSelect = _.get(quotation, 'policyOptions.rspSelect', false);
const multiFactor = _.get(quotation, 'policyOptions.multiFactor', '1');
const hasMultiplier = multiFactor !== '1' ? true : false;
const planCovCodes = _.map(_.get(quotation, 'plans'), plan => plan.covCode);
const withCiCip = (planCovCodes.indexOf('CIBX_LMP') > -1) || (planCovCodes.indexOf('CIP_LMP') > -1);
const withCiCipEci = ((planCovCodes.indexOf('CIBX_LMP') > -1) || (planCovCodes.indexOf('CIP_LMP') > -1)) && (planCovCodes.indexOf('ECIX_LMP') > -1);
const withEci = (planCovCodes.indexOf('ECIX_LMP') > -1);
let CIcondition = '';
if (withCiCipEci) {
	CIcondition = "CiCipEci";
} else if (withCiCip) {
	CIcondition = "CiCip";
} else if (withEci) {
	CIcondition = "Eci";
} else {
	CIcondition = "default";
}

// Check multiple conditions same time
if (_.get(subTextBlock, 'multipleConditionsCheck.isNeedCheck')) {

	let multipleConditions = _.get(subTextBlock, 'multipleConditionsCheck.conditions', []);
	if (multipleConditions.length > 0) {
		let isAllTrue = true;
		_.each(multipleConditions, (condition, index) => {
			if (condition === 'insuredAgeCheck') {
				if (_.get(subTextBlock, 'multipleConditionsCheck.insuredAgeCheck.ageMethod') === 'attainedAge') {
					if (_.get(subTextBlock, 'multipleConditionsCheck.insuredAgeCheck.condition') === 'showIfSmaller') {
						if (DateUtils.getAttainedAge(new Date(), new Date(_.get(quotation, 'iDob'))).year < _.get(subTextBlock, 'multipleConditionsCheck.insuredAgeCheck.age')) {
							isAllTrue = true;
						} else {
							isAllTrue = false;
						}
					} else if (_.get(subTextBlock, 'multipleConditionsCheck.insuredAgeCheck.condition') === 'showIfNotSmaller') {
						if (DateUtils.getAttainedAge(new Date(), new Date(_.get(quotation, 'iDob'))).year >= _.get(subTextBlock, 'multipleConditionsCheck.insuredAgeCheck.age')) {
							isAllTrue = true;
						} else {
							isAllTrue = false;
						}
					}
				}
			} else if (condition === 'hasMultiplier') {
				if (hasMultiplier !== _.get(subTextBlock, 'multipleConditionsCheck.hasMultiplier')) {
					isAllTrue = false;
				}
			} else if (condition === 'isPhSameAsLa') {
				if (isPhSameAsLa !== _.get(subTextBlock, 'multipleConditionsCheck.isPhSameAsLa')) {
					isAllTrue = false;
				}
			}
		});
		if (isAllTrue) {
			defaultRecommendBenefit += _.get(subTextBlock, 'benefits.' + lang);
			defaultRecommendLimit += _.get(subTextBlock, 'limitations.' + lang);

			// Check additional Limitations same time
			if (_.get(subTextBlock, 'additionalLimitationsCheck.isNeedCheck')) {
				let additionalLimitationsConditions = _.get(subTextBlock, 'additionalLimitationsCheck.conditions', []);
				if (additionalLimitationsConditions.length > 0) {
					let isAllTrue = true;
					_.each(additionalLimitationsConditions, (condition, index) => {
						if (condition === 'anyRiderCodes') {
							let appearOnce = _.get(subTextBlock, 'additionalLimitationsCheck.appearOnce', false);
							let anyRiderCodes = _.get(subTextBlock, 'additionalLimitationsCheck.anyRiderCodes', []);
							if (anyRiderCodes.length > 0) {
								if (appearOnce) {
									let firstRiderCodeMatched = '';
									_.each(anyRiderCodes, (riderCode, index) => {
										if (planCovCodes.indexOf(riderCode) > -1) {
											firstRiderCodeMatched = riderCode;
											return false;
										}
									});
									if (!firstRiderCodeMatched || plan.covCode !== firstRiderCodeMatched) {
										isAllTrue = false;
									}
								} else {
									let isRiderCodeMatched = false;
									_.each(anyRiderCodes, (riderCode, index) => {
										if (planCovCodes.indexOf(riderCode) > -1) {
											isRiderCodeMatched = true;
											return false;
										}
									});
									if (!isRiderCodeMatched) {
										isAllTrue = false;
									}
								}
							} else {
								isAllTrue = false;
							}
						} else {
							isAllTrue = false;
						}
					});
					if (isAllTrue) {
						defaultRecommendLimit += _.get(subTextBlock, 'additionalLimitations.' + lang);
					}
				}
			}
		}
	}
}
```

0036400: Budget used for non selected plans should not show in budget section
```
if (quotSelectedMap[quot.id] && _.get(quot, 'baseProductCode') === 'TPX' || _.get(quot, 'baseProductCode') === 'TPPX')
```

Modify shield ClientChoice ROP into correct path
```
1.
Change
insuredRopValue = _.get(values, `recommendation[${quotId}].rop_shield.ropBlock.shieldRopAnswer_${i}`, '');

to
insuredRopValue = _.get(values, `recommendation.${quotId}.rop_shield.ropBlock.shieldRopAnswer_${i}`, '');

2.
Change
_.get(values, `recommendation[${quotId}].rop_shield.ropBlock.iCidRopAnswerMap`, {});

to
_.get(values, `recommendation.${quotId}.rop_shield.ropBlock.iCidRopAnswerMap`, '');

3.
Change
newCcData.recommendation.rop_shield.ropBlock.ropQ2 = _.get(values, `recommendation[${quotId}].rop_shield.ropBlock.ropQ2`);
newCcData.recommendation.rop_shield.ropBlock.ropQ3 = _.get(values, `recommendation[${quotId}].rop_shield.ropBlock.ropQ3`);
newCcData.recommendation.rop_shield.ropBlock.ropQ1sub3 = _.get(values, `recommendation[${quotId}].rop_shield.ropBlock.ropQ1sub3`);


to
newCcData.recommendation.rop_shield.ropBlock.ropQ2 = _.get(values, `recommendation.${quotId}.rop_shield.ropBlock.ropQ2`);
newCcData.recommendation.rop_shield.ropBlock.ropQ3 = _.get(values, `recommendation.${quotId}.rop_shield.ropBlock.ropQ3`);
newCcData.recommendation.rop_shield.ropBlock.ropQ1sub3 = _.get(values, `recommendation.${quotId}.rop_shield.ropBlock.ropQ1sub3`);

```

keep code
```
// for all code that has following comment
/** DO NOT REMOVE ... */

// for function genClientChoiceAcceptanceTemplateValues, keep param: chosenList
var genClientChoiceAcceptanceTemplateValues = function(bundle, quotList, template, values, chosenList, cb) {
...
    /** DO NOT REMOVE chosenList.forEach */
}

// all code related to isDataRecommendationCompleted
isDataRecommendationCompleted
```


* `src/bz/handler/application/shield/clientChoice.js`

keep code with following comment pattern
```
/** DO NOT REMOVE ... */
```


* `src/bz/handler/ApplicationHandler.js`

keep code in getPaymentUrl
```
axiosWrapper({
  ...
.catch(e=>console.log(e));
```

keep code
```
var getSignatureStatusFromCb = function getSignatureStatusFromCb(data, session, cb) {
  if (session.platform) {
    defaultSignature.getSignatureInitMobile(data, session, cb);
  } else {
    defaultSignature.getSignatureInitUrl(data, session, cb);
  }
};

// Change it to get data for rlsStatus. Sheild and non-shield will update approval if anything is sync. 
ApprovalHandler.searchApprovalCaseById({id: app.policyNumber}, session, (approvalCase) => {
  ...
});

//Deal with spelling mistake between teleTransfter and teleTransfer
else if(paymentMethod === "teleTransfter" && !(_.isEmpty(mandDocs.teleTransfter) || _.isEmpty(mandDocs.teleTransfer))) {
  ...
}

module.exports.getUpdatedAttachmentUrl = function(data, session, cb) {
...
}

module.exports.saveSignedPdf = function(data, session, cb) {
...
}

module.exports.editSupportDocument = function(data, session, cb){
...
}

// keep following in goApplication
if (crossAgeResp.success) {
    result.crossAge = crossAgeResp;
}

// keep following in goApplication, _getPaymentTemplateValues, appFormSubmission, getSubmissionTemplateValues
if (session.platform) {
    commonApp.frozenTemplateMobile(template);
} else {
    commonApp.frozenTemplate(template);
}

// keep following return param in callback function in getSupportDocuments
application: app


// keep following return param in callback function in updateBIBackDateTrue
application: signCb.application.application,
```

keep code in confirmAppPaid
```
// all related to updateApplication(application);
const updateApplication = application => {
    applicationDao.updApplication(application.id, application, (result) => {
        if (result && !result.error) {
            dao.updateViewIndex("main", "summaryApps");
            cb({success: true})
        } else {
            cb({success: false, error: 'Update Application failure:' +application.id})
        }
    })
};

// add flag
application.isInvalidatedPaidToRLS = false;

if (session.platform) {
    updateApplication(application);
} else {
    callApiComplete("/submitInvalidApp/"+application.id, "GET", {}, null, true, (resp) => {
        logger.log('INFO: Submit invalidated paid application (paid after invalidated):', application.id, resp);
        if (resp && resp.success) {
            updateApplication(application);
        }
    });
}

updateApplication(application);
```

* `src/bz/handler/application/default/application.js`

keep code
```
// in function _checkCrossAge
callback({
    success:true,
    insuredStatus : insuredResult.status || 0,
    proposerStatus : proposerResult.status || 0,
    allowBackdate,
    crossedAge
})


  let needCheckProposerCrossAge = function(){
    return new Promise((resolve) => {
      // It is a third-party application where Proposer is not Life Assured AND
      if (_.get(app, 'quotation.iCid') !== _.get(app, 'quotation.pCid')) {
        // Proposer has selected a rider product AND
        // Rider’s age cross has an impact on premium (all Riders which are for Proposer (titled PH in medical questions product matrix)
        let riders = _.drop(_.get(app, 'quotation.plans', []));
        dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function(appFormMapping) {
          let proposerRiders = _.keys(
            _.get(
              _.find(
                _.get(appFormMapping, app.quotation.baseProductCode + '.eFormSections[0].items'),
                obj => obj.id === 'insurability',
                {}
              ),
              'form.ph',
              {}
            )
          );
          let checkProposer = false;
          for (let i in riders) {
            if (_.includes(proposerRiders, riders[i].covCode)) {
              checkProposer = true;
            }
          }
          resolve(checkProposer);
        });
      } else {
        resolve(false);
      }
    });
  };
```
added codes @ begining
```
const agentDao      = require('../../../cbDao/agent');

const _getNameOfOrganization = function(upline2Code, callback) {
  logger.log('INFO: _getNameOfOrganization->searchAgents upline2Code: ', upline2Code)
  agentDao.searchAgents('01',upline2Code,upline2Code,(res)=>{
    if (res.success){
      if (res.result.length > 0){
        callback(res.result[0].agentName);
      }
      else {
        logger.error('Error in _getNameOfOrganization->searchAgents upline2Code: ',upline2Code);
        callback(null);
      }
    }
    else {
      logger.error('Error in _getNameOfOrganization->searchAgents upline2Code: ',upline2Code);
      callback(null);
    }
  });
};
```
added codes @ _genAppFormPdfData
```
if (app.agentCodeDisp){
        appNewValues.agent.agentCodeDisp =   app.agentCodeDisp;
      }
      if (app.nameOfOrganization){
        appNewValues.agent.company = app.nameOfOrganization;
      } else {
        appNewValues.agent.company = '';
      }

```
...
```
  let agentCodeDisp = session.agent.agentCodeDisp;
  if (agentCodeDisp) {
    application.agentCodeDisp = agentCodeDisp;
  }
  let upline2Code = session.agent.rawData.upline2Code;
```
changed codes @ _genApplicationPDF

from
```
      _genAppFormPdfData(application, lang, function(data) {
        dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], (pda)=>{
          if (pda && !pda.error) {
            logger.log('INFO: _genApplicationPDF - (2) generateAppFormPdf - populate PDA data', appId);
            //For not FA channel
            if (pda.ownerConsentMethod) {
              let ownerConsentMethodList = pda.ownerConsentMethod.split(',');

              for (let i = 0; i < ownerConsentMethodList.length; i ++) {
                let method = ownerConsentMethodList[i];
                if (method === 'phone') {
                  data.proposer.declaration.PDPA01a = 'Yes';
                } else if (method === 'text') {
                  data.proposer.declaration.PDPA01b = 'Yes';
                } else if (method === 'fax') {
                  data.proposer.declaration.PDPA01c = 'Yes';

```
to 
```
      _getNameOfOrganization(upline2Code, function(nameOfOrganization) {
        if (nameOfOrganization) {
          application.nameOfOrganization = nameOfOrganization;
        }

        _genAppFormPdfData(application, lang, function(data) {
          dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], (pda)=>{
            if (pda && !pda.error) {
              logger.log('INFO: _genApplicationPDF - (2) generateAppFormPdf - populate PDA data', appId);
              //For not FA channel
              if (pda.ownerConsentMethod) {
                let ownerConsentMethodList = pda.ownerConsentMethod.split(',');

                for (let i = 0; i < ownerConsentMethodList.length; i ++) {
                  let method = ownerConsentMethodList[i];
                  if (method === 'phone') {
                    data.proposer.declaration.PDPA01a = 'Yes';
                  } else if (method === 'text') {
                    data.proposer.declaration.PDPA01b = 'Yes';
                  } else if (method === 'fax') {
                    data.proposer.declaration.PDPA01c = 'Yes';
                  }
```

* `src/bz/handler/application/default/signature.js`

keep code
```
const _frozenTemplateMobile = function (template) {
...
}

const _frozenTemplateMobileItem = template => {
...
}


module.exports.frozenTemplateMobile = _frozenTemplateMobile;

var _getSignatureInitMobile = function _getSignatureInitMobile(data, session, callback) {
...
}

const _getSignatureUpdatedPdfString = function(data, session, callback) {
...
}

const _saveSignedPdf = function(appId, attId, pdfData, callback) {
...
}

module.exports.saveSignedPdf = _saveSignedPdf;
module.exports.getSignatureUpdatedPdfString = _getSignatureUpdatedPdfString;
module.exports.getSignatureInitMobile = _getSignatureInitMobile;


// keep following in _getSignatureUpdatedUrl
if (session.platform) {
    commonApp.frozenTemplateMobile(template);
} else {
    commonApp.frozenTemplate(template);
}
```


* `src/bz/handler/application/shield/application.js`

keep code
```
// keep following in _forzenAppFormTemplateByTabIndex, _forzenAppFormTemplateByMenuItemKey
if (session.platform) {
    commonApp.frozenTemplateMobile(template);
} else {
    commonApp.frozenTemplate(template);
}

// keep callback in goNextStep, case (nextIndex === commonApp.EAPP_STEP.SIGNATURE)
callback({success: true, application: cache.application, signature: result.signature, updIds: cache.saveResult.updIds, warningMsg: result.warningMsg, crossAge: result.crossAge});
```

keep session param in function
```
_forzenAppFormTemplateByTabIndex
_forzenAppFormTemplateByMenuItemKey
_forzenAppFormTemplateByCid
_getAppFormTemplateWithOptionList
goNextStep
```

added codes @ begining
```
const agentDao        = require('../../../cbDao/agent');

const _getNameOfOrganization = function(upline2Code, callback) {
  logger.log('INFO: _getNameOfOrganization shield->searchAgents upline2Code: ', upline2Code);
  return new Promise((resolve, reject) => {
    agentDao.searchAgents('01',upline2Code,upline2Code,(res)=>{
      if (res.success){
        if (res.result.length > 0){
          resolve(res.result[0].agentName);
        }
        else {
          logger.error('Error in _getNameOfOrganization shield->searchAgents upline2Code: ',upline2Code);
          resolve(null);
        }
      }
      else {
        logger.error('Error in _getNameOfOrganization shield->searchAgents upline2Code: ',upline2Code);
        resolve(null);
      }
    });
  });
};
```
added codes @_genAppFormPdfData
```
  if (application.agentCodeDisp) {
    appNewValues.agent.agentCodeDisp = application.agentCodeDisp;
  }
  if (application.nameOfOrganization){
    appNewValues.agent.company = application.nameOfOrganization;
  } else {
    appNewValues.agent.company = '';
  }

```
...
```
  let quotationProposerAge = _.get(application, 'quotation.pAge', 0);
  appNewValues.proposer.personalInfo.quotationAge = quotationProposerAge;

  let quotationInsured = _.get(application, 'quotation.insureds', []);
  for (let i = 0; i < appNewValues.insured.length; i++) {
    let personalInfo = appNewValues.insured[i].personalInfo;
    if (personalInfo) {
      let cid = appNewValues.insured[i].personalInfo.cid;
      if (quotationInsured[cid] && quotationInsured[cid].iAge) {
        let quotationAge = quotationInsured[cid].iAge;
        appNewValues.insured[i].personalInfo.quotationAge = quotationAge;
      }
    }
  }
```
...@_generatePdf
```
  let agentCodeDisp = session.agent.agentCodeDisp;
  let upline2Code = session.agent.rawData.upline2Code;
```
...from
```
    return _getAppFormTemplate(application.quotation).then((templateRes) => {
      return _genAppFormProposerPdf(lang, application, bundle, templateRes.template).then((isPhGenPdf) => {
        return _genAppFormInsuredPdf(session, lang, application, bundle, templateRes.template).then((isLaGenPdfs) => {
          return {
            proposer: isPhGenPdf,
            insured: isLaGenPdfs
          };
```
...to
```
if (agentCodeDisp) {
      application.agentCodeDisp = agentCodeDisp;
    }
    return _getNameOfOrganization(upline2Code).then((nameOfOrganization) => {
      if (nameOfOrganization) {
        application.nameOfOrganization = nameOfOrganization;
      }
      return _getAppFormTemplate(application.quotation).then((templateRes) => {
        return _genAppFormProposerPdf(lang, application, bundle, templateRes.template).then((isPhGenPdf) => {
          return _genAppFormInsuredPdf(session, lang, application, bundle, templateRes.template).then((isLaGenPdfs) => {
            return {
              proposer: isPhGenPdf,
              insured: isLaGenPdfs
            };
          });

```
* `src/bz/handler/application/shield/payment.js`

keep code
```
// keep following in _getPaymentTemplate
if (session.platform) {
    commonApp.frozenTemplateMobile(template);
} else {
    commonApp.frozenTemplate(template);
}
```

keep session param in function
```
_getPaymentTemplate
```

keep code in confirmAppPaid
```
// add flag
masterApplication.isInvalidatedPaidToRLS = false;

// change code for callApiComplete with checking session.platform
if (session.platform) {
    resolve({success: true});
} else {
    callApiComplete('/submitInvalidApp/'+ id, 'GET', {}, null, true, (resp) => {
        logger.log('INFO: Submit invalidated paid application (paid after invalidated):', id, resp);
        if (resp && resp.success) {
            resolve({success: true});
        } else {
            reject(`Call submitInvalidApp failure: ${id}`);
        }
    });
}
```


* `src/bz/handler/application/shield/submission.js`

keep code
```
// keep following in _getSubmissionTemplate, submission
if (session.platform) {
    commonApp.frozenTemplateMobile(template);
} else {
    commonApp.frozenTemplate(template);
}
```

keep session param in function
```
_getSubmissionTemplate
```


* `src/bz/handler/application/shield/summary.js`

keep session param in function
```
continueApplication
```

keep code in continueApplication
```
if (session.platform) {
    initProcess.push(_signature.getSignatureInitPdfString(session, appId));
} else {
    initProcess.push(_signature.getSignatureInitUrl(session, appId));
}

// after _application.checkCrossAge(result.application)
result.crossAge = caResp;
```


* `src/bz/handler/application/shield/supportDocument.js`

keep code
```
// keep following return param in callback function in getSupportDocuments showSupportDocments
application: app
```


* `src/bz/utils/PDFHandler.js`

keep code
```
var XSLT = require('../../../../app/utilities/XSLTHandler');

const _getBiPdfMobile = function(pdfCodes, reportData, reportTemplates, basicPlanDetails, callback) {
...
};

// in getReportPdf function
module.exports.getReportPdf = function (pdfCodes, reportData, reportTemplates, isDynamic, basicPlanDetails, callback) {
  // TODO: add checking on mobile / web
  const isMobile = true;
  if (isMobile) {
    _getBiPdfMobile(pdfCodes, reportData, reportTemplates, basicPlanDetails, isDynamic, callback);
  } else {
    if(!isDynamic){
      _getStaticPdf(pdfCodes, reportData, reportTemplates, basicPlanDetails, callback);
    } else { 
      _getDynamicPdf(pdfCodes, reportData, reportTemplates, basicPlanDetails, callback);
    }
  }
};
```

Changed code
Removed

```
const bundledPlanNumOfPage = ["LMP"].indexOf(basicPlanDetails.covCode) >= 0 ? 6 : 0;
```

keep for handle LMP bundle page

```
    let bundledPlanNumOfPage = 0;
```
...

```
else if (["LMP"].indexOf(basicPlanDetails.covCode) >= 0 ) {
      if (_.find(reportTemplates, report => _.isEqual(report.pdfCode, "LMP_BUNDLEDPLAN"))) {
        bundledPlanNumOfPage = 6;
      }
```

```
// in getFnaReportPdf function
let params = {
  type: "FNA",
  header: {
    height: "40px",
    contents: {}
  },
  footer: {
    height: "40px",
    contents: {}
  },
  headerHeight: 30,
  footerHeight: 30,
};
```


* `src/bz/ClientHandler.js`

keep code
```
cDao.saveProfile(profile, photo, tiPhoto, session.agent, function(doc){
```


* `src/bz/cbDao/bundle/application.js`

keep comment out following
```
// if (profile._rev) delete profile._rev;
// if (profile._attachments) delete profile._attachments;
```


* `src/bz/cbDao/application.js`

keep code
```
const Buffer = require('buffer').Buffer;
```


* `src/bz/cbDao/client.js`

remove code
```
profile.relationship = relationship;
profile.relationshipOther = relationshipOther;
callback(exDoc, profile, fid);
callback(exDoc, profile, fid);
```

remove code
```
let {cid: did, relationship} = d;
```

keep code
```
var newProfile = Object.assign({}, profile);
newProfile.relationship = relationship;
newProfile.relationshipOther = relationshipOther;
callback(exDoc, newProfile, fid);
callback(exDoc, newProfile, fid);
```

keep code
```
let {cid: did, relationship, relationshipOther} = d;
```

add code
```
dProfile.relationshipOther = relationshipOther;
```

on the function : handleProfile

keep code
```

    data.age = cFunctions.calcAge(data.dob);
```

* `src/bz/cbDao/product.js`
 
require momont
```
var moment = require('moment');
```

use moment in function getPlanByCovCode by remove code
```
var now = new Date().getTime();

if (!(new Date(p.effDate) <= now && new Date(p.expDate) >= now)) {
```

then keep code
```
var now = moment().valueOf();

if (!(moment(p.effDate).valueOf() <= now && moment(p.expDate).valueOf() >= now)) {
```

Added to handle WLP at _getBiPdfMobile()

from
```
let overlayNumOfPage = hasOverlay ? 3 : 0;
    if (["TPX", "TPPX", "HIM", "HER"].indexOf(basicPlanDetails.covCode) >= 0) {
      overlayNumOfPage = 2;
    }
```
...
```
const pdfOptions = {
      type: "BI",
      headerHeight: 0,
      footerHeight: 96,
      overlayNumOfPage,
      bundledPlanNumOfPage,
      isQuickQuote,
      footer: {
        releaseVersion: global.config.biReleaseVersion,
        date: moment().format(ConfigConstant.DateTimeFormat3)
      }
    };
```
to
```
let overlayNumOfPage = hasOverlay ? 3 : 0;
    let overlayType = 1;
    if (["TPX", "TPPX", "HIM", "HER","ESC","PNP","PNPP","PNP2","PNPP2","DTA", "DTJ"].indexOf(basicPlanDetails.covCode) >= 0) {
      overlayType = 2;
      overlayNumOfPage = 2;
    } else if (["LMP","LITE"].indexOf(basicPlanDetails.covCode) >= 0 ) {
      if (_.find(reportTemplates, report => ["LMP_BUNDLEDPLAN", "LITE_Bundled_Plan"].includes(report.pdfCode))) {
      bundledPlanNumOfPage = 6;
      }
      overlayType = 3;
    }
```
...
```
const pdfOptions = {
      type: "BI",
      headerHeight: 0,
      footerHeight: 96,
      overlayType,
      overlayNumOfPage,
      bundledPlanNumOfPage,
      isQuickQuote,
      footer: {
        releaseVersion: global.config.biReleaseVersion,
        date: moment().format(ConfigConstant.DateTimeFormat3)
      }
    };
```
#### line 16 in queryBasicPlansByClient
change:
```
const now = currentDate.getTime();
```
to:
```
const now = moment();
```
#### line 126 in canViewProduct
change:
```
const now = currentDate.getTime();
```
to:
```
const now = moment();
```
#### line 166 in canViewProduct
change:
```
if (now < new Date(p.effDate) || now > new Date(p.expDate)) {
```
to:
```
if (now < moment(p.effDate) || now > moment(p.expDate)) {
```

* `src/bz/cbDao/quotation.js`

#### line 5

add:
```
const moment = require('moment');
```
#### line 179 in getPdfTemplate
change:
```
var now = new Date();
```
to:
```
var now = moment();
```
#### line 183 in getPdfTemplate
change:
```
if (now < new Date(template.effDate) || now > new Date(template.expDate)) {
```
to:
```
if (now < moment(template.effDate) || now > moment(template.expDate)) {
```

* `src/bz/CommonFunctions.js`
 
keep code
```
module.exports.mergePdfs = function (pdfs, callback) {
  if (!pdfs || pdfs.length <= 1) {
    callback(pdfs && pdfs[0]);
  } else {
    let base64Pdfs = _.filter(pdfs, pdf => pdf);
    global.JavaRunner.mergePdfs(base64Pdfs, callback);
  }
};
```

* `src/bz/handler/application/shield/signature.js`

add code
```
const getAttachmentPromise = function(docId, attachmentId, tabIdx) { ... }
module.exports.getSignatureUpdatedPdfString = function(data, session, callback) { ... }
var _getSignatureInitPdfString = function(session, appId){ ... }
var _prepareFnaReportPdfMobile = function (session, app, now, bundle) { ... }
var _prepareProposalPdfMobile = function(session, app, now, bundle) { ... }
var _prepareAppFormProposerPdfMobile = function(session, app, now, bundle) { ... }
var _prepareAppFormInsuredPdfMobile = function(session, app, now, bundle, iCid) { ... }
async function _genAppFormInsuredPdfAsync(lang, application, bundle, template) {...}
const _genAppFormInsuredPdfPromise = (lang, application, bundle, template, iCid, index) => {...}
const _saveSignedPdf = function(appId, attId, pdfData, callback) { ... }
module.exports.getSignatureInitPdfString = _getSignatureInitPdfString;
```

keep code
```
if (session.platform) {
    nextProcess.push(_signature.getSignatureInitPdfString(session, appId));
} else {
    nextProcess.push(_signature.getSignatureInitUrl(session, appId));
}

// in _genAppFormInsuredPdf
let genMultiplePdf = Promise.resolve([]);
if (session.platform) {
genMultiplePdf = _genAppFormInsuredPdfAsync(lang, application, bundle, template);
} else {
let genPdfPromise = application.iCids.map((iCid, index) => {
  return _genAppFormInsuredPdfPromise(lang, application, bundle, template, iCid, index);
});
genMultiplePdf = Promise.all(genPdfPromise);
}

return genMultiplePdf.then((results) => {...}
```

keep code in _getSignatureInitUrl & _getSignatureInitPdfString 
```
// in result object, add following
crossAge: {}

// after _application.checkCrossAge(application)
if (caRes.success) {
    result.crossAge = caRes;
}
```

* `src/bz/handler/application/shield/index.js`

add code
```
module.exports.getSignatureUpdatedPdfString = sign.getSignatureUpdatedPdfString;
```

* `src/bz/NeedsHandler.js`

Add 3 fields when send email with emailUtils in order to set password  for pdf
* userType
* agentCode
* dob

```
EmailUtils.sendEmail({
                from: companyInfo.fromEmail, // TODO
                userType: email.id,
                agentCode: email.agentCode,
                dob: email.dob,
                to: _.join(email.to, ','),
                title: email.title,
                content: email.content,
                attachments: attArr
                }, resolve);

```

* `src/bz/application/common.js`

Add 3 fields when send email with emailUtils in order to set password  for pdf
* userType
* agentCode
* dob

```
EmailUtils.sendEmail({
                from: companyInfo.fromEmail, // TODO
                userType: email.id.indexOf('agent') > -1 ? 'agent' : 'client',
                agentCode: email.agentCode,
                dob: email.dob,
                to: _.join(email.to, ','),
                title: email.title,
                content: email.content,
                attachments: attArr
                }, resolve);

```

Modified the following function

```
const handleQuotPlanSumInsured = function(quotType, plan, baseProductCode) {
  const isShield = quotType === 'SHIELD'
  if (isShield) {
    switch(_.get(plan, 'covClass')) {
      case 'A':
        return 'Plan A';
      case 'B':
        return 'Plan B';
      case 'C':
        return 'Standard Plan';
      default:
        return '-';
    }
  } else if (['IND', 'AWICA', 'AWICP'].indexOf(baseProductCode) > -1) {
    const planSumInsured = _.get(plan, 'sumInsured', 0);
    return (planSumInsured === 0) ? '-' : getCurrency(planSumInsured);
  } else if (['AWT', 'PUL'].indexOf(baseProductCode) > -1) {
    return '-';
  } else if (['RHP'].indexOf(baseProductCode) > -1) {
    let planRetireInCome = Math.round(_.get(plan, 'gteedAnnualRetireIncome', 0));
    return planRetireInCome === 0 ? '-' : getCurrency(planRetireInCome);
  } else if (['NPE'].indexOf(baseProductCode) > -1) {
    let incomePayout = _.get(plan, 'incomePayout', 0);
    return incomePayout === 0 ? '-' : getCurrency(incomePayout);
  } else if (plan.saViewInd == 'Y') {
    return getCurrency(_.get(plan, 'sumInsured', 0));
  } else {
    return '-';
  }
}
```


* `src/bz/handler/QuotationHandler.js`
Add 1 field (isQQ) to check whether we need password protected on email attactment pdfs.

```
EmailUtils.sendEmail({
                    from: companyInfo.fromEmail,
                    to: _.join(email.to, ','),
                    title: email.title,
                    content: email.content,
                    attachments: allAttachments,
                    userType: email.id,
                    agentCode: email.agentCode,
                    dob: email.dob,
                    isQQ: email.isQQ,
                  }
```

* `src/bz/handler/application/common.js`
Use async/await to generate email pdf attactments

```
async function getPdfs (emails, generatedPdfs, failedGenPdfIds, isShield, cid) {
  const tempPdfs = {};
  const tempFailedPdfs = {};
  
  for(let i = 0; i < emails.length; i += 1) {
    const id = emails[i].id;
    let requiredPdfs = {};
    generatedPdfs[emails[i]] = {};
    failedGenPdfIds[emails[i]] = {};
    _.each(emails[i].attachmentIds, (id) => {
      requiredPdfs[id] = true;
    })
      _.set(tempPdfs, emails[i].id, {});
      _.set(tempFailedPdfs, emails[i].id, {});
      if (requiredPdfs.fna) {
        // bDao.getCurrentBundle(cid).then(bundle=>{
          tempPdfs[id].fna = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId, (quotation)=>{
            bDao.getBundle(cid, quotation.bundleId, (bundle) => {
              if (bundle.isFnaReportSigned){
                dao.getAttachment(bundle.id, PDF_NAME.FNA, (pdfResult)=>{
                  resolve(pdfResult.data);
                });
              } else {
                needsHandler.generateFNAReport({cid}, session, (pdfResult) => {
                  resolve(pdfResult.fnaReport);
                });
              }
            });
          });
        });
      } 
      if (requiredPdfs.bi){
        tempPdfs[id].bi = await new Promise(resolve => {
          quotDao.getQuotationPDF(emails[i].quotId, false, (pdfResult)=>{
            resolve(pdfResult.data);
          })
        });
      }
      /*
      * Let product Summary handled by ProposalUtils function
      else if (id === 'prodSummary'){
        promises.push(new Promise((psResolve)=>{
          quotDao.getQuotation(email.quotId,(quotation)=>{
            ProposalUtils.getProductSummaries(compCode, quotation, lang).then((pdfResult) => {
              generatedPdfs[email.id][id] = pdfResult;
              psResolve();
            });
          });
        }));
      }
      */
      if (requiredPdfs.shield_product_summary){
        tempPdfs[id].shield_product_summary = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId,(quotation)=>{
            quotHandler.getPlanDetailsByQuot(session, quotation).then((planDetails) => {
              ProposalUtils.getExtraAttachmentPdf(id, session.agent, quotation, planDetails, true).then((pdfResult) => {
                resolve(pdfResult);
              });
            });
          });
        }) 
      }
      if (requiredPdfs.phs) {
        tempPdfs[id].phs = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId, (quotation)=>{
            ProposalUtils.getProductHighlightSheets(compCode, quotation).then((pdfResult)=>{
              resolve(pdfResult);
            });
          });
        });
      }
      if (requiredPdfs.fib) {
        tempPdfs[id].fib = await new Promise(resolve => {
          quotDao.getQuotation(emails[i].quotId, (quotation)=>{
            ProposalUtils.getFundInfoBooklet(quotation.baseProductId, lang).then((pdfResult)=>{
              resolve(pdfResult);
            });
          });
        });
      } 
      if (requiredPdfs[PDF_NAME.eAPP] > -1 && isShield) {
        tempPdfs[id].eAPP = await new Promise(resolve => {
          appDao.getAppAttachment(emails[i].appId, id, function(pdfResult) {
            resolve(pdfResult.data);
          });
        })
      } 
      if (requiredPdfs.app) {
        tempPdfs[id].app = await new Promise(resolve => {
          appDao.getAppAttachment(emails[i].appId, PDF_NAME.eAPP, function(pdfResult) {
            resolve(pdfResult.data);
          });
        });
      }
      if (requiredPdfs.eCpd) {
        tempPdfs[id].eCpd = await new Promise(resolve => {
          appDao.getAppAttachment(emails[i].appId, PDF_NAME.eCPD, function(pdfResult) {
            resolve(pdfResult.data);
          });
        });
      } 
      if (requiredPdfs.prodSummary) {
        tempFailedPdfs[id].prodSummary = true;
      }
  }
  return {
    generatedPdfs: tempPdfs,
    failedGenPdfIds: tempFailedPdfs
  };
};

const preparePdfs = function(emails, cid, session, lang){
  let compCode = session.agent.compCode;
  return new Promise((resolve) => {
    let generatedPdfs = {};
    let failedGenPdfIds = {};
    let promises = [];
    let isShield = emails.isShield;
    resolve(getPdfs(emails, generatedPdfs, failedGenPdfIds, isShield, cid));
  });
};
```


* `src/bz/handler/application/shield/clientChoice.js`

Modify shield ClientChoice ROP into correct path
```
1.
Change
_.set(recommendationValues, 'ropBlock.shieldRopTable.shieldRopAnswer_' + i, insuredRopValue);

to
_.set(recommendationValues, 'rop_shield.ropBlock.shieldRopAnswer_' + i, insuredRopValue);

2.
Change
_.set(recommendationValues, 'ropBlock.ropQ2', _.get(quotRopBlock, 'ropQ2'));
_.set(recommendationValues, 'ropBlock.ropQ3', _.get(quotRopBlock, 'ropQ3'));
_.set(recommendationValues, 'ropBlock.ropQ1sub3', _.get(quotRopBlock, 'ropQ1sub3'));


to
_.set(recommendationValues, 'rop_shield.ropBlock.ropQ2', _.get(quotRopBlock, 'ropQ2'));
_.set(recommendationValues, 'rop_shield.ropBlock.ropQ3', _.get(quotRopBlock, 'ropQ3'));
_.set(recommendationValues, 'rop_shield.ropBlock.ropQ1sub3', _.get(quotRopBlock, 'ropQ1sub3'));

```

* `src/bz/handler/application/common.js`

Modify variable

```
ProposalUtils.getExtraAttachmentPdf("shield_product_summary", session.agent, quotation, planDetails, true).then((pdfResult) => {
```

* `src/bz/handler/QuotationHandler.js`
Add 1 field (isProtectBase) to check whether we need password protected on email attactment pdfs.

```
 return {
                      isProtectBase: isProtect,
                      fileName: attachment.fileName || attachment.id + '.pdf',
                      data: attData
                    };

```


* `src/bz/EmailHandler.js`
Add 1 field (isProtectBase) to check whether we need password protected on email attactment pdfs.

```
  return new Promise((resolve) => {
    CommonFunctions.protectBase64Pdf(pdf, pwd, (data) => {
      resolve({
        isProtectBase:true,
        fileName,
        data
      });


```

* `src/bz/NeedsHandler.js`
Add 1 field (isProtectBase) to check whether we need password protected on email attactment pdfs.

```
  CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
    callback({
      isProtectBase:true,
      fileName: 'FNA_Report.pdf', // TODO file name
      data: data
    });


```

* `src/bz/handler/application/common.js`
Add 1 field (isProtectBase) to check whether we need password protected on email attactment pdfs.

```
     promises.push(new Promise((resolve) => {
      let pdf = pdfs[email.id][attachmentId];
      if (attachmentId === 'fna'){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'fna_report.pdf',
            data: data
          });
        });
      } else if (attachmentId === 'bi' && ProposalUtils.checkIsShield(quotation)){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'policy_illustration.pdf',
            data: data
          });
        });
      } else if (attachmentId === 'bi' && !ProposalUtils.checkIsShield(quotation)){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'policy_illustration_document(s).pdf',
            data: data
          });
        });
      } else if (attachmentId === 'prodSummary'){
        resolve({
          isProtectBase:false,
          fileName: 'product_summary.pdf',
          data: pdf
        });
      } else if (attachmentId === 'shield_product_summary' && _.get(application, 'isProposalSigned')){
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'AXA Shield Product Summary.pdf',
            data: data
          });
        });
      }  else if (attachmentId === 'shield_product_summary' && !_.get(application, 'isProposalSigned')){
        resolve({
          isProtectBase:false,
          fileName: 'AXA Shield Product Summary.pdf',
          data: pdf
        });
      } else if (attachmentId === 'phs') {
        resolve({
          isProtectBase:false,
          fileName: 'product_highlight_sheet.pdf',
          data: pdf
        });
      } else if (attachmentId === 'fib') {
        resolve({
          isProtectBase:false,
          fileName: 'fund_info_booklet.pdf',
          data: pdf
        });
      } else if (attachmentId.indexOf(PDF_NAME.eAPP) > -1 && isShield) {
        let fileName = _.get(application, `supportDocuments.values.policyForm.sysDocs.${attachmentId}.title`, 'eapp_form') + '.pdf';
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: fileName,
            data: data
          });
        });
      } else if (attachmentId === 'app') {
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'eapp_form.pdf',
            data: data
          });
        });
      } else if (attachmentId === 'eCpd') {
        CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
          resolve({
            isProtectBase:true,
            fileName: 'eCPD_form.pdf',
            data: data
          });
        });
      }
    }));



* `src/bz/Quote/QuotDriver.js`

Modify the for each loop to for loop. because it does not work for LITE product

```for (var p in quotation.plans) {  ```
```for (var p = 0; p < quotation.plans.length; p++) { ```
```
if (
  basicPlanDetail.covCode === "PNP" ||
  basicPlanDetail.covCode === "PNP2" ||
  basicPlanDetail.covCode === "PNPP" ||
  basicPlanDetail.covCode === "PNPP2"
){
  ...
} else {
  ...
}
Mum advantages, Mum advantages plus, Family advantages, Family advantages plus Name exception.
```

* `src/bz/PDFHandler.js` 
```
The getByLang need 2 parameters, but we only pass 1 paramter, add back "lang" paramter.

```
if (reportTemplate) {
  var templates = getByLang(reportTemplate.template, lang);   
  pageNum += templates.length;
}
```

* `src/common/ProductUtils.js`
```
module.exports.checkEntryAge = (product, iResidence, pAges, iAges) 
```
* `src/bz/handler/application/common.js`
add compCode and lang
```
async function getPdfs (emails, generatedPdfs, failedGenPdfIds, isShield, cid, compCode, lang)
resolve(getPdfs(emails, generatedPdfs, failedGenPdfIds, isShield, cid));
```

* `src/bz/handler/application/common.js`
```
add template diabled for SATable
```

* `src/bz/handler/application/common.js`
Add if for FPXP Prod Summary
```
if (requiredPdfs.FPXProdSummary) {
        tempFailedPdfs[id].FPXProdSummary = true;
      }
```

* `src/bz/handler/ApplicationHandler.js`
Found crash in getSupervisorApproval, add checknull to fix crash.
```
if (approvalCase.foundCase) {}
if (rlsCb.RLSSTATUS) {}
```

* `src/bz/handler/ApplicationHandler.js`
New function
```
module.exports.genFNA = function(data, session, cb){
 ...
};

```

* `src/bz/handler/application/default/application.js`
New function
```
module.exports.genFNA = function(data, session, callback){
 ...
};

```
* `src/bz/cbDao/application.js`
Added filtering
```
  if (row.value.iCids) {
          row.value.iCids = _.sortBy(row.value.iCids);
        }
        if (row.value.insureds) {
        row.value.insureds = Object.keys(row.value.insureds)
        .sort()
        .reduce((result, key) => {
          result[key] = row.value.insureds[key];
          return result;
        }, {});
  }
```

* `src/bz/cbDao/quotation.js`
Added filtering
```
if (quot.insureds) {
  quot.insureds = Object.keys(quot.insureds).sort().reduce((result, key) => {
    result[key] = quot.insureds[key];
    return result;
  }, {});
}
```

* `src/bz/cbDao/views/main.js`
Added couchbase view clienteApplications()
```
clienteApplications: function (doc) {
 ...
}
```

* `src/bz/cbDao/product.js`
Edited function - remove throw() and return object even if product couldnt found
```
module.exports.prepareLatestProductVersion = function(covCodes) {
...
}
```

* `src/bz/cbDao/applicationHandler.js`
Edited function - return new extra data -> productInvalidList
```
module.exports.getAppListView = function(data, session, cb) {
    ...
    cb({
      ...
      productInvalidList
      ...
    })
    ...
}
```

* `src\bz\Quote\QuotDrive.js`
Add new function - filter product list. Per 10b, some products are for quick quote only.
```
productList = _.filter(productList, (product) => this.checkHidden(product, agent, extraParams.quickQuote) === true);
```
```
this.checkHidden = (product, agent, quickQuote) => {
if (quickQuote) {
  if (product.prodFeature && product.prodFeature.en){
    return !product.prodFeature.en.includes('quickQuoteHidden');
  }
} else {
  if (product.prodFeature && product.prodFeature.en && agent.channel.type !== 'FA'){
    return !product.prodFeature.en.includes('productHidden');
  }
}
return true;
};
```


* `src\bz\Quote\QuotDrive.js`
Add variables for 8b products about age
```
quotation.iAttainedAge = getAgeByMethod('current', riskCommenDate, parseDate(quotation.iDob));
quotation.pAttainedAge = getAgeByMethod('current', riskCommenDate, parseDate(quotation.pDob));
```

* `src\bz\handler\quote\ProposalUtils.js`
Add function to handle extra product summary, ie, ESC has extra term and condition.
```
const getExtraProductSummary = function (productId, attId, lang) {
  return new Promise((resolve) => {
    prodDao.getAttachmentWithLang(productId, attId, lang, (attachData) => {
      resolve(attachData && attachData.data);
    });
  });
};

const getProductSummaries = function (compCode, quotation, lang) {
  let plans = quotation.plans;
  if (quotation.extraProdSummary){
    plans = [...quotation.plans, ...quotation.extraProdSummary];
  }
  return Promise.all(_.map(plans, (plan) => {
    return new Promise((resolve) => {
      if (plan.productId) {
        if (plan.hasExtraProdSummary && plan.attId){
          getExtraProductSummary(plan.productId,plan.attId, lang).then(resolve);
        }
        else {
          getProductSummary(plan.productId, lang).then(resolve);
        }
      } else if (plan.covCode === quotation.baseProductCode) {
        getProductSummary(quotation.baseProductId, lang).then(resolve);
      } else {
        prodDao.getPlanByCovCode(compCode, plan.covCode, 'R', true).then((prod) => {
          getProductSummary(prod._id, lang).then((pdf) => {
            resolve(pdf);
          });
        });
      }
    });
  })).then((pdfs) => {
    return new Promise((resolve) => {
      CommonFunctions.mergePdfs(_.filter(pdfs, pdf => !!pdf), (data) => {
        resolve(data);
      });
    });
  });
};
```

* `src/bz/handler/application/common.js`
Use sync function to generate attactment and send email
```
sendEmailsToClientAndAgent(companyInfo, emails, session, pdfs, agent, failedGenPdfIds, cid, authToken, webServiceUrl);

async function sendEmailsToClientAndAgent(companyInfo, emails, session, pdfs, agent, failedGenPdfIds, cid, authToken, webServiceUrl){
  for(let i = 0; i < emails.length; i++) {
    await new Promise(resolve => {
      quotDao.getQuotation(emails[i].quotId, ((quotation)=>{
        emails[i].quotation = quotation;
        let standalone = session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !emails[i].appId;
  
        return quotHandler.getPlanDetailsByQuot(session, quotation).then((planDetails) => {
          appDao.getApplication(emails[i].appId, ((application)=>{
            logger.log('INFO: sendApplicationEmail - _prepareEmailAttachments', cid);
  
            // Step 2: Use the generated pdfs, to generate attachments
            return _prepareEmailAttachments(pdfs, emails[i], emails[i].quotation, application).then((appAttachments)=>{
              // Step 3: failedGenPdfIds will become input parameter to generate attachments in product functions
              // return ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, _.keys(failedGenPdfIds[email.id])).then((productAttachments) => {
                return ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, _.keys(failedGenPdfIds[emails[i].id])).then((productAttachments) => {
  
                let allAttachments = [];
                allAttachments = allAttachments.concat(appAttachments);
                allAttachments = allAttachments.concat(productAttachments);
                _.each(emails[i].embedImgs, (value, key) => {
                  allAttachments.push({
                    fileName: key,
                    cid: key,
                    data: value
                  });
                });
                logger.log('INFO: sendApplicationEmail - sendEmail callApi', cid, emails[i].quotId);
                sendEmail({
                  from: companyInfo.fromEmail,
                  to: emails[i].to[0],
                  title: emails[i].title,
                  content: emails[i].content,
                  attachments: allAttachments,
                  userType: emails[i].id.indexOf('agent') > -1 ? 'agent': 'client',
                  agentCode: emails[i].agentCode,
                  dob: emails[i].dob
                }, (result) => {
                  logger.log('INFO: sendApplicationEmail - sendEmail result', cid, result);
                  resolve();
                }, authToken, webServiceUrl);
              });
            });
          }));
        });
      }));
    })
  }
};
```

* `src/bz/PDFHandler.js`
```
if (["TPX", "TPPX", "HIM", "HER","ESC","PNP","PNPP","PNP2","PNPP2","DTS","DTJ"].indexOf(basicPlanDetails.covCode) >= 0) {
```

* `src/bz/nativeRunner.js`
```
  change "return java.callStaticMethodSync('pdf.Pdf', 'addTitle', base64Pdf, title);"
  to "  return new Promise(resolve => {
    NativeUtils.addPdfTitle(base64Pdf, title, resolve);
  });"
```

* `src/bz/handler/quote/ProposalUtils.js`
```
  change "resolve(pdf && CommonFunctions.addPdfTitle(pdf, labelObject.fileName));"
  to "CommonFunctions.addPdfTitle(pdf, labelObject.fileName).then(pdfWithTitle => {
        resolve(pdf && pdfWithTitle);
      });"
```
