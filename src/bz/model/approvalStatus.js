const FAADMIN = 'FAADMIN';
const FAAGENT = 'FAAGENT';
const ONELEVEL_SUBMISSION = 'ONELEVEL_SUBMISSION';

const APPRVOAL_STATUS_APPROVED  = 'A';
const APPRVOAL_STATUS_REJECTED  = 'R';
const APPRVOAL_STATUS_EXPIRED   = 'E';
const APPRVOAL_STATUS_SUBMITTED = 'SUBMITTED';
const APPRVOAL_STATUS_PDOC      = 'PDoc';
const APPRVOAL_STATUS_PDIS      = 'PDis';
const APPRVOAL_STATUS_PFAFA     = 'PFAFA';
const APPRVOAL_STATUS_PDOCFAF   = 'PDocFAF';
const APPRVOAL_STATUS_PDISFAF   = 'PDisFAF';
const APPRVOAL_STATUS_PCDAA     = 'PCpaA';
const APPRVOAL_STATUS_PDOCCda   = 'PCpaA';
const APPRVOAL_STATUS_PDISCda   = 'PCpaA';

const STATUS = {
    A:          'Approved',
    R:          'Rejected',
    E:          'Expired',
    SUBMITTED:  'Pending Supervisor Approval',
    PDoc:       'Pending Document',
    PDis:       'Pending Discussion',
    PFAFA:      'Pending FA firm approval',
    PDocFAF:    'Pending Document (FA Firm)',
    PDisFAF:    'Pending Discussion (FA Firm)',
    PCdaA:      'Pending CDA approval',
    PDocCda:    'Pending Document (CDA)',
    PDisCda:    'Pending Discussion (CDA)'
};

module.exports = {
  STATUS, 
  FAADMIN,
  FAAGENT,
  ONELEVEL_SUBMISSION,
  APPRVOAL_STATUS_APPROVED,
  APPRVOAL_STATUS_REJECTED,
  APPRVOAL_STATUS_EXPIRED,
  APPRVOAL_STATUS_SUBMITTED,
  APPRVOAL_STATUS_PDOC,
  APPRVOAL_STATUS_PDIS,
  APPRVOAL_STATUS_PFAFA,
  APPRVOAL_STATUS_PDOCFAF,
  APPRVOAL_STATUS_PDISFAF,
  APPRVOAL_STATUS_PCDAA,
  APPRVOAL_STATUS_PDOCCda,
  APPRVOAL_STATUS_PDISCda,
}