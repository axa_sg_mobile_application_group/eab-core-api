const uuid = require('uuid');
const _ = require('lodash');

const {
  getFromRedis,
  setToRedis
} = require('../utils/CommonUtils');

const logger = global.logger;

const getPdfToken = (docId, attId, session) => {
  return new Promise((resolve) => {
    const now = Date.now();
    let token = createPdfToken(docId, attId, now, session.loginToken);
    setPdfTokensToRedis([token], () => {
      resolve(token.token);
    });
  });
};

//Function: for Signature, clear PDF token
const clearInvalidPdfToken = () => {
  const now = Date.now();
  getFromRedis('attachmentToken', (tokens) => {
    if (!tokens) {
      tokens = {};
    }
    for (var key in tokens) {
      let value = tokens[key];
      if (now > value.expiryTime) {
        delete tokens[key];
      }
    }
    setToRedis('attachmentToken', tokens);
  });
};

//Function: for Signature, create PDF token
const createPdfToken = (docId, attId, now, lToken) => {
  const expiryTime = new Date().setTime(now + global.config.signdoc.pdfTimeout);

  //Clear Timeout Token before start
  clearInvalidPdfToken();

  //Generate new token
  return {
    token: uuid.v4(),
    docId: docId,
    attId: attId,
    lToken: lToken,
    expiryTime: expiryTime
  };
};

const setPdfTokensToRedis = (pdfTokens, callback) => {
  logger.log('INFO: setPdfTokensToRedis - start');
  getFromRedis('attachmentToken', (tokens) => {
    if (pdfTokens instanceof Array) {
      if (pdfTokens.length > 0) {
        _.forEach(pdfTokens, (pdfToken, index) => {
          logger.log('INFO: setPdfTokensToRedis - prepare token array', (index + 1), 'of', pdfTokens.length, '-', pdfToken.token, pdfToken.docId, pdfToken.attId, pdfToken.expiryTime);
          tokens[pdfToken.token] = {
            docId: pdfToken.docId,
            attId: pdfToken.attId,
            expiryTime: pdfToken.expiryTime,
            lToken: pdfToken.lToken
          };
        });
        setToRedis('attachmentToken', tokens);
      }
    } else {
      logger.error('ERROR: setPdfTokensToRedis - pdfTokens is not an array');
    }

    logger.log('INFO: setPdfTokensToRedis - end');
    callback && callback();
  });
};

module.exports = {
  getPdfToken,
  createPdfToken,
  setPdfTokensToRedis
};

