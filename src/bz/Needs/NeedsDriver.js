'use strict';

var driver = new Object();

if(typeof exports === 'object' && typeof module === 'object') {
  module.exports = driver;

  var utils = require('../Quote/utils');
  var runCalc = utils.runCalc,
    getCurrency = utils.getCurrency,
    returnResult = utils.returnResult,
    math = utils.math,
    debug = utils.debug;
}

driver.evalMandatoryFunc = function(data) {
    let result = runCalc(data.fnStr, data.needStr, data.qForm);
    return result;
}

driver.calRiskFunc = function(data) {
    return runCalc(data.fnStr, data.rAns, data.rQuests);
}
