'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _ = require('lodash');
var crypto = require('crypto');

module.exports.calcAge = function (birthday) {
    var today = new Date();
    var birthDate = new Date(birthday);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || m === 0 && today.getDate() < birthDate.getDate()) {
        age--;
    }
    return age;
};

/**
 * Deep diff between two object, using lodash
 * @param  {Object} object Object compared
 * @param  {Object} base   Object to compare with
 * @return {Object}        Return a new object who represent the diff
 */
module.exports.difference = function (object, base) {
    function changes(object, base) {
        return _.transform(object, function (result, value, key) {
            if (!_.isEqual(value, base[key])) {
                result[key] = _.isObject(value) && _.isObject(base[key]) ? changes(value, base[key]) : value;
            }
        });
    }
    return changes(object, base);
};

module.exports.getStrSignature = function (value, key) {
    if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) == 'object') {
        value = JSON.stringify(value);
    }
    return crypto.createHmac('sha256', key).update(value).digest('hex');
};

module.exports.convertSGDtoInputCcy = function (sgdValue, ccy, compCode, decimal) {
    var optionsMap = !!global.optionsMap && global.optionsMap;
    var currencies = _.get(optionsMap, 'ccy.currencies');
    var convertedValue = sgdValue;
    _.each(currencies, function (obj) {
        if (obj.compCode === compCode) {
            _.each(obj.options, function (curencyObj) {
                if (curencyObj.value === ccy) {
                    convertedValue = sgdValue * curencyObj.exRate;
                }
            });
        }
    });
    var factor = Math.pow(10, decimal);
    return Math.round(convertedValue * factor) / factor;
};

module.exports.convertForeignCcytoSGD = function (foreignValue, ccy, compCode, decimal) {
    var optionsMap = !!global.optionsMap && global.optionsMap;
    var currencies = _.get(optionsMap, 'ccy.currencies');
    var convertedValue = foreignValue;
    _.each(currencies, function (obj) {
        if (obj.compCode === compCode) {
            _.each(obj.options, function (curencyObj) {
                if (curencyObj.value === ccy) {
                    convertedValue = foreignValue / curencyObj.exRate;
                }
            });
        }
    });
    var factor = Math.pow(10, decimal);
    return Math.round(convertedValue * factor) / factor;
};