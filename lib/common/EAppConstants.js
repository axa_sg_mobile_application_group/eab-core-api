'use strict';

module.exports = {
    // appStatus
    APPLYING: 'APPLYING',
    SUBMITTED: 'SUBMITTED',
    INVALIDATED: 'INVALIDATED',
    INVALIDATED_SIGNED: 'INVALIDATED_SIGNED',

    // RLS
    RLSPATH: 'rlsJobStatus',
    PAYMENTTRANSFERRED: 'TRANSFERRED'
};