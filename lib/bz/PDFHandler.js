"use strict";

var _ = require('lodash');
var getByLang = require('./utils/CommonUtils').getByLang;
var commonFunction = require('./CommonFunctions');
var XSLT = require('../../../../app/utilities/XSLTHandler');
var logger = global.logger || console;
var moment = require('moment');
var ConfigConstant = require('../app/constants/ConfigConstants');

var _getStaticPdf = function _getStaticPdf(pdfCodes, reportData, reportTemplates, basicPlanDetails, callback) {
  var lang = 'en'; // TODO

  var pageNum = 1;
  var startPages = {};
  var pageNumberGroup = [];
  var pageNumberGroupIndex = 0;
  _.each(pdfCodes, function (pdfCode) {
    var pdfTemplateConfig = _.find(basicPlanDetails.reportTemplate, function (reportTemplate) {
      return reportTemplate.pdfCode === pdfCode;
    }) || {};

    if (pdfTemplateConfig.resetPaging === 'Y') {
      pageNumberGroupIndex += 1;
      pageNum = 1;
    }
    startPages[pdfCode] = pageNum;
    var reportTemplate = reportTemplates[pdfCode];

    if (reportTemplate) {
      var templates = getByLang(reportTemplate.template);
      pageNum += templates.length;
    }

    pageNumberGroup[pageNumberGroupIndex] = pageNum;
  });

  var promises = [];
  pageNumberGroupIndex = 0;
  _.each(pdfCodes, function (pdfCode) {
    if (reportTemplates[pdfCode]) {
      var totalPageNumber = void 0;
      var pdfTemplateConfig = _.find(basicPlanDetails.reportTemplate, function (reportTemplate) {
        return reportTemplate.pdfCode === pdfCode;
      }) || {};
      if (pdfTemplateConfig.resetPaging === 'Y') {
        pageNumberGroupIndex += 1;
      }
      totalPageNumber = pageNumberGroup[pageNumberGroupIndex] || 1;
      promises.push(new Promise(function (resolve) {
        var data = Object.assign({}, reportData.root, reportData[pdfCode]);
        XSLT.data2Xml({ root: data }, function (xml) {
          var xsl = XSLT.prepareReportXSL(reportTemplates[pdfCode], lang, startPages[pdfCode], totalPageNumber - 1);

          XSLT.xsltTransform(xml, xsl, function (xslt) {
            resolve(xslt);
          });
        });
      }));
    }
  });

  Promise.all(promises).then(function (result) {
    var styles = [];
    _.each(reportTemplates, function (reportTemplate) {
      if (reportTemplate.style) {
        styles.push(reportTemplate.style);
      }
    });
    var contentHtml = _.join(result, '');
    var html = XSLT.prepareHtml(contentHtml, _.join(styles, '\n'));

    return XSLT.html2Pdf(html).then(callback);
  }).catch(function (err) {
    logger.error(err);
    callback(false);
  });
};

var _getDynamicPdf = function _getDynamicPdf(pdfCodes) {
  var reportData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { root: {} };
  var reportTemplates = arguments[2];
  var basicPlanDetails = arguments[3];
  var callback = arguments[4];


  var _handleHeaderFooter = function _handleHeaderFooter(options, template, type) {
    var lang = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "en";

    var templateHeader = _.get(template, 'header.' + lang);
    var templateFooter = _.get(template, 'footer.' + lang);

    if (templateFooter !== undefined && templateFooter.length > 0) {
      templateFooter = templateFooter.replace("[[@PAGE_NO]]", "<span class=\"page_no\"/>").replace("[[@TOTAL_PAGE]]", "<span class=\"total_page_no\"/>");
    }

    options.header[type] = templateHeader;
    options.footer[type] = templateFooter;
  };

  /** This is a list of  
   * {
   *    pdfOptions {
          numCoverPage: 0,
          header: {
            first:    "",
            default:  ""
          },
          footer: {
            first:    ""
            default:  ""
          }
        },
          pdfCodes: []
      }
   * 
  */

  var pdfToGenerate = [];
  var curPdf = null;

  var templateIndex = 0;
  /** Group PDF by resetPage Flag*/
  _.forEach(basicPlanDetails.reportTemplate, function (curTemplate) {
    if (templateIndex == 0) {
      curPdf = {
        pdfOptions: {
          header: {
            first: "",
            default: ""
          },
          footer: {
            first: "",
            default: ""
          }
        },

        pdfCodes: [curTemplate.pdfCode]
      };

      pdfToGenerate.push(curPdf);
    } else {
      if (curTemplate.resetPaging && curTemplate.resetPaging === 'Y') {
        curPdf = {
          pdfOptions: {
            header: {
              first: "",
              default: ""
            },
            footer: {
              first: "",
              default: ""
            }
          },

          pdfCodes: [curTemplate.pdfCode]
        };

        pdfToGenerate.push(curPdf);
      } else {
        curPdf.pdfCodes.push(curTemplate.pdfCode);
      }
    }

    templateIndex++;
  });

  /** Populate header and footer for each PDF to generate. 
   * The firstHeader and firstFooter is alwasy on the first template;
   * The defaulHeader and defaultFooter on teh seocnd template.*/
  _.forEach(pdfToGenerate, function (curPdfTogenerate) {

    var index = 0;
    _.forEach(curPdfTogenerate.pdfCodes, function (pdfCode) {
      var reportTemplate = reportTemplates[pdfCode];
      if (index == 0) {
        _handleHeaderFooter(curPdfTogenerate.pdfOptions, reportTemplate, "first", "en");
      }

      if (index == 1) {
        _handleHeaderFooter(curPdfTogenerate.pdfOptions, reportTemplate, "default", "en");
      }

      index++;
    });
  });

  var _generatePdf = function _generatePdf(curPdfTogenerate, callback_after_generate) {
    var pdfOptions = curPdfTogenerate.pdfOptions;
    var curPdfCodes = curPdfTogenerate.pdfCodes;
    var reportHtml = "",
        reportStyle = "";
    var data = Object.assign({}, reportData.root);

    reportHtml += '<header id="firstHeader" style="width: calc(100% - 120px);margin-left:60px; margin-right: 120px;">' + _.get(pdfOptions, "header.first") + '</header>';
    if (pdfOptions.header.default !== undefined && pdfOptions.header.default.length > 0) {
      reportHtml += '<header id="defaultHeader" style="width: calc(100% - 120px);margin-left:60px; margin-right: 120px;">' + _.get(pdfOptions, "header.default") + '</header>';
    }

    reportHtml += '<footer id="firstFooter" style="width: calc(100% - 120px);margin-left:60px; margin-right: 60px;">' + _.get(pdfOptions, "footer.first") + '</footer>';
    if (pdfOptions.footer.default !== undefined && pdfOptions.footer.default.length > 0) {
      reportHtml += '<footer id="defaultFooter" style="width: calc(100% - 120px);margin-left:60px; margin-right: 60px;">' + _.get(pdfOptions, "footer.default") + '</footer>';
    }

    _.forEach(curPdfCodes, function (pdfCode) {
      var reportTemplate = reportTemplates[pdfCode];
      data = Object.assign({}, data, reportData[pdfCode]);

      reportHtml += XSLT.prepareDynamicReportHtml(reportTemplate.template, "en");
      reportStyle += reportTemplate.style;
    });

    XSLT.data2Xml({ root: data }, function (xml) {
      var xsl = XSLT.prepareDynamicReportXsl(reportHtml, pdfOptions, false);

      var startTime = new Date();
      XSLT.xsltTransform(xml, xsl, function (xslt) {
        var elapsedTimeMs = new Date() - startTime;
        logger.log("EXEC: xsltTransform: Elapsed " + elapsedTimeMs + "ms");
        var html = XSLT.prepareDynamicHtml(xslt, pdfOptions, reportStyle);
        commonFunction.convertHtml2Pdf(html, pdfOptions, callback_after_generate);
      });
    });
  };

  var promises = [];

  _.forEach(pdfToGenerate, function (curPdfTogenerate) {
    promises.push(new Promise(function (resolve) {
      _generatePdf(curPdfTogenerate, function (pdf) {
        resolve(pdf);
      });
    }));
  });

  Promise.all(promises).then(function (result) {
    commonFunction.mergePdfs(result, callback);
  }).catch(function (err) {
    logger.error(err);
    callback(false);
  });
};

var _getBiPdfMobile = function _getBiPdfMobile(pdfCodes, reportData, reportTemplates, basicPlanDetails, isDynamic, callback) {
  var lang = 'en'; // TODO

  var pageNum = 1;
  var startPages = {};
  var pageNumberGroup = [];
  var pageNumberGroupIndex = 0;
  _.each(pdfCodes, function (pdfCode) {
    var pdfTemplateConfig = _.find(basicPlanDetails.reportTemplate, function (reportTemplate) {
      return reportTemplate.pdfCode === pdfCode;
    }) || {};

    if (pdfTemplateConfig.resetPaging === 'Y') {
      pageNumberGroupIndex += 1;
      pageNum = 1;
    }
    startPages[pdfCode] = pageNum;
    var reportTemplate = reportTemplates[pdfCode];

    if (reportTemplate) {
      var templates = getByLang(reportTemplate.template, lang);
      pageNum += templates.length;
    }

    pageNumberGroup[pageNumberGroupIndex] = pageNum;
  });

  var hasOverlay = false;
  var isQuickQuote = false;
  var dynamicData = Object.assign({}, reportData.root);
  var promises = [];

  pageNumberGroupIndex = 0;
  _.each(pdfCodes, function (pdfCode) {
    if (reportTemplates[pdfCode]) {
      var totalPageNumber = void 0;
      var pdfTemplateConfig = _.find(basicPlanDetails.reportTemplate, function (reportTemplate) {
        return reportTemplate.pdfCode === pdfCode;
      }) || {};
      if (pdfTemplateConfig.resetPaging === 'Y') {
        pageNumberGroupIndex += 1;
      }
      totalPageNumber = pageNumberGroup[pageNumberGroupIndex] || 1;
      hasOverlay = hasOverlay || _.has(reportTemplates[pdfCode], "overlay");

      promises.push(new Promise(function (resolve) {
        var data = Object.assign({}, reportData.root, reportData[pdfCode]);
        if (isDynamic) {
          dynamicData = Object.assign({}, dynamicData, reportData[pdfCode]);
        }
        var quickQuote = _.get(data, "mainInfo.quickQuote");
        if (!_.isEmpty(quickQuote)) {
          isQuickQuote = quickQuote === "Y";
        }

        XSLT.data2Xml({ root: isDynamic ? dynamicData : data }, function (xml) {
          var xsl = isDynamic ? XSLT.prepareDynamicReportXsl(reportTemplates[pdfCode], lang, startPages[pdfCode], totalPageNumber - 1) : XSLT.prepareReportXSL(reportTemplates[pdfCode], lang, startPages[pdfCode], totalPageNumber - 1);

          XSLT.xsltTransform(xml, xsl, function (xslt) {
            resolve(xslt);
          });
        });
      }));
    }
  });

  Promise.all(promises).then(function (result) {
    var styles = [];
    var bundledPlanNumOfPage = 0;
    _.each(reportTemplates, function (reportTemplate) {
      if (reportTemplate.style) {
        styles.push(reportTemplate.style);
      }
    });
    var contentHtml = _.join(result, '');
    var html = isDynamic ? XSLT.prepareDynamicHtml(contentHtml, _.join(styles, '\n')) : XSLT.prepareHtml(contentHtml, _.join(styles, '\n'));

    var overlayNumOfPage = hasOverlay ? 3 : 0;
    var overlayType = 1;
    if (["TPX", "TPPX", "HIM", "HER", "ESC", "PNP", "PNPP", "PNP2", "PNPP2", "DTS", "DTJ"].indexOf(basicPlanDetails.covCode) >= 0) {
      overlayType = 2;
      overlayNumOfPage = 2;
    } else if (["LMP", "LITE"].indexOf(basicPlanDetails.covCode) >= 0) {
      if (_.find(reportTemplates, function (report) {
        return ["LMP_BUNDLEDPLAN", "LITE_Bundled_Plan"].includes(report.pdfCode);
      })) {
        bundledPlanNumOfPage = 6;
      }
      overlayType = 3;
    }

    var pdfOptions = {
      type: "BI",
      headerHeight: 0,
      footerHeight: 96,
      overlayType: overlayType,
      overlayNumOfPage: overlayNumOfPage,
      bundledPlanNumOfPage: bundledPlanNumOfPage,
      isQuickQuote: isQuickQuote,
      isJoint: basicPlanDetails.covCode === "DTJ" || reportData[pdfCodes[0]].cover && reportData[pdfCodes[0]].cover.sameAs === "N",
      isLifeAssured: basicPlanDetails.covCode === "PVLVUL" || basicPlanDetails.covCode === "PVTVUL",
      footer: {
        releaseVersion: global.config.biReleaseVersion,
        date: moment().format(ConfigConstant.DateTimeFormat3)
      }
    };
    return XSLT.html2Pdf(html, pdfOptions).then(callback);
  }).catch(function (err) {
    logger.error(err);
    callback(false);
  });
};

module.exports.getReportPdf = function (pdfCodes, reportData, reportTemplates, isDynamic, basicPlanDetails, callback) {
  // TODO: add checking on mobile / web
  var isMobile = true;
  if (isMobile) {
    _getBiPdfMobile(pdfCodes, reportData, reportTemplates, basicPlanDetails, isDynamic, callback);
  } else {
    if (!isDynamic) {
      _getStaticPdf(pdfCodes, reportData, reportTemplates, basicPlanDetails, callback);
    } else {
      _getDynamicPdf(pdfCodes, reportData, reportTemplates, basicPlanDetails, callback);
    }
  }
};

module.exports.getFnaReportPdf = function (reportData, reportTemplates, callback) {
  var data = Object.assign({}, reportData.root);
  new Promise(function (resolve) {
    XSLT.data2Xml({ root: data }, function (xml) {
      var xsl = XSLT.prepareFnaReportXsl(reportTemplates, 'en', 1, 1);
      XSLT.xsltTransform(xml, xsl, function (xslt) {
        resolve(xslt);
      });
    });
  }).then(function (result) {
    var contentHtml = _.join(result, '').split('[[@newLine]]').join('<br/>');
    var style = "";
    var params = {
      type: "FNA",
      header: {
        height: "40px",
        contents: {}
      },
      footer: {
        height: "40px",
        contents: {}
      },
      headerHeight: 30,
      footerHeight: 30
    };

    _.forEach(reportTemplates, function (t, index) {
      if (index === 0) {
        var hf = _.at(t, ['header.en', 'footer.en']);
        if (hf[0]) {
          params.header.contents.first = '<div class="pageMargin">' + hf[0] + '</div>';
        }
        if (hf[1]) {
          params.footer.contents.first = '<div class="pageMargin">' + hf[1] + '</div>';
        }
      } else if (index === 1) {
        var hf = _.at(t, ['header.en', 'footer.en']);
        if (hf[0]) {
          params.header.contents.default = '<div class="pageMargin">' + hf[0] + '</div>';
        }
        if (hf[1]) {
          params.footer.contents.default = '<div class="pageMargin">' + hf[1] + '</div>';
        }
      }
      style += _.at(t, 'style')[0] || "";
    });

    var html = XSLT.prepareHtml(contentHtml, style);

    XSLT.html2Pdf(html, params).then(callback).catch(function (err) {
      logger.error(err);
    });
  }).catch(function (err) {
    callback(false);
  });
};

module.exports.getSupervisorTemplatePdf = function (reportData, reportTemplates, lang, callback) {
  new Promise(function (resolve) {
    if (reportData) {
      reportData.policyId = !_.isEmpty(reportData.proposalNumber) ? reportData.proposalNumber : reportData.policyId;
      reportData.supervisorApproveRejectDate = reportData.supervisorApproveRejectDate ? moment(reportData.supervisorApproveRejectDate).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3) : '';
      reportData.approveRejectDate = reportData.approveRejectDate ? moment(reportData.approveRejectDate).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3) : '';
    }

    XSLT.data2Xml({ root: reportData }, function (xml) {
      var xsl = XSLT.prepareSupervisorXSL(reportTemplates.template[lang], lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, function (xslt) {
        resolve(xslt);
      });
    });
  }).then(function (result) {
    var style = '';
    if (reportTemplates.style) {
      style += reportTemplates.style;
    }
    var html = XSLT.prepareHtml(result, style);
    var params = {
      header: {
        height: '40px',
        contents: {}
      },
      footer: {
        height: '40px',
        contents: {}
      }
    };

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch(function (err) {
    logger.error(err);
    callback(false);
  });
};

module.exports.getAppFormPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise(function (resolve) {
    XSLT.data2Xml(reportData, function (xml) {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, function (xslt) {
        resolve(xslt);
      });
    });
  }).then(function (result) {
    var params = {
      type: "EAPP",
      headerHeight: 30,
      footerHeight: 87.5,
      header: {
        height: '40px',
        contents: {}
      },
      footer: {
        height: '90px',
        contents: {}
      }
    };

    var style = '';
    reportTemplates.forEach(function (template, index) {
      if (template.style) {
        style += template.style;
      }
      if (index == 0) {
        params.header.contents.first = '<div class=\"appFormFooter\">' + template.header[lang] + '</div>';
        params.footer.contents.first = '<div class=\"appFormFooter\">' + template.footer[lang] + '</div>';
      } else {
        params.header.contents.default = '<div class=\"appFormFooter\">' + template.header[lang] + '</div>';
        params.footer.contents.default = '<div class=\"appFormFooter\">' + template.footer[lang] + '</div>';
      }
    });

    var html = XSLT.prepareHtml(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch(function (err) {
    callback(false);
  });
};

module.exports.getPremiumPaymentPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise(function (resolve) {
    XSLT.data2Xml(reportData, function (xml) {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, function (xslt) {
        resolve(xslt);
      });
    });
  }).then(function (result) {
    var params = {
      header: {
        height: '40px',
        contents: {}
      },
      footer: {
        height: '90px',
        contents: {}
      }
    };

    var style = '';
    reportTemplates.forEach(function (template, index) {
      if (template.style) {
        style += template.style;
      }
      // params.header.contents.default = '<div class=\"pageMargin\">' + template.header[lang] + '</div>';
      // params.footer.contents.default = '<div class=\"pageMargin\">' + template.footer[lang]  + '</div>';
    });

    var html = XSLT.prepareHtmlOnePage(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch(function (err) {
    callback(false);
  });
};

module.exports.getECpdPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise(function (resolve) {
    XSLT.data2Xml(reportData, function (xml) {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, function (xslt) {
        resolve(xslt);
      });
    });
  }).then(function (result) {
    var params = {
      header: {
        height: '40px',
        contents: {}
      },
      footer: {
        height: '90px',
        contents: {}
      }
    };

    var style = '';
    reportTemplates.forEach(function (template, index) {
      if (template.style) {
        style += template.style;
      }
      params.header.contents.default = '<div class=\"pageFooter\">' + template.header[lang] + '</div>';
      params.footer.contents.default = '<div class=\"pageFooter\">' + template.footer[lang] + '</div>';
    });

    var html = XSLT.prepareHtmlOnePage(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch(function (err) {
    callback(false);
  });
};