'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var async = require('async');
var _ = require('lodash');
var moment = require('moment');
var logger = global.logger || console;

var _require = require('../utils/RemoteUtils'),
    callApiByGet = _require.callApiByGet;

var _require2 = require('./invalidateFunctions'),
    getAllCustomerWithFundNHAF = _require2.getAllCustomerWithFundNHAF,
    getAllAgents = _require2.getAllAgents,
    insertNoticeToAgent = _require2.insertNoticeToAgent,
    getAllValidBundleId = _require2.getAllValidBundleId,
    getImpactedClientIds = _require2.getImpactedClientIds;

var dao = require('../cbDaoFactory').create();
var bundleDao = require('../cbDao/bundle');
var agentDao = require('../cbDao/agent');
var prodDao = require('../cbDao/product');

var sysParameter = 'sysParameter';
var rules = {
  "IND": {
    "invalidateAllVersion": true
  }
  // const rules = require('./inflightRules/rules.json');
};var sysParameterJson = {
  "sessionSecret": "7W1M6ufe5feoFIxZ",
  "couchbase_encrytion_key": "pkp1BeaRdyYE8PrX",
  "quotationIdEncryptionKey": "t1vK729zUB1PiOFo",
  "output": {},
  "useDocCache": true,
  "enableRSA": true,
  "audDocRecordLimit": 100,
  "docCacheTimeout": 60000,
  "maxConcurrentUserFaFirm": 5,
  "maxConcurrentUserNonFaFirm": 1,
  "defaultRedisValidTime": 1800000,
  "tempFolder": "temp",
  "signdoc": {
    "pdfTimeout": 1800000,
    "secretKey": "HiIJsJcxtJc2LknO"
  },
  "disableSMS": false,
  "signExpireWarningDay": 14,
  "signExpireDay": 21,
  "after_pEAppSigned_days": 21,
  "EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION": 14,
  "SECONDARY_PROXY_ASSIGNMENT_DAY": 7,
  "debugMode": false,
  "crossAgeDay": 14,
  "allowMultipleLogin": false,
  "SUPPORT_DOCUMENTS_MAX_FILE_SIZE_IN_BYTE": 10485760,
  "abnormalSuppDocsCaseToEmail": "application3.desk@axa365.onmicrosoft.com,satish.dubey@axa.com.sg,vincent.tan@axa.com.sg,ishu.gupta@axa.com.sg,daevesh.sharda@axa.com.sg,aiza.saysay@axa.com.sg,ping.jen@axa.com.sg,adrian.li@axa.com.sg",
  "ENV_NAME": "PROD",
  "inflightCaseHandling": {
    "fromTime": "2018-09-24 00:00:00",
    "toTime": "2018-09-24 23:59:59",
    "scheduleCron": "55 11 * 1-12 *",
    "handleCovCode": {
      "SAV": false,
      "IND": false,
      "BAA": false,
      "ASIM": false,
      "FPX": false,
      "FSX": false,
      "TPX": false,
      "TPPX": false,
      "AWT": false,
      "PUL": false,
      "LMP": true,
      "ESP": false,
      "RHP": false,
      "HIM": false,
      "HER": false,
      "AWICA": false,
      "AWICP": false
    },
    "perCaseTimeout": 0
  },
  "GenericCampaigninflightCaseHandling": {
    "fromTime": "2018-12-21 00:00:00",
    "toTime": "2018-12-21 23:59:59",
    "scheduleCron": "06 15 * 1-12 *",
    "handleCovCode": {
      "SAV": true,
      "IND": true,
      "BAA": true,
      "ASIM": true,
      "FPX": true,
      "FSX": true,
      "TPX": true,
      "TPPX": true,
      "AWT": true,
      "PUL": true,
      "LMP": true,
      "ESP": true,
      "RHP": true,
      "HIM": true,
      "HER": true,
      "AWICA": true,
      "AWICP": true
    },
    "perCaseTimeout": 0,
    "dataPatchCidsList": "release8a-datapatch-cids",
    "doDataPatch": true
  },
  "GenericFundflightCaseHandling": {
    "fromTime": "2018-12-28 00:00:00",
    "toTime": "2018-12-28 23:59:59",
    "scheduleCron": "53 16 * 1-12 *",
    "createDateCompare": 1545307200000,
    "handleCovCode": {
      "SAV": true,
      "IND": true,
      "BAA": true,
      "ASIM": true,
      "FPX": true,
      "FSX": true,
      "TPX": true,
      "TPPX": true,
      "AWT": true,
      "PUL": true,
      "LMP": true,
      "ESP": true,
      "RHP": true,
      "HIM": true,
      "HER": true,
      "AWICA": true,
      "AWICP": true
    },
    "perCaseTimeout": 0,
    "dataPatchCidsList": "release8a-datapatch-cids",
    "doDataPatch": true
  },
  "datapatch": {
    "fromTime": "2018-08-14 00:00:00",
    "toTime": "2018-08-14 23:59:59",
    "scheduleCron": "0 18 * 1-12 *",
    "dataPatchDocId": "sheildInflightDatapatch2018"
  },
  "biReleaseVersion": "9.0"

  // Couchbase File Name
};var currentDate = new Date();
var jobDetailsDocKey = 'FundInflightInvalidateJob_' + currentDate.getUTCFullYear() + '_' + (currentDate.getUTCMonth() + 1) + '_' + currentDate.getDate();

// Information of Invalidated Alert Message when agent Login
var invalidateAgentMsgId = 'FundInflightInvalidateJob2018Oct';
var invalidateAgentMsg = 'All cases containing H2O Allegro (SGD Hedged) fund which have not been approved by Supervisor are no longer valid for submission. Please re-generate a new Proposal with another fund.';
var messageGroup = 'invalidateMsg';
var messageGroupPriority = 2;

var addTrxLogs = function addTrxLogs(runTime, trxLogs, errLogs) {
  var nowString = moment().toDate().toISOString();
  var runTimeLong = runTime.getTime();
  var runTimeString = runTime.toISOString();
  return new Promise(function (resolve) {
    dao.getDoc(jobDetailsDocKey, function (doc) {
      if (doc && !doc.error) {
        var trxData = _.get(doc, 'log.' + runTimeLong);
        if (trxData) {
          var currData = _.cloneDeep(trxData);
          trxData.transaction = _.has(currData, 'transaction') ? _.concat([], currData.transaction, trxLogs) : trxLogs;
          if (!_.isEmpty(errLogs)) {
            trxData.error = _.has(currData, 'error') ? _.concat([], currData.error, errLogs) : errLogs;
          }
        } else {
          trxData = Object.assign({}, { transaction: trxLogs }, !_.isEmpty(errLogs) ? { error: errLogs } : {});
        }
        trxData.completeTime = nowString;
        trxData.runTime = runTimeString;

        doc.log[runTimeLong] = trxData;

        logger.log('Jobs :: Fund End:: InflightInvalidateJob :: update doc ' + jobDetailsDocKey + ' - runTime=' + runTimeString + ' completedTime=' + nowString);
        dao.updDoc(jobDetailsDocKey, doc, function (result) {
          doc._rev = result.rev;
          resolve(doc);
        });
      } else if (doc && doc.error === 'not_found') {
        var newDoc = {
          log: {}
        };

        newDoc.log[runTimeLong] = Object.assign({}, {
          runTime: runTimeString,
          completeTime: nowString,
          transaction: trxLogs
        }, !_.isEmpty(errLogs) ? { error: errLogs } : {});

        logger.log('Jobs :: Fund End:: InflightInvalidateJob :: new doc ' + jobDetailsDocKey + ' - runTime=' + runTimeString + ' completedTime=' + nowString);
        dao.updDoc(jobDetailsDocKey, newDoc, function (result) {
          newDoc._rev = result.rev;
          resolve(newDoc);
        });
      } else {
        logger.log('Jobs :: Fund End:: InflightInvalidateJob :: cannot find job details - runTime=' + runTimeString + ' completedTime=' + nowString);
        resolve(false);
      }
    });
  });
};

var createImpactedClientList = function createImpactedClientList(docName, cids) {
  return new Promise(function (resolve, reject) {
    try {
      dao.getDoc(docName, function (doc) {
        if (doc && !doc.error) {
          doc.lastUpdatedDate = new Date().toISOString();
          doc.impactedClientList = cids;

          logger.log('Jobs :: Fund End:: InflightInvalidateJob :: update doc ' + docName + ' - Create Impacted Client List');
          dao.updDoc(docName, doc, function (result) {
            doc._rev = result.rev;
            resolve(doc);
          });
        } else if (doc && doc.error === 'not_found') {
          var updatedDoc = {
            lastUpdatedDate: new Date().toISOString(),
            impactedClientList: cids
          };
          logger.log('Jobs :: Fund End:: InflightInvalidateJob :: update doc ' + docName + ' - Create Impacted Client List');
          dao.updDoc(docName, updatedDoc, function (result) {
            updatedDoc._rev = result.rev;
            resolve(updatedDoc);
          });
        } else {
          resolve(false);
        }
      });
    } catch (err) {
      logger.log('Error :: Jobs :: Fund End:: InflightInvalidateJob :: update doc ' + docName + ' - Create Impacted Client List');
      reject(err);
    }
  });
};

var checkIsJobExecutable = function checkIsJobExecutable(runTime, sysParam) {
  return new Promise(function (resolve, reject) {
    var fromTime = _.get(sysParam, 'fromTime');
    var toTime = _.get(sysParam, 'toTime');
    if (fromTime && toTime) {
      var runDateTime = moment(runTime);
      // The range of fromTime and toTime needs to include the scheduleCron Time
      var fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
      var toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
      //call API to check if this node server can perform Invalidation or not
      if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
        callApiByGet('/fairBiJobRunner', function (result) {
          resolve(_.get(result, 'success'));
        });
      } else {
        resolve(false);
      }
    } else {
      reject(new Error('Fail to get fromTime="' + fromTime + '" toTime="' + toTime + '" in ' + sysParameter));
    }
  });
};

var prepareProductList = function prepareProductList(sysParam) {
  var promises = [];
  _.forEach(_.get(sysParam, 'handleCovCode', {}), function (isHandle, covCode) {
    if (isHandle && _.get(rules, covCode + '.invalidateAllVersion')) {
      promises.push(Promise.resolve({
        covCode: covCode,
        productVersion: 99999999999999999999
      }));
    } else if (isHandle) {
      promises.push(prodDao.getPlanByCovCode('01', covCode, 'B').then(function (doc) {
        if (doc && !doc.error) {
          return doc;
        } else {
          throw new Error('Fail to get product for ' + covCode);
        }
      }));
    }
  });

  return Promise.all(promises).then(function (products) {
    var productsVersion = {};
    _.forEach(products, function (product) {
      if (product.covCode) {
        productsVersion[product.covCode] = _.get(product, 'productVersion', '1.0');
      }
      // productsVersion[product.covCode] = _.get(product, 'productVersion', '1.0');
    });
    return productsVersion;
  });
};

var prepareChannels = function prepareChannels() {
  return new Promise(function (resolve, reject) {
    agentDao.getChannels(function (channels) {
      if (channels) {
        resolve(channels);
      } else {
        reject(new Error('Fail to get channels'));
      }
    });
  });
};

var recursiveInvalidateCase = function recursiveInvalidateCase(cid, invalidateList) {
  var result = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  var index = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

  if (index !== invalidateList.length) {
    var docId = invalidateList[index];
    return bundleDao.onInvalidateApplicationById(cid, docId).then(function (iResult) {
      if (iResult && !iResult.error) {
        result[index] = docId;
        return recursiveInvalidateCase(cid, invalidateList, result, index + 1);
      } else {
        throw Error('Fail to invalidate case ' + docId);
      }
    });
  } else {
    return result;
  }
};

var invalidateCustomer = function invalidateCustomer(runTime, cid, sysParam, channels, productsVersion) {

  return bundleDao.getCurrentBundle(cid).then(function (bundle) {
    if (!bundle) {
      throw new Error('Bundle is null');
    }

    var agent = {
      agentCode: bundle.agentCode,
      channel: {
        code: bundle.dealerGroup
      },
      compCode: bundle.compCode
    };

    var isFaChannel = _.get(channels, 'channels.' + bundle.dealerGroup + '.type') === 'FA';

    var promises = [];
    //get data for processing case
    _.forEach(bundle.applications, function (app) {
      var isApplication = _.has(app, 'applicationDocId');
      var docId = '';
      if (isApplication && (_.get(app, 'appStatus') === 'APPLYING' || _.get(app, 'appStatus') === 'SUBMITTED')) {
        docId = _.get(app, 'applicationDocId', '');
      }
      if (!isApplication) {
        docId = _.get(app, 'quotationDocId', '');
      }

      if (docId.length > 0) {
        promises.push(new Promise(function (resolve, reject) {
          dao.getDoc(docId, function (doc) {
            if (doc && !doc.error) {
              resolve({ docId: docId, isApplication: isApplication, doc: doc });
            } else {
              reject(new Error('Fail to get doc ' + docId));
            }
          });
        }));
      }
    });

    return Promise.all(promises).then(function (results) {
      //check processing case

      var fullySignedCases = _.filter(bundle.applications, function (app) {
        //return app.isFullySigned && _.get(app, 'appStatus') === 'APPLYING';
        return app.isFullySigned;
      });
      var nonFullySignedCases = _.filter(bundle.applications, function (app) {
        return !app.isFullySigned && (!_.has(app, 'applicationDocId') && !_.has(app, 'appStatus') || _.get(app, 'appStatus') === 'APPLYING');
      });

      var checkCases = [];
      if (fullySignedCases.length > 0 || nonFullySignedCases.length > 0) {
        checkCases = _.concat([], fullySignedCases, nonFullySignedCases);
      }
      //else if (nonFullySignedCases.length > 0) {
      //no fully signed case
      //  checkCases = nonFullySignedCases;
      //}
      //let checkCases = bundle.applications || [];
      var handleCases = {
        isCreateNewBundle: false,
        invalidatedIdList: [],
        policyLists: []
      };

      if (checkCases.length > 0) {
        var needInvalidation = false;
        var hasFundAndFullySignedCase = false;

        _.forEach(checkCases, function (app) {
          logger.log(app);
          var isApplication = _.has(app, 'applicationDocId');
          var docId = isApplication ? _.get(app, 'applicationDocId', '') : _.get(app, 'quotationDocId', '');
          var data = _.find(results, function (result) {
            return result.docId === docId && result.isApplication === isApplication;
          });
          if (data) {
            var aFund = isApplication ? _.get(data.doc, 'quotation.fund') : _.get(data.doc, 'fund');
            var isFullySigned = _.get(data.doc, 'isFullySigned', false);
            var notYetSigned = false;
            var createDate = _.get(data.doc, 'createDate', false);
            if (_.get(app, 'isFnaReportSigned') === false && _.get(data.doc, 'isFullySigned') === false && _.get(data.doc, 'isApplicationSigned') === false && _.get(data.doc, 'isProposalSigned') === false && _.get(data.doc, 'isProposalSigned') === false || !_.has(app, 'applicationDocId') && !_.has(app, 'appStatus')) {
              notYetSigned = true;
            }
            if (aFund) {
              var funds = aFund.funds;
              // let createDateCompare = _.get(sysParam, 'createDateCompare');
              //   if (createDate < createDateCompare){
              for (var fund in funds) {
                if (funds[fund].fundCode == 'NHAF') {
                  needInvalidation = true;
                  break;
                }
              }
              // }
            }
          }
        });

        _.forEach(checkCases, function (app) {
          var isApplication = _.has(app, 'applicationDocId');
          var docId = isApplication ? _.get(app, 'applicationDocId', '') : _.get(app, 'quotationDocId', '');
          //let isFullySigned = _.get(app, 'isFullySigned', false);
          var data = _.find(results, function (result) {
            return result.docId === docId && result.isApplication === isApplication;
          });
          if (data) {
            var baseProductCode = isApplication ? _.get(data.doc, 'quotation.baseProductCode', '') : _.get(data.doc, 'baseProductCode', '');
            var isHandleCovCode = _.get(sysParam, 'handleCovCode.' + baseProductCode);
            //let paymentMethod = isApplication ? _.get(data.doc,'quotation.policyOptions.paymentMethod') : _.get(data.doc, 'policyOptions.paymentMethod');
            var policyNumber = isApplication ? _.get(data.doc, 'policyNumber', '') : null;
            //let plans = isApplication ? _.get(data.doc, 'quotation.plans') : _.get(data.doc, 'plans');
            var aFund = isApplication ? _.get(data.doc, 'quotation.fund') : _.get(data.doc, 'fund');
            var isFullySigned = _.get(data.doc, 'isFullySigned', false);
            var hasFund = false;

            if (aFund) {
              var funds = aFund.funds;
              for (var fund in funds) {
                if (funds[fund].fundCode == 'NHAF') {
                  hasFund = true;
                  break;
                }
              }
            }

            if (isHandleCovCode) {
              if (needInvalidation && !isFullySigned) {
                // not yet fully signed
                if (hasFund || fullySignedCases.length > 0) {
                  handleCases.invalidatedIdList.push(docId); //invalidated
                }
              } else if (needInvalidation && isFullySigned) {
                if (_.get(app, 'appStatus') === 'SUBMITTED') {
                  if (hasFund) {
                    handleCases.policyLists.push(policyNumber); // expired
                  }
                } else {
                  handleCases.invalidatedIdList.push(docId); //invalidated
                }
              }

              //if (bundle.applications.length > 0 && handleCases.invalidatedIdList.indexOf(docId) >= 0 && !isFaChannel) {
              //  handleCases.isCreateNewBundle = true;
              //}
            }
          } else {
            logger.log('INFO: Fund End :: InflightInvalidateJob - no doc is found for ' + docId);
          }
        });
      }
      return handleCases;
    }).then(function (handleCases) {
      // Expired The poclies
      var policyLists = handleCases.policyLists;

      var policyPromises = [];
      _.each(policyLists, function (docId) {
        policyPromises.push(new Promise(function (resolve, reject) {
          dao.getDoc(docId, function (foundCase) {
            resolve(foundCase);
          });
        }).then(function (appCase) {
          if (appCase && appCase.submittedDate && ['R', 'A', 'E'].indexOf(appCase.approvalStatus) === -1) {
            appCase.approvalStatus = 'E';
            appCase.expiredDate = new Date().toISOString();
            return new Promise(function (resolve) {
              dao.updDoc(docId, appCase, function (result) {
                if (result && !result.error) {
                  resolve(docId);
                } else {
                  resolve(false);
                }
              });
            });
          } else {
            return Promise.resolve();
          }
        }));
      });
      return Promise.all(policyPromises).then(function () {
        return handleCases;
      });
    }).then(function (handleCases) {
      if (!handleCases) {
        return null;
      }

      var isCreateNewBundle = handleCases.isCreateNewBundle,
          invalidatedIdList = handleCases.invalidatedIdList;


      if (isCreateNewBundle) {
        return bundleDao.createNewBundle(cid, agent, false).then(function (bundles) {
          return addTrxLogs(runTime, [{
            cid: cid,
            bundleId: _.get(bundle, 'id'),
            agentCode: bundle.agentCode,
            action: 'createNewBundle',
            newBundleId: _.get(_.find(bundles, function (b) {
              return b.isValid;
            }), 'id'),
            triggerByDocIds: invalidatedIdList
          }]);
        }).then(function (lResult) {
          return agent.agentCode;
        });
      } else if (invalidatedIdList.length > 0) {
        return recursiveInvalidateCase(cid, invalidatedIdList).then(function (docIds) {
          return addTrxLogs(runTime, [{
            cid: cid,
            bundleId: _.get(bundle, 'id'),
            agentCode: bundle.agentCode,
            action: 'invalidateApplicationById',
            triggerByDocIds: docIds
          }]);
        }).then(function (lResult) {
          if (!isFaChannel) {
            return bundleDao.rollbackApplication2Step1(cid);
          } else {
            return;
          }
        }).then(function () {
          return agent.agentCode;
        });
      }
      return null;
    }).then(function (agentCode) {
      if (agentCode) {
        return agentCode;
      } else {
        return addTrxLogs(runTime, [{
          cid: cid,
          bundleId: _.get(bundle, 'id'),
          agentCode: _.get(bundle, 'agentCode'),
          action: 'checked'
        }]).then(function (lResult) {
          return null;
        });
      }
    });
  }).catch(function (error) {
    logger.error('Jobs :: InflightInvalidateJob :: ERROR caught at ' + moment().toDate() + ' - ', cid, error);

    var errors = [];
    var errorStr = error instanceof Error ? error.toString() : _.toString(error);
    var errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + (typeof errorStr === 'undefined' ? 'undefined' : _typeof(errorStr));
    errors.push('[' + cid + '] - ' + errorLog + ' - at ' + moment().toISOString());
    return addTrxLogs(runTime, [{
      cid: cid,
      action: 'checked',
      error: errorLog
    }], errors).then(function (result) {
      return null;
    });
  });
};

var recursiveInvalidateCustomer = function recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents) {
  var handledAgentCodes = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : [];
  var index = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : 0;

  if (index !== cids.length) {
    logger.log('Jobs :: InflightInvalidateJob - [Fund End] :: EXECUTE Inflight Case Handling Process - START ' + (index + 1) + '/' + cids.length + ' (' + cids[index] + ')');
    return invalidateCustomer(runTime, cids[index], sysParam, channels, productsVersion).then(function (agentCode) {
      var willHandleNotification = handledAgentCodes.indexOf(agentCode) < 0 && agentCode;
      logger.log('Jobs :: InflightInvalidateJob - [Fund End] :: EXECUTE Inflight Case Handling Process - END ' + (index + 1) + '/' + cids.length + ' (' + cids[index] + ')' + (willHandleNotification ? ' handleNotificationAgentCode=' + agentCode : ''));

      if (willHandleNotification) {
        handledAgentCodes.push(agentCode);
        return _.find(agents, function (agent) {
          return agent.agentCode === agentCode;
        });
      } else {
        return;
      }
    }).then(function (agent) {
      if (agent) {
        return insertNoticeToAgent(agent.id, invalidateAgentMsgId, invalidateAgentMsg, messageGroup, messageGroupPriority).then(function (iResult) {
          if (iResult && !iResult.error) {
            return;
          } else {
            throw Error('Fail to insert notification to agent ' + agent.agentCode);
          }
        });
      } else {
        return;
      }
    }).then(function () {
      var timeout = _.get(sysParam, 'perCaseTimeout', 0);
      if (timeout) {
        async.waterfall([function (callback) {
          logger.log('Jobs :: InflightInvalidateJob - [Fund End] :: Set Timeout ');
          setTimeout(function () {
            logger.log('Jobs :: InflightInvalidateJob - [Fund End] :: Successfully callback in time out');
            callback(null, { success: true });
          }, timeout);
        }], function (err, result) {
          if (err) {
            logger.error('Jobs :: InflightInvalidateJob - [Fund End] :: Error in searchApprovalCaseByAppId: ', err);
            throw Error('Jobs :: InflightInvalidateJob :: Fail to setTimeout ' + cids[index]);
          } else {
            return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
          }
        });
      } else {
        return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
      }
    }).catch(function (error) {
      logger.error('Jobs :: InflightInvalidateJob - [Fund End] :: Error in recursiveInvalidateCustomer: ', error);
      return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
    });
  } else {
    logger.log('Jobs :: InflightInvalidateJob - [Fund End] :: COMPLETED Inflight Case Handling');
    return;
  }
};

var executeJob = function executeJob(runTime, sysParam, resolve, reject) {
  logger.log('Jobs :: Fund End:: InflightInvalidateJob :: JOB STARTED at ' + runTime);
  prepareChannels().then(function (channels) {
    return { channels: channels };
  }).then(function (param) {
    return getAllCustomerWithFundNHAF().then(function (cids) {
      return _.set(param, 'cids', cids);
    });
  }).then(function (param) {
    var docName = 'FundImpactedList_' + currentDate.getUTCFullYear() + '_' + (currentDate.getUTCMonth() + 1) + '_' + currentDate.getDate();
    return createImpactedClientList(docName, _.get(param, 'cids')).then(function () {
      return param;
    });
  }).then(function (param) {
    logger.log('Jobs :: Fund End:: InflightInvalidateJob :: GetAllAgents :: JOB ENDED at ' + moment().toDate());
    return getAllAgents().then(function (agents) {
      return _.set(param, 'agents', agents);
    });
  }).then(function (param) {
    return recursiveInvalidateCustomer(runTime, sysParam, param.channels, param.productsVersion, param.cids, param.agents);
  }).then(function () {
    logger.log('Jobs :: Fund End:: InflightInvalidateJob :: JOB ENDED at ' + moment().toDate());
    resolve();
  }).catch(function (error) {
    logger.error('Jobs :: InflightInvalidateJob :: ERROR caught at FINAL ' + moment().toDate(), error);

    var errors = [];
    var errorStr = error instanceof Error ? error.toString() : _.toString(error);
    var errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + (typeof errorStr === 'undefined' ? 'undefined' : _typeof(errorStr));
    errors.push(errorLog);
    addTrxLogs(runTime, [], errors);
    reject();
  });
};

module.exports.execute = function () {
  return new Promise(function (resolve, reject) {
    var param = sysParameterJson;

    if (param && !param.error) {
      var fairBISysParam = _.get(param, 'GenericFundflightCaseHandling');
      var scheduleCron = _.get(fairBISysParam, 'scheduleCron');
      var fromTime = _.get(fairBISysParam, 'fromTime');
      var toTime = _.get(fairBISysParam, 'toTime');

      if (fairBISysParam && scheduleCron) {
        executeJob(new Date(), fairBISysParam, resolve, reject);
      } else {
        reject(new Error('Fail to get scheduleCron="' + scheduleCron + '" in ' + sysParameter));
      }
    } else {
      reject(new Error('Fail to get doc ' + sysParameter));
    }
  });
};