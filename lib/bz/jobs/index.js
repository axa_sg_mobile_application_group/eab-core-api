'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var generalEndFundInvalidate = require('./generalEndFundInvalidate');

var jobs = exports.jobs = {
  generalEndFundInvalidate: generalEndFundInvalidate
};