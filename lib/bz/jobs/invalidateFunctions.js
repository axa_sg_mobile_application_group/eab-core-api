'use strict';

var aDao = require('../cbDao/agent');
var dao = require('../cbDaoFactory').create();
var _ = require('lodash');
var logger = global.logger || console;

var _require = require('../utils/RemoteUtils'),
    callApiByGet = _require.callApiByGet;

var moment = require('moment');

module.exports.getAllCustomer = function () {
  return new Promise(function (resolve) {
    dao.getViewRange('main', 'contacts', '', '', null, function () {
      var pList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      console.log("1. [getAllCustomer]-[main]-[contacts]-pList");
      console.log(pList);
      resolve(_.compact(_.map(pList.rows, 'id')));
    });
  });
};

module.exports.getAllCustomerWithFundNHAF = function () {
  return new Promise(function (resolve) {
    dao.getViewRange('invalidateViews', 'quotationsByNHAFFund', '', '', ['stale=false'], function () {
      var pList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      console.log("2. [getImpactedClientIds]-[invalidateViews]-[quotationsByNHAFFund]-pList");
      console.log(pList);
      var result = [];
      _.each(pList && pList.rows, function (row) {
        if (row && row.value && row.value.cids) {
          result = _.union(result, row.value.cids);
        }
      });
      resolve(result);
    });
  });
};

module.exports.getAllValidBundleId = function () {
  return new Promise(function (resolve) {
    dao.getViewRange('invalidateViews', 'validBundleInClient', '', '', null, function () {
      var bundles = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      console.log("[getImpactedClientIds]-[validBundleInClient]-result");
      console.log(bundles);
      resolve(_.compact(_.map(bundles.rows, function (row) {
        return row && row.value && row.value.validBundleId;
      })));
    });
  });
};

module.exports.getImpactedClientIds = function (bundleIds, handleCovCode) {
  var compCode = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '01';

  var promises = [];
  _.each(handleCovCode, function (isHandle, covCode) {
    if (isHandle) {
      promises.push(new Promise(function (resolve) {
        var keys = _.map([covCode], function (code) {
          return '["' + compCode + '","' + code + '"]';
        });
        dao.getViewByKeys('invalidateViews', 'quotationsByBaseProductCode', keys, null).then(function (result) {
          console.log("[getImpactedClientIds]-[invalidateViews]-result");
          console.log(result);
          var clientIds = [];
          if (result) {
            _.each(result.rows, function (row) {
              var quotation = row.value || {};
              if (quotation.clientId && quotation.bundleId && bundleIds.indexOf(quotation.bundleId) > -1) {
                clientIds = _.union(clientIds, [quotation.clientId]);
              }
            });
          }
          resolve(clientIds);
        });
      }));
    }
  });

  return Promise.all(promises).then(function (clientIdsArray) {
    var results = [];
    _.forEach(clientIdsArray, function (clientIds) {
      results = _.union(results, clientIds);
    });
    return results;
  });
};

module.exports.getAllAgents = function () {
  var impactAgents = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

  return new Promise(function (resolve) {
    dao.getViewRange('main', 'agents', '', '', null, function () {
      var pList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      console.log("[getAllAgents]-[main]-pList");
      console.log(pList);
      resolve(_.map(pList.rows, function (data) {
        return {
          id: _.get(data, 'value.profileId'),
          agentCode: _.get(data, 'value.agentCode')
        };
      }));
    });
  });
};

module.exports.getAllAgentRelatedDocumentIds = function (compCode, agentCodes) {
  return new Promise(function (resolve) {
    var keys = _.map([agentCodes], function (agentCode) {
      return '["' + compCode + '","' + agentCode + '"]';
    });
    dao.getViewByKeys('dataSync', 'agentDocuments', keys, null).then(function () {
      var pList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      console.log("[getAllAgentRelatedDocumentIds]-[dataSync]-pList");
      console.log(pList);
      resolve(_.map(pList.rows, function (data) {
        return data && data.id;
      }));
    });
  });
};

module.exports.getFilterAgent = function () {
  var impactAgents = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

  return new Promise(function (resolve) {
    dao.getViewRange('main', 'agents', '', '', null, function () {
      var pList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      console.log("[getFilterAgent]-[main]-pList");
      console.log(pList);
      var result = _.filter(pList.rows, function (row) {
        return impactAgents.indexOf(row.key[1]) > -1;
      });
      resolve(_.map(result, 'value.profileId'));
    });
  });
};

module.exports.insertNoticeToAgent = function (agentId, id, message, messageGroup, groupPriority) {
  return new Promise(function (resolve) {
    dao.getDoc("inflightJobList", function (doc) {
      var newMessages = _.cloneDeep(doc.messages);
      newMessages.push(message);

      dao.updDoc("inflightJobList", Object.assign({}, doc, { messages: newMessages }), resolve);
    });
  }).catch(function (error) {
    logger.error('Jobs :: InflightInvalidateJob :: ERROR caught at insertNoticeToAgent ' + error);
    throw Error('Fail to insert notification to agent ' + agentId);
  });
};

module.exports.checkIsJobExecutable = function (runTime, sysParam) {
  return new Promise(function (resolve, reject) {
    var fromTime = _.get(sysParam, 'fromTime');
    var toTime = _.get(sysParam, 'toTime');
    if (fromTime && toTime) {
      var runDateTime = moment(runTime);
      // The range of fromTime and toTime needs to include the scheduleCron Time
      var fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
      var toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
      //call API to check if this node server can perform Invalidation or not
      if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
        callApiByGet('/fairBiJobRunner', function (result) {
          resolve(_.get(result, 'success'));
        });
      } else {
        resolve(false);
      }
    } else {
      reject(new Error('Fail to get fromTime="' + fromTime + '" toTime="' + toTime + '" in ' + sysParam));
    }
  });
};

module.exports.addTrxLogs = function (runTime, trxLogs, errLogs, jobDetailsDocKey) {
  var nowString = moment().toDate().toISOString();
  var runTimeLong = runTime.getTime();
  var runTimeString = runTime.toISOString();
  return new Promise(function (resolve) {
    dao.getDoc(jobDetailsDocKey, function (doc) {
      if (doc && !doc.error) {
        var trxData = _.get(doc, 'log.' + runTimeLong);
        if (trxData) {
          var currData = _.cloneDeep(trxData);
          trxData.transaction = _.has(currData, 'transaction') ? _.concat([], currData.transaction, trxLogs) : trxLogs;
          if (!_.isEmpty(errLogs)) {
            trxData.error = _.has(currData, 'error') ? _.concat([], currData.error, errLogs) : errLogs;
          }
        } else {
          trxData = Object.assign({}, {
            transaction: trxLogs
          }, !_.isEmpty(errLogs) ? {
            error: errLogs
          } : {});
        }
        trxData.completeTime = nowString;
        trxData.runTime = runTimeString;

        doc.log[runTimeLong] = trxData;

        logger.log('Jobs :: DataPatchJob_DataSync :: update doc ' + jobDetailsDocKey + ' - runTime=' + runTimeString + ' completedTime=' + nowString);
        dao.updDoc(jobDetailsDocKey, doc, function (result) {
          doc._rev = result.rev;
          resolve(doc);
        });
      } else if (doc && doc.error === 'not_found') {
        var newDoc = {
          log: {}
        };

        newDoc.log[runTimeLong] = Object.assign({}, {
          runTime: runTimeString,
          completeTime: nowString,
          transaction: trxLogs
        }, !_.isEmpty(errLogs) ? {
          error: errLogs
        } : {});

        logger.log('Jobs :: DataPatchJob_DataSync :: new doc ' + jobDetailsDocKey + ' - runTime=' + runTimeString + ' completedTime=' + nowString);
        dao.updDoc(jobDetailsDocKey, newDoc, function (result) {
          newDoc._rev = result.rev;
          resolve(newDoc);
        });
      } else {
        logger.log('Jobs :: DataPatchJob_DataSync :: cannot find job details - runTime=' + runTimeString + ' completedTime=' + nowString);
        resolve(false);
      }
    });
  });
};