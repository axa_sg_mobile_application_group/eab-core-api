"use strict";

var ACTION_CONTINUE = 1;
var ROLL_BACK = 2;

var EmailException = function EmailException(message) {
  this.message = message;
  this.code = ACTION_CONTINUE;
};

var UpdateDocException = function UpdateDocException(message) {
  this.message = message;
  this.code = ROLL_BACK;
};

var CreateDocException = function CreateDocException(message) {
  this.message = message;
  this.code = ROLL_BACK;
};

var GetTemplateException = function GetTemplateException(message) {
  this.message = message;
  this.code = ROLL_BACK;
};

var UnknownException = function UnknownException(message) {
  this.message = message;
  this.code = ROLL_BACK;
};

EmailException.prototype = Error.prototype;
CreateDocException.prototype = Error.prototype;
GetTemplateException.prototype = Error.prototype;
UpdateDocException.prototype = Error.prototype;
UnknownException.prototype = Error.prototype;

module.exports = { EmailException: EmailException, CreateDocException: CreateDocException, GetTemplateException: GetTemplateException, UpdateDocException: UpdateDocException, UnknownException: UnknownException, ACTION_CONTINUE: ACTION_CONTINUE, ROLL_BACK: ROLL_BACK };