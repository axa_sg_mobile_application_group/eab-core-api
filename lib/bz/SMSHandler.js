'use strict';

var _require = require('./utils/RemoteUtils.js'),
    callApi = _require.callApi;

module.exports.sendSMS = function (data, session, cb) {
  if (global.config.disableSMS) {
    cb({});
  } else {
    callApi('/sms/sendSMS', data.sms, cb);
  }
};