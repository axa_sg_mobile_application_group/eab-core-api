"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Java = require("java");
var CryptoJS = require("crypto-js");
// var root = fs.realpathSync('.');
var path = require('path');
var fs = require('fs');

var JavaRunner = function () {
  function JavaRunner() {
    _classCallCheck(this, JavaRunner);

    var root = global.rootPath;
    // this.config = {
    //   dbuser: global.config.db.user,
    //   dbpwd: global.config.db.password,
    //   jdbc_url: global.config.db.jdbc_url
    // };
    Java.options.push("-Xss2m");
    Java.options.push('-Djava.awt.headless=true'); // necessary for mac to merge pdf, DO NOT remove
    // Java.options.push('-agentlib:jdwp=transport=dt_socket,address=8787,server=y,suspend=n');

    // load all java libraries
    Java.classpath.push(path.join(root, "java", "NodeJSModule.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "commons-codec-1.9.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "commons-logging-1.2.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "couchbase-lite-java-1.2.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "couchbase-lite-java-core-1.2.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "couchbase-lite-java-forestdb-1.2.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "couchbase-lite-java-sqlcipher-1.2.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "couchbase-lite-java-sqlite-custom-1.2.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "gson-2.6.2.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "httpclient-4.5.2.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "httpcore-4.4.4.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "jackson-annotations-2.5.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "jackson-core-2.5.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "jackson-databind-2.5.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "json-20160212.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "ojdbc7.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "stateless4j-2.4.0.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "pdfbox-app-2.0.8.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "pdfreactor.jar"));
    Java.classpath.push(path.join(root, "java", "lib", "xslt.jar"));

    this.java = Java;
  }

  _createClass(JavaRunner, [{
    key: "addWordsPdfs",
    value: function addWordsPdfs(base64Pdf, agentName, proposerName, x1, y1, x2, y2) {
      return this.java.callStaticMethodSync("pdf.Pdf", "addNames", base64Pdf, agentName, proposerName, x1, y1, x2, y2);
    }
  }, {
    key: "setPdfPassword",
    value: function setPdfPassword(base64Pdf, pass) {
      return this.java.callStaticMethodSync("pdf.Pdf", "setPassword", base64Pdf, pass);
    }
  }, {
    key: "mergePdfs",
    value: function mergePdfs(base64Pdfs, callback) {
      callback(this.java.callStaticMethodSync("pdf.Pdf", "merge", base64Pdfs));
    }
  }, {
    key: "convertHtml2Pdf",
    value: function convertHtml2Pdf(html, pdfOptions) {
      return this.java.callStaticMethodSync("pdf.Pdf", "convertHtml2Pdf", html, pdfOptions);
    }
  }, {
    key: "addPdfTitle",
    value: function addPdfTitle(base64Pdf, title) {
      return this.java.callStaticMethodSync('pdf.Pdf', 'addTitle', base64Pdf, title);
    }
  }, {
    key: "convertBase64Imgs2Pdf",
    value: function convertBase64Imgs2Pdf(base64Imgs) {
      return this.java.callStaticMethodSync('pdf.Pdf', 'convertBase64Images2Pdf', base64Imgs);
    }
  }, {
    key: "compareEncryptPassword",
    value: function compareEncryptPassword(plainPass, userName, privateKey, encryptedPass) {
      privateKey = userName + privateKey;
      var keyHex = CryptoJS.enc.Utf8.parse(privateKey);
      var encrypted_P1 = CryptoJS.DES.encrypt(plainPass, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
      });
      return encrypted_P1 == encryptedPass;
    }
  }, {
    key: "createAesKey",
    value: function createAesKey() {

      var aeskey = this.java.callStaticMethodSync("com.eab.Util", "GenerateRandomASCII", 32);
      return aeskey;
    }
  }, {
    key: "createKeyStore",
    value: function createKeyStore(ccb) {
      //var keyfile = require('eab');
      var keyfile = __dirname + '/keystore/eab.keystore';
      //var keyfile =  'eab.keystore';

      var keyConfig = require('./keystore/keyConfig.js').keyConfig;
      var ksstatus = this.java.callStaticMethodSync("com.eab.Keystore", "Create", keyfile, keyConfig.keystore_pass);

      ccb();
    }
  }, {
    key: "saveTripleDESToKeyStore",
    value: function saveTripleDESToKeyStore(tripledeskey, cb) {

      var keyfile = __dirname + '/keystore/eab.keystore';
      var keyConfig = require('./keystore/keyConfig.js').keyConfig;
      var keystore_pass = keyConfig.keystore_pass;
      var keyalias_name = keyConfig.keyalias_name;
      var keyalias_password = keyConfig.keyalias_password;
      var savestatus = this.java.callStaticMethodSync("com.eab.Keystore", "ImportPassphrase", keyfile, keystore_pass, keyalias_name, tripledeskey, keyalias_password);

      cb(savestatus);
    }
  }, {
    key: "getTripleDESFromKeyStore",
    value: function getTripleDESFromKeyStore() {
      var keyfile = __dirname + '/keystore/eab.keystore';
      var keyConfig = require('./keystore/keyConfig.js').keyConfig;
      var keystore_pass = keyConfig.keystore_pass;
      var keyalias_name = keyConfig.keyalias_name;
      var keyalias_password = keyConfig.keyalias_password;
      var key = this.java.callStaticMethodSync("com.eab.Keystore", "ExportPasspharase", keyfile, keystore_pass, keyalias_name, keyalias_password);
      return key;
    }

    //gen new tripleDES key

  }, {
    key: "createTripleDesKey",
    value: function createTripleDesKey() {
      var tripledeskey = this.java.callStaticMethodSync("com.eab.Util", "GenerateRandomASCII", 21); // 24 chars = 192-bit
      return tripledeskey;
    }

    // AES Encryption - TripleDES Key to be Encrypted String

  }, {
    key: "enrtyptAesKey",
    value: function enrtyptAesKey(tripledeskey, aes256key) {
      var enkey = CryptoJS.AES.encrypt(tripledeskey, aes256key);
      return enkey;
    }

    // using this

  }, {
    key: "jsonToXml",
    value: function jsonToXml(json, callback) {
      this.java.callStaticMethod("com.eab.xslt.XSLTTransform", "jsonToXml", json, function (err, result) {
        if (err) callback(false);else callback(result);
      });
    }

    // using this

  }, {
    key: "XSLTTransformer",
    value: function XSLTTransformer(xml, xsl, callback) {
      this.java.callStaticMethod("com.eab.xslt.XSLTTransform", "transformXml", xml, xsl, function (err, result) {
        callback(result);
      });
    }
  }, {
    key: "XSLTTest",
    value: function XSLTTest(callback) {
      this.java.callStaticMethod("com.eab.xslt.XSLTTransform", "test", function (err, result) {
        callback(result);
      });
    }

    // exeSql(sql, data, callback) {
    //   if (!callback && typeof data == 'function') {
    //     callback = data;
    //   }

    //   this.java.callStaticMethod("com.eab.database.JDBC", "execute", this.config.dbuser, this.config.dbpwd, this.config.jdbc_url, sql, function(err, results) {
    //     if (callback) {
    //       callback(err, results)
    //     }
    //   });
    // }

  }, {
    key: "initCBL",
    value: function initCBL(callback) {
      this.java.callStaticMethod("com.eab.database.CBLite", "main", [], function (err, result) {
        if (err) {
          callback(false);return;
        }
        if (callback) {
          callback(err, result);
        }
      });
    }
  }, {
    key: "createView",
    value: function createView(ddname, view, callback) {
      this.java.callStaticMethod("com.eab.database.CBLite", "CreateView", ddname, view, function (err, doc) {
        if (callback) {
          callback(err, JSON.parse(doc));
        }
      });
    }
  }, {
    key: "getViewRange",
    value: function getViewRange(ddname, name, start, end, params, callback) {
      this.java.callStaticMethod("com.eab.database.CBLite", "GetView", ddname, name, start, end, params, function (err, doc) {
        if (callback) {
          callback(err, JSON.parse(doc));
        }
      });
    }
  }, {
    key: "getDoc",
    value: function getDoc(docId, callback) {
      //Get document
      this.java.callStaticMethod("com.eab.database.CBLite", "GetDocument", docId, function (err, doc) {
        if (err) {
          callback(false);
          return;
        }
        callback(err, JSON.parse(doc));
      });
    }
  }, {
    key: "getAttachment",
    value: function getAttachment(docId, attName, callback) {
      //Get attachment
      this.java.callStaticMethod("com.eab.database.CBLite", "GetAttach", docId, attName, function (err, binary) {
        if (err || !binary) {
          callback(false);return;
        }
        callback(err, binary);
      });
    }
  }, {
    key: "addDoc",
    value: function addDoc(docId, data, callback) {
      //Create new document
      this.java.callStaticMethod("com.eab.database.CBLite", "CreateDocument", docId, data, function (err, newRevId) {
        if (err || !newRevId) {
          callback(false);
          return;
        }
        callback(true);
      });
    }
  }, {
    key: "updDoc",
    value: function updDoc(docId, data, callback) {
      //Create new document
      this.java.callStaticMethod("com.eab.database.CBLite", "UpdateDocument", docId, data, function (err, newRevId) {
        if (err || !newRevId) {
          callback(false);
          return;
        }
        callback(true);
      });
    }
  }, {
    key: "delDoc",
    value: function delDoc(docId, callback) {
      //Create new document
      this.java.callStaticMethod("com.eab.database.CBLite", "DeleteDocument", docId, function (err, status) {
        if (err || !status) {
          callback(false);
          return;
        }
        callback(status);
      });
    }
  }, {
    key: "setAttachment",
    value: function setAttachment(docId, name, mime, data, callback) {
      //Update attachment to document
      this.java.callStaticMethod("com.eab.database.CBLite", "UpdateAttach", docId, name, mime, data, function (err, newRevId) {
        if (err || !newRevId) {
          callback(false);return;
        }
        callback(true);
      });
    }
  }, {
    key: "delAttachment",
    value: function delAttachment(docId, name, callback) {
      //Update attachment to document
      this.java.callStaticMethod("com.eab.database.CBLite", "DeleteAttach", docId, name, function (err, newRevId) {
        if (err || !newRevId) {
          callback(false);return;
        }
        callback(true);
      });
    }
  }]);

  return JavaRunner;
}();

exports["default"] = JavaRunner;
module.exports = exports["default"];