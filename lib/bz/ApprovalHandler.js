"use strict";

var cDao = require("./cbDao/approval");
var agentDao = require("./cbDao/agent");
var bDao = require("./cbDao/bundle");
var applicationDao = require('./cbDao/application');
var dao = require('./cbDaoFactory').create();
var _ = require('lodash');
var forEach = require('lodash/forEach');
var cloneDeep = require('lodash/cloneDeep');
var indexOf = require('lodash/indexOf');
var filter = require('lodash/filter');
var trim = require('lodash/trim');
var split = require('lodash/split');
var toUpper = require('lodash/toUpper');
var get = require('lodash/get');
var concat = require('lodash/concat');

var _require = require('../common/DateUtils'),
    formatDate = _require.formatDate,
    parseDatetime = _require.parseDatetime,
    dayDiff = _require.dayDiff,
    truncDate = _require.truncDate;

var PDFHandler = require('./PDFHandler');
var clone = require('lodash/clone');
var cloneDeep = require('lodash/cloneDeep');
var logger = global.logger || console;
var moment = require('moment');
var _isArray = require('lodash/fp/isArray');
var _concat = require('lodash/fp/concat');

var async = require('async');
var _get = require('lodash/fp/get');
var _getOr = require('lodash/fp/getOr');
var _isEmpty = require('lodash/fp/isEmpty');
var _join = require('lodash/fp/join');

var approvalStatusModel = require('./model/approvalStatus');
var aNotifyHandler = require('./ApprovalNotificationHandler');

var FILTER_DATE_FORMAT = 'YYYY-MM-DD';

var ConfigConstant = require('../app/constants/ConfigConstants');

var _require2 = require('./utils/TokenUtils'),
    createPdfToken = _require2.createPdfToken,
    setPdfTokensToRedis = _require2.setPdfTokensToRedis;

var appHandler = require('./handler/ApplicationHandler');
var fileHandler = require('./FileHandler');

module.exports.searchApprovalCaseById = function (data, session, cb) {
    cDao.searchApprovalCaseById(data.id, function (c) {
        cb(c);
    });
};

module.exports.searchApprovalCaseByAppId = function (data, session, cb) {
    // const appId = _get('appId', data);
    var appId = data.appId;
    async.waterfall([function (callback) {
        applicationDao.getApplication(appId, function (app) {
            callback(null, _get('id', app));
        });
    }, function (policyNumber, callback) {
        cDao.searchApprovalCaseById(policyNumber, function (searchResult) {
            callback(null, _get('foundCase', searchResult));
        });
    }], function (err, approvalCase) {
        if (err) {
            logger.error("Error in searchApprovalCaseByAppId: ", err);
            cb({ success: false });
        } else {
            cb({
                success: true,
                approvalCase: approvalCase
            });
        }
    });
};

module.exports.geteApprovalByAppid = function (data, session, cb) {
    cDao.geteApprovalByAppid(data.id, function (c) {
        cb(c);
    });
};

var populateToChildrenJson = function populateToChildrenJson(subApprovalList, approvalCase) {
    logger.log('ApprovalHandler populateToChildrenJson starts');
    return new Promise(function (resolve) {
        var promises = [];
        var success = true;
        var copyMasterCase = _.cloneDeep(approvalCase);
        var fieldsToOmit = ['_attachments', '_id', '_rev', 'applicationId', 'approvalCaseId', 'policiesMapping', 'subApprovalList', 'type'];

        copyMasterCase = _.omit(copyMasterCase, fieldsToOmit);

        _.forEach(subApprovalList, function (subApprovalId) {
            promises.push(new Promise(function (resolve2) {
                dao.getDoc(subApprovalId, function (subApproval) {
                    cDao.updateApprovalCaseById(subApprovalId, _.assign(subApproval, copyMasterCase), function (c) {
                        if (!c.success) {
                            success = false;
                        }
                        resolve2();
                    });
                });
            }));
        });
        Promise.all(promises).then(function () {
            logger.log('ApprovalHandler populateToChildrenJson end with success -- ' + success);
            resolve();
        }).catch(function (error) {
            logger.error('Error in ApprovalHandler populateToChildrenJson: ', error);
        });
    });
};

var _updateApprovalCaseById = function _updateApprovalCaseById(data, session, cb) {
    var curTime = new Date();
    data.approvalCase.lastEditedDate = curTime.toISOString();
    data.approvalCase.lastEditedBy = session.agent.agentCode;

    dao.getDoc(data.id, function (approvalDoc) {
        if (_.get(approvalDoc, 'type') === 'masterApproval') {
            populateToChildrenJson(approvalDoc.subApprovalList, data.approvalCase).then(function () {
                cDao.updateApprovalCaseById(data.id, data.approvalCase, function (c) {
                    cb(c);
                });
            });
        } else {
            cDao.updateApprovalCaseById(data.id, data.approvalCase, function (c) {
                cb(c);
            });
        }
    });
};
module.exports.updateApprovalCaseById = _updateApprovalCaseById;

var _uploadAttachments = function _uploadAttachments(data, session, cb) {
    var uploadArr = data.attachments;
    uploadAtt(0, uploadArr, data, data.rev, cb);
};

module.exports.uploadAttachments = _uploadAttachments;

var uploadAtt = function uploadAtt(index, uploadArr, data, rev, cb) {
    if (uploadArr.length > index) {
        logger.log('APPROVAL:: Upload Approval Case Attachment');
        new Promise(function (resolve, reject) {
            if (rev === undefined) {
                cDao.searchApprovalCaseById(data.id, function (c) {
                    resolve(c.foundCase._rev);
                });
            } else {
                resolve(rev);
            }
        }).then(function (searchRev) {
            var base64Str = uploadArr[index].imageUrl.split(',')[1];
            cDao.uploadAttachment(data.id, 'eapproval_' + index, searchRev, base64Str, data.attachmentsType[index], function (rev) {
                uploadAtt(index + 1, uploadArr, data, rev, cb);
            });
        }).catch(function (error) {
            logger.error("Error in uploadAtt->new Promise: ", error);
        });
    } else {
        cDao.searchApprovalCaseById(data.id, function (c) {
            cb(c);
        });
    }
};

var _getAllPolicyIdsFromMasterApproval = function _getAllPolicyIdsFromMasterApproval(iCidMapping) {
    var subApprovalList = [];
    _.each(iCidMapping, function (cidArray) {
        _.each(cidArray, function (policyObj) {
            if (policyObj && policyObj.policyNumber) {
                subApprovalList.push(policyObj.policyNumber);
            }
        });
    });
    return subApprovalList;
};

var _getMasterApprovalIdFromMasterApplicationId = function _getMasterApprovalIdFromMasterApplicationId(appplicationId) {
    if (appplicationId) {
        return "SP" + appplicationId.substring(2, 14);
    } else {
        return '';
    }
};

module.exports.getMasterApprovalIdFromMasterApplicationId = _getMasterApprovalIdFromMasterApplicationId;

var _uploadAttachment = function _uploadAttachment(data, session, cb) {
    new Promise(function (resolve) {
        cDao.searchApprovalCaseById(data.id, function (c) {
            resolve(c.foundCase._rev);
        });
    }).then(function (searchRev) {
        dao.uploadAttachmentByBase64(data.id, data.fileId, searchRev, data.base64Str, 'application/pdf', function (res) {
            cb(res);
        });
    }).catch(function (error) {
        logger.error('Error in _uploadAttachment->new Promise: ', error);
    });
};
module.exports.uploadAttachment = _uploadAttachment;

module.exports.createApprovalCase = function (data, session, cb) {
    /**
     *
     *  ids: [application.id],
     *  newApprovalCase
     */
    var newApprovalCase = {};
    var isShieldMasterApproval = _getOr(false, 'isShieldMaster', data);
    var isShield = _getOr(false, 'isShield', data);
    var currentTime = _get('currentTime', data);
    return new Promise(function (resolve, reject) {
        cDao.getAllDoc(data.ids, function (results) {
            resolve([results.result[0], newApprovalCase]);
        });
    }).then(function (result) {
        return agentDao.getManagerDirectorProfileByAgentCode(session.agent.compCode, session.agent.agentCode).then(function (profiles) {
            if (!_.isEmpty(profiles)) {
                var agent = _.get(profiles, 'agentProfile') || {};
                var manager = _.get(profiles, 'managerProfile') || {};
                var director = _.get(profiles, 'directorProfile') || {};
                result.push({
                    agent: agent,
                    manager: manager,
                    director: director
                });

                return result;
            } else {
                throw Error('CANNOT get agnet / manager / director in createApprovalCase');
            }
        });
    }).then(function (result) {
        var today = void 0;
        today = currentTime || moment().toISOString();
        var markInfoStatus = ['A', 'PFAFA'];
        var application = result[0];
        var newApprovalCase = result[1];
        var agentProfiles = result[2];
        var aStatus = handleDirectorSubmitCase(session.agent, get(agentProfiles, 'agent.agentCode'), get(agentProfiles, 'director.agentCode'));

        newApprovalCase.agentId = get(agentProfiles, 'agent.agentCode');
        newApprovalCase.agentCode = get(session, 'agent.agentCode');
        newApprovalCase.compCode = get(session, 'agent.compCode');
        newApprovalCase.dealerGroup = get(session, 'agent.channel.code');
        newApprovalCase.agentProfileId = get(agentProfiles, 'agent.profileId');
        newApprovalCase.applicationId = application.id;

        if (isShieldMasterApproval) {
            newApprovalCase.approvalCaseId = _getMasterApprovalIdFromMasterApplicationId(application.id);
            newApprovalCase.subApprovalList = _getAllPolicyIdsFromMasterApproval(_.get(application, 'iCidMapping'));
            newApprovalCase.proposalNumber = newApprovalCase.subApprovalList.join(', ');
            newApprovalCase.isShield = true;
            newApprovalCase.type = 'masterApproval';
            newApprovalCase.policiesMapping = _.get(application, 'payment.premiumDetails');
            var pickedFields = ['covName', 'policyNumber', 'laName'];
            newApprovalCase.policiesMapping = _.map(newApprovalCase.policiesMapping, function (policyMappingObj) {
                return _.pick(policyMappingObj, pickedFields);
            });
        } else {
            newApprovalCase.approvalCaseId = application.policyNumber;
            newApprovalCase.proposalNumber = application.policyNumber;
            newApprovalCase.policyId = application.policyNumber;
            newApprovalCase.isShield = isShield;
            var parentId = _getOr(undefined, 'parentId', application);
            newApprovalCase.masterApprovalId = parentId ? _getMasterApprovalIdFromMasterApplicationId(parentId) : parentId;
            newApprovalCase.type = 'approval';
        }

        newApprovalCase.approveRejectDate = '';
        newApprovalCase.comment = [];
        newApprovalCase.compCode = get(agentProfiles, 'agent.compCode'); //TODO add to view
        newApprovalCase.onHoldReason = '';

        newApprovalCase.orginalManagerId = get(agentProfiles, 'agent.managerCode');

        newApprovalCase.customerId = get(application, 'applicationForm.values.proposer.personalInfo.cid');
        newApprovalCase.customerName = get(application, 'applicationForm.values.proposer.personalInfo.fullName');
        newApprovalCase.customerICNo = get(application, 'applicationForm.values.proposer.personalInfo.idCardNo');
        newApprovalCase.customerICType = get(application, 'applicationForm.values.proposer.personalInfo.idDocType');
        newApprovalCase.approvalStatus = aStatus;

        newApprovalCase.agentId = get(agentProfiles, 'agent.agentCode');
        newApprovalCase.agentName = get(agentProfiles, 'agent.name');
        newApprovalCase.agentEmail = get(agentProfiles, 'agent.email');
        newApprovalCase.agentMobile = get(agentProfiles, 'agent.mobile');

        newApprovalCase.directorId = get(agentProfiles, 'director.agentCode');
        newApprovalCase.directorName = get(agentProfiles, 'director.name');
        newApprovalCase.directorEmail = get(agentProfiles, 'director.email');
        newApprovalCase.directorMobile = get(agentProfiles, 'director.mobile');

        newApprovalCase.managerId = get(agentProfiles, 'manager.agentCode');
        newApprovalCase.managerName = get(agentProfiles, 'manager.name');
        newApprovalCase.managerEmail = get(agentProfiles, 'manager.email');
        newApprovalCase.managerMobile = get(agentProfiles, 'manager.mobile');
        if (markInfoStatus.indexOf(aStatus) > -1) {
            newApprovalCase.approveRejectDate = today;
            newApprovalCase.approveRejectManagerId = session.agent.agentCode;
            newApprovalCase.approveRejectManagerName = session.agent.name;
            newApprovalCase.approveRejectManagerEmail = session.agent.email;
            newApprovalCase.approveRejectManagerMobile = session.agent.mobile;
        }

        if (session.agent.channel.type === 'FA') {
            newApprovalCase.faFirmName = session.agent.company;
            newApprovalCase.isFACase = true;
        } else {
            newApprovalCase.organizationName = get(agentProfiles, 'agent.company'); //TODO add to view
            newApprovalCase.isFACase = false;
        }
        newApprovalCase.lastEditedDate = today;
        newApprovalCase.lastEditedBy = get(agentProfiles, 'agent.id');
        newApprovalCase.lifeAssuredName = get(application, 'quotation.iFullName');
        if (_.get(application, 'quotation.quotType') === 'SHIELD') {
            var shieldProductName = void 0;
            shieldProductName = get(application, 'quotation.plans[0].covName.en') || get(application, 'quotation.baseProductName.en');
            newApprovalCase.productName = shieldProductName + ' Plan';
            var multipleLangProductName = _.cloneDeep(get(application, 'quotation.plans[0].covName') || get(application, 'quotation.baseProductName'));
            multipleLangProductName.en = multipleLangProductName.en + ' Plan';
            newApprovalCase.multiLangProductName = multipleLangProductName;
        } else {
            newApprovalCase.productName = get(application, 'quotation.plans[0].covName.en');
            newApprovalCase.multiLangProductName = get(application, 'quotation.plans[0].covName');
        }
        newApprovalCase.proposerName = get(application, 'applicationForm.values.proposer.personalInfo.fullName');
        newApprovalCase.quotationId = get(application, 'quotation.id');
        newApprovalCase.submittedDate = today;
        return newApprovalCase;
    }).then(function (result) {
        cDao.createApprovalCase(result, function (resp) {
            if (resp) cb(resp);
        });
    }).catch(function (error) {
        logger.error('Error in createApprovalCase->new Promise: ', error);
        cb({ success: false });
    });
};

var handleDirectorSubmitCase = function handleDirectorSubmitCase(agentProfile, agentId, directorId) {
    var role = _identifyApproveLevel(agentProfile);
    if (agentId === directorId && role === approvalStatusModel.ONELEVEL_SUBMISSION) {
        return 'A';
    } else if (agentId === directorId && role === approvalStatusModel.FAAGENT) {
        return 'PFAFA';
    } else {
        return 'SUBMITTED';
    }
};

var _identifyApproveLevel = function _identifyApproveLevel(agentProfile) {
    var channelType = _getOr('', 'channel.type', agentProfile);
    //let role = _getOr('', 'rawData.faAdvisorRole', agentProfile );
    var faAdminRole = _getOr('', 'rawData.userRole', agentProfile);
    if (channelType === 'FA' && faAdminRole === 'MM1') {
        return approvalStatusModel.FAADMIN;
    } else if (channelType === 'FA') {
        return approvalStatusModel.FAAGENT;
    } else {
        return approvalStatusModel.ONELEVEL_SUBMISSION;
    }
};
module.exports.identifyApproveLevel = _identifyApproveLevel;

var _getApproveStatusByRole = function _getApproveStatusByRole(role) {
    if (role === approvalStatusModel.FAADMIN) {
        return approvalStatusModel.APPRVOAL_STATUS_APPROVED;
    } else if (role === approvalStatusModel.FAAGENT) {
        return approvalStatusModel.APPRVOAL_STATUS_PFAFA;
    } else {
        return approvalStatusModel.APPRVOAL_STATUS_APPROVED;
    }
};
module.exports.getApproveStatusByRole = _getApproveStatusByRole;

var _genSupervisorPdf = function _genSupervisorPdf(data, session, cb) {
    var promises = [];
    promises.push(new Promise(function (resolve) {
        cDao.searchApprovalCaseById(data.id, function (c) {
            resolve(c.foundCase);
        });
    }));
    promises.push(new Promise(function (resolve) {
        resolve(cDao.getSupervisorTemplate(data.approverLevel));
    }));
    return Promise.all(promises).then(function (datas) {
        PDFHandler.getSupervisorTemplatePdf(datas[0], datas[1], data.lang, function (pdf) {
            cb({ success: true, pdf: pdf });
        });
    }).catch(function (error) {
        logger.error("Error in _genSupervisorPdf->Promise.all: ", error);
    });
};

module.exports.genSupervisorPdf = _genSupervisorPdf;

var handleSearchCasesStatusList = function handleSearchCasesStatusList(agentProfile, workbenchStatus) {
    var channelType = _getOr('', 'channel.type', agentProfile);
    var faAdminRole = _getOr('', 'rawData.userRole', agentProfile);
    if (workbenchStatus === true) {
        return ['SUBMITTED', 'PDoc', 'PDis', 'PFAFA', 'PDocFAF', 'PDisFAF', 'PCdaA', 'PDocCda', 'PDisCda', 'A', 'R', 'E'];
    } else if (channelType === 'FA' && faAdminRole === 'MM1') {
        return ['SUBMITTED', 'PDoc', 'PDis', 'PFAFA', 'PDocFAF', 'PDisFAF'];
    } else if (channelType === 'FA') {
        return ['SUBMITTED', 'PDoc', 'PDis', 'PDocFAF', 'PDisFAF'];
    } else {
        return ['SUBMITTED', 'PDoc', 'PDis'];
    }
};

module.exports.getPendingForApprovalCaseListLength = function (data, session, cb) {

    var statusList = handleSearchCasesStatusList(session.agent, false);
    async.waterfall([function (callback) {
        getApprovalCases(session.agent, statusList).then(function (approvals) {
            callback(null, approvals);
        });
    }, function (approvals, callback) {
        cDao.getApprovalPageTemplate(data.role, session, function (resp) {
            callback(null, {
                success: resp.success,
                searchedApprovalCases: approvals,
                reviewPageTemplate: resp.result,
                length: approvals ? approvals.length : 0
            });
        });
    }], function (err, result) {
        if (err) {
            logger.error('Failed to get no. of pending for approval cases', err);
            cb({ success: false });
        } else {
            cb(result);
        }
    });
};

module.exports.getDoc = function (data, session, cb) {
    dao.getDoc(data.id, function (doc) {
        cb({ success: true, foundDoc: doc });
    });
};

module.exports.getAgentProfileId = function (data, session, cb) {
    var result = [];
    return new Promise(function (resolve, reject) {
        dao.getViewRange('main', 'agents', '["' + data.compCode + '","' + data.id + '"' + ']', '["' + data.compCode + '","' + data.id + '"' + ']', null, function (pList) {
            if (pList && pList.rows) {
                for (var i = 0; i < pList.rows.length; i++) {
                    var valueObj = pList.rows[i].value;
                    valueObj.id = pList.rows[i].id;
                    result.push(valueObj);
                }
            }
            resolve(cb({ success: true, result: result[0] }));
        });
    }).catch(function (e) {
        logger.error('ERROR:: getAgentProfileId', e);
        cb({ success: false });
    });
};

module.exports.searchApprovalCases = function (data, session, cb) {
    var statusList = handleSearchCasesStatusList(session.agent, false);
    getApprovalCases(session.agent, statusList).then(function (approvals) {
        var cases = _.filter(approvals, function (approval) {
            var selected = true;
            _.each(data.filter, function (value, key) {
                var searchTextValue = filter(split(toUpper(value), " "), function (text) {
                    return !_isEmpty(_.trim(text));
                });
                value = trim(value);
                if (value === '') {
                    return;
                } else if (key !== 'approvalStatus') {
                    _.forEach(searchTextValue, function (v) {
                        if (_.toUpper(approval[key]).indexOf(v) === -1) {
                            selected = false;
                        }
                    });
                } else if (key === 'approvalStatus') {
                    var filterValues = split(value, ',');
                    if (indexOf(filterValues, approval[key]) === -1 && indexOf(filterValues, 'all') === -1) {
                        selected = false;
                    }
                }
            });
            return selected;
        });
        cb({
            success: true,
            result: cases
        });
    }).catch(function (e) {
        logger.error('Failed to get approval cases', e);
        cb({ success: false });
    });
};

var handleStartRange = function handleStartRange(date) {
    if (_isEmpty(date)) {
        date = new Date('1900-01-01');
    }
    date = handlePaddingZero(date);
    return Date.parse(date + 'T00:00:00.000Z');
};

var handleEndRange = function handleEndRange(date) {
    if (_isEmpty(date)) {
        date = moment().utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(FILTER_DATE_FORMAT);
    }
    date = handlePaddingZero(date);
    return Date.parse(date + 'T23:59:59.999Z');
};

var handlePaddingZero = function handlePaddingZero(date) {
    var dateArr = _.split(date, '-');
    if (dateArr && dateArr.length === 3) {
        forEach(dateArr, function (value, index) {
            if (value.length < 2) {
                dateArr[index] = '0' + value;
            }
        });
        return _join('-', dateArr);
    } else {
        return "";
    }
};

module.exports.searchWorkbenchCases = function (data, session, cb) {
    var statusList = handleSearchCasesStatusList(session.agent, true);
    var dateFilterMapping = {
        lastEdit: 'lastEditedDate',
        dateSubmitted: 'submittedDate',
        dateAppRej: 'approveRejectDate'
    };
    getInprogressBIandApplications(session.agent, function (inProgressCases) {
        getWorkbenchCases(inProgressCases, session.agent, statusList).then(function (approvals) {
            var cases = _.filter(approvals, function (approval) {
                var selected = true;
                var prefix = void 0;
                _.each(data.filter, function (value, key) {
                    var fitleredKey = [];
                    value = trim(value);
                    var searchTextValue = filter(split(toUpper(value), " "), function (text) {
                        return !_isEmpty(_.trim(text));
                    });
                    if (value === '') {
                        return;
                    } else if (key.toUpperCase().indexOf('_START_DATE') > -1 || key.toUpperCase().indexOf('_END_DATE') > -1) {
                        var trimIndicator = void 0;
                        key.toUpperCase().indexOf('_START_DATE') > -1 ? trimIndicator = '_START_DATE' : trimIndicator = '_END_DATE';
                        prefix = key.substr(0, key.indexOf(trimIndicator));

                        if (fitleredKey.indexOf(prefix) === -1) {
                            var endDate = prefix + '_END_DATE';
                            var startDate = prefix + '_START_DATE';
                            var mappedField = dateFilterMapping[prefix];
                            var filterEndKey = handleEndRange(data.filter[endDate]);
                            var filterStartKey = handleStartRange(data.filter[startDate]);
                            var searchValue = undefined;
                            if (approval[mappedField]) {
                                searchValue = Date.parse(moment(approval[mappedField], moment.ISO_8601).utcOffset(ConfigConstant.MOMENT_TIME_ZONE));
                            }
                            if (_.isNaN(searchValue) || searchValue === undefined || filterStartKey > searchValue || searchValue > filterEndKey) {
                                selected = false;
                            }
                            fitleredKey.push(prefix);
                        } else {
                            return;
                        }
                    } else if (key !== 'approvalStatus' && key.toUpperCase().indexOf('_DATE') === -1) {
                        _.forEach(searchTextValue, function (v) {
                            if (_.toUpper(approval[key]).indexOf(v) === -1) {
                                selected = false;
                            }
                        });
                    } else if (key === 'approvalStatus') {
                        var filterValues = split(value, ',');
                        if (indexOf(filterValues, approval[key]) === -1 && indexOf(filterValues, 'all') === -1) {
                            selected = false;
                        }
                    }
                });
                return selected;
            });
            cb({
                success: true,
                result: cases
            });
        }).catch(function (e) {
            logger.error('Failed to get approval cases', e);
            cb({ success: false });
        });
    });
};

/**
 * Found out the downline / proxy agents
 * When user is FA Firm search all submitted cases in the same hierarchy
 * @param {*} compCode 
 * @param {*} agentCode 
 */
var getRelatedAgentsByAgentCode = function getRelatedAgentsByAgentCode(compCode, agentCode, isFAAdmin) {
    if (!isFAAdmin) {
        return getNonFAFirmRelatedAgents(compCode, agentCode);
    } else {
        return getFAAdminRelatedAgents(compCode, agentCode);
    }
};

var getFAAdminRelatedAgents = function getFAAdminRelatedAgents(compCode, agentCode) {
    return new Promise(function (resolve, reject) {
        async.waterfall([function (callback) {
            var relatedAgentCode = [agentCode];
            agentDao.searchFAFirmDownline(compCode, relatedAgentCode, callback);
        }, function (relatedAgentCodes, callback) {
            // Get Agent Map in hierarchy
            agentDao.getAgentsByAgentCode(compCode, relatedAgentCodes).then(function (agentMap) {
                callback(null, agentMap);
            }).catch(function (error) {
                logger.error("Error in getRelatedAgentsByAgentCode->FAAdminRelatedAgents--> get agentMap in hierarchy: " + error);
                callback(error);
            });
        }, function (agentMap, callback) {
            // Get related Proxy agents
            var profileIds = [];
            _.each(agentMap, function (agentProfile) {
                if (agentProfile.isProxying && agentProfile.rawData) {
                    profileIds.push(agentProfile.rawData.proxy1UserId);
                    profileIds.push(agentProfile.rawData.proxy2UserId);
                }
            });
            agentDao.getAgentsByUserId(compCode, profileIds).then(function (proxyMap) {
                callback(null, agentMap);
            }).catch(function (error) {
                logger.error("Error in getRelatedAgentsByAgentCode->FAAdminRelatedAgents--> get proxy agents: " + error);
                callback(error);
            });
        }], function (err, agentMap) {
            if (err) {
                logger.error('Error in getRelatedAgentsByAgentCode-->FAAdminRelatedAgents--> : ', err);
                reject({ success: false });
            } else {
                resolve({ success: true, agentMap: agentMap });
            }
        });
    }).catch(function (error) {
        logger.error("ERROR:: getRelatedAgentsByAgentCode-->FAAdminRelatedAgents--> : " + error);
    });
};

var getNonFAFirmRelatedAgents = function getNonFAFirmRelatedAgents(compCode, agentCode) {
    return new Promise(function (resolve, reject) {
        async.waterfall([function (callback) {
            var relatedAgentCode = [agentCode];
            agentDao.recursiveSearchDownline(compCode, relatedAgentCode, relatedAgentCode, callback);
        }, function (relatedAgentCodes, callback) {
            // Union Agent Code and manager Code
            agentDao.recursiveSearchUpline(compCode, relatedAgentCodes, relatedAgentCodes, callback);
        }, function (relatedAgentCodes, callback) {
            // Get Agent Map in hierarchy
            agentDao.getAgentsByAgentCode(compCode, relatedAgentCodes).then(function (agentMap) {
                callback(null, agentMap);
            });
        }, function (agentMap, callback) {
            agentDao.getProxyDownline(compCode, _.map(agentMap, function (agent) {
                return agent.profileId;
            })).then(function (proxyDownlineAgentCodes) {
                // let relatedAgents = _.union(_.map(agentMap, agent => agent.agentCode), proxyDownlineAgentCodes) || [];
                proxyDownlineAgentCodes = _.uniq(proxyDownlineAgentCodes);
                callback(null, { agentMap: agentMap, proxyDownlineAgentCodes: proxyDownlineAgentCodes });
            });
        }, function (result, callback) {
            var proxyDownlineAgentCodes = result.proxyDownlineAgentCodes,
                agentMap = result.agentMap;
            // Filter out the difference and do the downline search

            var proxyRelatedAgentCodes = _.difference(proxyDownlineAgentCodes, _.map(agentMap, function (agent) {
                return agent.agentCode;
            }));
            if (proxyRelatedAgentCodes.length) {
                agentDao.recursiveSearchDownline(compCode, proxyRelatedAgentCodes, proxyRelatedAgentCodes, function (error, proxyAgentCodes) {
                    proxyRelatedAgentCodes = _.union(proxyRelatedAgentCodes, proxyAgentCodes);
                    error ? callback(error) : callback(null, { agentMap: agentMap, proxyRelatedAgentCodes: proxyRelatedAgentCodes });
                });
            } else {
                callback(null, { agentMap: agentMap, proxyRelatedAgentCodes: proxyRelatedAgentCodes });
            }
        }, function (result, callback) {
            var proxyRelatedAgentCodes = result.proxyRelatedAgentCodes,
                agentMap = result.agentMap;
            // Dp upline search when there is proxy agents that not search before

            if (proxyRelatedAgentCodes.length) {
                agentDao.recursiveSearchUpline(compCode, proxyRelatedAgentCodes, proxyRelatedAgentCodes, function (error, proxyAgentCodes) {
                    proxyRelatedAgentCodes = _.union(proxyRelatedAgentCodes, proxyAgentCodes);
                    error ? callback(error) : callback(null, { agentMap: agentMap, proxyRelatedAgentCodes: proxyRelatedAgentCodes });
                });
            } else {
                callback(null, { agentMap: agentMap, proxyRelatedAgentCodes: proxyRelatedAgentCodes });
            }
        }, function (result, callback) {
            var agentMap = result.agentMap,
                proxyRelatedAgentCodes = result.proxyRelatedAgentCodes;

            var relatedAgentCodes = _.union(_.map(agentMap, function (agent) {
                return agent.agentCode;
            }), proxyRelatedAgentCodes);
            agentDao.getAgentsByAgentCode(compCode, relatedAgentCodes).then(function (finalAgentMap) {
                callback(null, finalAgentMap);
            });
        }, function (agentMap, callback) {
            // Get related Proxy agents
            var profileIds = [];
            _.each(agentMap, function (agentProfile) {
                if (agentProfile.isProxying && agentProfile.rawData) {
                    profileIds.push(agentProfile.rawData.proxy1UserId);
                    profileIds.push(agentProfile.rawData.proxy2UserId);
                }
            });
            agentDao.getAgentsByUserId(compCode, profileIds).then(function (proxyMap) {
                callback(null, Object.assign(agentMap, proxyMap));
            }).catch(function (error) {
                logger.error("Error in getRelatedAgentsByAgentCode->getNonFAFirmRelatedAgents--> get proxy agents: " + error);
                callback(error);
            });
        }], function (err, agentMap) {
            if (err) {
                logger.error('Error in getRelatedAgentsByAgentCodegetNonFAFirmRelatedAgents--> : ', err);
                reject({ success: false });
            } else {
                resolve({ success: true, agentMap: agentMap });
            }
        });
    }).catch(function (error) {
        logger.error("ERROR:: getRelatedAgentsByAgentCodegetNonFAFirmRelatedAgents--> : " + error);
    });
};

var getRelatedAgents = function getRelatedAgents(compCode, approvals) {
    return new Promise(function (resolve, reject) {
        var agentCodes = {};
        _.each(approvals, function (approval) {
            agentCodes[approval.agentId] = true;
        });
        agentDao.getAgents(compCode, _.map(agentCodes, function (val, key) {
            return key;
        })).then(function (agentMap) {
            var managerCodes = _.map(agentMap, function (agent) {
                return agent.managerCode;
            });
            agentDao.getAgents(compCode, managerCodes).then(function (managerMap) {
                _.each(managerMap, function (manager) {
                    agentMap[manager.agentCode] = manager;
                });
                var directorCodes = _.map(managerMap, function (manager) {
                    return manager.managerCode;
                });
                agentDao.getAgents(compCode, directorCodes).then(function (directorMap) {
                    _.each(directorMap, function (director) {
                        agentMap[director.agentCode] = director;
                    });
                    var proxyUserIds = [];
                    _.each(managerMap, function (manager) {
                        if (manager.isProxying && manager.rawData) {
                            proxyUserIds.push(manager.rawData.proxy1UserId);
                            proxyUserIds.push(manager.rawData.proxy2UserId);
                        }
                    });
                    agentDao.getAgentsByUserId(compCode, proxyUserIds).then(function (proxyMap) {
                        _.each(proxyMap, function (proxy) {
                            agentMap[proxy.rawData.userId] = proxy; // use userId as key for proxies
                        });
                        resolve(agentMap);
                    }).catch(function (error) {
                        logger.error("Error in getRelatedAgents->agentDao.getAgentsByUserId: ", error);
                    });
                }).catch(function (error) {
                    logger.error("Error in getRelatedAgents->agentDao.getAgents[3]: ", error);
                });
            }).catch(function (error) {
                logger.error("Error in getRelatedAgents->agentDao.getAgents[2]: ", error);
            });
        }).catch(function (error) {
            logger.error("Error in getRelatedAgents->agentDao.getAgents[1]: ", error);
        });
    });
};

var getAssignedManager = function getAssignedManager(approval, agent, manager, proxy1, proxy2) {
    var assignedManager = null;
    if (manager && (!manager.isProxying || (!proxy1 || proxy1.isProxying) && (!proxy2 || proxy2.isProxying))) {
        assignedManager = manager;
    } else if (proxy1 && agent.agentCode !== proxy1.agentCode && !proxy1.isProxying && (approval.approvalStatus === 'SUBMITTED' && (dayDiff(truncDate(parseDatetime(approval.submittedDate)), new Date()) < global.config.SECONDARY_PROXY_ASSIGNMENT_DAY || !proxy2 || proxy2.isProxying) || ['PDoc', 'PDis'].indexOf(approval.approvalStatus) > -1 && approval.caseLockedManagerCodebyStatus === proxy1.agentCode || approval.approvalStatus === 'PFAFA' && approval.approveRejectManagerId === proxy1.agentCode)) {
        approval.proxyManagerId = proxy1.agentCode;
        approval.proxyManagerName = proxy1.name;
        approval.proxyManagerEmail = proxy1.email;
        approval.proxyManagerMobile = proxy1.mobile;
        assignedManager = proxy1;
    } else if (proxy2 && agent.agentCode !== proxy2.agentCode && !proxy2.isProxying && (approval.approvalStatus === 'SUBMITTED' && dayDiff(truncDate(parseDatetime(approval.submittedDate)), new Date()) >= global.config.SECONDARY_PROXY_ASSIGNMENT_DAY || ['PDoc', 'PDis'].indexOf(approval.approvalStatus) > -1 && approval.caseLockedManagerCodebyStatus === proxy2.agentCode || proxy1.isProxying || agent.agentCode === proxy1.agentCode || approval.approvalStatus === 'PFAFA' && approval.approveRejectManagerId === proxy2.agentCode)) {
        approval.proxyManagerId = proxy2.agentCode;
        approval.proxyManagerName = proxy2.name;
        approval.proxyManagerEmail = proxy2.email;
        approval.proxyManagerMobile = proxy2.mobile;
        assignedManager = proxy2;
    }
    return assignedManager;
};

var gethierarchyAgentCode = function gethierarchyAgentCode(hierarchyOutput, searchingAgentCode, compCode, cb) {
    return agentDao.searchDownline(compCode, searchingAgentCode).then(function (result) {
        if (_isArray(result) && result.length > 0) {

            var excludedSearchedAgents = _.filter(result, function (agentCode) {
                return hierarchyOutput.indexOf(agentCode) === -1;
            });
            hierarchyOutput = _concat(hierarchyOutput, result);
            gethierarchyAgentCode(hierarchyOutput, excludedSearchedAgents, compCode, cb);
        } else {
            var outpout = [];
            _.each(hierarchyOutput, function (value) {
                if (outpout.indexOf(value) === -1) {
                    outpout.push(value);
                }
            });
            cb(outpout);
        }
    }).catch(function (error) {
        logger.error("Error in gethierarchyAgentCode->searchDownline: ", error);
    });
};

var returnViewKeysArray = function returnViewKeysArray(result, compCode) {
    var viewKeysArray = [];
    _.each(result, function (agentCode) {
        viewKeysArray.push('["' + compCode + '","' + agentCode + '"]');
    });
    return viewKeysArray;
};

var getInprogressBIandApplications = function getInprogressBIandApplications(currentAgent, cb) {
    var promises = [];
    var searchingAgentCode = [currentAgent.agentCode];
    var hierarchyOutput = [currentAgent.agentCode];
    return gethierarchyAgentCode(hierarchyOutput, searchingAgentCode, currentAgent.compCode, function (result) {
        var filterKeys = returnViewKeysArray(result, currentAgent.compCode);
        promises.push(cDao.getquotationByAgent(currentAgent.compCode, filterKeys));
        promises.push(cDao.getapplicationByAgent(currentAgent.compCode, filterKeys));
        promises.push(cDao.getValidBundleCaseByAgent(currentAgent.compCode, filterKeys));
        promises.push(cDao.getAgentsProfile(currentAgent.compCode, filterKeys));

        Promise.all(promises).then(function (datas) {
            var quotationArr = datas[0];
            var applicationArr = datas[1];
            var validCaseNo = datas[2];
            var relatedAgentProfiles = datas[3];
            var result = [];
            var agentProfile = void 0;
            var managerProfile = void 0;
            _.each(quotationArr, function (value, key) {
                if (validCaseNo.indexOf(value.caseNo) > -1) {
                    value.inProgressBI = true;
                    value.approvalStatus = 'inProgressBI';
                    agentProfile = _getOr({}, value.agentId, relatedAgentProfiles);
                    value.agentProfileId = agentProfile.profileId;
                    value.customerName = value.proposerName;
                    value.product = _getOr('', 'product.en', value);
                    result.push(value);
                }
            });

            _.each(applicationArr, function (value, key) {
                if (validCaseNo.indexOf(value.applicationId) > -1) {
                    value.inProgressApp = true;
                    value.approvalStatus = 'inProgressApp';

                    value.customerName = value.proposerName;

                    if (_.isEqual(_.get(value, 'agentId'), currentAgent.agentCode)) {
                        value.canSubmit = true;
                    } else {
                        value.canSubmit = false;
                    }

                    value.product = _getOr('', 'product.en', value);
                    value.productName = value.product;
                    agentProfile = _getOr({}, value.agentId, relatedAgentProfiles);
                    value.agentProfileId = agentProfile.profileId;
                    value.organizationName = _getOr('', 'company', agentProfile);

                    managerProfile = _getOr({}, agentProfile.managerCode, relatedAgentProfiles);

                    value.managerName = managerProfile.agentName;

                    result.push(value);
                }
            });

            cb(result);
        }).catch(function (error) {
            logger.error("Error in getInprogressBIandApplications->Promise.all: ", error);
        });
    });
};

var _commonChangesInSearchingCases = function _commonChangesInSearchingCases(approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent) {
    var name = currentAgent.name,
        company = currentAgent.company,
        _currentAgent$channel = currentAgent.channel,
        channel = _currentAgent$channel === undefined ? {} : _currentAgent$channel;

    var showStampedInformationStatus = ['A', 'R', 'E', 'PFAFA', 'PDocFAF', 'PDisFAF'];
    var showProxywhenApprovedRejected = ['A', 'R'];
    //Search the stamped director if have
    if (isFAAdmin) {
        approval.searchFilter_directorName = name;
        approval.searchFilter_supervisorName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
        approval.managerName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
    } else if (showStampedInformationStatus.indexOf(get(approval, 'approvalStatus')) > -1 && approval.directorId) {
        approval.searchFilter_directorName = agentMap[approval.directorId] && agentMap[approval.directorId].name;
        approval.searchFilter_supervisorName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
        approval.managerName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
    } else if (showStampedInformationStatus.indexOf(get(approval, 'approvalStatus')) > -1) {
        approval.searchFilter_directorName = director && director.name;
        approval.searchFilter_supervisorName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
        approval.managerName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
    } else {
        approval.searchFilter_directorName = director && director.name;
        approval.searchFilter_supervisorName = _.get(assignedManager, 'name');
        approval.managerName = _.get(manager, 'name');
        approval.managerId = _.get(manager, 'agentCode');
    }

    // Override the director name to fa firm name if it is fa channel
    if (channel.type === 'FA') {
        approval.searchFilter_directorName = company;
    }

    if (showProxywhenApprovedRejected.indexOf(get(approval, 'approvalStatus')) > -1 && _getOr(approvedRejectedManagerId, 'managerId', approval) !== approvedRejectedManagerId && _getOr(approvedRejectedManagerId, 'directorId', approval) !== approvedRejectedManagerId) {
        approval.proxyManagerId = approval.approveRejectManagerId;
        approval.proxyManagerName = approval.approveRejectManagerName;
    }
};

var getApprovalCases = function getApprovalCases(currentAgent, statusList) {
    var compCode = currentAgent.compCode,
        name = currentAgent.name;

    var FAAdminPApprovalStatus = ['PFAFA', 'PDocFAF', 'PDisFAF'];
    var isAgencyDirector = currentAgent.channel.type === 'AGENCY' && currentAgent.role === 'MM1';
    var faAdminRole = _.get(currentAgent, 'rawData.userRole');
    var isFAAdmin = currentAgent.channel.type === 'FA' && faAdminRole === 'MM1';
    return getRelatedAgentsByAgentCode(compCode, currentAgent.agentCode, isFAAdmin).then(function (result) {
        if (result.success && result.agentMap) {
            var agentMap = result.agentMap;

            return cDao.getApprovalCasesByAgentCode(compCode, _.uniq(_.map(agentMap, function (obj) {
                return obj.agentCode;
            })), statusList).then(function (approvals) {
                return _.filter(approvals, function (approval) {
                    var agent = agentMap[approval.agentId];
                    var manager = agent && agentMap[agent.managerCode];
                    var director = manager && agentMap[manager.managerCode];
                    var proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
                    var proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
                    var assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
                    var approvedRejectedManagerId = _getOr('', 'approveRejectManagerId', approval);
                    _commonChangesInSearchingCases(approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent);
                    return assignedManager && assignedManager.agentCode === currentAgent.agentCode || isAgencyDirector && manager && manager.managerCode === currentAgent.agentCode || isFAAdmin && _.get(agent, 'rawData.upline2Code') === currentAgent.agentCode && FAAdminPApprovalStatus.indexOf(approval.approvalStatus) > -1;
                });
            });
        } else {
            logger.info("INFO:: agentMap count: " + (result.agentMap && result.agentMap.length));
            return [];
        }
    });
    //   return cDao.getApprovalCases(compCode, statusList).then((approvals) => {

    //     return getRelatedAgents(compCode, approvals).then((agentMap) => {
    //       return _.filter(approvals, (approval) => {
    //         let agent = agentMap[approval.agentId];
    //         let manager = agent && agentMap[agent.managerCode];
    //         let director = manager && agentMap[manager.managerCode];
    //         let proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
    //         let proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
    //         let assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
    //         let approvedRejectedManagerId = _getOr('', 'approveRejectManagerId', approval);

    //         _commonChangesInSearchingCases(approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent);

    //         return (assignedManager && assignedManager.agentCode === currentAgent.agentCode) ||
    //           (isAgencyDirector && manager && manager.managerCode === currentAgent.agentCode) ||
    //           (isFAAdmin && _.get(agent, 'rawData.upline2Code') === currentAgent.agentCode && FAAdminPApprovalStatus.indexOf(approval.approvalStatus) > -1);
    //       });
    //     }).catch((error)=>{
    //         logger.error("Error in getApprovalCases->getRelatedAgents: ", error);
    //     });
    //   }).catch((error)=>{
    //     logger.error("Error in getApprovalCases->cDao.getApprovalCases: ", error);
    //   });
};

var getWorkbenchCases = function getWorkbenchCases(inProgressCases, currentAgent, statusList) {
    var compCode = currentAgent.compCode,
        name = currentAgent.name;

    var FAAdminWBStatus = ['PFAFA', 'PDocFAF', 'PDisFAF'];
    var showStampedInformationStatus = ['A', 'R', 'E', 'PFAFA'];
    var showProxywhenApprovedRejected = ['A', 'R'];
    var isAgencyDirector = currentAgent.channel.type === 'AGENCY' && currentAgent.role === 'MM1';
    var faAdminRole = _.get(currentAgent, 'rawData.userRole');
    var isFAAdmin = currentAgent.channel.type === 'FA' && faAdminRole === 'MM1';

    return getRelatedAgentsByAgentCode(compCode, currentAgent.agentCode, isFAAdmin).then(function (result) {
        if (result.success && result.agentMap) {
            var agentMap = result.agentMap;

            return cDao.getApprovalCasesByAgentCode(compCode, _.uniq(_.map(agentMap, function (obj) {
                return obj.agentCode;
            })), statusList).then(function (approvals) {
                approvals = _.concat(approvals, inProgressCases);
                return _.filter(approvals, function (approval) {
                    var agent = agentMap[approval.agentId];
                    var manager = agent && agentMap[agent.managerCode];
                    var director = manager && agentMap[manager.managerCode];
                    var proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
                    var proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
                    var assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
                    var approvedRejectedManagerId = _getOr('', 'approveRejectManagerId', approval);

                    _commonChangesInSearchingCases(approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent);

                    //Add the approver to getWorkbench cases result
                    if (_get('channel.type', currentAgent) === 'FA' && FAAdminWBStatus.indexOf(approval.approvalStatus) > -1) {
                        approval.approver = [_.get(assignedManager, 'agentCode'), _.get(agent, 'rawData.upline2Code')];
                    } else if (_get('channel.type', currentAgent) === 'FA') {
                        //Assigned Manager and FA Firm are the approver
                        approval.approver = [_.get(assignedManager, 'agentCode')];
                    } else {
                        //Assigned Manager and Director are the approver
                        approval.approver = [_.get(assignedManager, 'agentCode'), _.get(director, 'agentCode')];
                    }

                    return assignedManager && assignedManager.agentCode === currentAgent.agentCode || isAgencyDirector && manager && manager.managerCode === currentAgent.agentCode || isFAAdmin && _.get(agent, 'rawData.upline2Code') === currentAgent.agentCode || approval.agentId === currentAgent.agentCode || manager && manager.agentCode === currentAgent.agentCode;
                });
            });
        } else {
            logger.info("INFO:: agentMap count: " + (result.agentMap && result.agentMap.length));
            return [];
        }
    });
};

/**
 * Checks if the current agent can approve the given approval case.
 *
 * @param {*} currentAgent the agent from session
 * @param {*} approval the approval case
 */
var canApproveCase = function canApproveCase(currentAgent, approval) {
    approval = Object.assign({}, approval);
    return getRelatedAgents(currentAgent.compCode, [approval]).then(function (agentMap) {
        var agent = agentMap[approval.agentId];
        var manager = agent && agentMap[agent.managerCode];
        var proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
        var proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
        var assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
        if (['SUBMITTED', 'PDoc', 'PDis'].indexOf(approval.approvalStatus) > -1) {
            var isAgencyDirector = currentAgent.channel.type === 'AGENCY' && currentAgent.role === 'MM1';
            return assignedManager && currentAgent.agentCode === assignedManager.agentCode || isAgencyDirector && manager && manager.managerCode === currentAgent.agentCode;
        } else if (['PFAFA', 'PDocFAF', 'PDisFAF'].indexOf(approval.approvalStatus) > -1) {
            var isFAAdmin = currentAgent.channel.type === 'FA' && currentAgent.rawData.userRole === 'MM1';
            return isFAAdmin && currentAgent.agentCode === agent.rawData.upline2Code;
        }
        return false;
    }).catch(function (error) {
        logger.error("Error in canApproveCase->getRelatedAgents: ", error);
    });
};
module.exports.canApproveCase = canApproveCase;

module.exports.approveCase = function (data, session, cb) {
    logger.log('APPROVAL:: Approve Case Start');
    var approverLevel = _identifyApproveLevel(session.agent);
    var tempAppCase = data.approvalCase;
    var attachments = cloneDeep(data.approveChangedValues.jfwFile);
    var jfwFileName = [];
    var tempObj = {};
    var attachmentsType = [];

    forEach(data.jfwFileProperties, function (obj, index) {
        tempObj.fileName = obj.name;
        tempObj.type = obj.type;
        tempObj.attId = 'eapproval_' + index;
        jfwFileName.push(cloneDeep(tempObj));
        attachmentsType.push(obj.type);
    });

    return agentDao.getManagerDirectorProfileByAgentCode(tempAppCase.compCode, tempAppCase.agentId).then(function (profiles) {
        logger.log('APPROVAL:: SUCCESSFULLY GET Director Profile');
        return new Promise(function (resolve) {
            var tempAppCase = data.approvalCase;
            tempAppCase.approvalStatus = _getApproveStatusByRole(approverLevel);
            tempAppCase.onHoldReason = '';
            var curTime = new Date();
            var enablePOSEmail = approverLevel === approvalStatusModel.FAADMIN || approverLevel === approvalStatusModel.ONELEVEL_SUBMISSION;
            if (approverLevel !== approvalStatusModel.FAADMIN) {
                tempAppCase.directorId = _getOr('', 'directorProfile.agentCode', profiles);
                tempAppCase.directorName = _getOr('', 'directorProfile.name', profiles);
                tempAppCase.directorEmail = _getOr('', 'directorProfile.email', profiles);
                tempAppCase.directorMobile = _getOr('', 'directorProfile.mobile', profiles);

                tempAppCase.managerId = _getOr('', 'managerProfile.agentCode', profiles);
                tempAppCase.managerName = _getOr('', 'managerProfile.name', profiles);
                tempAppCase.managerEmail = _getOr('', 'managerProfile.email', profiles);
                tempAppCase.managerMobile = _getOr('', 'managerProfile.mobile', profiles);
                tempAppCase.isProxyApproved = _getOr(session.agent.agentCode, 'managerProfile.agentCode', profiles) !== session.agent.agentCode && _getOr(session.agent.agentCode, 'directorProfile.agentCode', profiles) !== session.agent.agentCode;
            }
            //Handle FA Admin Approval
            if (approverLevel === approvalStatusModel.FAADMIN) {
                logger.log('APPROVAL:: FA Admin Approve Case');
                //tempAppCase.supervisorApproveRejectDate = clone(tempAppCase.approveRejectDate);
                tempAppCase.approveRejectDate = curTime.toISOString();
                tempAppCase.approver_FAAdminCode = session.agent.agentCode;
                tempAppCase.accept_FAAdmin = data.approveChangedValues;
            } else if (approverLevel === approvalStatusModel.FAAGENT) {
                logger.log('APPROVAL:: FA Channel Agent Approve Case');
                tempAppCase.supervisorApproveRejectDate = curTime.toISOString();
                tempAppCase.approver_FAAdminCode = "";
                tempAppCase.accept = data.approveChangedValues;
                tempAppCase.approveRejectManagerId = session.agent.agentCode;
                tempAppCase.approveRejectManagerName = session.agent.name;
                tempAppCase.approveRejectManagerEmail = session.agent.email;
                tempAppCase.approveRejectManagerMobile = session.agent.mobile;
            } else {
                logger.log('APPROVAL:: Agency Channel Approve Case');
                tempAppCase.approveRejectDate = curTime.toISOString();
                tempAppCase.accept = data.approveChangedValues;
                tempAppCase.approveRejectManagerId = session.agent.agentCode;
                tempAppCase.approveRejectManagerName = session.agent.name;
                tempAppCase.approveRejectManagerEmail = session.agent.email;
                tempAppCase.approveRejectManagerMobile = session.agent.mobile;
                if (tempAppCase.accept) {
                    tempAppCase.accept.jfwFile = undefined;
                }
                tempAppCase.jfwFileName = jfwFileName;
                if (tempAppCase.accept && tempAppCase.accept.callDate !== undefined) {
                    var isoStringCallDate = new Date(tempAppCase.accept.callDate);
                    tempAppCase.accept.callDate = moment(isoStringCallDate.toISOString()).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                }
            }

            tempAppCase.approvalCaseHasIndividual = data.approvalCaseHasIndividual;

            _updateApprovalCaseById({ id: tempAppCase.approvalCaseId, approvalCase: tempAppCase }, session, function (resp) {
                resolve(resp);
            });
        });
    }).then(function (resp) {
        logger.log('APPROVAL:: Generate Approve supervisor validation PDF');
        var caseId = resp.updatedCase.approvalCaseId;
        return new Promise(function (resolve) {
            _genSupervisorPdf({
                id: caseId,
                lang: 'en',
                approverLevel: approverLevel
            }, session, function (resp) {
                if (resp.success && resp.pdf) {
                    logger.log('APPROVAL:: Start to upload Approve supervisor validation PDF');
                    var pdfName = void 0;
                    approverLevel === approvalStatusModel.FAADMIN ? pdfName = 'faFirm_Comment' : pdfName = 'eapproval_supervisor_pdf';
                    _uploadAttachment({
                        id: caseId,
                        base64Str: resp.pdf,
                        fileId: pdfName
                    }, session, function () {
                        resolve();
                    });
                } else {
                    logger.error('APPROVAL:: Fail to generate APPROVAL supervisor validation pdf (no success response / no base64 string');
                }
            });
        });
    }).then(function () {
        logger.log('APPROVAL:: Upload Approve Process Attachments');
        if (attachments && attachments.length > 0) {
            return new Promise(function (resolve) {
                _uploadAttachments({
                    id: tempAppCase.approvalCaseId,
                    attachments: attachments,
                    attachmentsType: attachmentsType
                }, session, function () {
                    return resolve();
                });
            });
        } else {
            return;
        }
    }).then(function () {
        return new Promise(function (resolve) {
            dao.getDoc(tempAppCase.approvalCaseId, function (doc) {
                resolve(doc);
            });
        });
    }).then(function (doc) {
        logger.log('APPROVAL:: Send Approve Notification');
        aNotifyHandler.ApproveNotification({ approverLevel: approverLevel, id: data.approvalCase.approvalCaseId }, session);
        cb({ success: true, approvedCaase: doc });
    }).catch(function (e) {
        logger.error('Approve Process Error: ' + e);
        cb({ success: false });
    });
};

module.exports.rejectCase = function (data, session, cb) {
    logger.log('APPROVAL:: reject case start');
    var agentProfile = session.agent;
    var approverLevel = _identifyApproveLevel(agentProfile);
    var tempAppCase = data.approvalCase;
    return agentDao.getManagerDirectorProfileByAgentCode(tempAppCase.compCode, tempAppCase.agentId).then(function (profiles) {
        logger.log('APPROVAL:: SUCCESSFULLY GET Director Profile');
        return new Promise(function (resolve) {
            tempAppCase.approvalStatus = approvalStatusModel.APPRVOAL_STATUS_REJECTED;
            tempAppCase.onHoldReason = '';
            var curTime = new Date();
            if (approverLevel !== approvalStatusModel.FAADMIN) {
                tempAppCase.directorId = _getOr('', 'directorProfile.agentCode', profiles);
                tempAppCase.directorName = _getOr('', 'directorProfile.name', profiles);
                tempAppCase.directorEmail = _getOr('', 'directorProfile.email', profiles);
                tempAppCase.directorMobile = _getOr('', 'directorProfile.mobile', profiles);

                tempAppCase.managerId = _getOr('', 'managerProfile.agentCode', profiles);
                tempAppCase.managerName = _getOr('', 'managerProfile.name', profiles);
                tempAppCase.managerEmail = _getOr('', 'managerProfile.email', profiles);
                tempAppCase.managerMobile = _getOr('', 'managerProfile.mobile', profiles);
                tempAppCase.isProxyApproved = _getOr(agentProfile.agentCode, 'managerProfile.agentCode', profiles) !== agentProfile.agentCode && _getOr(agentProfile.agentCode, 'directorProfile.agentCode', profiles) !== agentProfile.agentCode;
            }
            //Handle FA Admin Rejection
            if (approverLevel === approvalStatusModel.FAADMIN) {
                logger.log('APPROVAL:: FA Admin Reject Case');
                //tempAppCase.supervisorApproveRejectDate = clone(tempAppCase.approveRejectDate);;
                tempAppCase.approveRejectDate = curTime.toISOString();
                tempAppCase.approver_FAAdminCode = agentProfile.agentCode;
                tempAppCase.reject_FAAdmin = data.rejectChangedValues;
            } else if (approverLevel === approvalStatusModel.FAAGENT) {
                logger.log('APPROVAL:: FA Channel Agent reject case');
                tempAppCase.supervisorApproveRejectDate = curTime.toISOString();
                tempAppCase.approveRejectDate = curTime.toISOString();
                tempAppCase.approver_FAAdminCode = "";
                tempAppCase.reject = data.rejectChangedValues;
                tempAppCase.approveRejectManagerId = agentProfile.agentCode;
                tempAppCase.approveRejectManagerName = agentProfile.name;
                tempAppCase.approveRejectManagerEmail = agentProfile.email;
                tempAppCase.approveRejectManagerMobile = agentProfile.mobile;
            } else {
                logger.log('APPROVAL:: Agency Channel Reject Case');
                tempAppCase.approveRejectDate = curTime.toISOString();
                tempAppCase.reject = data.rejectChangedValues;
                tempAppCase.approveRejectManagerId = agentProfile.agentCode;
                tempAppCase.approveRejectManagerName = agentProfile.name;
                tempAppCase.approveRejectManagerEmail = agentProfile.email;
                tempAppCase.approveRejectManagerMobile = agentProfile.mobile;
            }
            tempAppCase.approvalCaseHasIndividual = data.approvalCaseHasIndividual;
            _updateApprovalCaseById({ id: tempAppCase.approvalCaseId, approvalCase: tempAppCase }, session, function (resp) {
                resolve(resp);
            });
        });
    }).then(function (resp) {
        logger.log('APPROVAL:: Generate Reject supervisor validation pdf');
        var caseId = resp.updatedCase.approvalCaseId;
        return new Promise(function (resolve) {
            _genSupervisorPdf({
                id: caseId,
                lang: 'en',
                approverLevel: approverLevel
            }, session, function (resp) {
                if (resp.success && resp.pdf) {
                    logger.log('APPROVAL:: Start to upload reject supervisor validation PDF');
                    var pdfName = void 0;
                    approverLevel === approvalStatusModel.FAADMIN ? pdfName = 'faFirm_Comment' : pdfName = 'eapproval_supervisor_pdf';
                    _uploadAttachment({
                        id: caseId,
                        base64Str: resp.pdf,
                        fileId: pdfName
                    }, session, function () {
                        resolve();
                    });
                } else {
                    logger.error('APPROVAL:: Fail to generate REJECT supervisor validation pdf (no success response / no base64 string');
                }
            });
        });
    }).then(function () {
        return new Promise(function (resolve) {
            dao.getDoc(tempAppCase.approvalCaseId, function (doc) {
                resolve(doc);
            });
        });
    }).then(function (doc) {
        logger.log('APPROVAL:: Start to send reject Notification');
        aNotifyHandler.RejectNotification({ approverLevel: approverLevel, id: data.approvalCase.approvalCaseId }, session);
        cb({ success: true, rejectCase: doc });
    }).catch(function (e) {
        logger.error('Reject Case: ' + e);
        cb({ success: false });
    });
};

module.exports.saveComment = function (data, session, cb) {
    logger.log('APPROVAL:: save comment start');
    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            resolve(doc);
        });
    }).then(function (doc) {
        return new Promise(function (resolve) {
            var tempAppCase = doc;
            var agentProfile = session.agent;
            var curTime = new Date();
            var currentTime = curTime.toISOString();
            var tempComment = get(tempAppCase, 'comment') || [];
            var commentAdded = [{
                'content': data.comment,
                'author': agentProfile.name,
                'authorId': agentProfile.agentCode,
                'authorPid': agentProfile.profileId,
                'createTime': currentTime,
                'editedTime': currentTime
            }];
            tempAppCase.onHoldReason = data.comment;
            tempAppCase.comment = concat(commentAdded, tempComment);
            _updateApprovalCaseById({ id: tempAppCase.approvalCaseId, approvalCase: tempAppCase }, session, function (resp) {
                resolve(cb({ success: true, updatedCase: resp.updatedCase }));
            });
        });
    }).catch(function (e) {
        logger.error('Save Comment Error: ' + e);
        cb({ success: false });
    });
};

module.exports.editComment = function (data, session, cb) {
    logger.log('APPROVAL:: edit comment start');
    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            resolve(doc);
        });
    }).then(function (doc) {
        var tempAppCase = doc;
        return new Promise(function (resolve) {
            var tempComment = get(tempAppCase, 'comment');
            if (data.index === 0) {
                tempAppCase.onHoldReason = data.comment;
            }
            tempComment[data.index].content = data.comment;
            var curTime = new Date();
            tempComment[data.index].editedTime = curTime.toISOString();

            _updateApprovalCaseById({ id: tempAppCase.approvalCaseId, approvalCase: tempAppCase }, session, function (resp) {
                resolve(cb({ success: true, updatedCase: resp.updatedCase }));
            });
        });
    }).catch(function (e) {
        logger.error('Save Comment Error: ' + e);
        cb({ success: false });
    });
};

module.exports.deleteComment = function (data, session, cb) {
    logger.log('APPROVAL:: delete comment start');
    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            resolve(doc);
        });
    }).then(function (doc) {
        var tempAppCase = doc;
        return new Promise(function (resolve) {
            var tempComment = get(tempAppCase, 'comment');

            if (data.index === 0) {
                tempAppCase.onHoldReason = _.get(tempComment, '[1].content', '');
            }

            tempAppCase.comment = filter(tempComment, function (obj, index) {
                return index !== data.index;
            });

            _updateApprovalCaseById({ id: tempAppCase.approvalCaseId, approvalCase: tempAppCase }, session, function (resp) {
                resolve(cb({ success: true, updatedCase: resp.updatedCase }));
            });
        });
    }).catch(function (e) {
        logger.error('Save Comment Error: ' + e);
        cb({ success: false });
    });
};

var _getRelatedAgentsWithRoleAssigned = function _getRelatedAgentsWithRoleAssigned(compCode, agentCode, approval) {
    var rolesObj = {};
    return new Promise(function (resolve) {
        agentDao.geAllAgents(compCode).then(function (agentMap) {
            rolesObj.agent = agentMap[agentCode];
            rolesObj.manager = rolesObj.agent && agentMap[rolesObj.agent.managerCode];
            rolesObj.director = rolesObj.manager && agentMap[rolesObj.manager.managerCode];
            rolesObj.proxy1 = rolesObj.manager && rolesObj.manager.rawData && agentMap[rolesObj.manager.rawData.proxy1UserId];
            rolesObj.proxy2 = rolesObj.manager && rolesObj.manager.rawData && agentMap[rolesObj.manager.rawData.proxy2UserId];
            if (approval.approveRejectManagerId) {
                rolesObj.assignedManager = agentMap[approval.approveRejectManagerId];
            } else {
                rolesObj.assignedManager = getAssignedManager(approval, rolesObj.agent, rolesObj.manager, rolesObj.proxy1, rolesObj.proxy2);
            }
            resolve(rolesObj);
        });
    }).catch(function (e) {
        logger.error('Get Role Assigned: ' + e);
    });
};
module.exports.getRelatedAgentsWithRoleAssigned = _getRelatedAgentsWithRoleAssigned;

module.exports.viewJFWFiles = function (data, session, cb) {
    var now = Date.now();
    var signDocConfig = {
        // pdfUrl: '',
        attUrl: global.config.signdoc.getPdf,
        postUrl: global.config.signdoc.postUrl,
        resultUrl: global.config.signdoc.resultUrl,
        dmsId: global.config.signdoc.dmsid,
        docid: data.attId,
        auth: appHandler.generateSignDocAuth(data.attId, now),
        docts: now
    };
    fileHandler.getAttachment(data, session, function () {
        var resp = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var tokens = [createPdfToken(data.docId, data.attId, now, session.loginToken)];
        setPdfTokensToRedis(tokens, function () {
            logger.log('INFO: View JFW Files', data.docId);
            cb({
                success: true,
                token: tokens[0].token,
                signDocConfig: signDocConfig,
                data: resp.data
            });
        });
    });
};