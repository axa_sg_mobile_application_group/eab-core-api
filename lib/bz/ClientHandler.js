"use strict";

var cDao = require("./cbDao/client");
var nDao = require("./cbDao/needs");
var bDao = require("./cbDao/bundle");
var dao = require('./cbDaoFactory').create();
var logger = global.logger || console;

module.exports.getProfileLayout = function (data, session, cb) {
  cDao.getProfileLayout({}, function (doc) {
    var ret = {
      success: true,
      template: doc
    };
    if (ret) {
      cb(ret);
    } else {
      cb({});
    }
  });
};

module.exports.saveTrustedIndividual = function (data, session, cb) {
  var cid = data.cid,
      tiInfo = data.tiInfo,
      tiPhoto = data.tiPhoto,
      confirm = data.confirm;
  //check change would affect current profile or not
  //also check which module will have effort

  bDao.onSaveTrustedIndividual(cid, tiInfo).then(function (result) {
    //if no effort or user clicked confirm at ui
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      //invalidate client if neccessary
      var save = function save() {
        cDao.saveTrustedIndividual(cid, tiInfo, tiPhoto).then(function (resp) {
          bDao.updateApplicationTrustedIndividual(cid, false, tiInfo).then(function () {
            nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
              cb({
                success: resp.profile ? true : false,
                profile: resp.profile,
                showFnaInvalidFlag: showFnaInvalidFlag
              });
            });
          }).catch(function (error) {
            logger.error("Error in saveTrustedIndividual->updateApplicationTrustedIndividual: ", error);
          });
        }).catch(function (error) {
          logger.error("Error in saveTrustedIndividual->saveTrustedIndividual: ", error);
        });
      };
      if (result.code === bDao.CHECK_CODE.VALID) {
        save();
      } else {
        bDao.invalidTrustedIndividual(cid).then(save).catch(function (error) {
          logger.error("Error in saveTrustedIndividual->invalidTrustedIndividual: ", error);
        });
      }
    } else {
      cb({ success: true, code: result.code });
    }
  }).catch(function (error) {
    logger.error("Error in saveTrustedIndividual->onSaveTrustedIndividual: ", error);
  });
};

module.exports.deleteTrustIndividual = function (data, session, cb) {
  var cid = data.cid;

  bDao.invalidTrustedIndividual(cid).then(function () {
    //save profile after invalidate
    cDao.saveTrustedIndividual(cid, {}, null).then(function (resp) {
      bDao.updateApplicationTrustedIndividual(cid, false, {}).then(function () {
        nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
          cb({
            success: resp.profile ? true : false,
            profile: resp.profile,
            showFnaInvalidFlag: showFnaInvalidFlag
          });
        });
      }).catch(function (error) {
        logger.error("Error in deleteTrustIndividual->updateApplicationTrustedIndividual: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in deleteTrustIndividual->saveTrustedIndividual: ", error);
    });
  }).catch(function (error) {
    logger.error("Error in deleteTrustIndividual->invalidTrustedIndividual: ", error);
  });
};

module.exports.unlinkRelationship = function (data, session, cb) {
  var cid = data.cid,
      fid = data.fid;

  var errObj = {
    fna: true,
    quotation: true,
    application: true,
    removeDependant: true,
    removeApplicant: true
  };

  bDao.invalidFamilyMember(cid, session.agent, fid, errObj).then(function (resp) {

    cDao.unlinkRelationship(data.cid, data.fid, function (doc) {
      if (!doc.error) {
        nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
          cb({
            success: true,
            profile: doc,
            showFnaInvalidFlag: showFnaInvalidFlag
          });
        });
      } else {
        cb({ success: false });
      }
    });
  }).catch(function (error) {
    logger.error("Error in unlinkRelationship->:invalidFamilyMember ", error);
  });
};

module.exports.getTrustIndividualLayout = function (data, session, cb) {
  cDao.getTrustIndividualLayout({}, function (doc) {
    if (!doc.error) {
      cb({
        success: true,
        template: doc
      });
    } else {
      cb({ success: false });
    }
  });
};

module.exports.getProfileDisplayLayout = function (data, session, cb) {
  cDao.getProfileDisplayLayout({}, function (doc) {
    if (!doc.error) {
      cb({
        success: true,
        template: doc
      });
    } else {
      cb({ success: false });
    }
  });
};

module.exports.getFamilProfileLayout = function (data, session, cb) {
  cDao.getFamilProfileLayout({}, function (doc) {
    if (!doc.error) {
      cb({
        success: true,
        template: doc
      });
    } else {
      cb({ success: false });
    }
  });
};

module.exports.saveFamilyMember = function (data, session, cb) {
  var fid = data.fid,
      cid = data.cid,
      profile = data.profile,
      photo = data.photo,
      confirm = data.confirm;
  var _session$agent = session.agent,
      aid = _session$agent.agentCode,
      _session$agent$featur = _session$agent.features,
      features = _session$agent$featur === undefined ? "" : _session$agent$featur;

  bDao.onSaveClient(cid, features.indexOf("FNA") > -1 ? true : false, profile).then(function (result) {
    if (result) {
      if (confirm || result.code === bDao.CHECK_CODE.VALID) {
        bDao.invalidFamilyMember(cid, session.agent, fid, result).then(function () {
          var resp = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          //update bundle if there is new bundle
          if (resp && resp.bundle) {
            profile.bundle = resp.bundle;
          }
          cDao.saveFamilyMember(fid, cid, session.agent, profile, photo, function (doc, fDoc, fid) {
            if (!doc.error) {
              bDao.updateApplicationClientProfiles(fDoc).then(function () {
                nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
                  cb({
                    success: true,
                    profile: doc,
                    fProfile: fDoc,
                    invalid: resp.invalid,
                    fid: fid,
                    showFnaInvalidFlag: showFnaInvalidFlag
                  });
                });
              }).catch(function (error) {
                logger.error("Error in saveFamilyMember->updateApplicationClientProfile: ", error);
              });
            } else {
              cb({ success: false });
            }
          });
        }).catch(function (error) {
          logger.error("Error in saveFamilyMember->invalidFamilyMember: ", error);
        });
      } else {
        cb({ success: true, code: result.code });
      }
    } else {
      logger.error("Error in saveFamilyMember->:onSaveClient [#POS 2]");
      cb({ success: false });
    }
  }).catch(function (error) {
    logger.error("Error in saveFamilyMember->:onSaveClient ", error);
  });
};

module.exports.getProfile = function (data, session, cb) {
  var cid = data.cid || data.docId;
  cDao.getProfile(cid).then(function (profile) {
    nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
      cb({
        success: profile ? true : false,
        profile: profile,
        showFnaInvalidFlag: showFnaInvalidFlag
      });
    });
  }).catch(function (error) {
    logger.error("Error in getProfile->getProfile: ", error);
  });
};

var getContactList = function getContactList(data, session, cb) {
  logger.log("start get contact list");
  cDao.getContactList("01", session.agent.agentCode, function (pList) {
    cb({
      success: true,
      contactList: pList
    });
  });
};

module.exports.getContactList = getContactList;

module.exports.addProfile = function (data, session, cb) {
  var profile = data.profile,
      photo = data.photo,
      aid = session.agent.agentCode;
  cDao.addProfile(profile, session.agent, photo, function (doc) {
    if (!doc.error) {
      cb({
        success: true,
        showFnaInvalidFlag: false,
        profile: doc
      });
    } else {
      cb({ success: false });
    }
  });
};

module.exports.deleteProfile = function (data, session, cb) {
  cDao.deleteProfile(data.cid, function (result) {
    logger.log("delete profile", result);
    if (!result.error) {
      cb({ success: true });
    } else {
      cb({ success: false });
    }
  });
};

module.exports.saveProfile = function (data, session, cb) {
  var _data$profile = data.profile,
      profile = _data$profile === undefined ? {} : _data$profile,
      photo = data.photo,
      tiPhoto = data.tiPhoto,
      confirm = data.confirm;
  var cid = profile.cid;
  var _session$agent2 = session.agent,
      aid = _session$agent2.agentCode,
      _session$agent2$featu = _session$agent2.features,
      features = _session$agent2$featu === undefined ? "" : _session$agent2$featu;
  //check change would affect current profile or not
  //also check which module will have effort

  bDao.onSaveClient(cid, features.indexOf("FNA") > -1 ? true : false, profile).then(function (result) {
    //if no effort or user clicked confirm at ui
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      //invalidate client if neccessary
      bDao.invalidFamilyMember(cid, session.agent, cid, result).then(function (resp) {
        //update bundle if there is new bundle
        if (resp && resp.bundle) {
          profile.bundle = resp.bundle;
        }
        //save profile after invalidate
        cDao.saveProfile(profile, photo, tiPhoto, session.agent, function (doc) {
          if (!doc.error) {
            bDao.updateApplicationClientProfiles(profile).then(function () {
              cDao.getAllDependantsProfile(cid).then(function (dependantProfiles) {
                nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
                  cb({
                    success: true,
                    dependantProfiles: dependantProfiles,
                    profile: doc,
                    invalid: resp.invalid,
                    showFnaInvalidFlag: showFnaInvalidFlag
                  });
                });
              }).catch(function (error) {
                logger.error("Error in saveProfile->getAllDependantsProfile: ", error);
              });
            }).catch(function (error) {
              logger.error("Error in saveProfile->updateApplicationClientProfile: ", error);
            });
          } else {
            cb({ success: false });
          }
        });
      }).catch(function (error) {
        logger.error("Error in saveProfile->invalidClient: ", error);
      });
    } else {
      cb({ success: true, code: result.code });
    }
  }).catch(function (error) {
    logger.error("Error in saveProfile->onSaveClient: ", error);
  });
};

module.exports.getAllDependantsProfile = function (data, session, cb) {
  var cid = data.cid;
  cDao.getAllDependantsProfile(cid).then(function (dependantProfiles) {
    cb({ success: true, dependantProfiles: dependantProfiles });
  });
};

module.exports.saveClientProfile = function (data, session, cb) {
  logger.log('call login');
  cb(true);
};

module.exports.getAddressByPostalCode = function (data, session, cb) {
  var postalCode = data.pC;
  var fileName = data.fileName;
  cDao.getAddressByPostalCode(postalCode, fileName, cb);
};