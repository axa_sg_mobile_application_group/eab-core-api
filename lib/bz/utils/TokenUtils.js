'use strict';

var uuid = require('uuid');
var _ = require('lodash');

var _require = require('../utils/CommonUtils'),
    getFromRedis = _require.getFromRedis,
    setToRedis = _require.setToRedis;

var logger = global.logger;

var getPdfToken = function getPdfToken(docId, attId, session) {
  return new Promise(function (resolve) {
    var now = Date.now();
    var token = createPdfToken(docId, attId, now, session.loginToken);
    setPdfTokensToRedis([token], function () {
      resolve(token.token);
    });
  });
};

//Function: for Signature, clear PDF token
var clearInvalidPdfToken = function clearInvalidPdfToken() {
  var now = Date.now();
  getFromRedis('attachmentToken', function (tokens) {
    if (!tokens) {
      tokens = {};
    }
    for (var key in tokens) {
      var value = tokens[key];
      if (now > value.expiryTime) {
        delete tokens[key];
      }
    }
    setToRedis('attachmentToken', tokens);
  });
};

//Function: for Signature, create PDF token
var createPdfToken = function createPdfToken(docId, attId, now, lToken) {
  var expiryTime = new Date().setTime(now + global.config.signdoc.pdfTimeout);

  //Clear Timeout Token before start
  clearInvalidPdfToken();

  //Generate new token
  return {
    token: uuid.v4(),
    docId: docId,
    attId: attId,
    lToken: lToken,
    expiryTime: expiryTime
  };
};

var setPdfTokensToRedis = function setPdfTokensToRedis(pdfTokens, callback) {
  logger.log('INFO: setPdfTokensToRedis - start');
  getFromRedis('attachmentToken', function (tokens) {
    if (pdfTokens instanceof Array) {
      if (pdfTokens.length > 0) {
        _.forEach(pdfTokens, function (pdfToken, index) {
          logger.log('INFO: setPdfTokensToRedis - prepare token array', index + 1, 'of', pdfTokens.length, '-', pdfToken.token, pdfToken.docId, pdfToken.attId, pdfToken.expiryTime);
          tokens[pdfToken.token] = {
            docId: pdfToken.docId,
            attId: pdfToken.attId,
            expiryTime: pdfToken.expiryTime,
            lToken: pdfToken.lToken
          };
        });
        setToRedis('attachmentToken', tokens);
      }
    } else {
      logger.error('ERROR: setPdfTokensToRedis - pdfTokens is not an array');
    }

    logger.log('INFO: setPdfTokensToRedis - end');
    callback && callback();
  });
};

module.exports = {
  getPdfToken: getPdfToken,
  createPdfToken: createPdfToken,
  setPdfTokensToRedis: setPdfTokensToRedis
};