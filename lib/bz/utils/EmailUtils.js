'use strict';

var _axiosWrapper = require('../../../../../app/utilities/axiosWrapper');

var _axiosWrapper2 = _interopRequireDefault(_axiosWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _require = require('./RemoteUtils.js'),
    callApi = _require.callApi;

// the file is included from main project
// e.g. /axa-sg-app/app/u


module.exports.sendEmail = function (email, cb, authToken, webServiceUrl) {

    // TODO: Keep the code
    // callApi('/email/sendEmail', email, cb)


    (0, _axiosWrapper2.default)({
        url: "/email/send",
        data: email,
        method: "POST",
        headers: {
            Authorization: 'Bearer ' + authToken
        },
        webServiceUrl: webServiceUrl
    }).then(cb).catch(function (e) {
        console.log(e);
    });
};