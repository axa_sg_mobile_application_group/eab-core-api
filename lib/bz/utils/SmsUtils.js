'use strict';

var _axiosWrapper = require('../../../../../app/utilities/axiosWrapper');

var _axiosWrapper2 = _interopRequireDefault(_axiosWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _require = require('./RemoteUtils.js'),
    callApi = _require.callApi;

// the file is included from main project
// e.g. /axa-sg-app/app/u


module.exports.sendSms = function (sms, cb, authToken, webServiceUrl) {
    var newSmsObject = {
        mobileNumber: null,
        content: null
    };
    if (sms && sms.mobileNo) {
        newSmsObject.mobileNumber = sms.mobileNo;
    }
    if (sms && sms.smsMsg) {
        newSmsObject.content = sms.smsMsg;
    }

    if (newSmsObject.mobileNumber && newSmsObject.content) {
        (0, _axiosWrapper2.default)({
            url: "/sms/send",
            data: newSmsObject,
            method: "POST",
            headers: {
                Authorization: 'Bearer ' + authToken
            },
            webServiceUrl: webServiceUrl
        }).then(cb).catch(function (e) {
            console.log(e);
        });
    }
};