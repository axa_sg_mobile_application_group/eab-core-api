'use strict';

var logger = global.logger || console;
var _ = require('lodash');

module.exports.getAppSysDocs = function (application, docNamesList, isFACase, isShield) {
  var result = {};
  var source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  var pdfArray = isFACase ? ['proposal', 'appPdf'] : ['fnaReport', 'proposal', 'appPdf'];
  _.forEach(source, function (tabs) {
    _.forEach(tabs, function (section, keySection) {
      if (keySection === 'sysDocs') {
        var sysDocs = isShield ? getSysDocsFiles(section, docNamesList) : getShieldSysDocsFiles(section, docNamesList);
        _.each(sysDocs, function (sysDoc, docKey) {
          // Handle the multiple app form in shield
          if (isShield && (pdfArray.indexOf(docKey) > -1 || docKey.indexOf('appPdf') > -1)) {
            result[docKey] = sysDoc;
          } else if (pdfArray.indexOf(docKey) > -1) {
            result[docKey] = sysDoc;
          }
        });
      }
    });
  });
  return result;
};

module.exports.getAppMandDocs = function (application, docNamesList, isShield) {
  var result = {};
  var source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, function (tabs, cid) {
    var titlePostFix = isShield ? getShieldLAFullNameByCid(application, cid) : '';
    cid = isShield ? cid : '';
    _.forEach(tabs, function (section, keySection) {
      if (keySection === 'mandDocs') {
        Object.assign(result, getMandDocsFiles(section, docNamesList, titlePostFix, cid));
      }
    });
  });
  return result;
};

module.exports.getAppOptionalDocs = function (application, docNamesList, isShield) {
  var result = {};
  var source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, function (tabs, cid) {
    var titlePostFix = isShield ? getShieldLAFullNameByCid(application, cid) : '';
    cid = isShield ? cid : '';
    _.forEach(tabs, function (section, keySection) {
      if (keySection === 'optDoc') {
        Object.assign(result, getMandDocsFiles(section, docNamesList, titlePostFix, cid));
      }
    });
  });
  return result;
};

module.exports.getAppOtherDocs = function (application, isShield) {
  var result = {};
  var source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, function (tabs, cid) {
    var titlePostFix = isShield ? getShieldLAFullNameByCid(application, cid) : '';

    _.forEach(tabs, function (section, keySection) {
      if (['sysDocs', 'mandDocs', 'optDoc'].indexOf(keySection) === -1) {
        Object.assign(result, getOtherDocsFiles(section, titlePostFix));
      }
    });
  });
  return result;
};

module.exports.getApprovalSysDocs = function (application, approval, docNamesList) {
  var result = {};
  if (approval) {
    var sysDocs = {};
    var source = _.get(application, 'supportDocuments.values');
    _.forEach(source, function (tabs) {
      _.forEach(tabs, function (section, keySection) {
        if (keySection === 'sysDocs') {
          Object.assign(sysDocs, getSysDocsFiles(section, docNamesList));
        }
      });
    });
    _.each(approval._attachments, function (att, key) {
      if (sysDocs[key]) {
        result[key] = sysDocs[key];
      }
    });
  }
  return result;
};

module.exports.getApprovalCoeDocs = function (application, approval, docNamesList) {
  var result = {};
  logger.log('INFO: getApprovalCoeDocs');
  var hasCoe = false;
  var pCertOfEndorsement = _.get(application, 'supportDocuments.values.policyForm.mandDocs.pCertOfEndorsement');
  if (_.isArray(pCertOfEndorsement) && pCertOfEndorsement.length > 0) {
    hasCoe = true;
  }
  if (hasCoe) {
    var docKey = 'eapproval_coe';
    result[docKey] = {
      'items': pCertOfEndorsement,
      'title': 'Certificate of Endorsement (COE)',
      'filename': docKey
    };
  }
  return result;
};

module.exports.getNonSysDocs = function (application, excludeFilter) {
  var result = [];
  var source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, function (tabs) {
    _.forEach(tabs, function (section, keySection) {
      if (keySection === 'otherDoc') {
        _.forEach(_.get(section, 'values'), function (documents, keyDocument) {
          _.forEach(documents, function (doc, index) {
            result.push(doc);
          });
        });
      } else if (keySection !== 'sysDocs') {
        _.forEach(section, function (attList, attKey) {
          _.forEach(attList, function (att, index) {
            if (excludeFilter.indexOf(att.fileType) < 0) {
              result.push(att);
            }
          });
        });
      }
    });
  });
  return result;
};

var getShieldSysDocsFiles = function getShieldSysDocsFiles(section, docsNameList) {
  var result = {};
  logger.log('INFO: getCurrentFilesList -- getSysDocsFiles');
  _.forEach(section, function (file, keyFile) {
    result[keyFile] = {
      'items': [file],
      'title': file.title || _.get(docsNameList, keyFile + '.name'),
      'filename': keyFile.indexOf('appPdf') > -1 ? file.title : _.get(docsNameList, keyFile + '.filename')
    };
  });
  return result;
};

var getSysDocsFiles = function getSysDocsFiles(section, docsNameList) {
  var result = {};
  logger.log('INFO: getCurrentFilesList -- getSysDocsFiles');
  _.forEach(section, function (file, keyFile) {
    result[keyFile] = {
      'items': [file],
      'title': file.title || _.get(docsNameList, keyFile + '.name'),
      'filename': _.get(docsNameList, keyFile + '.filename')
    };
  });
  return result;
};

// Mandatory Document and Optional Document have same structure, use this same function
var getMandDocsFiles = function getMandDocsFiles(section, docsNameList, titlePostFix, cid) {
  var result = {};
  logger.log('INFO: getCurrentFilesList -- getMandDocsFiles');
  _.forEach(section, function (document, keyDocument) {
    if (!_.isEmpty(document) && keyDocument !== 'pCertOfEndorsement') {
      result['' + keyDocument + cid] = {
        'items': document,
        'title': _.get(docsNameList, keyDocument + '.name') + titlePostFix,
        'filename': _.get(docsNameList, keyDocument + '.filename') + titlePostFix
      };
    }
  });
  return result;
};

var getOtherDocsFiles = function getOtherDocsFiles(section, titlePostFix) {
  var result = {};
  logger.log('INFO: getCurrentFilesList -- getOtherDocsFiles');
  _.forEach(_.get(section, 'values'), function (document, keyDocument) {
    result[keyDocument] = {
      'items': document,
      'title': _.get(_.find(_.get(section, 'template'), function (tmplItem) {
        return tmplItem.id === keyDocument;
      }), 'title') + titlePostFix
    };
  });
  return result;
};

var getShieldLAFullNameByCid = function getShieldLAFullNameByCid(application, cid) {
  cid = cid === 'proposer' || cid === 'policyForm' ? _.get(application, 'applicationForm.values.proposer.personalInfo.cid') : cid;
  return ' (' + _.get(application, 'quotation.insureds.' + cid + '.iFullName') + ')';
};