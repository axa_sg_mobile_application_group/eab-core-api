'use strict';

var _ = require('lodash');
var utils = require('./utils');
var math = require('./math2.js');

var QuotValid = function QuotValid(context, quotDriver) {
    var _this = this;

    // let runFunc = utils.runFunc,
    var getCurrency = utils.getCurrency,
        debug = utils.debug;
    this.context = context;
    this.quotDriver = quotDriver;

    var ErrCode = {
        missingSA: 'js.err.sa_missing',
        wrongClassType: 'js.err.wrong_class_type',
        wrongWPClassFix: 'js.err.wrong_wp_class_fix',
        wrongWPClassBP: 'js.err.wrong_wp_class_basic',
        invalidSumInsured: 'js.err.invalid_sum_insured',
        invalidPremium: 'js.err.invalid_premium',
        invalidSAMult: 'js.err.invalid_sa_mult',
        invalidPremMult: 'js.err.invalid_prem_mult',
        invalidSADecimal: 'js.err.invalid_sa_decimal',
        invalidPremDecimal: 'js.err.invalid_prem_decimal',
        invalidBenelimMin: 'js.err.invalid_benelim_min',
        invalidBenelimMax: 'js.err.invalid_benelim_max',
        invalidBenelimMinMax: 'js.err.invalid_benelim_min_max',
        invalidPremlimMin: 'js.err.invalid_premlim_min',
        invalidPremlimMax: 'js.err.invalid_premlim_max',
        invalidPremlimMinMax: 'js.err.invalid_premlim_min_max',
        invalidPremlimMinMulti: 'js.err.invalid_premlim_min_multi',
        invalidPremlimMaxMulti: 'js.err.invalid_premlim_max_multi',
        invalidPremlimMinMaxMulti: 'js.err.invalid_premlim_min_max_multi',
        invalidCoexistShould: 'js.err.invalid_coexist_should',
        invalidCoexistShouldNot: 'js.err.invalid_coexist_shouldnot',
        invalidCoexistRequire: 'js.err.invalid_coexist_require',
        invalidDA: 'js.err.invalid_da',
        overBasicSARange: 'js.err.basicSA_over_range',
        backDatebeyondToday: 'js.err.backDatebeyondToday',
        backDateBeforeEffDate: 'js.err.backDateBeforeEffDate',
        backDateOlderThanSixMonths: 'js.err.backDateOlderThanSixMonths'

        // validate benefit limit
    };this.validateSALimit = function (quotation, planInfo, planDetail) {
        if (planDetail && planInfo && planDetail.inputConfig) {
            if (planDetail.inputConfig.benlim && _.isNumber(planInfo.sumInsured)) {
                var _planDetail$inputConf = planDetail.inputConfig.benlim,
                    min = _planDetail$inputConf.min,
                    max = _planDetail$inputConf.max;
                var covCode = planInfo.covCode,
                    sumInsured = planInfo.sumInsured;


                if (min && min > sumInsured || max && max < sumInsured) {
                    var err = {
                        covCode: covCode,
                        msgPara: [],
                        code: ''
                    };
                    var errCode = '';
                    if (min && max) {
                        errCode = ErrCode.invalidBenelimMinMax;
                        err.msgPara.push(planInfo.covName);
                        err.msgPara.push(getCurrency(min, '$', 2));
                        err.msgPara.push(getCurrency(max, '$', 2));
                    } else if (min) {
                        errCode = ErrCode.invalidBenelimMin;
                        err.msgPara.push(getCurrency(min, '$', 2));
                    } else if (max) {
                        errCode = ErrCode.invalidBenelimMax;
                        err.msgPara.push(getCurrency(max, '$', 2));
                    }
                    err.msgPara.push(planDetail.inputConfig.premInput);

                    err.code = errCode;
                    err.msgPara.push(sumInsured);
                    _this.context.addError(err);
                }
            }
        } else {
            _this.context.addError({
                code: "Internal Error (validateSALimit)"
            });
        }
    };

    // validate premium limit
    this.validatePreimumLimit = function (quotation, planInfo, planDetail) {
        if (planDetail && planInfo && planDetail.inputConfig) {
            if (planDetail.inputConfig.premlim && _.isNumber(planInfo.premium)) {
                var _planDetail$inputConf2 = planDetail.inputConfig.premlim,
                    min = _planDetail$inputConf2.min,
                    max = _planDetail$inputConf2.max;
                var covCode = planInfo.covCode,
                    premium = planInfo.premium;


                if (min && min > premium || max && max < premium) {
                    var err = {
                        covCode: covCode,
                        msgPara: [],
                        code: ''
                    };
                    var errCode = '';
                    if (min && max) {
                        errCode = ErrCode.invalidPremlimMinMax;
                        err.msgPara.push(planInfo.covName);
                        err.msgPara.push(getCurrency(min, '$', 2));
                        err.msgPara.push(getCurrency(max, '$', 2));
                    } else if (min) {
                        errCode = ErrCode.invalidPremlimMin;
                        err.msgPara.push(getCurrency ? getCurrency(min, '$', 2) : min);
                    } else if (max) {
                        errCode = ErrCode.invalidPremlimMax;
                        err.msgPara.push(getCurrency ? getCurrency(max, '$', 2) : max);
                    }
                    err.code = errCode;
                    err.msgPara.push(premium);
                    _this.context.addError(err);
                }
            }
        } else {
            _this.context.addError({
                code: "Internal Error (validatePreimumLimit)"
            });
        }
    };

    var getDecimalPlace = function getDecimalPlace(num) {
        var strs = num.toString().split('.');
        return strs.length > 1 ? strs[1].length : 0;
    };

    // validate SA Multiple
    this.validateSAMultiple = function (quotation, planInfo, planDetail) {
        var saInput = _.find(planDetail.saInput, function (config) {
            return config.ccy === quotation.ccy;
        });
        if (saInput && _.isNumber(planInfo.sumInsured)) {
            if (_.isNumber(saInput.factor) && saInput.factor !== 0) {
                if (math.mod(planInfo.sumInsured, saInput.factor) !== 0) {
                    _this.context.addError({
                        covCode: planInfo.covCode,
                        code: ErrCode.invalidSAMult,
                        msgPara: [planInfo.covName, getCurrency(saInput.factor, '$', 0), planInfo.sumInsured]
                    });
                }
            } else if (_.isNumber(saInput.decimal) && saInput.decimal) {
                if (getDecimalPlace(planInfo.sumInsured) > saInput.decimal) {
                    _this.context.addError({
                        covCode: planInfo.covCode,
                        code: ErrCode.invalidSADecimal,
                        msgPara: [planInfo.covName, saInput.decimal, planInfo.sumInsured]
                    });
                }
            }
        }
    };

    this.validatePremiumMultiple = function (quotation, planInfo, planDetail) {
        var premInput = _.find(planDetail.premInput, function (config) {
            return config.ccy === quotation.ccy;
        });
        if (premInput && _.isNumber(planInfo.premium)) {
            if (_.isNumber(premInput.factor) && premInput.factor !== 0) {
                if (math.mod(planInfo.premium, premInput.factor) !== 0) {
                    _this.context.addError({
                        covCode: planInfo.covCode,
                        code: ErrCode.invalidPremMult,
                        msgPara: [planInfo.covName, getCurrency(premInput.factor, '$', 0), planInfo.premium]
                    });
                }
            } else if (_.isNumber(premInput.decimal) && premInput.decimal) {
                if (getDecimalPlace(planInfo.premium) > premInput.decimal) {
                    _this.context.addError({
                        covCode: planInfo.covCode,
                        code: ErrCode.invalidPremDecimal,
                        msgPara: [planInfo.covName, premInput.decimal, planInfo.premium]
                    });
                }
            }
        }
    };

    // validate plan coexist rule
    this.validateCoexist = function (quotation, planInfos, planDetails) {
        var basicPlanDetail = planDetails[quotation.baseProductCode];
        // let planInfos = quotation.plans;

        if (basicPlanDetail.coexist) {
            var _loop = function _loop(c) {
                var coex = basicPlanDetail.coexist[c];
                if ((!coex.country || coex.country == '*' || coex.country == quotation.iResidence) && (!coex.dealerGroup || coex.dealerGroup == '*' || coex.dealerGroup == quotation.agent.dealerGroup) && (!coex.gender || coex.gender == '*' || coex.gender == quotation.iGender) && (!coex.smoke || coex.smoke == '*' || coex.smoke == quotation.iSmoke) && (!coex.ageFr || coex.ageFr <= quotation.iAge) && (!coex.ageTo || coex.ageTo >= quotation.iAge)) {

                    for (var i = 1; i < planInfos.length; i++) {
                        var gA = coex.groupA.indexOf(planInfos[i].covCode);
                        for (var j = 1; j < planInfos.length; j++) {
                            if (i == j) continue;
                            var gB = coex.groupB.indexOf(planInfos[j].covCode);

                            if (gA >= 0 && gB >= 0 && coex.shouldCoExist === 'N') {
                                _this.context.addError({
                                    code: ErrCode.invalidCoexistShouldNot,
                                    msgPara: [planInfos[i].covName, planInfos[j].covName]
                                });
                            }

                            if ((gA >= 0 && gB < 0 || gB >= 0 && gA < 0) && coex.shouldCoExist === 'Y') {
                                _this.context.addError({
                                    code: ErrCode.invalidCoexistShould,
                                    msgPara: [planInfos[i].covName, planInfos[j].covName]
                                });
                            }
                        }
                    }

                    if (coex.shouldCoExist === 'REQUIRE') {
                        _.each(planInfos, function (plan) {
                            if (coex.groupB.indexOf(plan.covCode) > -1) {
                                if (!_.find(planInfos, function (p) {
                                    return coex.groupA.indexOf(p.covCode) > -1;
                                })) {
                                    _this.context.addError({
                                        code: ErrCode.invalidCoexistRequire,
                                        msgPara: [plan.covName]
                                    });
                                }
                            }
                        });
                    }
                }
            };

            for (var c in basicPlanDetail.coexist) {
                _loop(c);
            }
        }
    };

    // validateDA = function(quotation, planInfos, planDetails, validate, err) {
    //     if (!err) err = {};
    //     if (!err.msgPara) err.msgPara = [];
    //     // validate Death Aggregation
    //     if (validate.deathAggregate) {
    //         for (let k = 0; k < validate.deathAggregate.length; k++) {
    //             let deathAggregate = validate.deathAggregate[k];
    //             if (deathAggregate.ccy == quotation.ccy && deathAggregate.ageFr <= quotation.iAge && deathAggregate.ageTo >= quotation.iAge) {
    //                 let daLim = deathAggregate.maxSumIns;
    //                 let daSum = 0;
    //                 for (let j = 0; j < planInfos.length; j++) {
    //                     let planInfo = planInfos[j];
    //                     let planDetail = planDetails[planInfo.docId];
    //                     if (planDetail.deathAggregateSeq) {
    //                         let para = {
    //                             sumInsured: planInfo.sumInsured
    //                         };
    //                         let riskSA = runFunc(planDetail.deathAggregateFunc, planDetail, para);
    //                         daSum = daSum + riskSA;
    //                     }
    //                 }
    //                 if (daSum > daLim) {
    //                     err.isInvalidDA = true;
    //                 }
    //             }
    //         }
    //     }
    // };

    this.validateMandatoryFields = function (quotation, planInfo, planDetail) {
        if (planDetail.inputConfig) {
            var config = planDetail.inputConfig;
            if (config.canEditPolicyTerm) {
                if (planInfo.policyTerm == null) {
                    _this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'policyTerm'
                    });
                }
            }
            if (config.canEditPremTerm) {
                if (planInfo.premTerm == null) {
                    _this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'premTerm'
                    });
                }
            }
            if (config.canEditClassType) {
                if (planInfo.covClass == null) {
                    _this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'covClass'
                    });
                }
            }

            if (config.canEditSumAssured) {
                if (!_.isNumber(planInfo.sumInsured)) {
                    _this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'sumInsured'
                    });
                }
            }
            if (config.canEditPremium) {
                if (!_.isNumber(planInfo.premium)) {
                    _this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'premium'
                    });
                }
            }
        }
    };

    this.validatePolicyOptions = function (quotation, bpDetail) {
        var _this2 = this;

        if (bpDetail && bpDetail.inputConfig) {
            _.each(bpDetail.inputConfig.policyOptions, function (opt) {
                if (opt.errorMsg) {
                    _this2.context.addError({
                        type: 'policyOption',
                        key: opt.id,
                        msg: opt.errorMsg
                    });
                }
                if (opt.mandatory === 'Y' && !quotation.policyOptions[opt.id]) {
                    _this2.context.addError({
                        type: 'mandatory',
                        key: opt.id
                    });
                }
            });
        }
    };

    this.validateFundAlloc = function (quotation, bpDetail) {
        if (bpDetail && bpDetail.fundInd === 'Y') {
            var hasFundError = !quotation.fund;
            if (hasFundError || _.reduce(quotation.fund.funds, function (sum, fund) {
                return sum + (fund.alloc || 0);
            }, 0) !== 100) {
                hasFundError = true;
            }
            if (hasFundError || bpDetail.inputConfig.topUpSelect && _.reduce(quotation.fund.funds, function (sum, fund) {
                return sum + (fund.topUpAlloc || 0);
            }, 0) !== 100) {
                hasFundError = true;
            }
            if (hasFundError) {
                this.context.addError({
                    type: 'mandatory',
                    key: 'fund'
                });
            }
        }
    };

    this.validateGlobal = function (quotation, planInfos, planDetails) {
        var bpDetail = planDetails[quotation.baseProductCode];
        // validateDA(quotation, planInfos, planDetails, validate, err);
        this.validateCoexist(quotation, planInfos, planDetails);
        this.validatePolicyOptions(quotation, bpDetail);
        this.validateFundAlloc(quotation, bpDetail);
    };

    this.validatePlanAfterCalc = function (quotation, planInfo, planDetail) {
        _this.validateSALimit(quotation, planInfo, planDetail);
        _this.validatePreimumLimit(quotation, planInfo, planDetail);
        _this.validateSAMultiple(quotation, planInfo, planDetail);
        _this.validatePremiumMultiple(quotation, planInfo, planDetail);
    };

    this.validatePlanBeforeCalc = function (quotation, planInfo, planDetails) {
        var basicPlanInfo = quotation.plans[0];
        var planDetail = planDetails[planInfo.covCode];

        _this.validateMandatoryFields(quotation, planInfo, planDetail);

        // validateSAMultiple(quotation, planInfo, planDetail);
        // validateSALimit(quotation, planInfo, planDetail);
        // validatePreimumLimit(quotation, planInfo, planDetail);

        var basicPlanDetails = planDetails[basicPlanInfo.covCode];
        var riderList = basicPlanDetails.inputConfig.riderList;
        // let planDetail = planDetails[planInfo.covCode];

        // for rider,
        if (planDetail.planType == "R") {
            // check for attachable rider list rule on basic plan
            for (var r in riderList) {
                var rider = riderList[r];
                if (rider.covCode == planInfo.covCode) {
                    // check if basic SA over the limited range
                    if (_.isNumber(rider.basicSAMin) && rider.basicSAMin > basicPlanInfo.sumInsured || _.isNumber(rider.basicSAMax) && rider.basicSAMax < basicPlanInfo.sumInsured) {
                        _this.context.addError({
                            covCode: planInfo.covCode,
                            code: ErrCode.overBasicSARange,
                            msgPara: [planDetail.covName, rider.basicSAMin, rider.basicSAMax]
                        });
                    }

                    // check class rule for follow Basic plan
                    if (rider.classRule && rider.classRule == "B" && basicPlanInfo.classType) {
                        if (planInfo.classType != basicPlanInfo.classType) {
                            _this.context.addError({
                                covCode: planInfo.covCode,
                                code: ErrCode.wrongClassType,
                                msgPara: [planDetail.covName, basicPlanInfo.covName]
                            });
                        }
                    }

                    // check class rule for fix type
                    if (rider.classRule && rider.classRule == "F" && rider.classFix) {
                        if (planInfo.classType != rider.classFix) {
                            _this.context.addError({
                                covCode: planInfo.covCode,
                                code: ErrCode.wrongClassType,
                                msgPara: [planDetail.covName, rider.classFix]
                            });
                        }
                    }

                    break;
                }
            }
        }
    };
};

module.exports = QuotValid;