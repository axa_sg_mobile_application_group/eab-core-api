'use strict';

var utils = require('./utils');

var QuotContext = function QuotContext() {
  var planErrors = {};
  var policyOptionErrors = {};
  var mandatoryErrors = {};
  var otherErrors = [];

  var addError = function addError(error) {
    if (error.type === 'policyOption' && error.key) {
      policyOptionErrors[error.key] = error;
    } else if (error.type === 'mandatory' && error.key) {
      if (!mandatoryErrors[error.key]) {
        mandatoryErrors[error.key] = [];
      }
      mandatoryErrors[error.key].push(error);
    } else if (error.covCode) {
      if (!planErrors[error.covCode]) {
        planErrors[error.covCode] = [];
      }
      planErrors[error.covCode].push(error);
    } else {
      otherErrors.push(error);
    }
  };

  var returnResult = function returnResult(result) {
    var resultObj = Object.assign({}, result);
    resultObj.errors = {
      planErrors: planErrors,
      policyOptionErrors: policyOptionErrors,
      mandatoryErrors: mandatoryErrors,
      otherErrors: otherErrors
    };
    return utils.returnResult(resultObj);
  };

  this.addError = addError;
  this.returnResult = returnResult;
};

module.exports = QuotContext;