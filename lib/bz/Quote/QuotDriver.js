'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _ = require('lodash');

var QuotValid = require('./QuotValid');
var QuotCalc = require('./QuotCalc');
var QuotContext = require('./QuotContext');
var math = require('./math2.js');

var DateUtils = require('../../common/DateUtils');

var _require = require('../../common/ProductUtils'),
    getAgeByMethod = _require.getAgeByMethod,
    checkEntryAge = _require.checkEntryAge,
    isCrossBorder = _require.isCrossBorder;

var parseDate = DateUtils.parseDate,
    formatDate = DateUtils.formatDate,
    getAges = DateUtils.getAges;

var _require2 = require('./utils'),
    getCurrency = _require2.getCurrency,
    getCurrencyByCcy = _require2.getCurrencyByCcy,
    debug = _require2.debug;

var logger = global.logger;
var FormulaParser = require('hot-formula-parser').Parser;
var parser = new FormulaParser();

var runExcelFunc = function runExcelFunc() {
  try {
    var funcStr = '';

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _.forEach(args, function (arg, index) {
      if (index === 0) {
        funcStr = arg + '(';
      } else {
        var varId = 'VAR_' + index;
        parser.setVariable(varId, arg);
        funcStr += (index > 1 ? ';' : '') + varId;
      }
    });
    funcStr += ')';

    var resultObj = parser.parse(funcStr);
    return resultObj.result;
  } catch (e) {
    return null;
  }
};

var QuotDriver = function QuotDriver() {
  var _this = this;

  this.context = new QuotContext();
  this.quotValid = new QuotValid(this.context, this);
  this.quotCalc = new QuotCalc(this.context, this);

  this.runFunc = function (fnStr, para1, para2, para3, para4) {
    var quotDriver = _this;
    var quotValid = _this.quotValid;
    var quotCalc = _this.quotCalc;
    var dateUtils = DateUtils;

    if (typeof fnStr == 'string' && fnStr != 'undefined') {
      try {
        var fn = eval('(' + fnStr + ')');
        if (typeof fn == 'function') {
          var res = fn(para1, para2, para3, para4);
          return res;
        } else {
          debug.errFn = fn;
        }
      } catch (ex) {
        logger.log('ERROR: QuotDriver.runfunc; ' + ex);
        if (ex.errCode) {
          throw ex;
        } else {
          debug.ex = ex.message || ex;
          debug.exStr = ex.stack;
          debug.funcStr = fnStr;
        }
      }
    } else if (fnStr && typeof fnStr == 'function') {
      return fnStr(para1, para2, para3, para4);
    }
    return null;
  };

  var runFunc = this.runFunc;

  this.filterProducts = function (suitability, products, agent, profiles, extraParams) {
    var countryEligibility = global.optionsMap.countryEligibility;
    var productList = products;
    var filterCovCodeEsc = countryEligibility.filteredPlan;
    var filterCovCodeVUL = ["PVTVUL", "PVLVUL"];
    var LAresidency = "";
    LAresidency = profiles[1].residenceCity ? LAresidency = profiles[1].residenceCity : LAresidency = profiles[1].residenceCountry;
    var declinedEscList = countryEligibility.residencency.ESC ? countryEligibility.residencency.ESC : null;
    var declinedVulList = countryEligibility.residencency.VUL ? countryEligibility.residencency.VUL : null;
    var allowProducts = _.get(countryEligibility, 'allowProductsByResidencyCity');

    if (suitability.checkAgentForProductFunc) {
      var result = _this.runFunc(suitability.checkAgentForProductFunc, suitability, agent, productList, extraParams);
      if (result && result.error) {
        return result;
      } else {
        productList = result.productList;
      }
    }
    if (suitability.checkClientForProductFunc) {
      var _result = _this.runFunc(suitability.checkClientForProductFunc, suitability, profiles, productList, extraParams);
      if (_result && _result.error) {
        return _result;
      } else {
        productList = _result.productList;
      }
    }

    for (var i in declinedEscList) {
      if (LAresidency == declinedEscList[i]) {
        productList = _.filter(productList, function (product) {
          return !(filterCovCodeEsc.indexOf(product.covCode) > -1);
        });
        break;
      }
    }

    for (var j in declinedVulList) {
      if (LAresidency == declinedVulList[j]) {
        productList = _.filter(productList, function (product) {
          return product.covCode !== filterCovCodeVUL[0];
        });
        productList = _.filter(productList, function (product) {
          return product.covCode !== filterCovCodeVUL[1];
        });
        break;
      }
    }

    _.each(allowProducts, function (ruleConfig) {
      var allowBaseProductCode = _.get(ruleConfig, 'baseProductCode', []);
      var affectedResidencyCity = _.get(ruleConfig, 'residencyCity', []);
      // Always display Sg plans
      var sgPlans = _.get(ruleConfig, 'sgPlanDisplayAsAlways', []);

      if (affectedResidencyCity.indexOf(LAresidency) > -1) {
        productList = _.filter(productList, function (product) {
          return allowBaseProductCode.indexOf(product.covCode) > -1 || sgPlans.indexOf(product.covCode) > -1;
        });
      }
    });

    return { productList: productList };
  };

  this.getProducts = function (suitability, products, agent, profiles, fnaInfo, extraParams) {

    var result = _this.filterProducts(suitability, products, agent, profiles, extraParams);
    if (result.error) {
      return { error: result.message };
    }
    var productList = result.productList;
    if (fnaInfo) {
      productList = _.filter(productList, function (product) {
        return _this.checkProdSuitForView(suitability, fnaInfo, product, profiles) === true;
      });
    }
    productList = _.filter(productList, function (product) {
      return _this.checkHidden(product, agent, extraParams.quickQuote) === true;
    });
    return { productList: productList };
  };

  this.groupProducts = function (suitability, fnaInfo, products) {
    var fna = fnaInfo.fna;
    var aspects = fna && fna.aspects && fna.aspects.split(',');
    var groups = {};
    _.each(products, function (product) {
      var covCode = product.covCode;
      var suitableProd = suitability.products && suitability.products[covCode];
      if (suitableProd && suitableProd.needs) {
        var keys = _.filter(suitableProd.needs, function (n) {
          return aspects.indexOf(n) > -1;
        });
        var groupKey = keys.length > 0 ? keys.join(',') : 'otherProds';
        if (!groups[groupKey]) {
          groups[groupKey] = {
            info: keys,
            products: []
          };
        }
        groups[groupKey].products.push(covCode);
      }
    });
    return groups;
  };

  /**
   * Checks whether the product can be viewed by the FNA info.
   * Returns true if product is viewable, or error message to prompt.
   *
   * @param {*} suitability suitability json
   * @param {*} fnaInfo the fna information holder json
   * @param {*} product the product json
   */
  this.checkProdSuitForView = function (suitability, fnaInfo, product, profiles) {
    if (suitability && suitability.checkProdSuitForViewFunc) {
      return runFunc(suitability.checkProdSuitForViewFunc, suitability, fnaInfo, product, profiles);
    }
    return true;
  };

  /**
   * Checks whether the product can be viewed in Quick Quote/ Products.
   * Returns true if product is viewable, or error message to prompt.
   *
   * @param {*} suitability suitability json
   * @param {*} fnaInfo the fna information holder json
   * @param {*} product the product json
   */
  this.checkHidden = function (product, agent, quickQuote) {
    if (quickQuote) {
      if (product.prodFeature && product.prodFeature.en) {
        return !product.prodFeature.en.includes('quickQuoteHidden');
      }
    } else {
      if (product.prodFeature && product.prodFeature.en && agent.channel.type !== 'FA') {
        return !product.prodFeature.en.includes('productHidden');
      }
    }
    return true;
  };

  /**
   * Checks whether the product can be allowed for quotation by the FNA info.
   * Returns true if product can be quoted, or error message to prompt.
   *
   * @param {*} suitability suitability json
   * @param {*} fnaInfo the fna information holder json
   * @param {*} product the product json
   */
  this.checkProdSuitForQuot = function (suitability, fnaInfo, product) {
    if (suitability && suitability.checkProdSuitForQuotFunc) {
      return runFunc(suitability.checkProdSuitForQuotFunc, suitability, fnaInfo, product);
    }
    return true;
  };

  this.validateProdSuitability = function (suitability, fnaInfo, product, profiles) {
    return _this.checkProdSuitForView(suitability, fnaInfo, product, profiles) === true && _this.checkProdSuitForQuot(suitability, fnaInfo, product, profiles) === true;
  };

  /**
   * Checks whether the quotation inputs are allowed by the FNA info.
   * Returns true if allowed to proceed to proposal, or error message to prompt.
   *
   * @param {*} suitability
   * @param {*} fnaInfo
   * @param {*} quotation
   */
  this.validateQuotSuitability = function (suitability, fnaInfo, quotation) {
    if (suitability && suitability.checkQuotSuitabilityFunc) {
      return runFunc(suitability.checkQuotSuitabilityFunc, suitability, fnaInfo, quotation);
    }
    return true;
  };

  this.calculateQuotation = function (quotationJson, planDetailsJson) {
    var resultObj = {};
    try {
      var quotation = 0,
          planDetails = 0;
      // Initialize variables based on the input parameters
      if (typeof quotationJson === 'string') {
        if (JSON && typeof JSON.parse === 'function') {
          quotation = JSON.parse(quotationJson);
          planDetails = JSON.parse(planDetailsJson);
        } else {
          eval('quotation = ' + quotationJson);
          eval('planDetails = ' + planDetailsJson);
        }
      } else if ((typeof quotationJson === 'undefined' ? 'undefined' : _typeof(quotationJson)) === 'object') {
        quotation = quotationJson;
        planDetails = _.cloneDeep(planDetailsJson);
      }

      if (quotation && planDetails) {
        var bpDetail = planDetails[quotation.baseProductCode];

        var riskCommenDate = quotation.isBackDate === 'Y' ? parseDate(quotation.riskCommenDate) : new Date();
        quotation.iAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(quotation.iDob));
        quotation.pAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(quotation.pDob));
        quotation.iAttainedAge = getAgeByMethod('current', riskCommenDate, parseDate(quotation.iDob));
        quotation.pAttainedAge = getAgeByMethod('current', riskCommenDate, parseDate(quotation.pDob));
        quotation.iCrossBorderWithoutPass = isCrossBorder(quotation, null, false, false);
        quotation.pCrossBorderWithoutPass = isCrossBorder(quotation, null, true, false);
        quotation.isCrossBorderWithoutPass = quotation.pCrossBorderWithoutPass || quotation.iCrossBorderWithoutPass;
        quotation.riskCommenDate = formatDate(riskCommenDate);

        var retVal = _this.validateQuotation(quotation, planDetails);
        resultObj.quotation = retVal.quotation || quotation;

        planDetails = retVal.planDetails || planDetails;
        if (planDetails) {
          resultObj.inputConfigs = {};
          _.each(quotation.plans, function (plan) {
            resultObj.inputConfigs[plan.covCode] = planDetails[plan.covCode].inputConfig;
          });
        }
      } else {
        resultObj.error = {
          message: 'Quotation or Plan details missing.'
        };
      }
    } catch (ex) {
      debug.exception = {
        message: ex.message,
        stack: ex.stack
      };
    }
    return _this.context.returnResult(resultObj);
  };

  // generate illurstration and report data
  /*  parameter:
   *      quotation               : Object, quotation
   *      planDetails             : Object, map of covCode vs Product Configuration
   *      extraPara               : Object
   *          BI Options          : Object, key value map of BI options
   *          illustProps         : Object, map of covCode vs Illustration properties and rates
   *          templateFuncs       : Object, map of covCode vs PDF template formulas ( prepareReportDataFunc and prepareIlluDataFunc )
   *          requireReportData   : bool, indicate whether need to generate report data
   *  return: Object
   *      quotation               : quotation with illursation data
   *      reportData              : report data for Proposal
   */
  this.generateIllustrationData = function (quotationJson, planDetailsJson, extraParaJson) {
    var resultObj = {};
    var startTime = new Date();

    try {
      // Initialize variables based on the input parameters
      var quotation = 0,
          planDetails = 0,
          extraPara = 0;

      if (typeof quotationJson == 'string') {
        if (JSON && typeof JSON.parse === 'function') {
          quotation = JSON.parse(quotationJson);
          planDetails = JSON.parse(planDetailsJson);
          extraPara = JSON.parse(extraParaJson);
          // let globalValidation = JSON.parse(validationJson);
        } else {
          eval('quotation = ' + quotationJson);
          eval('planDetails = ' + planDetailsJson);
          eval('extraPara = ' + extraParaJson);
          // eval('let globalValidation = ' + validationJson);
        }
      } else if ((typeof quotationJson === 'undefined' ? 'undefined' : _typeof(quotationJson)) == 'object') {
        quotation = quotationJson;
        planDetails = planDetailsJson;
        extraPara = extraParaJson;
        // let globalValidation = (validationJson);
      }

      resultObj.error = null;

      if (quotation && planDetails) {
        try {
          var subStartTime = new Date();

          var _retVal = _this.validateQuotation(quotation, planDetails);
          quotation = _retVal.quotation || quotation;
          planDetails = _retVal.planDetails || planDetails;

          var subElapsedTimeMs = new Date() - subStartTime;
          logger.log("EXEC: generateIllustrationData | validateQuotation | Elapsed " + subElapsedTimeMs + "ms");
        } catch (ex) {
          ex.message = 'warning: failure to recal the quotation';
          throw ex;
        }

        // generate illustration
        // let illustrationList = [];
        var subStartTime2 = new Date();
        var retVal = _this.generateIllustration(quotation, planDetails, extraPara);
        var subElapsedTimeMs2 = new Date() - subStartTime2;
        logger.log("EXEC: generateIllustrationData | generateIllustration | Elapsed " + subElapsedTimeMs2 + "ms");

        if (retVal.isIllustrationGenerated) {
          resultObj.illustrations = retVal.illustrations;
          // resultObj.quotation = quotation;

          if (extraPara.requireReportData) {
            extraPara.illustrations = retVal.illustrations;
            if (retVal.warningMsg) {
              extraPara.warningMsg = retVal.warningMsg;
            }
            try {
              // if (extraPara.prepareDataFunc) {
              //   resultObj.reportData = runFunc(extraPara.prepareDataFunc, quotation, planDetails, extraPara);
              // } else {
              var subStartTime3 = new Date();
              resultObj.reportData = _this.prepareReportData(quotation, planDetails, extraPara);
              var subElapsedTimeMs3 = new Date() - subStartTime3;
              logger.log("EXEC: generateIllustrationData | prepareReportData | Elapsed " + subElapsedTimeMs3 + "ms");
              // }
            } catch (ex) {
              ex.message = 'Failed to prepare data for PDF';
              throw ex;
            }
          }
        } else {
          // Failed to generate illustration
          var ex = {
            message: 'Failed to generate PDF due to generating illustration failure'
          };
          debug.err = retVal;
          throw ex;
        }
      }
    } catch (ex) {
      debug.exception = {
        message: ex.message,
        stack: ex.stack
      };
    }

    var elapsedTimeMs = new Date() - startTime;
    logger.log("EXEC: generateIllustrationData | Elapsed " + elapsedTimeMs + "ms");

    return _this.context.returnResult(resultObj);
  };

  this.assignExtraPropertiesToPlanDetail = function (planDetail, quotation, planDetails) {
    planDetail.inputConfig = {};

    if (planDetail.planInd === 'B') {
      if (planDetail.formulas && planDetail.formulas.prepareQuotConfigs) {
        _this.runFunc(planDetail.formulas.prepareQuotConfigs, quotation, planDetail, planDetails);
      } else {
        _this.prepareQuotConfigs(quotation, planDetail, planDetails);
      }
    }

    if (planDetail.formulas && planDetail.formulas.preparePolicyOptions) {
      _this.runFunc(planDetail.formulas.preparePolicyOptions, planDetail, quotation, planDetails);
    } else {
      _this.preparePolicyOptions(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareClassConfig) {
      _this.runFunc(planDetail.formulas.prepareClassConfig, planDetail, quotation, planDetails);
    } else {
      _this.prepareClassConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareTermConfig) {
      _this.runFunc(planDetail.formulas.prepareTermConfig, planDetail, quotation, planDetails);
    } else {
      _this.prepareTermConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareSAPremConfig) {
      _this.runFunc(planDetail.formulas.prepareSAPremConfig, planDetail, quotation, planDetails);
    } else {
      _this.prepareAmountConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.planInd === 'B') {
      if (planDetail.formulas && planDetail.formulas.prepareAttachableRider) {
        _this.runFunc(planDetail.formulas.prepareAttachableRider, planDetail, quotation, planDetails);
      } else {
        _this.prepareAttachableRider(planDetail, quotation, planDetails);
      }
    }

    // let riderConfig =  {
    //   type: "setup",
    //   residencency: ["T180", "T174", "T206"],
    //   exeptionProducts: ["ASIM", "BAA"]
    // };
    // // TODO: update optionsMap 
    // //global.optionsMap.riderEligibility; 
    var riderConfig = global.optionsMap.riderEligibility;
    var iResidence = quotation.iResidenceCity || quotation.iResidence;
    var prodCode = quotation.baseProductCode;
    var declinedResidence = riderConfig.residencency ? riderConfig.residencency : null;
    var declinedEci = _.get(riderConfig, 'declinceECIRider.residency', null);
    var declinedEciRiderCovCode = _.get(riderConfig, 'declinceECIRider.riderCovCode', null);
    var exceptionProdList = riderConfig.exeptionProducts ? riderConfig.exeptionProducts : null;
    var exceptionProd = false;

    for (var i in exceptionProdList) {
      if (prodCode == exceptionProdList[i]) {
        exceptionProd = true;
      }
    }

    if (!exceptionProd) {
      for (var i in declinedResidence) {
        if (iResidence == declinedResidence[i]) {
          if (planDetail.inputConfig && planDetail.inputConfig.riderList && planDetail.inputConfig.riderList.length) {
            planDetail.inputConfig.riderList = planDetail.inputConfig.riderList.filter(function (product) {
              return product.autoAttach === 'Y' && product.compulsory === 'Y';
            });
          }
        }
      }
    }

    for (var i in declinedEci) {
      if (iResidence == declinedEci[i]) {
        if (planDetail.inputConfig && planDetail.inputConfig.riderList && planDetail.inputConfig.riderList.length) {
          planDetail.inputConfig.riderList = planDetail.inputConfig.riderList.filter(function (product) {
            return declinedEciRiderCovCode.indexOf(product.covCode) === -1;
          });
        }
      }
    }

    return planDetail;
  };

  this.prepareQuotConfigs = function (quotation, planDetail, planDetails) {
    if (!quotation.ccy) {
      quotation.ccy = planDetail.currencies[0].ccy[0];
    }

    var payModes = [];
    _.each(planDetail.payModes, function (payMode) {
      payModes.push(_.clone(payMode));
      if (!quotation.paymentMode && payMode.default === 'Y') {
        quotation.paymentMode = payMode.mode;
      }
    });
    planDetail.inputConfig.payModes = payModes;
  };

  this.preparePolicyOptions = function (planDetail, quotation, planDetails) {
    var policyOptions = [];

    var _loop = function _loop(p) {
      var po = _.cloneDeep(planDetail.policyOptions[p]);
      if (po.type === 'datepicker' || po.type === 'text' && (po.subType === 'currency' || po.subType === 'number')) {
        if (po.max || po.max === 0) {
          po.max = parseInt(po.max, 10);
        }
        if (po.min || po.min === 0) {
          po.min = parseInt(po.min, 10);
        }
      }

      if (po.type === 'picker' && po.options) {
        if (!quotation.policyOptions[po.id] && po.value && po.options.length) {
          if (_.find(po.options, function (opt) {
            return opt.value === po.value;
          })) {
            quotation.policyOptions[po.id] = po.value;
          } else if (po.value === '999') {
            quotation.policyOptions[po.id] = po.options[po.options.length - 1].value;
          } else {
            quotation.policyOptions[po.id] = null;
          }
        }
      } else {
        if (!quotation.policyOptions[po.id] && (po.value || po.value === 0 || po.value === '')) {
          if (po.type === 'text' && (po.subType === 'currency' || po.subType === 'number')) {
            quotation.policyOptions[po.id] = parseFloat(po.value);
          } else {
            quotation.policyOptions[po.id] = po.value;
          }
        }
      }
      policyOptions.push(po);
    };

    for (var p in planDetail.policyOptions) {
      _loop(p);
    }
    planDetail.inputConfig.policyOptions = policyOptions;
  };

  this.preparePremLimit = function (planDetail, quotation) {

    var planInfo = {
      classType: planDetail.inputConfig.defaultClassType || '',
      policyTerm: planDetail.inputConfig.defaultPolicyTerm || '',
      premTerm: planDetail.inputConfig.defaultPremTerm || ''
    };

    if (quotation.plans) {
      for (var p in quotation.plans) {
        if (quotation.plans[p].covCode == planDetail.covCode) {
          planInfo = Object.assign(planInfo, quotation.plans[p]);
        }
      }
    }

    var saInput = planDetail.saInput;
    if (saInput && saInput.length > 0) {
      for (var i in saInput) {
        if (saInput[i].ccy === quotation.ccy) {
          planDetail.inputConfig.saInput = saInput[i];
          break;
        }
      }
    }

    var premInput = planDetail.premInput;
    if (premInput && premInput.length > 0) {
      for (var _i in premInput) {
        if (premInput[_i].ccy === quotation.ccy) {
          planDetail.inputConfig.premInput = premInput[_i];
          break;
        }
      }
    }

    if (planDetail.premInputInd === 'Y') {
      var premlim = [];
      if (!planDetail.premlim) {
        // calc from benlim and put to input config
        if (planDetail.benlim && planDetail.benlim.length && planDetail.formulas && planDetail.formulas.saFunc) {
          for (var b in planDetail.benlim) {
            var bl = planDetail.benlim[b];

            if ((!bl.country || bl.country == '*' || bl.country == quotation.iResidence) && (!bl.classType || bl.classType == '*' || bl.classType == planInfo.classType) && quotation.iAge >= bl.ageFr && quotation.iAge <= bl.ageTo) {

              for (var l in bl.limits) {
                var lim = bl.limits[l];
                if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                  premlim = _this.calcMinMaxPrem(planDetail, quotation, planInfo, lim);
                  break;
                }
              }
            }
          }
        }
      } else {
        // massage premlim and put to input config
        if (planDetail.premlim.length) {
          for (var _p in planDetail.premlim) {
            var pl = planDetail.premlim[_p];

            if ((!pl.country || pl.country == '*' || pl.country == quotation.iResidence) && (!pl.classType || pl.classType == '*' || pl.classType == planInfo.classType) && quotation.iAge >= pl.ageFr && quotation.iAge <= pl.ageTo) {
              for (var _l in pl.limits) {
                var _lim = pl.limits[_l];
                if (_lim.ccy == quotation.ccy && _lim.payMode == quotation.paymentMode) {
                  premlim = {
                    max: _lim.max,
                    min: _lim.min
                  };
                  break;
                }
              }
            }
          }
        }
      }
      planDetail.inputConfig.premlim = premlim;
    }

    if (planDetail.saInputInd === 'Y') {
      var benlim = [];
      if (!planDetail.benlim) {
        // calc from benlim and put to input config
        if (planDetail.premlim && planDetail.premlim.length && planDetail.formulas && planDetail.formulas.saFunc) {
          for (var _p2 in planDetail.premlim) {
            var _pl = planDetail.premlim[_p2];

            if ((!_pl.country || _pl.country == '*' || _pl.country == quotation.iResidence) && (!_pl.classType || _pl.classType == '*' || _pl.classType == planInfo.classType) && quotation.iAge >= _pl.ageFr && quotation.iAge <= _pl.ageTo) {

              for (var _l2 in _pl.limits) {
                var _lim2 = _pl.limits[_l2];
                if (_lim2.ccy == quotation.ccy && _lim2.payMode == quotation.paymentMode) {
                  benlim = _this.calcMinMaxSa(planDetail, quotation, planInfo, _lim2);
                  break;
                }
              }
            }
          }
        }
      } else {
        // massage premlim and put to input config
        if (planDetail.benlim.length) {
          for (var _b in planDetail.benlim) {
            var _bl = planDetail.benlim[_b];

            if ((!_bl.country || _bl.country == '*' || _bl.country == quotation.iResidence) && (!_bl.classType || _bl.classType == '*' || _bl.classType == planInfo.classType) && quotation.iAge >= _bl.ageFr && quotation.iAge <= _bl.ageTo) {

              for (var _l3 in _bl.limits) {
                var _lim3 = _bl.limits[_l3];
                if (_lim3.ccy == quotation.ccy && _lim3.payMode == quotation.paymentMode) {
                  benlim = {
                    max: _lim3.max,
                    min: _lim3.min
                  };
                  break;
                }
              }
            }
          }
        }
      }

      planDetail.inputConfig.benlim = benlim;
    }
  };

  this.calcMinMaxSa = function (planDetail, quotation, planInfo, limit) {

    planInfo.premium = limit.min;
    var minSA = 0;
    if (limit.min != 0) {
      minSA = _this.runFunc(planDetail.saFunc || planDetail.formulas && planDetail.formulas.saFunc, quotation, planInfo, planDetail);
    }

    planInfo.premium = limit.max;
    var maxSA = 0;
    if (limit.max != 0) {
      maxSA = _this.runFunc(planDetail.saFunc || planDetail.formulas && planDetail.formulas.saFunc, quotation, planInfo, planDetail);
    }

    return {
      min: minSA,
      max: maxSA
    };
  };

  this.calcMinMaxPrem = function (planDetail, quotation, planInfo, limit) {

    planInfo.sumInsured = limit.min;
    var minPrem = 0;
    if (limit.min != 0) {
      minPrem = _this.runFunc(planDetail.premFunc || planDetail.formulas && planDetail.formulas.premFunc, quotation, planInfo, planDetail);
    }

    planInfo.sumInsured = limit.max;
    var maxPrem = 0;
    if (limit.max != 0) {
      maxPrem = _this.runFunc(planDetail.premFunc || planDetail.formulas && planDetail.formulas.premFunc, quotation, planInfo, planDetail);
    }

    return {
      min: minPrem,
      max: maxPrem
    };
  };

  this.prepareAmountConfig = function (planDetail, quotation) {
    var canEditSumAssured = false;
    var canEditPremium = false;
    var canViewSumAssured = false;

    var missPolTermInput = false;
    for (var i = 0; i < quotation.plans.length; i++) {
      var plan = quotation.plans[i];
      if (plan.covCode === planDetail.covCode && planDetail.polTermInd === 'Y' && !plan.policyTerm && !planDetail.inputConfig.defaultPolicyTerm) {
        missPolTermInput = true;
      }
    }

    if (quotation.paymentMode && !missPolTermInput) {
      if (planDetail.saInputInd && planDetail.saInputInd === 'Y') {
        canEditSumAssured = true;
      }
      if (planDetail.premInputInd && planDetail.premInputInd === 'Y') {
        canEditPremium = true;
      }
    }

    if (planDetail.saViewInd && planDetail.saViewInd === 'Y') {
      canViewSumAssured = true;
    }

    planDetail.inputConfig.canEditSumAssured = canEditSumAssured && canViewSumAssured;
    planDetail.inputConfig.canEditPremium = canEditPremium;
    planDetail.inputConfig.canViewSumAssured = canViewSumAssured;

    planDetail.inputConfig.canViewOthSa = planDetail.othSaInd === 'Y';
    planDetail.inputConfig.canEditOthSa = planDetail.othSaInputInd === 'Y';

    _this.preparePremLimit(planDetail, quotation);
  };

  this.prepareClassConfig = function (planDetail, quotation) {
    if (planDetail.classList) {
      var classList = planDetail.classList;
      planDetail.inputConfig.hasClass = planDetail.classInd === 'Y';
      planDetail.inputConfig.canEditClassType = planDetail.classInputInd === 'Y';

      var classTypeList = [];
      for (var i = 0; i < classList.length; i++) {
        var classType = classList[i];
        if (classType.default && classType.default === 'Y') {
          planDetail.inputConfig.defaultClassType = classType.covClass;
        }
        classTypeList.push({
          value: classType.covClass,
          title: classType.className
        });
      }
      if (classTypeList.length > 0) {
        planDetail.inputConfig.classTypeList = classTypeList;
      }
      planDetail.inputConfig.classTitle = planDetail.classTitle;
    } else {
      planDetail.inputConfig.canEditClassType = false;
    }
  };

  this.prepareAttachableRider = function (planDetail, quotation, planDetails) {
    var riderList = [];
    if (planDetail.riderList && planDetail.riderList.length) {
      for (var r in planDetail.riderList) {
        var rider = planDetail.riderList[r];

        if (rider.condition && rider.condition.length) {
          for (var c in rider.condition) {
            var cond = rider.condition[c];

            if ((!quotation.iResidence || cond.country === '*' || quotation.iResidence === cond.country) && (!quotation.dealerGroup || cond.dealerGroup === '*' || quotation.dealerGroup === cond.dealerGroup) && (!cond.endAge || quotation.iAge <= cond.endAge) && (!cond.staAge || quotation.iAge >= cond.staAge)) {

              var riderDetail = planDetails[rider.covCode];
              if (riderDetail) {
                var _ret2 = function () {
                  if (!_.find(riderDetail.payModes, function (pm) {
                    return pm.mode === quotation.paymentMode;
                  })) {
                    return 'continue';
                  }
                  var validRider = {};
                  _.each(rider, function (value, key) {
                    if (key !== 'condition') {
                      validRider[key] = value;
                    }
                  });
                  _.each(cond, function (value, key) {
                    if (['country', 'dealerGroup', 'staAge', 'endAge', 'amount'].indexOf(key) === -1) {
                      validRider[key] = value;
                    }
                  });
                  _.each(cond.amount, function (amt) {
                    if (quotation.ccy === amt.ccy) {
                      Object.assign(validRider, amt);
                    }
                  });
                  riderList.push(validRider);
                }();

                if (_ret2 === 'continue') continue;
              }
            }
          }
        }
      }
      planDetail.inputConfig.riderList = riderList;
    }
  };

  this.polTermsFunc = function (planDetail, quotation) {
    var policyTermList = null;

    var foundDefault = false;
    if (planDetail.polMaturities) {
      policyTermList = [];
      for (var p in planDetail.polMaturities) {
        var mat = planDetail.polMaturities[p];
        if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
          var interval = mat.interval || 1;
          for (var m = mat.minTerm; m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || m + quotation.iAge <= mat.maxAge) && (!mat.ownerMaxAge || !quotation.pAge || m + quotation.pAge <= mat.ownerMaxAge); m += interval) {
            foundDefault = foundDefault || mat.default === m;
            policyTermList.push({
              "value": m + "",
              "title": m + (quotation.iAge ? "(@" + (quotation.iAge + m) + ")" : ""),
              "default": mat.default === m
            });
          }
          if (mat.default === 0) {
            policyTermList[0].default = true;
          } else if (mat.default === 999 || !foundDefault) {
            policyTermList[policyTermList.length - 1].default = true;
          }
        }
      }
    } else if (planDetail.wholeLifeInd && planDetail.wholeLifeInd == 'Y' && planDetail.wholeLifeAge) {
      return [{
        "value": "999",
        "title": "To Age " + planDetail.wholeLifeAge,
        "default": true
      }];
    }
    return policyTermList;
  };

  this.premTermsFunc = function (planDetail, quotation) {
    var premTermList = null;
    var foundDefault = false;
    planDetail.inputConfig.isPolTermHide = planDetail.isPolTermHide == "Y";
    planDetail.inputConfig.isPremTermHide = planDetail.isPremTermHide == "Y";
    if (planDetail.premMaturities) {
      premTermList = [];
      for (var p in planDetail.premMaturities) {
        var mat = planDetail.premMaturities[p];
        if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
          var interval = mat.interval || 1;
          for (var m = mat.minTerm; m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || m + quotation.iAge <= mat.maxAge) && (!mat.ownerMaxAge || !quotation.pAge || m + quotation.pAge <= mat.ownerMaxAge); m += interval) {
            foundDefault = foundDefault || mat.default === m;
            premTermList.push({
              "value": m + "",
              "title": m + (quotation.iAge ? "(@" + (quotation.iAge + m) + ")" : ""),
              "default": mat.default === m
            });
          }

          if (mat.default === 0) {
            premTermList[0].default = true;
          } else if (mat.default === 999 || !foundDefault) {
            premTermList[premTermList.length - 1].default = true;
          }
        }
      }
    }
    return premTermList;
  };

  this.prepareTermConfig = function (planDetail, quotation, planDetails) {

    var policyTermList = [];
    var premTermList = [];

    if (planDetail.premMatSameAsBasic === 'Y') {
      var basicDetail = planDetails[quotation.baseProductCode];
      premTermList = basicDetail.inputConfig.premTermList;
    } else {
      if (planDetail.formulas && planDetail.formulas.premTermsFunc) {
        premTermList = _this.runFunc(planDetail.formulas.premTermsFunc, planDetail, quotation);
      } else {
        premTermList = _this.premTermsFunc(planDetail, quotation);
      }
    }

    if (planDetail.policyMatSameAsBasic === 'Y') {
      var _basicDetail = planDetails[quotation.baseProductCode];
      policyTermList = _basicDetail.inputConfig.policyTermList;
    } else {
      if (planDetail.formulas && planDetail.formulas.polTermsFunc) {
        policyTermList = _this.runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
      } else {
        policyTermList = _this.polTermsFunc(planDetail, quotation);
      }
    }

    if (planDetail.polMatSameAsPremMat === 'Y') {
      policyTermList = premTermList;
    } else if (planDetail.premMatSameAsPolMat === 'Y') {
      premTermList = policyTermList;
    }

    if (policyTermList && policyTermList.length) {
      for (var p in policyTermList) {
        if (policyTermList[p].default) {
          planDetail.inputConfig.defaultPolicyTerm = policyTermList[p].value;
          break;
        }
      }
      planDetail.inputConfig.canEditPolicyTerm = planDetail.polTermInd === 'Y';
    }
    planDetail.inputConfig.policyTermList = policyTermList;

    if (premTermList && premTermList.length) {
      for (var _p3 in premTermList) {
        if (premTermList[_p3].default) {
          planDetail.inputConfig.defaultPremTerm = premTermList[_p3].value;
          break;
        }
      }
      planDetail.inputConfig.canEditPremTerm = planDetail.premTermInd === 'Y';
    }
    planDetail.inputConfig.premTermList = premTermList;
    // planDetail.inputConfig.termChangeTogether = planDetail.polTermSameAsPremTerm || planDetail.premTermSameAsPolTerm;
  };

  this.validateQuotation = function (quotation, planDetails) {
    //, planInfoList, globalValidation, basicPlanInfo) {
    var retVal = {};

    if (quotation.baseProductCode && planDetails[quotation.baseProductCode]) {

      var basicPlanInfo = 0;
      var basicPlanCode = '';
      var planInfoList = 0;
      var basicPlanDetail = 0;
      var newQuot = false;

      // check if it is initializing
      if (quotation.plans && quotation.plans.length) {
        basicPlanInfo = quotation.plans[0];
        basicPlanCode = basicPlanInfo.covCode;
        basicPlanDetail = planDetails[basicPlanCode];
      } else if (quotation.baseProductCode) {
        newQuot = true;
        basicPlanCode = quotation.baseProductCode;
        basicPlanDetail = planDetails[basicPlanCode];
        basicPlanInfo = {
          covCode: basicPlanDetail.covCode
        };
        quotation.plans = [];
        quotation.plans.push(basicPlanInfo);
      }
      planInfoList = quotation.plans;

      var riderList = [];

      if (basicPlanDetail) {
        // assign extra properties for basic plan first
        var details = _this.assignExtraPropertiesToPlanDetail(basicPlanDetail, quotation, planDetails);

        if (details && details.inputConfig) {
          // check auto attach rider for new quotation and compulsory rider
          if (details.inputConfig.riderList && details.inputConfig.riderList.length) {
            var riskCommenDate = quotation.riskCommenDate ? parseDate(quotation.riskCommenDate) : new Date();
            var pAges = getAges(riskCommenDate, parseDate(quotation.pDob));
            var iAges = getAges(riskCommenDate, parseDate(quotation.iDob));
            riderList = details.inputConfig.riderList = _.filter(details.inputConfig.riderList, function (rider) {
              var riderDetail = planDetails[rider.covCode];
              return checkEntryAge(riderDetail, quotation.iResidence, pAges, iAges);
            });
            _.each(riderList, function (rider) {
              if (newQuot && rider.autoAttach === 'Y' || rider.compulsory === 'Y') {
                if (!_.find(quotation.plans, function (p) {
                  return p.covCode === rider.covCode;
                })) {
                  var riderDetail = planDetails[rider.covCode];
                  planInfoList.push({
                    covCode: riderDetail.covCode,
                    covName: riderDetail.covName,
                    version: riderDetail.version,
                    productLine: riderDetail.productLine,
                    hiddenRider: rider.hiddenRider,
                    hiddenInSelectRiderDialog: rider.hiddenInSelectRiderDialog,
                    addSum: rider.addSum
                  });
                }
              }
            });
          }
          var bpInfo = quotation.plans[0];

          bpInfo.covName = basicPlanDetail.covName;
          bpInfo.productLine = basicPlanDetail.productLine;
          bpInfo.version = basicPlanDetail.version;

          if (details.premTermRule == "BT") {
            bpInfo.premTerm = bpInfo.policyTerm;
          }
          if (details.polTermRule == "PT") {
            bpInfo.policyTerm = bpInfo.premTerm;
          }

          if (newQuot) {
            bpInfo.policyTerm = details.inputConfig.defaultPolicyTerm;
            bpInfo.premTerm = details.inputConfig.defaultPremTerm;
            bpInfo.classType = details.inputConfig.defaultClassType;
          }

          // reset basic plan's sa and prem to fulfill the limit and rule
          var saInput = details.inputConfig.saInput;
          var premInput = details.inputConfig.premInput;
          var benlim = details.inputConfig.benlim;
          var premlim = details.inputConfig.premlim;

          if (bpInfo.calcBy == 'sumAssured' && (bpInfo.sumInsured || bpInfo.sumInsured == 0) && typeof bpInfo.sumInsured == 'number') {
            // reset sa for decimal and factor if required
            if (saInput && saInput.decimal) {
              var scale = math.pow(10, saInput.decimal);
              bpInfo.sumInsured = math.number(math.divide(math.floor(math.multiply(math.bignumber(bpInfo.sumInsured), scale)), scale));
            }

            if (saInput && saInput.factor) {
              bpInfo.sumInsured = math.number(math.multiply(math.round(math.divide(math.bignumber(bpInfo.sumInsured), saInput.factor)), saInput.factor));
            }

            if (benlim) {
              if (benlim.min && benlim.min > bpInfo.sumInsured) {
                bpInfo.sumInsured = benlim.min;
              }
              if (benlim.max && benlim.max < bpInfo.sumInsured) {
                bpInfo.sumInsured = benlim.max;
              }
            }
          } else if (bpInfo.calcBy == 'premium' && (bpInfo.premium || bpInfo.premium == 0) && typeof bpInfo.premium == 'number') {
            // reset premium for decimal and factor if required
            if (premInput && premInput.decimal) {
              var _scale = math.pow(10, premInput.decimal);
              bpInfo.premium = math.number(math.divide(math.floor(math.multiply(math.bignumber(bpInfo.premium), _scale)), _scale));
            }

            if (premInput && premInput.factor) {
              bpInfo.premium = math.number(math.multiply(math.round(math.divide(math.bignumber(bpInfo.premium), premInput.factor)), premInput.factor));
            }

            if (premlim) {
              if (premlim.min && premlim.min > bpInfo.premium) {
                bpInfo.premium = premlim.min;
              }
              if (premlim.max && premlim.max < bpInfo.premium) {
                bpInfo.premium = premlim.max;
              }
            }
          }

          // planDetails[basicPlanCode].inputConfig = details.inputConfig;
        }

        // sort the rider in order
        var seqMap = {};
        if (basicPlanDetail.inputConfig && basicPlanDetail.inputConfig.riderList) {
          for (var rr = 0; rr < basicPlanDetail.inputConfig.riderList.length; rr++) {
            var rd = basicPlanDetail.inputConfig.riderList[rr];
            seqMap[rd.covCode] = rr;
          }
        }

        planInfoList.sort(function (a, b) {
          if (a.covCode == basicPlanCode) {
            return -1;
          } else if (b.covCode == basicPlanCode) {
            return 1;
          }
          return seqMap[a.covCode] - seqMap[b.covCode];
        });

        // quotation.plans = planInfoList;

        // assign extra properties for rider
        for (var p = 1; p < planInfoList.length; p++) {
          var plan = planInfoList[p];
          var planDetail = planDetails[plan.covCode];
          var _details = _this.assignExtraPropertiesToPlanDetail(planDetail, quotation, planDetails);

          // perform rider config. override by basic plan
          if (riderList && riderList.length) {
            for (var r in riderList) {
              var rider = riderList[r];
              if (rider.covCode == plan.covCode) {
                if (rider.maxSA) {
                  _details.inputConfig.benlim.max = rider.maxSA;
                }
                if (rider.minSA) {
                  _details.inputConfig.benlim.min = rider.minSA;
                }
                break;
              }
            }
          }
          plan.covName = planDetail.covName;
          plan.saPostfix = planDetail.saPostfix;
          plan.productLine = planDetail.productLine;
          plan.version = planDetail.version;

          if (_details.polTermRule == "BPT") {
            plan.policyTerm = basicPlanInfo.premTerm;
          } else if (_details.polTermRule == "BBT") {
            plan.policyTerm = basicPlanInfo.policyTerm;
          } else if (!quotation.plans[p].policyTerm) {
            plan.policyTerm = _details.inputConfig.defaultPolicyTerm;
          }

          if (_details.premTermRule == "BPT") {
            plan.premTerm = basicPlanInfo.premTerm;
          } else if (_details.premTermRule == "BBT") {
            plan.premTerm = basicPlanInfo.policyTerm;
          } else if (!quotation.plans[p].policyTerm) {
            plan.premTerm = _details.inputConfig.defaultPremTerm;
          }

          if (_details.premTermRule == "BT") {
            plan.premTerm = plan.policyTerm;
          }
          if (_details.polTermRule == "PT") {
            plan.policyTerm = plan.premTerm;
          }

          plan.classType = quotation.plans[p].classType || _details.inputConfig.defaultClassType;
          // planDetails[plan.covCode].inputConfig = details.inputConfig;

          // reset basic plan's sa and prem to fulfill the limit
          var _saInput = _details.inputConfig.saInput;
          var _premInput = _details.inputConfig.premInput;
          var _benlim = _details.inputConfig.benlim;
          var _premlim = _details.inputConfig.premlim;

          if (plan.calcBy == 'sumAssured' && (plan.sumInsured || plan.sumInsured == 0) && typeof plan.sumInsured == 'number') {
            // reset sa for decimal and factor if required
            if (_saInput && _saInput.decimal) {
              var _scale2 = math.pow(10, _saInput.decimal);
              plan.sumInsured = math.number(math.divide(math.floor(math.multiply(math.bignumber(plan.sumInsured), _scale2)), _scale2));
            }

            if (_saInput && _saInput.factor) {
              plan.sumInsured = math.number(math.multiply(math.round(math.divide(math.bignumber(plan.sumInsured), _saInput.factor)), _saInput.factor));
            }

            if (_benlim) {
              if (_benlim.min && _benlim.min > plan.sumInsured) {
                plan.sumInsured = _benlim.min;
              }
              if (_benlim.max && _benlim.max < plan.sumInsured) {
                plan.sumInsured = _benlim.max;
              }
            }
          } else if (plan.calcBy == 'premium' && (plan.premium || plan.premium == 0) && typeof plan.premium == 'number') {
            // reset premium for decimal and factor if required
            if (_premInput && _premInput.decimal) {
              var _scale3 = math.pow(10, _premInput.decimal);
              plan.premium = math.number(math.divide(math.floor(math.multiply(math.bignumber(plan.premium), _scale3)), _scale3));
            }

            if (_premInput && _premInput.factor) {
              plan.premium = math.number(math.multiply(math.round(math.divide(math.bignumber(plan.premium), _premInput.factor)), _premInput.factor));
            }

            if (_premlim) {
              if (_premlim.min && _premlim.min > plan.premium) {
                plan.premium = _premlim.min;
              }
              if (_premlim.max && _premlim.max < plan.premium) {
                plan.premium = _premlim.max;
              }
            }
          }
        }

        for (var i = 0; i < planInfoList.length; i++) {
          var detials = planDetails[planInfoList[i].covCode];
          if (detials.formulas && detials.formulas.willValidQuotFunc) {
            _this.runFunc(detials.formulas.willValidQuotFunc, quotation, planInfoList[i], planDetails);
          }
        }
        //saViewInd, policy term and premium term title
        for (var _i2 = 0; _i2 < planInfoList.length; _i2++) {
          var _plan = planInfoList[_i2];
          var pDetail = planDetails[planInfoList[_i2].covCode];
          var policyTermList = pDetail.inputConfig.policyTermList || [];
          for (var _j = 0; _j < policyTermList.length; _j++) {
            if (policyTermList[_j].value == _plan.policyTerm) {
              _plan.polTermDesc = policyTermList[_j].title;
            }
          }
          var premTermList = pDetail.inputConfig.premTermList || [];
          for (var _j2 = 0; _j2 < premTermList.length; _j2++) {
            if (premTermList[_j2].value == _plan.premTerm) {
              _plan.premTermDesc = premTermList[_j2].title;
            }
          }
          _plan.saViewInd = pDetail.saViewInd;
          _plan.othSaInd = pDetail.othSaInd;
        }
        //store picker option title as description when picker, use value otherwise
        quotation.policyOptionsDesc = {};
        for (var po in quotation.policyOptions) {
          quotation.policyOptionsDesc[po] = quotation.policyOptions[po] ? quotation.policyOptions[po] : '-';
          var configPolOpts = planDetails[quotation.baseProductCode].inputConfig.policyOptions;
          if (configPolOpts) {
            for (var j = 0; j < configPolOpts.length; j++) {
              var configPolOpt = configPolOpts[j];
              if (configPolOpt.id === po && configPolOpt.options) {
                var opts = configPolOpt.options;
                for (var k = 0; k < opts.length; k++) {
                  var opt = opts[k];
                  if (opt.value == quotation.policyOptions[po]) {
                    quotation.policyOptionsDesc[po] = opt.title;
                    break;
                  }
                }
                break;
              }
            }
          }
        }

        // start calc quotation
        quotation.premium = 0;
        quotation.totYearPrem = 0;
        quotation.totHalfyearPrem = 0;
        quotation.totQuarterPrem = 0;
        quotation.totMonthPrem = 0;
        quotation.sumInsured = 0;

        if (basicPlanDetail.formulas && basicPlanDetail.formulas.validQuotFunc) {
          var _retVal2 = _this.runFunc(basicPlanDetail.formulas.validQuotFunc, quotation, planDetails);
          _retVal2.quotation = _retVal2.quotation || quotation;
          _retVal2.planDetails = _retVal2.planDetails || planDetails;
        } else {

          // validate the input before calc quotation
          for (var _i3 = 0; _i3 < planInfoList.length; _i3++) {
            var _detials = planDetails[planInfoList[_i3].covCode];

            if (_detials.formulas && _detials.formulas.validBeforeCalcFunc) {
              _this.runFunc(_detials.formulas.validBeforeCalcFunc, quotation, planInfoList[_i3], planDetails);
            } else {
              _this.quotValid.validatePlanBeforeCalc(quotation, planInfoList[_i3], planDetails);
            }
          }

          retVal.planDetails = planDetails;

          var lastCalcList = {};
          var removeRiderList = [];

          for (var _i4 = 0; _i4 < planInfoList.length; _i4++) {
            var planInfo = planInfoList[_i4];
            var code = planInfo.covCode;

            // check if it is a valid rider by find it in input config of basic plan
            var curRiderConf = false;
            var riderDetail = planDetails[code];
            if (_i4 >= 1) {
              for (var _r in riderList) {
                var riderConf = riderList[_r];
                if (code == riderConf.covCode) {
                  curRiderConf = riderConf;
                  break;
                }
              }

              // check if it has SA override rule
              if (curRiderConf && (!planInfo.sumInsured || !riderDetail.inputConfig.canEditSumAssured)) {
                if (curRiderConf.saRule == 'BP') {
                  planInfo.sumInsured = basicPlanInfo.yearPrem * (curRiderConf.saRate || 1);
                } else if (curRiderConf.saRule == 'BSA') {
                  planInfo.sumInsured = basicPlanInfo.sumInsured * (curRiderConf.saRate || 1);
                } else if (curRiderConf.saRule == 'PP') {
                  lastCalcList[_i4 + ""] = curRiderConf;
                  continue; // skip and calc later
                } else if (curRiderConf.saRule == 'F') {
                  planInfo.sumInsured = curRiderConf.saFix || 0;
                }
              }
            }

            if (_i4 == 0 || curRiderConf) {
              // perform calc if it is basic plan or valid rider
              if (riderDetail.formulas && riderDetail.formulas.calcQuotFunc) {
                _this.runFunc(riderDetail.formulas.calcQuotFunc, quotation, planInfo, planDetails);
              } else {
                _this.quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
              }
            } else if (!curRiderConf) {
              // remove invalid rider
              removeRiderList.push(code);
            }
          }

          // calc for post calc rider
          var policyPremium = quotation.premium;

          for (var l in lastCalcList) {
            var _planInfo = planInfoList[l];
            var _curRiderConf = lastCalcList[l];
            if (_curRiderConf && (!_planInfo.sumInsured || !planDetails[_planInfo.covCode].inputConfig.canEditSumAssured)) {
              // override SA if required
              if (_curRiderConf.saRule === 'PP') {
                _planInfo.sumInsured = policyPremium * (_curRiderConf.saRate || 1);
              }
            }

            // recalc it
            if (planDetails[_planInfo.covCode].formulas && planDetails[_planInfo.covCode].formulas.calcQuotFunc) {
              _this.runFunc(planDetails[_planInfo.covCode].formulas.calcQuotFunc, quotation, _planInfo, planDetails);
            } else {
              _this.quotCalc.calcQuotPlan(quotation, _planInfo, planDetails);
            }
          }

          // remove invalid rider
          for (var _l4 in removeRiderList) {
            var _code = removeRiderList[_l4];
            for (var _p4 = 1; _p4 < quotation.plans.length; _p4++) {
              if (quotation.plans[_p4].covCode === _code) {
                quotation.plans.splice(_p4, 1);
              }
            }
          }

          basicPlanInfo = quotation.plans[0];
          quotation.sumInsured = quotation.sumInsured || basicPlanInfo.sumInsured;
          quotation.policyTerm = basicPlanInfo.benefitTerm;
          quotation.premTerm = basicPlanInfo.premTerm;

          retVal.quotation = quotation;

          // Validate each plan by calculated values
          for (var _i5 = 0; _i5 < planInfoList.length; _i5++) {
            var detail = planDetails[planInfoList[_i5].covCode];
            if (detail.formulas && detail.formulas.validAfterCalcFunc) {
              _this.runFunc(detail.formulas.validAfterCalcFunc, quotation, planInfoList[_i5], detail);
            } else {
              _this.quotValid.validatePlanAfterCalc(quotation, planInfoList[_i5], detail);
            }
          }

          // Validate by global rules
          _this.quotValid.validateGlobal(quotation, planInfoList, planDetails);
        }
      }
    }
    return retVal;
  };

  this.generateIllustration = function (quotation, planDetails, extraPara) {
    var illustrations = {};
    var isIllustrationGenerated = true;
    extraPara.illustrations = illustrations;
    var warningMsg = null;
    try {
      for (var p in quotation.plans) {
        var planInfo = quotation.plans[p];
        if (planDetails && planInfo && planDetails[planInfo.covCode]) {
          var planDetail = planDetails[planInfo.covCode];
          if (planDetail.formulas && planDetail.formulas.illustrateFunc) {
            var illust = _this.runFunc(planDetail.formulas.illustrateFunc, quotation, planInfo, planDetails, extraPara);
            if (illust) {
              try {
                warningMsg = illust[0].warningMsg;
              } catch (ex) {
                warningMsg = null;
              }
              illustrations[planInfo.covCode] = illust;
            } else {
              isIllustrationGenerated = false;
            }
          }
        }
      }
      var bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getIllustrateSummary) {
        illustrations = _this.runFunc(bpDetail.formulas.getIllustrateSummary, quotation, planDetails, extraPara, illustrations);
      }
    } catch (ex) {
      logger.log("generateIllustration(); Error -" + ex);
      isIllustrationGenerated = false;
    }

    return {
      'illustrations': illustrations,
      'isIllustrationGenerated': isIllustrationGenerated,
      'warningMsg': warningMsg
    };
  };

  /// function to prepare the data for genreate PDF
  ///     quotation - Quotation Model
  ///     planDetails - Map of covCode vs ProductModel
  ///     extraPara = {
  /// *          BI Options          : Object, key value map of BI options
  /// *          templateFuncs       : Object, map of template code vs PDF template formulas ( prepareReportDataFunc )
  /// *          requireReportData   : bool, indicate whether need to generate report data
  /// *          illustrations       : Object, map of plan code vs plan illustration if any
  ///                 }
  this.prepareReportData = function (quotation, planDetails, extraPara) {
    var reportData = {
      root: {
        mainInfo: {},
        planInfos: [],
        removeBookmark: []
      }
    };

    //expiry date
    var expiryDate = new Date();
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "Devember"];
    expiryDate = new Date(new Date(expiryDate).setMonth(expiryDate.getMonth() + 1));
    expiryDate = new Date(expiryDate.getTime() - 24 * 60 * 60 * 1000);
    var expiryDateStr = expiryDate.getDate() + ' ' + months[expiryDate.getMonth()] + ' ' + expiryDate.getFullYear();
    // MainInfo
    reportData.root.mainInfo.username = quotation.agent.agentCode;
    reportData.root.agent = quotation.agent;

    reportData.root.mainInfo.name = quotation.iFullName;
    reportData.root.mainInfo.age = quotation.iAge;
    reportData.root.mainInfo.gender = quotation.iGender;
    reportData.root.mainInfo.isSmoker = quotation.iSmoke;
    reportData.root.mainInfo.currency = quotation.ccy;
    reportData.root.mainInfo.createDate = quotation.createDate;
    reportData.root.mainInfo.expiryDate = expiryDateStr;
    reportData.root.mainInfo.occupation = quotation.occupation;
    reportData.root.mainInfo.nationality = quotation.nationality;
    reportData.root.mainInfo.residence = quotation.residence;
    reportData.root.mainInfo.dob = quotation.iDob;
    reportData.root.mainInfo.riskCommenDate = quotation.riskCommenDate;

    reportData.root.mainInfo.sameAs = quotation.sameAs;

    if (quotation.pLastName || quotation.pFirstName) {
      reportData.root.mainInfo.pName = quotation.pFullName;
      reportData.root.mainInfo.pAge = quotation.pAge;
      reportData.root.mainInfo.pGender = quotation.pGender;
      reportData.root.mainInfo.pIsSmoker = quotation.pSmoker;
      reportData.root.mainInfo.pOccupation = quotation.pOccupation;
      reportData.root.mainInfo.pNationality = quotation.pNationality;
      reportData.root.mainInfo.pResidence = quotation.pResidence;
      reportData.root.mainInfo.pDob = quotation.pDob;
    }
    reportData.root.agentName = quotation.agent.name;
    reportData.root.sysDate = extraPara.systemDate;
    reportData.root.releaseVersion = extraPara.releaseVersion;
    reportData.root.warningMsg = extraPara.warningMsg;

    var basicPlanDetail = planDetails[quotation.baseProductCode];

    if (basicPlanDetail) {
      reportData.root.mainInfo.basicPlanName = !basicPlanDetail.covName ? "" : typeof basicPlanDetail.covName == 'string' ? basicPlanDetail.covName : basicPlanDetail.covName.en || basicPlanDetail.covName[Object.keys(basicPlanDetail.covName)[0]];
      reportData.root.mainInfo.basicPlanCode = basicPlanDetail.covCode;
    }
    reportData.root.mainInfo.singlePremium = getCurrency(quotation.totYearPrem, null, 2);
    reportData.root.mainInfo.yearlyPremium = getCurrency(quotation.totYearPrem, null, 2);
    reportData.root.mainInfo.halfYearlyPremium = getCurrency(quotation.totHalfyearPrem, null, 2);
    reportData.root.mainInfo.quarterlyPremium = getCurrency(quotation.totQuarterPrem, null, 2);
    reportData.root.mainInfo.monthlyPremium = getCurrency(quotation.totMonthPrem, null, 2);
    reportData.root.mainInfo.premium = getCurrency(quotation.premium, null, (quotation.premium + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.sumAssured = getCurrency(quotation.sumInsured, null, (quotation.sumInsured + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.quickQuote = extraPara.quickQuote ? 'Y' : 'N';

    // Plans
    var planInfoList = quotation.plans;

    for (var i = 0; i < planInfoList.length; i++) {
      var planInfo = planInfoList[i];
      var planDetail = planDetails[planInfo.covCode];
      var plan = {};
      plan.planName = !planInfo.covName ? "" : typeof planInfo.covName == 'string' ? planInfo.covName : planInfo.covName.en || planInfo.covName[Object.keys(planInfo.covName)[0]];
      plan.postfix = !planInfo.saPostfix ? "" : typeof planInfo.saPostfix == 'string' ? planInfo.saPostfix : planInfo.saPostfix.en || planInfo.saPostfix[Object.keys(planInfo.saPostfix)[0]];
      plan.sumAssured = getCurrency(planInfo.sumInsured, ' ', 0);
      plan.policyTerm = extraPara.langMap && extraPara.langMap[planInfo.policyTerm] || planInfo.policyTerm;
      plan.premium = getCurrency(planInfo.premium, ' ', 2);
      plan.APremium = getCurrency(planInfo.yearPrem, ' ', 2);
      plan.HPremium = getCurrency(planInfo.halfYearPrem, ' ', 2);
      plan.QPremium = getCurrency(planInfo.quarterPrem, ' ', 2);
      plan.MPremium = getCurrency(planInfo.monthPrem, ' ', 2);
      plan.LPremium = getCurrency(planInfo.yearPrem, ' ', 2);
      plan.permTerm = extraPara.langMap && extraPara.langMap[planInfo.premTerm] || planInfo.premTerm;
      plan.payMode = quotation.paymentMode;
      plan.planInd = planDetail.planInd;
      plan.covCode = planDetail.covCode;
      plan.productLine = planDetail.productLine;

      reportData.root.planInfos.push(plan);
    }

    // execute prepare report data func in template
    if (basicPlanDetail.reportTemplate && extraPara.templateFuncs) {
      for (var rt in basicPlanDetail.reportTemplate) {
        var template = basicPlanDetail.reportTemplate[rt];
        var tempFunc = extraPara.templateFuncs[template.pdfCode];
        if (tempFunc) {
          if (template.covCode && template.covCode != '0' && template.covCode != 'na') {
            var found = false;
            for (var p in planInfoList) {
              if (planInfoList[p].covCode == template.covCode) {
                reportData[template.pdfCode + "/" + template.covCode] = _this.runFunc(tempFunc, quotation, planInfoList[p], planDetails, extraPara);
                found = true;
                break;
              }
            }
            if (!found) {
              reportData[template.pdfCode] = _this.runFunc(tempFunc, quotation, planInfoList[0], planDetails, extraPara);
            }
          } else {
            reportData[template.pdfCode] = _this.runFunc(tempFunc, quotation, planInfoList[0], planDetails, extraPara);
          }
        }
      }
    }

    return reportData;
  };
};

module.exports = QuotDriver;