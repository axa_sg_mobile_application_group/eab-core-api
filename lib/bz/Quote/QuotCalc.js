'use strict';

var _ = require('lodash');
var math = require('./math2.js');
var utils = require('./utils');

var QuotCalc = function QuotCalc(context, quotDriver) {
    var _this = this;

    this.context = context;
    var getCurrency = utils.getCurrency,
        debug = utils.debug;

    // let runFunc = quotDriver.runFunc;
    this.quotDriver = quotDriver;

    this.getPremTerm = function (quotation, planDetail, planInfo) {
        var term = 0;
        if (planInfo.premTerm) {
            term = parseInt(planInfo.premTerm, 10);
        } else if (planDetail.inputConfig.defaultPremTerm) {
            if (planDetail.inputConfig.defaultPremTerm == "999") {
                if (planDetail.wholeLifeInd && planDetail.wholeLifeAge) {
                    term = planDetail.wholeLifeAge - quotation.iAge;
                    planInfo.premTerm = "999";
                } else if (planDetail.inputConfig.premTermList) {
                    // show never call this
                    var _term = planDetail.inputConfig.premTermList[planDetail.inputConfig.premTermList.length - 1].value;
                    _term = parseInt(_term, 10);
                    planInfo.premTerm = _term + "";
                }
            } else {
                term = parseInt(planDetail.inputConfig.defaultPremTerm, 10);
                planInfo.premTerm = term + "";
            }
        }

        return term;
    };

    this.getPolicyTerm = function (quotation, planDetail, planInfo) {
        var term = 0;
        if (planInfo.policyTerm) {
            term = parseInt(planInfo.policyTerm, 10);
        } else if (planDetail.inputConfig.defaultPolicyTerm) {
            if (planDetail.inputConfig.defaultPolicyTerm == "999") {
                if (planDetail.wholeLifeInd && planDetail.wholeLifeAge) {
                    term = planDetail.wholeLifeAge - quotation.iAge;
                    planInfo.policyTerm = "999";
                } else if (planDetail.inputConfig.policyTermList) {
                    // show never call this
                    var _term2 = planDetail.inputConfig.policyTermList[planDetail.inputConfig.policyTermList.length - 1].value;
                    _term2 = parseInt(_term2, 10);
                    planInfo.policyTerm = _term2 + "";
                }
            } else {
                term = parseInt(planDetail.inputConfig.defaultPolicyTerm, 10);
                planInfo.policyTerm = term + "";
            }
        }

        return term;
    };

    this.getPaymentMode = function (mode, planDetail) {
        for (var i in planDetail.payModes) {
            var payment = planDetail.payModes[i];
            if (payment.mode == mode) {
                return payment;
            }
        }
        return null;
    };

    this.getAnnPrem = function (quotation, planInfo, planDetail) {
        if (planInfo.premium && quotation.paymentMode) {
            if (quotation.paymentMode == 'L' || quotation.paymentMode == 'A') {
                return planInfo.premium;
            } else {
                var payment = _this.getPaymentMode(quotation.paymentMode, planDetail);
                if (payment) {
                    if (payment.operator == "D") {
                        return math.number(math.multiply(math.bignumber(planInfo.premium), payment.factor));
                    } else {
                        return math.number(math.divide(math.bignumber(planInfo.premium), payment.factor));
                    }
                }
            }
        }
        return 0;
    };

    this.getSAScale = function (quotation, planDetail) {
        for (var s in planDetail.saInput) {
            var saInput = planDetail.saInput[s];
            if (saInput.ccy == quotation.ccy) {
                return saInput.scale;
            }
        }
        return 1;
    };

    this.updateQuotationPremium = function (quotation, planInfo, planDetail) {

        var cQuot = Object.assign({}, quotation);
        var cPlanInfo = Object.assign({}, planInfo);
        cQuot.paymentMode = 'A';
        var annPrem = _this.quotDriver.runFunc(planDetail.formulas.premFunc, cQuot, cPlanInfo, planDetail);

        if (!annPrem) return;

        var decimal = 0;

        var premInput = planDetail.premInput;
        if (premInput && premInput.length > 0) {
            for (var i in premInput) {
                if (premInput[i].ccy === quotation.ccy) {
                    decimal = premInput[i].decimal;
                    break;
                }
            }
        }

        for (var _i in planDetail.payModes) {
            var payment = planDetail.payModes[_i];

            var prem = payment.operator == "D" ? math.divide(math.bignumber(annPrem), payment.factor) : math.multiply(math.bignumber(annPrem), payment.factor);
            //assume all product premium is positive number and rounding method is truncation
            // reset premium for decimal by currency
            if (decimal) {
                var scale = math.pow(10, decimal);
                prem = math.divide(math.floor(math.multiply(prem, scale)), scale);
            }
            // if (factor) {
            //     prem = math.multiply(math.round(math.divide(math.bignumber(prem), factor)), factor);
            // }
            prem = math.number(prem);
            if (quotation.paymentMode === 'L') {
                if (payment.mode === 'L') {
                    planInfo.yearPrem = prem;
                    quotation.totYearPrem = math.number(math.add(math.bignumber(quotation.totYearPrem), prem));
                }
            } else {
                if (payment.mode === 'A') {
                    planInfo.yearPrem = prem;
                    quotation.totYearPrem = math.number(math.add(math.bignumber(quotation.totYearPrem), prem));
                } else if (payment.mode === 'S') {
                    planInfo.halfYearPrem = prem;
                    quotation.totHalfyearPrem = math.number(math.add(math.bignumber(quotation.totHalfyearPrem), prem));
                } else if (payment.mode === 'Q') {
                    planInfo.quarterPrem = prem;
                    quotation.totQuarterPrem = math.number(math.add(math.bignumber(quotation.totQuarterPrem), prem));
                } else if (payment.mode === 'M') {
                    planInfo.monthPrem = prem;
                    quotation.totMonthPrem = math.number(math.add(math.bignumber(quotation.totMonthPrem), prem));
                }
            }
        }
    };

    /**
     * Calculates the plan's premium and updates the total premium of quotation.
     *
     * @param {*} quotation
     * @param {*} planInfo
     * @param {*} planDetails
     */
    this.calcPlanPrem = function (quotation, planInfo, planDetails) {
        var planDetail = planDetails[planInfo.covCode];
        var premInput = planDetail.inputConfig.premInput;
        var cQuot = Object.assign({}, quotation);
        var cPlanInfo = Object.assign({}, planInfo);

        for (var i in planDetail.payModes) {
            var payment = planDetail.payModes[i];
            cQuot.paymentMode = payment.mode;
            var prem = _this.quotDriver.runFunc(planDetail.formulas.premFunc, cQuot, cPlanInfo, planDetail);

            if (premInput && premInput.decimal) {
                var scale = math.pow(10, premInput.decimal);
                prem = math.number(math.divide(math.floor(math.multiply(math.bignumber(prem), scale)), scale));
            }

            if (premInput && premInput.factor) {
                prem = math.number(math.multiply(math.round(math.divide(math.bignumber(prem), premInput.factor)), premInput.factor));
            }

            if (payment.mode === quotation.paymentMode) {
                planInfo.premium = prem;
                quotation.premium = math.number(math.add(math.bignumber(quotation.premium), prem));
                if (planInfo.sumInsured) {
                    quotation.sumInsured = math.number(math.add(math.bignumber(quotation.sumInsured), planInfo.sumInsured));
                }
            }

            if (quotation.paymentMode === 'L') {
                if (payment.mode === 'L') {
                    planInfo.yearPrem = prem;
                    cPlanInfo.yearPrem = prem;
                    quotation.totYearPrem = math.number(math.add(math.bignumber(quotation.totYearPrem), prem));
                }
            } else {
                if (payment.mode === 'A') {
                    planInfo.yearPrem = prem;
                    cPlanInfo.yearPrem = prem;
                    quotation.totYearPrem = math.number(math.add(math.bignumber(quotation.totYearPrem), prem));
                } else if (payment.mode === 'S') {
                    planInfo.halfYearPrem = prem;
                    quotation.totHalfyearPrem = math.number(math.add(math.bignumber(quotation.totHalfyearPrem), prem));
                } else if (payment.mode === 'Q') {
                    planInfo.quarterPrem = prem;
                    quotation.totQuarterPrem = math.number(math.add(math.bignumber(quotation.totQuarterPrem), prem));
                } else if (payment.mode === 'M') {
                    planInfo.monthPrem = prem;
                    quotation.totMonthPrem = math.number(math.add(math.bignumber(quotation.totMonthPrem), prem));
                }
            }
        }
    };

    /**
     * Calculates the plan's sum assured and update the total premium of quotation.
     * If premFunc is implemented, use the calculated sum assured to update the premium.
     *
     * @param {*} quotation
     * @param {*} planInfo
     * @param {*} planDetails
     */
    this.calcPlanSumAssured = function (quotation, planInfo, planDetails) {
        var planDetail = planDetails[planInfo.covCode];
        var premFunc = planDetail.formulas && planDetail.formulas.premFunc;
        var saInput = planDetail.inputConfig.saInput;

        var cPlanInfo = Object.assign({}, planInfo);
        cPlanInfo.premAdjAmt = 1;

        var closestPrem = null;
        var closestSA = null;
        if (planDetail.premAdj) {
            for (var i = 0; i < planDetail.premAdj.length; i++) {
                cPlanInfo.premAdjAmt = planDetail.premAdj[i].adjRate;

                var sa = _this.quotDriver.runFunc(planDetail.formulas.saFunc, quotation, cPlanInfo, planDetail);
                cPlanInfo.sumInsured = sa;

                if (premFunc) {
                    var p = _this.quotDriver.runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
                    if (p === cPlanInfo.premium) {
                        if (saInput && saInput.decimal) {
                            var scale = math.pow(10, saInput.decimal);
                            sa = math.number(math.divide(math.floor(math.multiply(math.bignumber(sa), scale)), scale));
                        }
                        if (saInput && saInput.factor) {
                            sa = math.number(math.multiply(math.ceil(math.divide(math.bignumber(sa), saInput.factor)), saInput.factor));
                        }
                        cPlanInfo.sumInsured = sa;
                        closestPrem = _this.quotDriver.runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
                        closestSA = sa;
                    }
                } else {
                    closestSA = sa;
                    closestPrem = planInfo.premium;
                    break;
                }
            }
        } else {
            closestSA = _this.quotDriver.runFunc(planDetail.formulas.saFunc, quotation, cPlanInfo, planDetail);
            if (saInput && saInput.decimal) {
                var _scale = math.pow(10, saInput.decimal);
                closestSA = math.number(math.divide(math.floor(math.multiply(math.bignumber(closestSA), _scale)), _scale));
            }
            if (saInput && saInput.factor) {
                closestSA = math.number(math.multiply(math.ceil(math.divide(math.bignumber(closestSA), saInput.factor)), saInput.factor));
            }
            cPlanInfo.sumInsured = closestSA;
            if (premFunc) {
                closestPrem = _this.quotDriver.runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
            } else {
                closestPrem = planInfo.premium;
            }
        }

        planInfo.premium = closestPrem;
        planInfo.sumInsured = closestSA;

        quotation.premium = math.number(math.add(math.bignumber(quotation.premium), closestPrem));
        quotation.sumInsured = math.number(math.add(math.bignumber(quotation.sumInsured), closestSA));

        _this.updateQuotationPremium(quotation, planInfo, planDetail);
    };

    this.calcQuotPlan = function (quotation, planInfo, planDetails) {
        var planDetail = planDetails[planInfo.covCode];
        var saFunc = planDetail.formulas && planDetail.formulas.saFunc;
        var premFunc = planDetail.formulas && planDetail.formulas.premFunc;

        _this.getPremTerm(quotation, planDetail, planInfo);
        _this.getPolicyTerm(quotation, planDetail, planInfo);

        if ((planInfo.calcBy === 'sumAssured' || !planInfo.calcBy) && _.isNumber(planInfo.sumInsured) && premFunc) {
            _this.calcPlanPrem(quotation, planInfo, planDetails);
        } else if ((planInfo.calcBy == 'premium' || !planInfo.calcBy) && _.isNumber(planInfo.premium) && saFunc) {
            _this.calcPlanSumAssured(quotation, planInfo, planDetails);
        } else if (planInfo.calcBy && planDetail.formulas[planInfo.calcBy]) {

            var cPlanInfo = _this.quotDriver.runFunc(planDetail.formulas[planInfo.calc], quotation, planInfo, planDetails);

            var prem = planInfo.premium = cPlanInfo.premium;
            planInfo.sumInsured = cPlanInfo.sumInsured;

            quotation.premium = math.number(math.add(math.bignumber(quotation.premium), prem));
            quotation.sumInsured = math.number(math.add(math.bignumber(quotation.sumInsured), planInfo.sumInsured));

            _this.updateQuotationPremium(quotation, planInfo, planDetail);
        } else if (isFinite(planInfo.premium)) {
            quotation.premium = math.number(math.add(math.bignumber(quotation.premium), planInfo.premium));
            _this.updateQuotationPremium(quotation, planInfo, planDetail);
        } else {
            // invalid input error handle
        }
    };
};

module.exports = QuotCalc;