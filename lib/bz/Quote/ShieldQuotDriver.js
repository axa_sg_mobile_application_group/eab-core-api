'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _ = require('lodash');
var QuotDriver = require('./QuotDriver');

var _require = require('../../common/DateUtils'),
    formatDate = _require.formatDate,
    parseDate = _require.parseDate;

var _require2 = require('../../common/ProductUtils'),
    getAgeByMethod = _require2.getAgeByMethod,
    isCrossBorder = _require2.isCrossBorder;

module.exports = function () {
  var _this = this;

  this.quotDriver = new QuotDriver();

  this.getAvailableApplicants = function (quotation, planDetails, profiles, fnaInfo) {
    if (quotation && planDetails) {
      var bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getAvailableApplicants) {
        return _this.quotDriver.runFunc(bpDetail.formulas.getAvailableApplicants, quotation, planDetails, profiles, fnaInfo);
      }
    }
    return profiles;
  };

  this.getEligibleClients = function (quotation, planDetails, profiles, fnaInfo) {
    if (quotation && planDetails) {
      var bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getEligibleClients) {
        return _this.quotDriver.runFunc(bpDetail.formulas.getEligibleClients, quotation, planDetails, profiles, fnaInfo);
      }
    }
    return profiles;
  };

  this.getAvailableClasses = function (quotation, planDetails, profiles) {
    if (quotation && planDetails) {
      var bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getAvailableClasses) {
        return _this.quotDriver.runFunc(bpDetail.formulas.getAvailableClasses, quotation, planDetails, profiles);
      }
    }
    return {};
  };

  this.calculateQuotation = function (quotationJson, planDetailsJson) {
    var resultObj = {};
    try {
      var quotation = 0,
          planDetails = 0;
      // Initialize variables based on the input parameters
      if (typeof quotationJson === 'string') {
        if (JSON && typeof JSON.parse === 'function') {
          quotation = JSON.parse(quotationJson);
          planDetails = JSON.parse(planDetailsJson);
        } else {
          eval('quotation = ' + quotationJson);
          eval('planDetails = ' + planDetailsJson);
        }
      } else if ((typeof quotationJson === 'undefined' ? 'undefined' : _typeof(quotationJson)) === 'object') {
        quotation = quotationJson;
        planDetails = planDetailsJson;
      }

      if (quotation && planDetails) {
        var bpDetail = planDetails[quotation.baseProductCode];

        var riskCommenDate = quotation.isBackDate === 'Y' ? parseDate(quotation.riskCommenDate) : new Date();
        quotation.riskCommenDate = formatDate(riskCommenDate);
        quotation.iCrossBorderWithoutPass = isCrossBorder(quotation, null, false, false);
        quotation.pCrossBorderWithoutPass = isCrossBorder(quotation, null, true, false);
        quotation.isCrossBorderWithoutPass = quotation.pCrossBorderWithoutPass || quotation.iCrossBorderWithoutPass;
        quotation.pAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(quotation.pDob));

        var inputConfigs = {};
        _.each(quotation.insureds, function (subQuot, iCid) {
          subQuot.pAge = quotation.pAge;
          subQuot.iAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(subQuot.iDob));

          var retVal = _this.quotDriver.validateQuotation(subQuot, planDetails);
          quotation.insureds[iCid] = retVal.quotation || subQuot;

          planDetails = retVal.planDetails || planDetails;

          inputConfigs[subQuot.iCid] = {};
          _.each(subQuot.plans, function (plan) {
            inputConfigs[subQuot.iCid][plan.covCode] = planDetails[plan.covCode].inputConfig;
          });
        });

        if (bpDetail.formulas && bpDetail.formulas.calcShieldSummary) {
          _this.quotDriver.runFunc(bpDetail.formulas.calcShieldSummary, quotation);
        }

        resultObj.quotation = quotation;
        resultObj.inputConfigs = inputConfigs;
      } else {
        resultObj.error = {
          message: 'Quotation or Plan details missing.'
        };
      }
    } catch (ex) {
      console.error(ex);
    }
    return _this.quotDriver.context.returnResult(resultObj);
  };

  this.generateIllustrationData = function () {
    return this.quotDriver.context.returnResult({
      reportData: null,
      illustrations: null
    });
  };
};