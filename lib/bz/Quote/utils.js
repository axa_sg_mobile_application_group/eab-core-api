'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _ = require('lodash');

var debug = {
  isDebug: true,
  debugOnly: false,
  messages: ''
};

var returnResult = function returnResult(resultObj) {
  var resultJson = '';
  if (process.env.NODE_ENV == 'development') {
    logger.debug("quotation return result:", resultObj);
  }
  if (debug.isDebug) {
    if (debug.debugOnly) {
      resultJson = JSON.stringify(debug);
    } else {
      resultObj.debug = Object.assign(debug, resultObj.debug || {});
      resultJson = JSON.stringify(resultObj);
    }
  } else {
    resultJson = JSON.stringify(resultObj);
  }
  return resultJson;
};

var getCurrency = function getCurrency(value, sign, decimals) {
  if (!_.isNumber(value)) {
    return value;
  }
  if (!_.isNumber(decimals)) {
    decimals = 2;
  }
  var text = parseFloat(value).toFixed(decimals);
  var parts = text.split('.');
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  parts[0] = (sign && sign.trim().length > 0 ? sign : '') + parts[0];
  return parts.join('.');
};

var getCurrencyByCcy = function getCurrencyByCcy(value, decimals, compCode, ccy) {
  return getCurrency(value, getCcySign(compCode, ccy), decimals);
};

var getCcySign = function getCcySign(compCode, ccy) {
  var currency = _.find(global.optionsMap.ccy.currencies, function (c) {
    return c.compCode === compCode;
  });
  if (currency) {
    var option = _.find(currency.options, function (opt) {
      return opt.value === ccy;
    });
    if (option) {
      return option.symbol;
    }
  }
  return 'S$';
};

var getCurrencySign = function getCurrencySign(ccy) {
  // if (ccy === 'SGD') {
  //   return 'S$';
  // }
  // return '$';
  var sign = '$';
  if (ccy === 'SGD') {
    sign = 'S$ ';
  } else if (ccy === 'USD') {
    sign = 'US$ ';
  } else if (ccy === 'GBP') {
    sign = '£ ';
  } else if (ccy === 'EUR') {
    sign = '€ ';
  } else if (ccy === 'AUD') {
    sign = 'A$ ';
  }
  return sign;
};

if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object' && (typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object') {
  module.exports.debug = debug;
  module.exports.getCurrency = getCurrency;
  module.exports.getCurrencySign = getCurrencySign;
  module.exports.getCurrencyByCcy = getCurrencyByCcy;
  module.exports.returnResult = returnResult;
}