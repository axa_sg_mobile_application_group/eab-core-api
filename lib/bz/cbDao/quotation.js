'use strict';

var _ = require('lodash');

var dao = require('../cbDaoFactory').create();
var utils = require('../utils/RemoteUtils');
var moment = require('moment');
var cFunctions = require('../utils/CommonUtils');
var encryptionHandler = require('../EncryptionHandler.js');

var logger = global.logger || console;

// const encryFields = ['iGender','iDob','iAge','iResidence','iSmoke','iEmail','iOccupClass','iPassCode','pCid','pFirstName','pLastName','pGender','pDob','pAge','pResidence','pSmoke','pEmail','pOccupClass','pPassCode','agent'];
var encryFields = [];

module.exports.queryQuickQuotes = function (compCode, pCid, callback) {
  dao.getViewRange('main', 'quickQuotes', '["' + compCode + '","' + pCid + '"]', '["' + compCode + '","' + pCid + '"]', null, function (result) {
    var quickQuotes = [];
    if (result) {
      _.each(result.rows, function (row) {
        quickQuotes.push(row.value);
      });
    }
    callback(_.sortBy(quickQuotes, function (qq) {
      return new Date(qq.lastUpdateDate);
    }).reverse());
  });
};

module.exports.deleteQuotation = function (quotation, updateViewIndex, callback) {
  dao.delDocWithRev(quotation.id, quotation._rev, function () {
    if (!updateViewIndex) {
      callback && callback();
    } else {
      dao.updateViewIndex('main', 'summaryQuots', function () {
        dao.updateViewIndex('main', 'quickQuotes', function () {
          callback && callback();
        });
      });
    }
  });
};

module.exports.updateQuotViewIndex = function () {
  return new Promise(function (resolve) {
    dao.updateViewIndex('main', 'summaryQuots', function () {
      dao.updateViewIndex('main', 'quickQuotes', resolve);
    });
  });
};

var genQuotationId = function genQuotationId(agentCode, callback) {
  cFunctions.getDocNumber(agentCode, 'quot', function (agentSeq, docSeq) {
    callback('QU' + cFunctions.getAgentIdForDoc(agentSeq) + '-' + cFunctions.getSeqForDoc(docSeq));
  });
};
module.exports.genQuotationId = genQuotationId;

var getQuotation = function getQuotation(quotId, callback) {
  dao.getDoc(quotId, function (quot) {
    if (quot && !quot.error) {
      if (quot.insureds) {
        quot.insureds = Object.keys(quot.insureds).sort().reduce(function (result, key) {
          result[key] = quot.insureds[key];
          return result;
        }, {});
      }
      var ret = quot;
      try {
        ret = utils.decrypt(quot, encryFields);
      } catch (e) {
        logger.error(e);
      }
      callback(ret);
    } else {
      callback(quot);
    }
  });
};
module.exports.getQuotation = getQuotation;

module.exports.updQuotation = function (quotId, newQuot, callback) {
  dao.updDoc(quotId || newQuot._id, newQuot, function (result) {
    dao.updateViewIndex('main', 'summaryQuots');
    callback(result);
  });
};

var upsertQuotation = function upsertQuotation(quotId, newQuot, callback) {
  var docId = quotId;
  if (!newQuot.id) {
    newQuot.id = quotId;
  }
  dao.getDoc(docId, function (quot) {
    if (quot && !quot.error) {
      // reset system key before update
      for (var key in quot) {
        if (key.indexOf('_') == 0) {
          newQuot[key] = quot[key];
        }
      }
    }
    var encrypted = utils.encrypt(newQuot, encryFields);
    dao.updDoc(docId, encrypted, function (result) {
      newQuot._rev = result.rev;
      if (result && result.error) {
        logger.log("ERROR: upsertQuotation failure:", quotId, docId, encrypted._rev);
      } else {
        // update view index
        dao.updateViewIndex("main", "summaryQuots");
      }
      callback(result);
    });
  });
};
module.exports.upsertQuotation = upsertQuotation;

var getQuotationPDF = function getQuotationPDF(quotId, standalone, callback) {
  dao.getAttachment(quotId, standalone ? 'standaloneProposal' : 'proposal', callback);
};
module.exports.getQuotationPDF = getQuotationPDF;

module.exports.upsertAttachment = function (quotId, attId, rev, data) {
  return new Promise(function (resolve) {
    dao.uploadAttachmentByBase64(quotId, attId, rev, data, 'application/pdf', function (result) {
      dao.updateViewIndex('main', 'summaryQuots');
      resolve(result);
    });
  });
};

module.exports.getAttachment = function (quotId, attId) {
  return new Promise(function (resolve) {
    dao.getAttachment(quotId, attId, resolve);
  });
};

module.exports.uploadSignedProposal = function (quotId, data) {
  return new Promise(function (resolve) {
    getQuotationPDF(quotId, false, function (pdfData) {
      var unsignedPdf = pdfData.data;
      getQuotation(quotId, function (quot) {
        dao.uploadAttachmentByBase64(quotId, 'proposal_unsigned', quot._rev, unsignedPdf, 'application/pdf', function (res) {
          if (res && !res.error) {
            dao.uploadAttachmentByBase64(quotId, 'proposal', res.rev, data, 'application/pdf', function (result) {
              resolve(result);
            });
          } else {
            resolve(res);
          }
        });
      });
    });
  });
};

module.exports.removeSignedProposal = function (quotId) {
  return new Promise(function (resolve) {
    dao.getAttachment(quotId, 'proposal_unsigned', function (pdfData) {
      var unsignedPdf = pdfData && pdfData.data;
      if (!unsignedPdf) {
        resolve(false);
      } else {
        getQuotation(quotId, function (quot) {
          dao.uploadAttachmentByBase64(quotId, 'proposal', quot._rev, unsignedPdf, 'application/pdf', function (res) {
            resolve(res);
          });
        });
      }
    });
  });
};

module.exports.getPdfTemplate = function (compCode, pdfCode, callback) {
  dao.getViewRange('main', 'pdfTemplates', '["' + compCode + '","QUOT","' + pdfCode + '"]', '["' + compCode + '","QUOT","' + pdfCode + '"]', null, function (list) {
    if (list && list.rows && list.rows.length) {
      logger.log('list size:' + list.rows.length);

      var now = moment();
      var latestRow = list.rows[0];
      _.each(list.rows, function (row) {
        var template = row.value;
        if (now < moment(template.effDate) || now > moment(template.expDate)) {
          return;
        }
        if (template.version > latestRow.value.version) {
          latestRow = row;
        }
      });

      if (callback) {
        dao.getDoc(latestRow.id, function (doc) {
          callback(doc ? doc : false);
        });
      }
    } else {
      callback(false);
    }
  });
};

module.exports.queryFunds = function (compCode, fundCodes, paymentMethod, mixedAsset, callback) {
  dao.getViewRange('main', 'funds', '["' + compCode + '","0"]', '["' + compCode + '","ZZZZ"]', null, function (result) {
    var funds = [];
    if (result) {
      var versionMap = {};
      _.each(result.rows, function (row) {
        var fund = row.value;
        fund._id = row.id;
        if (fundCodes && fundCodes.indexOf(fund.fundCode) === -1) {
          return;
        }
        if (fund.paymentMethod && fund.paymentMethod.indexOf(paymentMethod) === -1) {
          return;
        }
        if (mixedAsset && fund.isMixedAsset !== 'Y') {
          return;
        }
        var existingFund = versionMap[fund.fundCode];
        if (existingFund) {
          if (fund.version > existingFund.version) {
            funds[existingFund.index] = fund;
            versionMap[fund.fundCode] = {
              version: fund.version,
              index: existingFund.index
            };
          }
        } else {
          funds.push(fund);
          versionMap[fund.fundCode] = {
            version: fund.version,
            index: versionMap.length
          };
        }
      });
    }
    callback(_.sortBy(funds, function (f) {
      return f.fundName && f.fundName.en;
    }));
  });
};

module.exports.getProductHighlightSheet = function (fundId, callback) {
  dao.getAttachment(fundId, 'phs', callback);
};

module.exports.getEmailTemplate = function (callback) {
  dao.getDoc('eBI_email_template', callback);
};

module.exports.getQuotationsByQuotIdList = function (quotIdList, callback) {
  var resAllQuot = [];
  quotIdList.forEach(function (quotId, index) {

    getQuotation(quotId, function (quot) {
      resAllQuot.push(quot);

      if (quotIdList.length == resAllQuot.length) {
        callback(resAllQuot);
      }
    });
  });
};

module.exports.upsertQuotationByQuotDataList = function (upsertDataList, callback) {
  logger.log('INFO: upsertQuotationByQuotDataList', upsertDataList.length);

  var errorList = [];
  var successList = [];
  upsertDataList.forEach(function (quot, index) {
    upsertQuotation(quot._id, quot, function (res) {
      if (res && !res.error) {
        successList.push(res);
      } else {
        logger.error(res);
        errorList.push(res);
      }

      if (upsertDataList.length == successList.length + errorList.length) {
        if (errorList.length == 0) {
          callback({ success: true, result: successList });
        } else {
          callback({ success: false, result: errorList });
        }
      }
    });
  });
};