'use strict';

var _ = require('lodash');
var moment = require('moment');
var dao = require('../cbDaoFactory').create();

var _require = require('../../common/DateUtils'),
    getAges = _require.getAges;

var _require2 = require('../../common/ProductUtils'),
    checkEntryAge = _require2.checkEntryAge,
    getProductId = _require2.getProductId;

var logger = global.logger || console;

module.exports.queryBasicPlansByClient = function (compCode, insured, proposer, params) {
  return new Promise(function (resolve) {
    if (!proposer) {
      proposer = insured;
    }
    var currentDate = new Date();
    var iAges = getAges(currentDate, new Date(insured.dob));
    var oAges = getAges(currentDate, new Date(proposer.dob));
    var now = moment();
    dao.getViewRange('main', 'products', '["' + compCode + '","B","0"]', '["' + compCode + '","B","ZZZ"]', params ? params : null, function (list) {
      var filteredList = [];

      if (list && list.rows && list.rows.length) {
        logger.log('before filter :', list.rows.length);

        var pubDateMap = {};
        for (var i in list.rows) {
          var p = list.rows[i].value;

          if (!canViewProduct(p, insured, iAges, proposer, oAges, params.ccy, now)) {
            continue;
          }

          // set the doc id
          p._id = list.rows[i].id;

          if (pubDateMap[p.covCode]) {
            if (pubDateMap[p.covCode].version > p.version) {
              continue;
            } else {
              filteredList[pubDateMap[p.covCode].index] = p;
              pubDateMap[p.covCode] = {
                version: p.version,
                index: pubDateMap[p.covCode].index
              };
            }
          } else {
            pubDateMap[p.covCode] = {
              pubDate: p.pubDate,
              index: filteredList.length
            };
            filteredList.push(p);
          }
        }
      }
      resolve(filteredList);
    });
  });
};

module.exports.getProductByVersion = function (compCode, covCode, version, callback) {
  dao.getDocFromCacheFirst(compCode + '_product_' + covCode + '_' + version, function (product) {
    callback(product);
  });
};

module.exports.getProduct = function (docId, callback) {
  return new Promise(function (resolve) {
    dao.getDoc(docId, function (product) {
      callback && callback(product);
      resolve(product);
    });
  });
};

module.exports.getAttachment = function (planId, attName, callback) {
  dao.getAttachment(planId, attName, callback);
};

module.exports.getAttachmentWithLang = function (productId, attName, lang, callback) {
  dao.getAttachment(productId, attName + (lang ? '_' + lang : ''), callback);
};

var _getPlanByCovCode = function _getPlanByCovCode(compCode, covCode, planInd, getOldest) {
  return new Promise(function (resolve) {
    var now = moment().valueOf();
    dao.getViewRange('main', 'products', '["' + compCode + '","' + planInd + '","' + covCode + '"]', '["' + compCode + '","' + planInd + '","' + covCode + '"]',
    // null,
    // null,
    null, function (list) {
      if (list && list.rows) {
        var product = null;
        _.each(list.rows, function (row) {
          var p = row.value;
          if (!(moment(p.effDate).valueOf() <= now && moment(p.expDate).valueOf() >= now)) {
            return; // filter it if not match condition
          }
          if (!product || getOldest && p.version < product.version || !getOldest && p.version > product.version) {
            product = p;
          }
        });
        if (product) {
          dao.getDoc(getProductId(product), resolve);
        } else {
          resolve();
        }
      } else {
        resolve();
      }
    });
  });
};
module.exports.getPlanByCovCode = _getPlanByCovCode;

module.exports.prepareLatestProductVersion = function (covCodes) {
  var promises = [];
  _.forEach(covCodes, function (covCode) {
    if (covCodes) {
      promises.push(_getPlanByCovCode('01', covCode, 'B').then(function (doc) {
        if (doc && !doc.error) {
          return {
            success: true,
            doc: doc,
            covCode: covCode
          };
        } else {
          return {
            success: false,
            covCode: covCode
          };
          // throw new Error(`Fail to get product for ${covCode}`);
        }
      }));
    }
  });

  return Promise.all(promises).then(function (result) {
    var product = {};
    _.forEach(result, function (value) {
      if (value.success) {
        product[value.covCode] = {};
        product[value.covCode].success = true;
        product[value.covCode].version = _.get(value.doc, 'version', '1');
      } else {
        product[value.covCode] = {};
        product[value.covCode].success = false;
        product[value.covCode].version = null;
      }
    });
    return product;
  });
};

module.exports.canViewProduct = function (productId, proposer, insured, callback) {
  if (!insured) {
    insured = proposer;
  }
  dao.getDoc(productId, function (product) {
    var currentDate = new Date();
    var pAges = getAges(currentDate, new Date(proposer.dob));
    var iAges = getAges(currentDate, new Date(insured.dob));
    var now = moment();
    if (canViewProduct(product, insured, iAges, proposer, pAges, null, now)) {
      callback(product);
    } else {
      callback(false);
    }
  });
};

var canViewProduct = function canViewProduct(product, insured, iAges, proposer, pAges, ccy, now) {
  var p = product;

  if (!checkEntryAge(p, insured.residenceCountry, pAges, iAges)) {
    logger.log('Products :: canViewProduct :: product filtered as owner age:', p.covCode, p.entryAge);
    return false;
  }

  if (!(p.genderInd === '*' || p.genderInd === insured.gender)) {
    logger.log('Products :: canViewProduct :: product filtered as gender:', p.genderInd);
    return false;
  }

  if (p.ctyGroup.indexOf('*') === -1 && p.ctyGroup.indexOf(insured.residenceCountry) === -1) {
    logger.log('Products :: canViewProduct :: product filtered as city group:', p.covCode, p.ctyGroup);
    return false;
  }

  if (p.smokeInd !== '*' && p.smokeInd !== insured.isSmoker) {
    logger.log('Products :: canViewProduct :: product filtered as smoke:', p.covCode, p.smokeInd);
    return false;
  }

  if (ccy && ccy !== 'ALL') {
    var currency = _.find(p.currencies, function (c) {
      return c.country === '*' || c.country === insured.residenceCountry;
    });
    if (!currency || !_.find(currency.ccy, function (c) {
      return c === ccy;
    })) {
      logger.log('Products :: canViewProduct :: product filtered as ccy:', p.covCode, currency, ccy);
      return false;
    }
  }

  if (now < moment(p.effDate) || now > moment(p.expDate)) {
    logger.log('Products :: canViewProduct :: product filtered as not within date:', p.covCode, p.effDate, p.expDate, now);
    return false;
  }

  return true;
};

module.exports.getProductSuitability = function () {
  return new Promise(function (resolve) {
    dao.getDocFromCacheFirst('productSuitability', resolve);
  });
};