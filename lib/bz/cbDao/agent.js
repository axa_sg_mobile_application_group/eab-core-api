'use strict';

var dao = require('../cbDaoFactory').create();
var _ = require('lodash');
var async = require('async');
var notifyHandler = require('../handler/SystemNotificationHandler');
var logger = global.logger || console;

module.exports.getDocById = function (docId, callback) {
  logger.log('getDocById');
  dao.getDoc(docId, function (result) {
    if (result) {
      callback(result);
    }
  });
};

var getAgentDocId = function getAgentDocId(id) {
  // var agentId = "A-" + (new Buffer(email).toString('base64'));
  var agentId = "U_" + id;
  return agentId;
};

var getAgentSuppDocId = function getAgentSuppDocId(id) {
  return 'UX_' + id;
};

module.exports.getAgentSuppDocId = getAgentSuppDocId;

var getAgentById = function getAgentById(profileId, callback) {
  dao.getDoc(getAgentDocId(profileId), callback);
};
module.exports.getAgentById = getAgentById;

var mergeArray = function mergeArray(fir, sec) {
  var supp = fir;
  var notifications = supp.notifications || [];
  for (var i in sec) {
    var haveCampaign = false;
    if (sec[i].messageId) {
      for (var j in fir.notifications) {
        if (fir.notifications[j].messageId == sec[i].messageId) {
          haveCampaign = true;
          break;
        }
      }
    }
    if (!haveCampaign) {
      if (!sec[i].read) {
        sec[i].read = false;
      }
      notifications.push(sec[i]);
    }
  }
  supp.notifications = notifications;
  return supp;
};

var getLoginAgentById = function getLoginAgentById(profileId, callback) {

  dao.getDoc(getAgentDocId(profileId), function (agent) {
    if (agent && !agent.error) {

      notifyHandler.getValidSystemNotification(agent.compCode, function (notifications) {
        dao.getDoc(getAgentSuppDocId(profileId), function (supp) {

          supp = mergeArray(supp, notifications);
          var loginAgent = Object.assign({}, agent);

          if (supp && !supp.error) {
            loginAgent = Object.assign(loginAgent, supp);
            getEligProducts(loginAgent, function (eligProducts) {
              loginAgent.eligProducts = eligProducts;
              callback(loginAgent);
            });
          } else if (supp.error === 'not_found') {
            var docId = getAgentSuppDocId(profileId);
            dao.updDoc(docId, {}, function (resp) {
              if (resp && !resp.error) {
                loginAgent._id = resp.id;
                loginAgent._rev = resp.rev;
                getEligProducts(loginAgent, function (eligProducts) {
                  loginAgent.eligProducts = eligProducts;
                  callback(loginAgent);
                });
              } else {
                callback(resp);
              }
            });
          } else {
            getEligProducts(loginAgent, function (eligProducts) {
              loginAgent.eligProducts = eligProducts;
              callback(loginAgent);
            });
          }
        });
      });
    } else {
      callback(agent);
    }
  });
};
module.exports.getLoginAgentById = getLoginAgentById;

var getEligProducts = function getEligProducts(agent, callback) {
  dao.getDoc('eligProducts', function (eligProducts) {
    if (eligProducts && eligProducts.mapping) {
      getChannels(function (channels) {
        var channelType = channels.channels[agent.channel].type === 'AGENCY' ? 'AG' : 'FA';
        var mapping = eligProducts.mapping[channelType];
        var covCodes = new Set();
        _.each(mapping['999999999'], function (covCode) {
          covCodes.add(covCode);
        });
        _.each(mapping[agent.profileId], function (covCode) {
          covCodes.add(covCode);
        });
        callback(Array.from(covCodes));
      });
    } else {
      callback(agent.eligProducts);
    }
  });
};

module.exports.updateDoc = function (docId, data, callback) {
  logger.log('updateDoc');
  dao.updDoc(docId, data, function (result) {
    if (result) {
      callback(result);
    } else {
      callback(false);
    }
  });
};

module.exports.getAgentProfile = function (docId, callback) {
  logger.log('getAgentProfile');
  dao.getDoc(docId, function (result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.addAgentProfile = function (docId, user, callback) {
  logger.log('addAgentProfile');
  dao.updDoc(docId, user, function (result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.updateAgentProfile = function (docId, user, callback) {
  logger.log('updateAgentProfile');
  dao.updDoc(docId, user, function (result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.agentLoginAud = function (docId, user, callback) {
  logger.log('agentLoginAud');
  dao.updDoc(docId, user, function (result) {
    if (result) {
      callback(result);
    }
  });
};

var getChannels = function getChannels(callback) {
  dao.getDocFromCacheFirst("channels", function (result) {
    if (result) {
      callback(result);
    } else {
      callback(false);
    }
  });
};
module.exports.getChannels = getChannels;

module.exports.updateProfilePic = function (docId, type, data, callback) {
  dao.getDoc(docId, function (agent) {
    dao.uploadAttachmentByBase64(docId, 'agentProfilePic', agent._rev, data, type, function () {
      callback(true);
    });
  });
};

module.exports.assignEligProductsToAgent = function (agentId, callback) {
  getLoginAgentById(agentId, function (agent) {
    if (agent && !agent.error) {
      callback({ success: true, agent: agent });
    } else {
      callback({ success: false });
    }
  });
};

module.exports.searchAgents = function (compCode, idS, idE, callback) {
  var result = [];
  return new Promise(function (resolve) {
    dao.getViewRange('main', 'agents', '["' + compCode + '","' + idS + '"' + ']', '["' + compCode + '","' + idE + '"' + ']', null, function (pList) {
      if (pList && pList.rows) {
        for (var i = 0; i < pList.rows.length; i++) {
          pList.rows[i].value.docId = pList.rows[i].id;
          result.push(pList.rows[i].value);
        }
      }
      resolve(callback({ success: true, result: result }));
    });
  }).catch(function (e) {
    logger.error(e);
    callback({ success: false });
  });
};

var _getAgents = function _getAgents(compCode, agentCodes) {
  var currentDate = new Date();
  var keys = _.map(agentCodes, function (agentCode) {
    return '["' + compCode + '","agentCode","' + agentCode + '"]';
  });
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then(function (result) {
    var agentMap = {};
    if (result) {
      _.each(result.rows, function (row) {
        var agent = row.value || {};
        agent.isProxying = agent.rawData && currentDate >= new Date(agent.rawData.proxyStartDate) && currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + 1000 * 60 * 60 * 24;
        agentMap[row.value.agentCode] = agent;
      });
    }
    return agentMap;
  }).catch(function (error) {
    logger.error("Error in getAgents->getViewByKeys: ", error);
  });
};

module.exports.getAgents = _getAgents;

var getProxyDownline = function getProxyDownline(compCode, userIds) {
  var keys = _.map(userIds, function (userId) {
    return '["' + compCode + '","proxy","' + userId + '"]';
  });
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then(function (result) {
    var relatedDownline = [];
    if (result) {
      _.each(result.rows, function (row) {
        var agent = row.value || {};
        if (agent.agentCode) {
          relatedDownline.push(agent.agentCode);
        }
      });
    }
    return relatedDownline;
  }).catch(function (error) {
    logger.error("Error in getAgents->getViewByKeys: ", error);
    return [];
  });
};
module.exports.getProxyDownline = getProxyDownline;

var _geAllAgents = function _geAllAgents(compCode) {
  return new Promise(function (resolve) {
    var currentDate = new Date();
    dao.getViewRange('main', 'agentDetails', '["' + compCode + '","agentCode","0"]', '["' + compCode + '","agentCode","ZZZ"]', null, function (result) {
      var agentMap = {};
      if (result) {
        _.each(result.rows, function (row) {
          var agent = row.value || {};
          /**
           * Align with the view 'agents'
           */
          agent.agentName = agent.name;
          if (agent.rawData && agent.rawData.faAdvisorRole !== undefined) {
            agent.faadminCode = agent.rawData.upline2Code;
          }
          /**
           * Align with the view 'agents'
           */
          agent.isProxying = agent.rawData && currentDate >= new Date(agent.rawData.proxyStartDate) && currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + 1000 * 60 * 60 * 24;
          agentMap[row.value.agentCode] = agent;
          if (row.value.rawData) {
            agentMap[row.value.rawData.userId] = agent;
          }
        });
      }
      resolve(agentMap);
    });
  }).catch(function (e) {
    logger.error('Get All Agents Details: ' + e);
  });
};

module.exports.geAllAgents = _geAllAgents;

module.exports.getAgentsByUserId = function (compCode, userIds) {
  var currentDate = new Date();
  var keys = _.map(userIds, function (userId) {
    return '["' + compCode + '","userId","' + userId + '"]';
  });
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then(function (result) {
    var agentMap = {};
    if (result) {
      _.each(result.rows, function (row) {
        var agent = row.value || {};
        agent.isProxying = agent.rawData && currentDate >= new Date(agent.rawData.proxyStartDate) && currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + 1000 * 60 * 60 * 24;
        if (agent.rawData && agent.rawData.userId) {
          agentMap[agent.rawData.userId] = agent;
        }
        agentMap[agent.agentCode] = agent;
      });
    }
    return agentMap;
  }).catch(function (error) {
    logger.error("Error in getAgentsByUserId->getViewByKeys: ", error);
  });
};

module.exports.getAgentsByAgentCode = function (compCode, agentCodes) {
  var currentDate = new Date();
  var keys = _.map(agentCodes, function (agentCode) {
    return '["' + compCode + '","agentCode","' + agentCode + '"]';
  });
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then(function (result) {
    var agentMap = {};
    if (result) {
      _.each(result.rows, function (row) {
        var agent = row.value || {};
        agent.isProxying = agent.rawData && currentDate >= new Date(agent.rawData.proxyStartDate) && currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + 1000 * 60 * 60 * 24;
        if (agent.rawData && agent.rawData.userId) {
          agentMap[agent.rawData.userId] = agent;
        }
        agentMap[agent.agentCode] = agent;
      });
    }
    return agentMap;
  }).catch(function (error) {
    logger.error("Error in getAgentsByAgentCode->getViewByKeys: ", error);
  });
};

module.exports.getMangerProfileByAgentCode = function (compCode, agentCode, cb) {
  async.waterfall([function (callback) {
    _getAgents(compCode, [agentCode]).then(function (agentMap) {
      callback(null, agentMap[agentCode]);
    });
  }, function (agentProfile, callback) {
    _getAgents(compCode, [_.get(agentProfile, 'managerCode')]).then(function (agentMap) {
      callback(null, agentMap[_.get(agentProfile, 'managerCode')]);
    });
  }], function (err, managerProfile) {
    if (err) {
      logger.error("ERROR:: getMangerProfileByAgentCode: ", err);
      cb({});
    } else {
      cb(managerProfile);
    }
  });
};

module.exports.getDirectorProfileByAgentCode = function (compCode, agentCode) {
  return new Promise(function (resolve) {
    async.waterfall([function (callback) {
      _getAgents(compCode, [agentCode]).then(function (agentMap) {
        callback(null, agentMap[agentCode]);
      });
    }, function (agentProfile, callback) {
      _getAgents(compCode, [_.get(agentProfile, 'managerCode')]).then(function (agentMap) {
        callback(null, agentMap[_.get(agentProfile, 'managerCode')]);
      });
    }, function (managerProfile, callback) {
      _getAgents(compCode, [_.get(managerProfile, 'managerCode')]).then(function (agentMap) {
        callback(null, agentMap[_.get(managerProfile, 'managerCode')]);
      });
    }], function (err, directorProfile) {
      if (err) {
        logger.error("ERROR:: getDirectorProfileByAgentCode: ", err);
        resolve({});
      } else {
        resolve(directorProfile);
      }
    });
  });
};

module.exports.getManagerDirectorProfileByAgentCode = function (compCode, agentCode) {
  return new Promise(function (resolve) {
    async.waterfall([function (callback) {
      _getAgents(compCode, [agentCode]).then(function (agentMap) {
        callback(null, agentMap[agentCode]);
      });
    }, function (agentProfile, callback) {
      var profiles = {};
      _getAgents(compCode, [_.get(agentProfile, 'managerCode')]).then(function (agentMap) {
        profiles.agentProfile = agentProfile;
        profiles.managerProfile = agentMap[_.get(agentProfile, 'managerCode')];
        callback(null, profiles);
      });
    }, function (profiles, callback) {
      _getAgents(compCode, [_.get(profiles, 'managerProfile.managerCode')]).then(function (agentMap) {
        profiles.directorProfile = agentMap[_.get(profiles, 'managerProfile.managerCode')];
        callback(null, profiles);
      });
    }], function (err, profiles) {
      if (err) {
        resolve({});
      } else {
        resolve(profiles);
      }
    });
  });
};

var recursiveSearchUpline = function recursiveSearchUpline(compCode, agentCodes, searchingKeys, callback) {
  searchingKeys = _.clone(searchingKeys);
  searchUpline(compCode, searchingKeys).then(function (uplineAgentCodes) {
    // Filter out any non searched agent code
    searchingKeys = _.filter(uplineAgentCodes, function (agentCode) {
      return agentCodes.indexOf(agentCode) === -1;
    });
    agentCodes = _.union(agentCodes, uplineAgentCodes);
    if (searchingKeys.length) {
      recursiveSearchUpline(compCode, agentCodes, searchingKeys, callback);
    } else {
      callback(null, agentCodes);
    }
  }).catch(function (error) {
    logger.error('ERROR in recursiveSearchUpline: ' + error);
    callback(error);
  });
};
module.exports.recursiveSearchUpline = recursiveSearchUpline;

/**
 * Get the downline base on the agentCodes provided, included provided agentCodes in result
 * @param {*} compCode
 * @param {*} agentCodes
 * @param {*} searchingKeys
 * @param {*} callback
 */
var recursiveSearchDownline = function recursiveSearchDownline(compCode, agentCodes, searchingKeys, callback) {
  searchingKeys = _.clone(searchingKeys);
  searchDownline(compCode, searchingKeys).then(function (donwlineAgentArray) {
    // Filter out any non searched agent code
    searchingKeys = _.filter(donwlineAgentArray, function (agentCode) {
      return agentCodes.indexOf(agentCode) === -1;
    });
    agentCodes = _.union(agentCodes, donwlineAgentArray);
    if (searchingKeys.length) {
      recursiveSearchDownline(compCode, agentCodes, searchingKeys, callback);
    } else {
      callback(null, agentCodes);
    }
  }).catch(function (error) {
    logger.error('ERROR in recursiveSearchDownline: ' + error);
    callback(error);
  });
};
module.exports.recursiveSearchDownline = recursiveSearchDownline;

var searchFAFirmDownline = function searchFAFirmDownline(compCode, agentCodeArray, callback) {
  var keys = _.map(agentCodeArray, function (agentCode) {
    return '["' + compCode + '","fafirmCode","' + agentCode + '"]';
  });
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then(function (result) {
    var resultArrary = [];
    if (result && result.rows && result.rows.length > 0) {
      _.each(result.rows, function (row) {
        var agent = row.value || {};
        if (agent.agentCode) {
          resultArrary.push(agent.agentCode);
        }
      });
    }
    return callback(null, resultArrary);
  }).catch(function (error) {
    logger.error('Error in searchFAFirmDownline: ' + error);
    callback(error);
  });
};
module.exports.searchFAFirmDownline = searchFAFirmDownline;

var searchDownline = function searchDownline(compCode, agentCodeArray) {
  var keys = _.map(agentCodeArray, function (agentCode) {
    return '["' + compCode + '","' + agentCode + '"]';
  });
  return dao.getViewByKeys('main', 'managerDownline', keys, null).then(function (result) {
    var resultArrary = [];
    if (result && result.rows && result.rows.length > 0) {
      _.each(result.rows, function (row) {
        var agent = row.value || {};
        if (agent.agentCode) {
          resultArrary.push(agent.agentCode);
        }
      });
    }
    return resultArrary;
  }).catch(function (error) {
    logger.error('Error in searchDownline->getViewByKeys: ' + error);
    return [];
  });
};
module.exports.searchDownline = searchDownline;

var searchUpline = function searchUpline(compCode, agentCodes) {
  var keys = _.map(agentCodes, function (agentCode) {
    return '["' + compCode + '","agentCode","' + agentCode + '"]';
  });
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then(function (result) {
    var managerCodes = [];
    if (result) {
      _.each(result.rows, function (row) {
        var agent = row.value || {};
        managerCodes = _.union(managerCodes, [agent.managerCode]);
      });
    }
    return managerCodes;
  }).catch(function (error) {
    logger.error("Error in searchUpline->getViewByKeys: ", error);
    return [];
  });
};
module.exports.searchUpline = searchUpline;

module.exports.getNotifications = function (agent) {
  return new Promise(function (resolve) {
    var profileId = agent.profileId;

    dao.getDoc(getAgentSuppDocId(profileId), function (suppDoc) {
      if (suppDoc && !suppDoc.error) {
        var notifications = _.filter(_.get(suppDoc, 'notifications', []), function (notice) {
          return !notice.read;
        });
        resolve(notifications);
      } else {
        resolve([]);
      }
    });
  });
};

module.exports.readAllNotifications = function (notifications, messageGroup, agent) {
  return new Promise(function (resolve) {
    var profileId = agent.profileId;

    var suppDocId = getAgentSuppDocId(profileId);
    dao.getDoc(suppDocId, function (suppDoc) {
      if (suppDoc && !suppDoc.error) {

        // old function before doing cr115-one-time-message
        // _.forEach(suppDoc.notifications, notice => {
        //   if(notice.messageGroup == messageGroup) {
        //     if (!notice.read) {
        //       notice.readTime = new Date();
        //     }
        //     notice.read = true;
        //   }
        // });

        for (var i in notifications) {
          if (notifications[i].messageGroup == messageGroup) {
            if (!notifications[i].read) {
              notifications[i].readTime = new Date();
            }
            notifications[i].read = true;
          }
        }
        // if (!suppDoc.notifications) {
        suppDoc.notifications = notifications;
        // } else if (_.isArray(suppDoc.notifications)) {
        //   suppDoc.notifications = _.concat(suppDoc.notifications, notifications);
        // }
        dao.updDoc(suppDocId, suppDoc, function () {
          resolve(suppDoc);
        });
      } else {
        resolve(false);
      }
    });
  });
};