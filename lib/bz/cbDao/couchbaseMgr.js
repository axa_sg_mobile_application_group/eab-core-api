'use strict';

var _ = require('lodash');
var fs = require('fs');
var viewsFolder = './bz/cbDao/views/';

var cbDao = null;
var logger = global.logger || console;

var viewVersionNum = 2;
var init = function init(callback) {
  if (!cbDao) {
    cbDao = require('../cbDaoFactory').create();
  }
  cbDao.init(function (resp) {
    updateViews(callback);
  });
};

var generateViewJson = function generateViewJson(viewsMap, type) {
  var views = {};

  // default to main.js
  if (type === 'invalidateViews') {
    if (!viewsMap) {
      viewsMap = require('./views/invalidateViews.js');
    }
  } else {
    if (!viewsMap) {
      viewsMap = require('./views/main.js');
    }
  }

  _.each(viewsMap, function (viewFunc, viewId) {
    views[viewId] = {
      map: viewFunc + ''
    };
  });

  return JSON.stringify({
    views: views
  });
};

var generateAllViewJsons = function generateAllViewJsons() {
  var viewNames = getAllViews();
  var views = {};
  _.forEach(viewNames, function (viewName) {
    var view = require('./views/' + viewName + '.js');
    views[viewName] = generateViewJson(view);
  });

  return views;
};

var getAllViews = function getAllViews() {
  var files = fs.readdirSync(viewsFolder);
  return _.map(files, function (file) {
    return file.replace('.js', '');
  });
};

var hasDiffView = function hasDiffView(cbViews) {
  return _.find(getViewsMap(), function (viewFunc, viewId) {
    return !cbViews[viewId] || cbViews[viewId].map.indexOf(viewFunc + '') === -1;
  });
};

var getViewsMap = function getViewsMap() {
  var viewName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'main';

  return require('./views/' + viewName + '.js');
};

var updateViews = function updateViews(callback) {
  cbDao.getDoc('_design/main', function (doc) {
    if (!doc || !doc.views || hasDiffView(doc.views)) {
      var views = generateViewJson(null, "main");
      logger.log('CouchbaseMgr :: updateViews :: updating view');
      cbDao.createView('main', views, function (res) {
        logger.log('CouchbaseMgr :: updateViews :: complete:', res);
        if (typeof callback === "function") {
          callback(res);
        }
      });
    } else {
      if (typeof callback === "function") {
        callback(null);
      }
    }
  });
  cbDao.getDoc('_design/invalidateViews', function (doc) {
    if (!doc || !doc.views || hasDiffView(doc.views)) {
      var views = generateViewJson(null, "invalidateViews");
      logger.log('CouchbaseMgr :: updateViews :: updating view');
      cbDao.createView('invalidateViews', views, function (res) {
        logger.log('CouchbaseMgr :: updateViews :: complete:', res);
        if (typeof callback === "function") {
          callback(res);
        }
      });
    } else {
      if (typeof callback === "function") {
        callback(null);
      }
    }
  });
};

var exportViewsJson = function exportViewsJson() {
  return generateViewJson();
};

module.exports = {
  viewVersionNum: viewVersionNum,
  init: init,
  exportViewsJson: exportViewsJson,
  generateAllViewJsons: generateAllViewJsons
};