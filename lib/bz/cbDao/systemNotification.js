'use strict';

var _ = require('lodash');

var dao = require('../cbDaoFactory').create();
var logger = global.logger || console;

var getMessageByIds = function getMessageByIds(compCode, messageIds) {
  var keys = _.map(messageIds, function (messageId) {
    return '["' + compCode + '","messageId","' + messageId + '"]';
  });
  return new Promise(function (resolve) {
    logger.log('INFO:: getMessageByIds');
    dao.getViewByKeys('main', 'systemNotification', keys, null).then(function (result) {
      var validMessage = [];
      _.each(result && result.rows, function (row) {
        if (row.value && row.value.messageId) {
          validMessage.push(row.value);
        }
      });
      resolve(validMessage);
    });
  });
};
module.exports.getMessageByIds = getMessageByIds;

var getValidStartNotification = function getValidStartNotification(compCode, currentDateTime) {
  return new Promise(function (resolve) {
    dao.getViewRange('main', 'systemNotification', '["' + compCode + '","startTime",' + 0 + ']', '["' + compCode + '","startTime",' + currentDateTime + ']', null, function (viewResult) {
      resolve(viewResult);
    });
  });
};
module.exports.getValidStartNotification = getValidStartNotification;

var getValidEndNotification = function getValidEndNotification(compCode, currentDateTime, expiryDate) {
  return new Promise(function (resolve) {
    dao.getViewRange('main', 'systemNotification', '["' + compCode + '","endTime",' + currentDateTime + ']', '["' + compCode + '","endTime",' + expiryDate + ']', null, function (viewResult) {
      resolve(viewResult);
    });
  });
};
module.exports.getValidEndNotification = getValidEndNotification;