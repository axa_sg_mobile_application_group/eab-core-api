'use strict';

var _ = require('lodash');
var dao = require('../cbDaoFactory').create();
var agentDao = require("./agent");
var bDao = require('./bundle');
var _getOr = require('lodash/fp/getOr');
var async = require('async');
var approvalHandler = require('../ApprovalHandler');
var logger = global.logger || console;

module.exports.getSupervisorTemplate = function (approverLevel) {
    return new Promise(function (resolve) {
        if (approverLevel === 'FAADMIN') {
            dao.getDoc('SupervisorFAAdminTemplate', function (template) {
                resolve(template);
            });
        } else if (approverLevel === 'FAAGENT') {
            dao.getDoc('SupervisorFATemplate', function (template) {
                resolve(template);
            });
        } else {
            dao.getDoc('supervisorTemplate', function (template) {
                resolve(template);
            });
        }
    }).catch(function (e) {
        logger.error('ERROR:: GET Supervisor Template :: Doc Ids:: SupervisorFAAdminTemplate, SupervisorFATemplate, supervisorTemplate', e);
    });
};

module.exports.getApprovalPageTemplate = function (role, session, callback) {
    var promiseArr = [];
    var rpTemplate = void 0;
    var apTemplate = void 0;
    var rejTemplate = void 0;
    var reviewPage_selectClientTab_Template = 'reviewPage_selectedClient_tmpl';
    var reviewPage_jwf_Template = 'reviewPage_jwf_tmpl';
    var workbenchDocId = void 0;
    var channelType = _getOr('', 'channel.type', session.agent);
    if (channelType === 'FA') {
        workbenchDocId = 'workbenchTemplate_FA';
    } else {
        workbenchDocId = 'workbenchTemplate';
    }
    if (role === 'FAADMIN') {
        rpTemplate = 'reviewPageFAAdminTemplate';
        apTemplate = 'approvePageFATemplate';
        rejTemplate = 'rejectPageFATemplate';
    } else if (role === 'FAAGENT') {
        rpTemplate = 'reviewPageFATemplate';
        apTemplate = 'approvePageFATemplate';
        rejTemplate = 'rejectPageFATemplate';
    } else {
        rpTemplate = 'reviewPageTemplate';
        apTemplate = 'approvePageTemplate';
        rejTemplate = 'rejectPageTemplate';
    }

    // Get template for review page
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(rpTemplate, function (template) {
            resolve(template);
        });
    }));

    // Get template for approve page
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(apTemplate, function (template) {
            resolve(template);
        });
    }));

    // Get template for reject page
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(rejTemplate, function (template) {
            resolve(template);
        });
    }));

    // Get template for review page selected client tab when submitted the case to rls
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(reviewPage_selectClientTab_Template, function (template) {
            resolve(template);
        });
    }));

    // Get template for review page jwf tab when submitted the case to rls
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(reviewPage_jwf_Template, function (template) {
            resolve(template);
        });
    }));

    // Get template for pending for approval filter fields
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc('approvalTemplate', function (template) {
            resolve(template);
        });
    }));

    // Get template for workbench filter fields
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(workbenchDocId, function (template) {
            resolve(template);
        });
    }));

    // Get caseTemplate for card view in searching page
    promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc('caseTemplate', function (template) {
            resolve(template);
        });
    }));

    Promise.all(promiseArr).then(function (result) {
        callback({ success: true, result: result });
    }).catch(function (error) {
        logger.error("ERROR:: getApprovalPageTemplate->Promise.all: ", error);
    });
};

module.exports.searchApprovalCases = function (compCode, id, filter, callback) {
    var result = [];
    return new Promise(function (resolve, reject) {
        dao.getViewRange('main', 'approvalCases', '["' + compCode + '","' + id + '","' + '0' + '"' + ']', '["' + compCode + '","' + id + '","' + '9' + '"' + ']', null, function (pList) {
            if (pList && pList.rows) {
                for (var i = 0; i < pList.rows.length; i++) {
                    result.push(pList.rows[i].value);
                }
            }
            resolve(callback({ success: true, result: result, filter: filter }));
        });
    }).catch(function (e) {
        logger.error('ERROR:: searchApprovalCases', e);
        callback({ success: false });
    });
};

module.exports.searchApprovalCaseById = function (id, callback) {
    new Promise(function (resolve, reject) {
        dao.getDoc(id, function (foundCase) {
            resolve(foundCase);
        });
    }).then(function (approvalCase) {
        return new Promise(function (resolve) {
            agentDao.getMangerProfileByAgentCode(approvalCase.compCode, approvalCase.agentId, function (managerProfile) {
                resolve({ approvalCase: approvalCase, managerProfile: managerProfile });
            });
        }).catch(function (getManagerExp) {
            logger.error(getManagerExp);
        });
    }).then(function (result) {
        var approvalCase = result.approvalCase;
        var mProfile = result.managerProfile;
        var showStampedInformationStatus = ['A', 'R', 'E', 'PFAFA'];
        _getBundleidByApplication(approvalCase.compCode, approvalCase.applicationId, 'pCid').then(function (pcId) {
            if (showStampedInformationStatus.indexOf(approvalCase.approvalStatus) === -1 || approvalCase.managerName === undefined || _.isEmpty(approvalCase.managerName)) {
                approvalCase.managerName = _getOr('', 'name', mProfile);
            }

            new Promise(function (resolve, reject) {
                var trustedIndividualPathInApplication = 'applicationForm.values.proposer.extra.hasTrustedIndividual';
                dao.getDoc(approvalCase.applicationId, function (applicationJson) {
                    var trustedIndividual = _getOr('N', trustedIndividualPathInApplication, applicationJson);
                    var hasTrustedIndividual = void 0;
                    trustedIndividual === 'Y' ? hasTrustedIndividual = true : hasTrustedIndividual = false;
                    resolve(callback({ success: true, foundCase: approvalCase, hasTrustedIndividual: hasTrustedIndividual }));
                });
            }).catch(function (error) {
                logger.error("ERROR:: searchApprovalCaseById->_getBundleidByApplication: ", error);
            });
        }).catch(function (error) {
            logger.error("ERROR:: searchApprovalCaseById->new Promise: ", error);
        });
    });
};

module.exports.getApprovalCaseByAppId = function (appId) {
    return new Promise(function (resolve) {
        dao.getDoc(appId, function () {
            var appDoc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            var approvalId = appId && appId.indexOf('SA') > -1 ? approvalHandler.getMasterApprovalIdFromMasterApplicationId(appId) : appDoc.policyNumber;
            dao.getDoc(approvalId, function () {
                var foundCase = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                resolve(foundCase);
            });
        });
    }).catch(function (error) {
        logger.error('ERROR:: getApprovalCaseByAppId' + error);
    });
};

module.exports.geteApprovalByAppid = function (id, callback) {
    return new Promise(function (resolve, reject) {
        dao.getDoc(id, function () {
            var appDoc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            resolve(appDoc.policyNumber);
        });
    }).then(function (eApprovalId) {
        return new Promise(function (resolve) {
            dao.getDoc(eApprovalId, function () {
                var foundCase = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                resolve(callback({ success: true, foundCase: foundCase }));
            });
        });
    }).catch(function (error) {
        logger.error("ERROR:: geteApprovalByAppid->new Promise: ", error);
    });
};

module.exports.updateApprovalCaseById = function (id, approvalCase, callback) {
    return new Promise(function (resolve, reject) {
        dao.getDoc(id, function (foundCase) {
            resolve(foundCase);
        });
    }).then(function (appCase) {
        approvalCase._rev = appCase._rev;
        if (appCase._attachments) {
            approvalCase._attachments = appCase._attachments;
        }
        appCase = approvalCase;
        return new Promise(function (resolve) {
            dao.updDoc(id, approvalCase, function (result) {
                if (result && !result.error) {
                    resolve(id);
                } else {
                    resolve(false);
                }
            });
        });
    }).then(function (id) {
        if (id) {
            dao.getDoc(id, function (updatedCase) {
                dao.updateViewIndex("main", "approvalCases");
                callback({ success: true, updatedCase: updatedCase });
            });
        } else {
            callback({ success: false });
        }
    }).catch(function (error) {
        logger.error("Error in updateApprovalCaseById->new Promise: ", error);
    });
};

module.exports.uploadAttachment = function (id, attchId, rev, data, mime, callback) {
    dao.uploadAttachmentByBase64(id, attchId, rev, data, mime, function (result) {
        if (result && !result.error) {
            callback(result.rev);
        } else {
            logger.error('ERROR:: uploadAttachment', _getOr('', 'error', result));
            callback({ success: false });
        }
    });
};

module.exports.getAllDoc = function (docArr, cb) {
    var promiseArr = [];
    docArr.forEach(function (obj) {
        promiseArr.push(new Promise(function (resolve, reject) {
            dao.getDoc(obj, function (doc) {
                resolve(doc);
            });
        }));
    });
    Promise.all(promiseArr).then(function (result) {
        cb({ success: true, result: result });
    }).catch(function (error) {
        logger.error("Error in getAllDoc->Promise.all: ", error);
    });
};

module.exports.createApprovalCase = function (approvalCase, cb) {
    return new Promise(function (resolve, reject) {
        dao.getDoc(approvalCase.approvalCaseId, function (foundCase) {
            resolve(foundCase);
        });
    }).then(function (doc) {
        return new Promise(function (resolve) {
            if (doc._rev) approvalCase._rev = doc._rev;

            dao.updDoc(approvalCase.approvalCaseId, approvalCase, function (result) {
                if (result && !result.error) {
                    resolve(approvalCase.approvalCaseId);
                } else {
                    resolve(undefined);
                }
            });
        });
    }).then(function (id) {
        if (id) {
            dao.getDoc(id, function (createdCase) {
                dao.updateViewIndex("main", "approvalCases");
                cb({ success: true, createdCase: createdCase });
            });
        } else {
            cb({ success: false });
        }
    }).catch(function (error) {
        logger.error("Error in createApprovalCase->new Promise: ", error);
    });
};

module.exports.getPendingForApprovalCaseListLength = function (compCode, id, callback) {
    return new Promise(function (resolve, reject) {
        dao.getViewRange('main', 'approvalCases', '["' + compCode + '","' + id + '","' + '0' + '"' + ']', '["' + compCode + '","' + id + '","' + '9' + '"' + ']', null, function (pList) {
            if (pList && pList.rows) {
                var noOfPending = 0;
                var objStatus = void 0;
                pList.rows.forEach(function (obj) {
                    if (obj.value && ['A', 'R', 'E'].indexOf(obj.value.approvalStatus) === -1) {
                        noOfPending++;
                    }
                });

                resolve(callback({ success: true, length: noOfPending }));
            } else {
                reject(callback({ success: false }));
            }
        });
    }).catch(function (e) {
        logger.error('ERROR:: getPendingForApprovalCaseListLength', e);
        callback({ success: false });
    });
};

module.exports.getApprovalCasesByAgentCode = function (compCode, agentCodes, statusList) {
    var keys = _.map(agentCodes, function (agentCode) {
        return '["' + compCode + '","' + agentCode + '"]';
    });
    return new Promise(function (resolve, reject) {
        var promises = [];
        promises.push(new Promise(function (resolve2) {
            return dao.getViewByKeys('main', 'approvalDetails', keys, null).then(function (result) {
                var approvals = [];
                _.each(result && result.rows, function (row) {
                    if (row.value && (!statusList || statusList.indexOf(row.value.approvalStatus) > -1)) {
                        approvals.push(row.value);
                    }
                });
                resolve2(approvals);
            });
        }));

        promises.push(new Promise(function (resolve2) {
            return dao.getViewByKeys('main', 'masterApprovalDetails', keys, null).then(function (result) {
                var approvals = [];
                _.each(result && result.rows, function (row) {
                    if (row.value && (!statusList || statusList.indexOf(row.value.approvalStatus) > -1)) {
                        approvals.push(row.value);
                    }
                });
                resolve2(approvals);
            });
        }));

        Promise.all(promises).then(function (args) {
            var approvalList = args[0];
            var masterApprovalList = args[1];

            _.remove(approvalList, function (approval) {
                return approval.isShield;
            });
            resolve(_.concat(approvalList, masterApprovalList));
        }).catch(function (error) {
            logger.error('Error in getApprovalCasesByAgentCode->getViewByKeys: ', error);
            reject(error);
        });
    });
};

// module.exports.getApprovalCases = (compCode, statusList) => {
//     return new Promise((resolve) => {
//         let promises = [];
//         promises.push(new Promise((resolve2) => {
//             dao.getViewRange(
//                 'main',
//                 'approvalDetails',
//                 null,
//                 null,
//                 null,
//                 (result) => {
//                 let approvals = [];
//                 _.each(result && result.rows, (row) => {
//                     if (row.value && (!statusList || statusList.indexOf(row.value.approvalStatus) > -1)) {
//                         approvals.push(row.value);
//                     }
//                 });
//                 resolve2(approvals);
//                 }
//             );
//         }));
//         promises.push(new Promise((resolve2) => {
//             dao.getViewRange(
//                 'main',
//                 'masterApprovalDetails',
//                 null,
//                 null,
//                 null,
//                 (result) => {
//                 let approvals = [];
//                 _.each(result && result.rows, (row) => {
//                     if (row.value && (!statusList || statusList.indexOf(row.value.approvalStatus) > -1)) {
//                         approvals.push(row.value);
//                     }
//                 });
//                 resolve2(approvals);
//                 }
//             );
//         }));
//         Promise.all(promises).then((args)=>{
//             let approvalList = args[0];
//             let masterApprovalList = args[1];

//             _.remove(approvalList, (approval) => {
//                 return approval.isShield;
//             });
//             resolve(_.concat(approvalList, masterApprovalList));
//         }).catch((error) => {
//             logger.error('Error in getValidBundleCaseByAgent->getViewByKeys: ', error);
//         });
//     });
// };

module.exports.getValidBundleCaseByAgent = function (compCode, keys) {
    return new Promise(function (resolve) {
        dao.getViewByKeys('main', 'validbundleApplicationsByAgent', keys, null).then(function (result) {
            var validCaseNo = [];
            _.each(result && result.rows, function (row) {
                if (row.value && row.value.applicationDocId) {
                    validCaseNo.push(row.value.applicationDocId);
                } else if (row.value && row.value.quotationDocId) {
                    validCaseNo.push(row.value.quotationDocId);
                }
            });
            resolve(validCaseNo);
        }).catch(function (error) {
            logger.error("Error in getValidBundleCaseByAgent->getViewByKeys: ", error);
        });
    });
};

var _getAgentsProfile = function _getAgentsProfile(compCode, keys) {
    return new Promise(function (resolve) {
        dao.getViewByKeys('main', 'agents', keys, null).then(function (result) {
            var agentProfiles = {};
            _.each(result && result.rows, function (row) {
                if (row.value && row.value.agentCode) {
                    agentProfiles[row.value.agentCode] = row.value;
                }
            });
            resolve(agentProfiles);
        }).catch(function (error) {
            logger.error("Error in getAgentsProfile->getViewByKeys: ", error);
        });
    });
};
module.exports.getAgentsProfile = _getAgentsProfile;

module.exports.getquotationByAgent = function (compCode, keys) {
    return new Promise(function (resolve) {
        dao.getViewByKeys('main', 'quotationByAgent', keys, null).then(function (result) {
            var quotationList = [];
            _.each(result && result.rows, function (row) {
                if (row.value) {
                    quotationList.push(row.value);
                }
            });
            resolve(quotationList);
        }).catch(function (error) {
            logger.error("Error in getquotationByAgent->getViewByKeys: ", error);
        });
    });
};

module.exports.getapplicationByAgent = function (compCode, keys) {
    return new Promise(function (resolve) {
        dao.getViewByKeys('main', 'applicationsByAgent', keys, null).then(function (result) {
            var applicationList = [];
            _.each(result && result.rows, function (row) {
                if (row.value) {
                    applicationList.push(row.value);
                }
            });
            resolve(applicationList);
        });
    }).catch(function (error) {
        logger.error("Error in getapplicationByAgent->getViewByKeys: ", error);
    });
};

var _getBundleidByApplication = function _getBundleidByApplication(compCode, appId, targetId) {
    //To-Do
    if (compCode === 'AG') {
        compCode = '01';
    }
    return new Promise(function (resolve) {
        dao.getViewRange('main', 'bundleApplications', '["' + compCode + '","application","' + 'SUBMITTED' + '","' + appId + '"]', '["' + compCode + '","application","' + 'SUBMITTED' + '","' + appId + '"]', null, function (result) {
            var resultId = '';
            _.each(result && result.rows, function (row) {
                if (row.value) {
                    resultId = _.get(row, 'value.' + targetId);
                }
            });
            resolve(resultId);
        });
    }).catch(function (error) {
        logger.error("ERROR:: in _getBundleidByApplication: ", error);
    });
};
module.exports.getBundleidByApplication = _getBundleidByApplication;