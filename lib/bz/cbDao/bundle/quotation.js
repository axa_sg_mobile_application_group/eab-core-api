'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _ = require('lodash');
var _c = require('./common');
var _a = require('./applications');

var dao = require('../../cbDaoFactory').create();
var nDao = require('../needs');
var quotDao = require('../quotation');
var prodDao = require('../product');
var QuotDriver = require('../../Quote/QuotDriver');
var QuotUtils = require('../../handler/quote/QuotUtils');

var logger = global.logger || console;

var _getAppIdByQuot = function _getAppIdByQuot(cid, quotId) {
  return _c.getCurrentBundle(cid).then(function (bundle) {
    if (!bundle) {
      return null;
    }
    var application = _.find(bundle.applications, function (app) {
      return app.quotationDocId === quotId;
    });
    return application && application.applicationDocId;
  }).catch(function (error) {
    logger.error("Error in _getAppIdByQuot->getCurrentBundle: ", error);
  });
};
module.exports.getAppIdByQuotId = _getAppIdByQuot;

var invalidateQuotationsByFunc = function invalidateQuotationsByFunc(cid, validFunc) {
  return _c.getCurrentBundle(cid).then(function (bundle) {
    if (!bundle) {
      return;
    }
    var promises = [];
    _.each(bundle.applications, function (application) {
      if (application.appStatus !== 'INVALIDATED' && application.quotationDocId) {
        promises.push(new Promise(function (resolve, reject) {
          quotDao.getQuotation(application.quotationDocId, resolve);
        }).then(function (quotation) {
          return Promise.resolve(validFunc(quotation)).then(function (valid) {
            if (valid !== true) {
              _a.invalidateApplication(bundle, application.quotationDocId);
            }
          });
        }));
      }
    });
    return Promise.all(promises).then(function () {
      return new Promise(function (resolve) {
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:invalidateQuotationsByFunc]');
        _c.updateBundle(bundle, resolve);
      });
    });
  });
};

module.exports.invalidateQuotationsByFE = function (cid, fe) {
  return prodDao.getProductSuitability().then(function (suitability) {
    return invalidateQuotationsByFunc(cid, function (quotation) {
      return new QuotDriver().validateQuotSuitability(suitability, { fe: fe }, quotation);
    });
  });
};

module.exports.invalidateQuotationsByNA = function (cid, fna) {
  return prodDao.getProductSuitability().then(function (suitability) {
    return invalidateQuotationsByFunc(cid, function (quotation) {
      var promises = [];
      promises.push(nDao.getItem(cid, nDao.ITEM_ID.PDA));
      promises.push(nDao.getItem(cid, nDao.ITEM_ID.FE));
      promises.push(new Promise(function (resolve) {
        prodDao.getProduct(quotation.baseProductId, resolve);
      }));
      promises.push(QuotUtils.getQuotClients(quotation));
      return Promise.all(promises).then(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 4),
            pda = _ref2[0],
            fe = _ref2[1],
            product = _ref2[2],
            profiles = _ref2[3];

        return new QuotDriver().validateProdSuitability(suitability, { pda: pda, fna: fna, fe: fe }, product, profiles);
      });
    });
  });
};

module.exports.onCreateQuotation = function (agent, cid, confirm) {
  return _c.getCurrentBundle(cid).then(function (bundle) {
    if (bundle && bundle.status >= _c.BUNDLE_STATUS.SIGN_FNA) {
      logger.log('Bundle :: onCreateQuotation :: bundle fna is signed', bundle.id, bundle.status);
      var hasInProgress = _.find(bundle.applications, function (application) {
        return ['SUBMITTED', 'INVALIDATED', 'INVALIDATED_SIGNED'].indexOf(application.appStatus) === -1;
      });
      if (confirm || bundle.status === _c.BUNDLE_STATUS.SUBMIT_APP && !hasInProgress) {
        logger.log('Bundle :: onCreateQuotation :: create new bundle');
        return _c.createNewBundle(cid, agent).then(function (bundles) {
          return {
            bundle: _.find(bundles, function (b) {
              return b.isValid;
            })
          };
        });
      } else {
        return { code: _c.CHECK_CODE.INVALID_BUNDLE };
      }
    } else {
      return { bundle: bundle };
    }
  });
};

var _getProposalFuncPerms = function _getProposalFuncPerms(cid, quotId, isFaChannel) {
  return _c.getCurrentBundle(cid).then(function (bundle) {
    var perms = {
      requote: true,
      clone: false
    };
    if (bundle) {
      var application = _.find(bundle.applications, function (app) {
        return app.quotationDocId === quotId;
      });
      if (!isFaChannel && bundle.status >= _c.BUNDLE_STATUS.SIGN_FNA) {
        perms.requote = false;
      } else {
        perms.requote = !(application && (application.applicationDocId || application.appStatus === 'INVALIDATED'));
      }
      perms.requoteInvalid = application && application.appStatus === 'INVALIDATED';
    }
    return perms;
  });
};
module.exports.getProposalFuncPerms = _getProposalFuncPerms;

var _onSaveQuotation = function _onSaveQuotation(session, cid, quotId, isNewQuotation) {
  return _c.getCurrentBundle(cid).then(function (bundle) {
    if (!bundle) {
      return;
    }
    if (bundle.status < _c.BUNDLE_STATUS.HAVE_BI) {
      bundle.status = _c.BUNDLE_STATUS.HAVE_BI;
    }
    if (isNewQuotation) {
      if (!_.find(bundle.applications, function (app) {
        return app.quotationDocId === quotId;
      })) {
        logger.log('Bundle :: adding quotation to bundle: bundleId = ' + bundle.id + ', quotId = ' + quotId);
        bundle.applications.push({ quotationDocId: quotId });
      }
      bundle.clientChoice.isRecommendationCompleted = false;
    }
    bundle.clientChoice.clientChoiceStep = 0;
    bundle.clientChoice.isAppListChanged = true;
    bundle.clientChoice.isClientChoiceCompleted = false;
    bundle.clientChoice.isBudgetCompleted = false;
    bundle.clientChoice.isAcceptanceCompleted = false;
    //budget will be recalculated if isAppListChanged = true

    logger.log('Bundle :: client choice not completed: bundleId = ' + bundle.id);

    return new Promise(function (resolve) {
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:_onSaveQuotation]');
      _c.updateBundle(bundle, resolve);
    }).then(function () {
      if (session.agent.channel.type !== 'FA') {
        return _a.rollbackApplication2Step1(cid);
      } else {
        return true;
      }
    }).catch(function (error) {
      logger.error("Error in initClientChoice->getCurrentBundle: ", error);
    });
  }).catch(function (error) {
    logger.error("Error in initClientChoice->getCurrentBundle: ", error);
  });
};
module.exports.onSaveQuotation = _onSaveQuotation;

var invalidateQuotation = function invalidateQuotation(bundleId, quotIds) {
  return new Promise(function (resolve) {
    _c.getBundle(null, bundleId, function (bundle) {
      _.each(quotIds, function (quotId) {
        _a.invalidateApplication(bundle, quotId, 'invalidFund');
      });
      logger.log('INFO: BundleUpdate - [cid:' + _.get(bundle, 'pCid') + '; bundleId:' + bundleId + '; quotIds:' + JSON.stringify(quotIds) + '; fn:invalidateQuotation]');
      _c.updateBundle(bundle, function () {
        resolve();
      });
    });
  });
};

module.exports.invalidateApplicationsByFund = function (fundCodes) {
  return new Promise(function (resolve) {
    logger.log('Bundle :: invalidateApplicationsByFund :: fundCodes:', fundCodes);
    dao.getViewRange('main', 'inProgressQuotFunds', null, null, null, resolve);
  }).then(function (result) {
    if (result) {
      var quotMap = {};
      var qId = void 0,
          bId = void 0;
      _.each(result.rows, function (row) {
        var key = row.key,
            value = row.value;

        if (value.bundleId) {
          qId = key[1];
          bId = value.bundleId;
        } else if (key[2]) {
          if (value.quotationId === qId && fundCodes.indexOf(value.fundCode) > -1) {
            logger.log('Bundle :: invalidateApplicationsByFund :: bundleId = ' + bId + ', quot = ' + qId + ', fund: ' + value.fundCode);
            if (!quotMap[bId]) {
              quotMap[bId] = [];
            }
            if (quotMap[bId].indexOf(qId) === -1) {
              quotMap[bId].push(qId);
            }
          }
        }
      });
      return Promise.all(_.map(quotMap, function (quotIds, bundleId) {
        return invalidateQuotation(bundleId, quotIds);
      }));
    }
  }).catch(function (error) {
    logger.error("Error in invalidateApplicationsByFund->invalidateApplicationsByFund: ", error);
  });
};