"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _c = require("./common");
var _a = require("./applications");
var _cl = require("./client");
var _q = require('./quotation');
var _ = require("lodash");

var dao = require('../../cbDaoFactory').create();
var cDao = require('../client');
var nDao = require('../needs');
var clientChoiceHandler = require('../../handler/ClientChoiceHandler');

var logger = global.logger || console;

var _onSaveNeedItem = function _onSaveNeedItem(cid, data, itemId) {
  var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      var status = bundle.status;
      //no fna, pass
      if (status <= _c.BUNDLE_STATUS.HAVE_FNA) {
        resolve({ code: _c.CHECK_CODE.VALID });
      }
      //if no fully-signed case, pass, otherwise, prompt warning message
      else if (status >= _c.BUNDLE_STATUS.FULL_SIGN) {
          var code = _.find(bundle.applications, function (app) {
            return app.isFullySigned && app.appStatus === 'APPLYING' ? true : false;
          }) ? _c.CHECK_CODE.INVALID_BUNDLE : _c.CHECK_CODE.VALID;
          resolve({ code: code });
        }
        //exist fna report, prompt warning
        else if (status === _c.BUNDLE_STATUS.SIGN_FNA) {
            resolve({ code: _c.CHECK_CODE.INVALID_FNA_REPORT });
          } else if (status > _c.BUNDLE_STATUS.HAVE_FNA && status < _c.BUNDLE_STATUS.SIGN_FNA) {
            if (itemId == nDao.ITEM_ID.PDA) {
              _onSavePDA(cid, data, status, extra.tiInfo, resolve);
            } else if (itemId == nDao.ITEM_ID.FE) {
              _onSaveFE(cid, data, resolve);
            } else if (itemId == nDao.ITEM_ID.NA) {
              _onSaveFNA(cid, data, resolve);
            }
          } else {
            resolve({ code: 107 });
          }
    }).catch(function (error) {
      logger.error("Error in _onSaveNeedItem->getCurrentBundle: ", error);
    });
  });
};
module.exports.onSaveNeedItem = _onSaveNeedItem;

var _invalidNeedItem = function _invalidNeedItem(cid, agent, data, itemId) {
  var errObj = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var params = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};

  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      var aid = agent.agentCode;

      var status = bundle.status;
      if (status >= _c.BUNDLE_STATUS.FULL_SIGN) {
        _c.createNewBundle(cid, agent).then(function () {
          _c.getCurrentBundle(cid).then(function (newBundle) {
            resolve(_defineProperty({}, itemId, _.get(newBundle, "fna." + itemId)));
          }).catch(function (error) {
            logger.error("Error in _invalidNeedItem->getCurrentBundle[2]: ", error);
          });
        }).catch(function (error) {
          logger.error("Error in _invalidNeedItem->createNewBundle: ", error);
        });
      } else if (status > _c.BUNDLE_STATUS.HAVE_FNA && status <= _c.BUNDLE_STATUS.SIGN_FNA) {
        var rollbackApplication = function rollbackApplication() {
          return new Promise(function (rollbackResolve) {
            if (status === _c.BUNDLE_STATUS.START_GEN_PDF || status === _c.BUNDLE_STATUS.SIGN_FNA) {
              _a.rollbackApplication2Step1(cid).then(function () {
                _c.getCurrentBundle(cid).then(function (newBundle) {
                  bundle = newBundle;
                  rollbackResolve();
                });
              });
            } else {
              rollbackResolve();
            }
          });
        };
        rollbackApplication().then(function () {
          if (itemId === nDao.ITEM_ID.NA) {
            //check prodType
            nDao.getItem(cid, nDao.ITEM_ID.NA).then(function (oFna) {
              nDao.getItem(cid, nDao.ITEM_ID.PDA).then(function (oPda) {
                var oProdType = _.get(oFna, 'productType.lastProdType');
                var oAspect = _.split(_.get(oFna, 'aspects'), ",");
                var aspect = _.split(_.get(data, 'aspects'), ",");
                var raIds = ["riskPotentialReturn", "avgAGReturn", "smDroped", "alofLosses", "expInvTime", "invPref", "selfSelectedriskLevel"];
                //return true if aspect removed.
                var filterAspect = _.filter(oAspect, function (_a) {
                  return _.lastIndexOf(aspect, _a) === -1;
                });
                //if aspect removed, remove all quotation
                if (filterAspect.length) {
                  logger.log("INFO: Remove all BI", cid);
                  _a.invalidateApplicationByClientId(cid).then(resolve).catch(function (error) {
                    logger.error("Error in _invalidNeedItem->invalidateApplicationByClientId: ", error);
                  });
                } else {
                  var isLastStep = _.get(data, "lastStepIndex") >= 4;
                  //if change RA, invalidate all ILP BIs
                  var prodTypes = _.split(_.get(data, "productType.prodType"), ",");
                  var oProdTypes = _.split(oProdType, ",");
                  var hasILP = prodTypes.indexOf("investLinkedPlans") > -1;
                  var oOCkaPass = _.get(oFna, "ckaSection.owner.passCka");
                  var oCkaPass = _.get(data, "ckaSection.owner.passCka");
                  var hasJoint = _.get(oPda, "applicant") === "joint";
                  var rLv = _.get(data, "raSection.owner.selfSelectedriskLevel");
                  var oRLv = _.get(oFna, "raSection.owner.selfSelectedriskLevel");
                  var aLv = _.get(data, "raSection.owner.assessedRL");
                  var oALv = _.get(oFna, "raSection.owner.assessedRL");
                  var ckaPass = oCkaPass === "Y";
                  var ckaPassChange = oOCkaPass === "Y" && rLv && oOCkaPass !== oCkaPass;
                  var ckaFailChange = rLv && oOCkaPass === "N" && oOCkaPass !== oCkaPass;

                  //invalid if BI if risk accessment fulfill the condition
                  var _invalidByRa = function _invalidByRa() {
                    return new Promise(function (resolve2) {
                      if (hasILP && (ckaPassChange || ckaFailChange || rLv !== oRLv || (!rLv || !ckaPass) && aLv !== oALv)) {
                        _a.onInvalidateApplicationByProductLine(cid, "IL").then(resolve2).catch(function (error) {
                          logger.error("Error in _invalidNeedItem->onInvalidateApplicationByCovCode: ", error);
                        });;
                      } else {
                        resolve2();
                      }
                    });
                  };

                  //invalid if BI if product type fulfill the condition
                  var _invalidByProdType = function _invalidByProdType() {
                    return new Promise(function (resolve2) {
                      if (isLastStep) {
                        _q.invalidateQuotationsByNA(cid, data).then(resolve2).catch(function (error) {
                          logger.error("Error in _invalidNeedItem->invalidateQuotationsByNA: ", error);
                        });;
                      } else {
                        resolve2();
                      }
                    });
                  };

                  _invalidByRa().then(_invalidByProdType).then(resolve).catch(function (error) {
                    logger.error("Error in _invalidNeedItem: ", error);
                  });
                };
              });
            }).catch(function (error) {
              logger.error("Error in _invalidNeedItem->nDao.getItem[NA]: ", error);
            });
          } else if (itemId === nDao.ITEM_ID.FE) {
            //invalidate budget
            nDao.getItem(cid, nDao.ITEM_ID.FE).then(function (oFe) {
              var owner = _.get(data, "owner") || {},
                  oOwner = _.get(oFe, "owner") || {};
              var aRegPremBudget = owner.aRegPremBudget,
                  singPrem = owner.singPrem,
                  cpfOaBudget = owner.cpfOaBudget,
                  cpfSaBudget = owner.cpfSaBudget,
                  cpfMsBudget = owner.cpfMsBudget,
                  srsBudget = owner.srsBudget;
              var oARegPremBudget = oOwner.aRegPremBudget,
                  oSingPrem = oOwner.singPrem,
                  oCpfOaBudget = oOwner.cpfOaBudget,
                  oCpfSaBudget = oOwner.cpfSaBudget,
                  oCpfMsBudget = oOwner.cpfMsBudget,
                  oSrsBudget = oOwner.srsBudget;


              if (_.get(data, 'revisit')) {
                resolve();
              } else {
                _a.resetApplicationResidency(cid, data).then(function () {
                  _q.invalidateQuotationsByFE(cid, data).then(function () {
                    if (aRegPremBudget != oARegPremBudget || singPrem != oSingPrem || cpfOaBudget != oCpfOaBudget || cpfSaBudget != oCpfSaBudget || cpfMsBudget != oCpfMsBudget || srsBudget != oSrsBudget) {
                      clientChoiceHandler.resetClientChoiceBudget(cid).then(resolve);
                    } else {
                      resolve();
                    }
                  }).catch(function (error) {
                    logger.error("Error in _invalidNeedItem->invalidateQuotationsByFE: ", error);
                  });
                }).catch(function (error) {
                  logger.error("Error in _invalidNeedItem->resetApplicationResidency: ", error);
                });
              }
            }).catch(function (error) {
              logger.error("Error in _invalidNeedItem->nDao.getItem[FE]: ", error);
            });
          } else if (itemId === nDao.ITEM_ID.PDA) {
            //find relationship
            var promises = [],
                dependants = _.split(data.dependants, ",");
            cDao.getProfile(cid, true).then(function (profile) {
              if (status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP && errObj.initTi) {
                promises.push(_c.rollbackStatus(cid, _c.BUNDLE_STATUS.HAS_PRE_EAPP));
              }
              //add spouse ID to relationship if applicant = joint
              if (data.applicant === "joint") {
                var spouseId = _.get(_.find(profile.dependants, function (d) {
                  return d.relationship === "SPO";
                }), "cid");
                if (spouseId) {
                  dependants.push(spouseId);
                }
              }
              _.forEach(bundle.applications, function (application) {
                promises.push(new Promise(function (resolve2) {
                  dao.getDoc(application.quotationDocId, function (quotation) {
                    //remove dependents in applicants
                    var performInvalidate = false;
                    if (_.get(quotation, 'quotType') === 'SHIELD') {
                      var laIsNotDependants = false;
                      _.forEach(quotation.insureds, function (la, iCid) {
                        if (dependants.indexOf(iCid) === -1) {
                          laIsNotDependants = true;
                          return false;
                        }
                      });

                      performInvalidate = _.indexOf(_.keys(quotation.insureds), cid) < 0 && laIsNotDependants;
                    } else {
                      performInvalidate = quotation.iCid && quotation.iCid !== cid && dependants.indexOf(quotation.iCid) === -1;
                    }

                    if (performInvalidate) {
                      _a.invalidateApplication(bundle, application.quotationDocId);
                    }
                    resolve2();
                  });
                }));
              });

              Promise.all(promises).then(function (args) {
                logger.log("INFO: BundleUpdate - [cid:" + cid + "; bundleId:" + _.get(bundle, 'id') + "; fn:_invalidNeedItem]");
                _c.updateBundle(bundle, resolve);
              }).catch(function (error) {
                logger.error("Error in _invalidNeedItem->Promise.all: ", error);
              });
            });
          } else {
            resolve();
          }
        });
      } else {
        resolve();
      }
    }).catch(function (error) {
      logger.error("Error in _invalidNeedItem->getCurrentBundle: ", error);
    });
  });
};
module.exports.invalidNeedItem = _invalidNeedItem;

var _onSavePDA = function _onSavePDA(cid, pda, status) {
  var tiInfo = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var resolve = arguments[4];

  nDao.getItem(cid, nDao.ITEM_ID.PDA).then(function (oPda) {
    var exec = function exec() {
      if (oPda.dependants != pda.dependants || pda.applicant == 'single' && pda.applicant != oPda.applicant) {
        //resolve({code: 107})
        resolve({ code: _c.CHECK_CODE.VALID });
      } else {
        resolve({ code: _c.CHECK_CODE.VALID });
      }
    };
    if (status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP) {
      _cl.onSaveTrustedIndividual(cid, tiInfo, pda).then(function (resp) {
        if (resp.code != _c.CHECK_CODE.VALID) {
          resolve({
            //code: 107,
            code: _c.CHECK_CODE.VALID,
            errObj: { initTi: true }
          });
        } else {
          exec();
        }
      }).catch(function (error) {
        logger.error("Error in _onSavePDA->onSaveTrustedIndividual: ", error);
      });
    } else {
      exec();
    }
  }).catch(function (error) {
    logger.error("Error in _onSavePDA->nDao.getItem[PDA]: ", error);
  });
};
module.exports.onSavePDA = _onSavePDA;

var _onSaveFE = function _onSaveFE(cid, fe, resolve) {
  nDao.getItem(cid, nDao.ITEM_ID.FE).then(function (oFe) {
    var feValid = true;
    _.forEach(['singPrem', 'aRegPremBudget', 'cpfSaBudget', 'cpfOaBudget', 'srsBudget'], function (itemId) {
      if (_.get(fe.owner, itemId) != _.get(oFe.owner, itemId)) {
        feValid = false;
      }
    });
    if (!feValid) {
      //resolve({code: 107})
      resolve({ code: _c.CHECK_CODE.VALID });
    } else {
      resolve({ code: _c.CHECK_CODE.VALID });
    }
  }).catch(function (error) {
    logger.error("Error in initClientChoice->nDao.getItem: ", error);
  });
};
module.exports.onSaveFE = _onSaveFE;

var _onSaveFNA = function _onSaveFNA(cid, fna, resolve) {
  nDao.getItem(cid, nDao.ITEM_ID.NA).then(function (oFna) {
    var oProdType = _.get(oFna, 'productType.lastProdType');
    var oAspect = _.split(_.get(oFna, 'aspects'), ",");
    var aspect = _.split(_.get(fna, 'aspects'), ",");
    //return true if aspect removed.
    var filterAspect = _.filter(oAspect, function (_a) {
      return _.lastIndexOf(aspect, _a) === -1;
    });
    if (filterAspect.length || fna.lastStepIndex === 4 && _.get(fna, 'productType.prodType') != oProdType) {
      //resolve({code: 107})
      resolve({ code: _c.CHECK_CODE.VALID });
    } else {
      resolve({ code: _c.CHECK_CODE.VALID });
    }
  }).catch(function (error) {
    logger.error("Error in _onSaveFNA->nDao.getItem[PDA]: ", error);
  });
};
module.exports.onSaveFNA = _onSaveFNA;

var _invalidFna = function _invalidFna(cid) {
  var fnaParam = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var promises = [];
  promises.push(nDao.invalidSection(cid, nDao.ITEM_ID.PDA, fnaParam));
  promises.push(nDao.invalidSection(cid, nDao.ITEM_ID.FE, fnaParam));
  promises.push(nDao.invalidSection(cid, nDao.ITEM_ID.NA, fnaParam));
  return Promise.all(promises);
};

module.exports.invalidFna = _invalidFna;