'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var _c = require('./common');
var _ = require('lodash');

var dao = require('../../cbDaoFactory').create();
var quotDao = require('../quotation');
var appDao = require('../application');
var nDao = require('../needs');
var cDao = require('../client');
var _commonApplication = require('../../handler/application/common');

var _require = require('../../utils/RemoteUtils'),
    callApiComplete = _require.callApiComplete;

var logger = global.logger || console;

var _invalidateApplicationByClientId = function _invalidateApplicationByClientId(cid) {
  return new Promise(function (resolve, reject) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      var promises = [];
      _.forEach(bundle.applications, function (application) {
        var appStatus = application.appStatus,
            applicationDocId = application.applicationDocId,
            quotationDocId = application.quotationDocId;

        if (appStatus === 'APPLYING' || !applicationDocId) {
          _invalidateApplication(bundle, applicationDocId || quotationDocId);
        }
      });
      bundle.isFnaReportSigned = false;
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; rollback:true; fn:_invalidateApplicationByClientId]');
      _c.updateBundle(bundle, function () {
        resolve(bundle.applications);
      });
    }).catch(function (error) {
      logger.error("Error in _invalidateApplicationByClientId->getCurrentBundle: ", error);
    });
  });
};
module.exports.invalidateApplicationByClientId = _invalidateApplicationByClientId;

var _invalidateApplicationByFamilyClientId = function _invalidateApplicationByFamilyClientId(cid, fid) {
  return new Promise(function (resolve, reject) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      var promises = [];
      _.forEach(bundle.applications, function (application) {
        promises.push(new Promise(function (resolve2) {
          var appStatus = application.appStatus,
              applicationDocId = application.applicationDocId,
              quotationDocId = application.quotationDocId;

          dao.getDoc(quotationDocId, function (quotation) {
            var familyClientIdIsLa = _.get(quotation, 'quotType') === 'SHIELD' ? _.indexOf(_.keys(quotation.insureds), fid) >= 0 : quotation.iCid === fid;
            var hasInvalidBI = familyClientIdIsLa && (appStatus === 'APPLYING' || !applicationDocId);
            if (hasInvalidBI) {
              _invalidateApplication(bundle, applicationDocId || quotationDocId);
            }
            resolve2(hasInvalidBI);
          });
        }));
      });
      Promise.all(promises).then(function (args) {
        bundle.isFnaReportSigned = false;
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fid:' + fid + '; rollback:true; fn:_invalidateApplicationByFamilyClientId]');
        _c.updateBundle(bundle, function () {
          //return true if there is an invalidate BI
          resolve(_.find(args, function (arg) {
            return arg === true;
          }));
        });
      }).catch(function (error) {
        logger.error("Error in _invalidateApplicationByFamilyClientId->Promise.all: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in _invalidateApplicationByFamilyClientId->getCurrentBundle: ", error);
    });
  });
};
module.exports.invalidateApplicationByFamilyClientId = _invalidateApplicationByFamilyClientId;

module.exports.onDeleteInvalidateApplications = function (cid, deleteIds, isFaChannel, bundleId) {
  logger.log('INFO: onDeleteInvalidateApplications', cid, deleteIds, isFaChannel);
  return new Promise(function (resolve) {
    _c.getBundle(cid, bundleId, function (bundle) {
      var hasInvalidatedCase = false;
      if (!bundle) {
        resolve();
        return;
      }

      var applications = [];
      if (!deleteIds) {
        applications = bundle.applications;
      } else {
        _.each(bundle.applications, function (app) {
          if ((deleteIds.indexOf(app.applicationDocId) > -1 || deleteIds.indexOf(app.quotationDocId) > -1) && app.appStatus === 'INVALIDATED') {
            hasInvalidatedCase = true;
            // When on deleting the invalidated applicaitons, not push the object to bundle
            // Delete the quotation
            logger.log('INFO: onDeleteInvalidateApplications: BundleUpdate(before) - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + ']; deleteIds:' + deleteIds + '; quotationDocId:' + app.quotationDocId + '; Delete invalidated quotation]');
            quotDao.getQuotation(app.quotationDocId, function (quot) {
              if (quot.id && quot._rev) {
                quotDao.deleteQuotation(quot, false);
              } else {
                logger.error('Failed to get quotation: ' + app.quotationDocId);
              }
            });
          } else {
            applications.push(app);
          }
        });
      }
      if (hasInvalidatedCase) {
        bundle.applications = applications;
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; deleteIds:' + deleteIds + '; rollback:true; fn:onDeleteInvalidateApplications]');
        _c.updateBundle(bundle, function () {
          resolve(bundle.id);
        });
      } else {
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; deleteIds:' + deleteIds + '; No Invalidated Case; fn:onDeleteInvalidateApplications]');
        resolve(bundle.id);
      }
    });
  });
};

module.exports.onDeleteApplications = function (cid, deleteIds, isFaChannel) {
  logger.log('INFO: onDeleteApplications', cid, deleteIds, isFaChannel);
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        resolve();
        return;
      }

      if (!isFaChannel && bundle.status >= _c.BUNDLE_STATUS.FULL_SIGN) {
        resolve(bundle.id);
        return;
      }

      var applications = [];
      if (!deleteIds) {
        applications = bundle.applications;
      } else {
        _.each(bundle.applications, function (app) {
          if (deleteIds.indexOf(app.applicationDocId) > -1) {
            //rollback application to proposal
            logger.log('INFO: BundleUpdate(before) - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + ']; deleteIds:' + deleteIds + '; quotationDocId:' + app.quotationDocId + '; rollback:true; fn:onDeleteApplications; action: removeSignedProposal]');
            quotDao.removeSignedProposal(app.quotationDocId);
            delete app.applicationDocId;
            delete app.appStatus;
            applications.push(app);
          } else if (deleteIds.indexOf(app.quotationDocId) === -1) {
            //delete quotation
            applications.push(app);
          }
        });
      }

      bundle.applications = applications;
      bundle.status = _c.BUNDLE_STATUS.HAVE_BI;

      if (!isFaChannel) {
        bundle.isFnaReportSigned = false;
        bundle.clientChoice.isAppListChanged = true;
        bundle.clientChoice.isClientChoiceCompleted = false;
        bundle.clientChoice.isBudgetCompleted = false;
        bundle.clientChoice.isAcceptanceCompleted = false;
        bundle.clientChoice.clientChoiceStep = bundle.clientChoice.clientChoiceStep > 0 ? 1 : 0;
        //budget will be recalculated if isAppListChanged = true
      }

      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; deleteIds:' + deleteIds + '; rollback:true; fn:onDeleteApplications]');
      _c.updateBundle(bundle, function () {
        resolve(bundle.id);
      });
    });
  });
};

module.exports.onCreateApplication = function (cid, quotId, applicationId) {
  logger.log('INFO: onCreateApplication', cid);
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        resolve();
        return;
      }
      var application = _.find(bundle.applications, function (app) {
        return app.quotationDocId === quotId;
      });
      application.applicationDocId = applicationId;
      application.appStatus = 'APPLYING';
      if (bundle.status < _c.BUNDLE_STATUS.START_APPLY) {
        bundle.status = _c.BUNDLE_STATUS.START_APPLY;
      }
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; quotId:' + quotId + '; applicationId:' + applicationId + '; fn:onCreateApplication]');
      _c.updateBundle(bundle, resolve);
    }).catch(function (error) {
      logger.error("Error in onCreateApplication->getCurrentBundle: ", error);
    });
  });
};

module.exports.onSubmitApplication = function (cid, applicationId) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        resolve();
        return;
      }
      var application = _.find(bundle.applications, function (app) {
        return app.applicationDocId === applicationId;
      });
      application.appStatus = 'SUBMITTED';
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; applicationId:' + applicationId + '; fn:onSubmitApplication]');
      _c.updateBundle(bundle, resolve);
    }).catch(function (error) {
      logger.error("Error in onSubmitApplication->getCurrentBundle: ", error);
    });
  });
};

module.exports.onApplyApplication = function (cid, applicationId) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        resolve();
        return;
      }
      var application = _.find(bundle.applications, function (app) {
        return app.applicationDocId === applicationId;
      });
      application.appStatus = 'APPLYING';
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; applicationId:' + applicationId + '; fn:onApplyApplication]');
      _c.updateBundle(bundle, resolve);
    }).catch(function (error) {
      logger.error("Error in onApplyApplication->getCurrentBundle: ", error);
    });
  });
};

var _invalidateApplication = function _invalidateApplication(bundle, docId, reason) {
  var application = _.find(bundle.applications, function (app) {
    return app.applicationDocId === docId;
  });
  // If it is not appId, then search as quotationId
  if (!application) {
    application = _.find(bundle.applications, function (app) {
      return app.quotationDocId === docId;
    });
  }
  if (application.appStatus === 'APPLYING' || !application.applicationDocId) {
    application.appStatus = 'INVALIDATED' + (application.isFullySigned ? '_SIGNED' : '');
    application.invalidateDate = new Date().getTime(); //Rollback to use (UTC + 8), align with the production data in release 1 and 2
    if (reason) {
      application.invalidateReason = reason;
    }
    bundle.clientChoice.isAppListChanged = true;
    bundle.clientChoice.isClientChoiceCompleted = false;

    if (application.isFullySigned && application.applicationDocId) {
      appDao.getApplication(application.applicationDocId, function (app) {
        var invalidateIds = [];
        if (_.get(app, 'quotation.quotType') === 'SHIELD') {
          invalidateIds.push(application.applicationDocId);
          var childIds = _.get(app, 'childIds');
          invalidateIds = [].concat(_toConsumableArray(invalidateIds), _toConsumableArray(childIds));
        } else {
          invalidateIds.push(application.applicationDocId);
        }

        if (app.isInitialPaymentCompleted) {
          // submit
          logger.debug('DEBUG: Submit invalidated paid application:', application.applicationDocId);
          _.each(invalidateIds, function (invalidateId) {
            appDao.getApplication(invalidateId, function (invalidateApp) {
              invalidateApp.isInvalidated = true;
              invalidateApp.isInvalidatedPaidToRLS = false;

              appDao.updApplication(invalidateApp.id, invalidateApp, function (saveRes) {
                if (_.get(invalidateApp, 'type') === 'application') {
                  callApiComplete("/submitInvalidApp/" + invalidateApp.id, 'GET', {}, {}, true, function (resp) {
                    logger.log('INFO: Submit invalidated paid application:', invalidateApp.id, resp);
                    if (resp && resp.success) {
                      invalidateApp._rev = saveRes.rev;
                      invalidateApp.isSubmittedStatus = true;
                      invalidateApp.applicationSubmittedDate = new Date().getTime();
                      appDao.updApplication(invalidateApp.id, invalidateApp);
                    }
                  });
                }
              });
            });
          });
        } else {
          _.each(invalidateIds, function (invalidateId) {
            appDao.getApplication(invalidateId, function (invalidateApp) {
              invalidateApp.isInvalidated = true;
              appDao.updApplication(invalidateApp.id, invalidateApp);
            });
          });
        }
      });
    }
  }
};
module.exports.invalidateApplication = _invalidateApplication;

var _onInvalidateApplicationById = function _onInvalidateApplicationById(cid, docId) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        resolve();
        return;
      }
      //if no docId, invalidate all application
      if (_.isString(docId)) {
        _invalidateApplication(bundle, docId);
      } else if (_.isArray(docId)) {
        _.forEach(docId, function (_id) {
          _invalidateApplication(bundle, _id);
        });
      } else {
        _.forEach(bundle.applications, function (application) {
          _invalidateApplication(bundle, application.applicationDocId || application.quotationDocId);
        });
      }
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; docId:' + docId + ';fn:_onInvalidateApplicationById]');
      _c.updateBundle(bundle, resolve);
    }).catch(function (error) {
      logger.error("Error in _onInvalidateApplicationById->getCurrentBundle: ", error);
    });
  });
};
module.exports.onInvalidateApplicationById = _onInvalidateApplicationById;

var _onInvalidateApplicationByProductLine = function _onInvalidateApplicationByProductLine(cid, productLine) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      var promises = [];
      _.forEach(bundle.applications, function (application) {
        promises.push(new Promise(function (resolve2) {
          dao.getDoc(application.quotationDocId, function (quotation) {
            if (quotation.productLine === productLine) {
              _invalidateApplication(bundle, application.quotationDocId);
            }
            resolve2();
          });
        }));
      });
      Promise.all(promises).then(function (args) {
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + ']; productLine:' + productLine + ';fn:_onInvalidateApplicationByProductLine');
        _c.updateBundle(bundle, resolve);
      }).catch(function (error) {
        logger.error("Error in _onInvalidateApplicationByProductLine->Promise.all: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in _onInvalidateApplicationByProductLine->getCurrentBundle: ", error);
    });
  });
};

module.exports.onInvalidateApplicationByProductLine = _onInvalidateApplicationByProductLine;

var _onInvalidateApplicationByCovCode = function _onInvalidateApplicationByCovCode(cid, covCode) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      var promises = [];
      _.forEach(bundle.applications, function (application) {
        promises.push(new Promise(function (resolve2) {
          dao.getDoc(application.quotationDocId, function (quotation) {
            var _covCode = _.get(quotation, "plans[0].covCode") || quotation.baseProductCode;
            if (_.isString(covCode) && _covCode == covCode || _.isArray(covCode) && _.split(covCode, ",").indexOf(_covCode) > -1) {
              _invalidateApplication(bundle, application.quotationDocId);
            }
            resolve2();
          });
        }));
      });
      Promise.all(promises).then(function (args) {
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + ']; covCode:' + covCode + '; fn:_onInvalidateApplicationByCovCode');
        _c.updateBundle(bundle, resolve);
      }).catch(function (error) {
        logger.error("Error in _onInvalidateApplicationByCovCode->Promise.all: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in _onInvalidateApplicationByCovCode->getCurrentBundle: ", error);
    });
  });
};

module.exports.onInvalidateApplicationByCovCode = _onInvalidateApplicationByCovCode;

module.exports.getApplicationByBundleId = function (bundleId, appId) {
  return new Promise(function (resolve) {
    dao.getDoc(bundleId, function (bundle) {
      if (bundle) {
        var application = _.find(bundle.applications, function (app) {
          return app.applicationDocId === appId;
        });
        resolve(application);
      } else {
        resolve();
      }
    });
  });
};

module.exports.getApplicationIdsByBundle = function (bundleId) {
  return new Promise(function (resolve) {
    dao.getDoc(bundleId, function (bundle) {
      var applications = [];
      if (bundle) {
        applications = _.map(bundle.applications, function (application) {
          if (!application.applicationDocId) {
            return {
              type: 'quotation',
              id: application.quotationDocId,
              status: application.appStatus,
              invalidateReason: application.invalidateReason
            };
          } else {
            return {
              type: 'application',
              id: application.applicationDocId,
              status: application.appStatus,
              invalidateReason: application.invalidateReason
            };
          }
        });
      }
      resolve(applications);
    });
  });
};

module.exports.getApplication = function (cid, applicationId) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        resolve();
        return;
      }
      var application = _.find(bundle.applications, function (app) {
        return app.applicationDocId === applicationId;
      });
      resolve(application);
    }).catch(function (error) {
      logger.error("Error in getApplication->Promise.all: ", error);
    });
  });
};

module.exports.getDocByQuotId = function (cid, quotId) {
  logger.log('INFO: bundle getDocByQuotId starts');
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        resolve();
      }
      var document = _.find(bundle.applications, function (doc) {
        return doc.quotationDocId === quotId;
      });
      resolve(document);
    }).catch(function (error) {
      logger.error('Error in getDocByQuotId', error);
    });
  });
};

module.exports.rollbackApplication2Proposal = function (cid, fid, hasFNA) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      var promises = [];
      var invalidated = [];
      _.forEach(bundle.applications, function (application) {
        if (!hasFNA && application.isFullySigned) {
          promises.push(_invalidateApplication(bundle, application.applicationDocId));
        } else {
          promises.push(new Promise(function (resolve2) {
            dao.getDoc(application.quotationDocId, function (quot) {
              var familyClientIdIsLa = _.get(quot, 'quotType') === 'SHIELD' ? _.indexOf(_.keys(quot.insureds), fid) >= 0 : quot.iCid === fid;
              if (familyClientIdIsLa || !fid) {
                logger.log('INFO: BundleUpdate(before) - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + ']; fid:' + fid + '; hasFNA:' + hasFNA + '; quotationDocId:' + application.quotationDocId + '; rollback:true; fn:rollbackApplication2Proposal; action: removeSignedProposal]');
                quotDao.removeSignedProposal(application.quotationDocId);
                if (application.appStatus === 'APPLYING') {
                  invalidated.push(application.applicationDocId);
                  _updateApplicationAppStatus2Delete(application.applicationDocId);
                  delete application.applicationDocId;
                  delete application.appStatus;
                }
              }
              resolve2();
            });
          }));
        }
      });

      Promise.all(promises).then(function () {
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + ']; fid:' + fid + '; hasFNA:' + hasFNA + '; rollback:true; fn:rollbackApplication2Proposal]');
        _c.updateBundle(bundle, function () {
          resolve({ applications: bundle.applications, invalidated: invalidated });
        });
      }).catch(function (error) {
        logger.error("Error in rollbackApplication2Proposal->Promise.all: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in rollbackApplication2Proposal->getCurrentBundle: ", error);
    });
  });
};

module.exports.initClientChoice = function (cid) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      bundle.clientChoice.isAppListChanged = true;
      bundle.clientChoice.isClientChoiceCompleted = false;
      bundle.clientChoice.clientChoiceStep = 0;
      bundle.clientChoice.isAcceptanceCompleted = false;
      bundle.clientChoice.isRecommendationCompleted = false;
      bundle.clientChoice.isBudgetCompleted = false;
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; rollback:true; fn:initClientChoice]');
      _c.updateBundle(bundle, resolve);
    }).catch(function (error) {
      logger.error("Error in initClientChoice->getCurrentBundle: ", error);
    });
  });
};

var _rollbackApplication2Step1 = function _rollbackApplication2Step1(cid) {
  return _c.getCurrentBundle(cid).then(function (bundle) {
    if (!bundle || bundle.status > _c.BUNDLE_STATUS.SIGN_FNA) {
      return;
    }
    logger.log("INFO: roll back application:", cid);
    bundle.isFnaReportSigned = false;
    if (bundle.status > _c.BUNDLE_STATUS.START_APPLY) {
      bundle.status = _c.BUNDLE_STATUS.START_APPLY;
    }
    return new Promise(function (resolve) {
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; rollback:true; fn:_rollbackApplication2Step1]');
      _c.updateBundle(bundle, resolve);
    }).then(function () {
      var promises = [];
      //if only 1 bundle and not fully sign, update have sign doc flag for delete client
      promises.push(new Promise(function (resolve2) {
        cDao.getProfile(cid, true).then(function (profile) {
          if (profile.bundle.length === 1 && profile.haveSignDoc) {
            profile.haveSignDoc = _.find(bundle.applications, function (app) {
              return app.isFullySigned;
            });
            cDao.updateProfile(cid, profile).then(resolve2).catch(function (error) {
              logger.error("Error in _rollbackApplication2Step1->updateProfile: ", error);
            });
          } else {
            resolve2();
          }
        }).catch(function (error) {
          logger.error("Error in _rollbackApplication2Step1->getProfile: ", error);
        });
      }));
      _.forEach(bundle.applications, function (application) {
        var quotationDocId = application.quotationDocId,
            applicationDocId = application.applicationDocId,
            appStatus = application.appStatus;

        logger.log('INFO: BundleUpdate(after) - [appStatus: ' + appStatus + '; cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; quotationDocId:' + quotationDocId + '; rollback:true; fn:_rollbackApplication2Step1; action:removeSignedProposal]');
        if (!_.includes(_c.APPSTATUS.VALID_PROPOSAL_SIGNATURE_STATUS, appStatus)) {
          quotDao.removeSignedProposal(quotationDocId);
          if (applicationDocId) {
            promises.push(new Promise(function (resolve2) {
              dao.getDoc(applicationDocId, function (applicationDoc) {
                applicationDoc.appStep = 0;
                applicationDoc.isStartSignature = false;

                if (_.get(applicationDoc, 'quotation.quotType') === 'SHIELD') {
                  //For Shield
                  applicationDoc.appCompletedStep = -1;
                  applicationDoc.isAppFormProposerSigned = false;
                  applicationDoc.isAppFormInsuredSigned = _.times(applicationDoc.isAppFormInsuredSigned.length, _.constant(false));
                } else {
                  //For Normal product
                  applicationDoc.isApplicationSigned = false;
                }
                applicationDoc.isProposalSigned = false;
                applicationDoc.isFullySigned = false;
                dao.updDoc(applicationDocId, applicationDoc, resolve2);
              });
            }));
          }
        }
      });
      return Promise.all(promises);
    }).catch(function (error) {
      logger.error("Error in _rollbackApplication2Step1->new Promise: ", error);
    });
  }).catch(function (error) {
    logger.error("Error in _rollbackApplication2Step1->getCurrentBundle: ", error);
  });
};
module.exports.rollbackApplication2Step1 = _rollbackApplication2Step1;

module.exports.getApplyingApplicationsCount = function (bundleId) {
  logger.log('INFO -- getApplyingApplicationsCount');
  return new Promise(function (resolve) {
    dao.getDoc(bundleId, function (bundle) {
      var applyingApps = _.filter(_.get(bundle, 'applications'), function (bApp) {
        return _.get(bApp, 'appStatus') === 'APPLYING';
      });
      resolve(_.size(applyingApps));
    });
  });
};

var _getApplicationsByBundle = function _getApplicationsByBundle(bundle) {
  var promises = [];

  var _loop = function _loop(i) {
    if (bundle.applications[i].appStatus === 'APPLYING') {
      promises.push(new Promise(function (appResolve) {
        appDao.getApplication(bundle.applications[i].applicationDocId, function (app) {
          appResolve(app);
        });
      }));
    }
  };

  for (var i = 0; i < bundle.applications.length; i++) {
    _loop(i);
  }

  logger.log('INFO: getApplicationsByBundle', promises.length);
  return new Promise(function (resolve, reject) {
    Promise.all(promises).then(function (results) {
      resolve(results);
    }).catch(function (error) {
      logger.error('ERROR: getApplicationsByBundle', error);
    });
  });
};
module.exports.getApplicationsByBundle = _getApplicationsByBundle;

module.exports.updateApplicationClientProfiles = function (profile) {
  return new Promise(function (resolve, reject) {
    if (!profile.cid) {
      resolve();
    } else {
      var cids = _.concat(_.map(profile.dependants, 'cid'), [profile.cid]);
      var promises = [];
      _.forEach(cids, function (cid) {
        promises.push(_updateApplicationClientProfile(cid, profile));
      });
      Promise.all(promises).then(resolve);
    }
  });
};

//cid = proposer cid, profile = client profile (proposer / life assured)
var _updateApplicationClientProfile = function _updateApplicationClientProfile(cid, profile) {
  logger.log('INFO: updateApplicationClientProfile', cid);

  // removeInvalidProfileProp
  // TODO: comment by eric cheung - 24 Jul 2018 to prevent error "Cannot delete property '_rev' of #<Object>" during client update
  // if (profile._rev) delete profile._rev;
  // if (profile._attachments) delete profile._attachments;

  return new Promise(function (resolve, reject) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (bundle.formValues) {
        if (bundle.formValues[profile.cid]) {
          var _bundle$formValues$pr = bundle.formValues[profile.cid],
              personalInfo = _bundle$formValues$pr.personalInfo,
              insurability = _bundle$formValues$pr.insurability;

          var branchInfo = {};
          if (personalInfo && personalInfo.branchInfo) {
            branchInfo = _.cloneDeep(personalInfo.branchInfo);
          }

          bundle.formValues[profile.cid].personalInfo = Object.assign(profile, branchInfo ? { branchInfo: branchInfo } : {});

          //repopulate client profile to insurability question LIFESTYLE01
          if (_.get(bundle.formValues[profile.cid], 'insurability.LIFESTYLE01')) {
            insurability.LIFESTYLE01 = profile.isSmoker;
            if (profile.isSmoker === 'N') {
              insurability.LIFESTYLE01a = '';
              insurability.LIFESTYLE01a_OTH = '';
              insurability.LIFESTYLE01b = '';
              insurability.LIFESTYLE01c = '';
            }
          }
        }

        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:_updateApplicationClientProfile]');
        _c.updateBundle(bundle, function (bResult) {
          if (bResult && !bResult.error) {
            _getApplicationsByBundle(bundle).then(function (apps) {
              var upPromise = [];

              var _loop2 = function _loop2(i) {
                var app = apps[i];
                if (app && !app.error) {
                  var _app$applicationForm$ = app.applicationForm.values,
                      proposer = _app$applicationForm$.proposer,
                      insured = _app$applicationForm$.insured,
                      _insurability = _app$applicationForm$.insurability;

                  var isUpdate = false;

                  //update proposer personalInfo
                  if (proposer.personalInfo.cid == profile.cid) {
                    var _branchInfo = {};
                    if (proposer.personalInfo.branchInfo) {
                      _branchInfo = _.cloneDeep(proposer.personalInfo.branchInfo);
                    }
                    proposer.personalInfo = Object.assign(profile, _branchInfo ? { branchInfo: _branchInfo } : {});

                    //no need to repopulate client profile isSmoker to insurability question LIFESTYLE01, because application will be invalidated
                    isUpdate = true;
                  }

                  //update insured personalInfo
                  for (var j = 0; j < insured.length; j++) {
                    if (insured[j].personalInfo.cid == profile.cid) {
                      insured[j].personalInfo = profile;

                      //no need to repopulate client profile isSmoker to insurability question LIFESTYLE01, because application will be invalidated
                      isUpdate = true;
                    }
                  }

                  if (isUpdate) {
                    upPromise.push(new Promise(function (upResolve) {
                      appDao.upsertApplication(app._id, app, function (upResult) {
                        logger.log('INFO: updateApplicationClientProfile - update Application', app._id, 'for', cid);
                        upResolve(upResult);
                      });
                    }));
                  }
                }
              };

              for (var i = 0; i < apps.length; i++) {
                _loop2(i);
              }

              logger.log('INFO: updateApplicationClientProfile - before update Applications', cid, '[num=', upPromise.length, ']');
              Promise.all(upPromise).then(function (result) {
                logger.log('INFO: updateApplicationClientProfile - end [RETURN=1]', cid);
                resolve();
              }).catch(function (error) {
                logger.error('ERROR: updateApplicationClientProfile [RETURN=-1]', cid, error);
              });
            }).catch(function (error) {
              logger.error('ERROR: updateApplicationClientProfile - _getApplicationsByBundle', cid, error);
            });
          } else {
            logger.error('ERROR: updateApplicationClientProfile - end [RETURN=-2]', cid, _.get(bResult, 'error'));
            resolve();
          }
        });
      } else {
        resolve();
      }
    });
  });
};

module.exports.updateApplicationClientProfile = _updateApplicationClientProfile;

module.exports.getFormValuesByCid = function (cid) {
  logger.log('INFO: getFormValuesByCid starts');
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (bundle && bundle.formValues) {
        logger.log('INFO: getFormValuesByCid -- succeeds');
        resolve(bundle.formValues);
      } else {
        logger.log('INFO: getFormValuesByCid -- cannot find formValues');
        resolve();
      }
    }).catch(function (error) {
      logger.error('ERROR: getFormValuesByCid', error);
      resolve();
    });
  });
};
// FUNCTION ENDS: getFormValuesByCid


var _getApplicationExisitingPolicies = function _getApplicationExisitingPolicies(cid, fe, bundle) {
  logger.log('INFO: getApplicationExisitingPolicies - start');

  return new Promise(function (resolve, reject) {
    var nPromises = [];
    var nCids = [];

    var _loop3 = function _loop3(clientId) {
      nCids.push(clientId);
      nPromises.push(new Promise(function (nResolve, nReject) {
        nDao.getExistingPoliciesFromFe(cid, clientId, fe).then(function (ePolicies) {
          nResolve(ePolicies);
        }).catch(function (error) {
          logger.error('ERROR: getApplicationExisitingPolicies - getExistingPoliciesFromFe', error);
          nReject(error);
        });
      }));
    };

    for (var clientId in bundle.formValues) {
      _loop3(clientId);
    }

    Promise.all(nPromises).then(function (policies) {
      var result = {};
      for (var i = 0; i < policies.length; i++) {
        var ePolicies = policies[i];
        result[nCids[i]] = Object.assign(ePolicies, {
          havExtPlans: ePolicies.existLife > 0 || ePolicies.existTpd > 0 || ePolicies.existCi > 0 ? "Y" : "N",
          havAccPlans: ePolicies.existPaAdb > 0 ? "Y" : "N"
        });
      }
      logger.log('INFO: getApplicationExisitingPolicies - end');
      resolve(result);
    });
  }).catch(function (error) {
    logger.error('ERROR: getApplicationExisitingPolicies', error);
  });
};

//cid = proposer id
module.exports.resetApplicationResidency = function (cid, fe) {
  logger.log('INFO: resetApplicationResidency - start', cid);

  return new Promise(function (resolve, reject) {
    _c.getCurrentBundle(cid).then(function (bundle) {

      var nPromises = [];
      var nCids = [];

      if (bundle.formValues) {
        for (var clientId in bundle.formValues) {
          var formValues = bundle.formValues[clientId];

          // reset bundle residency
          if (formValues.residency) {
            formValues.residency = {};
          }

          // reset bundle foreigner
          if (formValues.foreigner) {
            formValues.foreigner = {};
          }
        }

        _getApplicationExisitingPolicies(cid, fe, bundle).then(function (newExPolicies) {
          for (var _clientId in bundle.formValues) {
            var _formValues = bundle.formValues[_clientId];
            _formValues.policies = _.assign(_formValues.policies, {
              existLife: newExPolicies[_clientId].existLife,
              existTpd: newExPolicies[_clientId].existTpd,
              existCi: newExPolicies[_clientId].existCi,
              existPaAdb: newExPolicies[_clientId].existPaAdb,
              existTotalPrem: newExPolicies[_clientId].existTotalPrem,
              havExtPlans: newExPolicies[_clientId].havExtPlans,
              havAccPlans: newExPolicies[_clientId].havAccPlans,
              existLifeAXA: _.get(_formValues, 'policies.existLife', false) && _formValues.policies.existLife !== newExPolicies[_clientId].existLife ? 0 : _.get(_formValues, 'policies.existLifeAXA', 0),
              existTpdAXA: _.get(_formValues, 'policies.existTpd', false) && _formValues.policies.existTpd !== newExPolicies[_clientId].existTpd ? 0 : _.get(_formValues, 'policies.existTpdAXA', 0),
              existCiAXA: _.get(_formValues, 'policies.existCi', false) && _formValues.policies.existCi !== newExPolicies[_clientId].existCi ? 0 : _.get(_formValues, 'policies.existCiAXA', 0),
              existPaAdbAXA: _.get(_formValues, 'policies.existPaAdb', false) && _formValues.policies.existPaAdb !== newExPolicies[_clientId].existPaAdb ? 0 : _.get(_formValues, 'policies.existPaAdbAXA', 0),
              existTotalPremAXA: _.get(_formValues, 'policies.existTotalPrem', false) && _formValues.policies.existTotalPrem !== newExPolicies[_clientId].existTotalPrem ? 0 : _.get(_formValues, 'policies.existTotalPremAXA', 0),

              existLifeOther: _.get(_formValues, 'policies.existLife', false) && _formValues.policies.existLife !== newExPolicies[_clientId].existLife ? 0 : _.get(_formValues, 'policies.existLifeOther', 0),
              existTpdOther: _.get(_formValues, 'policies.existTpd', false) && _formValues.policies.existTpd !== newExPolicies[_clientId].existTpd ? 0 : _.get(_formValues, 'policies.existTpdOther', 0),
              existCiOther: _.get(_formValues, 'policies.existCi', false) && _formValues.policies.existCi !== newExPolicies[_clientId].existCi ? 0 : _.get(_formValues, 'policies.existCiOther', 0),
              existPaAdbOther: _.get(_formValues, 'policies.existPaAdb', false) && _formValues.policies.existPaAdb !== newExPolicies[_clientId].existPaAdb ? 0 : _.get(_formValues, 'policies.existPaAdbOther', 0),
              existTotalPremOther: _.get(_formValues, 'policies.existTotalPrem', false) && _formValues.policies.existTotalPrem !== newExPolicies[_clientId].existTotalPrem ? 0 : _.get(_formValues, 'policies.existTotalPremOther', 0)
            }, _.get(_formValues, 'policies.havPndinApp') ? {
              havPndinApp: '',
              pendingLife: 0,
              pendingCi: 0,
              pendingTpd: 0,
              pendingPaAdb: 0,
              pendingTotalPrem: 0
            } : {}, _.get(_formValues, 'policies.ROP_01') ? {
              ROP01_DATA: [],
              ROP_DECLARATION_01: '',
              ROP_DECLARATION_02: ''
            } : {});
          }

          logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:resetApplicationResidency]');
          _c.updateBundle(bundle, function (bResult) {
            if (bResult && !bResult.error) {
              _getApplicationsByBundle(bundle).then(function (apps) {
                var upPromise = [];

                var _loop4 = function _loop4(i) {
                  var app = apps[i];
                  if (app && !app.error) {
                    var _app$applicationForm$2 = app.applicationForm.values,
                        proposer = _app$applicationForm$2.proposer,
                        insured = _app$applicationForm$2.insured;

                    // reset proposer residency

                    if (proposer.residency) {
                      proposer.residency = {};
                    }

                    // reset proposer foreigner
                    if (proposer.foreigner) {
                      proposer.foreigner = {};
                    }

                    // reset proposer policies
                    if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                      proposer.policies = _.assign({}, { ROP_01: _.get(proposer.policies, 'ROP_01', '') });
                    } else {
                      proposer.policies = _.assign(proposer.policies, {
                        existLife: newExPolicies[cid].existLife,
                        existTpd: newExPolicies[cid].existTpd,
                        existCi: newExPolicies[cid].existCi,
                        existPaAdb: newExPolicies[cid].existPaAdb,
                        existTotalPrem: newExPolicies[cid].existTotalPrem,
                        havExtPlans: newExPolicies[cid].havExtPlans,
                        havAccPlans: newExPolicies[cid].havAccPlans,

                        existLifeAXA: _.get(proposer, 'policies.existLife', false) && proposer.policies.existLife !== newExPolicies[cid].existLife ? 0 : _.get(proposer, 'policies.existLifeAXA', 0),
                        existTpdAXA: _.get(proposer, 'policies.existTpd', false) && proposer.policies.existTpd !== newExPolicies[cid].existTpd ? 0 : _.get(proposer, 'policies.existTpdAXA', 0),
                        existCiAXA: _.get(proposer, 'policies.existCi', false) && proposer.policies.existCi !== newExPolicies[cid].existCi ? 0 : _.get(proposer, 'policies.existCiAXA', 0),
                        existPaAdbAXA: _.get(proposer, 'policies.existPaAdb', false) && proposer.policies.existPaAdb !== newExPolicies[cid].existPaAdb ? 0 : _.get(proposer, 'policies.existPaAdbAXA', 0),
                        existTotalPremAXA: _.get(proposer, 'policies.existTotalPrem', false) && proposer.policies.existTotalPrem !== newExPolicies[cid].existTotalPrem ? 0 : _.get(proposer, 'policies.existTotalPremAXA', 0),

                        existLifeOther: _.get(proposer, 'policies.existLife', false) && proposer.policies.existLife !== newExPolicies[cid].existLife ? 0 : _.get(proposer, 'policies.existLifeOther', 0),
                        existTpdOther: _.get(proposer, 'policies.existTpd', false) && proposer.policies.existTpd !== newExPolicies[cid].existTpd ? 0 : _.get(proposer, 'policies.existTpdOther', 0),
                        existCiOther: _.get(proposer, 'policies.existCi', false) && proposer.policies.existCi !== newExPolicies[cid].existCi ? 0 : _.get(proposer, 'policies.existCiOther', 0),
                        existPaAdbOther: _.get(proposer, 'policies.existPaAdb', false) && proposer.policies.existPaAdb !== newExPolicies[cid].existPaAdb ? 0 : _.get(proposer, 'policies.existPaAdbOther', 0),
                        existTotalPremOther: _.get(proposer, 'policies.existTotalPrem', false) && proposer.policies.existTotalPrem !== newExPolicies[cid].existTotalPrem ? 0 : _.get(proposer, 'policies.existTotalPremOther', 0)

                      }, _.get(proposer, "policies.havPndinApp") ? {
                        havPndinApp: '',
                        pendingLife: 0,
                        pendingCi: 0,
                        pendingTpd: 0,
                        pendingPaAdb: 0,
                        pendingTotalPrem: 0
                      } : {});
                    }

                    for (var j = 0; j < insured.length; j++) {
                      var la = insured[j];

                      // reset life assured residency
                      if (la.residency) {
                        la.residency = {};
                      }

                      // reset life assured foreigner
                      if (la.foreigner) {
                        la.foreigner = {};
                      }

                      //reset life assured policies
                      if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                        la.policies = _.assign({}, { ROP_01: _.get(la.policies, 'ROP_01', '') });
                      } else {
                        la.policies = _.assign(la.policies, {
                          existLife: newExPolicies[la.personalInfo.cid].existLife,
                          existTpd: newExPolicies[la.personalInfo.cid].existTpd,
                          existCi: newExPolicies[la.personalInfo.cid].existCi,
                          existPaAdb: newExPolicies[la.personalInfo.cid].existPaAdb,
                          existTotalPrem: newExPolicies[la.personalInfo.cid].existTotalPrem,
                          havExtPlans: newExPolicies[la.personalInfo.cid].havExtPlans,
                          havAccPlans: newExPolicies[la.personalInfo.cid].havAccPlans,

                          existLifeAXA: _.get(la, 'policies.existLife', false) && la.policies.existLife !== newExPolicies[la.personalInfo.cid].existLife ? 0 : _.get(la, 'policies.existLifeAXA', 0),
                          existTpdAXA: _.get(la, 'policies.existTpd', false) && la.policies.existTpd !== newExPolicies[la.personalInfo.cid].existTpd ? 0 : _.get(la, 'policies.existTpdAXA', 0),
                          existCiAXA: _.get(la, 'policies.existCi', false) && la.policies.existCi !== newExPolicies[la.personalInfo.cid].existCi ? 0 : _.get(la, 'policies.existCiAXA', 0),
                          existPaAdbAXA: _.get(la, 'policies.existPaAdb', false) && la.policies.existPaAdb !== newExPolicies[la.personalInfo.cid].existPaAdb ? 0 : _.get(la, 'policies.existPaAdbAXA', 0),
                          existTotalPremAXA: _.get(la, 'policies.existTotalPrem', false) && la.policies.existTotalPrem !== newExPolicies[la.personalInfo.cid].existTotalPrem ? 0 : _.get(la, 'policies.existTotalPremAXA', 0),

                          existLifeOther: _.get(la, 'policies.existLife', false) && la.policies.existLife !== newExPolicies[la.personalInfo.cid].existLife ? 0 : _.get(la, 'policies.existLifeOther', 0),
                          existTpdOther: _.get(la, 'policies.existTpd', false) && la.policies.existTpd !== newExPolicies[la.personalInfo.cid].existTpd ? 0 : _.get(la, 'policies.existTpdOther', 0),
                          existCiOther: _.get(la, 'policies.existCi', false) && la.policies.existCi !== newExPolicies[la.personalInfo.cid].existCi ? 0 : _.get(la, 'policies.existCiOther', 0),
                          existPaAdbOther: _.get(la, 'policies.existPaAdb', false) && la.policies.existPaAdb !== newExPolicies[la.personalInfo.cid].existPaAdb ? 0 : _.get(la, 'policies.existPaAdbOther', 0),
                          existTotalPremOther: _.get(la, 'policies.existTotalPrem', false) && la.policies.existTotalPrem !== newExPolicies[la.personalInfo.cid].existTotalPrem ? 0 : _.get(la, 'policies.existTotalPremOther', 0)

                        }, _.get(la, "policies.havPndinApp") ? {
                          havPndinApp: "",
                          pendingLife: 0,
                          pendingCi: 0,
                          pendingTpd: 0,
                          pendingPaAdb: 0,
                          pendingTotalPrem: 0
                        } : {});
                      }
                    }

                    upPromise.push(new Promise(function (upResolve) {
                      appDao.upsertApplication(app._id, app, function (upResult) {
                        logger.log('INFO: resetApplicationResidency - update Application', app._id, 'for', cid);
                        upResolve(upResult);
                      });
                    }));
                  }
                };

                for (var i = 0; i < apps.length; i++) {
                  _loop4(i);
                }

                logger.log('INFO: resetApplicationResidency - before update Application', cid, '[num=', upPromise.length, ']');
                Promise.all(upPromise).then(function (result) {
                  logger.log('INFO: resetApplicationResidency - end [RETURN=1]', cid);
                  resolve();
                }).catch(function (error) {
                  logger.error('ERROR: resetApplicationResidency - end [RETURN=-1]', cid, error);
                });
              }).catch(function (error) {
                logger.error('ERROR: resetApplicationResidency - _getApplicationsByBundle', cid, error);
              });
            } else {
              logger.error('ERROR: resetApplicationResidency - end [RETURN=-2]', cid, _.get(bResult, 'error'));
              resolve();
            }
          });
        }).catch(function (error) {
          logger.error('ERROR: resetApplicationResidency - getApplicationExisitingPolicies', cid, error);
        });
      } else {
        logger.log('INFO: resetApplicationResidency - end [RETURN=2]', cid);
        resolve();
      }
    });
  });
};

var _preformRollbackApplication = function _preformRollbackApplication(cid) {
  logger.log('INFO: _preformRollbackApplication');
  return new Promise(function (resolve, reject) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      //rollback signed Pdf
      var status = bundle.status;
      if (status === _c.BUNDLE_STATUS.START_GEN_PDF || status === _c.BUNDLE_STATUS.SIGN_FNA) {
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; rollback:true; fn:_preformRollbackApplication]');
        _rollbackApplication2Step1(cid).then(resolve);
      } else {
        resolve();
      }
    }).catch(function (error) {
      logger.log('ERROR: preformRollbackApplication', error);
    });
  });
};

var _updateApplicationReplacementOfPolicies = function _updateApplicationReplacementOfPolicies(cid, newData) {
  logger.log('INFO: updateApplicationReplacementOfPolicies - start', cid);
  return new Promise(function (resolve, reject) {
    _preformRollbackApplication(cid).then(function () {
      _c.getCurrentBundle(cid).then(function (bundle) {
        //reset bundle formValue for prepareReplacementOfPolicies
        if (bundle.formValues) {
          for (var i = 0; i < newData.length; i++) {
            var nData = newData[i];
            if (nData.bundle) {
              for (var clientId in nData.bundle) {
                logger.log('INFO: updateApplicationReplacementOfPolicies - set bundle formValues', clientId);

                if (nData.bundle[clientId]) {
                  var currentBundle = nData.bundle[clientId];
                  var replaceTotalArr = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
                  var isAllZeroAXA = true;
                  var isAllZeroOther = true;
                  for (var _i = 0; _i < 5; _i++) {
                    var keyTotal = replaceTotalArr[_i];
                    var keyAXA = replaceTotalArr[_i] + 'AXA';
                    var keyOther = replaceTotalArr[_i] + 'Other';
                    if (Number(currentBundle[keyAXA]) + Number(currentBundle[keyOther]) !== Number(currentBundle[keyTotal])) {
                      currentBundle[keyAXA] = 0;
                      currentBundle[keyOther] = 0;
                    }
                    isAllZeroAXA = isAllZeroAXA && Number(currentBundle[keyAXA]) === 0;
                    isAllZeroOther = isAllZeroOther && Number(currentBundle[keyOther]) === 0;
                  }
                  if (isAllZeroAXA) {
                    currentBundle['replacePolTypeAXA'] = '-';
                  }
                  if (isAllZeroOther) {
                    currentBundle['replacePolTypeOther'] = '-';
                  }
                }
                if (bundle.formValues[clientId]) {
                  bundle.formValues[clientId].policies = nData.bundle[clientId];
                } else {
                  bundle.formValues[clientId] = {
                    policies: nData.bundle[clientId]
                  };
                }
              }
            }
          }
        }

        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:updateApplicationReplacementOfPolicies]');
        _c.updateBundle(bundle, function (bResult) {
          if (bResult && !bResult.error) {
            _getApplicationsByBundle(bundle).then(function (apps) {
              var promises = [];

              var _loop5 = function _loop5(_i2) {
                var app = apps[_i2];
                if (app && !app.error) {
                  var _nData = newData.find(function (dataset) {
                    return dataset.application && dataset.application._id == app._id;
                  });

                  if (_nData) {
                    var _nData$application$ap = _nData.application.applicationForm.values,
                        proposer = _nData$application$ap.proposer,
                        insured = _nData$application$ap.insured;

                    if (proposer.policies) {
                      var policies = proposer.policies;
                      var _replaceTotalArr = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
                      var _isAllZeroAXA = true;
                      var _isAllZeroOther = true;
                      for (var _i3 = 0; _i3 < 5; _i3++) {
                        var _keyTotal = _replaceTotalArr[_i3];
                        var _keyAXA = _replaceTotalArr[_i3] + 'AXA';
                        var _keyOther = _replaceTotalArr[_i3] + 'Other';
                        if (Number(policies[_keyAXA]) + Number(policies[_keyOther]) !== Number(policies[_keyTotal])) {
                          policies[_keyAXA] = 0;
                          policies[_keyOther] = 0;
                        }
                        _isAllZeroAXA = _isAllZeroAXA && Number(policies[_keyAXA]) === 0;
                        _isAllZeroOther = _isAllZeroOther && Number(policies[_keyOther]) === 0;
                      }
                      if (_isAllZeroAXA) {
                        policies['replacePolTypeAXA'] = '-';
                      }
                      if (_isAllZeroOther) {
                        policies['replacePolTypeOther'] = '-';
                      }
                    }
                    for (var j = 0; j < insured.length; j++) {
                      var la = insured[j];
                      if (la.policies) {
                        var _policies = la.policies;
                        var _replaceTotalArr2 = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
                        var _isAllZeroAXA2 = true;
                        var _isAllZeroOther2 = true;
                        for (var _i4 = 0; _i4 < 5; _i4++) {
                          var _keyTotal2 = _replaceTotalArr2[_i4];
                          var _keyAXA2 = _replaceTotalArr2[_i4] + 'AXA';
                          var _keyOther2 = _replaceTotalArr2[_i4] + 'Other';
                          if (Number(_policies[_keyAXA2]) + Number(_policies[_keyOther2]) !== Number(_policies[_keyTotal2])) {
                            _policies[_keyAXA2] = 0;
                            _policies[_keyOther2] = 0;
                          }
                          _isAllZeroAXA2 = _isAllZeroAXA2 && Number(_policies[_keyAXA2]) === 0;
                          _isAllZeroOther2 = _isAllZeroOther2 && Number(_policies[_keyOther2]) === 0;
                        }
                        if (_isAllZeroAXA2) {
                          _policies['replacePolTypeAXA'] = '-';
                        }
                        if (_isAllZeroOther2) {
                          _policies['replacePolTypeOther'] = '-';
                        }
                      }
                    }
                    app.applicationForm = _nData.application.applicationForm;

                    //reset residency menu completeness
                    var _app$applicationForm$3 = app.applicationForm.values,
                        completedMenus = _app$applicationForm$3.completedMenus,
                        checkedMenu = _app$applicationForm$3.checkedMenu;

                    if (completedMenus && completedMenus.length > 0) {
                      var completedIndex = completedMenus.indexOf('menu_residency');
                      if (completedIndex > -1) {
                        completedMenus.splice(completedIndex, 1);
                      }
                    }
                    if (checkedMenu && checkedMenu.length > 0) {
                      var checkedIndex = checkedMenu.indexOf('menu_residency');
                      if (checkedIndex > -1) {
                        checkedMenu.splice(checkedIndex, 1);
                      }
                    }
                    // if (checkedMenu) {
                    //   let removeIndex = [];
                    //   for (let j = 0; j < checkedMenu.length; j ++) {
                    //     if (checkedMenu[j] === 'menu_residency') {
                    //       removeIndex.push(j);
                    //     }
                    //   }
                    //   for (let j = removeIndex.length - 1; j >= 0; j--) {
                    //     checkedMenu.splice(removeIndex[j], 1);
                    //   }
                    // }

                    promises.push(new Promise(function (aResolve) {
                      appDao.upsertApplication(app._id, app, function (upApp) {
                        logger.log('INFO: updateApplicationReplacementOfPolicies - update Application', app._id, 'for', cid);
                        aResolve(upApp);
                      });
                    }));
                  }
                }
              };

              for (var _i2 = 0; _i2 < apps.length; _i2++) {
                _loop5(_i2);
              }

              logger.log('INFO: updateApplicationReplacementOfPolicies - before update applications', cid, '[num=', promises.length, ']');
              Promise.all(promises).then(function (result) {
                logger.log('INFO: updateApplicationReplacementOfPolicies - end [RETURN=1]', cid);
                resolve();
              }).catch(function (error) {
                logger.error('ERROR: updateApplicationReplacementOfPolicies - end [RETURN=-1]', cid, error);
              });
            }).catch(function (error) {
              logger.error('ERROR: updateApplicationReplacementOfPolicies - _getApplicationsByBundle', cid, error);
            });
          } else {
            logger.error('ERROR: updateApplicationReplacementOfPolicies - end [RETURN=-2]', cid, _.get(bResult, 'error'));
            resolve();
          }
        });
      }).catch(function (error) {
        logger.error('ERROR: updateApplicationReplacementOfPolicies - getCurrentBundle', cid, error);
      });
    }).catch(function (error) {
      logger.error('ERROR: updateApplicationReplacementOfPolicies - preformRollbackApplication', cid, error);
    });
  });
};
module.exports.updateApplicationReplacementOfPolicies = _updateApplicationReplacementOfPolicies;

var _updateApplicationAppStatus2Delete = function _updateApplicationAppStatus2Delete(appId) {
  logger.log('INFO: updateApplicationAppStatus2Delete', appId);
  return new Promise(function (resolve, reject) {
    appDao.getApplication(appId, function (app) {
      app.appStatus = 'DELETE';

      appDao.upsertApplication(app._id, app, function (result) {
        if (result && !result.error) {
          resolve();
        } else {
          reject();
        }
      });
    });
  });
};

var updateQuotCheckedList = function updateQuotCheckedList(bundle, quotCheckedList) {
  logger.log('INFO: bundle -- updateQuotCheckedList');
  return new Promise(function (resolve) {
    if (!_.get(bundle, 'isFnaReportSigned')) {
      _.set(bundle, 'clientChoice.quotCheckedList', quotCheckedList);
    }
    logger.log('INFO: BundleUpdate - [cid:' + _.get(bundle, 'pCid') + '; bundleId:' + _.get(bundle, 'id') + '; fn:updateQuotCheckedList]');
    _c.updateBundle(bundle, resolve);
  });
};
module.exports.updateQuotCheckedList = updateQuotCheckedList;

var _updateApplicationFnaAnsToApplications = function _updateApplicationFnaAnsToApplications(application) {
  var fnaQuestionFields = ['ROADSHOW01', 'ROADSHOW02', 'ROADSHOW03', 'ROADSHOW04', 'TRUSTED_IND01', 'TRUSTED_IND02', 'TRUSTED_IND04'];
  var cid = _.get(application, 'pCid');
  var appDeclaration = _.get(application, 'applicationForm.values.proposer.declaration', {});

  logger.log('INFO: _updateApplicationFnaAnsToApplications - start', cid);

  return _c.getCurrentBundle(cid).then(function (bundle) {
    var bunDeclaration = _.get(bundle, 'formValues.' + cid + '.declaration', {});

    _.forEach(fnaQuestionFields, function (fieldName) {
      bunDeclaration[fieldName] = appDeclaration[fieldName];
    });

    return new Promise(function (bResolve, bReject) {
      logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:_updateApplicationFnaAnsToApplications]');
      _c.updateBundle(bundle, function (bResult) {
        if (bResult && !bResult.error) {
          bundle._rev = bResult.rev;
          bResolve(bundle);
        } else {
          bReject(new Error('Fail to update bundle'));
        }
      });
    }).then(function (newBundle) {
      logger.log('INFO: _updateApplicationFnaAnsToApplications - get applying applications', cid);
      return _getApplicationsByBundle(newBundle).then(function (apps) {
        var filterApps = _.filter(apps, function (app) {
          return application._id !== app._id;
        });

        var upAppsPromise = [];

        _.forEach(filterApps, function (app) {
          var originalApp = _.cloneDeep(app);
          _.forEach(fnaQuestionFields, function (fieldName) {
            var declaration = _.get(app, 'applicationForm.values.proposer.declaration');
            if (_.has(appDeclaration, '' + fieldName)) {
              _.set(app, 'applicationForm.values.proposer.declaration.' + fieldName, appDeclaration[fieldName]);
            } else if (_.has(declaration, '' + fieldName)) {
              delete declaration[fieldName];
            }
          });

          if (!_.isEqual(_.get(originalApp, 'applicationForm.values.proposer.declaration'), _.get(app, 'applicationForm.values.proposer.declaration'))) {
            logger.log('INFO: _updateApplicationFnaAnsToApplications - diff found between ' + application._id + ' and ' + app._id + ' for', cid);
            if (app.appStep <= 1) {
              app.appStep = 0;
            }
            upAppsPromise.push(new Promise(function (aResolve, aReject) {
              appDao.upsertApplication(app._id, app, function (upApp) {
                if (upApp && !upApp.error) {
                  aResolve(true);
                } else {
                  aReject(new Error('Fail to update application ' + app._id));
                }
              });
            }));
          }
        });

        return Promise.all(upAppsPromise).then(function (results) {
          logger.log('INFO: _updateApplicationFnaAnsToApplications - end', cid);
          return results;
        });
      });
    });
  });
};
module.exports.updateApplicationFnaAnsToApplications = _updateApplicationFnaAnsToApplications;

var getApplicationStatus = function getApplicationStatus(app) {
  var pCid = _.get(app, 'pCid');
  var bundleId = _.get(app, 'bundleId');
  logger.log('INFO: getApplicationStatus', app.id, pCid, bundleId);
  return new Promise(function (resolve, reject) {
    if (pCid && bundleId) {
      _c.getBundle(pCid, bundleId, function (bundle) {
        var foundApp = _.find(_.get(bundle, 'applications', []), function (item) {
          return item.applicationDocId === app.id;
        });
        logger.log('INFO: getApplicationStatus success');
        resolve(foundApp.appStatus);
      });
    } else {
      logger.error('ERROR: getApplicationStatus fails');
      reject();
    }
  });
};
module.exports.getApplicationStatus = getApplicationStatus;