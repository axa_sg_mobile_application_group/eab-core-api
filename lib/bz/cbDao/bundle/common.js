'use strict';

var dao = require('../../cbDaoFactory').create();
var cDao = require('../client');
var _ = require('lodash');
var cFunctions = require('../../utils/CommonUtils');
var _a = require('./applications');
var logger = global.logger || console;

var STATUS = {
  START_FNA: 0,
  HAVE_FNA: 100,
  HAVE_BI: 200,
  HAS_PRE_EAPP: 300,
  START_APPLY: 400,
  START_GEN_PDF: 500,
  SIGN_FNA: 600,
  FULL_SIGN: 700,
  SUBMIT_APP: 800
};

var CODE = {
  VALID: 200,
  INVALID_FNA: 104,
  INVALID_QU: 105,
  INVALID_APP: 106,
  INVALID_FNA_REPORT: 108,
  INVALID_BUNDLE: 109,
  INVALID_DEPENDANT_BUNDLE: 111,
  INVALID_SIGN_CASE: 112
};

var APPSTATUS = {
  INPROGRESS_STATUS: {
    APPLYING: 'APPLYING'
  },
  COMPLETED_STATUS: {
    INVALIDATED_SIGNED: 'INVALIDATED_SIGNED',
    INVALIDATED: 'INVALIDATED',
    SUBMITTED: 'SUBMITTED'
  },
  VALID_PROPOSAL_SIGNATURE_STATUS: {
    INVALIDATED_SIGNED: 'INVALIDATED_SIGNED',
    INVALIDATED: 'INVALIDATED',
    SUBMITTED: 'SUBMITTED'
  }
};

module.exports.BUNDLE_STATUS = STATUS;
module.exports.CHECK_CODE = CODE;
module.exports.APPSTATUS = APPSTATUS;

var genBundleId = function genBundleId(agentNumber, seq) {
  agentNumber = cFunctions.getAgentIdForDoc(agentNumber);
  seq = cFunctions.getSeqForDoc(seq);
  return "FN" + agentNumber + "-" + seq;
};

var _createBundleNumber = function _createBundleNumber(agentCode, callback) {
  cFunctions.getDocNumber(agentCode, "fna", function (agentNumber, seq) {
    callback(genBundleId(agentNumber, seq));
  });
};

var _rollbackStatus = function _rollbackStatus(cid, status) {
  return new Promise(function (resolve) {
    _getCurrentBundle(cid).then(function (bundle) {
      if (!bundle.status || status < bundle.status) {
        bundle.status = status;
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; status:' + status + '; rollback:true; fn:_rollbackStatus]');
        _updateBundle(bundle, function () {
          resolve(bundle);
        });
      } else {
        resolve(bundle);
      }
    }).catch(function (error) {
      logger.error("Error in _rollbackStatus->_getCurrentBundle: ", error);
    });
  });
};
module.exports.rollbackStatus = _rollbackStatus;

// var _updateStatus = function(cid, status, callback){
//   _getCurrentBundle(cid).then((bundle)=>{
//     if (!bundle.status || status > bundle.status){
//       bundle.status = status;
//       _updateBundle(bundle, ()=>{callback(bundle)});
//     }
//     else {
//       callback(bundle);
//     }
//   });
// };

var _updateStatus = function _updateStatus(cid, status) {
  return _getCurrentBundle(cid).then(function (bundle) {
    if (!bundle.status || status > bundle.status) {
      bundle.status = status;
      return new Promise(function (resolve) {
        // _updateBundle(bundle, resolve);
        logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:_updateStatus]');
        _updateBundle(bundle, function () {
          resolve(bundle);
        });
      });
    } else {
      return bundle;
    }
  }).catch(function (error) {
    logger.error('Error in _updateStatus->_getCurrentBundle: ', error);
    throw error;
  });
};
module.exports.updateStatus = _updateStatus;

module.exports.createNewBundle = function (cid, agent) {
  return new Promise(function (resolve) {
    cDao.getProfile(cid, true).then(function () {
      var profile = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      _getCurrentBundle(cid).then(function (bundle) {
        var _profile$bundle = profile.bundle,
            bundles = _profile$bundle === undefined ? [] : _profile$bundle;
        var aid = agent.agentCode,
            channel = agent.channel,
            compCode = agent.compCode;
        //invalidate all old bundle

        _.forEach(bundles, function (b) {
          b.isValid = false;
        });

        //create new bundle
        _createBundleNumber(aid, function (bundleId) {
          var newBundle = {
            "type": "bundle",
            "id": bundleId,
            "pCid": cid,
            "agentId": aid,
            "agentCode": aid,
            "compCode": compCode,
            "dealerGroup": channel.code,
            "status": bundles.length ? STATUS.HAVE_FNA : STATUS.START_FNA,
            "fna": {
              "pdaid": bundleId + "-PDA",
              "feid": bundleId + "-FE",
              "naid": bundleId + "-NA"
            },
            "applications": [],
            "clientChoice": {},
            "formValues": {},
            "isValid": true,
            "isFnaReportSigned": false,
            "createTime": new Date()
          };

          dao.updDoc(bundleId, newBundle, function () {
            //copy fna
            var promises = [];
            if (bundle) {
              //save profile to bundle and invalidate all applying application and proposal
              bundle.profile = profile;
              _.forEach(bundle.applications, function (application) {
                var appStatus = application.appStatus,
                    applicationDocId = application.applicationDocId,
                    quotationDocId = application.quotationDocId;

                _a.invalidateApplication(bundle, applicationDocId || quotationDocId);
              });

              logger.log('INFO: BundleUpdate - [cid:' + cid + '; bundleId:' + _.get(bundle, 'id') + '; fn:createNewBundle]');
              _updateBundle(bundle);
              _.forEach(["pdaid", "feid", "naid"], function (itemId) {
                promises.push(new Promise(function (resolve2) {
                  dao.getDoc(_.get(bundle, 'fna.' + itemId), function (item) {
                    for (var i in item) {
                      if (i.indexOf("_") == 0) {
                        delete item[i];
                      }
                    }
                    var newId = _.get(newBundle, 'fna.' + itemId);
                    item.id = newId;
                    dao.updDoc(newId, item, resolve2);
                  });
                }));
              });
            }
            Promise.all(promises).then(function (args) {
              dao.updateViewIndex("main", "validbundleApplicationsByAgent");
              bundles.push({ id: bundleId, isValid: true });
              if (profile.cid) {
                profile.bundle = bundles;
                cDao.updateProfile(profile.cid, profile).then(function () {
                  resolve(bundles);
                }).catch(function (error) {
                  logger.error("Error in createNewBundle->updateProfile: ", error);
                });
              } else {
                //for save profile;
                resolve(bundles);
              }
            }).catch(function (error) {
              logger.error("Error in createNewBundle->Promise.all: ", error);
            });
          });
        });
      }).catch(function (error) {
        logger.error("Error in createNewBundle->_getCurrentBundle: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in createNewBundle->getProfile: ", error);
    });
  });
};

var _getCurrentBundle = function _getCurrentBundle(cid) {
  return new Promise(function (resolve) {
    dao.getDoc(cid, function (exDoc) {
      var bundle = _.find(_.get(exDoc, "bundle"), function (b) {
        return b.isValid;
      });
      if (bundle && bundle.id) {
        dao.getDoc(bundle.id, function (_b) {
          resolve(_b);
        });
      } else {
        resolve();
      }
    });
  });
};
module.exports.getCurrentBundle = _getCurrentBundle;

var _getBundle = function _getBundle(cid, bundleId, callback) {
  if (!bundleId && cid) {
    // _getCurrentBundle(cid, callback);
    _getCurrentBundle(cid).then(callback).catch(function (error) {
      logger.error('ERROR: _getCurrentBundle', error);
      callback(false);
    });
  } else {
    // dao.getDoc(cid, (exDoc) => {
    //   let bundle = _.find(_.get(exDoc, 'bundle'), b => b.id === bundleId);
    //   if (bundle && bundle.id) {
    //     dao.getDoc(bundle.id, (_b) => {
    //       callback(_b);
    //     });
    //   } else {
    //     callback(null);
    //   }
    // });
    dao.getDoc(bundleId, function (_b) {
      callback(_b);
    });
  }
};
module.exports.getBundle = _getBundle;

var _updateBundle = function _updateBundle(bundle, callback) {
  dao.getDoc(bundle.id, function (oldBundle) {
    // check masked file and unmasked it if it is not changed.
    for (var key in oldBundle) {
      if (key.indexOf('_') === 0) {
        bundle[key] = oldBundle[key];
      }
    }

    dao.updDoc(bundle.id, bundle, function (result) {
      if (_.isFunction(callback)) {
        callback(result);
      }
    });
  });
};

module.exports.updateBundle = _updateBundle;

module.exports.updateBundlePromise = function (bundle) {
  return new Promise(function (resolve, reject) {
    _updateBundle(bundle, function (res) {
      if (res && !res.error) {
        resolve(res);
      } else {
        reject(new Error('Fail to update bundle ' + bundle.id));
      }
    });
  });
};