"use strict";

var _c = require("./common");
var _a = require("./applications");
var _n = require("./needs");
var _ = require("lodash");

var dao = require('../../cbDaoFactory').create();
var cDao = require('../client');
var nDao = require('../needs');
var appDao = require('../application');

var logger = global.logger || console;

var CLIENT_TYPE = {
  MYSELF: "M",
  SPOUSE: "S",
  DEPENDANTS: "D",
  PROPOSER: "P",
  INSURANCE: "I"
};

var recursive = function recursive(template) {
  var values = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var changedValues = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var status = arguments[3];
  var relationship = arguments[4];
  var hasFNA = arguments[5];
  var isMyselfNSpouse = arguments[6];
  var invalidResult = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : {};
  var tid = template.id;

  var cValue = tid ? _.get(changedValues, tid) : changedValues;
  var oValue = tid ? _.get(values, tid) : values;
  var invalidate = _.get(template, "invalidate");
  var items = _.get(template, "items");
  if (invalidate && cValue != oValue) {
    var fna = invalidate.fna,
        quotation = invalidate.quotation,
        application = invalidate.application;

    if (tid === 'gender' && !relationship) {
      invalidResult.removeApplicant = true;
    }
    if (hasFNA && status > _c.BUNDLE_STATUS.START_FNA && fna && (!relationship && fna.indexOf(CLIENT_TYPE.MYSELF) > -1 || relationship === "SPO" && isMyselfNSpouse && fna.indexOf(CLIENT_TYPE.SPOUSE) > -1 || relationship && fna.indexOf(CLIENT_TYPE.DEPENDANTS) > -1)) {
      if (tid === "gender") {
        invalidResult.removeDependant = true;
      }
      var spouse = _.find(changedValues.dependants, function (d) {
        return d.relationship === 'SPO';
      });
      if (tid === 'marital' && oValue === 'M' && oValue != cValue && spouse) {
        invalidResult.removeSpouseRelation = spouse.cid;
        spouse.relationship = null;
      }

      invalidResult.fna = true;
    }
    if (status > _c.BUNDLE_STATUS.HAVE_FNA && quotation && (!relationship && quotation.indexOf(CLIENT_TYPE.PROPOSER) > -1 || relationship && quotation.indexOf(CLIENT_TYPE.INSURANCE) > -1)) {
      invalidResult.quotation = true;
    }
    if (status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP && application && (!relationship && application.indexOf(CLIENT_TYPE.PROPOSER) > -1 || relationship && application.indexOf(CLIENT_TYPE.INSURANCE) > -1)) {
      if (tid !== 'prStatus' || cValue) {
        invalidResult.application = true;
      }
    }
    return invalidResult;
  }
  if (items) {
    for (var i = items.length - 1; i >= 0; i--) {
      var errObj = recursive(items[i], oValue, cValue, status, relationship, hasFNA, isMyselfNSpouse, invalidResult);
    }
  }
  return invalidResult;
};

var _invalidFamilyMember = function _invalidFamilyMember(cid, agent, fid) {
  var invalidResult = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      cDao.getProfile(cid, true).then(function (profile) {
        var dependants = _.map(profile.dependants, function (d) {
          return d.cid;
        });
        if (cid === fid || _.indexOf(dependants, fid) === -1) {
          dependants = _.concat(dependants, fid);
        }
        //notice that promises are async, so updateBundle in promises may occur error
        //if family member is a dependant of insurance, invalidate all related plans
        var _agent$features = agent.features,
            features = _agent$features === undefined ? "" : _agent$features;

        var isFNA = features.indexOf("FNA") > -1 ? true : false;
        var resp = {};
        if (!isFNA || dependants.indexOf(fid) > -1) {
          var promises = [];
          _.forEach(_.keys(invalidResult), function (key) {
            //prevent they key is note cid, i.e. code
            var tid = _.get(invalidResult, key + ".cid");
            if (tid) {
              promises.push(_invalidClient(tid, fid, agent, invalidResult[key].errObj));
            }
          });

          Promise.all(promises).then(function (args) {
            resp.bundle = _.get(_.find(args, function (arg) {
              return arg.cid === fid;
            }), 'bundle');
            resp.invalid = {
              initApplications: _.get(_.find(args, function (arg) {
                return arg.cid === cid;
              }), 'invalid.initApplications')
            };
            resolve(resp);
          });
        } else {
          resolve();
        }
      }).catch(function (error) {
        logger.error("Error in _invalidFamilyMember->getProfile: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in _invalidFamilyMember->getCurrentBundle: ", error);
    });
  });
};
module.exports.invalidFamilyMember = _invalidFamilyMember;

var hasInvalidApplication = function hasInvalidApplication(pCid, cid, applications) {
  return new Promise(function (resolve) {
    applications = _.filter(applications, function (application) {
      return application.appStatus !== 'INVALIDATED';
    });
    var promises = [];
    _.forEach(applications, function (application) {
      promises.push(new Promise(function (resolve2) {
        dao.getDoc(application.quotationDocId, function (quotation) {
          var cids = [];
          if (quotation.quotType === 'SHIELD') {
            cids = _.keys(quotation.insureds);
            cids.push(quotation.pCid);
          } else {
            cids = [quotation.pCid, quotation.iCid];
          }

          resolve2({
            quotationDocId: application.quotationDocId,
            applicationDocId: application.applicationDocId,
            appStatus: application.appStatus,
            isFullySigned: application.isFullySigned,
            cids: cids
          });
        });
      }));
    });
    Promise.all(promises).then(function (apps) {
      // invalidate bundle if:
      // 1. there is applying or submitted case
      // 2. pCid === iCid or there is any impact application
      var hasInvalidBundle = _.find(apps, function (app) {
        return app.isFullySigned && ["APPLYING", "SUBMITTED"].indexOf(app.appStatus) > -1 && (pCid === cid || app.cids.indexOf(cid) > -1);
      });
      var hasInvalidApp = _.find(apps, function (app) {
        return ["APPLYING"].indexOf(app.appStatus) > -1 && app.cids.indexOf(cid) > -1;
      });
      var hasInvalidBi = _.find(apps, function (app) {
        return app.cids.indexOf(cid) > -1;
      });
      var hasProgressBi = _.find(apps, function (app) {
        return app.appStatus !== 'SUBMITTED';
      });
      resolve({ hasInvalidBi: hasInvalidBi, hasInvalidApp: hasInvalidApp, hasInvalidBundle: hasInvalidBundle, hasProgressBi: hasProgressBi });
    });
  });
};

var _invalidClient = function _invalidClient(cid, fid, agent) {
  var errObj = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  return new Promise(function (resolve, reject) {
    var resp = { cid: cid };
    var aid = agent.agentCode,
        _agent$features2 = agent.features,
        features = _agent$features2 === undefined ? "" : _agent$features2;

    var hasFNA = features.indexOf("FNA") > -1 ? true : false;
    var isChange = errObj.fna || errObj.quotation || errObj.application ? true : false;
    var fnaParam = { fid: fid, removeDependant: errObj.removeDependant, removeApplicant: errObj.removeApplicant };
    _c.getCurrentBundle(cid).then(function (bundle) {
      var checkCreateNewBundle = function checkCreateNewBundle() {
        return new Promise(function (checkResolve) {
          if (bundle.status >= _c.BUNDLE_STATUS.FULL_SIGN && hasFNA && isChange) {
            if (errObj.fna) {
              checkResolve(true);
            } else {
              hasInvalidApplication(cid, fid, bundle.applications).then(function () {
                var invalidResult = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                checkResolve(invalidResult.hasInvalidBundle);
              }).catch(function (error) {
                logger.log("Error in _invalidClient->hasInvalidApplication: ", error);
              });
            }
          } else {
            checkResolve(false);
          }
        });
      };

      var createNewBundle = function createNewBundle(isCreateNewBundle) {
        return new Promise(function (newBundleResolve) {
          if (isCreateNewBundle) {
            _c.createNewBundle(cid, agent).then(function (newBundle) {
              cDao.getProfile(cid, true).then(function (profile) {
                //update profile bundle
                profile.bundle = newBundle;

                dao.updDoc(cid, profile, function () {
                  if (errObj.fna) {
                    _n.invalidFna(cid, fnaParam);
                  }

                  resp = _.assign(resp, { bundle: cid === fid ? newBundle : null, invalid: { initApplications: true } });

                  newBundleResolve(true);
                });
              }).catch(function (error) {
                logger.error("Error in _invalidClient->getProfile: ", error);
              });
            }).catch(function (error) {
              logger.error("Error in _invalidClient->createNewBundle: ", error);
            });
          } else {
            newBundleResolve(false);
          }
        });
      };

      var invalidateClient = function invalidateClient(haveCreateNewBundle) {
        return new Promise(function (invalidClientResolve) {
          var invalidResp = {};
          if (haveCreateNewBundle || !isChange) {
            invalidClientResolve();
          } else {
            var isInitClientChoice = false;
            var rollbackStatus = bundle.status;

            var _removeSpouseRelationship = function _removeSpouseRelationship() {
              return new Promise(function (removeSpouseRelationResolve) {
                var sid = errObj.removeSpouseRelation;
                if (!sid) {
                  removeSpouseRelationResolve();
                } else {
                  cDao.getProfile(sid, true).then(function (sProfile) {
                    var myself = _.find(sProfile.dependants, function (d) {
                      return d.relationship === 'SPO';
                    });
                    myself.relationship = null;
                    dao.updDoc(sid, sProfile, removeSpouseRelationResolve);
                  });
                }
              });
            };

            var _invalidFna = function _invalidFna() {
              return new Promise(function (fnaResolve) {
                if (errObj.fna && hasFNA) {
                  logger.log("Start invalid fna: cid - " + cid + ", remove dependant - " + errObj.removeDependant + ", remove applicant - " + errObj.removeApplicant);
                  invalidResp.fna = true;
                  rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.SIGN_FNA ? rollbackStatus : _c.BUNDLE_STATUS.SIGN_FNA;
                  _n.invalidFna(cid, fnaParam).then(fnaResolve);
                } else {
                  fnaResolve();
                }
              });
            };

            var _invalidQuotation = function _invalidQuotation() {
              return new Promise(function (quotationReoslve) {
                if (errObj.quotation) {
                  if (fid !== cid) {
                    logger.log("INFO: invalidate application by family member: cid - " + cid + ", fid - " + fid);
                    _a.invalidateApplicationByFamilyClientId(cid, fid).then(function (hasInvalidateBi) {
                      if (hasInvalidateBi) {
                        invalidResp.initApplications = true;
                      }

                      //if there is any BI invalidated, client choice should be init
                      isInitClientChoice = hasInvalidateBi;
                      quotationReoslve();
                    }).catch(function (error) {
                      logger.error("Error in _invalidClient->invalidateApplicationByFamilyClientId: ", error);
                    });
                  } else {
                    logger.log("INFO: invalidate application by client: cid - " + cid);
                    invalidResp.initApplications = true;

                    // since all application would be invalid, client choice should always init in this condition
                    isInitClientChoice = true;

                    _a.invalidateApplicationByClientId(cid).then(quotationReoslve).catch(function (error) {
                      logger.error("Error in _invalidClient->invalidateApplicationByClientId: ", error);
                    });
                  }
                } else {
                  quotationReoslve();
                }
              });
            };

            var _initClientChoice = function _initClientChoice() {
              return new Promise(function (clientChoiceResolve) {
                if (isInitClientChoice) {
                  invalidResp.initApplications = true;
                  rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.HAS_PRE_EAPP ? rollbackStatus : _c.BUNDLE_STATUS.HAS_PRE_EAPP;

                  _a.initClientChoice(cid).then(clientChoiceResolve).catch(function (error) {
                    logger.error("Error in _invalidClient->initClientChoice: ", error);
                  });
                } else {
                  clientChoiceResolve();
                }
              });
            };

            var _invalidApplication = function _invalidApplication() {
              return new Promise(function (applicationResolve) {

                if (!errObj.quotation && errObj.application) {
                  _a.rollbackApplication2Proposal(cid, cid === fid ? null : fid, hasFNA).then(function () {
                    var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                    // if no application, rollback to has pre eapp status.
                    var hasApplications = _.find(result.applications, function (application) {
                      return application.appStatus === 'APPLYING' || !application.applicationDocId;
                    });

                    // prefer rollback status is greater than has_pre_eapp if hasApplications = true
                    rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.HAS_PRE_EAPP || hasApplications ? rollbackStatus : _c.BUNDLE_STATUS.HAS_PRE_EAPP;

                    //get invalidated application
                    if (_.size(result.invalidated)) {
                      invalidResp.initApplications = true;
                    }

                    applicationResolve();
                  }).catch(function (error) {
                    logger.error("Error in _invalidClient->invalidateApplicationByClientId: ", error);
                  });
                } else {
                  applicationResolve();
                }
              });
            };

            var _rollbackApplication2Step1 = function _rollbackApplication2Step1() {
              return new Promise(function (rollbackResolve) {
                if (errObj.fna && hasFNA && !errObj.quotation && !errObj.application && bundle.status < _c.BUNDLE_STATUS.FULL_SIGN || fid && !errObj.fna && errObj.quotation) {
                  rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.START_APPLY ? rollbackStatus : _c.BUNDLE_STATUS.START_APPLY;
                  _a.rollbackApplication2Step1(cid).then(rollbackResolve).catch(function (error) {
                    logger.error("Error in _rollbackApplication2Step1->rollbackApplication2Step1: ", error);
                  });
                } else {
                  rollbackResolve();
                }
              });
            };

            _removeSpouseRelationship().then(_invalidFna).then(_invalidQuotation).then(_initClientChoice).then(_invalidApplication).then(_rollbackApplication2Step1).then(function () {
              _c.rollbackStatus(cid, rollbackStatus).then(function () {
                resp = _.assign(resp, { invalid: invalidResp });
                invalidClientResolve();
              }).catch(function (error) {
                logger.error("Error in _invalidClient->rollbackStatus", error);
              });
            }).catch(function (error) {
              logger.error("Error in _invalidClient", error);
            });
          }
        });
      };

      checkCreateNewBundle().then(createNewBundle).then(invalidateClient).then(function () {
        resolve(resp);
      }).catch(function (error) {
        logger.error("Error in _invalidClient: ", error);
      });
    }).catch(function (error) {
      logger.error("Error in _invalidClient->getCurrentBundle: ", error);
    });
  });
};
module.exports.invalidClient = _invalidClient;

var getWarnCode = function getWarnCode(errObj, bundle, hasFNA, cid, fid) {
  return new Promise(function (resolve) {
    //new profile if no cid
    if (!fid) {
      resolve(_c.CHECK_CODE.VALID);
    } else {
      var _bundle$applications = bundle.applications,
          applications = _bundle$applications === undefined ? [] : _bundle$applications,
          status = bundle.status;

      //get all valid quotation data

      hasInvalidApplication(cid, fid, applications).then(function () {
        var invalidResult = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var returnCode = _c.CHECK_CODE.VALID;
        var hasFNAError = hasFNA && errObj.fna;
        if ((hasFNAError || errObj.quotation || errObj.application) && status >= _c.BUNDLE_STATUS.FULL_SIGN) {
          if (status > _c.BUNDLE_STATUS.FULL_SIGN && (errObj.quotation && invalidResult.hasInvalidBi || errObj.application && invalidResult.hasInvalidApp)) {
            returnCode = _c.CHECK_CODE.INVALID_BUNDLE;
          } else if (status === _c.BUNDLE_STATUS.FULL_SIGN && invalidResult.hasInvalidBundle && (invalidResult.hasInvalidBi || invalidResult.hasInvalidApp)) {
            returnCode = _c.CHECK_CODE.INVALID_SIGN_CASE;
          }
        } else if (errObj.quotation && status >= _c.BUNDLE_STATUS.HAVE_BI && invalidResult.hasInvalidBi) {
          returnCode = _c.CHECK_CODE.INVALID_QU;
        } else if (hasFNAError && status === _c.BUNDLE_STATUS.SIGN_FNA) {
          returnCode = _c.CHECK_CODE.INVALID_FNA_REPORT;
        } else if (errObj.application && status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP && invalidResult.hasInvalidApp) {
          returnCode = status === _c.BUNDLE_STATUS.SIGN_FNA ? _c.CHECK_CODE.INVALID_FNA_REPORT : _c.CHECK_CODE.INVALID_APP;
        }
        resolve(returnCode);
      }).catch(function (error) {
        logger.log("Error in _invalidClient->hasInvalidApplication: ", error);
      });
    }
  });
};

var validClient = function validClient(template, cid, hasFNA, profile, tid) {
  return new Promise(function (resolve, reject) {
    _c.getCurrentBundle(tid).then(function (bundle) {
      if (bundle) {
        if (bundle.status < _c.BUNDLE_STATUS.HAVE_FNA) {
          resolve({ cid: tid, code: _c.CHECK_CODE.VALID });
        } else {
          dao.getDoc(profile.cid, function (oProfile) {
            nDao.getItem(tid, nDao.ITEM_ID.PDA).then(function (pda) {
              var isMyselfNSpouse = _.get(pda, 'applicant') === 'joint';
              var relationship = _.get(_.find(profile.dependants, function (d) {
                return d.cid === tid;
              }), "relationship");
              var result = recursive(template, oProfile, profile, bundle.status, relationship, hasFNA, isMyselfNSpouse);
              var handleRelationship = new Promise(function (resolve2) {
                if (profile.relationship && cid != profile.cid && tid === cid) {
                  cDao.getProfile(cid, true).then(function (cProfile) {
                    var dependant = _.find(cProfile.dependants, function (d) {
                      return d.cid === profile.cid;
                    });
                    if (dependant && dependant.relationship != profile.relationship) {
                      result.fna = true;
                      result.quotation = true;
                      if (dependant.relationship === 'SPO') {
                        result.removeApplicant = true;
                      }
                    }
                    resolve2();
                  }).catch(function (error) {
                    logger.error("Error in _onSaveClient->getCurrentBundle: ", error);
                  });
                } else {
                  resolve2();
                }
              });

              handleRelationship.then(function () {
                getWarnCode(result, bundle, hasFNA, cid, profile.cid).then(function (warnCode) {
                  resolve({
                    errObj: result,
                    code: warnCode,
                    hasInvalidateField: result.fna || result.quotation || result.application,
                    cid: tid
                    //code: warnCode  === _c.CHECK_CODE.INVALID_FNA_REPORT? _c.CHECK_CODE.INVALID_FNA_REPORT: _c.CHECK_CODE.VALID
                  });
                }).catch(function (error) {
                  return logger.error("Error in _onSaveClient->getWarnCode: ", error);
                });
              }).catch(function (error) {
                return logger.error("Error in onSaveClient->handleRelationship: ", error);
              });
            });
          });
        }
      } else {
        logger.error("Error in _onSaveClient->getCurrentBundle, cannot get bundle");
        resolve();
      }
    }).catch(function (error) {
      logger.error("Error in _onSaveClient->getProfile: ", error);
    });
  });
};

var _onSaveClient = function _onSaveClient(cid, hasFNA, profile) {
  return new Promise(function (resolve) {
    if (!profile.cid) {
      resolve({ code: _c.CHECK_CODE.VALID });
    } else {
      cDao.getProfileLayout({}, function (template) {
        var promises = [];
        _.forEach(_.concat(profile.cid, _.map(profile.dependants, 'cid')), function (tid) {
          promises.push(validClient(template, cid, hasFNA, profile, tid));
        });

        Promise.all(promises).then(function (args) {
          var result = _.keyBy(args, 'cid');
          // get current client warning message first
          result.code = _.get(result, cid + ".code", _c.CHECK_CODE.VALID);
          if (result.code === _c.CHECK_CODE.VALID) {
            result.code = _.find(args, function (arg) {
              return arg.code !== _c.CHECK_CODE.VALID;
            }) ? _c.CHECK_CODE.INVALID_DEPENDANT_BUNDLE : _c.CHECK_CODE.VALID;
          }
          resolve(result);
        });
      });
    }
  });
};
module.exports.onSaveClient = _onSaveClient;

var _onSaveTrustedIndividual = function _onSaveTrustedIndividual(cid, tiInfo, nPda) {
  return new Promise(function (resolve) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (bundle.status === _c.BUNDLE_STATUS.START_FNA) {
        resolve({ code: _c.CHECK_CODE.VALID });
      } else {
        cDao.getProfile(cid, false).then(function (profile) {
          nDao.getItem(cid, nDao.ITEM_ID.PDA).then(function (oPda) {
            var cnt = 0,
                hasChange = false,
                pda = nPda || oPda;
            var _profile$age = profile.age,
                age = _profile$age === undefined ? 0 : _profile$age,
                _profile$language = profile.language,
                language = _profile$language === undefined ? "" : _profile$language,
                _profile$education = profile.education,
                education = _profile$education === undefined ? "" : _profile$education;

            if (age >= 62) {
              cnt++;
            }
            if (language.indexOf("en") == -1) {
              cnt++;
            }
            if (education == "below") {
              cnt++;
            }
            if (cnt >= 2 && pda.trustedIndividual === "Y") {
              for (var i in tiInfo) {
                if (tiInfo[i] != _.get(profile, "trustedIndividuals[" + i + "]")) {
                  hasChange = true;
                }
              }
            }
            if (hasChange) {
              resolve({ code: 104 });
            } else {
              resolve({ code: _c.CHECK_CODE.VALID });
            }
          }).catch(function (error) {
            logger.error("Error in _onSaveTrustedIndividual->nDao.getItem: ", error);
          });
        }).catch(function (error) {
          logger.error("Error in _onSaveTrustedIndividual->getProfile: ", error);
        });
      }
    }).catch(function (error) {
      logger.error("Error in _onSaveTrustedIndividual->getCurrentBundle: ", error);
    });
  });
};

module.exports.onSaveTrustedIndividual = _onSaveTrustedIndividual;

var _updateApplicationTrustedIndividual = function _updateApplicationTrustedIndividual(cid, hasTi, tiInfo) {
  logger.log('INFO: updateApplicationTrustedIndividual - start', cid, '[hasTi=', hasTi, ';TiDataIsEmpty=', _.isEmpty(tiInfo), ']');

  return new Promise(function (resolve, reject) {
    _c.getCurrentBundle(cid).then(function (bundle) {
      if (bundle.formValues[cid]) {
        var _bundle$formValues$ci = bundle.formValues[cid],
            declaration = _bundle$formValues$ci.declaration,
            extra = _bundle$formValues$ci.extra;

        if (declaration) {
          if (declaration.TRUSTED_IND01) {
            declaration.TRUSTED_IND01 = '';
          }
          if (declaration.TRUSTED_IND02) {
            declaration.TRUSTED_IND02 = '';
          }
          if (declaration.TRUSTED_IND04) {
            declaration.TRUSTED_IND04 = '';
          }
          if (hasTi) {
            declaration.trustedIndividuals = tiInfo;
          } else if (declaration.trustedIndividuals) {
            delete declaration.trustedIndividuals;
          }
        }
        if (extra && extra.hasTrustedIndividual) {
          extra.hasTrustedIndividual = hasTi ? 'Y' : 'N';
        }
      }

      logger.log("INFO: BundleUpdate - [cid:" + cid + "; bundleId:" + _.get(bundle, 'id') + "; fn:updateApplicationTrustedIndividual]");
      _c.updateBundle(bundle, function (bResult) {
        if (bResult && !bResult.error) {
          _a.getApplicationsByBundle(bundle).then(function (apps) {
            var upPromise = [];

            var _loop = function _loop(i) {
              var app = apps[i];
              if (app && !app.error) {
                var _app$applicationForm$ = app.applicationForm.values.proposer,
                    _declaration = _app$applicationForm$.declaration,
                    _extra = _app$applicationForm$.extra;

                var isUpdate = false;

                if (_declaration) {
                  if (_declaration.TRUSTED_IND01) {
                    _declaration.TRUSTED_IND01 = '';
                    if (!isUpdate) {
                      isUpdate = true;
                    }
                  }
                  if (_declaration.TRUSTED_IND02) {
                    _declaration.TRUSTED_IND02 = '';
                    if (!isUpdate) {
                      isUpdate = true;
                    }
                  }
                  if (_declaration.TRUSTED_IND04) {
                    _declaration.TRUSTED_IND04 = '';
                    if (!isUpdate) {
                      isUpdate = true;
                    }
                  }
                  if (hasTi) {
                    _declaration.trustedIndividuals = tiInfo;
                    if (!isUpdate) {
                      isUpdate = true;
                    }
                  } else if (_declaration.trustedIndividuals) {
                    delete _declaration.trustedIndividuals;
                    if (!isUpdate) {
                      isUpdate = true;
                    }
                  }
                }

                if (_extra && _extra.hasTrustedIndividual) {
                  _extra.hasTrustedIndividual = hasTi ? 'Y' : 'N';
                  if (!isUpdate) {
                    isUpdate = true;
                  }
                }

                if (isUpdate) {
                  upPromise.push(new Promise(function (upResolve) {
                    appDao.upsertApplication(app._id, app, function (upResult) {
                      logger.log('INFO: updateApplicationTrustedIndividual - update Application', app._id, 'for', cid);
                      upResolve(upResult);
                    });
                  }));
                }
              }
            };

            for (var i = 0; i < apps.length; i++) {
              _loop(i);
            }

            logger.log('INFO: updateApplicationTrustedIndividual - before update Applications', cid, '[num=', upPromise.length, ']');
            Promise.all(upPromise).then(function (result) {
              logger.log('INFO: updateApplicationTrustedIndividual - end [RETURN=1]', cid);
              resolve();
            }).catch(function (error) {
              logger.error('ERROR: updateApplicationTrustedIndividual [RETURN=-3]', cid, error);
            });
          }).catch(function (error) {
            logger.error('ERROR: updateApplicationTrustedIndividual [RETURN=-2]', cid, error);
          });
        } else {
          logger.error('ERROR: updateApplicationTrustedIndividual - end [RETURN=-1]', cid);
          resolve();
        }
      });
    });
  });
};
module.exports.updateApplicationTrustedIndividual = _updateApplicationTrustedIndividual;

var _invalidTrustedIndividual = function _invalidTrustedIndividual(cid) {
  var promises = [_n.invalidFna(cid)];
  promises.push(new Promise(function (resolve2) {
    //for eApp
    resolve2();
  }));
  return Promise.all(promises);
};

module.exports.invalidTrustedIndividual = _invalidTrustedIndividual;