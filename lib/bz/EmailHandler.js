'use strict';

var _require = require('./utils/RemoteUtils.js'),
    callApi = _require.callApi;

var dao = require('./cbDaoFactory').create();
var applicationDao = require('./cbDao/application');
var approvalDao = require('./cbDao/approval');
var companyDao = require('./cbDao/company');
var CommonFunctions = require('./CommonFunctions');
var nUtil = require('./utils/NotificationsUtils');
var SuppDocsUtils = require('./utils/SuppDocsUtils');
var EmailUtils = require('./utils/EmailUtils');
var productUtils = require('../common/ProductUtils');

var _ = require('lodash');
var _get = require('lodash/fp/get');
var _getOr = require('lodash/fp/getOr');

var logger = global.logger;

module.exports.sendEmail = function (data, session, cb) {
  callApi('/email/sendEmail', data.email, cb);
};

var getSysDocAttachment = function getSysDocAttachment(id, attId, title) {
  return new Promise(function (resolve) {
    applicationDao.getAppAttachment(id, attId, function (pdf) {
      resolve({ fileName: title, data: _get('data', pdf) });
    });
  });
};

var getProtectedPdf = function getProtectedPdf(pwd, pdf, fileName) {
  return new Promise(function (resolve) {
    CommonFunctions.protectBase64Pdf(pdf, pwd, function (data) {
      resolve({
        isProtectBase: true,
        fileName: fileName,
        data: data
      });
    });
  });
};

module.exports.getEmailObject4AllDoc = function (data, session, cb) {
  var appId = data.appId;
  var _session$agent = session.agent,
      email = _session$agent.email,
      name = _session$agent.name;

  companyDao.getCompanyInfo(function (companyInfo) {
    var emailObj = nUtil.getEmailObject(nUtil.TYPE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS, {
      sender: companyInfo.fromEmail,
      recipients: email,
      agentName: name
    });
    getAttachmentList(appId).then(function (suppDocs) {
      cb({
        success: true,
        email: emailObj,
        availableAttachments: _.map(suppDocs, function (doc, docKey) {
          return { id: docKey, title: doc.title };
        })
      });
    }).catch(function (error) {
      logger.error("Error in getEmailObject4AllDoc->getAttachmentList: ", error);
    });
  });
};

var getSuppDocPdf = function getSuppDocPdf(appId, items, title) {
  var promises = _.map(items, function (item) {
    return getSysDocAttachment(appId, item.id, item.title);
  });
  return Promise.all(promises).then(function (attachments) {
    var imgs = _.map(attachments, function (att) {
      return att.data;
    });
    var pdfData = CommonFunctions.convertBase64Imgs2Pdf(imgs);
    return { fileName: title, data: pdfData };
  }).catch(function (error) {
    logger.error("Error in getSuppDocPdf->Promise.all: ", error);
  });
};

var getAttachmentList = function getAttachmentList(appId, approvalId) {
  return approvalDao.getApprovalCaseByAppId(appId).then(function (approval) {
    return new Promise(function (resolve) {
      dao.getDoc('suppDocsNames', function (suppDocsNames) {
        var docsNameList = suppDocsNames.values;
        applicationDao.getApplication(appId, function (app) {
          var result = {};
          var isFACase = _.get(approval, 'isFACase', false);
          var isShield = _.get(approval, 'isShield', false);
          Object.assign(result, SuppDocsUtils.getAppSysDocs(app, docsNameList, isFACase, isShield));
          Object.assign(result, SuppDocsUtils.getAppMandDocs(app, docsNameList, isShield));
          Object.assign(result, SuppDocsUtils.getAppOptionalDocs(app, docsNameList, isShield));
          Object.assign(result, SuppDocsUtils.getAppOtherDocs(app, isShield));
          Object.assign(result, SuppDocsUtils.getApprovalSysDocs(approval, docsNameList));
          resolve(result);
        });
      });
    });
  }).catch(function (error) {
    logger.error("Error in getAttachmentList ->: ", error);
  });
};

module.exports.sendAllDoc = function (data, session, cb) {
  var appId = data.appId,
      suppDocIds = data.suppDocIds;
  var _session$agent2 = session.agent,
      email = _session$agent2.email,
      name = _session$agent2.name,
      agentCode = _session$agent2.agentCode;

  companyDao.getCompanyInfo(function (companyInfo) {
    applicationDao.getApplication(appId, function (application) {
      var quoId = _get('quotation.id', application);
      var fnaId = _get('bundleId', application);
      var isShield = _get('quotation.quotType', application) === 'SHIELD';
      var emailObj = nUtil.getEmailObject(nUtil.TYPE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS, {
        sender: companyInfo.fromEmail,
        recipients: email,
        agentName: name
      });
      getAttachmentList(appId).then(function (suppDocs) {
        var promises = [];
        _.each(suppDocs, function (suppDoc, docKey) {
          if (!_.find(suppDocIds, function (v) {
            return v === docKey;
          })) {
            return;
          }
          if (docKey.indexOf('appPdf') > -1 && isShield && suppDoc) {
            var shieldAppPdfName = _transformInvalidFileName(suppDoc.title);
            promises.push(getSysDocAttachment(appId, docKey, shieldAppPdfName + '.pdf'));
          } else if (docKey === 'appPdf') {
            promises.push(getSysDocAttachment(appId, docKey, 'eapp_form.pdf'));
          } else if (docKey === 'fnaReport') {
            promises.push(getSysDocAttachment(fnaId, docKey, 'fna_report.pdf'));
          } else if (docKey === 'proposal' && isShield) {
            promises.push(getSysDocAttachment(quoId, docKey, 'AXA Shield Product Summary.pdf'));
          } else if (docKey === 'proposal') {
            var pdfName = _getOr('policy_illustration.pdf', 'fileName', productUtils.getPILabelByQuotType(_get('quotation.quotType', application)));
            promises.push(getSysDocAttachment(quoId, docKey, pdfName));
          } else {
            var filename = suppDocs[docKey] && suppDocs[docKey].filename || docKey;
            var imgItems = [];
            _.each(suppDoc.items, function (item, index) {
              if (item.fileSize && item.fileType) {
                if (item.fileType.indexOf('pdf') > -1) {
                  promises.push(getSysDocAttachment(appId, item.id, filename + '_' + (index + 1) + '.pdf'));
                } else if (item.fileType.indexOf('image') > -1) {
                  imgItems.push(item);
                }
              }
            });
            if (imgItems.length) {
              promises.push(getSuppDocPdf(appId, imgItems, filename + '.pdf'));
            }
          }
        });
        return Promise.all(promises);
      }).then(function (attachments) {
        return Promise.all(_.map(attachments, function (attachment) {
          return getProtectedPdf(agentCode.substr(agentCode.length - 6, 6), attachment.data, attachment.fileName);
        }));
      }).then(function (attachments) {
        EmailUtils.sendEmail(Object.assign(emailObj, {
          attachments: attachments.concat(emailObj.attachments)
        }), function (result) {
          cb({ success: true });
        });
      }).catch(function (error) {
        logger.error("Error in sendAllDoc->getAttachmentList: ", error);
      });
    });
  });
};

var _transformInvalidFileName = function _transformInvalidFileName(fileName) {
  return fileName.replace(/[|:/\?*"<>]/g, '');
};