'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _ = require('lodash');

var clientDao = require('../cbDao/client');
var prodDao = require('../cbDao/product');
var needDao = require('../cbDao/needs');
var needsHandler = require('../NeedsHandler');
var QuotDriver = require('../Quote/QuotDriver');

var _require = require('./quote/common'),
    validateClient = _require.validateClient,
    validateFNAInfo = _require.validateFNAInfo,
    checkAllowQuotProduct = _require.checkAllowQuotProduct,
    getFNAInfo = _require.getFNAInfo;

var logger = global.logger;

var getProfile = function getProfile(profileId) {
  if (!profileId) {
    return Promise.resolve(null);
  } else {
    return clientDao.getProfile(profileId, true);
  }
};

var filterProductsByEligibility = function filterProductsByEligibility(agent, prods) {
  if (agent && agent.eligProducts) {
    if (agent.eligProducts.includes('*')) {
      return prods;
    } else {
      return _.filter(prods, function (prod) {
        return agent.eligProducts.indexOf(prod.covCode) > -1;
      });
    }
  } else {
    return prods;
  }
};

module.exports.queryProducts = function (data, session, callback) {
  if (data.optionsMap) {
    global.optionsMap = data.optionsMap;
  }
  var proposerCid = data.proposerCid,
      insuredCid = data.insuredCid,
      quickQuote = data.quickQuote,
      params = data.params;

  var compCode = session.agent.compCode;

  var pCid = proposerCid || insuredCid;
  var requireFNA = session.agent.channel.type === 'AGENCY' && !quickQuote;

  var promises = [];
  promises.push(getProfile(proposerCid));
  promises.push(getProfile(insuredCid));
  promises.push(prodDao.getProductSuitability());
  if (requireFNA) {
    promises.push(getFNAInfo(pCid));
  }

  Promise.all(promises).then(function (args) {
    var _args = _slicedToArray(args, 4),
        proposer = _args[0],
        insured = _args[1],
        suitability = _args[2],
        fnaInfo = _args[3];

    proposer = proposer || insured;

    // validate inputs including client profiles, FNA completeness etc.
    var inputError = validateInputForProducts(proposer, insured, fnaInfo, requireFNA);
    if (inputError) {
      callback({
        success: true,
        prodCategories: [],
        error: inputError
      });
      return;
    }

    logger.log('Products :: queryProducts :: start querying for basic plan by client');
    return prodDao.queryBasicPlansByClient(compCode, insured, proposer, params).then(function (prods) {
      return filterProductsByEligibility(session.agent, prods);
    }).then(function (prods) {
      logger.log('Products :: queryProducts :: ' + prods.length + ' prods queried');
      var result = new QuotDriver().getProducts(suitability, prods, session.agent, [proposer, insured], requireFNA ? fnaInfo : null, { optionsMap: global.optionsMap, quickQuote: quickQuote });
      var error = null;
      var prodCategories = [];
      if (!result || _.isEmpty(result.productList)) {
        error = 'noData';
      } else {
        prodCategories = _getProdByProductLine(result.productList);
        if (_.isEmpty(prodCategories)) {
          error = 'noData';
        }

        // CR71 - no products show for proposer if there is no need selected for myself
        if (requireFNA && (!proposerCid || proposerCid === insuredCid)) {
          var fna = fnaInfo.fna;

          var aspects = _.split(_.get(fna, 'aspects'), ',');
          if (!_.find(aspects, function (aspect) {
            return _.get(fna, aspect + '.owner.isActive');
          })) {
            error = 'noData';
          }
        }
      }
      callback({
        success: true,
        prodCategories: prodCategories,
        error: error
      });
    });
  }).catch(function (err) {
    logger.error('Products :: queryProducts :: Failed to query products\n', err);
    callback({ success: false });
  });
};

var validateInputForProducts = function validateInputForProducts(proposer, insured, fnaInfo, requireFNA) {
  if (proposer !== insured) {
    var dependant = _.find(proposer.dependants, function (d) {
      return d.cid === insured.cid;
    });
    if (!validateClient(insured) || !dependant || !dependant.relationship) {
      return 'missingInsuredInfo';
    }
  }
  if (!validateClient(proposer)) {
    return 'missingProposerInfo';
  }
  if (requireFNA && !validateFNAInfo(fnaInfo)) {
    return 'incompleteFNA';
  }
};

var _getProdByProductLine = function _getProdByProductLine(products) {
  var productLines = global.optionsMap.productLine.options;
  var pcMap = {};
  _.each(products, function (prod) {
    var pl = prod.productLine;
    var productLine = productLines[pl];
    var category;
    if (productLine) {
      category = pcMap[pl];
      if (!category) {
        category = pcMap[pl] = {
          title: productLine.title,
          seq: productLine.sequence,
          products: []
        };
      }
    } else {
      category = pcMap.others;
      if (!category) {
        category = pcMap.others = {
          title: 'Other Products',
          seq: 999,
          products: []
        };
      }
    }
    category.products.push(prod);
  });
  return _.sortBy(pcMap, 'seq');
};

module.exports.getBrochure = function (data, session, callback) {
  prodDao.getAttachmentWithLang(data.productId, 'brochures', data.lang, function (data) {
    logger.log('Products :: getBrochure :: getBrochure after get:', !!data || data.length);
    callback({
      data: data.data
    });
  });
};

module.exports.getDependantList = function (data, session, callback) {
  var cid = data.cid;
  var requireFNA = session.agent.channel.type === 'AGENCY' && !data.quickQuote;
  getProfile(cid).then(function (profile) {
    if (requireFNA) {
      var promises = [];
      promises.push(needDao.getItem(cid, needDao.ITEM_ID.PDA));
      promises.push(needDao.getItem(cid, needDao.ITEM_ID.NA));
      return Promise.all(promises).then(function (result) {
        var pda = result[0];
        var fna = result[1];
        if (pda) {
          var dependants = [];
          if (pda.applicant === 'joint') {
            var spouse = _.find(profile.dependants, function (d) {
              return d.relationship === 'SPO';
            });
            if (spouse) {
              dependants.push(spouse.cid);
            }
          }
          dependants = dependants.concat(pda.dependants && pda.dependants.length && pda.dependants.split(','));

          var needsMap = {};
          _.each(dependants.concat(cid), function (cid) {
            needsMap[cid] = needsHandler.getSelectedNeeds(cid, profile, pda, fna);
          });
          var validDependents = _.filter(_.map(needsMap, function (needs, cid) {
            return needs && needs.length > 0 ? cid : null;
          }), function (id) {
            return id !== cid && id;
          });
          return validDependents;
        } else {
          return null;
        }
      });
    } else {
      return _.map(profile && profile.dependants, function (dependant) {
        return dependant.cid;
      });
    }
  }).then(function (dCids) {
    return Promise.all(_.map(dCids, function (dCid) {
      return getProfile(dCid);
    }));
  }).then(function (dProfiles) {
    callback({
      success: true,
      dependants: _.map(dProfiles, function (dProfile) {
        return {
          cid: dProfile.cid,
          fullName: dProfile.fullName
        };
      })
    });
  }).catch(function (err) {
    logger.error('Products :: getDependantList :: failed to get dependant list\n', err);
    callback({ success: false });
  });
};

module.exports.validateProductForQuot = function (data, session, callback) {
  var productId = data.productId,
      insuredCid = data.insuredCid,
      proposerCid = data.proposerCid;

  var promises = [];
  promises.push(getProfile(proposerCid));
  promises.push(getProfile(insuredCid || proposerCid));
  promises.push(getFNAInfo(proposerCid));
  Promise.all(promises).then(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 3),
        proposer = _ref2[0],
        insured = _ref2[1],
        fnaInfo = _ref2[2];

    if (!validateClient(proposer) || !validateClient(insured) || !validateFNAInfo(fnaInfo)) {
      logger.log('Products :: validateProductForQuot :: mandatory fields are not completed');
      callback({ success: true, viewable: false });
    } else {
      return checkAllowQuotProduct(session.agent, productId, proposer, insured, fnaInfo).then(function (allowQuot) {
        callback({
          success: true,
          viewable: allowQuot
        });
      });
    }
  }).catch(function (error) {
    logger.error('Products :: validateProductForQuot :: failed to validate product\n', error);
    callback({ success: false });
  });
};