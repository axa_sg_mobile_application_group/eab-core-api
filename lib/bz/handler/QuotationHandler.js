'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _ = require('lodash');

var dao = require('../cbDaoFactory').create();
var bundleDao = require('../cbDao/bundle');
var prodDao = require('../cbDao/product');
var needDao = require('../cbDao/needs');
var quotDao = require('../cbDao/quotation');

var EmailUtils = require('../utils/EmailUtils');
var CommonFunctions = require('../CommonFunctions');
var TokenUtils = require('../utils/TokenUtils');
var QuotUtils = require('./quote/QuotUtils');
var ProposalUtils = require('./quote/ProposalUtils');

var _require = require('./quote/common'),
    getQuotDriver = _require.getQuotDriver,
    getProfile = _require.getProfile,
    getCompanyInfo = _require.getCompanyInfo,
    getFNAInfo = _require.getFNAInfo,
    validateClient = _require.validateClient,
    validateFNAInfo = _require.validateFNAInfo,
    validateClientFNAInfo = _require.validateClientFNAInfo,
    checkAllowQuotProduct = _require.checkAllowQuotProduct;

var _require2 = require('../../common/DateUtils'),
    formatDatetime = _require2.formatDatetime,
    parseDatetime = _require2.parseDatetime,
    dayDiff = _require2.dayDiff,
    parseDate = _require2.parseDate;

var _require3 = require('../../common/ProductUtils'),
    getCovCodeFromProductId = _require3.getCovCodeFromProductId,
    getProductId = _require3.getProductId;

var _require4 = require('../cbDao/application'),
    getApplication = _require4.getApplication;

var async = require('async');

var logger = global.logger;
global.quotCache = {
  planDetails: {},
  illustrationRates: {}
};

var queryQuickQuotes = function queryQuickQuotes(data, session, callback) {
  var pCid = data.pCid;

  quotDao.queryQuickQuotes(session.agent.compCode, pCid, function (quickQuotes) {
    callback({
      success: true,
      quickQuotes: quickQuotes
    });
  });
};
module.exports.queryQuickQuotes = queryQuickQuotes;

module.exports.deleteQuickQuotes = function (data, session, callback) {
  var quotIds = data.quotIds,
      pCid = data.pCid;

  Promise.all(_.map(quotIds, function (quotId) {
    return new Promise(function (resolve) {
      quotDao.getQuotation(quotId, function (quot) {
        if (quot.pCid === pCid && session.agent.agentCode === quot.agent.agentCode) {
          quotDao.deleteQuotation(quot, false, resolve);
        } else {
          resolve();
        }
      });
    });
  })).then(function () {
    return quotDao.updateQuotViewIndex();
  }).then(function () {
    queryQuickQuotes({ pCid: pCid }, session, callback);
  }).catch(function (error) {
    logger.error('Quotation :: deleteQuickQuotes :: failed to delete quick quotes\n', error);
    callback({ success: false });
  });
};

var checkAllowCreateQuot = function checkAllowCreateQuot(requireFNA, agent, pCid, confirm) {
  if (!requireFNA) {
    return Promise.resolve(true);
  } else {
    return bundleDao.onCreateQuotation(agent, pCid, confirm).then(function (result) {
      logger.log('Quotation :: checkAllowCreateQuot :: bundle is fully signed, require confirmation');
      return result && (!result.code || result.code === bundleDao.CHECK_CODE.VALID);
    });
  }
};

var getAvailableInsureds = function getAvailableInsureds(quotation, planDetails, requireFNA) {
  var promises = [];
  promises.push(QuotUtils.getRelatedProfiles(quotation.pCid));
  if (requireFNA) {
    promises.push(needDao.getItem(quotation.pCid, needDao.ITEM_ID.PDA));
    promises.push(needDao.getItem(quotation.pCid, needDao.ITEM_ID.NA));
  }
  return Promise.all(promises).then(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 3),
        profiles = _ref2[0],
        pda = _ref2[1],
        fna = _ref2[2];

    var quotDriver = getQuotDriver(quotation);
    var availableApplicants = profiles;
    if (quotDriver.getAvailableApplicants) {
      availableApplicants = quotDriver.getAvailableApplicants(quotation, planDetails, profiles, { pda: pda, fna: fna });
    }
    var eligibleClients = profiles;
    if (quotDriver.getEligibleClients) {
      eligibleClients = quotDriver.getEligibleClients(quotation, planDetails, profiles, { pda: pda, fna: fna });
    }
    var availableClassMap = {};
    if (quotDriver.getAvailableClasses) {
      availableClassMap = quotDriver.getAvailableClasses(quotation, planDetails, eligibleClients);
    }
    return _.map(availableApplicants, function (profile) {
      return {
        profile: profile,
        availableClasses: availableClassMap[profile.cid],
        valid: validateClient(profile),
        eligible: !!_.find(eligibleClients, function (c) {
          return c === profile;
        })
      };
    });
  });
};

module.exports.initQuotation = function (data, session, callback) {
  var quickQuote = data.quickQuote,
      iCid = data.iCid,
      pCid = data.pCid,
      confirm = data.confirm,
      productId = data.productId;

  var ccy = data.params && data.params.ccy;
  var requireFNA = session.agent.channel.type === 'AGENCY' && !quickQuote;
  checkAllowCreateQuot(requireFNA, session.agent, pCid, confirm).then(function (allowCreate) {
    if (!allowCreate) {
      callback({
        success: true,
        requireConfirm: true
      });
      return;
    }
    logger.log('Quotation :: initQuotation :: creating new quotation of product:', productId);
    var promises = [];
    promises.push(getProfile(iCid));
    promises.push(getProfile(pCid && pCid !== iCid ? pCid : null));
    promises.push(prodDao.getProductSuitability());
    promises.push(getCompanyInfo());
    if (requireFNA) {
      promises.push(needDao.getItem(pCid || iCid, needDao.ITEM_ID.FE));
      promises.push(needDao.getItem(pCid || iCid, needDao.ITEM_ID.NA));
    }
    return Promise.all(promises).then(function (args) {
      var _args = _slicedToArray(args, 6),
          insured = _args[0],
          proposer = _args[1],
          suitability = _args[2],
          companyInfo = _args[3],
          fe = _args[4],
          fna = _args[5];

      proposer = proposer || insured;
      var covCode = getCovCodeFromProductId(productId);
      return getPlanDetails(session, covCode, true).then(function (planDetails) {
        var basicPlan = planDetails[covCode];
        if (basicPlan) {
          if (requireFNA) {
            var suitResult = getQuotDriver().checkProdSuitForQuot(suitability, { fna: fna, fe: fe }, basicPlan);
            if (suitResult !== true) {
              logger.log('Quotation :: initQuotation :: product is not allowed to be quoted');
              callback({
                success: true,
                errorMsg: suitResult,
                profile: proposer
              });
              return;
            }
          }
          var quotation = void 0;
          if (basicPlan.covCode === 'ASIM') {
            // TODO: check flag from planDetail
            logger.log('Quotation :: initQuotation :: profiles:', proposer.cid);
            quotation = QuotUtils.genShieldQuotation(basicPlan, session.agent, companyInfo, proposer, quickQuote);
          } else {
            logger.log('Quotation :: initQuotation :: profiles:', insured.cid, proposer.cid);
            if (basicPlan.covCode === 'PVTVUL' || basicPlan.covCode === 'PVLVUL') {
              if (ccy === 'ALL') {
                quotation = QuotUtils.genQuotation(basicPlan, session.agent, companyInfo, proposer, insured, quickQuote, 'USD', suitability, fna);
              } else if (!ccy) {
                quotation = QuotUtils.genQuotation(basicPlan, session.agent, companyInfo, proposer, insured, quickQuote, 'USD', suitability, fna);
              } else {
                quotation = QuotUtils.genQuotation(basicPlan, session.agent, companyInfo, proposer, insured, quickQuote, ccy, suitability, fna);
              }
            } else {
              quotation = QuotUtils.genQuotation(basicPlan, session.agent, companyInfo, proposer, insured, quickQuote, ccy, suitability, fna);
            }
          }
          if (!quotation.extraFlags) {
            quotation.extraFlags = {};
          }
          quotation.extraFlags.init = true;
          var calResult = JSON.parse(getQuotDriver(quotation).calculateQuotation(quotation, planDetails));
          quotation = session.quotation = calResult.quotation || quotation;
          delete quotation.extraFlags.init;
          var warnings = QuotUtils.getQuotationWarnings(basicPlan, quotation, suitability);
          return getAvailableInsureds(quotation, planDetails, requireFNA).then(function (availableInsureds) {
            callback({
              success: true,
              errors: calResult.errors,
              quotation: quotation,
              planDetails: getPlanDetails4Client(planDetails),
              inputConfigs: calResult.inputConfigs,
              quotWarnings: warnings,
              availableInsureds: availableInsureds,
              profile: proposer
            });
          });
        } else {
          callback({
            success: false,
            error: 'Basic plan details not found!'
          });
        }
      });
    });
  }).catch(function (err) {
    logger.error('Quotation :: initQuotation :: failed to init quotation\n', err);
    callback({ success: false });
  });
};

var prefetchIllustrationRates = function prefetchIllustrationRates(planDetails) {
  var p = Promise.resolve();
  var hasPrefetch = false;
  _.each(planDetails, function (planDetail, covCode) {
    var prefetchRates = void 0;
    if (planDetail.formulas.getPrefetchIllustrationRates) {
      prefetchRates = getQuotDriver().runFunc(planDetail.formulas.getPrefetchIllustrationRates);
    }
    if (prefetchRates) {
      _.each(prefetchRates, function (config, rateKey) {
        logger.log('Quotation :: prefetchIllustrationRates :: caching rates:', covCode, rateKey);
        var docId = config.docId;

        if (docId) {
          hasPrefetch = true;
          p = p.then(function () {
            return new Promise(function (resolve) {
              dao.getDoc(docId, function (doc) {
                if (doc && doc.rates && global.quotCache.planDetails[covCode]) {
                  logger.log('Quotation :: prefetchIllustrationRates :: rates fetched:', docId);
                  if (!global.quotCache.illustrationRates[planDetail._id]) {
                    global.quotCache.illustrationRates[planDetail._id] = {};
                  }
                  global.quotCache.illustrationRates[planDetail._id][rateKey] = doc.rates;
                }
                resolve();
              });
            });
          });
        }
      });
    }
  });
  if (hasPrefetch) {
    global.quotCache.fetching = p;
    p.then(function () {
      logger.log('Quotation :: prefetchIllustrationRates :: rates prefetch completed');
      delete global.quotCache.fetching;
    });
  }
};

var _preparePlanDetails = function _preparePlanDetails(compCode, covCode) {
  return prodDao.getPlanByCovCode(compCode, covCode, 'B').then(function (basicPlan) {
    var planDetails = {};
    planDetails[basicPlan.covCode] = basicPlan;
    var promises = _.map(basicPlan.riderList, function (rider) {
      return prodDao.getPlanByCovCode(compCode, rider.covCode, 'R').then(function (riderDetail) {
        if (riderDetail && riderDetail.covCode) {
          logger.log('Quotation :: preparePlanDetails :: found rider details:', rider.covCode);
          planDetails[riderDetail.covCode] = riderDetail;
        } else {
          logger.error('Quotation :: preparePlanDetails :: cannot find plan detail:', rider.covCode);
        }
      });
    });
    return Promise.all(promises).then(function () {
      return planDetails;
    });
  });
};

var getPlanDetails = function getPlanDetails(session, baseProductCode, noCache) {
  if (noCache || !global.quotCache.planDetails[baseProductCode]) {
    return new Promise(function (resolve) {
      _preparePlanDetails(session.agent.compCode, baseProductCode).then(function (planDetails) {
        prefetchIllustrationRates(planDetails);
        preparePlanDetailsWithCampaign(session, planDetails, resolve);
      });
    });
  } else {
    var planDetails = {};
    var bpDetail = global.quotCache.planDetails[baseProductCode];
    planDetails[baseProductCode] = bpDetail;
    _.each(bpDetail.riderList, function (rider) {
      planDetails[rider.covCode] = global.quotCache.planDetails[rider.covCode];
    });
    return Promise.resolve(planDetails);
  }
};
module.exports.getPlanDetails = getPlanDetails;

/**
 * Prepare Valid Campaign for Plan Details
 * @param {*} planDetails 
 */
var preparePlanDetailsWithCampaign = function preparePlanDetailsWithCampaign(session, planDetails, resolve) {
  async.waterfall([function (callback) {
    getValidCampaign(session.agent.compCode, function (result) {
      if (result.success && result.validCampaign) {
        callback(null, result.validCampaign);
      } else {
        //Not going to modify the planDetails
        callback(result);
      }
    });
  }, function (validCampaign, callback) {
    planDetails = addCampaignToPlanDetails(planDetails, validCampaign);
    callback(null, planDetails);
  }], function (err, modifiedPlanDetails) {
    var result = void 0;
    if (err) {
      logger.error('Error in preparePlanDetailsWithCampaign ', err);
      result = planDetails;
    } else {
      result = modifiedPlanDetails;
    }
    _.each(modifiedPlanDetails, function (planDetail, covCode) {
      logger.log('Quotation :: getPlanDetails :: preparePlanDetailsWithCampaign :: update plan details in cache:', covCode);
      global.quotCache.planDetails[covCode] = planDetail;
    });
    resolve(result);
  });
};

var addCampaignToPlanDetails = function addCampaignToPlanDetails(planDetails, validCampaign) {
  // Group by covCode
  var groupedCampaign = {};
  _.each(validCampaign, function (campaign) {
    var campaignCode = campaign.campaignCode;
    var campaignId = campaign.campaignId;
    _.each(campaign && campaign.impactedPlan, function (plan, key) {
      // Add the campaignCode and campaignId to individual campaign
      _.each(plan, function (campaignPlanWithDiffCondition) {
        campaignPlanWithDiffCondition.campaignCode = campaignCode;
        campaignPlanWithDiffCondition.campaignId = campaignId;
      });

      var individualCampaign = groupedCampaign[key];
      if (individualCampaign) {
        groupedCampaign[key] = _.concat(groupedCampaign[key], plan);
      } else {
        groupedCampaign[key] = plan;
      }
    });
  });

  planDetails = _.each(planDetails, function (planDetail, key) {
    planDetail.campaign = [];
    var campaign = _.get(groupedCampaign, key);
    campaign ? planDetail.campaign = campaign : [];
  });

  return planDetails;
};

var getValidCampaign = function getValidCampaign(compCode, cb) {
  var currentDateTime = Date.parse(new Date().toUTCString());
  var expiryDate = Date.parse(new Date('9999-12-31').toISOString());
  async.waterfall([function (callback) {
    var promises = [];
    promises.push(new Promise(function (resolve) {
      dao.getViewRange('main', 'quotationCampaign', '["' + compCode + '","endTime",' + currentDateTime + ']', '["' + compCode + '","endTime",' + expiryDate + ']', null, function (viewResult) {
        resolve(viewResult);
      });
    }));

    Promise.all(promises).then(function (data) {
      var validCampaign = {};
      var campaign = void 0;
      var campaignId = void 0;
      _.each(data, function (campaignView, key) {
        _.each(campaignView && campaignView.rows, function (rowObj) {
          campaign = _.get(rowObj, 'value');
          campaignId = _.get(rowObj, 'value.campaignId');
          if (campaign && campaign.impactedPlan && campaign.startTime && currentDateTime > Date.parse(campaign.startTime) && campaignId) {
            validCampaign[campaignId] = campaign;
          }
        });
      });
      callback(null, validCampaign);
    });
  }], function (err, validCampaign) {
    if (err) {
      logger.error('ERROR in getValidCampaign: ', err);
      cb({ success: false });
    } else {
      cb({ success: true, validCampaign: validCampaign });
    }
  });
};

var getCampaignByIds = function getCampaignByIds(compCode, campaignIds) {
  var keys = _.map(campaignIds, function (campaignId) {
    return '["' + compCode + '","campaignId","' + campaignId + '"]';
  });
  return new Promise(function (resolve) {
    dao.getViewByKeys('main', 'proposalCampaign', keys, null).then(function (result) {
      var validCampaign = {};
      _.each(result && result.rows, function (row) {
        if (row.value && row.value.campaignId && row.value.impactedPlan) {
          validCampaign[row.value.campaignId] = row.value;
        }
      });
      resolve({ validCampaign: validCampaign });
    });
  });
};

/**
 * Returns the plan details of the version used by the quotation.
 *
 * @param {*} session
 * @param {*} quotation
 */
var getPlanDetailsByQuot = function getPlanDetailsByQuot(session, quotation) {
  var planDetails = {};
  var planDetailsWithCampaign = {};
  var plans = quotation.plans;
  if (quotation.quotType === 'SHIELD') {
    var planMap = {};
    _.each(quotation.insureds, function (subQuot) {
      _.each(subQuot.plans, function (plan) {
        planMap[plan.covCode] = {
          productId: plan.productId,
          covCode: plan.covCode
        };
      });
    });
    plans = _.values(planMap);
  }
  var promises = _.map(plans, function (plan) {
    var p = void 0;
    if (plan.productId) {
      var cached = global.quotCache.planDetails[plan.covCode];
      if (cached && plan.productId === getProductId(cached)) {
        p = Promise.resolve(cached);
      } else {
        p = prodDao.getProduct(plan.productId);
      }
    } else if (plan.covCode === quotation.baseProductCode) {
      p = prodDao.getProduct(quotation.baseProductId);
    } else {
      p = prodDao.getPlanByCovCode(session.agent.compCode, plan.covCode, 'R', true);
    }
    return p.then(function (product) {
      planDetails[product.covCode] = _.cloneDeep(product);
    });
  });
  var includedCampaignIds = [];
  _.each(quotation && quotation.plans, function (plan) {
    _.each(plan && plan.campaignIds, function (campaignId) {
      if (includedCampaignIds.indexOf(campaignId) === -1) {
        includedCampaignIds.push(campaignId);
      }
    });
  });

  if (!_.isEmpty(includedCampaignIds)) {
    promises.push(getCampaignByIds(session.agent.compCode, includedCampaignIds).then(function (result) {
      if (result && result.validCampaign && !_.isEmpty(result.validCampaign)) {
        planDetailsWithCampaign = addCampaignToPlanDetails(planDetails, result.validCampaign);
      } else {
        return Promise.reject('ERROR:: Cannot get campaignByIds:: Including campaign Ids:: ' + includedCampaignIds + ' :: Result:: ' + (result && result.validCampaign));
      }
    }));
  }

  return Promise.all(promises).then(function () {
    return global.quotCache.fetching;
  }).then(function () {
    _.each(planDetails, function (planDetail) {
      _.each(global.quotCache.illustrationRates[planDetail._id], function (rates, rateKey) {
        planDetail.rates[rateKey] = rates;
      });
      var campaign = _.get(planDetailsWithCampaign, planDetail.covCode + '.campaign', []);
      planDetail.campaign = campaign;
    });
    return planDetails;
  });
};
module.exports.getPlanDetailsByQuot = getPlanDetailsByQuot;

/**
 * Remove unnecessary details for client side.
 *
 * @param {*} planDetails
 */
var getPlanDetails4Client = function getPlanDetails4Client(planDetails) {
  var details = {};
  _.each(planDetails, function (planDetail, covCode) {
    var detail = _.clone(planDetail);
    delete detail.rates;
    delete detail.formulas;
    details[covCode] = detail;
  });
  return details;
};

var quote = function quote(data, session, callback) {
  getPlanDetails(session, data.quotation.baseProductCode).then(function (planDetails) {
    // TODO:: add globalValidation !!
    var result = JSON.parse(getQuotDriver(data.quotation).calculateQuotation(data.quotation, planDetails));
    logger.log('Quotation :: quote :: finish calc');

    var finalQuot = result.quotation || data.quotation;
    finalQuot.lastUpdateDate = formatDatetime(new Date());

    var basicPlan = planDetails[getCovCodeFromProductId(data.quotation.baseProductId)];
    return prodDao.getProductSuitability().then(function (suitability) {
      // save product id for product versioning
      QuotUtils.updateProductIds(finalQuot, planDetails);
      var warnings = QuotUtils.getQuotationWarnings(basicPlan, data.quotation, suitability);
      callback({
        success: !result.error && !data.error,
        errors: result.errors,
        quotWarnings: warnings,
        quotation: finalQuot,
        inputConfigs: result.inputConfigs
      });
    });
  }).catch(function (error) {
    logger.error('Quotation :: quote :: failed to quote\n', error);
    callback({ success: false });
  });
};
module.exports.quote = quote;

var getQuotation = function getQuotation(data, session) {
  return new Promise(function (resolve, reject) {
    if (data.quotation) {
      resolve(data.quotation);
    } else if (data.quotId) {
      if (session.quotation && session.quotation.id === data.quotId) {
        resolve(session.quotation);
      } else {
        quotDao.getQuotation(data.quotId, function (quotation) {
          resolve(quotation);
        });
      }
    } else {
      throw new Error('Cannot find quotation in session and request');
    }
  });
};

var prepareQuotPage = function prepareQuotPage(session, quotation, planDetails, requireFNA) {
  var bpDetail = planDetails[quotation.baseProductCode];
  return prodDao.getProductSuitability().then(function (suitability) {
    var warnings = QuotUtils.getQuotationWarnings(bpDetail, quotation, suitability);
    return getAvailableInsureds(quotation, planDetails, requireFNA).then(function (availableInsureds) {
      return new Promise(function (resolve) {
        quote({ quotation: quotation }, session, function (result) {
          resolve({
            success: result.success,
            errors: result.errors,
            quotation: result.quotation,
            planDetails: getPlanDetails4Client(planDetails),
            inputConfigs: result.inputConfigs,
            quotWarnings: warnings,
            availableInsureds: availableInsureds
          });
        });
      });
    });
  });
};

module.exports.requote = function (data, session, cb) {
  getQuotation(data, session).then(function (quotation) {
    return getPlanDetails(session, quotation.baseProductCode, true).then(function (planDetails) {
      var requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      QuotUtils.updateQuot(quotation, planDetails, false);
      return prepareQuotPage(session, quotation, planDetails, requireFNA).then(function (result) {
        cb(result);
      });
    });
  }).catch(function (error) {
    logger.error('Quotation :: requote :: failed to requote\n', error);
    cb({ success: false });
  });
};

var checkAllowCloneQuot = function checkAllowCloneQuot(quotation, bpDetail) {
  var currentDate = new Date();
  if (dayDiff(currentDate, parseDatetime(quotation.createDate)) > 90) {
    // check if BI is older than 90 days
    logger.log('Quotation :: checkAllowCloneQuot :: quotation over 90 days');
    return Promise.resolve(false);
  }

  if (QuotUtils.hasCrossAge(quotation, bpDetail)) {
    logger.log('Quotation :: checkAllowCloneQuot :: proposer or insured age changed');
    return Promise.resolve(false);
  }

  return QuotUtils.getQuotClients(quotation).then(function (profiles) {
    if (QuotUtils.hasProfileUpdate(quotation, profiles)) {
      logger.log('Quotation :: checkAllowCloneQuot :: client profile is updated');
      return false;
    }
    return new Promise(function (resolve) {
      prodDao.canViewProduct(quotation.baseProductId, profiles[0], profiles[1], function (product) {
        if (!product) {
          logger.log('Quotation :: checkAllowCloneQuot :: product cannot be viewed');
          resolve(false);
        } else {
          resolve(true);
        }
      });
    });
  });
};

module.exports.cloneQuotation = function (data, session, cb) {
  var confirm = data.confirm;

  getQuotation(data, session).then(function (quotation) {
    return getPlanDetails(session, quotation.baseProductCode, true).then(function (planDetails) {
      var bpDetail = planDetails[quotation.baseProductCode];
      var requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      return checkAllowCloneQuot(quotation, bpDetail).then(function (allowClone) {
        if (!allowClone) {
          cb({ success: true, allowClone: false });
          return;
        }
        return checkAllowCreateQuot(requireFNA, session.agent, quotation.pCid, confirm).then(function (allowCreate) {
          if (!allowCreate) {
            cb({ success: true, requireConfirm: true });
            return;
          }
          QuotUtils.updateQuot(quotation, planDetails, true);
          return prepareQuotPage(session, quotation, planDetails, requireFNA).then(function (result) {
            cb(Object.assign(result, { allowClone: true }));
          });
        });
      });
    });
  }).catch(function (err) {
    logger.error('Quotation :: cloneQuotation :: Failed to clone quotation\n', err);
    cb({ success: false });
  });
};

var checkAllowRequoteInvalidated = function checkAllowRequoteInvalidated(agent, quotation, requireFNA) {
  return QuotUtils.getQuotClients(quotation).then(function (profiles) {
    var invalidProfile = _.find(profiles, function (profile) {
      return !validateClient(profile);
    });
    if (invalidProfile) {
      logger.log('Quotation :: checkAllowRequoteInvalidated :: mandatory fields are missing');
      return false;
    }
    return Promise.resolve(requireFNA && getFNAInfo(quotation.pCid)).then(function (fnaInfo) {
      if (requireFNA && !validateFNAInfo(fnaInfo)) {
        logger.log('Quotation :: checkAllowRequoteInvalidated :: FNA is not completed');
        return false;
      }
      return checkAllowQuotProduct(agent, quotation.baseProductId, profiles[0], profiles[1], fnaInfo).then(function (allowQuot) {
        if (!allowQuot) {
          logger.log('Quotation :: checkAllowRequoteInvalidated :: product not viewable to client');
          return false;
        }
        return true;
      });
    });
  });
};

module.exports.requoteInvalid = function (data, session, callback) {
  var confirm = data.confirm;

  getQuotation(data, session).then(function (quotation) {
    return getPlanDetails(session, quotation.baseProductCode, true).then(function (planDetails) {
      var requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      return checkAllowRequoteInvalidated(session.agent, quotation, requireFNA).then(function (allowRequote) {
        if (!allowRequote) {
          callback({ success: true, allowRequote: false });
          return;
        }
        return checkAllowCreateQuot(requireFNA, session.agent, quotation.pCid, confirm).then(function (allowCreate) {
          if (!allowCreate) {
            callback({ success: true, requireConfirm: true });
            return;
          }
          QuotUtils.updateQuot(quotation, planDetails, true);
          quotation.fund = null;
          return QuotUtils.updateClientFields(quotation, planDetails, requireFNA).then(function () {
            return prepareQuotPage(session, quotation, planDetails, requireFNA).then(function (result) {
              callback(Object.assign(result, { allowRequote: true }));
            });
          });
        });
      });
    });
  }).catch(function (err) {
    logger.error('Quotation :: requoteInvalidated :: Failed to requote\n', err);
    callback({ success: false });
  });
};

module.exports.resetQuot = function (data, session, callback) {
  var keepConfigs = data.keepConfigs,
      keepPolicyOptions = data.keepPolicyOptions,
      keepPlans = data.keepPlans,
      keepFunds = data.keepFunds;

  getQuotation(data, session).then(function (quotation) {
    if (!keepConfigs) {
      quotation.ccy = null;
      quotation.paymentMode = null;
      quotation.isBackDate = 'N';
    }
    if (!keepPolicyOptions) {
      _.each(quotation.policyOptions, function (value, key) {
        quotation.policyOptions[key] = null;
      });
    }
    if (!keepPlans) {
      quotation.plans = null;
    }
    if (!keepFunds) {
      quotation.fund = null;
    }
    quotation.extraFlags.reset = true;
    quote(data, session, function (result) {
      if (result.quotation && result.quotation.extraFlags) {
        delete result.quotation.extraFlags.reset;
      }
      callback(result);
    });
  }).catch(function (err) {
    logger.error('Quotation :: resetQuot :: Failed to reset quotation\n', err);
    callback({ success: false });
  });
};

module.exports.updateRiderList = function (data, session, callback) {
  var quotation = data.quotation,
      newRiderList = data.newRiderList;

  var plans = [quotation.plans[0]];
  getPlanDetails(session, quotation.baseProductCode).then(function (planDetails) {
    _.each(newRiderList, function (covCode) {
      if (covCode !== quotation.baseProductCode) {
        var existingPlan = _.find(quotation.plans, function (plan) {
          return plan.covCode === covCode;
        });
        if (existingPlan) {
          plans.push(existingPlan);
        } else {
          plans.push({ covCode: covCode });
        }
      }
    });
    quotation.plans = plans;
    quote(data, session, callback);
  }).catch(function (err) {
    logger.error('Quotation :: updateRiderList :: Failed to update rider list\n', err);
    callback({ success: false });
  });
};

module.exports.getFundDetails = function (data, session, cb) {
  var productId = data.productId,
      invOpt = data.invOpt,
      paymentMethod = data.paymentMethod;

  if (!paymentMethod) {
    cb({
      success: true,
      funds: []
    });
    return;
  }
  var covCode = getCovCodeFromProductId(productId);
  getPlanDetails(session, covCode).then(function (planDetails) {
    var bpDetail = planDetails[covCode];
    return QuotUtils.getFunds(session.agent.compCode, bpDetail.fundList, paymentMethod, invOpt).then(function (funds) {
      cb({
        success: true,
        funds: funds
      });
    });
  }).catch(function (error) {
    logger.error('Quotation :: getFundDetails : failed to get funds\n', error);
    cb({ success: false });
  });
};

module.exports.allocFunds = function (data, session, callback) {
  var invOpt = data.invOpt,
      portfolio = data.portfolio,
      fundAllocs = data.fundAllocs,
      hasTopUpAlloc = data.hasTopUpAlloc,
      topUpAllocs = data.topUpAllocs,
      adjustedModel = data.adjustedModel;

  getQuotation(data, session).then(function (quotation) {
    return getPlanDetails(session, quotation.baseProductCode).then(function (planDetails) {
      var paymentMethod = quotation.policyOptions.paymentMethod;
      var fundCodes = _.map(fundAllocs, function (alloc, fundCode) {
        return fundCode;
      });
      fundCodes = _.filter(fundCodes, function (funcCode) {
        return fundAllocs[funcCode] || hasTopUpAlloc && topUpAllocs[funcCode];
      });
      return QuotUtils.getFunds(session.agent.compCode, fundCodes, paymentMethod, invOpt).then(function (funds) {
        var selectedFunds = _.map(funds, function (fund) {
          return {
            fundCode: fund.fundCode,
            fundName: fund.fundName,
            alloc: fundAllocs[fund.fundCode],
            topUpAlloc: hasTopUpAlloc ? topUpAllocs[fund.fundCode] : null
          };
        });
        quotation.fund = {
          invOpt: invOpt,
          portfolio: invOpt === 'buildPortfolio' ? portfolio : null,
          funds: selectedFunds,
          adjustedModel: adjustedModel
        };
        quote(data, session, callback);
      });
    });
  }).catch(function (error) {
    logger.error('Quotation :: allocFunds :: failed to allocate funds\n', error);
    callback({ success: false });
  });
};

module.exports.updateClients = function (data, session, callback) {
  getQuotation(data, session).then(function (quotation) {
    var requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
    return Promise.resolve(!requireFNA || validateClientFNAInfo(quotation.pCid)).then(function (completed) {
      if (!completed) {
        callback({
          success: true,
          errorMsg: 'FNA is invalidated. Please visit and complete FNA again.'
        });
        return;
      }
      return getPlanDetails(session, quotation.baseProductCode).then(function (planDetails) {
        return getAvailableInsureds(quotation, planDetails, requireFNA).then(function (availableInsureds) {
          callback({
            success: true,
            availableInsureds: availableInsureds
          });
        });
      });
    });
  }).catch(function (error) {
    logger.error('Quotation :: updateClients :: failed to update client info\n', error);
    callback({ success: false });
  });
};

module.exports.selectInsured = function (data, session, callback) {
  var cid = data.cid,
      covClass = data.covClass;

  getQuotation(data, session).then(function (quotation) {
    if (covClass) {
      var promises = [];
      promises.push(getProfile(quotation.pCid));
      promises.push(getProfile(cid));
      promises.push(getCompanyInfo());
      return Promise.all(promises).then(function (_ref3) {
        var _ref4 = _slicedToArray(_ref3, 3),
            proposer = _ref4[0],
            insured = _ref4[1],
            companyInfo = _ref4[2];

        return getPlanDetails(session, quotation.baseProductCode).then(function (planDetails) {
          var basicPlan = planDetails[quotation.baseProductCode];
          quotation.insureds[cid] = QuotUtils.genBasicQuotation(basicPlan, session.agent, companyInfo, proposer, insured);
          quote({ quotation: quotation }, session, function (result) {
            var resultQuot = result.quotation || quotation;
            resultQuot.insureds[cid].plans[0].covClass = covClass;
            quote({ quotation: resultQuot }, session, callback);
          });
        });
      });
    } else {
      delete quotation.insureds[cid];
      quote(data, session, callback);
    }
  }).catch(function (error) {
    logger.error('Quotation :: selectInsured :: failed to select insured\n', error);
    callback({ success: false });
  });
};

module.exports.updateShieldRiderList = function (data, session, callback) {
  var quotation = data.quotation,
      newRiderList = data.newRiderList,
      cid = data.cid;

  var requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
  var subQuot = quotation.insureds[cid];
  var plans = [subQuot.plans[0]];
  return getPlanDetails(session, quotation.baseProductCode).then(function (planDetails) {
    _.each(newRiderList, function (covCode) {
      if (covCode !== subQuot.baseProductCode) {
        var existingPlan = _.find(subQuot.plans, function (plan) {
          return plan.covCode === covCode;
        });
        if (existingPlan) {
          plans.push(existingPlan);
        } else {
          plans.push({ covCode: covCode });
        }
      }
    });
    subQuot.plans = plans;
    if (requireFNA && plans.length > 1) {
      needDao.getItem(quotation.pCid, needDao.ITEM_ID.FE).then(function (fe) {
        if (_.get(fe, 'owner.aRegPremBudget')) {
          quote(data, session, callback);
        } else {
          callback({
            success: true,
            errorMsg: 'Cash Premium (RP Budget) is required for selected riders for AXA Shield.'
          });
        }
      });
    } else {
      quote(data, session, callback);
    }
  });
};

var saveQuotation = function saveQuotation(session, quotation) {
  return new Promise(function (resolve) {
    logger.log('Quotation :: saveQuotation :: saving quotation');
    if (!quotation) {
      throw new Error('Quotation is empty');
    }
    quotation.type = 'quotation';
    quotation.lastUpdateDate = formatDatetime(new Date());
    if (!quotation.id) {
      quotDao.genQuotationId(session.agent.agentCode, function (id) {
        quotation.id = id;
        resolve(true);
      });
    } else {
      resolve(false);
    }
  }).then(function (isNewQuotation) {
    return new Promise(function (resolve) {
      if (quotation.clientChoice) {
        delete quotation.clientChoice;
      }
      if (session.platform && isNewQuotation) {
        if (quotation._rev) {
          delete quotation._rev;
        }
        if (quotation._id) {
          delete quotation._id;
        }
        if (quotation._attachments) {
          delete quotation._attachments;
        }
      }
      quotDao.upsertQuotation(quotation.id, quotation, function (result) {
        resolve(result);
      });
    }).then(function (result) {
      if (!result || !result.rev) {
        throw new Error('Failed to get rev from saving quotation');
      }
      logger.log('Quotation :: saveQuotation :: saved quotation');
      session.quotation = quotation;
      return Promise.resolve(quotation.quickQuote || bundleDao.onSaveQuotation(session, quotation.pCid, quotation.id, isNewQuotation)).then(function () {
        return result.rev;
      });
    });
  });
};

var saveAttachment = function saveAttachment(quotation, attId, rev, data) {
  return quotDao.upsertAttachment(quotation.id, attId, rev, data).then(function (result) {
    if (result && result.rev) {
      logger.log('Quotation :: saveAttachment :: saved proposal');
      return result.rev;
    } else {
      throw new Error('Failed to get rev from saving proposal');
    }
  });
};

var handleSave = function handleSave(session, quotation, attachments) {
  var promise = void 0;
  if (quotation.quickQuote) {
    promise = saveQuotation(session, quotation);
  } else {
    promise = bundleDao.updateStatus(quotation.pCid, bundleDao.BUNDLE_STATUS.HAVE_BI).then(function (bundle) {
      quotation.bundleId = bundle && (bundle.id || bundle._id);
      return saveQuotation(session, quotation);
    });
  }
  _.each(attachments, function (att) {
    logger.log('Quotation :: handleSave :: saving attachment:', att.saveAttId);
    promise = promise.then(function (rev) {
      return saveAttachment(quotation, att.saveAttId, rev, att.data);
    });
  });
  return promise;
};

var getProposalFuncPerms = function getProposalFuncPerms(session, quotation, readonly) {
  logger.log('Quotation :: getProposalFuncPerms :: checking for function permissions');
  if (readonly) {
    return Promise.resolve({
      requote: false,
      clone: false
    });
  }
  if (quotation.quickQuote) {
    return Promise.resolve({
      requote: false,
      clone: true
    });
  } else {
    return bundleDao.getProposalFuncPerms(quotation.pCid, quotation.id, session.agent.channel.type === 'FA');
  }
};

var validateQuotation = function validateQuotation(agent, quotation) {
  var requireFNA = agent.channel.type === 'AGENCY' && !quotation.quickQuote;
  if (!requireFNA) {
    return Promise.resolve(true);
  } else {
    return prodDao.getProductSuitability().then(function (suitability) {
      return needDao.getItem(quotation.pCid, needDao.ITEM_ID.FE).then(function (fe) {
        return getQuotDriver().validateQuotSuitability(suitability, { fe: fe }, quotation);
      });
    });
  }
};

var prepareProposal = function prepareProposal(data, session, cb, createProposal, readonly) {
  var agent = session.agent;

  getQuotation(data, session).then(function (quotation) {
    return getPlanDetailsByQuot(session, quotation).then(function (planDetails) {
      return bundleDao.getAppIdByQuotId(quotation.pCid, quotation.id).then(function (appId) {
        logger.log('Quotation :: prepareProposal :: begin preparing pdfs');
        return session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !appId;
      }).then(function (standalone) {
        return ProposalUtils.prepareProposalData(quotation, planDetails, false).then(function (proposalData) {
          var illustrations = proposalData.illustrations,
              errorMsg = proposalData.errorMsg,
              warningMsg = proposalData.warningMsg;

          if (errorMsg) {
            cb({ success: true, errorMsg: errorMsg });
            return;
          }
          var promise = void 0;
          if (createProposal) {
            promise = ProposalUtils.genAttachments(agent, quotation, planDetails, standalone, proposalData);
          } else {
            promise = ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, null, true);
          }
          return promise.then(function (attachments) {
            var saveAttachments = _.filter(attachments, function (att) {
              return !!att.saveAttId;
            });
            return Promise.resolve(createProposal && handleSave(session, quotation, saveAttachments)).then(function () {
              return getProposalFuncPerms(session, quotation, readonly).then(function (funcPerms) {
                var pdfs = [];
                _.each(attachments, function (att) {
                  if (!att.hidden) {
                    var pdf = {
                      id: att.id,
                      tabLabel: att.tabLabel,
                      label: att.label,
                      allowSave: !!att.allowSave && !readonly,
                      fileName: att.fileName
                    };
                    if (att.attId) {
                      pdf.docId = quotation.id;
                      pdf.attachmentId = att.attId;
                    } else if (att.data) {
                      pdf.data = att.data;
                    }
                    pdfs.push(pdf);
                  }
                });
                cb({
                  success: true,
                  proposal: pdfs,
                  quotation: quotation,
                  illustrateData: illustrations,
                  planDetails: getPlanDetails4Client(planDetails),
                  warningMsg: warningMsg,
                  funcPerms: funcPerms
                });
              });
            });
          });
        });
      });
    });
  }).catch(function (error) {
    logger.error('Quotation :: prepareProposal :: failed to get quotation\n', error);
    cb({ success: false });
  });
};

module.exports.genProposal = function (data, session, cb) {
  if (data.optionsMap) {
    global.optionsMap = data.optionsMap;
  }
  getQuotation(data, session).then(function (quotation) {
    return validateQuotation(session.agent, quotation);
  }).then(function (validResult) {
    if (validResult === true) {
      prepareProposal(data, session, cb, true);
    } else {
      cb({
        success: true,
        errorMsg: validResult || 'Please change the selected product/payment mode or input the relevant budget in FNA.'
      });
    }
  }).catch(function (err) {
    logger.error('Quotation :: genProposal :: failed to gen proposal\n', err);
    cb({ success: false });
  });
};

module.exports.getProposal = function (data, session, cb) {
  if (data.optionsMap) {
    global.optionsMap = data.optionsMap;
  }
  prepareProposal(data, session, cb, false, data.readonly);
};

module.exports.getEmailTemplate = function (data, session, callback) {
  var standalone = data.standalone;

  quotDao.getEmailTemplate(function (result) {
    if (result && !result.error) {
      callback({
        success: true,
        clientSubject: result.clientSubject,
        clientContent: standalone ? result.clientContentStandalone : result.clientContent,
        agentSubject: result.agentSubject,
        agentContent: standalone ? result.agentContentStandalone : result.agentContent,
        embedImgs: result.embedImgs
      });
    } else {
      callback({ success: false });
    }
  });
};

module.exports.emailProposal = function (data, session, callback) {
  var agent = session.agent;
  var emails = data.emails,
      authToken = data.authToken,
      webServiceUrl = data.webServiceUrl;

  if (emails) {
    getQuotation(data, session).then(function (quotation) {
      return bundleDao.getAppIdByQuotId(quotation.pCid, quotation.id).then(function (appId) {
        getApplication(appId, function (application) {
          var standalone = session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !appId;
          var isShield = _.get(quotation, 'quotType') === 'SHIELD';
          var isProposalSigned = _.get(application, 'isProposalSigned');
          var requiredPdfs = {};
          _.each(emails, function (email) {
            _.each(email.attachmentIds, function (id) {
              requiredPdfs[id] = true;
            });
          });
          return getPlanDetailsByQuot(session, quotation).then(function (planDetails) {
            return ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, _.keys(requiredPdfs)).then(function (attachments) {
              return getCompanyInfo().then(function (companyInfo) {
                _.each(emails, function (email) {
                  var allAttachments = _.map(email.attachmentIds, function (attId) {
                    var attachment = _.find(attachments, function (att) {
                      return att.id === attId;
                    });
                    var attData = attachment.data;
                    var isProtect = false;
                    if (isShield) {
                      var agentCode = quotation.agent.agentCode;
                      var dob = parseDate(quotation.pDob);
                      attachment.agentPassword = agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
                      attachment.clientPassword = dob.format('ddmmmyyyy').toUpperCase();
                    }

                    if (!standalone && !isShield || !standalone && isShield && isProposalSigned) {
                      if (email.id === 'agent' && attachment.agentPassword) {
                        isProtect = true;
                        attData = CommonFunctions.protectBase64Pdf(attachment.data, attachment.agentPassword);
                      } else if (email.id === 'client' && attachment.clientPassword) {
                        isProtect = true;
                        attData = CommonFunctions.protectBase64Pdf(attachment.data, attachment.clientPassword);
                      }
                    }
                    return {
                      isProtectBase: isProtect,
                      fileName: attachment.fileName || attachment.id + '.pdf',
                      data: attData
                    };
                  });
                  _.each(email.embedImgs, function (value, key) {
                    allAttachments.push({
                      fileName: key,
                      cid: key,
                      data: value
                    });
                  });
                  EmailUtils.sendEmail({
                    from: companyInfo.fromEmail,
                    to: _.join(email.to, ','),
                    title: email.title,
                    content: email.content,
                    attachments: allAttachments,
                    userType: email.id,
                    agentCode: email.agentCode,
                    dob: email.dob,
                    isQQ: email.isQQ
                  }, function (result) {
                    logger.log('Quotation :: emailProposal :: email sent');
                  }, authToken, webServiceUrl);
                });
                callback({ success: true });
              });
            });
          });
        });
      });
    }).catch(function (error) {
      logger.error('Quotation :: emailProposal :: failed to prepare proposal pdfs\n', error);
      callback({ success: false });
    });
  } else {
    callback({ success: true });
  }
};

module.exports.getPdfToken = function (data, session, callback) {
  var docId = data.docId,
      attachmentId = data.attachmentId;

  TokenUtils.getPdfToken(docId, attachmentId, session).then(function (token) {
    callback({
      success: true,
      token: token
    });
  }).catch(function (error) {
    logger.error('Quotation :: getPdfToken :: failed to get pdf token\n', error);
    callback({ success: false });
  });
};

// module.exports.alterate = function(data, session, callback) {

//   // var allocations = data.quotation.allocations;
//   var newAlters = data.newAlters;

//   getPlanDetails(session, data.quotation.baseProductId, (basicPlanCode, planDetails) => {
//     if (!session.planDetails[basicPlanCode]) {
//       session.planDetails = planDetails;
//     }

//     var validCodes = []
//     var plan = planDetails[data.planCode];
//     for (var f in plan.alterationList) {
//       var alter = plan.alterationList[f];
//       validCodes.push(alter.alterationCode);
//     }

//     var errors = {};
//     var alteration = [];

//     logger.log('update alteration:', validCodes);

//     for (var n = 0; n < newAlters.length; n++) {
//       alter = newAlters[n];
//       var error = false;
//       if (validCodes.indexOf(alter.alterationCode) >= 0) {
//         for (var m=n+1; m < newAlters.length; m++) {
//           var calter = newAlters[m];
//           if (alter.alterationCode == calter.alterationCode &&
//             ((alter.fromYear <= calter.fromYear && calter.fromYear <= alter.toYear)
//             || (alter.fromYear <= calter.toYear && calter.toYear <= alter.toYear )
//             || (calter.fromYear <= alter.fromYear && alter.fromYear <= calter.toYear)
//             || (calter.fromYear <= alter.toYear && alter.toYear <= calter.toYear)
//             || alter.fromYear > alter.toYear)) {
//             error = "invalid_alteration_period"
//             break;
//           }
//         }
//       } else {
//         error = ["invalid_alteration_code", alter.alterationCode];
//       }
//       if (isNaN(alter.amount) || alter.amount == 0) {
//         error = ["invalid_alteration_amount", alter.amount];
//       }
//       if (!error) {
//         alteration.push(alter);
//       } else {
//         errors[n] = error;
//       }
//     }
//     data.error = JSON.stringify(errors);
//     logger.log('update alteration:', errors);
//     if (Object.keys(errors).length == 0) {
//       data.quotation.alteration = alteration;
//     }
//     quote(data, session, callback);
//   });
// };