'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var clientDao = require('../../cbDao/client');
var companyDao = require('../../cbDao/company');
var needDao = require('../../cbDao/needs');
var prodDao = require('../../cbDao/product');

var QuotDriver = require('../../Quote/QuotDriver');
var ShieldQuotDriver = require('../../Quote/ShieldQuotDriver');

var logger = global.logger;

var getQuotDriver = function getQuotDriver(quotation) {
  return quotation && quotation.quotType === 'SHIELD' ? new ShieldQuotDriver() : new QuotDriver();
};

var getProfile = function getProfile(profileId) {
  if (!profileId) {
    return Promise.resolve(null);
  } else {
    return clientDao.getProfile(profileId, true);
  }
};

var getCompanyInfo = function getCompanyInfo() {
  return new Promise(companyDao.getCompanyInfo);
};

var getFNAInfo = function getFNAInfo(cid) {
  var promises = [];
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.PDA));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.FE));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.NA));
  return Promise.all(promises).then(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 3),
        pda = _ref2[0],
        fe = _ref2[1],
        fna = _ref2[2];

    return { pda: pda, fe: fe, fna: fna };
  });
};

var validateClient = function validateClient(client) {
  // R-eBI-P1-1 completeness of client profile
  return !!(client.dob && client.gender && client.residenceCountry && client.nationality && client.industry && client.occupation && client.isSmoker);
};

var validateFNAInfo = function validateFNAInfo(fnaInfo) {
  // R-eBI-P1-2 completeness of FNA
  return fnaInfo && fnaInfo.pda && fnaInfo.pda.isCompleted && fnaInfo.fe && fnaInfo.fe.isCompleted && fnaInfo.fna && fnaInfo.fna.isCompleted;
};

var validateClientFNAInfo = function validateClientFNAInfo(cid) {
  var promises = [];
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.PDA));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.FE));
  promises.push(needDao.getItem(cid, needDao.ITEM_ID.NA));
  return Promise.all(promises).then(function (_ref3) {
    var _ref4 = _slicedToArray(_ref3, 3),
        pda = _ref4[0],
        fe = _ref4[1],
        fna = _ref4[2];

    return validateFNAInfo({ pda: pda, fe: fe, fna: fna });
  });
};

/**
 * Checks the eligibility and suitability of the product.
 *
 * @param {*} agent
 * @param {*} productId
 * @param {*} proposer
 * @param {*} insured
 * @param {*} fnaInfo
 */
var checkAllowQuotProduct = function checkAllowQuotProduct(agent, productId, proposer, insured, fnaInfo) {
  return prodDao.getProductSuitability().then(function (suitability) {
    return new Promise(function (resolve) {
      prodDao.canViewProduct(productId, proposer, insured, resolve);
    }).then(function (product) {
      if (!product) {
        logger.log('Products :: checkAllowQuotProduct :: product not found');
        return false;
      }
      var result = new QuotDriver().filterProducts(suitability, [product], agent, [proposer, insured], {
        optionsMap: global.optionsMap
      });
      if (!result || !result.productList || !result.productList.length) {
        logger.log('Products :: checkAllowQuotProduct :: product filtered out');
        return false;
      }
      if (fnaInfo && !new QuotDriver().validateProdSuitability(suitability, fnaInfo, product, [proposer, insured])) {
        logger.log('Products :: checkAllowQuotProduct :: product is not suitable');
        return false;
      }
      return true;
    });
  });
};

module.exports = {
  getQuotDriver: getQuotDriver,
  getProfile: getProfile,
  getCompanyInfo: getCompanyInfo,
  getFNAInfo: getFNAInfo,
  validateClient: validateClient,
  validateFNAInfo: validateFNAInfo,
  validateClientFNAInfo: validateClientFNAInfo,
  checkAllowQuotProduct: checkAllowQuotProduct
};