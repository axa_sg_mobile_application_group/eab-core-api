'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var _ = require('lodash');

var dao = require('../../cbDaoFactory').create();
var quotDao = require('../../cbDao/quotation');
var prodDao = require('../../cbDao/product');

var QuotUtils = require('./QuotUtils');

var _require = require('./common'),
    getQuotDriver = _require.getQuotDriver,
    getCompanyInfo = _require.getCompanyInfo;

var PDFHandler = require('../../PDFHandler');
var CommonFunctions = require('../../CommonFunctions');

var _require2 = require('../../../common/DateUtils'),
    formatDatetime = _require2.formatDatetime,
    parseDate = _require2.parseDate;

var _require3 = require('../../../common/ProductUtils'),
    getPILabelByQuotType = _require3.getPILabelByQuotType;

var getPdfTemplates = function getPdfTemplates(bpDetail, quotation) {
  var pdfTemplates = {};
  var promises = _.map(bpDetail.reportTemplate, function (reportTemplate) {
    return new Promise(function (resolve) {
      var code = reportTemplate.pdfCode;
      if (reportTemplate.covCode && reportTemplate.covCode !== '0' && reportTemplate.covCode !== 'na') {

        var plan = _.find(quotation.plans, function (p) {
          return reportTemplate.covCode === p.covCode;
        });
        if (plan) {
          code += '/' + reportTemplate.covCode;
        }
      }

      quotDao.getPdfTemplate('01', reportTemplate.pdfCode, function (pdfTemplate) {
        if (pdfTemplate) {
          pdfTemplates[code] = pdfTemplate;
        }
        resolve();
      });
    });
  });
  return Promise.all(promises).then(function () {
    return pdfTemplates;
  });
};

var getIllustrationProps = function getIllustrationProps(quotation, bpDetail, planDetails) {
  var illustProps = {};
  var promises = [];
  if (bpDetail.nfInd && bpDetail.nfInd.toUpperCase() === 'Y') {
    _.each(quotation.plans, function (plan) {
      promises.push(new Promise(function (resolve) {
        var covCode = plan.covCode;
        var planDetail = planDetails[covCode];
        dao.getDoc(planDetail._id + '_' + quotation.iGender + quotation.iSmoke + quotation.iAge, function (doc) {
          if (!doc || doc.error === 'not_found') {
            dao.getDoc(planDetail._id + '_A' + quotation.iSmoke + quotation.iAge, function (doc) {
              if (!doc || doc.error === 'not_found') {
                dao.getDoc(planDetail._id + '_' + quotation.iGender + 'A' + quotation.iAge, function (doc) {
                  if (doc && doc.error !== 'not_found') {
                    illustProps[covCode] = doc;
                  }
                  resolve();
                });
              } else {
                illustProps[covCode] = doc;
                resolve();
              }
            });
          } else {
            illustProps[covCode] = doc;
            resolve();
          }
        });
      }));
    });
  }
  return Promise.all(promises).then(function () {
    return illustProps;
  });
};

var prepareProposalData = function prepareProposalData(quotation, planDetails, standalone) {
  var bpDetail = planDetails[quotation.baseProductCode];
  var promises = [];
  promises.push(getPdfTemplates(bpDetail, quotation));
  promises.push(getIllustrationProps(quotation, bpDetail, planDetails));
  promises.push(getCompanyInfo());

  return Promise.all(promises).then(function (args) {
    var _args = _slicedToArray(args, 3),
        pdfTemplates = _args[0],
        illustProps = _args[1],
        companyInfo = _args[2];

    var extraPara = {
      prepareProposalDataFunc: bpDetail.prepareProposalDataFunc,
      systemDate: formatDatetime(new Date()),
      dateFormat: companyInfo.dateFormat,
      datetimeFormat: companyInfo.datetimeFormat,
      langMap: !!global.langMaps && (global.langMaps.en || global.langMaps[Object.keys(global.langMaps)[0]]),
      optionsMap: !!global.optionsMap && global.optionsMap,
      requireReportData: true,
      templateFuncs: {},
      illustProps: illustProps,
      company: companyInfo,
      quickQuote: standalone,
      releaseVersion: global.config.biReleaseVersion
    };

    _.each(pdfTemplates, function (pdfTemplate) {
      if (pdfTemplate.formulas && pdfTemplate.formulas.prepareReportData) {
        extraPara.templateFuncs[pdfTemplate.pdfCode] = pdfTemplate.formulas.prepareReportData;
      }
    });

    var result = JSON.parse(getQuotDriver(quotation).generateIllustrationData(quotation, planDetails, extraPara));
    var illustrations = result.illustrations;

    return {
      pdfTemplates: pdfTemplates,
      reportData: result.reportData,
      illustrations: illustrations,
      errorMsg: (_.find(illustrations, function (ill) {
        return ill.errorMsg;
      }) || {}).errorMsg,
      warningMsg: (_.find(illustrations && illustrations[quotation.baseProductCode], function (ill) {
        return ill.warningMsg;
      }) || {}).warningMsg
    };
  });
};

var hidePdfTempalte = function hidePdfTempalte(reportData, pdfTemplates) {
  _.each(reportData, function (obj, pdfCodekey) {
    // Do when the report Data has hidePagesArray
    if (reportData[pdfCodekey] && reportData[pdfCodekey].hidePagesIndexArray) {
      // Start to remove the page in different langauges
      _.each(pdfTemplates[pdfCodekey].template, function (langArr, lang) {
        pdfTemplates[pdfCodekey].template[lang] = _.remove(langArr, function (value, index) {
          return reportData[pdfCodekey].hidePagesIndexArray.indexOf(index) === -1;
        });
      });
    }
  });
  return pdfTemplates;
};

var genBenefitIllustration = function genBenefitIllustration(quotation, planDetails, proposalData) {
  var bpDetail = planDetails[quotation.baseProductCode];
  var pdfTemplates = proposalData.pdfTemplates,
      reportData = proposalData.reportData;

  var pdfCodes = _.map(bpDetail.reportTemplate, function (reportTemplate) {
    var retVal = reportTemplate.pdfCode;
    if (reportTemplate.covCode && reportTemplate.covCode !== '0' && reportTemplate.covCode !== 'na') {
      retVal = retVal + '/' + reportTemplate.covCode;
    }
    return retVal;
  });
  if (quotation.plans[0].planTemplateDisappear) {
    if (pdfCodes.indexOf(quotation.plans[0].planTemplateDisappear) != -1) {
      var tobeDeleted = pdfCodes.indexOf(quotation.plans[0].planTemplateDisappear);
      pdfCodes.splice(tobeDeleted, 1);
      bpDetail.reportTemplate.splice(tobeDeleted, 1);
    }
    delete pdfTemplates[quotation.plans[0].planTemplateDisappear];
    delete reportData[quotation.plans[0].planTemplateDisappear];
  }
  return new Promise(function (resolve) {
    pdfTemplates = hidePdfTempalte(reportData, pdfTemplates);
    var labelObject = getPILabelByQuotType(quotation.quotType);
    PDFHandler.getReportPdf(pdfCodes, reportData, pdfTemplates, bpDetail.dynPdfInd === 'Y', bpDetail, function (pdf) {
      CommonFunctions.addPdfTitle(pdf, labelObject.fileName).then(function (pdfWithTitle) {
        resolve(pdf && pdfWithTitle);
      });
    });
  });
};

var getBenefitIllustration = function getBenefitIllustration(quotation, planDetails, standalone) {
  return new Promise(function (resolve) {
    quotDao.getQuotationPDF(quotation.id, standalone, function (pdfData) {
      resolve(pdfData.data);
    });
  });
};

var hasBenefitIllustration = function hasBenefitIllustration(quotation, planDetails) {
  var bpDetail = planDetails[quotation.baseProductCode];
  return !_.isEmpty(bpDetail.reportTemplate);
};

var getProductSummary = function getProductSummary(productId, lang) {
  return new Promise(function (resolve) {
    prodDao.getAttachmentWithLang(productId, 'prod_summary', lang, function (attachData) {
      resolve(attachData && attachData.data);
    });
  });
};

var getExtraProductSummary = function getExtraProductSummary(productId, attId, lang) {
  return new Promise(function (resolve) {
    prodDao.getAttachmentWithLang(productId, attId, lang, function (attachData) {
      resolve(attachData && attachData.data);
    });
  });
};

var getProductSummaries = function getProductSummaries(compCode, quotation, lang) {
  var plans = quotation.plans;
  if (quotation.extraProdSummary) {
    plans = [].concat(_toConsumableArray(quotation.plans), _toConsumableArray(quotation.extraProdSummary));
  }
  return Promise.all(_.map(plans, function (plan) {
    return new Promise(function (resolve) {
      if (plan.productId) {
        if (plan.hasExtraProdSummary && plan.attId) {
          getExtraProductSummary(plan.productId, plan.attId, lang).then(resolve);
        } else {
          getProductSummary(plan.productId, lang).then(resolve);
        }
      } else if (plan.covCode === quotation.baseProductCode) {
        getProductSummary(quotation.baseProductId, lang).then(resolve);
      } else {
        prodDao.getPlanByCovCode(compCode, plan.covCode, 'R', true).then(function (prod) {
          getProductSummary(prod._id, lang).then(function (pdf) {
            resolve(pdf);
          });
        });
      }
    });
  })).then(function (pdfs) {
    return new Promise(function (resolve) {
      CommonFunctions.mergePdfs(_.filter(pdfs, function (pdf) {
        return !!pdf;
      }), function (data) {
        resolve(data);
      });
    });
  });
};

var hasProductSummary = function hasProductSummary(quotation, planDetails) {
  var bpDetail = planDetails[quotation.baseProductCode];
  return bpDetail && bpDetail.prod_summary;
};

var getProductHighlightSheets = function getProductHighlightSheets(compCode, quotation) {
  var _ref = quotation.fund || {},
      invOpt = _ref.invOpt;

  var _ref2 = quotation.policyOptions || {},
      paymentMethod = _ref2.paymentMethod;

  var fundCodes = _.map(quotation.fund.funds, function (f) {
    return f.fundCode;
  });
  return QuotUtils.getFunds(compCode, fundCodes, paymentMethod, invOpt).then(function (funds) {
    return Promise.all(_.map(funds, function (fund) {
      return new Promise(function (resolve) {
        quotDao.getProductHighlightSheet(fund._id, function (pdf) {
          resolve(pdf && pdf.data);
        });
      });
    }));
  }).then(function (pdfs) {
    return new Promise(function (resolve) {
      CommonFunctions.mergePdfs(_.filter(pdfs, function (pdf) {
        return !!pdf;
      }), function (data) {
        resolve(data);
      });
    });
  });
};

var getFundInfoBooklet = function getFundInfoBooklet(productId, lang) {
  return new Promise(function (resolve) {
    prodDao.getAttachmentWithLang(productId, 'fund_info_booklet', lang, function (attachData) {
      resolve(attachData && attachData.data);
    });
  });
};

var getProductAttachment = function getProductAttachment(productId, attachmentId, lang) {
  return new Promise(function (resolve) {
    prodDao.getAttachmentWithLang(productId, attachmentId, lang, function (attachData) {
      resolve(attachData && attachData.data);
    });
  });
};

var getSystemAttachmentConfigs = function getSystemAttachmentConfigs(quotation, planDetails, standalone, proposalData) {
  var bpDetail = planDetails[quotation.baseProductCode];
  var attachments = [];
  if (hasBenefitIllustration(quotation, planDetails)) {
    var labelObject = getPILabelByQuotType(quotation.quotType);
    if (!quotation.quickQuote) {
      attachments.push({
        id: 'bi',
        label: labelObject.label,
        fileName: labelObject.fileName,
        saveAttId: 'proposal',
        hidden: standalone,
        getAgentPassword: function getAgentPassword() {
          var agentCode = quotation.agent.agentCode;
          return agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
        },
        getClientPassword: function getClientPassword() {
          var dob = parseDate(quotation.pDob);
          return dob.format('ddmmmyyyy').toUpperCase();
        },
        getData: function getData() {
          return getBenefitIllustration(quotation, planDetails, false);
        },
        genData: function genData() {
          return genBenefitIllustration(quotation, planDetails, proposalData);
        }
      });
    }

    if (standalone) {
      attachments.push({
        id: 'standaloneBi',
        label: labelObject.label,
        fileName: labelObject.fileName,
        saveAttId: 'standaloneProposal',
        allowAsync: true,
        allowSave: true,
        getAgentPassword: function getAgentPassword() {
          var agentCode = quotation.agent.agentCode;
          return agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
        },
        getClientPassword: function getClientPassword() {
          var dob = parseDate(quotation.pDob);
          return dob.format('ddmmmyyyy').toUpperCase();
        },
        getData: function getData() {
          return getBenefitIllustration(quotation, planDetails, true);
        },
        genData: function genData() {
          return prepareProposalData(quotation, planDetails, true).then(function (standaloneProposalData) {
            return genBenefitIllustration(quotation, planDetails, standaloneProposalData);
          });
        }
      });
    }
  }

  if (hasProductSummary(quotation, planDetails)) {
    attachments.push({
      id: 'prodSummary',
      label: 'Product Summary',
      fileName: 'product_summary.pdf',
      getData: function getData() {
        return getProductSummaries(quotation.compCode, quotation);
      }
    });
  }

  if (bpDetail.fundInd === 'Y') {
    attachments.push({
      id: 'phs',
      label: 'Product Highlight Sheet',
      fileName: 'product_highlight_sheet.pdf',
      getData: function getData() {
        return getProductHighlightSheets(quotation.compCode, quotation);
      }
    });
    attachments.push({
      id: 'fib',
      label: 'Fund Info Booklet',
      fileName: 'fund_info_booklet.pdf',
      getData: function getData() {
        return getFundInfoBooklet(quotation.baseProductId);
      }
    });
  }
  return attachments;
};

var getExtraAttachmentPdf = function getExtraAttachmentPdf(attId, agent, quotation, planDetails) {
  var extraAttachments = getExtraAttachmentConfigs(agent, quotation, planDetails);
  var attachment = _.find(extraAttachments, function (att) {
    return att.id === attId;
  });
  if (attachment.saveAttId) {
    return getExtraAttachmentData(quotation, attachment);
  } else {
    return genExtraAttachmentData(quotation, planDetails, attachment);
  }
};

var getExtraAttachmentConfigs = function getExtraAttachmentConfigs(agent, quotation, planDetails) {
  var bpDetail = planDetails[quotation.baseProductCode];
  if (bpDetail.formulas && bpDetail.formulas.getExtraAttachments) {
    return getQuotDriver().runFunc(bpDetail.formulas.getExtraAttachments, agent, quotation, planDetails);
  }
  return null;
};

var genExtraAttachmentPdf = function genExtraAttachmentPdf(quotation, planDetails, attachment) {
  var promises = [];
  _.each(attachment.files, function (file) {
    var planDetail = planDetails[file.covCode];
    if (planDetail) {
      promises.push(getProductAttachment(planDetail._id, file.fileId).then(function (attData) {
        if (file.covCode === 'ASIM') {
          if (file.fileId === 'prod_summary_2' || file.fileId === 'prod_summary_1') {
            var agentName = '';
            var proposerName = '';
            if (quotation.agent.name) {
              agentName = quotation.agent.name;
            }
            if (quotation.pFullName) {
              proposerName = quotation.pFullName;
            }
            var x1 = 68.0;
            var y1 = 620.0;
            var x2 = 310.0;
            var y2 = 620.0;
            if (file.fileId === 'prod_summary_2') {
              y1 += 20;
              y2 += 20;
            }
            return CommonFunctions.addWordsPdfs(attData, agentName, proposerName, x1, y1, x2, y2);
          } else {
            return attData;
          }
        } else {
          return attData;
        }
      }));
    }
  });
  return Promise.all(promises).then(function (pdfs) {
    return new Promise(function (resolve) {
      CommonFunctions.mergePdfs(_.filter(pdfs, function (pdf) {
        return !!pdf;
      }), resolve);
    });
  });
};

var genExtraAttachmentData = function genExtraAttachmentData(quotation, planDetails, attachmentConfig) {
  return genExtraAttachmentPdf(quotation, planDetails, attachmentConfig).then(function (data) {
    if (attachmentConfig.title) {
      CommonFunctions.addPdfTitle(data, attachmentConfig.title).then(function (pdfWithTitle) {
        return pdfWithTitle;
      });
    }
    return data;
  });
};

var getExtraAttachmentData = function getExtraAttachmentData(quotation, attachmentConfig) {
  return quotDao.getAttachment(quotation.id, attachmentConfig.saveAttId).then(function (result) {
    return result && result.data;
  });
};

/**
 * Returns possible attachment lists for the quotation.
 * - id: mandatory id for referencing the attachment
 * - label: attachment name for display
 * - hidden: true if not for display
 *
 * @param {*} agent
 * @param {*} quotation
 * @param {*} planDetails
 * @param {*} standalone
 */
var getAttachmentList = function getAttachmentList(agent, quotation, planDetails, standalone) {
  var sysAttConfigs = getSystemAttachmentConfigs(quotation, planDetails, standalone);
  var extraAttConfigs = getExtraAttachmentConfigs(agent, quotation, planDetails);
  var attachments = [];
  _.each([].concat(sysAttConfigs, extraAttConfigs), function (config) {
    attachments.push({
      id: config.id,
      label: config.label,
      hidden: config.hidden
    });
  });
  return attachments;
};

/**
 * Returns list of proposal attachment with following fields:
 * - id: mandatory id for referencing the attachment
 * - label: attachment name for display
 * - fileName: file name for download
 * - data: base64 data of the attachment
 * - saveAttId: attachment id for storage
 * - attId: attachment id for async retrieval
 * - allowSave: allow the user to save the attachment
 * - hidden: true if not for display
 *
 * @param {*} agent
 * @param {*} quotation
 * @param {*} planDetails
 * @param {*} standalone
 * @param {*} attachmentIds
 */
var getAttachments = function getAttachments(agent, quotation, planDetails, standalone, attachmentIds, async) {
  var sysAttConfigs = getSystemAttachmentConfigs(quotation, planDetails, standalone);
  var extraAttConfigs = getExtraAttachmentConfigs(agent, quotation, planDetails);
  var attIds = attachmentIds || [];

  if (!attachmentIds) {
    attIds = attIds.concat(_.map(sysAttConfigs, function (c) {
      return c.id;
    }));
    attIds = attIds.concat(_.map(extraAttConfigs, function (c) {
      return c.id;
    }));
  }

  var promises = _.map(attIds, function (attId) {
    var config = _.find(sysAttConfigs, function (c) {
      return c.id === attId;
    });
    if (config) {
      var att = {
        tabLabel: getTabLabelByQuotTypeAndId(checkIsShield(quotation), config.id, config.label),
        id: config.id,
        label: config.label,
        fileName: config.fileName,
        allowSave: config.allowSave,
        hidden: config.hidden,
        agentPassword: config.getAgentPassword && config.getAgentPassword(),
        clientPassword: config.getClientPassword && config.getClientPassword()
      };

      if (async && config.allowAsync && config.saveAttId) {
        att.attId = config.saveAttId;
        return att;
      } else {
        return config.getData().then(function (data) {
          att.data = data;
          return att;
        });
      }
    } else {
      config = _.find(extraAttConfigs, function (c) {
        return c.id === attId;
      });
      if (config) {
        var promise = config.saveAttId ? getExtraAttachmentData(quotation, config) : genExtraAttachmentData(quotation, planDetails, config);
        return promise.then(function (data) {
          return {
            tabLabel: getTabLabelByQuotTypeAndId(checkIsShield(quotation), config.id, config.label),
            id: config.id,
            label: config.label,
            fileName: config.fileName,
            data: data,
            attId: config.attId,
            allowSave: config.allowSave,
            hidden: config.hidden
          };
        });
      } else {
        return Promise.resolve();
      }
    }
  });
  return Promise.all(promises).then(function (atts) {
    return sortAttachments(atts);
  });
};

/**
 * Returns list of proposal attachment with following fields:
 * - id: mandatory id for referencing the attachment
 * - label: attachment name for display
 * - data: base64 data of the attachment
 * - saveAttId: attachment id for storage
 * - attId: attachment id for async retrieval
 * - allowSave: allow the user to save the attachment
 * - hidden: true if not for display
 *
 * @param {*} agent
 * @param {*} quotation
 * @param {*} planDetails
 * @param {*} standalone
 * @param {*} proposalData
 */
var genAttachments = function genAttachments(agent, quotation, planDetails, standalone, proposalData) {
  var promises = [];

  var sysAttConfigs = getSystemAttachmentConfigs(quotation, planDetails, standalone, proposalData);
  _.each(sysAttConfigs, function (config) {
    var promise = config.genData ? config.genData() : config.getData();
    promises.push(promise.then(function (data) {
      return {
        tabLabel: getTabLabelByQuotTypeAndId(checkIsShield(quotation), config.id, config.label),
        id: config.id,
        label: config.label,
        fileName: config.fileName,
        data: data,
        attId: config.allowAsync && config.saveAttId,
        allowSave: config.allowSave,
        hidden: config.hidden,
        saveAttId: config.saveAttId
      };
    }));
  });

  var extraAttConfigs = getExtraAttachmentConfigs(agent, quotation, planDetails);
  _.each(extraAttConfigs, function (config) {
    promises.push(genExtraAttachmentData(quotation, planDetails, config).then(function (data) {
      return {
        tabLabel: getTabLabelByQuotTypeAndId(checkIsShield(quotation), config.id, config.label),
        id: config.id,
        label: config.label,
        fileName: config.fileName,
        data: data,
        attId: config.attId,
        allowSave: config.allowSave,
        hidden: config.hidden,
        saveAttId: config.saveAttId
      };
    }));
  });
  return Promise.all(promises).then(function (atts) {
    return sortAttachments(atts);
  });
};

var sortAttachments = function sortAttachments(attachments) {
  var attSeqs = ['Policy Illustration Document(s)', 'Policy Illustration', 'Product Summary', 'Product Highlight Sheet', 'Fund Info Booklet'];
  return _.sortBy(attachments, function (att, index) {
    var seq = attSeqs.indexOf(att.label);
    return seq === -1 ? attSeqs.length + index : seq;
  });
};

var getTabLabelByQuotTypeAndId = function getTabLabelByQuotTypeAndId(isShield, id, label) {
  if ((id === 'bi' || id === 'standaloneBi') && isShield) {
    return 'Policy Illustration';
  } else if (id === 'bi' || id === 'standaloneBi') {
    return 'Policy Illustration Document(s)';
  } else {
    return label;
  }
};

var checkIsShield = function checkIsShield(quotation) {
  return _.get(quotation, 'quotType') === 'SHIELD';
};

module.exports = {
  prepareProposalData: prepareProposalData,
  getBenefitIllustration: getBenefitIllustration,
  getProductSummaries: getProductSummaries,
  getProductHighlightSheets: getProductHighlightSheets,
  getFundInfoBooklet: getFundInfoBooklet,
  getExtraAttachmentPdf: getExtraAttachmentPdf,

  getAttachmentList: getAttachmentList,
  genAttachments: genAttachments,
  getAttachments: getAttachments,
  checkIsShield: checkIsShield
};