'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _ = require('lodash');

var _require = require('./common'),
    getProfile = _require.getProfile;

var quotDao = require('../../cbDao/quotation');
var needDao = require('../../cbDao/needs');

var _require2 = require('../../../common/DateUtils'),
    formatDate = _require2.formatDate,
    parseDate = _require2.parseDate,
    formatDatetime = _require2.formatDatetime,
    parseDatetime = _require2.parseDatetime;

var _require3 = require('../../../common/ProductUtils'),
    getAgeByMethod = _require3.getAgeByMethod,
    getOccupationClass = _require3.getOccupationClass,
    getProductId = _require3.getProductId;

var _require4 = require('../../cbDao/product'),
    getProductSuitability = _require4.getProductSuitability;

var getProposerFields = function getProposerFields(proposer, basicPlan, currentDate) {
  return {
    pCid: proposer._id,
    pFullName: proposer.fullName,
    pFirstName: proposer.firstName,
    pLastName: proposer.lastName,
    pGender: proposer.gender,
    pDob: proposer.dob,
    pAge: getAgeByMethod(basicPlan.calcAgeMethod, currentDate, parseDate(proposer.dob)),
    pResidence: proposer.residenceCountry,
    pResidenceCity: proposer.residenceCity,
    pNationality: proposer.nationality,
    pPrStatus: proposer.prStatus,
    pPass: proposer.pass,
    pResidenceCountryName: getResidenceCountryName(proposer),
    pResidenceCityName: getResidenceCityName(proposer),
    pSmoke: proposer.isSmoker,
    pEmail: proposer.email,
    pOccupation: proposer.occupation,
    pOccupationClass: getOccupationClass(basicPlan, proposer.occupation)
  };
};

var getInsuredFields = function getInsuredFields(insured, basicPlan, currentDate) {
  return {
    iCid: insured._id,
    iFullName: insured.fullName,
    iFirstName: insured.firstName,
    iLastName: insured.lastName,
    iGender: insured.gender,
    iResidenceCity: insured.residenceCity,
    iNationality: insured.nationality,
    iPrStatus: insured.prStatus,
    iPass: insured.pass,
    iDob: insured.dob,
    iAge: getAgeByMethod(basicPlan.calcAgeMethod, currentDate, parseDate(insured.dob)),
    iResidence: insured.residenceCountry,
    iResidenceCountryName: getResidenceCountryName(insured),
    iResidenceCityName: getResidenceCityName(insured),
    iSmoke: insured.isSmoker,
    iEmail: insured.email,
    iOccupation: insured.occupation,
    iOccupationClass: getOccupationClass(basicPlan, insured.occupation)
  };
};

var getCommonQuotFields = function getCommonQuotFields(basicPlan, agent, companyInfo, proposer, currentDate) {
  return Object.assign({
    type: 'quotation',

    // reference fields, used in eBI/eApp
    baseProductCode: basicPlan.covCode,
    baseProductId: basicPlan._id,
    baseProductName: basicPlan.covName,
    productLine: basicPlan.productLine,
    budgetRules: basicPlan.budgetRules,
    productVersion: basicPlan.productVersion,

    compCode: agent.compCode,
    agentCode: agent.agentCode,
    dealerGroup: agent.channel.code,
    agent: {
      agentCode: agent.agentCode,
      name: agent.name,
      dealerGroup: agent.channel.code,
      company: agent.channel.type === 'AGENCY' ? companyInfo.compName : agent.company,
      tel: agent.tel,
      mobile: agent.mobile,
      email: agent.email
    }
  }, getProposerFields(proposer, basicPlan, currentDate));
};

var getQuotFields = function getQuotFields(basicPlan, agent, companyInfo, proposer, insured, ccy, currentDate) {
  return Object.assign(getCommonQuotFields(basicPlan, agent, companyInfo, proposer, currentDate), {
    sameAs: insured._id === proposer._id ? 'Y' : 'N',
    ccy: getQuotCcy(basicPlan, insured, ccy),
    policyOptions: {}
  }, getInsuredFields(insured, basicPlan, currentDate));
};

var genQuotation = function genQuotation(basicPlan, agent, companyInfo, proposer, insured, quickQuote, ccy, suitability, fna) {
  var currentDate = new Date();
  return Object.assign(getQuotFields(basicPlan, agent, companyInfo, proposer, insured, ccy, currentDate), {
    quickQuote: quickQuote,
    fund: null, // available only for investment linked products
    extraFlags: getQuotationFlags(insured, proposer, suitability, fna),
    isBackDate: 'N',
    riskCommenDate: formatDate(currentDate),
    createDate: formatDatetime(currentDate),
    lastUpdateDate: formatDatetime(currentDate)
  });
};

var genBasicQuotation = function genBasicQuotation(basicPlan, agent, companyInfo, proposer, insured) {
  var currentDate = new Date();
  return getQuotFields(basicPlan, agent, companyInfo, proposer, insured, null, currentDate);
};

var genShieldQuotation = function genShieldQuotation(basicPlan, agent, companyInfo, proposer, quickQuote) {
  var currentDate = new Date();
  return Object.assign(getCommonQuotFields(basicPlan, agent, companyInfo, proposer, currentDate), {
    quickQuote: quickQuote,
    quotType: 'SHIELD',
    insureds: {},
    isBackDate: 'N',
    riskCommenDate: formatDate(currentDate),
    createDate: formatDatetime(currentDate),
    lastUpdateDate: formatDatetime(currentDate)
  });
};

var updateClientFields = function updateClientFields(quotation, planDetails, requireFNA) {
  var bpDetail = planDetails[quotation.baseProductCode];
  var currentDate = new Date();
  if (quotation.quotType === 'SHIELD') {
    var promises = [];
    promises.push(getProfile(quotation.pCid));
    _.each(quotation.insureds, function (subQuot) {
      promises.push(getProfile(subQuot.iCid));
    });
    return Promise.all(promises).then(function (profiles) {
      _.each(profiles, function (profile) {
        if (profile.cid === quotation.pCid) {
          Object.assign(quotation, getProposerFields(profile, bpDetail, currentDate));
        }
        if (quotation.insureds[profile.cid]) {
          Object.assign(quotation.insureds[profile.cid], getInsuredFields(profile, bpDetail, currentDate));
        }
      });
    });
  } else {
    var _promises = [];
    _promises.push(getProfile(quotation.pCid));
    _promises.push(getProfile(quotation.iCid));
    _promises.push(getProductSuitability());
    if (requireFNA) {
      _promises.push(needDao.getItem(quotation.pCid, needDao.ITEM_ID.NA));
    }
    return Promise.all(_promises).then(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 4),
          proposer = _ref2[0],
          insured = _ref2[1],
          suitability = _ref2[2],
          fna = _ref2[3];

      Object.assign(quotation, getProposerFields(proposer, bpDetail, currentDate), getInsuredFields(insured, bpDetail, currentDate), {
        extraFlags: getQuotationFlags(insured, proposer, suitability, fna)
      });
    });
  }
};

var getQuotCcy = function getQuotCcy(basicPlan, insured, ccy) {
  var quotCcy = '';
  if (basicPlan.currencies instanceof Array) {
    var currency = _.find(basicPlan.currencies, function (c) {
      return c.country === '*' || c.country === insured.residenceCountry;
    });

    if (currency) {
      if (_.find(currency.ccy, function (c) {
        return c === ccy;
      })) {
        quotCcy = ccy;
      }
    }
  }
  return quotCcy;
};

var getQuotationFlags = function getQuotationFlags(insured, proposer, suitability, fna) {
  var flags = {};
  var iNR = getNationalityCategory(insured) + getResidenceCategory(insured);
  var iResult = suitability.nationalityResidence[iNR];
  var pNR = getNationalityCategory(proposer) + getResidenceCategory(proposer);
  var pResult = suitability.nationalityResidence[pNR];

  flags.nationalityResidence = {
    iCategory: iNR,
    iRejected: iResult && iResult.reject ? 'Y' : 'N',
    pCategory: pNR,
    pRejected: pResult && pResult.reject ? 'Y' : 'N'
  };

  if (fna) {
    flags.fna = {};
    var ownerRa = _.get(fna, 'raSection.owner');
    if (ownerRa) {
      var passCka = _.get(fna, 'ckaSection.owner.passCka') === 'Y';
      if (passCka && ownerRa.selfSelectedriskLevel) {
        flags.fna.riskProfile = ownerRa.selfSelectedriskLevel;
      } else {
        flags.fna.riskProfile = ownerRa.assessedRL;
      }
    }
  }
  return flags;
};

var getResidenceCountryName = function getResidenceCountryName(profile) {
  var option = _.find(global.optionsMap.residency.options, function (opt) {
    return opt.value === profile.residenceCountry;
  });
  if (option && option.title && option.title.en) {
    return option.title.en;
  } else {
    return '';
  }
};

var getResidenceCityName = function getResidenceCityName(profile) {
  var option = _.find(global.optionsMap.city.options, function (opt) {
    return opt.value === profile.residenceCity && opt.condition && profile.residenceCountry;
  });
  if (option && option.title && option.title.en) {
    return option.title.en;
  } else {
    return '';
  }
};

var getNationalityCategory = function getNationalityCategory(profile) {
  if (profile.prStatus === 'Y') {
    return 'A';
  }
  var option = _.find(global.optionsMap.nationality.options, function (opt) {
    return opt.value === profile.nationality;
  });
  return option && option.category;
};

var getResidenceCategory = function getResidenceCategory(profile) {
  var option = _.find(global.optionsMap.cityCategory.options, function (opt) {
    return opt.residency === profile.residenceCountry && (!opt.city || opt.city === profile.residenceCity);
  });

  return option && option.category;
};

var checkNationalityResidence = function checkNationalityResidence(suitability, role, nrKey) {
  var mapping = suitability.nationalityResidence[nrKey];
  if (mapping && mapping.showInQuot) {
    var messages = suitability.messages;
    if (messages[mapping.message + '_' + role]) {
      return messages[mapping.message + '_' + role];
    }
    return messages[mapping.message];
  }
  return null;
};

var getQuotationWarnings = function getQuotationWarnings(basicPlan, quotation, suitability) {
  var warnings = [];
  if (quotation.isBackDate === 'N') {
    var currentDate = new Date();
    var pAgeCurr = getAgeByMethod(basicPlan.calcAgeMethod, currentDate, parseDate(quotation.pDob));
    var iAgeCurr = getAgeByMethod(basicPlan.calcAgeMethod, currentDate, parseDate(quotation.iDob));
    if (pAgeCurr !== quotation.pAge || iAgeCurr !== quotation.iAge) {
      warnings.push('Life assured\'s age has changed. This may result in increased Premium. System to auto-backdate the policy commencement date to Policy Illustration (PI) generation date.');
    }
  }

  if (quotation.extraFlags && quotation.extraFlags.nationalityResidence) {
    var warning = checkNationalityResidence(suitability, 'I', quotation.extraFlags.nationalityResidence.iCategory);

    if (warning) {
      warnings.push(warning);
    }

    if (quotation.iCid !== quotation.pCid) {
      warning = checkNationalityResidence(suitability, 'P', quotation.extraFlags.nationalityResidence.pCategory);
      if (warning) {
        warnings.push(warning);
      }
    }
  }
  var occupationClassByPlan = _.get(suitability, 'occupationClassByPlan');

  if (basicPlan.occupClassType && occupationClassByPlan[basicPlan.occupClassType]) {
    var occKeyObject = occupationClassByPlan[basicPlan.occupClassType];
    var matchedOccupation = _.find(global.optionsMap.occupation.options, function (option) {
      return option.value === quotation.iOccupation && option[basicPlan.occupClassType] === occKeyObject.messageKey;
    });
    warning = matchedOccupation ? _.get(occKeyObject, 'warning') : undefined;

    if (warning) {
      warnings.push(warning);
    }
  }

  var crossBorderWithoutPass = _.get(suitability, 'crossBorderWithoutPass');
  if (crossBorderWithoutPass && crossBorderWithoutPass.all) {
    var _crossBorderWithoutPa = crossBorderWithoutPass.all,
        quotFieldCheck = _crossBorderWithoutPa.quotFieldCheck,
        message = _crossBorderWithoutPa.message;

    if (quotFieldCheck && message) {
      var isCrossBorderWithoutPass = quotation[quotFieldCheck];
      if (isCrossBorderWithoutPass) {
        warnings.push(message);
      }
    }
  }
  return warnings;
};

var hasCrossAge = function hasCrossAge(quotation, bpDetail) {
  var currentDate = new Date();
  var pAgeCurr = getAgeByMethod(bpDetail.calcAgeMethod, currentDate, parseDate(quotation.pDob));
  if (quotation.quotType === 'SHIELD') {
    return pAgeCurr !== quotation.pAge || _.find(quotation.insureds, function (subQuot) {
      var iAgeCurr = getAgeByMethod(bpDetail.calcAgeMethod, currentDate, parseDate(subQuot.iDob));
      return iAgeCurr !== subQuot.iAge;
    });
  } else {
    var iAgeCurr = getAgeByMethod(bpDetail.calcAgeMethod, currentDate, parseDate(quotation.iDob));
    return pAgeCurr !== quotation.pAge || iAgeCurr !== quotation.iAge;
  }
};

var hasProfileUpdate = function hasProfileUpdate(quotation, profiles) {
  var lastUpdateDate = parseDatetime(quotation.lastUpdateDate);
  return _.find(profiles, function (profile) {
    return parseDatetime(profile.lastUpdateDate) > lastUpdateDate;
  });
};

var getFunds = function getFunds(compCode, fundCodes, paymentMethod, invOpt) {
  return new Promise(function (resolve) {
    quotDao.queryFunds(compCode, fundCodes, paymentMethod, invOpt === 'mixedAssets', resolve);
  });
};

var transformShieldQuot = function transformShieldQuot(quotation) {
  var insureds = {};
  _.each(quotation.insureds, function (subQuot, cid) {
    if (subQuot.plans.length === 0) {
      insureds[cid] = subQuot;
    } else {
      insureds[cid] = [_.cloneDeep(subQuot), _.cloneDeep(subQuot)];
      var plans = subQuot.plans;
      insureds[cid][0].plans = [plans[0]];
      insureds[cid][1].plans = [_.slice(plans, 1)];
    }
  });
  var quot = _.cloneDeep(quotation);
  quot.insureds = insureds;
  return quot;
};

var getRelatedProfiles = function getRelatedProfiles(pCid) {
  return getProfile(pCid).then(function (profile) {
    var cids = [pCid];
    _.each(profile.dependants, function (dependant) {
      cids.push(dependant.cid);
    });
    return cids;
  }).then(function (dCids) {
    return Promise.all(_.map(dCids, function (dCid) {
      return getProfile(dCid);
    }));
  });
};

var getQuotClients = function getQuotClients(quotation) {
  var cids = void 0;
  if (quotation.quotType === 'SHIELD') {
    cids = [quotation.pCid];
    _.each(quotation.insureds, function (subQuot) {
      cids.push(subQuot.iCid);
    });
  } else {
    cids = [quotation.pCid, quotation.iCid];
  }
  return Promise.all(_.map(cids, function (cid) {
    return getProfile(cid);
  }));
};

var updateQuot = function updateQuot(quotation, planDetails, newQuot) {
  if (newQuot) {
    var currentDate = new Date();
    quotation.id = null;
    quotation.lastUpdateDate = formatDatetime(currentDate);
    quotation.createDate = formatDatetime(currentDate);
  }
  quotation.baseProductId = planDetails[quotation.baseProductCode]._id;

  // Remove campaign when requote and clone quotation
  quotation.plans = _.map(quotation && quotation.plans, function (plan) {
    return _.omit(plan, ['campaignIds', 'campaignCodes']);
  });
  updateProductIds(quotation, planDetails);
};

var updateProductIds = function updateProductIds(quotation, planDetails) {
  var plans = quotation.plans;
  if (quotation.quotType === 'SHIELD') {
    plans = [];
    _.each(quotation.insureds, function (subQuot) {
      plans = plans.concat(subQuot.plans);
    });
  }
  _.each(plans, function (plan) {
    plan.productId = getProductId(planDetails[plan.covCode]);
  });
};

module.exports = {
  genQuotation: genQuotation,
  genBasicQuotation: genBasicQuotation,
  genShieldQuotation: genShieldQuotation,
  updateClientFields: updateClientFields,
  getQuotationWarnings: getQuotationWarnings,
  hasCrossAge: hasCrossAge,
  hasProfileUpdate: hasProfileUpdate,
  getFunds: getFunds,
  transformShieldQuot: transformShieldQuot,
  getRelatedProfiles: getRelatedProfiles,
  getQuotClients: getQuotClients,
  updateQuot: updateQuot,
  updateProductIds: updateProductIds
};