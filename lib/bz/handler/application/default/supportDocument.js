'use strict';

var _ = require('lodash');
var logger = global.logger || console;
var appDao = require('../../../cbDao/application');
var bDao = require('../../../cbDao/bundle');
var dao = require('../../../cbDaoFactory').create();

var _validateOtherDocumentName = function _validateOtherDocumentName(documentName, app, tabId) {
  var reservedNamesDocId = 'suppDocsDefaultDocNames';
  var otherDocValuesArr = _.get(app, 'supportDocuments.values.' + tabId + '.otherDoc.template', []);

  return new Promise(function (resolve) {
    dao.getDocFromCacheFirst(reservedNamesDocId, function (reservedNamesDoc) {
      // 1.Check whether is duplicated with reserved names
      if (_.includes(reservedNamesDoc.defaultDocumentNames, documentName)) {
        resolve(true);
      }

      // 2.Check whether is duplicated with current otherDoc names
      for (var index in otherDocValuesArr) {
        if (otherDocValuesArr[index].title === documentName) {
          resolve(true);
        }
      }

      // 3.Check whether is duplicated with 'e-App (iName/pName)' for Shield case
      if (app.type === 'masterApplication') {
        if ('e-App (' + _.get(app, 'quotation.pFullName') + ')' === documentName) {
          resolve(true);
        }
        _.forEach(_.get(app, 'quotation.insureds'), function (insuredObj) {
          if ('e-App (' + insuredObj.iFullName + ')' === documentName) {
            resolve(true);
          }
        });
      }

      resolve(false);
    });
  });
};

var _updateOtherDocumentName = function _updateOtherDocumentName(data, session, cb) {
  var methodName = 'updateOtherDocumentName';
  var appId = data.appId;
  var tabId = data.tabId;
  var documentId = data.documentId;
  var newName = _.trim(data.newName);
  var docNameOption = _.trim(data.docNameOption);
  var docNameTitle = docNameOption !== 'Other' ? docNameOption : newName;
  var cbValues = void 0;

  logger.log('INFO: ' + methodName + ' - start', appId, documentId);

  appDao.getApplication(appId, function (app) {
    if (app) {
      _validateOtherDocumentName(docNameTitle, app, tabId).then(function (isDuplicated) {
        if (!isDuplicated) {
          return bDao.getApplicationByBundleId(app.bundleId, app.id).then(function (bApp) {
            if (bApp.appStatus === 'SUBMITTED') {
              cbValues = data.rootValues;
            } else {
              cbValues = _.get(app, 'supportDocuments.values');
            }

            // const otherDocumentArr = _.get(app, 'supportDocuments.values.' + tabId + '.otherDoc.template');
            var otherDocumentArr = _.get(cbValues, tabId + '.otherDoc.template');
            var index = _.findIndex(otherDocumentArr, function (obj) {
              return obj.id === documentId;
            });

            if (index > -1) {
              if (bApp.appStatus === 'SUBMITTED') {
                _.set(cbValues, tabId + '.otherDoc.template.' + index + '.title', newName);
                _.set(cbValues, tabId + '.otherDoc.template.' + index + '.docNameOption', docNameOption);
                _.set(cbValues, tabId + '.otherDoc.template.' + index + '.docName', newName);
                cb({
                  success: true,
                  values: cbValues
                });
              } else {
                _.set(app, 'supportDocuments.values.' + tabId + '.otherDoc.template.' + index + '.title', newName);
                _.set(app, 'supportDocuments.values.' + tabId + '.otherDoc.template.' + index + '.docNameOption', docNameOption);
                _.set(app, 'supportDocuments.values.' + tabId + '.otherDoc.template.' + index + '.docName', newName);
                appDao.upsertApplication(appId, app, function (resp) {
                  if (resp) {
                    logger.log('INFO: ' + methodName + ' - end [RETURN=1]', appId, documentId);
                    cb({
                      success: true,
                      values: _.get(app, 'supportDocuments.values')
                    });
                  } else {
                    logger.error('INFO: ' + methodName + ' - end [RETURN=2] - update app fail', appId, documentId);
                    cb({ success: false });
                  }
                });
              }
            } else {
              logger.error('INFO: ' + methodName + ' - end [RETURN=4] - get otherDoc existing template fails', appId, documentId);
              cb({ success: false });
            }
          });
        } else {
          logger.log('INFO: ' + methodName + ' - end [RETURN=3] - newName is duplicated', appId, documentId);
          cb({
            success: true,
            duplicated: true
          });
        }
      }).catch(function (error) {
        logger.error('Error in ' + methodName + ' - _validateOtherDocumentName: ', error);
      });
    } else {
      logger.error('Error in ' + methodName + ' : cannot get application document');
    }
  });
};

module.exports.validateOtherDocumentName = _validateOtherDocumentName;
module.exports.updateOtherDocumentName = _updateOtherDocumentName;