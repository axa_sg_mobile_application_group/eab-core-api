'use strict';

var _ = require('lodash');
var moment = require('moment');
var async = require('async');
var logger = global.logger || console;

var utils = require('../../../Quote/utils');
var CommonUtils = require('../../../utils/CommonUtils');
var DateUtils = require('../../../../common/DateUtils');
var bundleCommon = require('../../../cbDao/bundle/common');
var commonApp = require('../common');
var dao = require('../../../cbDaoFactory').create();
var bDao = require('../../../cbDao/bundle');
var cDao = require('../../../cbDao/client');
var nDao = require('../../../cbDao/needs');
var pDao = require('../../../cbDao/product');
var appDao = require('../../../cbDao/application');
var PDFHandler = require('../../../PDFHandler');
var needsHandler = require('../../../NeedsHandler');
var agentDao = require('../../../cbDao/agent');

var _getNameOfOrganization = function _getNameOfOrganization(upline2Code, callback) {
  logger.log('INFO: _getNameOfOrganization->searchAgents upline2Code: ', upline2Code);
  agentDao.searchAgents('01', upline2Code, upline2Code, function (res) {
    if (res.success) {
      if (res.result.length > 0) {
        callback(res.result[0].agentName);
      } else {
        logger.error('Error in _getNameOfOrganization->searchAgents upline2Code: ', upline2Code);
        callback(null);
      }
    } else {
      logger.error('Error in _getNameOfOrganization->searchAgents upline2Code: ', upline2Code);
      callback(null);
    }
  });
};

var _genMenuForm = function _genMenuForm(forms, title, detailSeq) {
  var result = {
    'title': title,
    'type': 'menuItem',
    'skipCheck': forms.skipCheck,
    'key': forms.formId || forms.id || forms._id,
    'detailSeq': detailSeq,
    'items': []
  };

  if (forms.id && (forms.id === 'menu_plan' || forms.id === 'plan')) {
    result.items = forms.items;
  } else {
    var tabs = {
      type: 'tabs',
      items: forms.items
    };
    result.items.push(tabs);
  }
  return result;
};

var _getAppFormTemplate = function _getAppFormTemplate(quotation, callback) {
  var isPhSameAsLa = false;
  if (quotation.iCid === quotation.pCid) {
    isPhSameAsLa = true;
  }
  var plans = quotation.plans;
  var dynTemplate = {};

  var allDone = function allDone(sections) {
    var eFormMenus = [];
    if (sections && sections.length) {
      for (var s in sections) {
        var sectTmp = {
          type: 'menuSection',
          detailSeq: parseInt(s),
          items: []
        };
        var forms = sections[s];
        for (var f in forms.items) {
          var form = forms.items[f];
          if (form.items && form.items.length) {
            sectTmp.items.push(_genMenuForm(form, form.title, parseInt(f)));
          }
        }
        eFormMenus.push(sectTmp);
      }
    }

    callback({
      items: [{
        // "id": "stepApp",  comment by sam
        'type': 'section',
        'title': 'Application',
        'detailSeq': 1,
        'items': [{
          'type': 'menu',
          'items': eFormMenus
        }]
      }, {
        'id': 'stepSign',
        'type': 'section',
        'title': 'Signature',
        'detailSeq': 2,
        'items': [{
          'id': 'sign',
          'type': 'signature'
        }]
      }, {
        'id': 'stepPay',
        'type': 'section',
        'title': 'Payment',
        'detailSeq': 3,
        'items': [{
          'id': 'pay',
          'type': 'payment'
        }]
      }, {
        'id': 'stepSubmit',
        'type': 'section',
        'title': 'Submit',
        'detailSeq': 4,
        'items': [{
          'id': 'submit',
          'type': 'submission'
        }]
      }]
    }, sections, dynTemplate);
  };

  var getInsForms = function getInsForms(insForms, forms, index, cb) {
    logger.log('INFO: getAppFormTemplate - getInsForms', index, isPhSameAsLa);
    if (forms.length > index) {
      var form = forms[index];
      _.set(dynTemplate, 'insurability.' + form.id + '.' + form.covCode, form.form);

      dao.getDocFromCacheFirst(form.form, function (tpl) {
        if (tpl && !tpl.error) {
          var template = _.cloneDeep(tpl);
          if (isPhSameAsLa) {
            template.item.subType = 'proposer';
          } else if (form.id === 'ph') {
            template.item.subType = 'proposer';
          } else if (form.id === 'la') {
            template.item.subType = 'insured';
          }
          // assume only one insure form for proposer/insured
          // TODO::: handle multiple insur form for proposer/insured

          insForms.formId = template.formId;
          insForms.items.push(template.item);
        }
        getInsForms(insForms, forms, index + 1, cb);
      });
    } else {
      cb();
    }
  };

  var getTemplate = function getTemplate(forms, index, cb) {
    if (forms.length > index) {
      var form = forms[index];

      if (typeof form.form === 'string') {
        dynTemplate[form.id] = form.form;

        dao.getDocFromCacheFirst(form.form, function (tpl) {
          if (tpl && !tpl.error) {
            var template = _.cloneDeep(tpl);
            if (isPhSameAsLa) {
              var newItems = [];
              newItems.push(template.items[0]);
              // newItems[0].priority = "";
              newItems[0].subType = 'proposer';
              template.items = newItems;
            }

            delete form.form;
            form.formId = template.formId;
            form.skipCheck = template.skipCheck;
            form.items = template.items;
          }
          logger.log('INFO: getAppFormTemplate - getTemplate', index, '(got forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
          getTemplate(forms, index + 1, cb);
        });
      } else {
        var insForms = [];
        if (!isPhSameAsLa && form.form.ph) {
          var _loop = function _loop(i) {
            var plan = plans[i];
            var tplCode = form.form.ph[plan.covCode];
            if (tplCode && plan.covCode !== 'ESC') {
              if (_.findIndex(insForms, function (it) {
                return it.form === tplCode;
              }) === -1) {
                insForms.push({
                  id: 'ph',
                  form: tplCode,
                  covCode: plan.covCode
                });
              }
            }
          };

          for (var i in plans) {
            _loop(i);
          }
        }

        if (form.form.la) {
          for (var i in plans) {
            var _plan = plans[i];
            if (form.form.la[_plan.covCode]) {
              (function () {
                var tplCode = form.form.la[_plan.covCode];
                if (_.findIndex(insForms, function (it) {
                  return it.form === tplCode;
                }) === -1) {
                  insForms.push({
                    id: 'la',
                    form: tplCode,
                    covCode: _plan.covCode
                  });
                }
              })();
            }
          }
        }

        // Filter out basic plan template when there is different template used in Rider
        var baseProductCode = plans[0].covCode;
        var isBPDuplicateinLa = _.filter(insForms, function (insForm) {
          return insForm.covCode !== baseProductCode && insForm.id === 'la';
        }).length;
        var isBPDuplicateinPh = _.filter(insForms, function (insForm) {
          return insForm.covCode !== baseProductCode && insForm.id === 'ph';
        }).length;
        var removeArray = [];

        if (isBPDuplicateinLa) {
          removeArray.push('la');
        }
        if (isBPDuplicateinPh) {
          removeArray.push('ph');
        }
        _.each(removeArray, function (removeArrayId) {
          insForms = _.filter(insForms, function (insForm) {
            if (insForm.covCode === baseProductCode && insForm.id === removeArrayId) {
              return false;
            } else {
              return true;
            }
          });
        });

        if (insForms.length > 0) {
          delete form.form;
          form.items = [];
          getInsForms(form, insForms, 0, function () {
            logger.log('INFO: getAppFormTemplate - getTemplate', index, '(got Ins forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
            getTemplate(forms, index + 1, cb);
          });
        } else {
          getTemplate(forms, index + 1, cb);
        }
      }
    } else {
      cb();
    }
  };

  var getSectionItems = function getSectionItems(sections, index) {
    logger.log('INFO: getAppFormTemplate - getSectionItems [index=', index, '; numOfSection=', sections.length, ']');
    if (sections.length > index) {
      getTemplate(sections[index].items, 0, function () {
        getSectionItems(sections, index + 1);
      });
    } else {
      allDone(sections);
    }
  };

  dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (mapping) {
    if (mapping && mapping[quotation.baseProductCode]) {
      var sections = _.cloneDeep(mapping[quotation.baseProductCode].eFormSections);
      if (sections && sections.length) {
        getSectionItems(sections, 0);
      } else {
        allDone(sections);
      }
    } else {
      logger.error('ERROR: getAppFormTemplate - [RETURN=-1]');
    }
  });
};

var _checkCrossAge = function _checkCrossAge(app, callback) {
  var appId = app.id;
  var productId = _.get(app, 'quotation.baseProductId');
  var days = global.config.crossAgeDay;
  logger.log('INFO: checkCrossAge - start', appId);

  var willCrossAgeInDays = function willCrossAgeInDays(nowAge, inDays, ageCrossLine) {
    var futureDate = new Date();
    futureDate.setDate(futureDate.getDate() + inDays);
    return ageCrossLine === DateUtils.getNearestAge(futureDate, nowAge);
  };

  var getCrossAgeStatus = function getCrossAgeStatus(dobDate, ageCrossLine, bundleApp) {
    var currentAge = DateUtils.getNearestAge(new Date(), dobDate);
    if (currentAge - ageCrossLine >= 0) {
      // crossed age
      if (!app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
        return {
          status: commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED,
          crossedAge: true
        };
        // appStatus need to change to get from bundle
      } else if (app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
        if (_.get(bundleApp, 'appStatus') === 'APPLYING') {
          return {
            status: commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED,
            crossedAge: true
          };
        }
      } else {
        return {
          status: commonApp.CROSSAGE_STATUS.CROSSED_AGE_NO_ACTION,
          crossedAge: true
        };
      }
    } else {
      if (willCrossAgeInDays(dobDate, days, ageCrossLine)) {
        if (app.quotation.isBackDate === 'N' && !app.isBackDate && !app.isStartSignature) {
          return {
            status: commonApp.CROSSAGE_STATUS.WILL_CROSSAGE
          };
        } else {
          return {
            status: commonApp.CROSSAGE_STATUS.WILL_CROSSAGE_NO_ACTION
          };
        }
      } else {
        return {
          status: commonApp.CROSSAGE_STATUS.NO_CROSSAGE
        };
      }
    }
  };

  var needCheckProposerCrossAge = function needCheckProposerCrossAge() {
    return new Promise(function (resolve) {
      // It is a third-party application where Proposer is not Life Assured AND
      if (_.get(app, 'quotation.iCid') !== _.get(app, 'quotation.pCid')) {
        // Proposer has selected a rider product AND
        // Rider’s age cross has an impact on premium (all Riders which are for Proposer (titled PH in medical questions product matrix)
        var riders = _.drop(_.get(app, 'quotation.plans', []));
        dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (appFormMapping) {
          var proposerRiders = _.keys(_.get(_.find(_.get(appFormMapping, app.quotation.baseProductCode + '.eFormSections[0].items'), function (obj) {
            return obj.id === 'insurability';
          }, {}), 'form.ph', {}));
          var checkProposer = false;
          for (var i in riders) {
            if (_.includes(proposerRiders, riders[i].covCode)) {
              checkProposer = true;
            }
          }
          resolve(checkProposer);
        });
      } else {
        resolve(false);
      }
    });
  };

  pDao.getProduct(productId, function (product) {
    if (product && app) {
      console.log('INFO: checkCrossAge - product is', product.covName['en']);
      bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then(function (bundleApp) {
        return needCheckProposerCrossAge().then(function (checkProposer) {
          console.log('INFO: checkCrossAge - needCheckProposerCrossAge', checkProposer);
          var proposerResult = {},
              insuredResult = {};
          var crossedAge = false;
          var allowBackdate = product.allowBackdate === 'Y';

          insuredResult = getCrossAgeStatus(new Date(_.get(app, 'quotation.iDob')), _.get(app, 'quotation.iAge') + 1, bundleApp);
          if (checkProposer) {
            proposerResult = getCrossAgeStatus(new Date(_.get(app, 'quotation.pDob')), _.get(app, 'quotation.pAge') + 1, bundleApp);
          }

          crossedAge = insuredResult.crossedAge || proposerResult.crossedAge || false;

          console.log('INFO: checkCrossAge - end [RETURN=-0] - insuredResult, proposerResult, allowBackdate, crossedAge', insuredResult, proposerResult, allowBackdate, crossedAge);
          callback({
            success: true,
            insuredStatus: insuredResult.status || 0,
            proposerStatus: proposerResult.status || 0,
            allowBackdate: allowBackdate,
            crossedAge: crossedAge
          });
        });
      }).catch(function (err) {
        logger.error('ERROR: checkCrossAge - getApplicationByBundleId [RETURN=-2]', appId, err);
        callback({ success: false });
      });
    } else {
      logger.error('ERROR: checkCrossAge - end [RETURN=-1 (app:', _.get(app, 'error'), ')(product:', _.get(product, 'error'), ')]', appId);
      callback({ success: false });
    }
  });
};

var _genAppFormPdfSectionVisible = function _genAppFormPdfSectionVisible(values, callback) {
  var showQuestions = {
    residency: {
      question: []
    },
    foreigner: {
      question: []
    },
    policies: {
      question: []
    },
    insurability: {
      question: []
    },
    declaration: {
      question: []
    }
  };

  var _loop2 = function _loop2(menuItemKey) {
    commonApp.checkAppFormByPerson(values.proposer, menuItemKey, showQuestions);
    values.insured.forEach(function (la, index) {
      commonApp.checkAppFormByPerson(la, menuItemKey, showQuestions);
    });
  };

  for (var menuItemKey in showQuestions) {
    _loop2(menuItemKey);
  }

  callback(showQuestions);
};

var _genAppFormPdfData = function _genAppFormPdfData(app, lang, callback) {
  logger.log('INFO: _genAppFormPdfData - start', app._id);
  var keyStr = 'applicationForm.values.appFormTemplate.properties.dollarSignForAllCcy';
  var isShowDollarSign = _.get(app, keyStr, true);
  var dollarSign = isShowDollarSign ? '$' : utils.getCurrencySign(_.get(app, 'quotation.ccy'));
  var ccy = isShowDollarSign ? null : _.get(app, 'quotation.ccy');

  var trigger = {};

  _getAppFormTemplate(app.quotation, function (template) {

    commonApp.getOptionsList(app, template, function () {
      var appTemplate = template.items[0].items[0]; //contain items of menuSection
      var appOrgValues = app.applicationForm.values;
      var appNewValues = _.cloneDeep(app.applicationForm.values);

      commonApp.replaceAppFormValuesByTemplate(appTemplate, appOrgValues, appNewValues, trigger, [], lang, ccy);

      appNewValues.lang = lang;
      appNewValues.baseProductCode = app.quotation.baseProductCode;
      appNewValues.policyNumber = app.policyNumber || app._id;
      appNewValues.showLaSignature = commonApp.checkLaIsAdult(app);
      appNewValues.agent = app.quotation.agent;
      if (app.agentCodeDisp) {
        appNewValues.agent.agentCodeDisp = app.agentCodeDisp;
      }
      if (app.nameOfOrganization) {
        appNewValues.agent.company = app.nameOfOrganization;
      } else {
        appNewValues.agent.company = '';
      }
      appNewValues.policyOptions = app.quotation.policyOptions;
      appNewValues.policyOptionsDesc = app.quotation.policyOptionsDesc;
      appNewValues.payment = app.payment;

      if (app.quotation.fund) {
        appNewValues.fund = app.quotation.fund;
      }

      var policyOptions = appNewValues.policyOptions;

      //format currency in policyOptions
      if (policyOptions) {
        if (policyOptions.topUpAmt && policyOptions.topUpAmt !== 'null') {
          policyOptions.topUpAmt = utils.getCurrency(policyOptions.topUpAmt, dollarSign, 2);
        }

        if (policyOptions.rspAmount && policyOptions.rspAmount !== 'null') {
          policyOptions.rspAmount = utils.getCurrency(policyOptions.rspAmount, dollarSign, 2);
        }

        if (policyOptions.wdAmount && policyOptions.wdAmount !== 'null') {
          policyOptions.wdAmount = utils.getCurrency(policyOptions.wdAmount, dollarSign, 2);
        }
      }

      var planDetails = appNewValues.planDetails;

      //format currency in planDetails
      if (planDetails) {
        if (planDetails.rspAmount && planDetails.rspAmount !== 'null') {
          planDetails.rspAmount = utils.getCurrency(planDetails.rspAmount, dollarSign, 2);
        }

        if (planDetails.wdAmount && planDetails.wdAmount !== 'null') {
          planDetails.wdAmount = utils.getCurrency(planDetails.wdAmount, dollarSign, 2);
        }
      }

      if (planDetails && planDetails.planList) {
        if (planDetails.totYearPrem) {
          planDetails.totYearPrem = utils.getCurrency(planDetails.totYearPrem, dollarSign, 2);
        }

        for (var i = 0; i < planDetails.planList.length; i++) {
          var _plan2 = planDetails.planList[i];
          for (var key in _plan2) {
            if (key === 'halfYearPrem' || key === 'monthPrem' || key === 'premium' || key === 'quarterPrem' || key === 'yearPrem' || key === 'sumInsured') {
              _plan2[key] = utils.getCurrency(_plan2[key], dollarSign, 2);
            }
          }
        }
      }

      //handle residency question (PR / Type of Pass)
      _.set(appNewValues, 'proposer.residency.groupOnePass', '');
      if (_.get(appOrgValues, 'proposer.personalInfo.prStatus') === 'Y' || _.filter(appOrgValues.insured, function (la) {
        return _.get(la, 'personalInfo.prStatus') === 'Y';
      }).length > 0) {
        _.set(appNewValues, 'proposer.residency.groupOnePass', 'Singapore Permanent Resident');
      }

      var _handlePass = function _handlePass(groupPass, checkValue, useValue) {
        var phPass = _.get(appOrgValues, 'proposer.personalInfo.pass');
        var laPass = appOrgValues.insured.map(function (la) {
          return _.get(la, 'personalInfo.pass');
        });
        var phPassText = _.get(appNewValues, 'proposer.personalInfo.pass');
        var laPassText = appNewValues.insured.map(function (la) {
          return _.get(la, 'personalInfo.' + useValue);
        });
        if (phPass === checkValue || laPass.indexOf(checkValue) >= 0) {
          var passText = phPass === checkValue ? phPassText : laPassText[laPass.indexOf(checkValue)];
          var orgPassText = _.get(appNewValues, 'proposer.residency.' + groupPass, '');
          var addPassText = (orgPassText.length > 0 ? ' / ' : '') + passText;
          _.set(appNewValues, 'proposer.residency.' + groupPass, orgPassText + addPassText);
        }
      };
      _handlePass('groupOnePass', 'ep', 'pass');
      _handlePass('groupOnePass', 'sp', 'pass');
      _handlePass('groupOnePass', 'wp', 'pass');

      //Uppercase personalInfo
      for (var _key in appNewValues.proposer.personalInfo) {
        var info = appNewValues.proposer.personalInfo[_key];
        if (typeof info === 'string' && _key !== 'email' && _key !== 'isSameAddr') {
          appNewValues.proposer.personalInfo[_key] = info.toUpperCase();
        }
      }
      for (var _i = 0; _i < appNewValues.insured.length; _i++) {
        for (var _key2 in appNewValues.insured[_i].personalInfo) {
          var _info = appNewValues.insured[_i].personalInfo[_key2];
          if (typeof _info === 'string' && _key2 !== 'email' && _key2 !== 'isSameAddr') {
            appNewValues.insured[_i].personalInfo[_key2] = _info.toUpperCase();
          }
        }
      }
      //Uppercase payor & trusted individual information
      for (var _key3 in appNewValues.proposer.declaration) {
        var _info2 = appNewValues.proposer.declaration[_key3];
        if (typeof _info2 === 'string' && commonApp.CAPITAL_LETTER_IDS.indexOf(_key3) >= 0) {
          appNewValues.proposer.declaration[_key3] = _info2.toUpperCase();
        } else if (_key3 === 'trustedIndividuals') {
          appNewValues.proposer.declaration.trustedIndividuals.fullName = _.toUpper(appNewValues.proposer.declaration.trustedIndividuals.fullName);
        }
      }

      for (var _key4 in appOrgValues) {
        if (_key4 === 'proposer') {
          commonApp.removeAppFormValuesByTrigger(_key4, appOrgValues[_key4], appNewValues[_key4], appOrgValues[_key4], trigger[_key4], true, appOrgValues);
        } else if (_key4 === 'insured') {
          for (var _i2 = 0; _i2 < appOrgValues[_key4].length; _i2++) {
            var iOrgValues = appOrgValues[_key4][_i2];
            var iNewValues = appNewValues[_key4][_i2];
            var iTrigger = trigger[_key4][_i2];
            commonApp.removeAppFormValuesByTrigger(_key4, iOrgValues, iNewValues, iOrgValues, iTrigger, true, appOrgValues);
          }
        }
      }

      //Grouping HEALTH data into one array
      var groupingHealthTableData = function groupingHealthTableData(person, groupHealthKey, groupedKey) {
        var groupHealthData = [];

        for (var _i3 = 0; _i3 < groupHealthKey.length; _i3++) {
          var groupHealthDataKey = groupHealthKey[_i3] + '_DATA';

          var healthDataArray = _.get(person, 'insurability.' + groupHealthDataKey);
          if (healthDataArray) {
            for (var j = 0; j < healthDataArray.length; j++) {
              var healthDataRow = healthDataArray[j];
              var groupedRow = {};
              for (var _key5 in healthDataRow) {
                var postfix = _key5.replace(groupHealthKey[_i3], '');

                groupedRow['HEALTH_GROUP' + postfix] = healthDataRow[_key5];
                groupedRow.DATATABLE = groupHealthDataKey;
                groupedRow.SHOW_QN = j === 0 ? 'Y' : 'N';
                groupedRow.ROW_COUNT = healthDataArray.length;
              }
              groupHealthData.push(groupedRow);
            }
          }
        }

        if (groupHealthData.length > 0) {
          person.insurability[groupedKey] = groupHealthData;
        }
      };

      var groupHealthKey = ['HEALTH02', 'HEALTH03', 'HEALTH04', 'HEALTH05', 'HEALTH06', 'HEALTH07', 'HEALTH01', 'HEALTH08', 'HEALTH09', 'HEALTH10', 'HEALTH11'];
      var groupedKey = 'HEALTH_GROUP_DATA';
      if (appNewValues.baseProductCode === 'HIM' || appNewValues.baseProductCode === 'HER') {
        groupHealthKey = ['HEALTH_HER02', 'HEALTH_HER03', 'HEALTH_HER04', 'HEALTH_HER05', 'HEALTH_HER06', 'HEALTH_HER07', 'HEALTH_HER08', 'HEALTH_HIM02', 'HEALTH_HIM03', 'HEALTH_HIM04'];
        groupedKey = 'HEALTH_GROUP_DATA';
      }
      groupingHealthTableData(appNewValues.proposer, groupHealthKey, groupedKey);
      var insured = appNewValues.insured;

      if (insured) {
        for (var _i4 = 0; _i4 < insured.length; _i4++) {
          groupingHealthTableData(appNewValues.insured[_i4], groupHealthKey, groupedKey);
        }
      }

      _genAppFormPdfSectionVisible(appNewValues, function (showQuestions) {
        appNewValues.showQuestions = showQuestions;
        appNewValues.originalData = app.applicationForm.values;
        logger.log('INFO: _genAppFormPdfData - end', app._id);
        callback(appNewValues);
      });
    });
  });
};

var _genApplicationPDF = function _genApplicationPDF(application, session, callback) {
  //get client id
  var appId = application.id;
  var clientId = application.pCid;
  var isFaChannel = session.agent.channel.type === 'FA';
  var agentCodeDisp = session.agent.agentCodeDisp;
  if (agentCodeDisp) {
    application.agentCodeDisp = agentCodeDisp;
  }
  var upline2Code = session.agent.rawData.upline2Code;
  logger.log('INFO: _genApplicationPDF - start', appId);

  //get bundle
  bDao.getCurrentBundle(clientId).then(function (bundle) {
    // let bundleId = _.get(bundle, 'id');
    var lang = 'en';

    //4. call the function to get FNA report string and update bundle
    var generateFNAReportPdf = function generateFNAReportPdf(profile) {
      logger.log('INFO: _genApplicationPDF - (4) generateFNAReport', appId);
      if (profile && !profile.error) {
        needsHandler.generateFNAReport({ cid: profile.cid }, session, function (pdfResult) {
          if (!pdfResult.fnaReport) {
            logger.error('ERROR: _genApplicationPDF - (4) generateFNAReport [RETURN=-4]', appId);
            callback({ success: false });
          } else {
            bDao.getCurrentBundle(profile.cid).then(function (bundle) {
              dao.uploadAttachmentByBase64(bundle.id, commonApp.PDF_NAME.FNA, bundle._rev, pdfResult.fnaReport, 'application/pdf', function (res) {
                // bDao.updateStatus(profile.cid, bDao.BUNDLE_STATUS.START_GEN_PDF, (newBundle) => {
                bDao.updateStatus(profile.cid, bDao.BUNDLE_STATUS.START_GEN_PDF).then(function (newBundle) {
                  logger.log('INFO: _genApplicationPDF - end [RETURN=2]', appId);
                  callback({ success: true });
                }).catch(function (error) {
                  logger.error('ERROR: _genApplicationPDF - end [RETURN=2] fails:', error);
                  callback({ success: false });
                });
              });
            });
          }
        });
      } else {
        logger.error('ERROR: _genApplicationPDF - (4) generateFNAReport [RETURN=-3]', appId);
        callback({ success: false });
      }
    };

    //3. check FNA is signed, if no, generate FNA report
    var checkFnaReportSigned = function checkFnaReportSigned(cid) {
      logger.log('INFO: _genApplicationPDF - (3) checkFnaReportSigned', bundle.isFnaReportSigned, appId);
      if (bundle.isFnaReportSigned || isFaChannel) {
        logger.log('INFO: _genApplicationPDF - end [RETURN=1]', appId);
        callback({ success: true });
      } else {
        cDao.getProfileById(cid, function (profile) {
          generateFNAReportPdf(profile);
        });
      }
    };

    //2. generate App Form Pdf
    var generateAppFormPdf = function generateAppFormPdf(appFormTemplates) {
      logger.log('INFO: _genApplicationPDF - (2) generateAppFormPdf', appId);

      var reportData = {};
      _getNameOfOrganization(upline2Code, function (nameOfOrganization) {
        if (nameOfOrganization) {
          application.nameOfOrganization = nameOfOrganization;
        }

        _genAppFormPdfData(application, lang, function (data) {
          dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], function (pda) {
            if (pda && !pda.error) {
              logger.log('INFO: _genApplicationPDF - (2) generateAppFormPdf - populate PDA data', appId);
              //For not FA channel
              if (pda.ownerConsentMethod) {
                var ownerConsentMethodList = pda.ownerConsentMethod.split(',');

                for (var i = 0; i < ownerConsentMethodList.length; i++) {
                  var method = ownerConsentMethodList[i];
                  if (method === 'phone') {
                    data.proposer.declaration.PDPA01a = 'Yes';
                  } else if (method === 'text') {
                    data.proposer.declaration.PDPA01b = 'Yes';
                  } else if (method === 'fax') {
                    data.proposer.declaration.PDPA01c = 'Yes';
                  }
                }
              }
            }

            reportData.root = data;

            // handle mutiple template
            var reportTemplates = [];
            for (var _i5 = 0; _i5 < appFormTemplates.length; _i5++) {
              if (appFormTemplates[_i5] instanceof Array) {
                reportTemplates.push.apply(reportTemplates, appFormTemplates[_i5]);
              } else {
                reportTemplates.push(appFormTemplates[_i5]);
              }
            }

            PDFHandler.getAppFormPdf(reportData, reportTemplates, lang, function (pdfStr) {
              dao.uploadAttachmentByBase64(appId, commonApp.PDF_NAME.eAPP, application._rev, pdfStr, 'application/pdf', function (res) {
                checkFnaReportSigned(clientId);
              });
            });
          });
        });
      });
    };

    //1. get App Form Pdf template from CB
    var getAppFormPdfTemplates = function getAppFormPdfTemplates() {
      logger.log('INFO: _genApplicationPDF - (1) getAppFormPdfTemplates', appId);
      var appFormTemplate = application.applicationForm.values.appFormTemplate;

      var startGen = function startGen() {
        dao.getDocFromCacheFirst(appFormTemplate.main, function (main) {
          if (main && !main.error) {
            var tplFiles = [];
            commonApp.getReportTemplates(tplFiles, appFormTemplate.template, 0, function () {
              if (tplFiles.length) {
                generateAppFormPdf([main, tplFiles]);
              } else {
                logger.error('ERROR: _genApplicationPDF - (1) getAppFormPdfTemplates [RETURN=-2]');
                callback({ success: false });
              }
            });
          } else {
            logger.error('ERROR: _genApplicationPDF - (1) getAppFormPdfTemplates [RETURN=-1');
            callback({ success: false });
          }
        });
      };

      if (!appFormTemplate) {
        dao.getDoc(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (mapping) {
          if (mapping && !mapping.error && mapping[application.quotation.baseProductCode]) {
            appFormTemplate = mapping[application.quotation.baseProductCode].appFormTemplate;
            startGen();
          }
        });
      } else {
        startGen();
      }
    };

    getAppFormPdfTemplates();
  });
};

var _saveApplication = function _saveApplication(app, callback) {
  var now = new Date().getTime();
  app.lastUpdateDate = now;
  dao.updDoc(app._id || app.id, app, callback);
};

var _saveAppForm = function _saveAppForm(data, session, callback) {
  var appId = data.applicationId;
  var formValues = data.values;
  var clientId = data.clientId;
  var application = null;

  logger.log('INFO: saveForm - start', appId);

  bDao.getCurrentBundle(clientId).then(function (bundle) {
    var bId = _.get(bundle, 'id');
    logger.log('INFO: saveForm - (1) getBundle', bId, 'for', appId);

    var policyNumber = data.policyNumber;
    var updIds = [];

    async.waterfall([function (cb) {
      appDao.getApplication(appId, function (app) {
        application = app;

        if (app && !app.error) {
          var orgFormValues = app.applicationForm.values;
          var diffs = CommonUtils.difference(orgFormValues, data.values);

          if (Object.keys(diffs).length > 0) {
            logger.log('INFO: saveForm - (2) getApplication - update diffs to bundle', bId, 'for', appId);
            var proposer = formValues.proposer;
            var proposers = [];
            if (proposer instanceof Object) {
              proposers.push(proposer);
            } else if (proposer instanceof Array) {
              proposers = proposer;
            }

            var cids = [];
            for (var j = 0; j < proposers.length; j++) {
              cids.push(proposers[j]['personalInfo']['cid']);
              commonApp.updateClientFormValues(bundle, proposers[j]);
            }

            var insured = formValues.insured;
            for (var _j = 0; _j < insured.length; _j++) {
              var _insured = insured[_j];
              if (cids.length === 0 || cids.indexOf(_insured.personalInfo.cid) === -1) {
                commonApp.updateClientFormValues(bundle, _insured);
              }
            }

            logger.log('INFO: BundleUpdate - [cid:' + clientId + '; bundleId:' + _.get(bundle, 'id') + '; fn:_saveAppForm]');
            bDao.updateBundle(bundle, function (upRes) {
              if (upRes && !upRes.error) {
                cb(null);
              } else {
                cb('Fail to update bundle:' + _.get(upRes, 'error'));
              }
            });
          } else {
            cb(null);
          }
        } else {
          cb('Fail to get application:' + _.get(app, 'error'));
        }
      });
    }, function (cb) {
      logger.log('INFO: saveForm - (3) saveApp', appId);

      // may need to get policy number
      var toGetPolicyNumber = false;
      var formCompleted = false;
      var resetAppStep = false;
      if (data.extra) {
        toGetPolicyNumber = data.extra.toGetPolicyNumber;
        formCompleted = data.extra.formCompleted;
        resetAppStep = data.extra.resetAppStep;
      }

      application.applicationForm.values = formValues;
      if (formCompleted && application.appStep <= 1) {
        application.appStep = 1;
      }
      if (resetAppStep) {
        application.appStep = 0;
      }
      logger.log('INFO: saveForm - (3) saveApp', appId, 'appStep=', application.appStep);
      //add policy number
      if (!toGetPolicyNumber) {
        cb(null, application);
      } else {
        if (session.platform) {
          application.policyNumber = policyNumber;
          cb(null, application);
        } else {
          commonApp.getPolicyNumberFromApi(1, false, function (pNumbers) {
            if (pNumbers && pNumbers.length) {
              application.policyNumber = pNumbers[0];
              policyNumber = pNumbers[0];
            }
            cb(null, application);
          });
        }
      }
    }, function (application, cb) {
      if (session.agent.channel.type !== 'FA') {
        bDao.updateApplicationFnaAnsToApplications(application).then(function (results) {
          cb(null, application);
        }).catch(function (error) {
          cb(error);
        });
      } else {
        cb(null, application);
      }
    }, function (application, cb) {
      _saveApplication(application, function (saveRes) {
        if (saveRes && !saveRes.error) {
          application._rev = saveRes.rev;
          cb(null, application);
        } else {
          cb('Fail to save application:', _.get(saveRes, 'error'));
        }
      });
    }, function (application, cb) {
      var _populateClientInfo = function _populateClientInfo(personalInfo, curPersonalInfo, cbk) {
        // logger.log('INFO: saveForm - (4) populateClientInfo', appId);

        var toUpdate = false;
        for (var cfIndex = 0; cfIndex < commonApp.CLIENT_FIELDS_TO_UPDATE.length; cfIndex++) {
          if (personalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]] !== curPersonalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]]) {

            toUpdate = true;
            curPersonalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]] = personalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]];
          }
        }

        logger.log('INFO: saveForm - (4) populateClientInfo', appId, _.get(curPersonalInfo, '_id'), 'update=', toUpdate);
        if (toUpdate) {
          updIds.push(curPersonalInfo._id);
          dao.updDoc(curPersonalInfo._id, curPersonalInfo, cbk);
        } else {
          cbk();
        }
      };

      var _populateLaProfile = function _populateLaProfile(index) {
        logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'index=', index);
        if (!Number(index)) {
          index = 0;
        }
        var laPersonalInfo = formValues.insured;
        var phPersonalInfo = formValues.proposer.personalInfo;
        if (laPersonalInfo && laPersonalInfo.length > 0) {
          logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'index=', index, 'numOfLa=', laPersonalInfo.length);
          var info = laPersonalInfo[index].personalInfo;
          if (info.cid !== phPersonalInfo.cid) {
            dao.getDoc(info.cid, function (curPersonalInfo) {
              if (index + 1 === laPersonalInfo.length) {
                logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'ready to genPdf (with LA)');
                // bDao.updateStatus(clientId, bDao.BUNDLE_STATUS.START_GEN_PDF, ()=>{
                bDao.updateStatus(clientId, bDao.BUNDLE_STATUS.START_GEN_PDF).then(function () {
                  _populateClientInfo(info, curPersonalInfo, function () {
                    cb(null, application);
                  });
                }).catch(function (error) {
                  cb('Fail to populateLaProfile' + error);
                });
              } else {
                _populateClientInfo(info, curPersonalInfo, function () {
                  _populateLaProfile(index + 1);
                });
              }
            });
          }
        } else {
          logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'ready to genPdf (no LA)');
          cb(null, application);
        }
      };

      var personalInfo = formValues.proposer.personalInfo;
      dao.getDoc(personalInfo.cid, function (curPersonalInfo) {
        _populateClientInfo(personalInfo, curPersonalInfo, _populateLaProfile);
      });
    }, function (application, cb) {
      logger.log('INFO: saveForm - (5) genPDF', appId, 'genPDF', data.genPDF);
      if (data.genPDF) {
        _genApplicationPDF(application, session, function (resp) {
          cb(null, resp);
        });
      } else {
        cb(null, { success: true });
      }
    }, function (resp, cb) {
      var success = true;
      if (resp && resp.error) {
        success = false;
      }
      var result = {
        success: success,
        updIds: updIds
      };
      if (policyNumber) {
        result.policyNumber = policyNumber;
        //update view
        dao.updateViewIndex('main', 'summaryApps');
      }
      cb(null, result);
    }], function (err, result) {
      if (err) {
        logger.error('ERROR: saveForm - end', appId, err);
        callback({ success: false });
      } else {
        logger.log('INFO: saveForm - end', appId, result.success);
        callback(result);
      }
    });
  }).catch(function (error) {
    logger.error('ERROR: saveForm - unexpected error', error);
  });
};

module.exports.getAppFormTemplate = _getAppFormTemplate;
module.exports.checkCrossAge = _checkCrossAge;
module.exports.saveAppForm = _saveAppForm;
module.exports.genAppFormPdfData = _genAppFormPdfData;
module.exports.genApplicationPDF = _genApplicationPDF;

module.exports.applicationSubmit = function (data, session, callback) {
  if (data.isShield) {
    var shieldApplication = [];
    data.application.forEach(function (value) {
      shieldApplication.push(new Promise(function (resolve, reject) {
        appDao.upsertApplication(value.id, value, function (resp) {
          if (resp) {
            resolve(resp);
          } else {
            reject("applicationSubmit:upsertApplication failed, application id: " + value.id);
          }
        });
      }));
    });

    // create policy document
    data.policyDocument.forEach(function (value) {
      shieldApplication.push(new Promise(function (resolve, reject) {
        appDao.upsertApplication(value.approvalCaseId, value, function (resp) {
          if (resp) {
            resolve(resp);
          } else {
            reject("applicationSubmit:upsertApplication failed, policy document approvalCaseId: " + value.approvalCaseId);
          }
        });
      }));
    });

    data.appPdf.forEach(function (value) {
      shieldApplication.push(new Promise(function (resolve, reject) {
        dao.uploadAttachmentByBase64(value.docId, value.attchId, value.rev, value.data, 'application/pdf', function (res) {
          resolve(res);
        });
      }));
    });

    Promise.all([shieldApplication]).then(function (resp) {
      bundleCommon.updateBundle(data.bundle, function (bundleResp) {
        dao.updateViewIndex("main", "applicationsByAgent");
        dao.updateViewIndex("main", "approvalDetails");
        callback({ success: true, application: resp, bundle: bundleResp });
      });
    }).catch(function (error) {
      callback({ success: false, error: error });
    });
  } else {
    appDao.upsertApplication(data.application.id, data.application, function (resp) {
      if (resp) {
        appDao.upsertApplication(data.application.policyNumber, data.policyDocument, function (resp) {
          if (resp) {
            dao.uploadAttachmentByBase64(data.application._id, "appPdf", data.application._rev, data.appPdf, 'application/pdf', function (res) {
              bundleCommon.updateBundle(data.bundle, function (bundleResp) {
                console.log('applicationSubmit - Bundle has updated');
                dao.updateViewIndex("main", "applicationsByAgent");
                dao.updateViewIndex("main", "approvalDetails");
                callback({ success: true, application: resp, bundle: bundleResp });
              });
            });
          } else {
            console.log('applicationSubmit - policy document failed to created in ' + data.application.id);
            callback({ success: false });
          }
        });
      } else {
        console.log('applicationSubmit - application  failed to created in ' + data.application.id);
        callback({ success: false });
      }
    });
  }
};

module.exports.genFNA = function (data, session, callback) {
  //get client id
  var cid = data.cid;
  logger.log('INFO: genFNA - start', cid);
  // call the function to get FNA report string and update bundle
  cDao.getProfileById(cid, function (profile) {
    logger.log('INFO: genFNA - (4) generateFNAReport', cid);
    if (profile && !profile.error) {
      needsHandler.generateFNAReport({ cid: profile.cid }, session, function (pdfResult) {
        if (!pdfResult.fnaReport) {
          logger.error('ERROR: genFNA - (4) generateFNAReport [RETURN=-4]', cid);
          callback({ success: false });
        } else {
          bDao.getCurrentBundle(profile.cid).then(function (bundle) {
            dao.uploadAttachmentByBase64(bundle.id, commonApp.PDF_NAME.FNA, bundle._rev, pdfResult.fnaReport, 'application/pdf', function (res) {
              bDao.updateStatus(profile.cid, bDao.BUNDLE_STATUS.START_GEN_PDF).then(function () {
                logger.log('INFO: genFNA - end [RETURN=2]', cid);
                callback({ success: true });
              }).catch(function (error) {
                logger.error('ERROR: genFNA - end [RETURN=2] fails:', error);
                callback({ success: false });
              });
            });
          });
        }
      });
    } else {
      logger.error('ERROR: genFNA - (4) generateFNAReport [RETURN=-3]', cid);
      callback({ success: false });
    }
  });
};