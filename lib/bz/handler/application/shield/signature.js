'use strict';

var _ = require('lodash');
var moment = require('moment');
var logger = global.logger || console;

var tokenUtils = require('../../../utils/TokenUtils');
var commonApp = require('../common');
var dao = require('../../../cbDaoFactory').create();
var fDao = require('../../../cbDao/file');
var cDao = require('../../../cbDao/client');
var bDao = require('../../../cbDao/bundle');
var quotDao = require('../../../cbDao/quotation');
var appDao = require('../../../cbDao/application');
var _application = require('./application');

var APP_FORM_SIGN_BOX_WIDTH = {
  // TWO_BOXES: 0.188,
  // THREE_BOXES: 0.118
  TWO_BOXES: 0.147,
  THREE_BOXES: 0.092
};

var APP_FORM_SIGNDOC_SIGN_FIELD = {
  searchTag: '',
  // width: 0.188,
  // height: 0.039
  width: 0.147,
  height: 0.030
};

var FNA_SIGNDOC_SIGN_FIELD = {
  searchTag: '',
  // width: 0.139,
  // height: 0.0300
  width: 0.103,
  height: 0.026
};

var PROPOSAL_SIGNDOC_SIGN_FIELD = {
  searchTag: '',
  // width: 0.124,
  // height: 0.026,
  width: 0.108,
  height: 0.026,
  offsety: 0.006
};

var _getAppFormAttachmentName = function _getAppFormAttachmentName(iCid) {
  return commonApp.PDF_NAME.eAPP + iCid;
};

var _prepareFnaReportPdf = function _prepareFnaReportPdf(session, app, now, bundle) {
  // const SIGNDOC_SIGN_FIELD = {
  //   searchTag: '',
  //   width: 0.139,
  //   height: 0.0300
  // };

  var SIGNDOC_TEXT_FIELD = {
    searchTag: '',
    size: 12,
    width: 0.127,
    height: 0.0055
  };

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    textFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareFnaReportPdf', app._id);

  result.agentSignFields.push([Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
    name: app.quotation.agent.name,
    searchTag: 'FNAREPORT_AGENT_SIGNATURE'
    // height: 0.035
  })]);
  result.textFields.push([Object.assign({}, SIGNDOC_TEXT_FIELD, {
    searchTag: 'FNAREPORT_AGENT_NAME',
    value: 'agent'
  })]);
  var clientSignFields = [Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
    name: app.applicationForm.values.proposer.personalInfo.fullName,
    searchTag: 'FNAREPORT_CLIENT_SIGNATURE'
  })];

  result.tokens.push(tokenUtils.createPdfToken(bundle._id, commonApp.PDF_NAME.FNA, now, session.loginToken));

  return new Promise(function (resolve, reject) {
    result.docid = app._id + '_' + commonApp.PDF_NAME.FNA;
    result.attUrl = global.config.signdoc.getPdf + '/' + result.tokens[0].token;
    result.isSigned = bundle.isFnaReportSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + commonApp.PDF_NAME.FNA, now);
    result.docts = now;

    //Check if Spouse exists
    cDao.getClientById(app.pCid, function (pClient) {
      if (pClient && !pClient.error) {
        var dependant = pClient.dependants.find(function (d) {
          return d.relationship === 'SPO';
        });
        // add SPOUSE name if dependant has relationship SPO
        if (dependant) {
          cDao.getClientById(dependant.cid, function (spouse) {
            if (spouse && !spouse.error) {
              clientSignFields.push(Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
                name: spouse.fullName,
                searchTag: 'FNAREPORT_SPOUSE_SIGNATURE'
              }));
              result.clientSignFields.push(clientSignFields);
              resolve(result);
            } else {
              reject(new Error('Fail to get spouse info in pCid=' + app.pCid + ' dependant.cid=' + dependant.cid + ' ' + _.get(spouse, 'error')));
            }
          });
        } else {
          result.clientSignFields.push(clientSignFields);
          resolve(result);
        }
      } else {
        reject(new Error('Fail to get client info in pCid=' + app.pCid + ' ' + _.get(pClient, 'error')));
      }
    });
  });
};

var _prepareProposalPdf = function _prepareProposalPdf(session, app, now, bundle) {
  // const SIGNDOC_SIGN_FIELD = {
  //   searchTag: '',
  //   width: 0.124,
  //   height: 0.026,
  //   offsety: 0.006
  // };
  var isFaChannel = session.agent.channel.type === 'FA';

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareProposalPdf', app._id);

  return new Promise(function (resolve, reject) {
    fDao.getAttachment(app.quotationDocId, commonApp.PDF_NAME.BI, function (attProposal) {
      if (attProposal.success) {
        resolve(attProposal);
      } else {
        reject(new Error('Fail to get attachment ' + app.quotationDocId + '/' + commonApp.PDF_NAME.BI + ' ' + _.get(attProposal, 'error')));
      }
    });
  }).then(function (attProposal) {
    result.agentSignFields.push([Object.assign({}, PROPOSAL_SIGNDOC_SIGN_FIELD, {
      name: app.quotation.agent.name,
      searchTag: 'Signature of Financial Consultant'
    })]);
    result.clientSignFields.push([Object.assign({}, PROPOSAL_SIGNDOC_SIGN_FIELD, {
      name: app.applicationForm.values.proposer.personalInfo.fullName,
      searchTag: 'Signature of Proposer'
    })]);

    var isGenToken = app.isProposalSigned || bundle.isFnaReportSigned || isFaChannel;

    if (isGenToken) {
      result.tokens.push(tokenUtils.createPdfToken(app.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken));
    }

    result.docid = app._id + '_' + commonApp.PDF_NAME.BI;
    result.pdfStr = !app.isProposalSigned ? attProposal.data : '';
    result.attUrl = isGenToken ? global.config.signdoc.getPdf + '/' + result.tokens[0].token : '';
    result.isSigned = app.isProposalSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + commonApp.PDF_NAME.BI, now);
    result.docts = now;

    return result;
  });
};

var _prepareAppFormProposerPdf = function _prepareAppFormProposerPdf(session, app, now, bundle) {

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareAppFormProposerPdf', app._id);
  // let isPhSameAsLa = app.applicationForm.values.proposer.extra.isPhSameAsLa === 'Y';
  var hasTrustedIndividual = app.applicationForm.values.proposer.extra.hasTrustedIndividual === 'Y';
  var signatureWidth = 0;
  //no signature for LA age < 18

  //Signature box width
  var numOfSignature = 2; //agent & proposor
  if (hasTrustedIndividual) {
    numOfSignature += 1;
  }
  switch (numOfSignature) {
    case 4:
      signatureWidth = 0.084;
      break;
    case 3:
      // signatureWidth = 0.118;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.THREE_BOXES;
      break;
    default:
      // signatureWidth = 0.188;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.TWO_BOXES;
  }

  return new Promise(function (resolve, reject) {
    fDao.getAttachment(app._id, commonApp.PDF_NAME.eAPP, function (attAppForm) {
      if (attAppForm.success) {
        resolve(attAppForm);
      } else {
        reject(new Error('Fail to get attachment ' + app._id + '/' + commonApp.PDF_NAME.eAPP + ' ' + _.get(attAppForm, 'error')));
      }
    });
  }).then(function (attAppForm) {
    //prepare result
    result.agentSignFields.push([Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: app.quotation.agent.name,
      searchTag: 'ADVISOR_SIGNATURE',
      width: signatureWidth
    })]);
    var clientSignFields = [Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: app.applicationForm.values.proposer.personalInfo.fullName,
      searchTag: 'PROP_SIGNATURE',
      width: signatureWidth
    })];

    if (hasTrustedIndividual) {
      clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: _.get(app, 'applicationForm.values.proposer.declaration.trustedIndividuals.fullName'),
        searchTag: 'TI_SIGNATURE',
        width: signatureWidth
      }));
    }

    result.clientSignFields.push(clientSignFields);

    var isGenToken = app.isAppFormProposerSigned || app.isProposalSigned;
    if (isGenToken) {
      result.tokens.push(tokenUtils.createPdfToken(app._id, commonApp.PDF_NAME.eAPP, now, session.loginToken));
    }

    result.docid = app._id + '_' + commonApp.PDF_NAME.eAPP;
    result.pdfStr = !app.isAppFormProposerSigned ? attAppForm.data : '';
    result.attUrl = isGenToken ? global.config.signdoc.getPdf + '/' + result.tokens[0].token : '';
    result.isSigned = app.isAppFormProposerSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + commonApp.PDF_NAME.eAPP, now);
    result.docts = now;

    return result;
  });
};

var _prepareAppFormInsuredPdf = function _prepareAppFormInsuredPdf(session, app, now, bundle, iCid) {

  var attName = _getAppFormAttachmentName(iCid);

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareAppFormInsuredPdf', app._id, '(' + iCid + ')');
  // let isPhSameAsLa = app.applicationForm.values.proposer.extra.isPhSameAsLa === 'Y';
  var iCids = _.get(app, 'iCids', []);
  var iCidIndex = iCids.indexOf(iCid);
  var isAppFormProposerSigned = _.get(app, 'isAppFormProposerSigned');
  var isAppFormInsuredSigned = _.get(app, 'isAppFormInsuredSigned[' + iCidIndex + ']');
  var isAppFormInsuredCompleted = _.get(app, 'applicationForm.values.insured[' + iCidIndex + '].extra.isCompleted', false);

  // let isSignedByLegalGuardian = app.applicationForm.values.insured.length > 0 && commonApp.checkIsLaSignedByProposer(app, iCidIndex);
  var hasTrustedIndividual = app.applicationForm.values.proposer.extra.hasTrustedIndividual === 'Y';
  var signatureWidth = 0;
  //no signature for LA age < 18

  //Signature box width
  var numOfSignature = 2; //agent & proposor
  if (hasTrustedIndividual) {
    numOfSignature += 1;
  }
  switch (numOfSignature) {
    case 4:
      signatureWidth = 0.084;
      break;
    case 3:
      // signatureWidth = 0.118;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.THREE_BOXES;
      break;
    default:
      // signatureWidth = 0.188;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.TWO_BOXES;
  }

  return new Promise(function (resolve, reject) {
    logger.log('DEBUG: _prepareAppFormInsuredPdf', app._id, '(' + iCid + ')', iCidIndex, isAppFormInsuredCompleted);
    if (iCidIndex < 0 || isAppFormProposerSigned || !isAppFormInsuredCompleted) {
      resolve({});
    } else {
      fDao.getAttachment(app._id, attName, function (attAppForm) {
        if (attAppForm.success) {
          resolve(attAppForm);
        } else {
          reject(new Error('Fail to get attachment ' + app._id + '/' + attName + ' ' + _.get(attAppForm, 'error')));
        }
      });
    }
  }).then(function (attAppForm) {
    //prepare result
    result.agentSignFields.push([Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: app.quotation.agent.name,
      searchTag: 'ADVISOR_SIGNATURE',
      width: signatureWidth
    })]);
    var clientSignFields = [Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: _.get(app, 'applicationForm.values.proposer.personalInfo.fullName', ''),
      searchTag: 'PROP_SIGNATURE',
      width: signatureWidth
    })];

    if (hasTrustedIndividual) {
      clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: _.get(app, 'applicationForm.values.proposer.declaration.trustedIndividuals.fullName'),
        searchTag: 'TI_SIGNATURE',
        width: signatureWidth
      }));
    }

    clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: _.get(app, 'applicationForm.values.insured[' + iCidIndex + '].personalInfo.fullName', ''),
      searchTag: 'LA_SIGNATURE',
      width: signatureWidth
    }));

    result.clientSignFields.push(clientSignFields);

    var isGenToken = isAppFormProposerSigned && isAppFormInsuredCompleted;
    if (isGenToken) {
      result.tokens.push(tokenUtils.createPdfToken(app._id, attName, now, session.loginToken));
    }

    result.docid = app._id + '_' + attName;
    result.pdfStr = !isAppFormProposerSigned ? _.get(attAppForm, 'data', '') : '';
    result.attUrl = isGenToken ? global.config.signdoc.getPdf + '/' + result.tokens[0].token : '';
    result.isSigned = isAppFormInsuredSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + attName, now);
    result.docts = now;

    return result;
  });
};

//=ApplicationHandler.getSignatureStatusFromCb
var _getSignatureInitUrl = function _getSignatureInitUrl(session, appId) {
  var isFaChannel = session.agent.channel.type === 'FA';
  var now = moment().valueOf();

  var result = {
    success: false,
    signature: {
      isFaChannel: isFaChannel,
      signingTabIdx: 0,
      isSigningProcess: [],
      numOfTabs: 0,
      pdfStr: [],
      attUrls: [],
      isSigned: [],
      agentSignFields: [],
      clientSignFields: [],
      signDocConfig: {
        auths: [],
        docts: [],
        ids: [],
        postUrl: global.config.signdoc.postUrl,
        resultUrl: global.config.signdoc.resultUrl,
        dmsId: global.config.signdoc.dmsid
      }
    },
    warningMsg: {
      msgCode: 0
    },
    crossAge: {}
  };

  logger.log('INFO: _getSignatureInitUrl', appId);

  return _application.getApplication(appId).then(function (app) {
    return bDao.getCurrentBundle(app.pCid).then(function (bundle) {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function (cache) {
    var application = cache.application,
        bundle = cache.bundle;

    var preparePromise = [];
    var iCids = _.get(application, 'iCids');

    return _application.checkCrossAge(application).then(function (caRes) {
      if (caRes.success) {
        result.crossAge = caRes;
      }

      if (caRes.success && caRes.status !== commonApp.CROSSAGE_STATUS.NO_CROSSAGE) {
        result.warningMsg.msgCode = caRes.status;
      }

      if (_.get(caRes, 'crossedAgeCid.length', 0) > 0) {
        return caRes.crossedAgeCid;
      }

      return [];
    }).then(function (crossedAgeCid) {
      if (crossedAgeCid.length === 0) {
        return;
      }

      var lastUpdateDate = moment().toISOString();
      return new Promise(function (resolve, reject) {
        application.isCrossAge = true;
        application.lastUpdateDate = lastUpdateDate;
        appDao.upsertApplication(application.id, application, function (resp) {
          if (resp && !resp.error) {
            application._rev = resp.rev;
            resolve();
          } else {
            reject(new Error('Fail to update Application ' + result.application.id + ' ' + _.get(resp, 'error')));
          }
        });
      }).then(function () {
        var childIds = [];
        _.forEach(crossedAgeCid, function (iCid) {
          var mappingList = application.iCidMapping[iCid];
          _.forEach(mappingList, function (mapping) {
            childIds.push(mapping.applicationId);
          });
        });

        return _application.getChildApplications(childIds).then(function (childApps) {
          return Promise.all(childApps.map(function (childApp) {
            childApp.isCrossAge = true;
            childApp.lastUpdateDate = lastUpdateDate;

            return new Promise(function (resolve, reject) {
              appDao.upsertApplication(childApp._id, childApp, function (resp) {
                if (resp && !resp.error) {
                  resolve();
                } else {
                  reject(new Error('Fail to update Application ' + childApp._id + ' ' + _.get(resp, 'error')));
                }
              });
            });
          }));
        });
      });
    }).then(function () {
      if (!isFaChannel) {
        preparePromise.push(_prepareFnaReportPdf(session, application, now, bundle));
      }
      preparePromise.push(_prepareProposalPdf(session, application, now, bundle));
      preparePromise.push(_prepareAppFormProposerPdf(session, application, now, bundle));
      _.forEach(iCids, function (iCid) {
        preparePromise.push(_prepareAppFormInsuredPdf(session, application, now, bundle, iCid));
      });

      return Promise.all(preparePromise).then(function (results) {
        var stopUpdate = false;
        var numOfMandTabs = results.length - iCids.length;
        var tokens = [];

        _.forEach(results, function (res, index) {
          var isSigning = false;
          var iCidIndex = index - numOfMandTabs;
          var isCompleted = _.get(application, 'applicationForm.values.insured[' + iCidIndex + '].extra.isCompleted', false);

          if (index < numOfMandTabs || application.isAppFormProposerSigned && isCompleted) {
            isSigning = res.attUrl.length > 0 && !res.isSigned;
          }

          if (isSigning && !stopUpdate) {
            result.signature.signingTabIdx = index;
            stopUpdate = true;
          }
          result.signature.isSigningProcess.push(isSigning);
          result.signature.numOfTabs++;
          result.signature.pdfStr.push(res.pdfStr);
          result.signature.attUrls.push(res.attUrl);
          result.signature.isSigned.push(res.isSigned);
          result.signature.agentSignFields = result.signature.agentSignFields.concat(res.agentSignFields);
          result.signature.clientSignFields = result.signature.clientSignFields.concat(res.clientSignFields);
          result.signature.signDocConfig.auths.push(res.auth);
          result.signature.signDocConfig.docts.push(res.docts);
          result.signature.signDocConfig.ids.push(res.docid);

          tokens = tokens.concat(res.tokens);
        });

        return new Promise(function (resolve) {
          tokenUtils.setPdfTokensToRedis(tokens, function () {
            result.success = true;
            resolve(result);
          });
        });
      });
    });
  });
};

//=ApplicationHandler.getSignedPdfFromSignDocPost
var _saveSignedPdfFromSignDoc = function _saveSignedPdfFromSignDoc(appId, attId, pdfData, callback) {
  var cache = {
    application: {},
    bundle: {}
  };

  logger.log('INFO: saveSignedPdfFromSignDoc - start', appId);

  _application.getApplication(appId).then(function (app) {
    return bDao.getCurrentBundle(app.pCid).then(function (bundle) {
      if (bundle && !bundle.error) {
        cache.application = app;
        cache.bundle = bundle;
        return bundle;
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function (bundle) {
    if (attId === commonApp.PDF_NAME.FNA) {
      //handle fnaReport
      return new Promise(function (resolve, reject) {
        dao.uploadAttachmentByBase64(bundle._id, attId, bundle._rev, pdfData, 'application/pdf', function (res) {
          if (res && !res.error) {
            cache.bundle._rev = res.rev;
            resolve(bundle);
          } else {
            reject(new Error('Fail to update Fna Report Pdf to bundle ' + _.get(res, 'error')));
          }
        });
      }).then(function () {
        return bDao.updateStatus(cache.application.pCid, bDao.BUNDLE_STATUS.SIGN_FNA);
      }).then(function (newBundle) {
        cache.bundle = newBundle;
        return new Promise(function (resolve, reject) {
          cDao.setIsProfileCanDel(cache.application.pCid, function (res) {
            if (res.profile && !res.profile.error) {
              cache.profile = res.profile;
              resolve(res.profile);
            } else {
              reject(new Error('Fail to update profile ' + _.get(res, 'error')));
            }
          });
        });
      }).then(function (profile) {
        return new Promise(function (resolve, reject) {
          var today = new Date();
          cache.bundle.profile = profile;
          cache.bundle.isFnaReportSigned = true;
          cache.bundle.fnaSignDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
          logger.log('INFO: BundleUpdate - [cid:' + _.get(cache.application, 'pCid') + '; bundleId:' + _.get(cache.bundle, 'id') + '; fn:saveSignedPdfFromSignDoc]');
          bDao.updateBundle(cache.bundle, function (res) {
            if (res && !res.error) {
              resolve(cache.application);
            } else {
              reject(new Error('Fail to update bundle ' + _.get(res, 'error')));
            }
          });
        });
      });
    } else if (attId === commonApp.PDF_NAME.BI) {
      //handle proposal
      return new Promise(function (resolve, reject) {
        quotDao.getQuotation(cache.application.quotationDocId, function (quot) {
          if (quot && !quot.error) {
            resolve(quot);
          } else {
            reject(new Error('Fail to get quotation ' + _.get(quot, 'error')));
          }
        });
      }).then(function (quot) {
        return new Promise(function (resolve, reject) {
          quotDao.uploadSignedProposal(cache.application.quotationDocId, pdfData).then(function (res) {
            if (res && !res.error) {
              resolve();
            } else {
              reject(new Error('Fail to update proposal pdf to quotation ' + _.get(res, 'error')));
            }
          });
        }).then(function () {
          return bDao.updateStatus(cache.application.pCid, bDao.BUNDLE_STATUS.SIGN_FNA).then(function (newBundle) {
            return cache.application;
          });
        });
      });
    } else {
      //handle appForm
      return new Promise(function (resolve, reject) {
        dao.uploadAttachmentByBase64(cache.application._id, attId, cache.application._rev, pdfData, 'application/pdf', function (res) {
          if (res && !res.error) {
            cache.application._rev = res.rev;
            resolve(cache.application);
          } else {
            reject(new Error('Fail to update App form Proposer Pdf to application ' + _.get(res, 'error')));
          }
        });
      }).then(function (app) {
        if (attId === commonApp.PDF_NAME.eAPP) {
          //update bundle status to Fully Signed
          logger.log('INFO: saveSignedPdfFromSignDoc - update bundle status to fully signed', appId);
          return bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.FULL_SIGN).then(function (newBundle) {
            _.forEach(newBundle.applications, function (appItem) {
              if (appItem.applicationDocId === app._id) {
                appItem.isFullySigned = true;
                return false;
              }
            });

            return new Promise(function (resolve, reject) {
              logger.log('INFO: BundleUpdate - [cid:' + app.pCid + '; bundleId:' + _.get(newBundle, 'id') + '; fn:saveSignedPdfFromSignDoc]');
              bDao.updateBundle(newBundle, function (upRes) {
                if (upRes && !upRes.error) {
                  resolve(app);
                } else {
                  reject(new Error('Fail to update bundle to fully signed status'));
                }
              });
            });
          }).catch(function (error) {
            logger.error('ERROR: saveSignedPdfFromSignDoc - Fail to update bundle status', appId, error);
            throw error;
          });
        } else {
          return app;
        }
      });
    }
  }).then(function (app) {
    var now = moment().toISOString();
    var childAppIds = [];

    var upPromise = [];
    if (attId === commonApp.PDF_NAME.FNA) {
      app.isStartSignature = true;
    } else if (attId === commonApp.PDF_NAME.BI) {
      // in FA channel, there is no FNA, that's why proposal also need to set isStartSignature
      app.isStartSignature = true;
      app.isProposalSigned = true;
      app.biSignedDate = now;
      childAppIds = _.get(app, 'childIds');
    } else if (attId === commonApp.PDF_NAME.eAPP) {
      app.isFullySigned = true;
      app.isAppFormProposerSigned = true;
      if (_.get(app, 'applicationForm.values.proposer.extra.isPhSameAsLa') === 'Y') {
        childAppIds = app.iCidMapping[app.pCid].map(function (mapping) {
          return mapping.applicationId;
        });
      }
    } else if (attId.startsWith(commonApp.PDF_NAME.eAPP) && attId.length > commonApp.PDF_NAME.eAPP.length) {
      var cid = _.replace(attId, commonApp.PDF_NAME.eAPP, '');
      childAppIds = app.iCidMapping[cid].map(function (mapping) {
        return mapping.applicationId;
      });

      var upIndex = app.iCids.indexOf(cid);
      if (upIndex >= 0) {
        app.isAppFormInsuredSigned[upIndex] = true;
      }
    }
    app.applicationSignedDate = now;
    app.lastUpdateDate = now;

    _application.updateApplicationCompletedStep(app);

    upPromise.push(new Promise(function (resolve, reject) {
      appDao.upsertApplication(app._id, app, function (upRes) {
        if (upRes && !upRes.error) {
          app._rev = upRes.rev;
          resolve(app);
        } else {
          reject(new Error('Fail to update application ' + _.get(upRes, 'error')));
        }
      });
    }));

    _.forEach(childAppIds, function (cAppId) {
      upPromise.push(new Promise(function (resolve, reject) {
        _application.getApplication(cAppId).then(function (cApp) {
          _.union(cApp.parentAttachmentIds, [attId]);
          cApp.lastUpdateDate = now;
          if (attId === commonApp.PDF_NAME.BI) {
            cApp.biSignedDate = _.get(app, 'biSignedDate');
          } else if (attId === commonApp.PDF_NAME.eAPP || attId === _getAppFormAttachmentName(_.get(cApp, 'iCid'))) {
            cApp.applicationSignedDate = now;
          }
          cApp.isFullySigned = _.get(app, 'isFullySigned');
          appDao.upsertApplication(cAppId, cApp, function (upRes) {
            if (upRes && !upRes.error) {
              cApp._rev = upRes.rev;
              resolve(cApp);
            } else {
              reject(new Error('Fail to update child application ' + _.get(upRes, 'error')));
            }
          });
        });
      }));
    });

    return Promise.all(upPromise);
  }).then(function (results) {
    logger.log('INFO: saveSignedPdfFromSignDoc - end [RETURN=100]', appId);
    callback({ success: true, id: appId, application: results[0] });
  }).catch(function (error) {
    logger.error('ERROR: saveSignedPdfFromSignDoc - end [RETURN=-100]', appId);
    callback({ success: false, id: appId });
  });
};

//=ApplicationHandler.getUpdatedAttachmentUrl
module.exports.getSignatureUpdatedUrl = function (data, session, callback) {
  var isFaChannel = session.agent.channel.type === 'FA';
  var sdwebDocid = data.sdwebDocid,
      tabIdx = data.tabIdx;

  var ids = commonApp.getIdFromSignDocId(sdwebDocid);
  var docId = ids.docId,
      attId = ids.attId;

  var attUrl = global.config.signdoc.getPdf;
  var now = moment().valueOf();
  var nextTabIdx = tabIdx;
  var isSigned = [];
  var isSigningProcess = [];

  logger.log('INFO: getSignatureUpdatedUrl - start', docId);

  _application.getApplication(docId).then(function (app) {
    return bDao.getCurrentBundle(app.pCid).then(function (bundle) {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function (cache) {
    var pdfTokens = [];
    var tokenIndexMap = {};
    var application = cache.application,
        bundle = cache.bundle;


    var _setTokensArrayAndIndexMap = function _setTokensArrayAndIndexMap(pdfToken, tokens, tokenMap, tabIndex) {
      tokens.push(pdfToken);
      tokenMap[pdfToken.token] = tabIndex;
    };

    if (!isFaChannel) {
      isSigned.push(bundle.isFnaReportSigned);
    }
    isSigned.push(application.isProposalSigned);
    isSigned.push(application.isAppFormProposerSigned);
    isSigned = isSigned.concat(application.isAppFormInsuredSigned);

    isSigningProcess = _.times(isSigned.length, _.constant(false));

    if (attId === commonApp.PDF_NAME.FNA) {
      nextTabIdx = tabIdx + 1;
      isSigningProcess[1] = true;

      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application.bundleId, commonApp.PDF_NAME.FNA, now, session.loginToken), pdfTokens, tokenIndexMap, tabIdx);
      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken), pdfTokens, tokenIndexMap, nextTabIdx);
    } else if (attId === commonApp.PDF_NAME.BI) {
      nextTabIdx = tabIdx + 1;
      isSigningProcess[isFaChannel ? 1 : 2] = true;

      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken), pdfTokens, tokenIndexMap, tabIdx);
      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application._id, commonApp.PDF_NAME.eAPP, now, session.loginToken), pdfTokens, tokenIndexMap, nextTabIdx);
    } else if (attId.startsWith(commonApp.PDF_NAME.eAPP)) {
      nextTabIdx = tabIdx;

      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application._id, attId, now, session.loginToken), pdfTokens, tokenIndexMap, tabIdx);

      if (application.isAppFormInsuredSigned.length > 0 && application.isAppFormInsuredSigned.indexOf(false) >= 0) {
        var numOfMandTabs = isFaChannel ? 2 : 3;
        var skip = false;
        _.forEach(application.isAppFormInsuredSigned, function (isInsuredSigned, i) {
          var isCompleted = _.get(application, 'applicationForm.values.insured[' + i + '].extra.isCompleted', false);
          if (isCompleted && !isInsuredSigned) {
            var insuredTabIndex = numOfMandTabs + i;
            isSigningProcess[insuredTabIndex] = true;
            _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application._id, _getAppFormAttachmentName(application.iCids[i]), now, session.loginToken), pdfTokens, tokenIndexMap, insuredTabIndex);

            if (!skip) {
              nextTabIdx = insuredTabIndex;
              skip = true;
            }
          }
        });
      }
    }

    return new Promise(function (resolve) {
      tokenUtils.setPdfTokensToRedis(pdfTokens, function () {
        var newAttUrls = [];
        _.forEach(pdfTokens, function (pdfToken) {
          var token = pdfToken.token;

          newAttUrls.push({
            index: tokenIndexMap[token],
            attUrl: attUrl + '/' + token
          });
        });
        resolve(newAttUrls);
      });
    });
  }).then(function (newAttUrls) {
    callback({ success: true, newAttUrls: newAttUrls, nextTabIdx: nextTabIdx, isSigned: isSigned, isSigningProcess: isSigningProcess, isFaChannel: isFaChannel });
  }).catch(function (error) {
    logger.error('ERROR: getSignatureUpdatedUrl - end [RETURN=-100]', docId, error);
    // callback({success: false});
  });
};

// ====================================================== For Mobile below =======================================================

var getAttachmentPromise = function getAttachmentPromise(docId, attachmentId, tabIdx) {
  return new Promise(function (resolve, reject) {
    fDao.getAttachment(docId, attachmentId, function (pdfData) {
      if (pdfData) {
        resolve({
          index: tabIdx,
          pdfStr: pdfData.data
        });
      }
      reject(new Error('Reject - shield/signature.getAttachmentPromise(): fail to get ' + attachmentId + ' for ' + docId));
    });
  });
};
//=ApplicationHandler.getSignatureUpdatedPdfString
module.exports.getSignatureUpdatedPdfString = function (data, session, callback) {
  var isFaChannel = session.agent.channel.type === 'FA';
  var sdwebDocid = data.sdwebDocid,
      tabIdx = data.tabIdx;

  var ids = commonApp.getIdFromSignDocId(sdwebDocid);
  var docId = ids.docId,
      attId = ids.attId;

  var nextTabIdx = tabIdx;
  var isSigned = [];
  var isSigningProcess = [];

  logger.log('INFO: getSignatureUpdatedPdfString - start', docId);

  _application.getApplication(docId).then(function (app) {
    return bDao.getCurrentBundle(app.pCid).then(function (bundle) {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function (cache) {
    return new Promise(function (resolve) {
      var attPromise = [];
      var application = cache.application,
          bundle = cache.bundle;


      if (!isFaChannel) {
        isSigned.push(bundle.isFnaReportSigned);
      }
      isSigned.push(application.isProposalSigned);
      isSigned.push(application.isAppFormProposerSigned);
      isSigned = isSigned.concat(application.isAppFormInsuredSigned);

      isSigningProcess = _.times(isSigned.length, _.constant(false));

      if (attId === commonApp.PDF_NAME.FNA) {
        nextTabIdx = tabIdx + 1;
        isSigningProcess[1] = true;
        attPromise.push(getAttachmentPromise(application.bundleId, commonApp.PDF_NAME.FNA, tabIdx));
        attPromise.push(getAttachmentPromise(application.quotationDocId, commonApp.PDF_NAME.BI, nextTabIdx));
      } else if (attId === commonApp.PDF_NAME.BI) {
        nextTabIdx = tabIdx + 1;
        isSigningProcess[isFaChannel ? 1 : 2] = true;
        attPromise.push(getAttachmentPromise(application.quotationDocId, commonApp.PDF_NAME.BI, tabIdx));
        attPromise.push(getAttachmentPromise(application._id, commonApp.PDF_NAME.eAPP, nextTabIdx));
      } else if (attId.startsWith(commonApp.PDF_NAME.eAPP)) {
        nextTabIdx = tabIdx;
        attPromise.push(getAttachmentPromise(application._id, attId, tabIdx));
        if (application.isAppFormInsuredSigned.length > 0 && application.isAppFormInsuredSigned.indexOf(false) >= 0) {
          var numOfMandTabs = isFaChannel ? 2 : 3;
          var skip = false;
          _.forEach(application.isAppFormInsuredSigned, function (isInsuredSigned, index) {
            var isCompleted = _.get(application, 'applicationForm.values.insured[' + index + '].extra.isCompleted', false);
            if (isCompleted && !isInsuredSigned) {
              var insuredTabIndex = numOfMandTabs + index;
              isSigningProcess[insuredTabIndex] = true;
              attPromise.push(getAttachmentPromise(application._id, _getAppFormAttachmentName(application.iCids[index]), insuredTabIndex));

              if (!skip) {
                skip = true;
              }
            }
          });
        }
      }
      Promise.all(attPromise).then(function (result) {
        resolve(result);
      });
    });
  }).then(function (result) {
    callback({ success: true, newPdfStrings: result, nextTabIdx: nextTabIdx, isSigned: isSigned, isSigningProcess: isSigningProcess, isFaChannel: isFaChannel });
  }).catch(function (error) {
    logger.error('ERROR: getSignatureUpdatedPdfString - end [RETURN=-100]', docId, error);
    // callback({success: false});
  });
};

//=ApplicationHandler.getSignatureStatusFromCb
var _getSignatureInitPdfString = function _getSignatureInitPdfString(session, appId) {
  var isFaChannel = session.agent.channel.type === 'FA';
  var now = moment().valueOf();

  var result = {
    success: false,
    signature: {
      isFaChannel: isFaChannel,
      signingTabIdx: 0,
      isSigningProcess: [],
      numOfTabs: 0,
      pdfStr: [],
      attUrls: [],
      isSigned: [],
      agentSignFields: [],
      clientSignFields: [],
      signDocConfig: {
        auths: [],
        docts: [],
        ids: [],
        postUrl: global.config.signdoc.postUrl,
        resultUrl: global.config.signdoc.resultUrl,
        dmsId: global.config.signdoc.dmsid
      }
    },
    warningMsg: {
      msgCode: 0
    },
    crossAge: {}
  };

  logger.log('INFO: _getSignatureInitPdfString', appId);

  return _application.getApplication(appId).then(function (app) {
    return bDao.getCurrentBundle(app.pCid).then(function (bundle) {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function (cache) {
    var application = cache.application,
        bundle = cache.bundle;

    var preparePromise = [];
    var iCids = _.get(application, 'iCids');

    return _application.checkCrossAge(application).then(function (caRes) {
      if (caRes.success) {
        result.crossAge = caRes;
      }

      if (caRes.success && caRes.status !== commonApp.CROSSAGE_STATUS.NO_CROSSAGE) {
        result.warningMsg.msgCode = caRes.status;
      }

      if (_.get(caRes, 'crossedAgeCid.length', 0) > 0) {
        return caRes.crossedAgeCid;
      }

      return [];
    }).then(function (crossedAgeCid) {
      if (crossedAgeCid.length === 0) {
        return;
      }

      var lastUpdateDate = moment().toISOString();
      return new Promise(function (resolve, reject) {
        application.isCrossAge = true;
        application.lastUpdateDate = lastUpdateDate;
        appDao.upsertApplication(application.id, application, function (resp) {
          if (resp && !resp.error) {
            application._rev = resp.rev;
            resolve();
          } else {
            reject(new Error('Fail to update Application ' + result.application.id + ' ' + _.get(resp, 'error')));
          }
        });
      }).then(function () {
        var childIds = [];
        _.forEach(crossedAgeCid, function (iCid) {
          var mappingList = application.iCidMapping[iCid];
          _.forEach(mappingList, function (mapping) {
            childIds.push(mapping.applicationId);
          });
        });

        return _application.getChildApplications(childIds).then(function (childApps) {
          return Promise.all(childApps.map(function (childApp) {
            childApp.isCrossAge = true;
            childApp.lastUpdateDate = lastUpdateDate;

            return new Promise(function (resolve, reject) {
              appDao.upsertApplication(childApp._id, childApp, function (resp) {
                if (resp && !resp.error) {
                  resolve();
                } else {
                  reject(new Error('Fail to update Application ' + childApp._id + ' ' + _.get(resp, 'error')));
                }
              });
            });
          }));
        });
      });
    }).then(function () {
      if (!isFaChannel) {
        preparePromise.push(_prepareFnaReportPdfMobile(session, application, now, bundle));
      }
      preparePromise.push(_prepareProposalPdfMobile(session, application, now, bundle));
      preparePromise.push(_prepareAppFormProposerPdfMobile(session, application, now, bundle));
      _.forEach(iCids, function (iCid) {
        preparePromise.push(_prepareAppFormInsuredPdfMobile(session, application, now, bundle, iCid));
      });

      return Promise.all(preparePromise).then(function (results) {
        var stopUpdate = false;
        var numOfMandTabs = results.length - iCids.length;
        var tokens = [];

        _.forEach(results, function (res, index) {
          var isSigning = false;
          var iCidIndex = index - numOfMandTabs;
          var isCompleted = _.get(application, 'applicationForm.values.insured[' + iCidIndex + '].extra.isCompleted', false);

          if (index < numOfMandTabs || application.isAppFormProposerSigned && isCompleted) {
            isSigning = res.attUrl.length > 0 && !res.isSigned;
          }

          if (isSigning && !stopUpdate) {
            result.signature.signingTabIdx = index;
            stopUpdate = true;
          }
          result.signature.isSigningProcess.push(isSigning);
          result.signature.numOfTabs++;
          result.signature.pdfStr.push(res.pdfStr);
          result.signature.attUrls.push(res.attUrl);
          result.signature.isSigned.push(res.isSigned);
          result.signature.agentSignFields = result.signature.agentSignFields.concat(res.agentSignFields);
          result.signature.clientSignFields = result.signature.clientSignFields.concat(res.clientSignFields);
          result.signature.signDocConfig.auths.push(res.auth);
          result.signature.signDocConfig.docts.push(res.docts);
          result.signature.signDocConfig.ids.push(res.docid);

          tokens = tokens.concat(res.tokens);
        });
        return new Promise(function (resolve) {
          result.success = true;
          resolve(result);
        });
      });
    });
  });
};

var _prepareFnaReportPdfMobile = function _prepareFnaReportPdfMobile(session, app, now, bundle) {
  // const SIGNDOC_SIGN_FIELD = {
  //   searchTag: '',
  //   width: 0.139,
  //   height: 0.0300
  // };

  var SIGNDOC_TEXT_FIELD = {
    searchTag: '',
    size: 12,
    width: 0.127,
    height: 0.0055
  };

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    textFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareFnaReportPdfMobile', app._id);

  result.agentSignFields.push([Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
    name: app.quotation.agent.name,
    searchTag: 'FNAREPORT_AGENT_SIGNATURE'
    // height: 0.035
  })]);
  result.textFields.push([Object.assign({}, SIGNDOC_TEXT_FIELD, {
    searchTag: 'FNAREPORT_AGENT_NAME',
    value: 'agent'
  })]);
  var clientSignFields = [Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
    name: app.applicationForm.values.proposer.personalInfo.fullName,
    searchTag: 'FNAREPORT_CLIENT_SIGNATURE'
  })];

  return new Promise(function (resolve, reject) {
    result.docid = app._id + '_' + commonApp.PDF_NAME.FNA;
    result.isSigned = bundle.isFnaReportSigned;
    result.docts = now;

    //Check if Spouse exists
    cDao.getClientById(app.pCid, function (pClient) {
      if (pClient && !pClient.error) {
        var dependant = pClient.dependants.find(function (d) {
          return d.relationship === 'SPO';
        });
        fDao.getAttachment(app.bundleId, commonApp.PDF_NAME.FNA, function (attFnaReport) {
          if (attFnaReport) {
            result.pdfStr = attFnaReport.data;
            // add SPOUSE name if dependant has relationship SPO
            if (dependant) {
              cDao.getClientById(dependant.cid, function (spouse) {
                if (spouse && !spouse.error) {
                  clientSignFields.push(Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
                    name: spouse.fullName,
                    searchTag: 'FNAREPORT_SPOUSE_SIGNATURE'
                  }));
                  result.clientSignFields.push(clientSignFields);
                  resolve(result);
                } else {
                  reject(new Error('Fail to get spouse info in pCid=' + app.pCid + ' dependant.cid=' + dependant.cid + ' ' + _.get(spouse, 'error')));
                }
              });
            } else {
              result.clientSignFields.push(clientSignFields);
              resolve(result);
            }
          } else {
            reject(new Error('Fail to get attachment ' + app.quotationDocId + '/' + commonApp.PDF_NAME.BI + ' ' + _.get(attFnaReport, 'error')));
          }
        });
      } else {
        reject(new Error('Fail to get client info in pCid=' + app.pCid + ' ' + _.get(pClient, 'error')));
      }
    });
  });
};

var _prepareProposalPdfMobile = function _prepareProposalPdfMobile(session, app, now, bundle) {
  // const SIGNDOC_SIGN_FIELD = {
  //   searchTag: '',
  //   width: 0.124,
  //   height: 0.026,
  //   offsety: 0.006
  // };
  var isFaChannel = session.agent.channel.type === 'FA';

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareProposalPdf', app._id);

  return new Promise(function (resolve, reject) {
    fDao.getAttachment(app.quotationDocId, commonApp.PDF_NAME.BI, function (attProposal) {
      if (attProposal) {
        resolve(attProposal);
      } else {
        reject(new Error('Fail to get attachment ' + app.quotationDocId + '/' + commonApp.PDF_NAME.BI + ' ' + _.get(attProposal, 'error')));
      }
    });
  }).then(function (attProposal) {
    result.agentSignFields.push([Object.assign({}, PROPOSAL_SIGNDOC_SIGN_FIELD, {
      name: app.quotation.agent.name,
      searchTag: 'Signature of Financial Consultant'
    })]);
    result.clientSignFields.push([Object.assign({}, PROPOSAL_SIGNDOC_SIGN_FIELD, {
      name: app.applicationForm.values.proposer.personalInfo.fullName,
      searchTag: 'Signature of Proposer'
    })]);

    result.docid = app._id + '_' + commonApp.PDF_NAME.BI;
    result.pdfStr = attProposal.data;
    result.isSigned = app.isProposalSigned;
    result.docts = now;

    return result;
  });
};

var _prepareAppFormProposerPdfMobile = function _prepareAppFormProposerPdfMobile(session, app, now, bundle) {

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareAppFormProposerPdf', app._id);
  // let isPhSameAsLa = app.applicationForm.values.proposer.extra.isPhSameAsLa === 'Y';
  var hasTrustedIndividual = app.applicationForm.values.proposer.extra.hasTrustedIndividual === 'Y';
  var signatureWidth = 0;
  //no signature for LA age < 18

  //Signature box width
  var numOfSignature = 2; //agent & proposor
  if (hasTrustedIndividual) {
    numOfSignature += 1;
  }
  switch (numOfSignature) {
    case 4:
      signatureWidth = 0.084;
      break;
    case 3:
      // signatureWidth = 0.118;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.THREE_BOXES;
      break;
    default:
      // signatureWidth = 0.188;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.TWO_BOXES;
  }

  return new Promise(function (resolve, reject) {
    fDao.getAttachment(app._id, commonApp.PDF_NAME.eAPP, function (attAppForm) {
      if (attAppForm) {
        resolve(attAppForm);
      } else {
        reject(new Error('Fail to get attachment ' + app._id + '/' + commonApp.PDF_NAME.eAPP + ' ' + _.get(attAppForm, 'error')));
      }
    });
  }).then(function (attAppForm) {
    //prepare result
    result.agentSignFields.push([Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: app.quotation.agent.name,
      searchTag: 'ADVISOR_SIGNATURE',
      width: signatureWidth
    })]);
    var clientSignFields = [Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: app.applicationForm.values.proposer.personalInfo.fullName,
      searchTag: 'PROP_SIGNATURE',
      width: signatureWidth
    })];

    if (hasTrustedIndividual) {
      clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: _.get(app, 'applicationForm.values.proposer.declaration.trustedIndividuals.fullName'),
        searchTag: 'TI_SIGNATURE',
        width: signatureWidth
      }));
    }

    result.clientSignFields.push(clientSignFields);

    result.docid = app._id + '_' + commonApp.PDF_NAME.eAPP;
    result.pdfStr = attAppForm.data;
    result.isSigned = app.isAppFormProposerSigned;
    result.docts = now;

    return result;
  });
};

var _prepareAppFormInsuredPdfMobile = function _prepareAppFormInsuredPdfMobile(session, app, now, bundle, iCid) {

  var attName = _getAppFormAttachmentName(iCid);

  var result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareAppFormInsuredPdf', app._id, '(' + iCid + ')');
  // let isPhSameAsLa = app.applicationForm.values.proposer.extra.isPhSameAsLa === 'Y';
  var iCids = _.get(app, 'iCids', []);
  var iCidIndex = iCids.indexOf(iCid);
  var isAppFormInsuredSigned = _.get(app, 'isAppFormInsuredSigned[' + iCidIndex + ']');
  var isAppFormInsuredCompleted = _.get(app, 'applicationForm.values.insured[' + iCidIndex + '].extra.isCompleted', false);

  // let isSignedByLegalGuardian = app.applicationForm.values.insured.length > 0 && commonApp.checkIsLaSignedByProposer(app, iCidIndex);
  var hasTrustedIndividual = app.applicationForm.values.proposer.extra.hasTrustedIndividual === 'Y';
  var signatureWidth = 0;
  //no signature for LA age < 18

  //Signature box width
  var numOfSignature = 2; //agent & proposor
  if (hasTrustedIndividual) {
    numOfSignature += 1;
  }
  switch (numOfSignature) {
    case 4:
      signatureWidth = 0.084;
      break;
    case 3:
      // signatureWidth = 0.118;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.THREE_BOXES;
      break;
    default:
      // signatureWidth = 0.188;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.TWO_BOXES;
  }

  return new Promise(function (resolve, reject) {
    logger.log('DEBUG: _prepareAppFormInsuredPdf', app._id, '(' + iCid + ')', iCidIndex, isAppFormInsuredCompleted);
    if (iCidIndex < 0 || !isAppFormInsuredCompleted) {
      resolve({});
    } else {
      fDao.getAttachment(app._id, attName, function (attAppForm) {
        if (attAppForm) {
          resolve(attAppForm);
        } else {
          reject(new Error('Fail to get attachment ' + app._id + '/' + attName + ' ' + _.get(attAppForm, 'error')));
        }
      });
    }
  }).then(function (attAppForm) {
    //prepare result
    result.agentSignFields.push([Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: app.quotation.agent.name,
      searchTag: 'ADVISOR_SIGNATURE',
      width: signatureWidth
    })]);
    var clientSignFields = [Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: _.get(app, 'applicationForm.values.proposer.personalInfo.fullName', ''),
      searchTag: 'PROP_SIGNATURE',
      width: signatureWidth
    })];

    if (hasTrustedIndividual) {
      clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: _.get(app, 'applicationForm.values.proposer.declaration.trustedIndividuals.fullName'),
        searchTag: 'TI_SIGNATURE',
        width: signatureWidth
      }));
    }

    clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: _.get(app, 'applicationForm.values.insured[' + iCidIndex + '].personalInfo.fullName', ''),
      searchTag: 'LA_SIGNATURE',
      width: signatureWidth
    }));

    result.clientSignFields.push(clientSignFields);

    result.docid = app._id + '_' + attName;
    result.pdfStr = attAppForm.data;
    result.isSigned = isAppFormInsuredSigned;
    result.docts = now;

    return result;
  });
};

var _saveSignedPdf = function _saveSignedPdf(appId, attId, pdfData, callback) {
  if (session.platform) {
    _saveSignedPdfFromSignDoc(appId, attId, pdfData, callback);
  } else {
    // web
    callback({ success: false });
  }
};

module.exports.saveSignedPdfFromSignDoc = _saveSignedPdfFromSignDoc;
module.exports.saveSignedPdf = _saveSignedPdf;
module.exports.getSignatureInitUrl = _getSignatureInitUrl;
module.exports.getSignatureInitPdfString = _getSignatureInitPdfString;
module.exports.getAppFormAttachmentName = _getAppFormAttachmentName;