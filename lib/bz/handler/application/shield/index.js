'use strict';

var summary = require('./summary');
var app = require('./application');
var sign = require('./signature');
var pay = require('./payment');
var submit = require('./submission');
var supp = require('./supportDocument');
var clientChoice = require('./clientChoice');

module.exports.applyAppForm = summary.applyAppForm;
module.exports.continueApplication = summary.continueApplication;

module.exports.saveAppForm = app.saveAppForm;
module.exports.invalidateApplication = app.invalidateApplication;
module.exports.setSignExpiryShown = app.setSignExpiryShown;
module.exports.goNextStep = app.goNextStep;

module.exports.getSignatureUpdatedUrl = sign.getSignatureUpdatedUrl;
module.exports.getSignatureUpdatedPdfString = sign.getSignatureUpdatedPdfString;

module.exports.checkPaymentStatus = pay.checkPaymentStatus;
module.exports.savePaymentMethods = pay.savePaymentMethods;
module.exports.confirmAppPaid = pay.confirmAppPaid;

module.exports.saveSubmissionValues = submit.saveSubmissionValues;
module.exports.submission = submit.submission;

module.exports.showSupportDocments = supp.showSupportDocments;
module.exports.closeSupportDocments = supp.closeSupportDocments;

module.exports.setMenuItemTemplate_shield = clientChoice.setMenuItemTemplate_shield;
module.exports.setMenuItemValues_shield = clientChoice.setMenuItemValues_shield;
module.exports.getClientChoiceDefaultString_shield = clientChoice.getClientChoiceDefaultString_shield;