'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var _ = require('lodash');
var _getOr = require('lodash/fp/getOr');
var moment = require('moment');
var logger = global.logger || console;

var async = require('async');

var commonApp = require('../common');
var dao = require('../../../cbDaoFactory').create();
var _application = require('./application');
var appDao = require('../../../cbDao/application');

var _require = require('../../../utils/RemoteUtils'),
    callApiComplete = _require.callApiComplete;

var _removeInitPayMethodOptions = function _removeInitPayMethodOptions(templateItems, removeOptions) {
  _.each(templateItems, function (templateObj) {
    if (templateObj.items) {
      _removeInitPayMethodOptions(templateObj.items, removeOptions);
    } else if (templateObj.options) {
      templateObj.options = _.filter(templateObj.options, function (option) {
        return removeOptions.indexOf(option.value) < 0;
      });
    }
  });
};

var _getPaymentTemplate = function _getPaymentTemplate(session, frozen, initPayment, isFaChannel) {
  var result = {
    success: false,
    template: {}
  };

  return new Promise(function (resolve, reject) {
    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.PAYMENT_SHIELD, function (tmpl) {
      if (tmpl && !tmpl.error) {
        var removeOptions = [];
        if (initPayment >= 5000) {
          removeOptions.push('cash');
        }
        if (!isFaChannel) {
          removeOptions.push('payLater');
        }
        if (tmpl.items && removeOptions.length > 0) {
          _removeInitPayMethodOptions(tmpl.items, removeOptions);
        }
        result.success = true;
        if (frozen) {
          if (session.platform) {
            commonApp.frozenTemplateMobile(tmpl);
          } else {
            commonApp.frozenTemplate(tmpl);
          }
        }
        result.template = tmpl;
        resolve(result);
      } else {
        reject(new Error('Fail to get payment template ' + _.get(tmpl, 'error')));
      }
    });
  }).catch(function (err) {
    logger.error('ERROR in getPaymentTemplate ' + err);
  });
};

//=ApplicationHandler.getPaymentTemplateValues
var _getPaymentTemplateValues = function _getPaymentTemplateValues() {
  return new Promise(function (resolve, reject) {
    resolve();
  }).catch(function (error) {
    logger.error('ERROR in _getPaymentTemplateValues ' + error);
  });
};

var _savePaymentMethods = function _savePaymentMethods(data) {
  var appId = data.appId,
      changedValues = data.changedValues;


  return _application.getApplication(appId).then(function (app) {
    return new Promise(function (resolve, reject) {
      async.waterfall([function (callback) {
        //Update Application
        app.payment = _.assignIn(app.payment, changedValues);
        _application.updateApplicationCompletedStep(app);
        commonApp.updateAndReturnApp(app, callback);
      }, function (parentApplication, callback) {
        _saveParentPaymenttoChild(parentApplication, callback);
      }], function (err, application) {
        if (err) {
          logger.error('ERROR: _savePaymentMethods ' + appId + ', ' + err);
          resolve({ success: false });
        } else {
          resolve(application);
        }
      });
    }).catch(function (error) {
      logger.error('ERROR in _savePaymentMethods ' + error);
    });
  });
};

module.exports.savePaymentMethods = function (data, session, callback) {
  var appId = data.appId;


  return _savePaymentMethods(data).then(function (app) {
    logger.log('INFO: savePaymentMethods - end [RETURN=100]', appId);
    if (_.isFunction(callback)) {
      callback({ success: true, application: app });
    } else {
      return { success: true };
    }
  }).catch(function (error) {
    logger.error('ERROR: savePaymentMethods - end [RETURN=-101]', appId, error);
    if (_.isFunction(callback)) {
      callback({ success: false });
    } else {
      return { success: false };
    }
  });
};

var _saveParentPaymenttoChild = function _saveParentPaymenttoChild(parentApplication, callback) {
  var childIds = _.get(parentApplication, 'childIds');
  var payment = _.get(parentApplication, 'payment');
  _application.getChildApplications(childIds).then(function (childApplications) {
    var updAppPromises = childApplications.map(function (app) {
      return new Promise(function (resolve, reject) {
        app.payment = _transformParentPaymenttoChildPayment(payment, app.id);
        app.payment.trxAmount = _.get(app, 'payment.cashPortion');
        appDao.updApplication(app.id, app, function (res) {
          if (res && !res.error) {
            resolve(res);
          } else {
            reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
          }
        });
      });
    });

    return Promise.all(updAppPromises).then(function (results) {
      callback(null, parentApplication);
    }, function (rejectReason) {
      callback('ERROR in _saveParentPaymenttoChild with Reject Reason,  ' + rejectReason);
    }).catch(function (error) {
      logger.error('ERROR in _saveParentPaymenttoChild ' + error);
      callback('ERROR in _saveParentPaymenttoChild, ' + error);
    });
  }).catch(function (error) {
    logger.error('ERROR in _saveParentPaymenttoChild: ' + error);
  });
};

var _transformParentPaymenttoChildPayment = function _transformParentPaymenttoChildPayment(payment, childAppId) {
  var parentPayment = _.cloneDeep(payment);
  var childPayment = parentPayment;
  var cpfAccNo = _.get(payment, 'cpfMedisaveAccDetails[0].cpfAccNo');
  var paymentChildObj = {};
  var cpfisoaBlock = {
    'idCardNo': cpfAccNo
  };
  var cpfissaBlock = {
    'idCardNo': cpfAccNo
  };
  var srsBlock = {
    'idCardNo': cpfAccNo
  };

  _.each(childPayment.premiumDetails, function (premiumDetail) {
    if (premiumDetail.applicationId === childAppId) {
      paymentChildObj = premiumDetail;
    }
  });
  childPayment.cpfisoaBlock = cpfisoaBlock;
  childPayment.cpfissaBlock = cpfissaBlock;
  childPayment.srsBlock = srsBlock;

  childPayment = _.assignIn(childPayment, paymentChildObj);
  return _.omit(childPayment, ['cpfMedisaveAccDetails', 'premiumDetails', 'applicationId']);
};

module.exports.preparePayment = function (data) {
  var appId = data.appId;


  return _application.getApplication(appId).then(function (app) {
    return new Promise(function (resolve, reject) {
      async.waterfall([function (callback) {
        if (_.isEmpty(app.payment)) {
          // Initial Payment Store for first Time
          _initPaymentStore(data, app, callback);
        } else {
          callback(null, app);
        }
      }], function (err, callbackApplication) {
        if (err) {
          logger.error('ERROR prepare Payemnt: ' + appId + ', ' + err);
          resolve({ success: false });
        } else {
          resolve(callbackApplication);
        }
      });
    }).catch(function (error) {
      logger.error('ERROR in preparePayment ' + error);
    });
  });
};

var _initPaymentStore = function _initPaymentStore(data, app, cb) {
  var appId = data.appId;
  var pCid = app.pCid;

  async.waterfall([function (callback) {
    // Init Premium Details
    _application.getApplication(appId).then(function (app) {
      var payment = {
        premiumDetails: [],
        cpfMedisaveAccDetails: [],
        totCPFPortion: 0,
        totCashPortion: 0,
        totMedisave: 0
      };

      var cidsMapping = _.get(app, 'iCidMapping');
      var iCids = _.keys(cidsMapping);
      var laPremiumDetails = [];
      var proposerPremiumDetails = [];
      var mergeProposerPremiumDetails = false;
      _.each(iCids, function (cid, key) {
        var iCidChildren = _.get(cidsMapping, cid);
        _.each(iCidChildren, function (mapping) {
          var nonAXASheild = _.get(mapping, 'covCode') === 'ASIM' ? false : true;
          var premiumDetails = [];
          if (cid === pCid) {
            premiumDetails.push(_createPremiumDetails(mapping, app, cid, true, nonAXASheild));
            premiumDetails.postfix = '(proposer)';
            proposerPremiumDetails = [].concat(_toConsumableArray(proposerPremiumDetails), premiumDetails);
            mergeProposerPremiumDetails = true;
          } else {
            premiumDetails.push(_createPremiumDetails(mapping, app, cid, false, nonAXASheild));
            laPremiumDetails = [].concat(_toConsumableArray(laPremiumDetails), premiumDetails);
          }
        });
      });

      if (mergeProposerPremiumDetails) {
        payment.premiumDetails = [].concat(_toConsumableArray(proposerPremiumDetails), _toConsumableArray(laPremiumDetails));
      } else {
        payment.premiumDetails = [].concat(_toConsumableArray(laPremiumDetails));
      }

      payment.premiumDetails = _.filter(payment.premiumDetails, function (obj) {
        return !_.isEmpty(obj);
      });

      // Cal tot premium
      _.each(payment.premiumDetails, function (premiumObj) {
        payment.totMedisave += premiumObj.medisave;
        payment.totCPFPortion += premiumObj.cpfPortion;
        payment.totCashPortion += premiumObj.cashPortion;
      });
      callback(null, payment);
    });
  }, function (payment, callback) {
    // Init cpfMedisaveAccDetails
    dao.getDocFromCacheFirst(pCid, function (clientProfile) {
      if (clientProfile && !clientProfile.error) {
        var proposerAccDetails = {
          cpfAccHolderName: _.get(clientProfile, 'fullName'),
          cpfAccNo: _.get(clientProfile, 'idCardNo')
        };
        payment.cpfMedisaveAccDetails.push(proposerAccDetails);
        callback(null, payment);
      } else {
        callback('Cannot find client profile');
      }
    });
  }, function (payment, callback) {
    //Update Application
    app.isInitialPaymentCompleted = false;
    app.payment = payment;
    if (app.appStep < commonApp.EAPP_STEP.PAYMENT) {
      app.appStep = commonApp.EAPP_STEP.PAYMENT;
    }
    appDao.updApplication(appId, app, function () {
      _saveParentPaymenttoChild(app, callback);
    });
  }], function (err, finalApplication) {
    if (err) {
      logger.error('ERROR in initial payment store: ' + err);
      cb(err, { success: false });
    } else {
      cb(null, finalApplication);
    }
  });
};

var _createPremiumDetails = function _createPremiumDetails(mapping, application, cid, isPhSameAsLa, nonAXASheild) {
  var pathToGroupedPlan = void 0;
  var personInfoObj = _.get(application, 'quotation.insureds.[' + cid + ']');
  if (isPhSameAsLa) {
    pathToGroupedPlan = 'applicationForm.values.proposer.groupedPlanDetails';
  } else {
    var insuredApplicationFormValues = _getOr({}, 'applicationForm.values.insured', application);
    var currentInsuredIndex = -1;
    insuredApplicationFormValues.forEach(function (obj, index) {
      if (_.get(obj, 'personalInfo.cid') === cid) {
        currentInsuredIndex = index;
      }
    });

    pathToGroupedPlan = currentInsuredIndex !== -1 ? 'applicationForm.values.insured[' + currentInsuredIndex + '].groupedPlanDetails' : undefined;
  }
  var groupedPlanDetails = _.get(application, pathToGroupedPlan);

  var planList = nonAXASheild ? _.get(groupedPlanDetails, 'riderPlanList') : _.get(groupedPlanDetails, 'basicPlanList');

  var basicPlanObj = nonAXASheild ? _.get(_.filter(planList, function (obj) {
    return obj.covCode === 'ASP';
  }), [0]) : _.get(_.filter(planList, function (obj) {
    return obj.covCode === 'ASIM';
  }), [0]);

  // Rider only has cash 
  var cashPort = nonAXASheild ? _getOr(0, 'totalRiderPremium', groupedPlanDetails) : _getOr(0, 'cashPortion', basicPlanObj);
  var payFrequency = _.get(basicPlanObj, 'payFreq');
  var details = {
    cashPortion: payFrequency === 'M' ? cashPort * 2 : cashPort,
    covName: _.get(basicPlanObj, 'covName'),
    cpfPortion: nonAXASheild ? 0 : _getOr(0, 'cpfPortion', basicPlanObj),
    medisave: nonAXASheild ? 0 : _getOr(0, 'medisave', basicPlanObj),
    laName: _.get(personInfoObj, 'iFullName'),
    policyNumber: _.get(mapping, 'policyNumber'),
    applicationId: _.get(mapping, 'applicationId'),
    subseqPayMethod: cashPort !== 0 && payFrequency === 'M' ? 'Y' : 'N',
    cid: cid,
    payFrequency: payFrequency
  };

  return details;
};

module.exports.confirmAppPaid = function (data, session, cb) {
  var masterApplicationId = _.get(data, 'appId');
  if (masterApplicationId) {
    appDao.getApplication(data.appId, function (masterApplication) {
      if (masterApplication && !masterApplication.error) {
        var isSubmitted = _.get(masterApplication, 'isSubmittedStatus');
        async.waterfall([function (callback) {
          masterApplication.isInvalidatedPaidToRLS = false;
          masterApplication.isInitialPaymentCompleted = true;
          masterApplication.lastUpdateDate = new Date().getTime();
          if (!masterApplication.isSubmittedStatus) {
            masterApplication.isSubmittedStatus = true;
            masterApplication.applicationSubmittedDate = new Date().getTime();
          }
          commonApp.updateAndReturnApp(masterApplication, callback);
        }, function (mApplication, callback) {
          var updateObj = {};
          if (!isSubmitted) {
            updateObj = {
              isInvalidatedPaidToRLS: false,
              isInitialPaymentCompleted: mApplication.isInitialPaymentCompleted,
              lastUpdateDate: mApplication.lastUpdateDate,
              isSubmittedStatus: mApplication.isSubmittedStatus,
              applicationSubmittedDate: mApplication.applicationSubmittedDate
            };
          } else {
            updateObj = {
              isInitialPaymentCompleted: mApplication.isInitialPaymentCompleted,
              lastUpdateDate: mApplication.lastUpdateDate
            };
          }

          commonApp.saveParentValuestoChild(masterApplication, updateObj, callback);
        }, function (mApplication, callback) {
          if (!isSubmitted) {
            var promises = [];
            var childIds = _.get(mApplication, 'childIds');
            _.each(childIds, function (id) {
              promises.push(new Promise(function (resolve, reject) {
                if (session.platform) {
                  resolve({ success: true });
                } else {
                  callApiComplete('/submitInvalidApp/' + id, 'GET', {}, null, true, function (resp) {
                    logger.log('INFO: Submit invalidated paid application (paid after invalidated):', id, resp);
                    if (resp && resp.success) {
                      resolve({ success: true });
                    } else {
                      reject('Call submitInvalidApp failure: ' + id);
                    }
                  });
                }
              }));
            });

            Promise.all(promises).then(function (results) {
              callback(null, mApplication);
            }, function (rejectReason) {
              callback('Rejected in calling all promises confirmAppPaid:,  ' + rejectReason);
            }).catch(function (error) {
              callback('ERROR in calling all promises confirmAppPaid: ' + error);
            });
          } else {
            callback(null, mApplication);
          }
        }], function (error, app) {
          if (error) {
            cb({ success: false, error: 'Update Application failure:' + app.id });
          } else {
            dao.updateViewIndex('main', 'summaryApps');
            cb({ success: true });
          }
        });
      } else {
        cb({ success: false, error: 'Cannot get Application:' + data.appId });
      }
    });
  } else {
    cb({ success: false, error: 'Invalid Master Application' });
  }
};
/** Copy from old non-shield payment */
var _checkPaymentStatus = function _checkPaymentStatus(data, session, cb) {
  var trxNo = data.trxNo;
  var appId = data.appId;

  logger.log('INFO: checkShieldPaymentStatus - start', appId);
  _application.getApplication(appId).then(function (app) {
    if (app && !app.error && app.payment) {
      var status = app.payment.trxStatus;
      var trxStatusRemark = '';
      if (status === 'I') {
        trxStatusRemark = 'O';
      } else {
        trxStatusRemark = status;
      }
      if (trxStatusRemark !== app.payment.trxStatusRemark) {
        app.payment.trxStatusRemark = trxStatusRemark;
        appDao.updApplication(appId, app, function (upsertResult) {
          if (upsertResult && !upsertResult.error) {
            logger.log('INFO: checkPaymentStatus - end [RETURN=1]', appId);
            async.waterfall([function (callback) {
              _saveParentPaymenttoChild(app, callback);
            }], function (error, callback) {
              if (error) {
                logger.error('ERROR: Update child application in wildcard payment process ' + error);
                cb({
                  success: false,
                  application: app
                });
              } else {
                cb({
                  success: true,
                  application: app
                });
              }
            });
          } else {
            logger.error('ERROR: checkPaymentStatus - end [RETURN=-2]', appId, _.get(upsertResult, 'error'));
            cb({
              success: false,
              application: app
            });
          }
        });
      } else {
        logger.log('INFO: checkPaymentStatus - end [RETURN=2]', appId);
        cb({
          success: true,
          application: app
        });
      }
    } else {
      logger.error('ERROR: checkPaymentStatus - end [RETURN=-1]', appId, _.get(app, 'error'));
      cb({
        success: false,
        application: app
      });
    }
  }).catch(function (error) {
    logger.error('ERROR: in check shield payment status ' + error);
    cb({
      success: false,
      application: {}
    });
  });
};

module.exports.checkPaymentStatus = _checkPaymentStatus;
module.exports.getPaymentTemplate = _getPaymentTemplate;
module.exports.getPaymentTemplateValues = _getPaymentTemplateValues;
module.exports.saveParentPaymenttoChild = _saveParentPaymenttoChild;