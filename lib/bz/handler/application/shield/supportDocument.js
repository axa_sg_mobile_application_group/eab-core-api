'use strict';

//=ApplicationHandler.getSupportDocuments
var logger = global.logger || console;
var _ = require('lodash');
var dao = require('../../../cbDaoFactory').create();
var applicationDao = require('../../../cbDao/application');
var bundleDao = require('../../../cbDao/bundle');
var ApprovalHandler = require('../../../ApprovalHandler');
var commonAppHandler = require('../common');
var clientDao = require('../../../cbDao/client');
var SuppDocsUtils = require('../../../utils/SuppDocsUtils');

var _require = require('../../../utils/TokenUtils'),
    createPdfToken = _require.createPdfToken,
    setPdfTokensToRedis = _require.setPdfTokensToRedis;

var DateUtils = require('../../../../common/DateUtils');
var moment = require('moment');

var _genSupportDocumentsValues = function _genSupportDocumentsValues(app) {
  var result = {};

  var genSuppDocsPolicyFormValues = function genSuppDocsPolicyFormValues() {
    var values = {
      "sysDocs": {
        "fnaReport": {
          "id": "fnaReport",
          "title": "e-FNA",
          "fileType": "application/pdf",
          "fileSize": ""
        },
        "proposal": {
          "id": "proposal",
          "title": "Product Summary",
          "fileType": "application/pdf",
          "fileSize": ""
        },
        "appPdf": {
          "id": "appPdf",
          "title": 'e-App (' + _.get(app, 'quotation.pFullName') + ')',
          "fileType": "application/pdf",
          "fileSize": ""
        },
        // "eCpdPdf":{
        //   "id": 'eCpdPdf',
        //   "title": "e-CPD",
        //   "fileType":"application/pdf",
        //   "fileSize": ""
        // },
        "eapproval_supervisor_pdf": {
          "id": "eapproval_supervisor_pdf",
          "title": "Supervisor Validation",
          "fileType": "application/pdf",
          "fileSize": ""
        },
        "faFirm_Comment": {
          "id": "faFirm_Comment",
          "title": "Comments by FA Firm Admin",
          "fileType": "application/pdf",
          "fileSize": ""
        }
      },
      "mandDocs": {
        "cAck": [],
        "pNric": [],
        "pPass": [],
        "pPassport": [],
        "pPassportWStamp": [],
        "pAddrProof": [],
        "thirdPartyID": [],
        "thirdPartyAddrProof": [],
        "axasam": [],
        "chequeCashierOrder": [],
        "teleTransfer": [],
        "cash": [],
        "healthDeclaration": [],
        "pChinaVisit": [],
        "pBoardingPass": []
      },
      "optDoc": {
        "axasam": [],
        "chequeCashierOrder": [],
        "teleTransfer": [],
        "cash": []
      }
    };

    _.forEach(_.get(app, 'iCids', []), function (iCid) {
      var iKey = 'appPdf' + iCid;
      var iFullName = _.get(app, 'quotation.insureds.' + iCid + '.iFullName');
      var docObject = {
        'id': iKey,
        'title': 'e-App (' + iFullName + ')',
        'fileType': 'application/pdf',
        'fileSize': ''
      };
      _.set(values, 'sysDocs.' + iKey, docObject);
    });

    return values;
  };

  var genSuppDocsInsuredValues = function genSuppDocsInsuredValues() {
    var values = {
      "mandDocs": {
        "iNric": [],
        "iReentryPermit": []
      },
      "optDoc": {
        "iJuvenileQuestions": [],
        "iMedicalReport": [],
        "iDischargeSummary": [],
        "iOtherIllnessQues": []
      }
    };
    return values;
  };

  var genSuppDocsProposerValues = function genSuppDocsProposerValues() {
    var values = {
      "mandDocs": {},
      "optDoc": {
        "pMedicalReport": [],
        "pDischargeSummary": [],
        "pOtherIllnessQues": []
      }
    };
    return values;
  };

  result.policyForm = genSuppDocsPolicyFormValues();
  result.proposer = genSuppDocsProposerValues();

  _.forEach(_.get(app, 'iCids'), function (iCid) {
    result[iCid] = genSuppDocsInsuredValues();
  });

  // For Not influence batch submitToRLSWFI function
  result[app.pCid] = genSuppDocsInsuredValues();

  return result;
};

var _getSupportDocumentsTemplate = function _getSupportDocumentsTemplate(app, appStatus, agentData, eApprovalCase) {
  logger.log('INFO: _getSupportDocumentsTemplate');
  var promises = [];
  var template = {};

  var _checkAppFormInsQuesAnswer = function _checkAppFormInsQuesAnswer(appFormInsObject) {
    var medicalReportQuesNumList = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '20', '21', '22', '23', '24'];
    /**
    * Ref: "AXASG-EASE-FS-Release 2-Shield V1.4", then "Shield-medical Qn"
    * T10 - "Have you ever had been treated for or been told to get treatment for: "
    * contains: "HEALTH_SHIELD_" + [01-17, 18, 20-23].
    * T13 - "Have you ever been informed by Ministry of Health that an extra premium of 30% has been imposed on your Medishield life insurance?"
    * contains: "INS05"
    * T14 - "Have you undergone any sex reassignment surgery?"
    * contains: "HEALTH_SHIELD_19"
    */
    var isAnyT10QuesCYes = false;
    var isAnyT10QuesCNo = false;
    var isAnyT13QuesDYes = false;
    var isAnyT13QuesDNo = false;
    var isAnyT14QuesCYes = false;
    _.forEach(medicalReportQuesNumList, function (quesNumber) {
      _.forEach(_.get(appFormInsObject, 'HEALTH_SHIELD_' + quesNumber + '_DATA', []), function (t10RowData) {
        if (_.get(t10RowData, 'HEALTH_SHIELD_' + quesNumber + 'c') === 'Y') {
          isAnyT10QuesCYes = true;
        } else if (_.get(t10RowData, 'HEALTH_SHIELD_' + quesNumber + 'c') === 'N') {
          isAnyT10QuesCNo = true;
        }
      });
    });
    // Health13b question: "Have you been diagnosed with any pregnancy complications or pregnancy-related conditions?"
    // It is one of the displaying 'Medical Report' condition in Support Document
    _.forEach(_.get(appFormInsObject, 'HEALTH13b_DATA', []), function (t13bRowData) {
      if (_.get(t13bRowData, 'HEALTH13b3') === 'Y') {
        isAnyT10QuesCYes = true;
      } else if (_.get(t13bRowData, 'HEALTH13b3') === 'N') {
        isAnyT10QuesCNo = true;
      }
    });

    _.forEach(_.get(appFormInsObject, 'INS05_DATA', []), function (ins05RowData) {
      if (_.get(ins05RowData, 'INS05d') === 'Y') {
        isAnyT13QuesDYes = true;
      } else if (_.get(ins05RowData, 'INS05d') === 'N') {
        isAnyT13QuesDNo = true;
      }
    });
    _.forEach(_.get(appFormInsObject, 'HEALTH_SHIELD_19_DATA', []), function (t19RowData) {
      if (_.get(t19RowData, 'HEALTH_SHIELD_19c') === 'Y') {
        isAnyT14QuesCYes = true;
      }
    });

    return {
      isAnyT10QuesCYes: isAnyT10QuesCYes,
      isAnyT10QuesCNo: isAnyT10QuesCNo,
      isAnyT13QuesDYes: isAnyT13QuesDYes,
      isAnyT13QuesDNo: isAnyT13QuesDNo,
      isAnyT14QuesCYes: isAnyT14QuesCYes
    };
  };

  /**
   * agentData = session.agent
   * agentChannel = session.agent.channel.type
   * agentCode = session.agent.agentCode
   * managerCode = session.agent.managerCode
   */
  var getSuppDocsPolicyFormTemplate = function getSuppDocsPolicyFormTemplate() {
    logger.log('INFO: getSuppDocsPolicyFormTemplate');
    // const eCpdPdfName = 'eCpdPdf';
    var pCid = _.get(app, 'quotation.pCid');
    var isFAChannel = _.get(agentData, 'channel.type') === 'FA';
    var sysDocsDocuments = [];
    var mandDocsDocuments = [];
    var optDocsDocuments = [];

    var _commonAppHandler$get = commonAppHandler.getPayerInfo(app),
        isThirdPartyPayer = _commonAppHandler$get.isThirdPartyPayer;

    return new Promise(function (resolve) {

      clientDao.getProfileById(pCid, function (pProfile) {
        if (_.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated')) {
          sysDocsDocuments.push({
            type: 'file',
            id: 'appPdf'
          });
        }
        _.forEach(_.get(app, 'iCids', []), function (iCid) {
          var foundInsuredObj = _.find(_.get(app, 'applicationForm.values.insured'), function (insured) {
            return insured.personalInfo.cid === iCid;
          });
          if (_.get(foundInsuredObj, 'extra.isPdfGenerated')) {
            sysDocsDocuments.push({
              type: 'file',
              id: 'appPdf' + iCid
            });
          }
        });

        if (!isFAChannel) {
          sysDocsDocuments.push({
            type: 'file',
            id: 'fnaReport'
          });
        }

        sysDocsDocuments.push({
          type: 'file',
          id: 'proposal'
        });

        /*
        * Shield contains NO eCpd as in FS
        if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(_.get(app, 'payment.initPayMethod')) > -1 && app.isSubmittedStatus){
          sysDocsDocuments.push({
            type: 'file',
            id: eCpdPdfName
          });
        }
        */
        var showSupervisorStatus = ['A', 'R', 'PFAFA'];
        if (showSupervisorStatus.indexOf(eApprovalCase.approvalStatus) > -1 && eApprovalCase.isFACase === true && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined) {
          //Show when the approver is not the director
          if (eApprovalCase.agentId !== eApprovalCase.approveRejectManagerId) {
            sysDocsDocuments.push({
              type: 'file',
              id: 'eapproval_supervisor_pdf'
            });
          }

          //Show when it is approved by FA Admin
          if (eApprovalCase.approver_FAAdminCode !== '') {
            sysDocsDocuments.push({
              "type": "file",
              "id": "faFirm_Comment"
            });
          }
        } else if ((eApprovalCase.approvalStatus === 'A' || eApprovalCase.approvalStatus === 'R') && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined) {
          // } else if (eApprovalCase.approvalStatus === 'A' || eApprovalCase.approvalStatus === 'R'){
          sysDocsDocuments.push({
            "type": "file",
            "id": "eapproval_supervisor_pdf"
          });
        }

        // mandDocsDocuments
        // Application is in FA channel
        if (isFAChannel) {
          mandDocsDocuments.push({
            "type": "subSection",
            "id": "cAck",
            "title": "Client acknowledgement/ FNA"
          });
        }
        // proposer is singaporean of singapore pr, N1 stands for singapore
        if (pProfile.nationality === 'N1' || pProfile.prStatus === 'Y') {
          mandDocsDocuments.push({
            "type": "subSection",
            "id": "pNric",
            "title": "Proposer's Singapore NRIC "
          });
        } else {
          if (pProfile.pass == 'ep' || pProfile.pass == 'wp' || pProfile.pass == 'dp' || pProfile.pass == 's' || pProfile.pass == 'lp' || pProfile.pass === 'sp') {
            mandDocsDocuments.push({
              "type": "subSection",
              "id": "pPass",
              "title": "Proposer's Copy of valid pass in Singapore ",
              "toolTips": "Copy of valid Employment Pass / S Pass / Work Permit / Dependant / Student / Long Term Visit Pass. (both sides)"
            });
          } else {
            mandDocsDocuments.push({
              "type": "subSection",
              "id": "pPassport",
              "title": "Proposer's Copy of valid passport (first page) ",
              "toolTips": "Passport cover page which contain all personal details "
            });
            mandDocsDocuments.push({
              "type": "subSection",
              "id": "pPassportWStamp",
              "title": "Proposer's Copy of valid passport pages with entry stamps issued by Singapore immigration ",
              "toolTips": "Passport entry stamp is required as proof of entry into Singapore. "
            });
          }
        }

        mandDocsDocuments.push({
          "type": "subSection",
          "id": "pAddrProof",
          "title": "Proposer's Proof of Residential Address "
        });
        // If payer is third party payer
        if (isThirdPartyPayer) {
          mandDocsDocuments.push({
            "type": "subSection",
            "id": "thirdPartyID",
            "title": "Third Party Payor's ID copy ",
            "toolTips": "Example of ID copy for third party payor: NRIC / Valid Pass in Singapore / Passport "
          });
          mandDocsDocuments.push({
            "type": "subSection",
            "id": "thirdPartyAddrProof",
            "title": "Third Party Payor's Proof of Residential Address "
          });
        }

        var pMailingAddrCountry = _.get(app, "applicationForm.values.proposer.personalInfo.mAddrCountry", "");
        // proposer is China nationality and type of pass is others
        if (pProfile.nationality !== "N1" && pProfile.prStatus === "N" && (pMailingAddrCountry === "R51" || pMailingAddrCountry === "R52" || pProfile.residenceCountry === "R51" || pProfile.residenceCountry === "R52") && (pProfile.pass === "o" || pProfile.pass === "svp" || pProfile.pass === "lp")) {
          mandDocsDocuments.push({
            "type": "subSection",
            "id": "pChinaVisit",
            "title": "Proposer\"s Mainland China Visitor Declaration Form ",
            "toolTips": "Mainland China Visitor Declaration Form is compulsory for applicants who are China residents. "
          });
          mandDocsDocuments.push({
            "type": "subSection",
            "id": "pBoardingPass",
            "title": "Proposer\"s Copy of Boarding Pass ",
            "toolTips": "Copy of Boarding Pass is compulsory for applicants who are China residents. "
          });
        }

        // optDocsDocuments
        if (appStatus === 'SUBMITTED' || _.get(agentData, 'agentCode') === _.get(agentData, 'managerCode')) {
          // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
          if (app.payment && app.payment.initPayMethod) {
            if (app.payment.initPayMethod == 'axasam') {
              mandDocsDocuments.push({
                "type": "subSection",
                "id": "axasam",
                "title": "AXS/SAM Receipt "
              });
            } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
              mandDocsDocuments.push({
                "type": "subSection",
                "id": "chequeCashierOrder",
                "title": "Cheque / Cashier Order Copy "
              });
            } else if (app.payment.initPayMethod == 'teleTransfter') {
              mandDocsDocuments.push({
                "type": "subSection",
                "id": "teleTransfer",
                "title": "Telegraphic Transfer Receipt "
              });
            } else if (app.payment.initPayMethod === 'cash') {
              mandDocsDocuments.push({
                "type": "subSection",
                "id": "cash",
                "title": "Conditional Receipt of Cash "
              });
            }
          }
        } else {
          // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
          if (app.payment && app.payment.initPayMethod) {
            if (app.payment.initPayMethod == 'axasam') {
              optDocsDocuments.push({
                "type": "subSection",
                "id": "axasam",
                "title": "AXS/SAM Receipt "
              });
            } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
              optDocsDocuments.push({
                "type": "subSection",
                "id": "chequeCashierOrder",
                "title": "Cheque / Cashier Order Copy "
              });
            } else if (app.payment.initPayMethod == 'teleTransfter') {
              optDocsDocuments.push({
                "type": "subSection",
                "id": "teleTransfer",
                "title": "Telegraphic Transfer Receipt "
              });
            } else if (app.payment.initPayMethod == 'cash') {
              optDocsDocuments.push({
                "type": "subSection",
                "id": "cash",
                "title": "Conditional Receipt of Cash "
              });
            }
          }
        }

        // Health Declaration
        if (commonAppHandler.isShowHealthDeclaration(app)) {
          if (eApprovalCase.approvalStatus && !_.includes(['A', 'R', 'E'], eApprovalCase.approvalStatus)) {
            mandDocsDocuments.push({
              "type": "subSection",
              "id": "healthDeclaration",
              "title": "Health Declaration"
            });
          }
        }

        // let subTemplate = [];
        var subTemplate = {};

        subTemplate.sysDocs = {
          "id": "sysDocs",
          "title": "System Document",
          "hasSubLevel": "false",
          "disabled": "true",
          "type": "section",
          "items": sysDocsDocuments
        };

        subTemplate.mandDocs = {
          "id": "mandDocs",
          "hasSubLevel": "false",
          "disabled": "false",
          "title": "Mandatory Document",
          "type": "section",
          "items": mandDocsDocuments
        };

        if (optDocsDocuments.length > 0) {
          subTemplate.optDoc = {
            "id": "optDoc",
            "hasSubLevel": "false",
            "disabled": "false",
            "title": "Optional Document",
            "type": "section",
            "items": optDocsDocuments
          };
        }

        subTemplate.otherDoc = {
          "id": "otherDoc",
          "hasSubLevel": "true",
          "disabled": "false",
          "title": "Other Document",
          "type": "section",
          "items": []
        };

        template.policyForm = {
          tabName: 'Policy Form',
          tabContent: subTemplate,
          tabSequence: 0
        };
        resolve();
      });
    });
  };

  var getSuppDocsProposerTemplate = function getSuppDocsProposerTemplate(app) {
    logger.log('INFO: getSuppDocsProposerTemplate');
    var optDocsDocuments = [];

    return new Promise(function (resolve) {
      var pAppFormInsurablity = _.get(app, 'applicationForm.values.proposer.insurability', {});

      var _checkAppFormInsQuesA = _checkAppFormInsQuesAnswer(pAppFormInsurablity),
          isAnyT10QuesCYes = _checkAppFormInsQuesA.isAnyT10QuesCYes,
          isAnyT10QuesCNo = _checkAppFormInsQuesA.isAnyT10QuesCNo,
          isAnyT13QuesDYes = _checkAppFormInsQuesA.isAnyT13QuesDYes,
          isAnyT13QuesDNo = _checkAppFormInsQuesA.isAnyT13QuesDNo,
          isAnyT14QuesCYes = _checkAppFormInsQuesA.isAnyT14QuesCYes;

      // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as Yes, or at least 1 of T13 column "Any Existing Medical reports?" selected as Yes'


      if (isAnyT10QuesCYes || isAnyT13QuesDYes) {
        optDocsDocuments.push({
          type: 'subSection',
          id: 'pMedicalReport',
          title: 'Medical Report'
        });
      }

      // add condition as 'If T14 column "Discharge summary  Report" selected as Yes'
      if (isAnyT14QuesCYes) {
        optDocsDocuments.push({
          type: 'subSection',
          id: 'pDischargeSummary',
          title: 'Discharge Summary Report'
        });
      }

      // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as no and at least 1 of T13 column "Any Existing Medical reports?" selected as No'
      if (isAnyT10QuesCNo || isAnyT13QuesDNo) {
        optDocsDocuments.push({
          type: 'subSection',
          id: 'pOtherIllnessQues',
          title: 'Other Illness Questionnaire'
        });
      }

      var subTemplate = {};

      subTemplate.optDoc = {
        "id": "optDoc",
        "hasSubLevel": "false",
        "disabled": "false",
        "title": "Optional Document",
        "type": "section",
        "items": optDocsDocuments
      };

      subTemplate.otherDoc = {
        "id": "otherDoc",
        "hasSubLevel": "true",
        "disabled": "false",
        "title": "Other Document",
        "type": "section",
        "items": []
      };

      template.proposer = {
        tabName: _.get(app, 'quotation.pFullName'),
        tabContent: subTemplate,
        tabSequence: 1
      };
      resolve();
    });
  };

  var getSuppDocsInsuredListTemplate = function getSuppDocsInsuredListTemplate(app) {
    logger.log('INFO: getSuppDocsInsuredListTemplate');

    return new Promise(function (resolve) {
      var promises = [];
      _.forEach(_.get(app, 'iCids'), function (iCid, iIndex) {
        promises.push(new Promise(function (resolve2) {
          clientDao.getProfileById(iCid, function (iProfile) {
            var iAppFormInsurablity = _.get(app, 'applicationForm.values.insured[' + iIndex + '].insurability', {});

            var _checkAppFormInsQuesA2 = _checkAppFormInsQuesAnswer(iAppFormInsurablity),
                isAnyT10QuesCYes = _checkAppFormInsQuesA2.isAnyT10QuesCYes,
                isAnyT10QuesCNo = _checkAppFormInsQuesA2.isAnyT10QuesCNo,
                isAnyT13QuesDYes = _checkAppFormInsQuesA2.isAnyT13QuesDYes,
                isAnyT13QuesDNo = _checkAppFormInsQuesA2.isAnyT13QuesDNo,
                isAnyT14QuesCYes = _checkAppFormInsQuesA2.isAnyT14QuesCYes;

            var objInsured = {};

            objInsured.tabName = _.get(app, "quotation.insureds." + iCid + ".iFullName");
            objInsured.tabSequence = iIndex + 2;

            var mandDocsDocuments = [];
            var optDocsDocuments = [];

            if (_.get(iProfile, 'nationality') === 'N1' || _.get(iProfile, 'prStatus') === 'Y') {
              mandDocsDocuments.push({
                type: 'subSection',
                id: 'iNric',
                title: 'Singapore NRIC /Birth Cert'
              });
            }

            if (_.get(iProfile, 'prStatus') === 'Y' && _.get(iProfile, 'age') <= 15) {
              mandDocsDocuments.push({
                type: 'subSection',
                id: 'iReentryPermit',
                title: 'Re-entry Permit Form 7'
              });
            }

            // 'Below are the conditions for requesting Clients to upload Child Health Booklet:
            // a) Any of the Juvenile questions is answered as YES; OR
            // question a) HEALTH15, b) HEALTH14, c) HEALTH16, d) HEALTH_SHIELD_24
            // b) Life assured =<6 months'
            // tooltip is 'Child Health Booklet is required due to declaration in Juvenile Question in Medical and Health Information.
            // 1.   If only condition (a) is met
            // -   “Child Health Booklet is required due to declaration in Juvenile Question of Medical and Health Information”
            // 2.   If only condition (b) is met
            // -   “Child Health Booklet is required for Life Assured aged 6 months or less”
            // 3.   If both conditions (a) & (b) are met
            // -   “Child Health Booklet is required for Life Assured aged 6 months or less”'
            var isAnyJuvenileYes = _.find(_.at(iAppFormInsurablity, ["HEALTH15", "HEALTH14", "HEALTH16", "HEALTH_SHIELD_24"]), function (value) {
              return value === "Y";
            }) ? true : false;
            var isLAUnder6Months = DateUtils.getAttainedAge(new Date(), new Date(_.get(app, 'quotation.insureds.' + iCid + '.iDob'))).month < 6;
            // let isLAUnder6Months = _.get(iProfile, 'nearAge') == 0 ? true : false;
            if (isAnyJuvenileYes || isLAUnder6Months) {
              var tooltip = void 0;
              if (isLAUnder6Months) {
                tooltip = 'Child Health Booklet is required for Life Assured aged 6 months or less';
              } else {
                tooltip = 'Child Health Booklet is required due to declaration in Juvenile Question of Medical and Health Information';
              }
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iJuvenileQuestions',
                title: 'Copy of latest Child Health Booklet including all assessments done to date',
                toolTips: tooltip
              });
            }

            // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as Yes, or at least 1 of T13 column "Any Existing Medical reports?" selected as Yes'
            if (isAnyT10QuesCYes || isAnyT13QuesDYes) {
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iMedicalReport',
                title: 'Medical Report'
              });
            }

            // add condition as 'If T14 column "Discharge summary  Report" selected as Yes'
            if (isAnyT14QuesCYes) {
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iDischargeSummary',
                title: 'Discharge Summary Report'
              });
            }

            // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as no and at least 1 of T13 column "Any Existing Medical reports?" selected as No'
            if (isAnyT10QuesCNo || isAnyT13QuesDNo) {
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iOtherIllnessQues',
                title: 'Other Illness Questionnaire'
              });
            }

            var subTemplate = {};
            subTemplate.mandDocs = {
              "id": "mandDocs",
              "hasSubLevel": "false",
              "disabled": "false",
              "title": "Mandatory Document",
              "type": "section",
              "items": mandDocsDocuments
            };

            if (optDocsDocuments.length > 0) {
              subTemplate.optDoc = {
                "id": "optDoc",
                "hasSubLevel": "false",
                "disabled": "false",
                "title": "Optional Document",
                "type": "section",
                "items": optDocsDocuments
              };
            }

            subTemplate.otherDoc = {
              "id": "otherDoc",
              "hasSubLevel": "true",
              "disabled": "false",
              "title": "Other Document",
              "type": "section",
              "items": []
            };

            objInsured.tabContent = subTemplate;
            template[iCid] = objInsured;
          });
        }));
      });
      resolve();
    });
  };

  return new Promise(function (resolve) {
    promises.push(getSuppDocsPolicyFormTemplate());

    // Only display if P is LA
    if (_.includes(_.keys(_.get(app, 'quotation.insureds', {})), app.pCid)) {
      promises.push(getSuppDocsProposerTemplate(app));
    }

    promises.push(getSuppDocsInsuredListTemplate(app));

    Promise.all(promises).then(function () {
      resolve(template);
    }).catch(function (error) {
      logger.error('ERROR: _getSupportDocumentsTemplate ' + app.id);
    });
  });
};
module.exports.getSupportDocumentsTemplate = _getSupportDocumentsTemplate;

var _checkMandDocsStatus = function _checkMandDocsStatus(template, values) {
  logger.log('INFO: Shield _checkMandDocsStatus');

  _.forEach(template, function (tabTemplate, tabId) {
    var mandDocsTemplate = _.get(_.find(tabTemplate, function (section) {
      return section.id === 'mandDocs';
    }), 'items');
    var mandDocsValues = _.get(values[tabId], 'mandDocs', {});

    _.forEach(mandDocsTemplate, function (fileTemplate) {
      if (_.isEmpty(_.get(mandDocsValues, _.get(fileTemplate, 'id')))) {
        return false;
      }
    });
  });

  return true;
};

//MOVE to ./common.js
/*
var _deletePendingSubmitList = function(app) {
  logger.log('INFO: closeSuppDocs - _deletePendingSubmitList');

  return new Promise((resolve) => {
    if (_.get(app, 'supportDocuments.pendingSubmitList')) {
      // remove the attachments
      applicationDao.deleteAppAttachments(app.id, _.get(app, 'supportDocuments.pendingSubmitList'), (resp) => {
        if (resp.success) {
          let changedApplication = resp.application;
          // remove pendingSubmitList
          delete changedApplication.supportDocuments['pendingSubmitList'];
          resolve({
            success: true,
            application: changedApplication
          });
        } else {
          resolve({success: false});
        }
      });
    } else {
      resolve({
        success: true,
        application: app
      });
    }
  });
};
*/

var showSupportDocments = function showSupportDocments(data, session, cb) {
  logger.log('INFO: showSupportDocments - start', data.applicationId);
  // manager role include 'manager' and 'director' here
  var agentCode = session.agentCode;
  var defaultDocNameListId = 'suppDocsDefaultDocNames';

  var initViewedList = function initViewedList(app, template) {
    // let diffWithSysDocs = function(app, template){
    var idList = [];

    if (agentCode === app.agentCode) {
      idList = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        idList.push('fnaReport');
      }
      _.each(app.iCids, function (iCid) {
        idList.push('appPdf' + iCid);
      });
    } else {
      idList = _.keys(_.get(app, 'supportDocuments.viewedList.' + app.agentCode, {}));

      var shieldSysDocsIds = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        shieldSysDocsIds.push('fnaReport');
      }
      _.each(app.iCids, function (iCid) {
        shieldSysDocsIds.push('appPdf' + iCid);
      });

      idList = _.union(idList, shieldSysDocsIds);
    }

    if (session.agent.channel.type === "FA") {
      // if in FA Channel, need to add cAck's files into ViewedList, as the documents of FNA in FA Channel
      _.forEach(_.get(app, 'supportDocuments.values.policyForm.mandDocs.cAck', []), function (cAckFile) {
        idList.push(cAckFile.id);
      });
    }
    _.forEach(idList, function (id) {
      if (!_.includes(_.keys(_.get(app, "supportDocuments.viewedList." + agentCode)), id)) {
        _.set(app, "supportDocuments.viewedList." + agentCode + "." + id, false);
      }
    });
  };

  var updateViewedList = function updateViewedList(app, template, eApprovalCase) {
    if (!app.supportDocuments.viewedList[agentCode]) {
      app.supportDocuments.viewedList[agentCode] = {};
      initViewedList(app, template);
      // diffWithSysDocs(app, template);
    } else {
      if (agentCode !== app.agentCode) {
        if (_.get(eApprovalCase, 'approvalStatus') && !_.includes(['A', 'R', 'E'], _.get(eApprovalCase, 'approvalStatus'))) {
          var loginAgentViewedObj = app.supportDocuments.viewedList[agentCode];
          if (_.isEmpty(loginAgentViewedObj)) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - Shield - empty object :', app.id);

            var idList = ['appPdf', 'proposal'];
            _.each(app.iCids, function (iCid) {
              idList.push('appPdf' + iCid);
            });
            if (session.agent.channel.type !== 'FA') {
              idList.push('fnaReport');
            }
            _.each(idList, function (id) {
              loginAgentViewedObj[id] = false;
            });
          } else if (!loginAgentViewedObj.hasOwnProperty('appPdf')) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - Shield - missed eApp : ', app.id);

            var _idList = ['appPdf'];
            _.each(app.iCids, function (iCid) {
              _idList.push('appPdf' + iCid);
            });
            _.each(_idList, function (id) {
              if (!loginAgentViewedObj.hasOwnProperty(id)) {
                loginAgentViewedObj[id] = false;
              }
            });
          }
        }
      }
    }
  };

  var getDefaultDocNameList = function getDefaultDocNameList() {
    return new Promise(function (resolve) {
      dao.getDoc(defaultDocNameListId, function (list) {
        resolve(list);
      });
    });
  };

  var getOtherDocNameList = function getOtherDocNameList(docNamesList, iIsJapaneseAndNotShield, pIsJapaneseAndNotShield) {
    var otherDocNameListId = 'otherDocNames';
    var ikey = iIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    var pkey = pIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    return new Promise(function (resolve) {
      dao.getDoc(otherDocNameListId, function (obj) {
        var otherDocNames = obj ? {
          'la': obj[ikey].la,
          'ph': obj[pkey].ph
        } : { 'la': [], 'ph': [] };
        docNamesList.otherDocNames = otherDocNames;
        resolve(docNamesList);
      });
    });
  };

  applicationDao.getApplication(data.applicationId, function (appDoc) {
    if (!appDoc.error) {
      commonAppHandler.deletePendingSubmitList(appDoc).then(function (resp) {
        var app = resp.application || appDoc;
        var pCid = app.quotation.pCid;
        var iCid = app.quotation.iCid;
        var template = void 0,
            values = void 0;
        var tokensMap = {};
        var approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);

        dao.getDocFromCacheFirst(approvalDocId, function (approvalDoc) {
          commonAppHandler.isSuppDocsReadOnly(app, session.agent, session.agentCode).then(function (isReadOnly) {
            bundleDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then(function (bApp) {
              var tokens = [];
              var suppDocsAppView = {
                id: app.id,
                iCid: app.iCid,
                pCid: app.pCid,
                iFullName: app.quotation.iFullName,
                pFullName: app.quotation.pFullName
              };
              logger.log('isReadOnly :' + isReadOnly);
              _getSupportDocumentsTemplate(app, bApp.appStatus, session.agent, approvalDoc).then(function (template) {
                if (!app.supportDocuments) {
                  app.supportDocuments = {};
                  app.supportDocuments.values = _genSupportDocumentsValues(app);
                  app.supportDocuments.viewedList = {};
                  app.supportDocuments.isAllViewed = false;
                } else {
                  if (!app.supportDocuments.viewedList) {
                    app.supportDocuments.viewedList = {};
                  }
                  if (!app.supportDocuments.values) {
                    app.supportDocuments.values = _genSupportDocumentsValues(app);
                  }

                  //generate token for image in mandDocs & optDoc
                  var now = new Date().getTime();
                  _.forEach(SuppDocsUtils.getNonSysDocs(app, ['application/pdf']), function (doc) {
                    var token = createPdfToken(app._id, doc.id, now, session.loginToken);
                    tokens.push(token);
                    tokensMap[doc.id] = token.token;
                  });
                }
                updateViewedList(app, template, approvalDoc);
                applicationDao.upsertApplication(app._id, app, function (resp) {
                  if (resp) {
                    // Default DOC NAME LIST for checking duplication of document name
                    getDefaultDocNameList().then(function (docNamesList) {
                      //get other doc selction list for Shield,update docNamesList
                      return getOtherDocNameList(docNamesList, false, false);
                    }).then(function (docNamesList) {
                      var supportDocDetails = {
                        otherDocNameList: docNamesList.otherDocNames
                      };
                      if (session.platform === "ios") {
                        logger.log('INFO: getSupportDocuments - end [RETURN=2]', data.applicationId);
                        cb({
                          success: true,
                          template: template,
                          values: app.supportDocuments.values,
                          suppDocsAppView: suppDocsAppView,
                          viewedList: app.supportDocuments.viewedList[agentCode],
                          isReadOnly: isReadOnly,
                          defaultDocNameList: docNamesList.defaultDocumentNames,
                          supportDocDetails: supportDocDetails,
                          tokensMap: tokensMap,
                          application: app
                        });
                      } else {
                        setPdfTokensToRedis(tokens, function () {
                          logger.log('INFO: getSupportDocuments - end [RETURN=2]', data.applicationId);
                          cb({
                            success: true,
                            template: template,
                            values: app.supportDocuments.values,
                            suppDocsAppView: suppDocsAppView,
                            viewedList: app.supportDocuments.viewedList[agentCode],
                            isReadOnly: isReadOnly,
                            defaultDocNameList: docNamesList.defaultDocumentNames,
                            supportDocDetails: supportDocDetails,
                            tokensMap: tokensMap,
                            application: app
                          });
                        });
                      }
                    });
                  } else {
                    logger.error('ERROR: getSupportDocuments - end [RETURN=-3]', data.applicationId);
                    cb({ success: false });
                  }
                });
              });
            });
          });
        });
      });
    } else {
      logger.error('ERROR: getSupportDocuments - end [RETURN=-1]', data.applicationId);
      cb({ success: false });
    }
  });
};

var closeSuppDocs = function closeSuppDocs(data, session, cb) {
  logger.log('INFO: Shield Handler closeSuppDocs - start', data.appId);
  var app = data.appDoc;

  var prepareSubmissionTemplateValues = function prepareSubmissionTemplateValues(currApp) {
    getSubmissionTemplate(currApp, function (tmpl) {
      getSubmissionValues(currApp, session, '', '', function (resValues) {
        logger.log('INFO: closeSuppDocs - end [RETURN=1]', data.appId);
        cb({
          success: true,
          template: tmpl,
          values: resValues.values,
          isMandDocsAllUploaded: mandDocsAllUploaded,
          isSubmitted: false
        });
      });
    });
  };

  var mandDocsAllUploaded = _checkMandDocsStatus(data.template, _.get(app, 'supportDocuments.values', {}));

  commonAppHandler.deletePendingSubmitList(app).then(function (resp) {
    if (resp.success) {
      var application = resp.application;
      application.isMandDocsAllUploaded = mandDocsAllUploaded;
      applicationDao.upsertApplication(application.id, application, function (resp) {
        if (resp) {
          prepareSubmissionTemplateValues(application);
        } else {
          logger.error('ERROR: closeSuppDocs - end [RETURN=-3]', data.appId);
          cb({ success: false });
        }
      });
    } else {
      logger.error('ERROR: closeSuppDocs - end [RETURN=-2]', data.appId);
      cb({ success: false });
    }
  }).catch(function (error) {
    logger.error('ERROR: closeSuppDocs - _deletePendingSubmitList [RETURN=-1]', data.appId, error);
  });
};

module.exports.showSupportDocments = showSupportDocments;
module.exports.closeSuppDocs = closeSuppDocs;