'use strict';

var _ = require('lodash');
var _getOr = require('lodash/fp/getOr');
var moment = require('moment');
var logger = global.logger || console;

var commonApp = require('../common');
var _application = require('./application');
var _signature = require('./signature');
var _payment = require('./payment');
var _submission = require('./submission');
var dao = require('../../../cbDaoFactory').create();
var bDao = require('../../../cbDao/bundle');
var cDao = require('../../../cbDao/client');
var nDao = require('../../../cbDao/needs');
var quotDao = require('../../../cbDao/quotation');
var appDao = require('../../../cbDao/application');

var _getStepperSections = function _getStepperSections() {
  return {
    items: [{
      'id': 'stepApp',
      'type': 'section',
      'title': 'Application',
      'detailSeq': 1,
      'items': [{
        'id': 'app',
        'type': 'shieldAppForm'
      }]
    }, {
      'id': 'stepSign',
      'type': 'section',
      'title': 'Signature',
      'detailSeq': 2,
      'items': [{
        'id': 'sign',
        'type': 'shieldSignature'
      }]
    }, {
      'id': 'stepPay',
      'type': 'section',
      'title': 'Payment',
      'detailSeq': 3,
      'items': [{
        'id': 'pay',
        'type': 'shieldPayment'
      }]
    }, {
      'id': 'stepSubmit',
      'type': 'section',
      'title': 'Submit',
      'detailSeq': 4,
      'items': [{
        'id': 'submit',
        'type': 'shieldSubmission'
      }]
    }]
  };
};

var _genParentApplicationDoc = function _genParentApplicationDoc(session, quotation, bundle, sections, template) {
  var now = moment().toISOString();
  var quotationId = _.get(quotation, 'id');
  var pCid = _.get(quotation, 'pCid');
  var insureds = _.keys(quotation.insureds);
  var iCids = [];
  var isPhSameAsLa = false;
  var ccy = '';
  _.forEach(insureds, function (iCid) {
    if (pCid === iCid) {
      isPhSameAsLa = true;
    } else {
      iCids.push(iCid);
    }
    var subQuotation = quotation.insureds[iCid];
    if (ccy.length === 0) {
      ccy = subQuotation.ccy;
    }
  });

  var iCidMapping = {};
  _.forEach(iCids, function (iCid) {
    iCidMapping[iCid] = [];
  });
  if (isPhSameAsLa) {
    iCidMapping[pCid] = [];
  }

  var parentApplication = Object.assign({
    //Parent only
    id: '',
    type: 'masterApplication',
    childIds: [],
    iCids: iCids,
    iCidMapping: iCidMapping,
    appStep: 0,
    isPolicyNumberGenerated: false,
    isSubmittedStatus: false,
    isFailSubmit: false,
    isProposalSigned: false,
    isAppFormProposerSigned: false,
    isAppFormInsuredSigned: _.times(iCids.length, _.constant(false)),
    isStartSignature: false,
    isFullySigned: false,
    isMandDocsAllUploaded: false,
    signExpiryShown: false,
    applicationSubmittedDate: 0,
    applicationInforceDate: 0,
    quotation: quotation,
    payment: {},
    submission: {}
  }, {
    //Common
    bundleId: _.get(bundle, 'id'),
    pCid: _.get(quotation, 'pCid'),
    applicationForm: {
      values: {}
    },
    biSignedDate: 0,
    applicationSignedDate: 0,
    supportDocuments: {},
    isValid: true,
    isCrossAge: false,
    applicationStartedDate: now,
    lastUpdateDate: now,
    createDate: now,
    compCode: session.agent.compCode,
    dealerGroup: session.agent.channel.code,
    agentCode: session.agent.agentCode,
    agent: quotation.agent,
    quotationDocId: quotationId
  });

  var channel = session.agent ? session.agent.channel.code : '';
  var formValues = parentApplication.applicationForm.values;
  var oneyearpremium = quotation.premium;
  var payMode = quotation.paymentMode;
  switch (payMode) {
    case 'S':
      oneyearpremium = quotation.premium * 2;
      break;
    case 'Q':
      oneyearpremium = quotation.premium * 4;
      break;
    case 'M':
      oneyearpremium = quotation.premium * 12;
      break;
    default:
      oneyearpremium = quotation.premium;
  }
  var extra = {
    isPhSameAsLa: isPhSameAsLa ? 'Y' : 'N',
    ccy: ccy,
    channel: commonApp.getChannelType(channel),
    channelName: channel,
    premium: oneyearpremium
  };

  var laProfilePromise = [];

  _.forEach(iCids, function (iCid) {
    if (!isPhSameAsLa || iCid !== pCid) {
      laProfilePromise.push(new Promise(function (resolve, reject) {
        //getLAProfile
        cDao.getClientById(iCid, function (laDoc) {
          if (laDoc && !laDoc.error) {
            laDoc.cid = laDoc._id;
            var laValue = {
              personalInfo: laDoc,
              declaration: {},
              policies: {
                ROP_01: ''
              },
              extra: Object.assign({}, extra, { isCompleted: false }),
              insurability: {
                'LIFESTYLE01': laDoc.isSmoker
              }
            };

            //handleLaRop
            if (extra.channel !== 'FA') {
              var ccRopId = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.iCidRopAnswerMap.' + iCid);
              laValue.policies.ROP_01 = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.' + ccRopId);
            }
            resolve(laValue);
          } else {
            reject();
          }
        });
      }));
    }
  });

  return Promise.all(laProfilePromise).then(function (laValues) {
    formValues.insured = laValues;

    //getPHProfile
    return new Promise(function (resolve) {
      cDao.getClientById(parentApplication.pCid, function (phDoc) {
        var hasTrustedIndividual = 'N';
        dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], function (pdaDoc) {
          if (pdaDoc && !pdaDoc.error) {
            logger.log('INFO: genApplicationDoc - getPHProfile - populate PDA data', quotationId);
            hasTrustedIndividual = _.get(pdaDoc, 'trustedIndividual', 'N');
          }

          commonApp.removeInvalidProfileProp(phDoc);
          phDoc.cid = phDoc._id || phDoc.id;

          //populate personal info
          formValues.proposer = {
            personalInfo: phDoc,
            declaration: {
              trustedIndividuals: hasTrustedIndividual === 'Y' ? phDoc.trustedIndividuals : {},
              ccy: quotation.ccy
            },
            policies: isPhSameAsLa ? { ROP_01: '' } : {},
            extra: Object.assign({}, extra, {
              hasTrustedIndividual: hasTrustedIndividual
            }),
            insurability: {
              'LIFESTYLE01': phDoc.isSmoker
            }
          };

          //handlePhRop
          if (extra.channel !== 'FA') {
            var ccRopId = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.iCidRopAnswerMap.' + pCid);
            formValues.proposer.policies.ROP_01 = _.get(quotation, 'clientChoice.recommendation.rop_shield.ropBlock.' + ccRopId);
          }
          resolve();
        });
      });
    });
  }).then(function () {
    //setExtraProp
    var phDependants = formValues.proposer.personalInfo.dependants;

    //set relations
    _.forEach(phDependants, function (dep) {
      var insured = _.filter(formValues.insured, function (la) {
        return la.personalInfo.cid === dep.cid;
      });

      if (_.get(insured, '[0].personalInfo')) {
        insured[0].personalInfo.relationship = dep.relationship;
        insured[0].personalInfo.relationshipOther = dep.relationshipOther;
      }
    });
  }).then(function (res) {
    //handlePlans
    logger.log('INFO: genApplicationDoc - handlePlans', quotationId);

    var quotationInsuredArray = _.get(quotation, 'insureds');
    var formValuesInsuredArray = _.get(formValues, 'insured') || {};
    var quotationCids = Object.keys(quotationInsuredArray);

    var formValuesIndexCidMapping = {};

    formValuesInsuredArray.forEach(function (insured, index) {
      var formValuesCid = _.get(insured, 'personalInfo.cid');
      formValuesIndexCidMapping[formValuesCid] = index;
    });

    var planDetails = void 0,
        groupedPlanDetails = void 0;
    _.each(quotationCids, function (cid, key) {
      planDetails = commonApp.transformQuotationToPlanDetails(quotation, cid, false);
      groupedPlanDetails = commonApp.transformQuotationToPlanDetails(quotation, cid, true);
      if (_.get(quotation, 'pCid') === cid) {
        formValues.proposer.planDetails = planDetails;
        formValues.proposer.groupedPlanDetails = groupedPlanDetails;
      } else if (_.isNumber(formValuesIndexCidMapping[cid]) && formValues.insured[formValuesIndexCidMapping[cid]]) {
        formValues.insured[formValuesIndexCidMapping[cid]].planDetails = planDetails;
        formValues.insured[formValuesIndexCidMapping[cid]].groupedPlanDetails = groupedPlanDetails;
      }
    });

    return new Promise(function (resolve) {
      dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (mapping) {
        // set extra value such as channel
        formValues.appFormTemplate = mapping[quotation.baseProductCode].appFormTemplate;
        var app = parentApplication;

        commonApp.getOptionsList(app, template, function () {
          //Remove generated values which is not found in template
          var appTemplate = template.items[0].items[0]; //contain items of menuSection
          var appValuesOrg = app.applicationForm.values;
          var appValuesTemp = _.cloneDeep(app.applicationForm.values);
          var appValuesNew = _.cloneDeep(app.applicationForm.values);
          var trigger = {};

          //for create trigger object using template
          logger.log('INFO: _genParentApplicationDoc - replaceAppFormValuesByTemplate start', quotationId);
          commonApp.replaceAppFormValuesByTemplate(appTemplate, appValuesOrg, appValuesTemp, trigger, [], 'en', null);
          logger.log('INFO: _genParentApplicationDoc - replaceAppFormValuesByTemplate end', quotationId);

          //remove appForm values which is not found in template + trigger
          for (var key in appValuesOrg) {
            if (key === 'proposer') {
              logger.log('INFO: _genParentApplicationDoc - removeAppFormValuesByTrigger: handle', key, quotationId);
              commonApp.removeAppFormValuesByTrigger(key, appValuesOrg[key], appValuesNew[key], appValuesOrg[key], trigger[key], false, appValuesOrg);
            } else if (key === 'insured') {
              for (var i = 0; i < appValuesOrg[key].length; i++) {
                var iOrgValues = appValuesOrg[key][i];
                var iNewValues = appValuesNew[key][i];
                var iTrigger = trigger[key][i];
                logger.log('INFO: _genParentApplicationDoc - removeAppFormValuesByTrigger: handle', key, i, quotationId);
                commonApp.removeAppFormValuesByTrigger(key, iOrgValues, iNewValues, iOrgValues, iTrigger, false, appValuesOrg);
              }
            }
          }

          app.applicationForm.values = appValuesNew;

          logger.log('INFO: genApplicationDoc - end', quotationId);
          resolve(app);
        });
      });
    });
  });
};

var _genChildApplicationDocs = function _genChildApplicationDocs(session, pApplication, quotation, bundle, sections, template) {
  var now = moment().toISOString();

  var childApplicationDefault = Object.assign({
    id: '',
    type: 'application',
    parentId: '',
    parentAttachmentIds: [],
    iCid: '',
    policyNumber: '',
    isApplicationSigned: false,
    isMandDocsUploaded: false,
    isBackDate: false,
    appFormAttachmentId: ''
  }, {
    //Common
    bundleId: _.get(bundle, 'id'),
    pCid: pApplication.pCid,
    applicationForm: {
      values: {
        proposer: {},
        insured: []
      }
    },
    quotation: {},
    isValid: true,
    isCrossAge: false,
    supportDocuments: {},
    payment: {},
    biSignedDate: 0,
    applicationSignedDate: 0,
    applicationStartedDate: now,
    lastUpdateDate: now,
    createDate: now,
    compCode: session.agent.compCode,
    dealerGroup: session.agent.channel.code,
    agentCode: session.agent.agentCode,
    agent: quotation.agent,
    quotationDocId: quotation.id
  });

  var childApplications = [];
  var cidsOfLA = [];

  if (_.get(pApplication, 'applicationForm.values.proposer.extra.isPhSameAsLa') === 'Y') {
    //Split quotation to 2 applications
    cidsOfLA.push(pApplication.pCid);
  }

  cidsOfLA = _.union(cidsOfLA, pApplication.iCids);

  _.forEach(cidsOfLA, function (iCid) {
    //Split quotation to 2 applications
    var subQuotPlans = _getOr([], 'insureds.' + iCid + '.plans', quotation);
    var needToSplit = _.filter(subQuotPlans, function (obj) {
      return obj.covCode !== 'ASIM';
    }).length > 0;

    var childApp = _.cloneDeep(childApplicationDefault);
    childApp.iCid = iCid;

    //child quotation with specific iCid
    var childQuot = _.cloneDeep(quotation);
    childQuot.insureds = _.cloneDeep(_.get(quotation, 'insureds[' + iCid + ']', {}));

    childApp.quotation = childQuot;
    childApp.applicationForm.values.planDetails = commonApp.transformQuotationToPlanDetails(quotation, iCid, false, true);
    childApplications.push(childApp);

    if (needToSplit) {
      childApp = _.cloneDeep(childApplicationDefault);
      childApp.iCid = iCid;
      childApp.quotation = childQuot;
      childApp.applicationForm.values.planDetails = commonApp.transformQuotationToPlanDetails(quotation, iCid, false, false);
      childApplications.push(childApp);
    }
  });

  _application.saveParentApplicationValuesToChild(pApplication, childApplications);

  return [pApplication].concat(childApplications);
};

//=ApplicationHandler.apply
module.exports.applyAppForm = function (data, session, callback) {
  var isFaChannel = session.agent.channel.type === 'FA';
  var quotId = data.quotId;

  var result = {
    success: false,
    stepper: {
      sections: _getStepperSections(),
      index: {
        current: 0,
        completed: -1,
        active: 0
      },
      enableNextStep: false
    },
    template: {},
    application: {}
  };

  var cache = {
    bundle: {},
    quotation: {},
    profile: {},
    template: {}
  };

  logger.log('INFO: applyAppForm - start', quotId);
  if (!quotId) {
    logger.log('INFO: applyAppForm - end [RETURN=-100]', quotId);
    // callback({success: false, error:'quotId null'});
    return;
  }

  new Promise(function (resolve, reject) {
    quotDao.getQuotation(quotId, function (quot) {
      if (quot && !quot.error) {
        cache.quotation = quot;
        resolve(quot);
      } else {
        reject(new Error('Fail to get Quotation ' + _.get(quot, 'error')));
      }
    });
  }).then(function (quotation) {
    return bDao.getCurrentBundle(_.get(quotation, 'pCid')).then(function (bundle) {
      if (bundle && !bundle.error) {
        cache.bundle = bundle;
        //getOptionsList will be run in below step
        return _application.getAppFormTemplate(quotation).then(function (templateRes) {
          if (bundle.isFnaReportSigned) {
            logger.log('INFO: applyAppForm - frozenAppFormTemplateFnaQuestions', quotId);
            commonApp.frozenAppFormTemplateFnaQuestions(templateRes.template);
          }
          return templateRes;
        });
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function (templateRes) {
    return _genParentApplicationDoc(session, cache.quotation, cache.bundle, templateRes.sections, templateRes.template).then(function (pApplication) {
      return commonApp.populateFromBundle(pApplication, templateRes.sections, isFaChannel).then(function (pApplication) {
        return new Promise(function (resolve) {
          commonApp.getOptionsList(pApplication, templateRes.template, function () {
            result.template = templateRes.template;
            result.application = pApplication;
            resolve(pApplication);
          });
        });
      });
    }).then(function (pApplication) {
      return _genChildApplicationDocs(session, pApplication, cache.quotation, cache.bundle, templateRes.sections, templateRes.template);
    });
  }).then(function (applications) {
    if (applications.length === 0) {
      throw new Error('Fail to Generate Parent and Child Doc ' + quotId);
    }

    return new Promise(function (resolve) {
      commonApp.getShieldApplicationIds(session, applications.length).then(function (ids) {
        _.forEach(ids, function (id, i) {
          logger.log('INFO: applyAppForm - getShieldApplicationIds', quotId, '->', id);
          applications[i].id = id;
        });

        resolve(applications);
      });
    }).then(function (apps) {
      var parentApplications = _.filter(apps, function (app) {
        return app.hasOwnProperty('childIds');
      });
      var childApplications = _.filter(apps, function (app) {
        return app.hasOwnProperty('parentId');
      });
      var parentApplication = parentApplications[0];

      var parentAppId = parentApplication.id;
      var clientAppIds = childApplications.map(function (app) {
        var mapping = _.get(parentApplication, 'iCidMapping[' + app.iCid + ']');
        if (mapping instanceof Array) {
          var isAXAShield = _.filter(_.get(app, 'applicationForm.values.planDetails.planList'), function (obj) {
            return obj.covCode !== 'ASIM';
          }).length > 0 ? false : true;
          mapping.push({
            applicationId: app.id,
            policyNumber: '',
            covCode: isAXAShield ? 'ASIM' : 'ASP'
          });
        }
        return app.id;
      });

      parentApplication.childIds = clientAppIds;
      _.forEach(childApplications, function (app) {
        app.parentId = parentAppId;
        if (app.iCid === app.pCid) {
          app.appFormAttachmentId = 'appPdf';
        } else {
          app.appFormAttachmentId = 'appPdf' + app.iCid;
        }
      });

      return parentApplications.concat(childApplications);
    });
  }).then(function (applications) {
    var updAppPromises = applications.map(function (app) {
      return new Promise(function (resolve, reject) {
        appDao.updApplication(app.id, app, function (res) {
          if (res && !res.error) {
            resolve(res);
          } else {
            reject(_.get(res, 'error'));
          }
        });
      });
    });
    return Promise.all(updAppPromises);
  }).then(function (res) {
    dao.updateViewIndex('main', 'summaryApps');
    dao.updateViewIndex('main', 'quotationByAgent');
    return bDao.onCreateApplication(_.get(cache, 'quotation.pCid'), _.get(cache, 'quotation.id'), result.application.id);
  }).then(function (upRes) {
    logger.log('INFO: applyAppForm - update profile application count', quotId, upRes);
    if (upRes && !upRes.error) {
      return cDao.getProfile(_.get(cache, 'quotation.pCid'), true).then(function (profile) {
        cache.profile = profile;
        return bDao.getApplyingApplicationsCount(result.application.bundleId);
      }).then(function (count) {
        var pCid = _.get(cache, 'quotation.pCid');
        cache.profile.applicationCount = count;
        return cDao.updateProfile(pCid, cache.profile);
      });
    } else {
      throw new Error('Fail to update Bundle ' + _.get(cache.bundle, 'id') + ' ' + _.get(upRes, 'error'));
    }
  }).then(function (res) {
    if (res && !res.error) {
      dao.updateViewIndex('main', 'contacts');
      result.application.agentChannelType = isFaChannel ? 'Y' : 'N';

      result.success = true;

      logger.log('INFO: applyAppForm - end [RETURN=100]', quotId);
      callback(Object.assign(result, { success: true }));
    } else {
      throw new Error('Fail to update Profile ' + _.get(cache.profile, 'id') + ' ' + _.get(res, 'error'));
    }
  }).catch(function (error) {
    logger.error('ERROR: applyAppForm - end [RETURN=-101]', quotId, error);
    // callback({success: false});
  });
};

var _calcApplicationActiveStep = function _calcApplicationActiveStep(app) {
  var appActiveStep = commonApp.EAPP_STEP.APPLICATION;

  if (_.get(app, 'applicationForm.values.proposer.extra.isCompleted', false)) {
    //Check step application
    appActiveStep++;
  }

  if (appActiveStep === commonApp.EAPP_STEP.SIGNATURE && app.isAppFormProposerSigned) {
    //Check step signature
    appActiveStep++;
  }

  if (appActiveStep === commonApp.EAPP_STEP.PAYMENT && _.get(app, 'appCompletedStep', -1) === commonApp.EAPP_STEP.PAYMENT) {
    //Check step payment
    appActiveStep++;
  }

  return appActiveStep;
};

//=ApplicationHandler.goApplication
module.exports.continueApplication = function (data, session, callback) {
  var isFaChannel = session.agent.channel.type === 'FA';
  var appId = data.appId;

  var result = {
    success: false,
    warningMsg: {
      msgCode: 0,
      showSignExpiryWarning: false
    },
    stepper: {
      sections: _getStepperSections(),
      index: {
        current: 0,
        completed: -1,
        active: 0
      },
      enableNextStep: false
    },
    template: {},
    application: {},
    signature: {}
  };
  // let crossAgeData = {appId: appId};

  logger.log('INFO: continueApplication - start', appId);
  if (!appId) {
    logger.log('INFO: continueApplication - end [RETURN=-100]', appId);
    // callback({success: false, error:'appId null'});
    return;
  }

  _application.getApplication(appId).then(function (app) {
    result.application = app;

    var appStep = _.get(app, 'appStep', commonApp.EAPP_STEP.APPLICATION);
    var appCompletedStep = _.get(app, 'appCompletedStep', commonApp.EAPP_STEP.APPLICATION - 1);

    result.stepper.index.current = _.min([appCompletedStep + 1, appStep]);
    result.stepper.index.active = _.max([_calcApplicationActiveStep(app), appStep]);
    result.stepper.index.completed = appCompletedStep;

    return bDao.getCurrentBundle(_.get(app, 'pCid'));
  }).then(function (bundle) {
    var continueAppStep = result.stepper.index.current;

    var initProcess = [];
    if (continueAppStep === commonApp.EAPP_STEP.APPLICATION) {
      initProcess.push(_application.getAppFormTemplateWithOptionList(session, result.application).then(function (templateRes) {
        if (bundle.isFnaReportSigned) {
          logger.log('INFO: continueApplication - frozenAppFormTemplateFnaQuestions', appId);
          commonApp.frozenAppFormTemplateFnaQuestions(templateRes.template);
        }
        return Object.assign(templateRes, { success: true });
      }));
    } else if (continueAppStep === commonApp.EAPP_STEP.SIGNATURE) {
      if (session.platform) {
        initProcess.push(_signature.getSignatureInitPdfString(session, appId));
      } else {
        initProcess.push(_signature.getSignatureInitUrl(session, appId));
      }
    } else if (continueAppStep === commonApp.EAPP_STEP.PAYMENT) {
      initProcess.push(_payment.getPaymentTemplate(session, false, _.get(result.application, 'payment.totCashPortion', 0), isFaChannel));
    } else {
      initProcess.push(_submission.getSubmissionTemplate(session, false));
    }
    return Promise.all(initProcess);
  }).then(function (initResults) {
    var continueAppStep = result.stepper.index.current;

    logger.log('INFO: continueApplication - initProcess completed', appId);
    if (initResults.length > 0 && !initResults[0].success) {
      throw new Error('Fail to init step ' + continueAppStep);
    }
    var initResult = initResults[0];

    if (continueAppStep === commonApp.EAPP_STEP.SIGNATURE) {
      result.signature = initResult.signature;
      result.warningMsg = Object.assign({}, initResult.warningMsg, { showSignExpiryWarning: false });
    } else {
      result.template = initResult.template;
    }

    if (continueAppStep !== commonApp.EAPP_STEP.APPLICATION) {
      return;
    }

    logger.log('INFO: continueApplication - process for step', continueAppStep, appId);
    return new Promise(function (resolve) {
      if (initResult.sections) {
        return commonApp.populateFromBundle(result.application, initResult.sections, isFaChannel).then(function (fvApp) {
          result.application = fvApp;
          resolve(true);
        });
      } else {
        resolve(false);
      }
    }).then(function (isGetPolicyNumber) {
      if (isGetPolicyNumber) {
        //getPNumber
        return new Promise(function (resolve) {
          var values = result.application.applicationForm.values;

          logger.log('INFO: continueApplication - getPolicyNumber isGen=', result.application.isPolicyNumberGenerated, 'for', appId);
          if (result.application.isPolicyNumberGenerated) {
            resolve();
          } else {
            //determinate if next menu is insurability
            var menus = values.menus || [];
            var checkedMenu = values.checkedMenu || [];
            var completedMenus = values.completedMenus || [];
            var targetMenu = 'menu_insure';
            var toGetPolicyNumber = false;

            _.forEach(menus, function (menu, index) {
              if (checkedMenu.indexOf(menu) === -1 || completedMenus.indexOf(menu) === -1) {
                if (menu === targetMenu) {
                  toGetPolicyNumber = true;
                }
                return false;
              }
            });

            logger.log('INFO: continueApplication - getPolicyNumber toGet=', toGetPolicyNumber, 'for', appId);
            if (toGetPolicyNumber) {
              _application.getPolicyNumbers(result.application).then(function (apps) {
                result.application = apps.parentApp;

                var upChildPromise = apps.childApps.map(function (childApp) {
                  return new Promise(function (upResolve, upReject) {
                    childApp.lastUpdateDate = moment().toISOString();
                    appDao.updApplication(childApp.id, childApp, function (res) {
                      if (res && !res.error) {
                        upResolve(res);
                      } else {
                        upReject(new Error('Fail to update application ' + childApp.id + ' ' + _.get(res, 'error')));
                      }
                    });
                  });
                });

                return Promise.all(upChildPromise).then(function (upResults) {
                  resolve();
                });
              });
            } else {
              resolve();
            }
          }
        });
      } else {
        return;
      }
    });
  }).then(function () {
    //callBack
    logger.log('INFO: continueApplication - callBack', appId);

    // delete appPdf if the values changed
    var signExpireWarningDay = global.config.signExpireWarningDay;
    logger.debug('DEBUG: check signature expiry for showing warning: ', result.application.biSignedDate, result.application.appStatus, result.application.signExpiryShown, appId);
    // check signature expiry for showing warning
    if (result.application.biSignedDate && !result.application.isSubmittedStatus && !result.application.signExpiryShown) {
      var expiryTime = moment().subtract(signExpireWarningDay, 'days');
      var biSignDate = moment(result.application.biSignedDate);
      if (biSignDate.valueOf() < expiryTime.valueOf()) {
        result.warningMsg.showSignExpiryWarning = true;
      }
    }

    return _application.checkCrossAge(result.application).then(function (caResp) {
      if (caResp.success) {
        result.crossAge = caResp;
        if (caResp.status === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED) {
          // result.warningMsg.showCrossedAgeSignedMsg = true;
          result.warningMsg.msgCode = caResp.status;
        }
        if (caResp.crossedAge) {
          result.application.isCrossAge = true;
        }

        if (_.get(caResp, 'crossedAgeCid.length', 0) > 0) {
          return caResp.crossedAgeCid;
        }
      }
      return [];
    }).then(function (crossedAgeCid) {
      if (crossedAgeCid.length === 0) {
        return;
      }
      logger.log('INFO: continueApplication - checkCrossAge', appId, crossedAgeCid);

      var childIds = [];
      _.forEach(crossedAgeCid, function (iCid) {
        var mappingList = result.application.iCidMapping[iCid];
        _.forEach(mappingList, function (mapping) {
          childIds.push(mapping.applicationId);
        });
      });

      return _application.getChildApplications(childIds).then(function (childApps) {
        return Promise.all(childApps.map(function (childApp) {
          childApp.isCrossAge = true;
          childApp.lastUpdateDate = moment().toISOString();

          return new Promise(function (resolve, reject) {
            appDao.upsertApplication(childApp._id, childApp, function (resp) {
              if (resp && !resp.error) {
                resolve();
              } else {
                reject(new Error('Fail to update Application ' + childApp._id + ' ' + _.get(resp, 'error')));
              }
            });
          });
        }));
      }).then(function (upResults) {
        return;
      });
    });
  }).then(function () {
    result.application.lastUpdateDate = moment().toISOString();

    appDao.upsertApplication(result.application.id, result.application, function (resp) {
      if (resp && !resp.error) {
        result.application._rev = resp.rev;
        return;
      } else {
        throw new Error('Fail to update Application ' + result.application.id + ' ' + _.get(resp, 'error'));
      }
    });
  }).then(function () {
    result.application.agentChannelType = isFaChannel ? 'Y' : 'N';
    result.success = true;

    logger.log('INFO: continueApplication - end [RETURN=100]', appId);
    callback(result);
  }).catch(function (error) {
    logger.error('ERROR: continueApplication - end [RETURN=-101]', appId, error);
    // callback({success: false});
  });
};