'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var commonApp = require('../common');
var dao = require('../../../cbDaoFactory').create();
var _ = require('lodash');
var _application = require('./application');
var _signature = require('./signature');
var async = require('async');
var logger = global.logger || console;
var appDao = require('../../../cbDao/application');
var bundleDao = require('../../../cbDao/bundle');
var approvalHandler = require('../../../ApprovalHandler');
var aEmaillHandler = require('../../../ApprovalNotificationHandler');
var PDFHandler = require('../../../PDFHandler');
var commonFunctions = require('../../../CommonFunctions');
var utils = require('../../../Quote/utils');

var _getSubmissionTemplate = function _getSubmissionTemplate(session, frozen, callback) {
  var result = {
    success: false,
    template: {}
  };

  return new Promise(function (resolve, reject) {
    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.SUBMISSION_SHIELD, function (tmpl) {
      if (tmpl && !tmpl.error) {
        result.success = true;
        if (frozen) {
          if (session.platform) {
            commonApp.frozenTemplateMobile(tmpl);
          } else {
            commonApp.frozenTemplate(tmpl);
          }
        }
        result.template = tmpl;
        resolve(result);
      } else {
        reject(new Error('Fail to get payment template ' + _.get(tmpl, 'error')));
      }
    });
  }).catch(function (error) {
    logger.error('ERROR in _getSubmissionTemplate ' + error);
  });
};

var _prepareSubmission = function _prepareSubmission(session, data) {
  var appId = data.appId;


  return _application.getApplication(appId).then(function (app) {
    return new Promise(function (resolve, reject) {
      async.waterfall([function (callback) {
        if (app.appStep < commonApp.EAPP_STEP.SUBMISSION) {
          app.appStep = commonApp.EAPP_STEP.SUBMISSION;
          app.agentChannelType = session.agent.channel.type === 'FA' ? 'Y' : 'N';

          appDao.upsertApplication(app._id, app, function (res) {
            if (res && !res.error) {
              app._rev = res.rev;
              callback(null, app);
            } else {
              callback('Fail to Update application');
            }
          });
        } else {
          callback(null, app);
        }
      }], function (err, callbackApplication) {
        if (err) {
          logger.error('ERROR: _prepareSubmission: ' + appId + ' ' + err);
          resolve({ success: false });
        } else {
          resolve(callbackApplication);
        }
      });
    }).catch(function (error) {
      logger.error('ERROR: _prepareSubmission: ' + appId + ' caught ' + error);
    });
  });
};

var _saveSubmissionValuesPromise = function _saveSubmissionValuesPromise(data, session) {
  return new Promise(function (resolve, reject) {
    _saveSubmissionValues(data, session, function (result) {
      if (result.success) {
        resolve(result);
      } else {
        resolve(undefined);
      }
    });
  }).catch(function (error) {
    logger.error('ERROR in _saveSubmissionValuesPromise ' + error);
  });
};
module.exports.saveSubmissionValuesPromise = _saveSubmissionValuesPromise;

var _saveSubmissionValues = function _saveSubmissionValues(data, session, cb) {
  var appId = data.appId,
      changedValues = data.changedValues;

  async.waterfall([function (callback) {
    _application.getApplication(appId).then(function (app) {
      callback(null, app);
    }).catch(function (err) {
      callback(err);
    });
  }, function (application, callback) {
    application.submission = _.get(changedValues, 'submission') || {};
    commonApp.updateAndReturnApp(application, callback);
  }], function (err, application) {
    if (err) {
      logger.error('ERROR in _saveSubmissionValues, ' + err);
      cb({ success: false, application: {} });
    } else {
      cb({ success: true, application: application });
    }
  });
};
module.exports.saveSubmissionValues = _saveSubmissionValues;

var _saveParentSubmissionValuestoChild = function _saveParentSubmissionValuestoChild(parentApplication, updateObj, callback) {
  var childIds = _.get(parentApplication, 'childIds');
  _application.getChildApplications(childIds).then(function (childApplications) {
    var updAppPromises = childApplications.map(function (app) {
      app = _.assignIn(app, updateObj);

      // app.payment.trxStatus = _.get(parentApplication, 'payment.trxStatus', '');
      // app.payment.trxRemark = _.get(parentApplication, 'payment.trxRemark', '');
      // app.payment.trxAmount = _.get(parentApplication, 'payment.trxAmount', '');
      // app.payment.trxCcy = _.get(parentApplication, 'payment.trxCcy', '');
      // app.payment.trxTime = _.get(parentApplication, 'payment.trxTime', '');
      // app.payment.trxPayType = _.get(parentApplication, 'payment.trxPayType', '');
      // app.payment.receiptNo = _.get(parentApplication, 'payment.receiptNo', '');
      // app.payment.trxStatusRemark = _.get(parentApplication, 'payment.trxStatusRemark', '');
      // app.payment.trxEnquiryStatus = _.get(parentApplication, 'payment.trxEnquiryStatus', '');
      // app.payment.isInitialPaymentCompleted = _.get(parentApplication, 'payment.isInitialPaymentCompleted', '');

      // // Change the trxAmount from total in Parent Application to child Cash portion
      // app.payment.trxAmount = app.payment.cashPortion;
      return new Promise(function (resolve, reject) {
        appDao.updApplication(app.id, app, function (res) {
          if (res && !res.error) {
            resolve(res);
          } else {
            reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
          }
        });
      });
    });

    return Promise.all(updAppPromises).then(function (results) {
      callback(null, parentApplication);
    }, function (rejectReason) {
      callback('ERROR in _saveParentPaymenttoChild with Reject Reason,  ' + rejectReason);
    }).catch(function (error) {
      logger.error('ERROR in _saveParentPaymenttoChild ' + error);
      callback('ERROR in _saveParentPaymenttoChild, ' + error);
    });
  }).catch(function (error) {
    logger.error('ERROR in _saveParentPaymenttoChild: ' + error);
  });
};

//=ApplicationHandler.genAppFormPdfByPaymentData
var _genAppFormPdfPaymentPageByData = function _genAppFormPdfPaymentPageByData(app, lang, callback) {
  var trigger = {};
  var paymentValues = _.cloneDeep(app.payment);
  var ccy = _.get(app, 'applicationForm.values.proposer.extra.ccy');

  dao.getDoc(commonApp.TEMPLATE_NAME.PAYMENT_SHIELD, function (paymentTmpl) {
    logger.log('INFO: _genAppFormPdfPaymentPageByData - replaceAppFormValuesByTemplate start', app._id);
    commonApp.replaceAppFormValuesByTemplate(paymentTmpl, app.payment, paymentValues, trigger, [], lang, ccy);
    logger.log('INFO: _genAppFormPdfPaymentPageByData - replaceAppFormValuesByTemplate end', app._id);

    var ccySign = utils.getCurrencySign(ccy);
    if (paymentValues.totCPFPortion > 0) {
      paymentValues.totCPFPortion = utils.getCurrency(paymentValues.totCPFPortion, ccySign, 2);
    }
    if (paymentValues.totCashPortion > 0) {
      paymentValues.totCashPortion = utils.getCurrency(paymentValues.totCashPortion, ccySign, 2);
    }
    if (paymentValues.totMedisave > 0) {
      paymentValues.totMedisave = utils.getCurrency(paymentValues.totMedisave, ccySign, 2);
    }

    var reportData = {
      root: {
        reportData: paymentValues,
        originalData: app.payment
      }
    };

    reportData.root.reportData.pCid = app.pCid;

    new Promise(function (resolve, reject) {
      dao.getDoc(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (mapping) {
        if (mapping && !mapping.error && mapping[app.quotation.baseProductCode]) {
          resolve(mapping[app.quotation.baseProductCode].paymentTemplate);
        } else {
          reject(new Error('Fail to get ' + commonApp.TEMPLATE_NAME.APP_FORM_MAPPING));
        }
      });
    }).then(function (paymentTemplate) {
      var tplFiles = [];
      commonApp.getReportTemplates(tplFiles, [paymentTemplate.main].concat(_toConsumableArray(paymentTemplate.template)), 0, function () {
        if (tplFiles.length) {
          PDFHandler.getPremiumPaymentPdf(reportData, tplFiles, lang, function (pdfStr) {
            callback(pdfStr);
          });
        } else {
          logger.error('ERROR: _genAppFormPdfPaymentPageByData - end [RETURN=-101]', app._id);
          callback(false);
        }
      });
    }).catch(function (error) {
      logger.error('ERROR: _genAppFormPdfPaymentPageByData - end [RETURN=-100]', app._id, error);
      callback(false);
    });
  });
};

var _preparePaymentPDf = function _preparePaymentPDf(app, lang, callback) {
  _genAppFormPdfPaymentPageByData(app, lang, function (pdfStr) {
    if (pdfStr && pdfStr.length > 0) {
      callback(null, pdfStr);
    } else {
      callback('Fail to generate Payment page for App Form');
    }
  });
};

var _mergeAppFormPdf = function _mergeAppFormPdf(app, pdfStr, appformPdfStrs, callback) {
  logger.log('INFO: _mergeAppFormPdf - start', app._id);
  async.waterfall([function (cb) {
    // let getPdfPromise = [];
    // getPdfPromise.push(new Promise((resolve, reject) => {
    //   appDao.getAppAttachment(app._id, commonApp.PDF_NAME.eAPP, (appPdf) => {
    //     if (appPdf.success) {
    //       resolve(appPdf);
    //     } else {
    //       reject(new Error('Fail to get attachment for proposer'));
    //     }
    //   });
    // }));
    // getPdfPromise = getPdfPromise.concat(app.iCids.map((iCid, i) => {
    //   return new Promise((resolve, reject) => {
    //     appDao.getAppAttachment(app._id, _signature.getAppFormAttachmentName(iCid), (appPdf) => {
    //       if (appPdf.success) {
    //         resolve(appPdf);
    //       } else {
    //         reject(new Error('Fail to get attachment for insured ' + i + ' ' + iCid));
    //       }
    //     });
    //   });
    // }));

    // Promise.all(getPdfPromise).then((appPdfs) => {
    //   cb(null, appPdfs);
    // }).catch((error) => {
    //   logger.error('ERROR: _mergeAppFormPdf', app._id, error);
    //   cb(error);
    // });
    cb(null, appformPdfStrs);
  }, function (appPdfs, cb) {
    var mergePdfPromise = [];
    var phAppPdf = appPdfs[0];

    mergePdfPromise.push(new Promise(function (resolve, reject) {
      commonFunctions.mergePdfs([phAppPdf.data, pdfStr], function (mergedPdf) {
        if (mergedPdf) {
          logger.log('INFO: _mergeAppFormPdf - mergedPdf', '0', 'done', app._id);
          resolve(mergedPdf);
        } else {
          reject(new Error('Fail to merge pdf for proposer'));
        }
      });
    }));

    var _loop = function _loop(i) {
      var laAppPdf = appPdfs[i];
      mergePdfPromise.push(new Promise(function (resolve, reject) {
        commonFunctions.mergePdfs([phAppPdf.data, laAppPdf.data, pdfStr], function (mergedPdf) {
          if (mergedPdf) {
            logger.log('INFO: _mergeAppFormPdf - mergedPdf', i, 'done', app._id);
            resolve(mergedPdf);
          } else {
            reject(new Error('Fail to merge pdf for insured ' + i));
          }
        });
      }));
    };

    for (var i = 1; i < appPdfs.length; i++) {
      _loop(i);
    }

    Promise.all(mergePdfPromise).then(function (mergedPdfs) {
      cb(null, mergedPdfs);
    }).catch(function (error) {
      logger.error('ERROR: _mergeAppFormPdf', app._id, error);
      cb(error);
    });
  }, function (mergedPdfs, cb) {
    var uploadInfoList = [];

    for (var i = 0; i < mergedPdfs.length; i++) {
      var uploadInfo = {
        attachName: '',
        mergedPdf: mergedPdfs[i]
      };

      if (i === 0) {
        uploadInfo.attachName = commonApp.PDF_NAME.eAPP;
      } else {
        var iCid = app.iCids[i - 1];
        uploadInfo.attachName = _signature.getAppFormAttachmentName(iCid);
      }
      uploadInfoList.push(uploadInfo);
    }

    var _uploadAttachment = function _uploadAttachment(currApp, index, cbk) {
      var _uploadInfoList$index = uploadInfoList[index],
          attachName = _uploadInfoList$index.attachName,
          mergedPdf = _uploadInfoList$index.mergedPdf;

      dao.uploadAttachmentByBase64(currApp._id, attachName, currApp._rev, mergedPdf, 'application/pdf', function (res) {
        if (res && res.rev && !res.error) {
          currApp._rev = res.rev;
          cbk(null, currApp, index + 1);
        } else {
          cbk('Fail to upload Pdf attachment ' + index);
        }
      });
    };

    var uploadFunctions = [];
    _.forEach(mergedPdfs, function (item, i) {
      if (i > 0) {
        uploadFunctions.push(_uploadAttachment);
      }
    });

    async.waterfall([function (cbk) {
      _uploadAttachment(app, 0, cbk);
    }].concat(uploadFunctions), function (err, newApp, index) {
      if (err) {
        cb('ERROR: _mergeAppFormPdf - _uploadAttachment ' + err);
      } else {
        cb(null, newApp);
      }
    });
  }], function (err, newApp) {
    if (err) {
      logger.error('ERROR: _mergeAppFormPdf - end [RETURN=-100]', app._id, err);
      callback('Fail to merge AppForm Pdf');
    } else {
      logger.log('INFO: _mergeAppFormPdf - end [RETURN=100]', app._id);
      callback(null, newApp);
    }
  });
};

var _prepareSubmissionEmail = function _prepareSubmissionEmail(data, app, session, cb) {
  var receiveEmailFa = _.get(data, 'changedValues.submission.receiveEmailFa', 'N');
  var emailValues = Object.assign({}, data);
  var iCidMapping = _.get(app, 'iCidMapping');
  //Prepare Submission Email
  async.waterfall([function (callback) {
    appDao.getSubmissionEmailTemplate(session.agent.channel.type === 'FA', function (result) {
      if (result && !result.error) {
        // Check whether need to send email
        var now = Date.now();
        var filteredTemplate = _.filter(result.eSubmissionEmails, function (template) {
          return template.productCode.indexOf(app.quotation.baseProductCode) !== -1;
        });

        if (filteredTemplate && filteredTemplate.length !== 0) {
          logger.log('INFO: _prepareSubmissionEmail filteredTemplate', app.quotation.baseProductCode, session.agent.channel.type, app.id);
          var attachmentKeys = filteredTemplate[0].attachmentKeys;

          var appPdfAttachment = _.get(app, '_attachments');
          _.each(appPdfAttachment, function (value, key) {
            if (key.indexOf(commonApp.PDF_NAME.eAPP) > -1) {
              attachmentKeys.push(key);
            }
          });
          emailValues.agentName = _.get(data, 'agentProfile.name');
          emailValues.agentContactNum = _.get(data, 'agentProfile.mobile');
          emailValues.agentEmail = _.get(data, 'agentProfile.email');
          emailValues.agentTitle = '';
          emailValues.clientName = _.get(data, 'clientProfile.fullName');
          emailValues.clientEmail = _.get(data, 'clientProfile.email');
          emailValues.clientId = _.get(data, 'clientProfile.cid');
          emailValues.clientTitle = _.get(data, 'clientProfile.title');
          emailValues.emailContent = filteredTemplate[0].emailContent;
          emailValues.attKeys = attachmentKeys;
          emailValues.policyNumber = commonApp.getAllPolicyIdsStrFromMaster(iCidMapping);
          emailValues.submissionDate = commonFunctions.getFormattedDate(now);
          emailValues.quotId = app.quotation.id;
          emailValues.appId = app._id;
          emailValues.receiveEmailFa = session.agent.channel.type === 'FA' ? receiveEmailFa : 'Y';
          emailValues.isShield = true;
          callback(null, emailValues);
        } else {
          callback('Fail to get email template from product');
        }
      } else {
        callback('Fail to get email template');
      }
    });
  }, function (eValues, callback) {
    commonApp.prepareSubmissionEmails(eValues, session, function (resp) {
      if (resp && resp.success) {
        callback(null, app);
      } else {
        callback('Fail to prepareSubmissionEmails');
      }
    });
  }], function (err, callbackApplication) {
    if (err) {
      cb('Fail in _prepareSubmissionEmail, ' + err);
    } else {
      cb(null, callbackApplication);
    }
  });
};

var _createApprovalCases = function _createApprovalCases(app, session, callback) {
  //Create Approval Cases
  var promises = [];
  var childIds = _.get(app, 'childIds');
  var currentTime = new Date().toISOString();
  //Create Master approval docs
  promises.push(_createMasterApprovalCase(app.id, session, currentTime));

  // Create child approval docs
  _.each(childIds, function (id) {
    promises.push(_createApprovalCase(id, session, currentTime));
  });

  return Promise.all(promises).then(function (results) {
    callback(null, { success: true });
  }, function (rejectReason) {
    callback('ERROR in _createApprovalCases with Reject Reason,  ' + rejectReason);
  }).catch(function (error) {
    logger.error('ERROR in _createApprovalCases ' + error);
    callback('ERROR in _createApprovalCases, ' + error);
  });
};

var _createMasterApprovalCase = function _createMasterApprovalCase(masterId, session, currentTime) {
  return new Promise(function (resolve, reject) {
    approvalHandler.createApprovalCase({ ids: [masterId], isShieldMaster: true, currentTime: currentTime }, session, function (eAppResult) {
      if (eAppResult.success) {
        resolve({ success: true });
      } else {
        reject({ success: false });
      }
    });
  });
};

var _createApprovalCase = function _createApprovalCase(childAppId, session, currentTime) {
  return new Promise(function (resolve, reject) {
    approvalHandler.createApprovalCase({ ids: [childAppId], isShield: true, currentTime: currentTime }, session, function (eAppResult) {
      if (eAppResult.success) {
        resolve({ success: true });
      } else {
        reject({ success: false });
      }
    });
  });
};

var _sendApprovalSubmittedCaseNotification = function _sendApprovalSubmittedCaseNotification(app, session, callback) {
  //Send email
  var masterEapprovalId = approvalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);
  aEmaillHandler.SubmittedCaseNotification({ id: masterEapprovalId }, session);
  callback(null, app);
};

var _submitApplication = function _submitApplication(data, app, session, cb) {
  var submissionValues = _.get(data, 'changedValues.submission');
  async.waterfall([function (callback) {
    // update bundle
    bundleDao.onSubmitApplication(app.pCid, app._id).then(function (result) {
      if (result.ok) {
        // Success update bundle
        callback(null, { success: true });
      } else {
        callback('Fail to create/ update Bundle ');
      }
    }).catch(function (Err) {
      logger.error('ERROR _submitApplication in update bundle promise', Err);
      callback('Fail to create/ update Bundle ' + Err);
    });
  }, function (previousResult, callback) {
    // update application

    if (_.get(session, 'agent.agentCode') !== _.get(session, 'agent.managerCode')) {
      commonApp.transformSuppDocsPolicyFormValues(app);
    }
    if (_.get(app, 'payment.initialPayment.paymentMethod') && _.get(_.get(app, 'supportDocuments.viewedList'), session.agentCode)) {
      app.supportDocuments.viewedList[session.agentCode][app.payment.initialPayment.paymentMethod] = false;
    }

    if (session.agent.channel.type === 'FA') {
      app.submission = submissionValues;
    }

    app.isSubmittedStatus = true;
    app.submitStatus = 'SUCCESS';
    app.applicationSubmittedDate = new Date().toISOString();
    _application.updateApplicationCompletedStep(app);

    // Declare which fields need to update to child
    var updateObjforChild = {
      submission: app.submission,
      isSubmittedStatus: app.isSubmittedStatus,
      submitStatus: app.submitStatus,
      applicationSubmittedDate: app.applicationSubmittedDate
    };

    appDao.upsertApplication(app._id, app, function (res) {
      if (res && !res.error) {
        //isMandDocsAllUploaded may change to false in transformSuppDocsPolicyFormValues, always return isMandDocsAllUploaded = true to web in submission success case
        app.isMandDocsAllUploaded = true;
        callback(null, [app, updateObjforChild]);
      } else {
        callback('Fail to Update application');
      }
    });
  }, function (result, callback) {
    _saveParentSubmissionValuestoChild(result[0], result[1], callback);
  }, function (pApplication, callback) {
    bundleDao.updateStatus(pApplication.pCid, bundleDao.BUNDLE_STATUS.SUBMIT_APP).then(function (res) {
      logger.log('INFO: Shield appFormSubmission - update bundle', pApplication._id);
      callback(null, pApplication);
    }).catch(function (error) {
      logger.error('ERROR: Shield appFormSubmission - end [RETURN=1] fails :', pApplication._id, error);
      callback('Failed to update Bundle ');
    });
  }], function (error, modifiedApplication) {
    if (error) {
      logger.error('ERROR in _submitApplication ' + error);
      cb('Fail to _submitApplication ' + error);
    } else {
      cb(null, modifiedApplication);
    }
  });
};

var startSubmission = function startSubmission(data, app, session, cb) {
  logger.log('INFO: appFormSubmission - (2) doSubmission', app.id);
  var appId = data.appId;

  async.waterfall([function (callback) {
    logger.info('Start Submission - Create Approval Cases');
    _createApprovalCases(app, session, callback);
  }, function (result, callback) {
    //Update View
    logger.info('Start Submission - Update Views');
    dao.updateViewIndex('main', 'applicationsByAgent');
    dao.updateViewIndex('main', 'approvalDetails');
    callback(null, { success: true });
  }, function (result, callback) {
    // Submit the Case
    logger.info('Start Submission - Submit Application');
    _submitApplication(data, app, session, callback);
  }, function (modifiedApplication, callback) {
    // Send Approval Email
    logger.info('Start Submission - Send Approval Notification');
    _sendApprovalSubmittedCaseNotification(modifiedApplication, session, callback);
  }, function (modifiedApplication, callback) {
    // Send Submission Pos Email
    logger.info('Start Submission - Send Pos Email');
    _prepareSubmissionEmail(data, modifiedApplication, session, callback);
  }], function (err, callbackApplication) {
    if (err) {
      logger.error('ERROR startSubmission: ' + appId + ', ' + err);
      cb('Fail in start Submission');
    } else {
      // Return modified application
      cb(null, callbackApplication);
    }
  });
};

//=ApplicationHandler.appFormSubmission
module.exports.submission = function (data, session, cb) {
  var appId = data.appId,
      submissionTemplate = data.submissionTemplate,
      changedValues = data.changedValues;

  return _application.getApplication(appId).then(function (app) {
    var appformPdfstrs = void 0;
    var rollbackDoc = void 0;
    async.waterfall([function (callback) {
      // GetPdf Doc for rollback 
      var getPdfPromise = [];
      getPdfPromise.push(new Promise(function (resolve, reject) {
        appDao.getAppAttachment(app._id, commonApp.PDF_NAME.eAPP, function (appPdf) {
          if (appPdf.success) {
            resolve(appPdf);
          } else {
            reject(new Error('Fail to get attachment for proposer'));
          }
        });
      }));
      getPdfPromise = getPdfPromise.concat(app.iCids.map(function (iCid, i) {
        return new Promise(function (resolve, reject) {
          appDao.getAppAttachment(app._id, _signature.getAppFormAttachmentName(iCid), function (appPdf) {
            if (appPdf.success) {
              resolve(appPdf);
            } else {
              reject(new Error('Fail to get attachment for insured ' + i + ' ' + iCid));
            }
          });
        });
      }));

      Promise.all(getPdfPromise).then(function (appPdfs) {
        appformPdfstrs = appPdfs;
        callback(null, { success: true });
      }).catch(function (error) {
        logger.error('ERROR: prepare rollback in submission', app._id, error);
        callback(error);
      });
    }, function (result, callback) {
      // Get application, bundle, approval json
      var docArr = [_.get(app, 'bundleId'), app.id];
      var promiseArr = [];
      docArr.forEach(function (obj) {
        promiseArr.push(new Promise(function (resolve, reject) {
          dao.getDoc(obj, function (doc) {
            resolve(doc);
          });
        }));
      });
      Promise.all(promiseArr).then(function (getDocresult) {
        rollbackDoc = getDocresult;
        callback(null, { success: true });
      }).catch(function (error) {
        logger.error("Error in getAllDoc->Promise.all: ", error);
        callback('Error in getAllDoc for rollback ' + error);
      });
    }, function (result, callback) {
      // Prepare the payment Pdf
      _preparePaymentPDf(app, 'en', callback);
    }, function (pdfStr, callback) {
      // merge to all AppForm Pdf
      _mergeAppFormPdf(app, pdfStr, appformPdfstrs, callback);
    }, function (newApp, callback) {
      // Start Submission
      startSubmission(data, newApp, session, callback);
    }], function (err, callbackApplication) {
      if (err) {
        logger.error('ERROR submission: ' + appId + ', ' + err);

        //TO-DO: handle rollback application, and fail message
        //msg 1: Supervisor detail not available. Case cannot be submitted.
        //msg 2: Submission Fail. Please try again later

        // Call Roll Back here
        _rollbackSubmission(app, submissionTemplate, appformPdfstrs, rollbackDoc, cb);
      } else {
        // Return template and modified application
        var frozenTemplate = _.cloneDeep(submissionTemplate);
        if (session.platform) {
          commonApp.frozenTemplateMobile(frozenTemplate);
        } else {
          commonApp.frozenTemplate(frozenTemplate);
        }
        cb({ success: true, application: callbackApplication, template: frozenTemplate });
      }
    });
  }).catch(function (err) {
    logger.error('ERROR submission: ' + appId + ', ' + err);
    changedValues.submitStatus = 'FAIL';
    cb({ success: true, application: changedValues, template: submissionTemplate });
  });
};

var _rollbackSubmission = function _rollbackSubmission(app, template, rollbackPdfs, rollbackDocs, cb) {
  async.waterfall([function (callback) {
    // rollback pdf
    var cidMappingArr = _.get(app, 'iCids');
    _rollbackPdf(app, rollbackPdfs, cidMappingArr, undefined, 0, callback);
  }, function (result, callback) {
    // rollback json
    _rollbackDoc(app, rollbackDocs, callback);
  }], function (error, result) {
    if (error) {
      cb('ERROR in rollbacksubmission ' + error);
    } else {
      app.submitStatus = 'FAIL';
      cb({ success: false, application: app, template: template });
    }
  });
};

var _rollbackDoc = function _rollbackDoc(application, rollbackDocs, cb) {
  async.waterfall([function (callback) {
    // Get application, bundle, approval json
    var docArrIds = [_.get(application, 'bundleId'), application.id];
    var deleteArrids = [];
    deleteArrids.push(approvalHandler.getMasterApprovalIdFromMasterApplicationId(application.id));
    _.each(_.get(application, 'iCidMapping'), function (cidObj) {
      _.each(cidObj, function (obj) {
        if (obj && obj.policyNumber) {
          deleteArrids.push(obj.policyNumber);
        }
      });
    });

    var promiseArr = [];
    // Rollback application
    docArrIds.map(function (id, index) {
      promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(id, function (doc) {
          resolve(doc);
        });
      }).then(function (doc) {
        return new Promise(function (resolve, reject) {
          var updateDoc = void 0;
          _.each(rollbackDocs, function (obj) {
            if (obj._id === doc._id && doc.type === 'masterApplication') {
              updateDoc = obj;
              updateDoc.submitStatus = 'FAIL';
              updateDoc.applicationSubmittedDate = new Date().toISOString();
            } else if (obj._id === doc._id) {
              updateDoc = obj;
            }
          });
          updateDoc._rev = doc._rev;
          if (updateDoc) {
            dao.updDoc(doc._id, updateDoc, function (result) {
              resolve(result);
            });
          } else {
            resolve({ success: false });
          }
        });
      }));
    });

    //Delete Json
    deleteArrids.map(function (id) {
      promiseArr.push(new Promise(function (resolve, reject) {
        dao.getDoc(id, function (doc) {
          resolve(doc);
        });
      }).then(function (doc) {
        return new Promise(function (resolve, reject) {
          if (doc._id) {
            dao.delDocWithRev(doc._id, doc._rev, function (result) {
              resolve(result);
            });
          } else {
            resolve({ success: true });
          }
        });
      }));
    });

    Promise.all(promiseArr).then(function (getDocresult) {
      callback(null, { success: true });
    }).catch(function (error) {
      logger.error('Error in rollbackDoc for rollback ' + error);
      callback('Error in rollbackDoc for rollback ' + error);
    });
  }], function (error, result) {
    if (error) {
      cb('ERROR in _rollbackdoc ' + error);
    } else {
      cb(null, { success: true });
    }
  });
};

var _rollbackPdf = function _rollbackPdf(application, base64StrsArr, cidMappingArr, rev, index, cb) {
  async.waterfall([function (callback) {
    new Promise(function (resolve, reject) {
      if (rev === undefined) {
        _application.getApplication(application.id).then(function (app) {
          resolve(app._rev);
        });
      } else {
        resolve(rev);
      }
    }).then(function (searchRev) {
      var base64Str = base64StrsArr[index];
      var attId = index === 0 ? commonApp.PDF_NAME.eAPP : _signature.getAppFormAttachmentName(cidMappingArr[index - 1]);
      uploadAttachment(application.id, attId, searchRev, base64Str, 'application/pdf', function (resultRev) {
        if (index + 1 !== base64StrsArr.length) {
          _rollbackPdf(application, base64StrsArr, cidMappingArr, resultRev, index + 1, callback);
        } else {
          callback(null, { success: true });
        }
      });
    }).catch(function (error) {
      logger.error("Error in rollbackPdf->new Promise: ", error);
      callback('Error in rollbackPdf->new Promise:  ' + error);
    });
  }], function (error, result) {
    if (error) {
      cb(error);
    } else {
      cb(null, { success: true });
    }
  });
};

var uploadAttachment = function uploadAttachment(id, attchId, rev, data, mime, callback) {
  dao.uploadAttachmentByBase64(id, attchId, rev, data.data, mime, function (result) {
    if (result && !result.error) {
      callback(result.rev);
    } else {
      logger.error('ERROR:: uploadAttachment in rollback submission pdf', _.get(result, 'error'));
      callback({ success: false });
    }
  });
};

module.exports.rollbackSubmission = _rollbackSubmission;
module.exports.getSubmissionTemplate = _getSubmissionTemplate;
module.exports.prepareSubmission = _prepareSubmission;