'use strict';

var _ = require('lodash');
var logger = global.logger || console;
var dao = require('../../../cbDaoFactory').create();
var fcRecommendationShieldId = 'fcRecommendation_shield';
var commonAppHandler = require('../common');

/*
  * MOVE TO COMMON FUNCTION
// has to make sure input parm arrOne & arrTwo are arrayType
var isTwoArraysEqual = function(arrOne, arrTwo) {
  if (!arrOne || !arrTwo) {
    return false;
  }
  return _.isEmpty(_.difference(arrOne, arrTwo)) && _.isEmpty(_.difference(arrTwo, arrOne));
};
*/

module.exports.getClientChoiceDefaultString_shield = function (insuredList) {
  logger.log('INFO: getClientChoiceDefaultString_shield - start');
  var lang = 'en';
  var defaultRecommendBenefit = '';
  var defaultRecommendLimit = '';
  var insuredListCopy = _.cloneDeep(insuredList);
  var insuredPlanListMap = {};

  var getInsuredNameList = function getInsuredNameList(currentInsuredCid) {
    var nameList = '';
    var samePlanInsuredList = [];

    _.forEach(insuredPlanListMap, function (planList, iCid) {
      if (commonAppHandler.isTwoArraysEqual(insuredPlanListMap[iCid], insuredPlanListMap[currentInsuredCid])) {
        samePlanInsuredList.push(insuredListCopy[iCid]);
      }
    });

    _.forEach(samePlanInsuredList, function (insured, index) {
      insured.hasRead = true;
      nameList += insured.iFullName;
      if (index !== samePlanInsuredList.length - 1) {
        nameList += ', ';
      }
    });
    return nameList;
  };

  return new Promise(function (resolve, reject) {
    dao.getDocFromCacheFirst(fcRecommendationShieldId, function (res) {
      if (res && !res.error) {
        var defaultTextList = _.get(res, 'items', []);

        _.forEach(insuredListCopy, function (insured, iCid) {
          insuredPlanListMap[iCid] = [];
          _.forEach(_.get(insured, 'plans'), function (plan) {
            insuredPlanListMap[iCid].push(plan.planCode);
          });
        });

        _.forEach(insuredListCopy, function (insured) {
          // hasRead means this insured's data has been used.
          if (!insured.hasRead) {
            // get insured name list whose basic plan and riders and same
            var nameList = getInsuredNameList(insured.iCid);
            var objFoundText = void 0;
            defaultRecommendBenefit += nameList + '\n';
            defaultRecommendLimit += nameList + '\n';
            // get default text from doc
            _.forEach(defaultTextList, function (objDefaultText) {
              if (commonAppHandler.isTwoArraysEqual(objDefaultText.planList, insuredPlanListMap[insured.iCid])) {
                objFoundText = objDefaultText;
              }
            });
            if (objFoundText) {
              defaultRecommendBenefit += objFoundText.benefits[lang] + '\n\n';
              defaultRecommendLimit += objFoundText.limitations[lang] + '\n\n';
              /* 20171228 Does not need to check age under 1 for Shield
              if (_.get(insured, 'iAge') === 0) {
                defaultRecommendLimit += objFoundText.additionalLimitations[lang] + '\n\n';
              }
              */
            }
          }
        });
        logger.log('INFO: getClientChoiceDefaultString_shield - end', _.keys(insuredList));
        resolve({
          // hasClientChoice:hasClientChoice,
          defaultRecommendBenefit: _.trim(defaultRecommendBenefit),
          defaultRecommendLimit: _.trim(defaultRecommendLimit)
        });
      } else {
        logger.error('ERROR: getClientChoiceDefaultString_shield - get fcRecommendationShieldId fails', res);
        reject(new Error('Fail to get fcRecommendationShieldId' + _.get(res, 'error')));
      }
    });
  }).catch(function (error) {
    logger.error('ERROR: getClientChoiceDefaultString_shield - catched error', _.keys(insuredList), error);
  });
};

/** DO NOT REMOVE recommendationValues */
module.exports.setMenuItemValues_shield = function (menuValues, quot, recommendationValues) {
  var lang = 'en';
  var anyInsuredChoseROP = false;
  var insuredRopValue = '';
  var quotRopBlock = _.get(quot, 'clientChoice.recommendation.rop_shield.ropBlock');
  var shieldInsuredPlans = [];
  var planNameMapping = {
    ASIMSA: 'Shield Plan A',
    ASIMSB: 'Shield Plan B',
    ASIMSC: 'Shield Standard',
    ASPA: 'Basic Care A',
    ASPB: 'Basic Care B',
    ASPC: 'Basic Care Standard',
    ASGC: 'General Care',
    ASGCB: 'General Care',
    ASHCD: 'Home Care',
    ASHC: 'Home Care'
  };

  for (var i = 0; i < 6; i++) {
    insuredRopValue = _.get(quotRopBlock, 'shieldRopAnswer_' + i, '');
    if (insuredRopValue) {
      _.set(menuValues, 'ropBlock.shieldRopTable.shieldRopAnswer_' + i, insuredRopValue);
      /** DO NOT REMOVE recommendationValues */
      _.set(recommendationValues, 'rop_shield.ropBlock.shieldRopAnswer_' + i, insuredRopValue);
      if (insuredRopValue === 'Y') {
        anyInsuredChoseROP = true;
      }
    }
  }
  if (anyInsuredChoseROP) {
    _.set(menuValues, 'ropBlock.ropQ2', _.get(quotRopBlock, 'ropQ2'));
    _.set(menuValues, 'ropBlock.ropQ3', _.get(quotRopBlock, 'ropQ3'));
    _.set(menuValues, 'ropBlock.ropQ1sub3', _.get(quotRopBlock, 'ropQ1sub3'));
    /** DO NOT REMOVE recommendationValues */
    _.set(recommendationValues, 'rop_shield.ropBlock.ropQ2', _.get(quotRopBlock, 'ropQ2'));
    _.set(recommendationValues, 'rop_shield.ropBlock.ropQ3', _.get(quotRopBlock, 'ropQ3'));
    _.set(recommendationValues, 'rop_shield.ropBlock.ropQ1sub3', _.get(quotRopBlock, 'ropQ1sub3'));
  }

  _.forEach(_.get(quot, 'insureds'), function (insured) {
    /** DO NOT REMOVE iFullName, cid */
    var iFullName = _.get(insured, 'iFullName');
    var cid = _.get(insured, 'iCid');
    var arrPlanList = [];

    _.forEach(_.get(insured, 'plans'), function (plan) {
      // arrPlanList.push(planNameMapping[plan.planCode]);
      arrPlanList.push(_.get(plan, 'covName.' + lang, ''));
    });

    /** DO NOT REMOVE iFullName, cid */
    shieldInsuredPlans.push({
      shieldInsured: iFullName + ':',
      shieldPlan: _.join(arrPlanList, ', '),
      fullName: iFullName,
      cid: cid
    });
  });
  menuValues.shieldInsuredPlans = shieldInsuredPlans;
  /** DO NOT REMOVE recommendationValues */
  recommendationValues.extra.shieldInsuredPlans = shieldInsuredPlans;
};

module.exports.setMenuItemTemplate_shield = function (menuTemplateCopy, iCidRopAnswerMap, insuredList) {
  var count = 0;
  var ropBlock = _.find(menuTemplateCopy.items, function (block) {
    return block.id === 'ropBlock';
  });
  var ropAnswerArr = [];
  var ropAnswerObj = {
    "type": "QRADIOGROUP",
    "horizontal": true,
    "mandatory": true,
    "options": [{
      "value": "N",
      "title": "No"
    }, {
      "value": "Y",
      "title": "Yes"
    }]
  };

  _.forEach(insuredList, function (insured) {
    var id = 'shieldRopAnswer_' + count;
    var title = insured.iFullName;
    var ropAnswerObjCopy = _.assign({}, ropAnswerObj, {
      id: id,
      title: title
    });
    ropAnswerArr.push(ropAnswerObjCopy);
    count++;
    iCidRopAnswerMap[insured.iCid] = id;
  });

  _.set(ropBlock, 'items[1].items', ropAnswerArr);
};