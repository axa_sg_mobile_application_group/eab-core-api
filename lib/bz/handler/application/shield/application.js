'use strict';

var _genAppFormInsuredPdfAsync = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(lang, application, bundle, template) {
    var results, index, iCid, r;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            results = [];
            index = 0;

          case 2:
            if (!(index < application.iCids.length)) {
              _context.next = 11;
              break;
            }

            iCid = application.iCids[index];
            _context.next = 6;
            return _genAppFormInsuredPdfPromise(lang, application, bundle, template, iCid, index);

          case 6:
            r = _context.sent;

            results.push(r);

          case 8:
            index += 1;
            _context.next = 2;
            break;

          case 11:
            return _context.abrupt('return', results);

          case 12:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function _genAppFormInsuredPdfAsync(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var _ = require('lodash');
var moment = require('moment');
var logger = global.logger || console;

var utils = require('../../../Quote/utils');
var CommonUtils = require('../../../utils/CommonUtils');
// const DateUtils = require('../../../../common/DateUtils');
var commonApp = require('../common');
var _signature = require('./signature');
var _payment = require('./payment');
var _submission = require('./submission');
var dao = require('../../../cbDaoFactory').create();
var bDao = require('../../../cbDao/bundle');
var pDao = require('../../../cbDao/product');
var cDao = require('../../../cbDao/client');
var nDao = require('../../../cbDao/needs');
var appDao = require('../../../cbDao/application');
var PDFHandler = require('../../../PDFHandler');
var needsHandler = require('../../../NeedsHandler');
var agentDao = require('../../../cbDao/agent');

var _getNameOfOrganization = function _getNameOfOrganization(upline2Code, callback) {
  logger.log('INFO: _getNameOfOrganization shield->searchAgents upline2Code: ', upline2Code);
  return new Promise(function (resolve, reject) {
    agentDao.searchAgents('01', upline2Code, upline2Code, function (res) {
      if (res.success) {
        if (res.result.length > 0) {
          resolve(res.result[0].agentName);
        } else {
          logger.error('Error in _getNameOfOrganization shield->searchAgents upline2Code: ', upline2Code);
          resolve(null);
        }
      } else {
        logger.error('Error in _getNameOfOrganization shield->searchAgents upline2Code: ', upline2Code);
        resolve(null);
      }
    });
  });
};

var _getAppFormTemplate = function _getAppFormTemplate(quotation) {
  return new Promise(function (resolve, reject) {
    logger.log('INFO: _getAppFormTemplate - start', quotation.id);
    var isPhSameAsLa = false;
    var plans = [];
    var pCid = _.get(quotation, 'pCid');
    var insureds = _.keys(_.get(quotation, 'insureds'));
    var iCids = [];
    _.forEach(insureds, function (iCid, i) {
      if (pCid === iCid) {
        isPhSameAsLa = true;
      } else {
        iCids.push(iCid);
      }
      plans = [].concat(_toConsumableArray(plans), _toConsumableArray(_.get(quotation, 'insureds.' + iCid + '.plans', [])));
    });
    plans = _.uniqBy(plans, 'covCode');

    var _getInsForms = function _getInsForms(insForms, forms, index, cb) {
      logger.log('INFO: getAppFormTemplate - getInsForms', index, forms.length, isPhSameAsLa);
      if (forms.length > index) {
        var form = forms[index];

        dao.getDocFromCacheFirst(form.form, function (tpl) {
          if (tpl && !tpl.error) {
            var laItem = _.cloneDeep(tpl.item);
            if (isPhSameAsLa && form.id === 'ph') {
              laItem.subType = 'proposer';
            } else if (form.id === 'la') {
              laItem.subType = 'insured';
            }

            var template = _.cloneDeep(tpl);
            template.items = _.cloneDeep(insForms.items) || [];
            template.items.push(laItem);
            if (form.id === 'la') {
              //copy insured tab if more then 1 la
              for (var i = 1; i < iCids.length; i++) {
                var insuredTab = _.cloneDeep(laItem);
                template.items.push(insuredTab);
              }
            }

            insForms.formId = template.formId;
            insForms.items = template.items;
          }
          _getInsForms(insForms, forms, index + 1, cb);
        });
      } else {
        if (insForms && insForms.items) {
          // Sort Proposer to the first item
          insForms.items = insForms.items.sort(function (a, b) {
            if (a.subType > b.subType) {
              return -1;
            } else {
              return 1;
            }
          });
        }
        cb();
      }
    };

    var _getTemplate = function _getTemplate(forms, index, callback) {
      if (forms.length > index) {
        var form = forms[index];

        if (typeof form.form === 'string') {
          dao.getDocFromCacheFirst(form.form, function (tpl) {
            if (tpl && !tpl.error) {
              var template = _.cloneDeep(tpl);
              delete form.form;
              form.formId = template.formId;
              form.skipCheck = template.skipCheck;

              //copy insured tab if more then 1 la
              var insuredTabs = _.filter(template.items, function (tab) {
                return tab.subType === 'insured';
              });
              for (var i = 1; i < iCids.length && insuredTabs.length > 0; i++) {
                var insuredTab = _.cloneDeep(insuredTabs[0]);
                template.items.push(insuredTab);
              }

              //remove insured tab if ph is la and no other insured
              if (isPhSameAsLa && iCids.length === 0) {
                for (var _i = template.items.length; _i > 1; _i--) {
                  template.items.splice(1, 1);
                }
              }

              //remove proposer tab in ROP if ph is not la
              if (!isPhSameAsLa && (template.formId === 'menu_residency' || template.formId === 'menu_plan')) {
                template.items.splice(0, 1);
              }

              form.items = template.items;
            }
            logger.log('INFO: _getAppFormTemplate - getTemplate', index, '(got forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
            _getTemplate(forms, index + 1, callback);
          });
        } else {
          var insForms = [];
          if (iCids.length > 0 && form.form.la) {
            for (var i in plans) {
              var plan = plans[i];
              if (form.form.la[plan.covCode]) {
                (function () {
                  var tplCode = form.form.la[plan.covCode];
                  if (_.findIndex(insForms, function (it) {
                    return it.form === tplCode;
                  }) === -1) {
                    insForms.push({
                      id: 'la',
                      form: tplCode
                    });
                  }
                })();
              }
            }
          }

          if (isPhSameAsLa && form.form.ph) {
            var _loop = function _loop(_i2) {
              var plan = plans[_i2];
              var tplCode = form.form.ph[plan.covCode];

              if (tplCode) {
                if (_.findIndex(insForms, function (it) {
                  return it.form === tplCode;
                }) === -1) {
                  insForms.push({
                    id: 'ph',
                    form: tplCode
                  });
                }
              }
            };

            for (var _i2 in plans) {
              _loop(_i2);
            }
          }
          if (insForms.length > 0) {
            delete form.form;
            form.items = [];
            _getInsForms(form, insForms, 0, function () {
              logger.log('INFO: getAppFormTemplate - getTemplate', quotation.id, index, '(got Ins forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
              _getTemplate(forms, index + 1, callback);
            });
          } else {
            _getTemplate(forms, index + 1, callback);
          }
        }
      } else {
        callback();
      }
    };

    var _getSectionItems = function _getSectionItems(sections, index, callback) {
      logger.log('INFO: _getAppFormTemplate - getSectionItems', quotation.id, '[index=', index, '; numOfSection=', sections.length, ']');
      if (sections.length > index) {
        _getTemplate(sections[index].items, 0, function () {
          _getSectionItems(sections, index + 1, callback);
        });
      } else {
        callback(sections);
      }
    };

    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (mapping) {
      if (mapping && mapping[quotation.baseProductCode]) {
        var sections = _.cloneDeep(mapping[quotation.baseProductCode].eFormSections);
        if (sections && sections.length) {
          _getSectionItems(sections, 0, resolve);
        } else {
          resolve(sections);
        }
      } else {
        logger.error('ERROR: _getAppFormTemplate - end [RETURN=-100]', quotation.id);
        reject();
      }
    });
  }).then(function (sections) {
    logger.log('INFO: _getAppFormTemplate - allDone', quotation.id);

    var _genMenuForm = function _genMenuForm(forms, title, detailSeq) {
      logger.log('INFO: getAppFormTemplate - genMenuForm', title);
      var menuForm = {
        'title': title,
        'type': 'menuItem',
        'skipCheck': forms.skipCheck,
        'key': forms.formId || forms.id || forms._id,
        'detailSeq': detailSeq,
        'items': [],
        'shieldLayout': true
      };

      // if (forms.id && (forms.id === 'menu_plan' || forms.id === 'plan')) {
      //   menuForm.items = forms.items;
      // } else {
      menuForm.items.push({
        type: 'tabs',
        subType: 'servergenerated',
        items: forms.items
      });
      // }
      return menuForm;
    };

    var eFormMenus = [];
    if (sections && sections.length) {
      for (var s in sections) {
        var sectTmp = {
          type: 'menuSection',
          detailSeq: parseInt(s),
          items: []
        };
        var forms = sections[s];
        for (var f in forms.items) {
          var form = forms.items[f];
          if (form.items && form.items.length) {
            sectTmp.items.push(_genMenuForm(form, form.title, parseInt(f)));
          }
        }
        eFormMenus.push(sectTmp);
      }
    }

    logger.log('INFO: _getAppFormTemplate - end', quotation.id);
    return {
      template: {
        'type': 'BLOCK',
        'items': [{
          'type': 'MENU',
          'items': eFormMenus
        }]
      },
      sections: sections
    };
  });
};

var _forzenAppFormTemplateByTabIndex = function _forzenAppFormTemplateByTabIndex(session, template, formTabMap, tabIndex) {
  var type = _.toUpper(_.get(template, 'type', ''));

  if (['BLOCK', 'MENU', 'MENUSECTION'].indexOf(type) >= 0 && _.isArray(template.items)) {
    _.forEach(template.items, function (item) {
      _forzenAppFormTemplateByTabIndex(session, item, formTabMap);
    });
  } else if (type === 'MENUITEM') {
    var key = _.get(template, 'key', '');
    var forzenTabIndex = formTabMap[key];
    _.forEach(template.items, function (item) {
      _forzenAppFormTemplateByTabIndex(session, item, formTabMap, forzenTabIndex);
    });
  } else if (type === 'TABS' && _.isNumber(tabIndex)) {
    if (tabIndex >= 0 && tabIndex < _.get(template, 'items.length', 0)) {
      logger.debug('DEBUG: _forzenAppFormTemplateByTabIndex - perpare to forzen', type, 'with index', tabIndex);
      if (session.platform) {
        commonApp.frozenTemplateMobile(template.items[tabIndex]);
      } else {
        commonApp.frozenTemplate(template.items[tabIndex]);
      }
    } else {
      logger.debug('DEBUG: _forzenAppFormTemplateByTabIndex - tab index not found', tabIndex);
    }
  }
};

var _forzenAppFormTemplateByMenuItemKey = function _forzenAppFormTemplateByMenuItemKey(session, template, key) {
  var type = _.toUpper(_.get(template, 'type', ''));
  if (['BLOCK', 'MENU'].indexOf(type) >= 0 && _.isArray(template.items)) {
    _.forEach(template.items, function (item) {
      _forzenAppFormTemplateByMenuItemKey(session, item, key);
    });
  } else if (type === 'MENUSECTION' && _.isArray(template.items)) {
    var forzenItemIndex = _.findIndex(template.items, function (item) {
      return _.get(item, 'key', '') === key;
    });
    if (forzenItemIndex >= 0 && forzenItemIndex < _.get(template, 'items.length', 0)) {
      logger.debug('DEBUG: _forzenAppFormTemplateByMenuItemKey - menuItem key', key, 'found in type=', type, ',prepare to frozen');
      if (session.platform) {
        commonApp.frozenTemplateMobile(template.items[forzenItemIndex]);
      } else {
        commonApp.frozenTemplate(template.items[forzenItemIndex]);
      }
    } else {
      logger.debug('DEBUG: _forzenAppFormTemplateByMenuItemKey - menuItem key', key, 'not found in type=', type);
    }
  }
};

var _forzenAppFormTemplateByCid = function _forzenAppFormTemplateByCid(session, application, template, cid) {
  var formTabMap = {
    menu_person: -1,
    menu_residency: -1,
    menu_insure: -1,
    menu_declaration: -1
  };
  var isPhSameAsLa = _.get(application, 'applicationForm.values.proposer.extra.isPhSameAsLa') === 'Y';

  if (application.pCid === cid) {
    formTabMap.menu_person = 0;
    formTabMap.menu_declaration = 0;

    if (isPhSameAsLa) {
      formTabMap.menu_residency = 0;
      formTabMap.menu_insure = 0;
    }
    _forzenAppFormTemplateByTabIndex(session, template, formTabMap);
  } else {
    var iCidIndex = _.get(application, 'iCids', []).indexOf(cid);

    if (iCidIndex < 0) {
      logger.error('ERROR: _forzenAppFormTemplateByCid - iCid not found', cid, application._id);
    } else {
      formTabMap.menu_person = iCidIndex + 1;
      formTabMap.menu_residency = isPhSameAsLa ? iCidIndex + 1 : iCidIndex;
      formTabMap.menu_insure = isPhSameAsLa ? iCidIndex + 1 : iCidIndex;
      formTabMap.menu_declaration = iCidIndex + 1;

      _forzenAppFormTemplateByTabIndex(session, template, formTabMap);
    }
  }
};

var _getAppFormTemplateWithOptionList = function _getAppFormTemplateWithOptionList(session, application) {
  return _getAppFormTemplate(application.quotation).then(function (templateRes) {
    return new Promise(function (resolve) {
      commonApp.getOptionsList(application, templateRes.template, function () {
        if (application.isStartSignature) {
          logger.log('INFO: _forzenAppFormTemplateByMenuItemKey - menu_person', application._id);
          _forzenAppFormTemplateByMenuItemKey(session, templateRes.template, 'menu_person');
          logger.log('INFO: _forzenAppFormTemplateByCid', application._id);
          _forzenAppFormTemplateByCid(session, application, templateRes.template, application.pCid);

          _.forEach(_.get(application, 'isAppFormInsuredSigned', []), function (isLaSigned, index) {
            if (isLaSigned) {
              var iCid = _.get(application, 'iCids[' + index + ']', '');
              if (iCid) {
                logger.log('INFO: _forzenAppFormTemplateByCid', application._id, iCid);
                _forzenAppFormTemplateByCid(session, application, templateRes.template, iCid);
              } else {
                logger.error('ERROR: _getAppFormTemplateWithOptionList - Fail to find iCids with index', index, application._id);
              }
            }
          });
        }

        resolve(templateRes);
      });
    });
  });
};

var _getApplication = function _getApplication(appId) {
  return new Promise(function (resolve, reject) {
    appDao.getApplication(appId, function (app) {
      if (app && !app.error) {
        resolve(app);
      } else {
        reject(new Error('_getApplication - Fail to get Application ' + appId + _.get(app, 'error')));
      }
    });
  });
};

var _getChildApplications = function _getChildApplications(childIds) {
  var getAppPromises = childIds.map(function (childId, i) {
    return _getApplication(childId);
  });
  return Promise.all(getAppPromises);
};

var _saveParentApplicationValuesToChild = function _saveParentApplicationValuesToChild(parentApplication, childApplications) {
  logger.log('INFO: _saveParentApplicationValuesToChild', parentApplication._id);
  var pCid = _.get(parentApplication, 'pCid');
  var paFormValues = parentApplication.applicationForm.values;

  var proposerValues = _.cloneDeep(paFormValues.proposer);
  if (_.get(proposerValues, 'groupedPlanDetails')) {
    delete proposerValues.groupedPlanDetails;
  }

  _.forEach(childApplications, function (childApp) {
    var iCid = _.get(childApp, 'iCid');

    if (pCid === iCid) {
      childApp.applicationForm.values.proposer = proposerValues;
    } else {
      var insuredValues = _.cloneDeep(_.filter(paFormValues.insured, function (la) {
        return _.get(la, 'personalInfo.cid') === iCid;
      }));

      _.forEach(insuredValues, function (iValues) {
        if (_.get(iValues, 'groupedPlanDetails')) {
          delete iValues.groupedPlanDetails;
        }
      });

      childApp.applicationForm.values.proposer = proposerValues;
      childApp.applicationForm.values.insured = insuredValues;
    }
  });
};

//=ApplicationHandler.checkCrossAge
var _checkCrossAge = function _checkCrossAge(app) {
  // *** Shield don't have isBackDate, always N in quotation
  logger.log('INFO: _checkCrossAge - start', app._id);

  return new Promise(function (resolve, reject) {
    pDao.getProduct(app.quotation.baseProductId, function (product) {
      if (product && !product.error) {
        resolve(product);
      } else {
        reject(new Error('Fail to get Product ' + app.quotation.baseProductId + ' for ' + app._id));
      }
    });
  }).then(function (product) {
    var checkPromise = [];
    var insureds = _.get(app, 'quotation.insureds', {});

    _.forEach(insureds, function (insured, i) {
      checkPromise.push(new Promise(function (resolve, reject) {
        var _commonApp$getAgeForC = commonApp.getAgeForCheckingCrossAge(commonApp.CROSSAGE_MODE.NEXT_BIRTHDAY, insured.iAge, insured.iDob),
            currentAge = _commonApp$getAgeForC.currentAge,
            futureAge = _commonApp$getAgeForC.futureAge,
            ageCrossLine = _commonApp$getAgeForC.ageCrossLine;

        var result = {
          success: true,
          crossedAge: false,
          status: commonApp.CROSSAGE_STATUS.NO_CROSSAGE,
          allowBackdate: product.allowBackdate === 'Y',
          iCid: insured.iCid
        };

        if (currentAge - ageCrossLine >= 0) {
          // crossed age
          result.crossedAge = true;

          // if (!app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
          if (!app.isStartSignature) {
            result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED;
            resolve(result);
            // appStatus need to change to get from bundle
            // } else if (app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
          } else if (app.isStartSignature) {
            bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then(function (bApp) {

              if (_.get(bApp, 'appStatus') === 'APPLYING') {
                result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED;
              } else {
                result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NO_ACTION;
              }
              resolve(result);
            }).catch(function (err) {
              logger.error('ERROR: _checkCrossAge - getApplicationByBundleId', app._id, err);
              reject(err);
            });
          } else {
            //No this case for shield
            result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NO_ACTION;
            resolve(result);
          }
        } else if (currentAge - ageCrossLine === -1) {
          // will cross age soon
          var willCrossAgeInDays = ageCrossLine === futureAge;
          // if (willCrossAgeInDays && app.quotation.isBackDate === 'N' && !app.isBackDate && !app.isStartSignature) {
          if (willCrossAgeInDays && !app.isStartSignature) {
            result.status = commonApp.CROSSAGE_STATUS.WILL_CROSSAGE;
          } else {
            result.status = commonApp.CROSSAGE_STATUS.WILL_CROSSAGE_NO_ACTION;
          }
          resolve(result);
        } else {
          // will not cross age
          resolve(result);
        }
      }));
    });

    return Promise.all(checkPromise).then(function (results) {
      var isCrossedAge = _.findIndex(results, function (res) {
        return res.crossedAge;
      }) >= 0;

      var crossedAgeCid = [];
      _.forEach(results, function (res) {
        if (res.crossedAge) {
          crossedAgeCid.push(res.iCid);
        }
      });

      var showWillCrossAge = _.findIndex(results, function (res) {
        return res.status === commonApp.CROSSAGE_STATUS.WILL_CROSSAGE;
      }) >= 0;
      var showCrossedAgeUnsignedMsg = _.findIndex(results, function (res) {
        return res.status === commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED;
      }) >= 0;
      var showCrossedAgeSignedMsg = _.findIndex(results, function (res) {
        return res.status === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED;
      }) >= 0;

      var status = 0;
      if (showCrossedAgeSignedMsg) {
        status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED;
      } else if (showCrossedAgeUnsignedMsg) {
        status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED;
      } else if (showWillCrossAge) {
        status = commonApp.CROSSAGE_STATUS.WILL_CROSSAGE;
      }

      logger.log('INFO: _checkCrossAge - end', app._id, 'status=', status);
      return {
        success: true,
        crossedAge: isCrossedAge,
        crossedAgeCid: crossedAgeCid,
        allowBackdate: false,
        status: status
      };
    });
  });
};

var _getPolicyNumbers = function _getPolicyNumbers(application) {
  var result = {
    success: false,
    parentApp: _.cloneDeep(application),
    childApps: []
  };
  var childIds = _.get(application, 'childIds', []);

  return new Promise(function (resolve) {
    var numOfPolicyNumReq = childIds.length;
    commonApp.getPolicyNumberFromApi(numOfPolicyNumReq, true, function (policyNumbers) {
      if (policyNumbers) {
        if (policyNumbers.length === numOfPolicyNumReq) {
          resolve(policyNumbers);
        } else {
          logger.error('ERROR: _getPolicyNumbers - Exception occurs on getting policy number, count=' + numOfPolicyNumReq, application._id);
          resolve([]);
        }
      } else {
        logger.error('ERROR: _getPolicyNumbers - Fail to get policy number, count=' + numOfPolicyNumReq, application._id);
        resolve([]);
      }
    });
  }).then(function (policyNumbers) {

    return _getChildApplications(childIds).then(function (cApplications) {
      if (policyNumbers.length > 0) {
        _.forEach(cApplications, function (childApp, i) {
          var policyNumber = policyNumbers[i];
          childApp.policyNumber = policyNumber;

          var mappingList = result.parentApp.iCidMapping[childApp.iCid];
          _.forEach(mappingList, function (mapping) {
            if (mapping.applicationId === childApp.id) {
              mapping.policyNumber = policyNumber;
            }
          });
        });

        result.parentApp.isPolicyNumberGenerated = true;
        result.success = true;
      }
      result.childApps = [].concat(_toConsumableArray(cApplications));
      return result;
    });
  });
};

var _getPolicyNumbersMobile = function _getPolicyNumbersMobile(application, policyNumbers) {
  var result = {
    success: false,
    parentApp: _.cloneDeep(application),
    childApps: []
  };

  var childIds = _.get(application, 'childIds', []);

  return new Promise(function (resolve) {
    _getChildApplications(childIds).then(function (cApplications) {
      if (policyNumbers.length > 0) {
        _.forEach(cApplications, function (childApp, i) {
          var policyNumber = policyNumbers[i];
          childApp.policyNumber = policyNumber;

          var mappingList = result.parentApp.iCidMapping[childApp.iCid];
          _.forEach(mappingList, function (mapping) {
            if (mapping.applicationId === childApp.id) {
              mapping.policyNumber = policyNumber;
            }
          });
        });

        result.parentApp.isPolicyNumberGenerated = true;
        result.success = true;
      }
      result.childApps = [].concat(_toConsumableArray(cApplications));
      resolve(result);
    });
  });

  // return new Promise((resolve) => {
  //   let numOfPolicyNumReq = childIds.length;
  //   commonApp.getPolicyNumberFromApi(numOfPolicyNumReq, true, (policyNumbers) => {
  //     if (policyNumbers) {
  //       if (policyNumbers.length === numOfPolicyNumReq) {
  //         resolve(policyNumbers);
  //       } else {
  //         logger.error('ERROR: _getPolicyNumbers - Exception occurs on getting policy number, count=' + numOfPolicyNumReq, application._id);
  //         resolve([]);
  //       }
  //     } else {
  //       logger.error('ERROR: _getPolicyNumbers - Fail to get policy number, count=' + numOfPolicyNumReq, application._id);
  //       resolve([]);
  //     }
  //   });
  // }).then((policyNumbers) => {

  //   return _getChildApplications(childIds).then((cApplications) => {
  //     if (policyNumbers.length > 0) {
  //       _.forEach(cApplications, (childApp, i) => {
  //         let policyNumber = policyNumbers[i];
  //         childApp.policyNumber = policyNumber;

  //         let mappingList = result.parentApp.iCidMapping[childApp.iCid];
  //         _.forEach(mappingList, (mapping) => {
  //           if (mapping.applicationId === childApp.id) {
  //             mapping.policyNumber = policyNumber;
  //           }
  //         });
  //       });

  //       result.parentApp.isPolicyNumberGenerated = true;
  //       result.success = true;
  //     }
  //     result.childApps = [...cApplications];
  //     return result;
  //   });
  // });
};

var _saveAppForm = function _saveAppForm(data, session) {
  var appId = data.appId,
      changedValues = data.changedValues,
      policyNumber = data.policyNumber;
  var getPolicyNumber = data.getPolicyNumber;

  var cache = {
    application: {},
    bundle: {}
  };
  var result = {
    success: false,
    application: {},
    getPolicyNumberResult: false,
    updIds: []
  };
  var isFaChannel = session.agent.channel.type === 'FA';

  return _getApplication(appId).then(function (app) {
    return bDao.getCurrentBundle(app.pCid).then(function (bundle) {
      if (bundle && !bundle.error) {
        cache.application = app;
        cache.bundle = bundle;
        return;
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function () {
    var orgFormValues = cache.application.applicationForm.values;
    var diffs = CommonUtils.difference(orgFormValues, changedValues);

    if (Object.keys(diffs).length > 0) {
      logger.log('INFO: _saveAppForm - update diffs to bundle', _.get(cache.bundle, 'id'), 'for', appId);
      var proposer = changedValues.proposer;
      var proposers = [];
      if (proposer instanceof Object) {
        proposers.push(proposer);
      } else if (proposer instanceof Array) {
        proposers = proposer;
      }

      var cids = [];
      for (var j = 0; j < proposers.length; j++) {
        cids.push(proposers[j].personalInfo.cid);
        commonApp.updateClientFormValues(cache.bundle, proposers[j]);
      }

      var insured = changedValues.insured;
      for (var _j = 0; _j < insured.length; _j++) {
        var _insured = insured[_j];
        if (cids.length === 0 || cids.indexOf(_insured.personalInfo.cid) === -1) {
          commonApp.updateClientFormValues(cache.bundle, _insured);
        }
      }

      return true;
    }

    return false;
  }).then(function (isUpdateBundle) {
    return new Promise(function (resolve, reject) {
      if (isUpdateBundle) {
        logger.log('INFO: BundleUpdate - [cid:' + _.get(cache.application, 'pCid') + '; bundleId:' + _.get(cache.bundle, 'id') + '; fn:_saveAppForm]');
        bDao.updateBundle(cache.bundle, function (upRes) {
          if (upRes && !upRes.error) {
            resolve();
          } else {
            reject(new Error('Fail to update bundle' + _.get(upRes, 'error')));
          }
        });
      } else {
        resolve();
      }
    });
  }).then(function () {
    //populateBundle
    //update latest personalInfo data to client profile
    var _populatePersonalInfoToProfile = function _populatePersonalInfoToProfile(personalInfo, profile, cb) {
      logger.log('INFO: saveForm - _populatePersonalInfoToProfile', appId);

      var toUpdate = false;
      for (var i = 0; i < commonApp.CLIENT_FIELDS_TO_UPDATE.length; i++) {
        var field = commonApp.CLIENT_FIELDS_TO_UPDATE[i];
        if (personalInfo[field] !== profile[field]) {
          toUpdate = true;
          profile[field] = personalInfo[field];
        }
      }

      if (toUpdate) {
        result.updIds.push(profile._id);
        dao.updDoc(profile._id, profile, cb);
      } else {
        cb(true);
      }
    };

    var updProfilePromises = [];
    updProfilePromises.push(new Promise(function (resolve, reject) {
      var pPersonalInfo = _.get(changedValues, 'proposer.personalInfo');
      var cid = _.get(pPersonalInfo, 'cid');
      cDao.getProfile(cid, false).then(function (profile) {
        if (profile && !profile.error) {
          _populatePersonalInfoToProfile(pPersonalInfo, profile, function (upRes) {
            if (upRes && !upRes.error) {
              resolve();
            } else {
              reject(new Error('Fail to update Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
            }
          });
        } else {
          reject(new Error('Fail to get Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
        }
      });
    }));

    var insured = _.get(changedValues, 'insured');
    if (insured && insured.length > 0) {
      _.forEach(insured, function (la) {
        updProfilePromises.push(new Promise(function (resolve, reject) {
          var iPersonalInfo = _.get(la, 'personalInfo');
          var cid = _.get(la, 'personalInfo.cid');
          cDao.getProfile(cid, false).then(function (profile) {
            if (profile && !profile.error) {
              _populatePersonalInfoToProfile(iPersonalInfo, profile, function (upRes) {
                if (upRes && !upRes.error) {
                  resolve();
                } else {
                  reject(new Error('Fail to update Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
                }
              });
            } else {
              reject(new Error('Fail to get Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
            }
          });
        }));
      });
    }

    return Promise.all(updProfilePromises);
  }).then(function () {
    result.application = _.cloneDeep(cache.application);
    result.application.applicationForm.values = changedValues;
    //reset appStep if proposer is not completed but it's in Signature page now
    if (!_.get(changedValues, 'proposer.extra.isCompleted', false) && result.application.appStep === commonApp.EAPP_STEP.SIGNATURE) {
      result.application.appStep = commonApp.EAPP_STEP.APPLICATION;
    }

    var childIds = _.get(result.application, 'childIds', []);
    if (session.platform) {
      if (policyNumber && policyNumber.length) {
        getPolicyNumber = true;
      } else {
        getPolicyNumber = false;
      }
      var isGeneratePolicyNumber = getPolicyNumber && !result.application.isPolicyNumberGenerated;

      logger.log('INFO: _saveForm - getPolicyNumber isGen=', isGeneratePolicyNumber, 'for', appId);
      if (!isGeneratePolicyNumber) {
        result.getPolicyNumberResult = getPolicyNumber;
        return _getChildApplications(childIds).then(function (childApps) {
          return {
            parentApp: result.application,
            childApps: childApps
          };
        });
      } else {
        return _getPolicyNumbersMobile(result.application, policyNumber).then(function (res) {
          result.getPolicyNumberResult = res.success;
          result.application = res.parentApp;
          return {
            parentApp: res.parentApp,
            childApps: res.childApps
          };
        });
      }
    } else {
      var _isGeneratePolicyNumber = getPolicyNumber && !result.application.isPolicyNumberGenerated;

      logger.log('INFO: _saveForm - getPolicyNumber isGen=', _isGeneratePolicyNumber, 'for', appId);
      if (!_isGeneratePolicyNumber) {
        result.getPolicyNumberResult = getPolicyNumber;
        return _getChildApplications(childIds).then(function (childApps) {
          return {
            parentApp: result.application,
            childApps: childApps
          };
        });
      } else {
        return _getPolicyNumbers(result.application).then(function (res) {
          result.getPolicyNumberResult = res.success;
          result.application = res.parentApp;
          return {
            parentApp: res.parentApp,
            childApps: res.childApps
          };
        });
      }
    }
  }).then(function (apps) {
    if (!isFaChannel) {
      var parentApp = apps.parentApp;

      return bDao.updateApplicationFnaAnsToApplications(parentApp).then(function (results) {
        return apps;
      });
    } else {
      return apps;
    }
  }).then(function (apps) {
    _updateApplicationCompletedStep(apps.parentApp);
    _saveParentApplicationValuesToChild(apps.parentApp, apps.childApps);
    return [apps.parentApp].concat(_toConsumableArray(apps.childApps));
  }).then(function (applications) {
    var updAppPromises = applications.map(function (app) {
      return new Promise(function (resolve, reject) {
        app.lastUpdateDate = moment().toISOString();
        appDao.updApplication(app.id, app, function (res) {
          if (res && !res.error) {
            resolve(res);
          } else {
            reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
          }
        });
      });
    });
    return Promise.all(updAppPromises);
  }).then(function (upResults) {
    result.success = true;
    return result;
  });
};

var _genFnaReportPdf = function _genFnaReportPdf(session, application, bundle) {
  logger.log('INFO: _genFnaReportPdf', _.get(application, 'id'));
  var pCid = _.get(bundle, 'pCid');

  return new Promise(function (resolve, reject) {
    needsHandler.generateFNAReport({ cid: pCid }, session, function (pdfResult) {
      if (!pdfResult.fnaReport) {
        reject(new Error('Fail to generate FNA report'));
      } else {
        dao.uploadAttachmentByBase64(bundle.id, commonApp.PDF_NAME.FNA, bundle._rev, pdfResult.fnaReport, 'application/pdf', function (res) {
          if (res && !res.error) {
            bundle._rev = res.rev;
            resolve(bundle);
          } else {
            reject(new Error('Fail to upload FNA report ' + _.get(res, 'id') + ' ' + _.get(res, 'error')));
          }
        });
      }
    });
  }).then(function () {
    return bDao.updateStatus(pCid, bDao.BUNDLE_STATUS.START_GEN_PDF);
  });
};

var _getAppFormPdfTemplates = function _getAppFormPdfTemplates(application, isLa) {
  var appFormTemplate = null;

  return new Promise(function (resolve, reject) {
    dao.getDoc(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (mapping) {
      if (mapping && !mapping.error && mapping[application.quotation.baseProductCode]) {
        appFormTemplate = mapping[application.quotation.baseProductCode].appFormTemplate;
        resolve();
      } else {
        reject(new Error('Fail to get ' + commonApp.TEMPLATE_NAME.APP_FORM_MAPPING));
      }
    });
  }).then(function () {
    return new Promise(function (resolve, reject) {
      dao.getDocFromCacheFirst(appFormTemplate.main, function (main) {
        if (main && !main.error) {
          var tplFiles = [];
          commonApp.getReportTemplates(tplFiles, appFormTemplate.template, 0, function () {
            if (tplFiles.length) {
              resolve([main, tplFiles]);
            } else {
              reject(new Error('Fail to get app form template template'));
            }
          });
        } else {
          reject(new Error('Fail to get app form template main' + _.get(main, 'error')));
        }
      });
    });
  });
};

var _formatAppFormPdfData = function _formatAppFormPdfData(appFormValues, ccy) {
  var __formatPlanList = function __formatPlanList(planList) {
    _.forEach(planList, function (plan, i) {
      for (var key in plan) {
        if (key === 'halfYearPrem' || key === 'monthPrem' || key === 'premium' || key === 'quarterPrem' || key === 'yearPrem') {
          plan[key] = utils.getCurrency(plan[key], utils.getCurrencySign(ccy), 2);
        }
      }
    });
  };

  //format currency in planList
  // for proposer is la
  if (_.get(appFormValues, 'proposer.extra.isPhSameAsLa') === 'Y') {
    var basicPlanList = _.get(appFormValues, 'proposer.groupedPlanDetails.basicPlanList', []);
    if (basicPlanList.length > 0) {
      __formatPlanList(basicPlanList);
    }

    var riderPlanList = _.get(appFormValues, 'proposer.groupedPlanDetails.riderPlanList', []);
    if (riderPlanList.length > 0) {
      __formatPlanList(riderPlanList);
    }
  }

  var insured = _.get(appFormValues, 'insured', []);
  _.forEach(insured, function (la, i) {
    var basicPlanList = _.get(appFormValues, 'insured[' + i + '].groupedPlanDetails.basicPlanList', []);
    if (basicPlanList.length > 0) {
      __formatPlanList(basicPlanList);
    }

    var riderPlanList = _.get(appFormValues, 'insured[' + i + '].groupedPlanDetails.riderPlanList', []);
    if (riderPlanList.length > 0) {
      __formatPlanList(riderPlanList);
    }
  });

  //Uppercase personalInfo
  for (var key in appFormValues.proposer.personalInfo) {
    var info = appFormValues.proposer.personalInfo[key];
    if (typeof info === 'string' && key !== 'email' && key !== 'isSameAddr') {
      appFormValues.proposer.personalInfo[key] = info.toUpperCase();
    }
  }
  for (var i = 0; i < appFormValues.insured.length; i++) {
    for (var _key in appFormValues.insured[i].personalInfo) {
      var _info = appFormValues.insured[i].personalInfo[_key];
      if (typeof _info === 'string' && _key !== 'email' && _key !== 'isSameAddr') {
        appFormValues.insured[i].personalInfo[_key] = _info.toUpperCase();
      }
    }
  }
  //Uppercase payor & trusted individual information
  for (var _key2 in appFormValues.proposer.declaration) {
    var _info2 = appFormValues.proposer.declaration[_key2];
    if (typeof _info2 === 'string' && commonApp.CAPITAL_LETTER_IDS.indexOf(_key2) >= 0) {
      appFormValues.proposer.declaration[_key2] = _info2.toUpperCase();
    } else if (_key2 === 'trustedIndividuals') {
      appFormValues.proposer.declaration.trustedIndividuals.fullName = _.toUpper(appFormValues.proposer.declaration.trustedIndividuals.fullName);
    }
  }
};

var _genAppFormPdfSectionVisible = function _genAppFormPdfSectionVisible(values, isProposer, callback) {
  var showQuestions = {
    residency: {
      question: []
    },
    foreigner: {
      question: []
    },
    policies: {
      question: []
    },
    insurability: {
      question: []
    },
    declaration: {
      question: []
    }
  };

  var _loop2 = function _loop2(menuItemKey) {
    if (isProposer) {
      commonApp.checkAppFormByPerson(values.proposer, menuItemKey, showQuestions);
    } else {
      values.insured.forEach(function (la, index) {
        commonApp.checkAppFormByPerson(la, menuItemKey, showQuestions);
      });
    }
  };

  for (var menuItemKey in showQuestions) {
    _loop2(menuItemKey);
  }

  callback(showQuestions);
};

var _genAppFormPdfData = function _genAppFormPdfData(application, bundle, template, iCid) {
  var lang = 'en';
  var trigger = {};
  var appTemplate = template.items[0]; //contain items of menuSection
  var appOrgValues = _.cloneDeep(application.applicationForm.values);
  var appNewValues = _.cloneDeep(application.applicationForm.values);
  var isProposer = !_.isString(iCid);
  var ccy = _.get(appOrgValues, 'proposer.extra.ccy');

  logger.log('INFO: _genAppFormPdfData - replaceAppFormValuesByTemplate - start', application._id, iCid);
  commonApp.replaceAppFormValuesByTemplate(appTemplate, appOrgValues, appNewValues, trigger, [], lang, ccy);
  logger.log('INFO: _genAppFormPdfData - replaceAppFormValuesByTemplate - end', application._id, iCid);

  if (!isProposer) {
    //insured
    appOrgValues.insured = _.filter(appOrgValues.insured, function (la) {
      return _.get(la, 'personalInfo.cid') === iCid;
    });
    appNewValues.insured = _.filter(appNewValues.insured, function (la) {
      return _.get(la, 'personalInfo.cid') === iCid;
    });
  }
  appNewValues.lang = lang;
  appNewValues.baseProductCode = application.quotation.baseProductCode;
  appNewValues.iCidMapping = application.iCidMapping;
  appNewValues.showLaSignature = commonApp.checkLaIsAdult(application);
  appNewValues.agent = application.quotation.agent;

  if (application.agentCodeDisp) {
    appNewValues.agent.agentCodeDisp = application.agentCodeDisp;
  }
  if (application.nameOfOrganization) {
    appNewValues.agent.company = application.nameOfOrganization;
  } else {
    appNewValues.agent.company = '';
  }

  appNewValues.payment = application.payment;
  appNewValues.isProposer = isProposer;
  if (application.quotation.fund) {
    appNewValues.fund = application.quotation.fund;
  }

  _formatAppFormPdfData(appNewValues, ccy);

  for (var key in appOrgValues) {
    if (key === 'proposer') {
      logger.log('INFO: _genAppFormPdfData - removeAppFormValuesByTrigger: handle', key, application._id);
      commonApp.removeAppFormValuesByTrigger(key, appOrgValues[key], appNewValues[key], appOrgValues[key], trigger[key], true, appOrgValues);
    } else if (key === 'insured') {
      for (var i = 0; i < appOrgValues[key].length; i++) {
        var iOrgValues = appOrgValues[key][i];
        var iNewValues = appNewValues[key][i];
        var iTrigger = trigger[key][i];
        logger.log('INFO: _genAppFormPdfData - removeAppFormValuesByTrigger: handle', key, i, application._id);
        commonApp.removeAppFormValuesByTrigger(key, iOrgValues, iNewValues, iOrgValues, iTrigger, true, appOrgValues);
      }
    }
  }

  //add age to insured in for shield case, which is equal to quotation age
  var quotationProposerAge = _.get(application, 'quotation.pAge', 0);
  appNewValues.proposer.personalInfo.quotationAge = quotationProposerAge;

  var quotationInsured = _.get(application, 'quotation.insureds', []);
  for (var _i3 = 0; _i3 < appNewValues.insured.length; _i3++) {
    var personalInfo = appNewValues.insured[_i3].personalInfo;
    if (personalInfo) {
      var cid = appNewValues.insured[_i3].personalInfo.cid;
      if (quotationInsured[cid] && quotationInsured[cid].iAge) {
        var quotationAge = quotationInsured[cid].iAge;
        appNewValues.insured[_i3].personalInfo.quotationAge = quotationAge;
      }
    }
  }

  //Grouping HEALTH data into one array
  var groupingHealthTableData = function groupingHealthTableData(person, groupHealthKey, groupedKey) {
    var groupHealthData = [];

    for (var _i4 = 0; _i4 < groupHealthKey.length; _i4++) {
      var groupHealthDataKey = groupHealthKey[_i4] + '_DATA';

      var healthDataArray = _.get(person, 'insurability.' + groupHealthDataKey);
      if (healthDataArray) {
        for (var j = 0; j < healthDataArray.length; j++) {
          var healthDataRow = healthDataArray[j];
          var groupedRow = {};
          for (var _key3 in healthDataRow) {
            var postfix = _key3.replace(groupHealthKey[_i4], '');

            groupedRow['HEALTH_GROUP' + postfix] = healthDataRow[_key3];
            groupedRow.DATATABLE = groupHealthDataKey;
            groupedRow.SHOW_QN = j === 0 ? 'Y' : 'N';
            groupedRow.ROW_COUNT = healthDataArray.length;
          }
          groupHealthData.push(groupedRow);
        }
      }
    }

    if (groupHealthData.length > 0) {
      logger.log('INFO: genAppFormPdfData - groupingHealthTableData: length', groupHealthData.length, application._id, iCid);
      person.insurability[groupedKey] = groupHealthData;
    }
  };

  return new Promise(function (resolve) {

    if (isProposer) {
      groupingHealthTableData(appNewValues.proposer, commonApp.APP_FORM_SHIELD_GROUP_HEALTH_QN, 'HEALTH_GROUP_DATA');
      groupingHealthTableData(appNewValues.proposer, commonApp.APP_FORM_SHIELD_GROUP_HEALTH_F_QN, 'HEALTH_GROUP_DATA2');
    } else {
      var insured = appNewValues.insured;

      if (insured) {
        for (var _i5 = 0; _i5 < insured.length; _i5++) {
          groupingHealthTableData(appNewValues.insured[_i5], commonApp.APP_FORM_SHIELD_GROUP_HEALTH_QN, 'HEALTH_GROUP_DATA');
          groupingHealthTableData(appNewValues.insured[_i5], commonApp.APP_FORM_SHIELD_GROUP_HEALTH_F_QN, 'HEALTH_GROUP_DATA2');
        }
      }
    }

    _genAppFormPdfSectionVisible(appNewValues, isProposer, function (showQuestions) {
      appNewValues.showQuestions = showQuestions;
      resolve();
    });
  }).then(function () {
    return new Promise(function (resolve, reject) {
      var reportData = {
        root: {}
      };

      dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], function (pda) {
        if (pda && !pda.error) {
          logger.log('INFO: _genAppFormPdfData - populate PDA data', application._id, iCid);
          //For not FA channel
          if (pda.ownerConsentMethod) {
            var ownerConsentMethodList = pda.ownerConsentMethod.split(',');

            for (var _i6 = 0; _i6 < ownerConsentMethodList.length; _i6++) {
              var method = ownerConsentMethodList[_i6];
              if (method === 'phone') {
                appNewValues.proposer.declaration.PDPA01a = 'Yes';
              } else if (method === 'text') {
                appNewValues.proposer.declaration.PDPA01b = 'Yes';
              } else if (method === 'fax') {
                appNewValues.proposer.declaration.PDPA01c = 'Yes';
              }
            }
          }
        }

        reportData.root.reportData = appNewValues;
        reportData.root.originalData = appOrgValues;

        resolve(reportData);
      });
    });
  });
};

var _genAppFormProposerPdf = function _genAppFormProposerPdf(lang, application, bundle, template) {
  return new Promise(function (resolve) {
    resolve(!_.get(application, 'isAppFormProposerSigned'));
  }).then(function (isGenPdf) {
    logger.log('INFO: _genAppFormProposerPdf', _.get(application, 'id'), 'isGenPdf=', isGenPdf);
    if (!isGenPdf) {
      return isGenPdf;
    }

    return _getAppFormPdfTemplates(application, false).then(function (appFormTemplates) {
      // handle mutiple template
      var reportTemplates = [];
      for (var i = 0; i < appFormTemplates.length; i++) {
        if (appFormTemplates[i] instanceof Array) {
          reportTemplates.push.apply(reportTemplates, appFormTemplates[i]);
        } else {
          reportTemplates.push(appFormTemplates[i]);
        }
      }

      return _genAppFormPdfData(application, bundle, template).then(function (reportData) {
        return new Promise(function (resolve, reject) {
          PDFHandler.getAppFormPdf(reportData, reportTemplates, lang, function (pdfStr) {
            dao.uploadAttachmentByBase64(application._id, commonApp.PDF_NAME.eAPP, application._rev, pdfStr, 'application/pdf', function (res) {
              if (res && !res.error) {
                application._rev = res.rev;
                resolve(isGenPdf);
              } else {
                reject(new Error('Fail to update App Form PDF ' + application._id + ' ' + _.get(res, 'error')));
              }
            });
          });
        });
      });
    });
  });
};

var _genAppFormInsuredPdfPromise = function _genAppFormInsuredPdfPromise(lang, application, bundle, template, iCid, index) {
  return new Promise(function (resolve) {
    var isCompletedInsured = _.get(application, 'applicationForm.values.insured[' + index + '].extra.isCompleted', false);
    resolve(!_.get(application, 'isAppFormInsuredSigned[' + index + ']') && isCompletedInsured && application.iCids.length > 0);
  }).then(function (isGenPdf) {
    logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), iCid, 'isGenPdf=', isGenPdf);
    if (!isGenPdf) {
      return { isGenPdf: isGenPdf, iCid: iCid };
    }

    return _getAppFormPdfTemplates(application, true).then(function (appFormTemplates) {
      // handle mutiple template
      var reportTemplates = [];
      for (var i = 0; i < appFormTemplates.length; i++) {
        if (appFormTemplates[i] instanceof Array) {
          reportTemplates.push.apply(reportTemplates, appFormTemplates[i]);
        } else {
          reportTemplates.push(appFormTemplates[i]);
        }
      }

      return _genAppFormPdfData(application, bundle, template, iCid).then(function (reportData) {
        return new Promise(function (resolve, reject) {
          PDFHandler.getAppFormPdf(reportData, reportTemplates, lang, function (pdfStr) {
            resolve({ isGenPdf: isGenPdf, iCid: iCid, pdfStr: pdfStr });
          });
        });
      });
    });
  });
};

var _genAppFormInsuredPdf = function _genAppFormInsuredPdf(session, lang, application, bundle, template) {
  var genMultiplePdf = Promise.resolve([]);
  if (session.platform) {
    genMultiplePdf = _genAppFormInsuredPdfAsync(lang, application, bundle, template);
  } else {
    var genPdfPromise = application.iCids.map(function (iCid, index) {
      return _genAppFormInsuredPdfPromise(lang, application, bundle, template, iCid, index);
    });
    genMultiplePdf = Promise.all(genPdfPromise);
  }

  return genMultiplePdf.then(function (results) {
    var _uploadAttachment = function _uploadAttachment(index, callback) {
      if (index >= results.length) {
        logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), '- _uploadAttachment complete');
        callback();
      } else {
        logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), '- _uploadAttachment', index + 1, 'of', results.length);
        if (_.get(results, '[' + index + '].isGenPdf')) {
          var _results$index = results[index],
              iCid = _results$index.iCid,
              pdfStr = _results$index.pdfStr;

          dao.uploadAttachmentByBase64(application._id, _signature.getAppFormAttachmentName(iCid), application._rev, pdfStr, 'application/pdf', function (res) {
            if (res && !res.error) {
              application._rev = res.rev;
              results[index].success = true;
              _uploadAttachment(index + 1, callback);
            } else {
              results[index].success = false;
              _uploadAttachment(results.length, callback);
            }
          });
        } else {
          logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), '- empty result[' + index + ']');
          results[index].success = true;
          _uploadAttachment(index + 1, callback);
        }
      }
    };

    return new Promise(function (resolve, reject) {
      _uploadAttachment(0, function () {
        var filterUpResults = _.filter(results, function (res) {
          return !res.success;
        });
        if (filterUpResults.length > 0) {
          reject(new Error('Fail to upload attachment to application ' + application._id));
        } else {
          resolve(results);
        }
      });
    });
  });
};

var _generatePdf = function _generatePdf(session, data) {
  var appId = data.appId;

  var isFaChannel = session.agent.channel.type === 'FA';
  var lang = 'en';
  var agentCodeDisp = session.agent.agentCodeDisp;
  var upline2Code = session.agent.rawData.upline2Code;

  return _getApplication(appId).then(function (app) {
    return bDao.getCurrentBundle(app.pCid).then(function (bundle) {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(function (cache) {
    var application = cache.application,
        bundle = cache.bundle;

    if (agentCodeDisp) {
      application.agentCodeDisp = agentCodeDisp;
    }
    return _getNameOfOrganization(upline2Code).then(function (nameOfOrganization) {
      if (nameOfOrganization) {
        application.nameOfOrganization = nameOfOrganization;
      }
      return _getAppFormTemplate(application.quotation).then(function (templateRes) {
        return _genAppFormProposerPdf(lang, application, bundle, templateRes.template).then(function (isPhGenPdf) {
          return _genAppFormInsuredPdf(session, lang, application, bundle, templateRes.template).then(function (isLaGenPdfs) {
            return {
              proposer: isPhGenPdf,
              insured: isLaGenPdfs
            };
          });
        });
      });
    }).then(function (isGenPdfResult) {
      return _getApplication(appId).then(function (app) {
        if (app.appStep < commonApp.EAPP_STEP.SIGNATURE) {
          app.appStep = commonApp.EAPP_STEP.SIGNATURE;
        }

        if (!_.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated') && isGenPdfResult.proposer) {
          _.set(app, 'applicationForm.values.proposer.extra.isPdfGenerated', true);
        }

        _.forEach(_.get(app, 'applicationForm.values.insured', []), function (insured, index) {
          var isLaGenPdfResult = _.filter(isGenPdfResult.insured, function (la) {
            return la.iCid === _.get(insured, 'personalInfo.cid');
          });

          if (!_.get(insured, 'extra.isPdfGenerated') && isLaGenPdfResult.length > 0 && isLaGenPdfResult[0].isGenPdf) {
            _.set(insured, 'extra.isPdfGenerated', true);
          }
        });

        _updateApplicationCompletedStep(app);

        app.lastUpdateDate = moment().toISOString();
        return new Promise(function (resolve, reject) {
          appDao.updApplication(app.id, app, function (res) {
            if (res && !res.error) {
              app._rev = res.rev;
              resolve(app);
            } else {
              reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
            }
          });
        });
      });
    }).then(function (newApp) {
      if (isFaChannel) {
        return bDao.updateStatus(newApp.pCid, bDao.BUNDLE_STATUS.START_GEN_PDF).then(function (newBundle) {
          return newApp;
        });
      } else if (!bundle.isFnaReportSigned) {
        return _genFnaReportPdf(session, application, bundle).then(function (newBundle) {
          return newApp;
        });
      } else {
        return newApp;
      }
    }).then(function (newApp) {
      return { success: true, application: newApp };
    }).catch(function (error) {
      logger.error('ERROR: _generatePdf', appId, error);
      throw error;
    });
  });
};

var _updateApplicationCompletedStep = function _updateApplicationCompletedStep(app) {
  var appCompletedStep = -1;

  //Check step application
  var phIsCompleted = _.get(app, 'applicationForm.values.proposer.extra.isCompleted', false);
  var laIsComplated = true;
  var phIsPdfGenerated = _.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated', false);
  var laIsPdfGenerated = true;

  var insured = _.get(app, 'applicationForm.values.insured', []);
  _.forEach(insured, function (la) {
    laIsComplated = laIsComplated && _.get(la, 'extra.isCompleted', false);
    laIsPdfGenerated = laIsPdfGenerated && _.get(la, 'extra.isPdfGenerated', false);
  });

  if (phIsCompleted && laIsComplated && phIsPdfGenerated && laIsPdfGenerated) {
    appCompletedStep++;
  }

  if (appCompletedStep === commonApp.EAPP_STEP.APPLICATION) {
    //Check step signature
    if (app.isAppFormProposerSigned && (app.isAppFormInsuredSigned.length === 0 || app.isAppFormInsuredSigned.indexOf(false) < 0)) {
      appCompletedStep++;
    }
  }

  if (appCompletedStep === commonApp.EAPP_STEP.SIGNATURE) {
    //Check step payment
    if (_.get(app, 'payment.isCompleted', false)) {
      appCompletedStep++;
    }
  }

  if (appCompletedStep === commonApp.EAPP_STEP.PAYMENT) {
    if (_.get(app, 'isSubmittedStatus', false)) {
      appCompletedStep++;
    }
  }

  app.appCompletedStep = appCompletedStep;
};

//=ApplicationHandler.saveForm
module.exports.saveAppForm = function (data, session, callback) {
  var appId = data.appId;

  logger.log('INFO: saveAppForm - start', appId);

  _saveAppForm(data, session).then(function (result) {
    dao.updateViewIndex('main', 'summaryApps');
    logger.log('INFO: saveAppForm - end [RETURN=100]', appId);
    callback(result);
  }).catch(function (error) {
    logger.error('ERROR: saveAppForm - end [RETURN=-101]', appId, error);
    // callback({success: false});
  });
};

module.exports.goNextStep = function (data, session, callback) {
  var appId = data.appId,
      currIndex = data.currIndex,
      nextIndex = data.nextIndex;

  var cache = {
    application: {},
    saveResult: {},
    beforeNextResult: {}
  };
  var isFaChannel = session.agent.channel.type === 'FA';

  var saveProcess = [];
  if (currIndex === commonApp.EAPP_STEP.APPLICATION) {
    saveProcess.push(_saveAppForm(data, session));
  } else if (currIndex === commonApp.EAPP_STEP.PAYMENT) {
    saveProcess.push(_payment.savePaymentMethods(data));
  } else if (currIndex === commonApp.EAPP_STEP.SUBMISSION) {
    saveProcess.push(_submission.saveSubmissionValuesPromise(data, session));
  }

  Promise.all(saveProcess).then(function (saveResult) {
    logger.log('INFO: goNextStep - saveProcess completed', appId);
    if (saveResult.length > 0 && !saveResult[0]) {
      throw new Error('Fail to complete save process for step ' + currIndex + '->' + nextIndex);
    }
    cache.saveResult = saveResult[0];

    var beforeNextProcess = [];
    if (nextIndex === commonApp.EAPP_STEP.APPLICATION) {
      beforeNextProcess.push(_getApplication(appId));
    } else if (nextIndex === commonApp.EAPP_STEP.SIGNATURE) {
      beforeNextProcess.push(_generatePdf(session, data));
    } else if (nextIndex === commonApp.EAPP_STEP.PAYMENT) {
      beforeNextProcess.push(_payment.preparePayment(data));
    } else if (nextIndex === commonApp.EAPP_STEP.SUBMISSION) {
      beforeNextProcess.push(_submission.prepareSubmission(session, data));
    }

    return Promise.all(beforeNextProcess);
  }).then(function (beforeNextResult) {
    logger.log('INFO: goNextStep - beforeNextProcess completed', appId);
    if (beforeNextResult.length > 0 && !beforeNextResult[0]) {
      throw new Error('Fail to complete before Next process for step ' + currIndex + '->' + nextIndex);
    }
    cache.beforeNextResult = beforeNextResult[0];

    var nextProcess = [];
    if (nextIndex === commonApp.EAPP_STEP.APPLICATION) {
      cache.application = _.get(beforeNextResult, '[0]');
      nextProcess.push(_getAppFormTemplateWithOptionList(session, cache.application).then(function (templateRes) {
        return Object.assign(templateRes, { success: true });
      }));
    } else if (nextIndex === commonApp.EAPP_STEP.SIGNATURE) {
      cache.application = _.get(beforeNextResult, '[0].application');
      if (session.platform) {
        nextProcess.push(_signature.getSignatureInitPdfString(session, appId));
      } else {
        nextProcess.push(_signature.getSignatureInitUrl(session, appId));
      }
    } else if (nextIndex === commonApp.EAPP_STEP.PAYMENT) {
      cache.application = _.get(beforeNextResult, '[0]');
      nextProcess.push(_payment.getPaymentTemplate(session, cache.application.isSubmittedStatus, _.get(cache.application, 'payment.totCashPortion', 0), isFaChannel));
    } else if (nextIndex === commonApp.EAPP_STEP.SUBMISSION) {
      cache.application = _.get(beforeNextResult, '[0]');
      nextProcess.push(_submission.getSubmissionTemplate(session, cache.application.isSubmittedStatus));
    }

    return Promise.all(nextProcess);
  }).then(function (results) {
    logger.log('INFO: goNextStep - nextProcess completed', appId);
    if (results.length > 0 && !results[0].success) {
      throw new Error('Fail to complete Next process for step ' + currIndex + '->' + nextIndex);
    }
    var result = results[0];

    logger.log('INFO: goNextStep - end [RETURN=100]', appId);
    if (nextIndex === commonApp.EAPP_STEP.APPLICATION) {
      callback({ success: true, application: cache.application, template: result.template });
    } else if (nextIndex === commonApp.EAPP_STEP.SIGNATURE) {
      callback({ success: true, application: cache.application, signature: result.signature, updIds: cache.saveResult.updIds, warningMsg: result.warningMsg, crossAge: result.crossAge });
    } else if (nextIndex === commonApp.EAPP_STEP.PAYMENT) {
      callback({ success: true, application: cache.application, template: result.template });
    } else if (nextIndex === commonApp.EAPP_STEP.SUBMISSION) {
      callback({ success: true, application: cache.application, template: result.template });
    }
  }).catch(function (error) {
    logger.error('ERROR: goNextStep - end [RETURN=-101]', appId, error);
    // callback({success: false});
  });
};

module.exports.invalidateApplication = function (data, session, callback) {
  var appId = data.appId;

  logger.log('INFO: invalidateApplication - start', appId);
  _getApplication(appId).then(function (app) {
    return bDao.onInvalidateApplicationById(app.quotation.pCid, appId);
  }).then(function (resp) {
    logger.log('INFO: invalidateApplication - end [RETURN=100]', appId);
    callback({ success: true });
  }).catch(function (error) {
    logger.error('ERROR: invalidateApplication - end [RETURN=-100]', appId, error);
    // callback({success: false});
  });
};

module.exports.setSignExpiryShown = function (data, session, callback) {
  var appId = data.appId;

  logger.log('INFO: setSignExpiryShown - start', appId);

  _getApplication(appId).then(function (app) {
    app.signExpiryShown = true;
    app.lastUpdateDate = moment().toISOString();

    return new Promise(function (resolve, reject) {
      appDao.updApplication(app._id, app, function (res) {
        if (res && !res.error) {
          resolve();
        } else {
          reject(new Error('Fail to update application ' + app._id + ' ' + _.get(res, 'error')));
        }
      });
    });
  }).then(function () {
    logger.log('INFO: setSignExpiryShown - end [RETURN=100]', appId);
    callback({ success: true });
  }).catch(function (error) {
    logger.error('ERROR: setSignExpiryShown - end [RETURN=-100]', appId, error);
  });
};

module.exports.getApplication = _getApplication;
module.exports.getChildApplications = _getChildApplications;
module.exports.getAppFormTemplate = _getAppFormTemplate;
module.exports.getAppFormTemplateWithOptionList = _getAppFormTemplateWithOptionList;
module.exports.saveParentApplicationValuesToChild = _saveParentApplicationValuesToChild;
module.exports.updateApplicationCompletedStep = _updateApplicationCompletedStep;
module.exports.getPolicyNumbers = _getPolicyNumbers;
module.exports.checkCrossAge = _checkCrossAge;