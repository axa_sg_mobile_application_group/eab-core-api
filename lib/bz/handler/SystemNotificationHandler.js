'use strict';

var _ = require('lodash');

var notifyDao = require('../cbDao/systemNotification');
var async = require('async');

var logger = global.logger;

var getValidSystemNotification = function getValidSystemNotification(compCode, cb) {
  var currentDateTime = Date.parse(new Date().toUTCString());
  var expiryDate = Date.parse(new Date('9999-12-31').toISOString());
  async.waterfall([function (callback) {
    var promises = [];
    promises.push(notifyDao.getValidEndNotification(compCode, currentDateTime, expiryDate));

    Promise.all(promises).then(function (data) {
      var validMessages = [];
      var message = void 0;
      var messageId = void 0;
      _.each(data, function (campaignView, key) {
        _.each(campaignView && campaignView.rows, function (rowObj) {
          message = _.get(rowObj, 'value');
          messageId = _.get(rowObj, 'value.messageId');
          if (message && message.startTime && currentDateTime > Date.parse(message.startTime) && messageId) {
            validMessages.push(message);
          }
        });
      });
      callback(null, validMessages);
    });
  }], function (err, validMessage) {
    if (err) {
      logger.error('ERROR in getvalidMessage: ', err);
      cb([]);
    } else {
      cb(validMessage);
    }
  });
};

module.exports.getValidSystemNotification = getValidSystemNotification;