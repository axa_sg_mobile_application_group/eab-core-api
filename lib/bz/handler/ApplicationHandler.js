'use strict';

var _axiosWrapper = require('../../../../../app/utilities/axiosWrapper');

var _axiosWrapper2 = _interopRequireDefault(_axiosWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ = require('lodash');

var dao = require('../cbDaoFactory').create();
var applicationDao = require('../cbDao/application');
var quotDao = require('../cbDao/quotation');
var prodDao = require('../cbDao/product');
var nDao = require('../cbDao/needs');
var bDao = require('../cbDao/bundle');
// const appFormMapping = 'application_form_mapping';

//Add by Alex Tse for CR49 start ***********************
var productPaymentMapping = 'product_to_payment_method';
//Add by Alex Tse for CR49 end *************************

var clientDao = require('../cbDao/client');
var needsHandler = require('../NeedsHandler');
var PDFHandler = require('../PDFHandler');
var ApprovalHandler = require('../ApprovalHandler');
var aEmaillHandler = require('../ApprovalNotificationHandler');
var clientChoiceHandler = require('./ClientChoiceHandler');
var Iconv = require('iconv').Iconv;
var crypto = require('crypto');
var async = require('async');
var CommonFunctions = require('../CommonFunctions');
var approvalStatusModel = require('../model/approvalStatus');
var moment = require('moment');
var Exception = require('../model/Exception');
var eAppConstants = require('../../common/EAppConstants');

var _require = require('../../common/ProductUtils'),
    isCrossBorder = _require.isCrossBorder;

var _require2 = require('../utils/TokenUtils'),
    createPdfToken = _require2.createPdfToken,
    setPdfTokensToRedis = _require2.setPdfTokensToRedis;

var SuppDocsUtils = require('../utils/SuppDocsUtils');
var commonApp = require('./application/common');
var defaultApplication = require('./application/default/application');
var defaultSignature = require('./application/default/signature');
var defaultSupportDocument = require('./application/default/supportDocument');
var shieldSupportDocument = require('./application/shield/supportDocument');
var shieldSignature = require('./application/shield/signature');
var payment = require('../handler/application/shield/payment');

var _require3 = require('../utils/RemoteUtils'),
    callApi = _require3.callApi,
    callApiByGet = _require3.callApiByGet,
    callApiComplete = _require3.callApiComplete;

var logger = global.logger || console;

//TO-DO: MOVE to ./application/common.js
var fnaPdfName = 'fnaReport';
var proposalPdfName = 'proposal';
var appPdfName = 'appPdf';
var eCpdPdfName = 'eCpdPdf';
// const coverPdfName = 'cover';

var initCrossBorder = function initCrossBorder(app) {
  var quotation = app.quotation;

  var pCrossBorder = isCrossBorder(quotation, app, true, true);
  var iCrossBorder = isCrossBorder(quotation, app, false, true);
  _.set(app, 'iCrossBorder', iCrossBorder);
  _.set(app, 'pCrossBorder', pCrossBorder);
  _.set(app, 'isCrossBorder', iCrossBorder || pCrossBorder);
  _.set(app, 'applicationForm.values.proposer.extra.isCrossBorder', iCrossBorder || pCrossBorder);
};

var getLabel = function getLabel(appList, callback) {
  var rpPaymentMethod = ['A', 'S', 'Q', 'M'];
  var spPaymentMethod = ['L'];
  var lang = 'en';

  appList.forEach(function (item) {
    var nameLabel = 'RP';
    var paymentMode = item.type === 'quotation' ? _.get(item, 'paymentMode') : _.get(item, 'quotation.paymentMode');
    var isShield = (item.type === 'quotation' ? _.get(item, 'quotType') : _.get(item, 'quotation.quotType')) === 'SHIELD';
    if (isShield || rpPaymentMethod.indexOf(paymentMode) > -1) {
      nameLabel = 'RP';
    } else if (spPaymentMethod.indexOf(paymentMode) > -1) {
      nameLabel = item.type === 'quotation' ? _.get(item, 'policyOptionsDesc.paymentMethod.' + lang, 'SP') : _.get(item, 'quotation.policyOptionsDesc.paymentMethod.' + lang, 'SP');
    }

    item.paymentMethod = nameLabel;
  });

  callback();
};

module.exports.getAppListView = function (data, session, cb) {
  var cid = _.get(data, 'profile.cid');
  var productInvalidList = [];
  logger.log('INFO: getAppListView - start', cid);

  var getSupervisorApproval = function getSupervisorApproval(appList, bundleAppList) {
    logger.log('INFO: getAppListView - getSupervisorApproval', cid);
    return new Promise(function (resolve) {
      var promises = [];
      _.forEach(appList, function (app) {
        promises.push(new Promise(function (resolve2) {
          var matchedBundleApp = _.find(bundleAppList, function (bundleApp) {
            return bundleApp.applicationDocId === app.id;
          });
          if (_.get(app, 'type') === 'masterApplication') {
            _.set(app, 'policyNumber', _.replace(app.id, 'SA', 'SP'));
          }
          if (_.get(app, 'policyNumber') && _.get(matchedBundleApp, 'appStatus') === 'SUBMITTED') {
            // approvalStatus
            ApprovalHandler.searchApprovalCaseById({ id: app.policyNumber }, session, function (approvalCase) {
              if (approvalCase.foundCase) {
                if (['A', 'E', 'R'].includes(_.get(approvalCase.foundCase, 'approvalStatus')) || _.get(matchedBundleApp, 'appStatus') === 'INVALIDATED_SIGNED') {
                  app.approvalStatus = _.get(approvalStatusModel.STATUS, _.get(approvalCase.foundCase, 'approvalStatus'), '');
                  dao.getDoc('U_' + _.get(approvalCase.foundCase, 'agentProfileId') + '_RLSSTATUS', function (rlsCb) {
                    var isTransferred = false;
                    if (rlsCb.RLSSTATUS) {
                      if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                        isTransferred = _.get(approvalCase.foundCase, 'policiesMapping', []).every(function (policy) {
                          return (rlsCb.RLSSTATUS[policy.policyNumber] || {}).rlsSuccess;
                        });
                      } else {
                        isTransferred = (rlsCb.RLSSTATUS[_.get(approvalCase.foundCase, 'policyId')] || {}).rlsSuccess;
                      }
                    }
                    app.approvalStatus = isTransferred ? eAppConstants.PAYMENTTRANSFERRED : app.approvalStatus;
                    resolve2(app);
                  });
                } else {
                  app.approvalStatus = _.get(approvalStatusModel.STATUS, _.get(approvalCase.foundCase, 'approvalStatus'), '');
                  resolve2(app);
                }
              }
            });
          } else if (_.get(app, 'policyNumber') && _.get(matchedBundleApp, 'appStatus') === 'INVALIDATED_SIGNED') {
            dao.getDoc('U_' + _.get(session, 'agent.profileId') + '_RLSSTATUS', function (rlsCb) {
              var isTransferred = false;
              if (rlsCb.RLSSTATUS) {
                if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                  var policyIdArray = [];
                  _.each(_.get(app, 'iCidMapping', []), function (cid) {
                    _.each(cid, function (policyNumber) {
                      return policyIdArray.push(policyNumber);
                    });
                  });
                  isTransferred = policyIdArray.every(function (policyNumber) {
                    return (rlsCb.RLSSTATUS[policyNumber] || {}).rlsSuccess;
                  });
                } else {
                  isTransferred = (rlsCb.RLSSTATUS[_.get(app, 'policyNumber')] || {}).rlsSuccess;
                }
              }
              app.approvalStatus = isTransferred ? eAppConstants.PAYMENTTRANSFERRED : app.approvalStatus;
              resolve2(app);
            });
          } else {
            resolve2(app);
          }
        }));
      });
      Promise.all(promises).then(function (results) {
        resolve(results);
      }).catch(function (error) {
        logger.error('Error in getAppListView->Promise.all: ', error);
      });
    });
  };

  var checkFNACompleted = function checkFNACompleted(cid) {
    logger.log('INFO: getAppListView - checkFNACompleted', cid);
    return new Promise(function (resolve) {
      nDao.isFNACompleted(cid).then(function (isFNACompleted) {
        resolve(isFNACompleted);
      }).catch(function (error) {
        logger.error("Error in checkFNACompleted->nDao.isFNACompleted: ", error);
      });
    });
  };

  bDao.getBundle(_.get(data, 'profile.cid'), data.bundleId, function (bundle) {
    if (bundle) {
      applicationDao.getAppListView('01', data.profile.cid, bundle.id).then(function (rawAppList) {
        var productList = _.map(rawAppList, function (obj) {
          return obj.baseProductCode;
        });
        prodDao.prepareLatestProductVersion(productList).then(function (result) {
          getSupervisorApproval(rawAppList, bundle.applications).then(function (appList) {
            _.each(appList, function (app) {
              if (result[app.baseProductCode] && result[app.baseProductCode].success) {
                app.version = result[app.baseProductCode].version || 1;
              } else {
                app.version = null;
              }
              if (!result[app.baseProductCode].success) {
                productInvalidList.push(app.baseProductCode);
              }
            });
            //Add by Alex Tse for CR49 Start ***
            getLabel(appList, function () {
              logger.log('INFO: getAppListView - getInfoForAppList end', cid);

              var _getQuotCheckedList = getQuotCheckedList(appList, bundle),
                  isClientChoiceCompleted = _getQuotCheckedList.isClientChoiceCompleted,
                  quotCheckedList = _getQuotCheckedList.quotCheckedList;

              var agentChannel = session.agent.channel.type;

              if (agentChannel === 'FA') {
                // FA Channel does not have FNA, so isFNACompleted set to true
                clientDao.getAllDependantsProfile(data.profile.cid).then(function (dependantProfiles) {
                  logger.log('INFO: getAppListView - end [RETURN=1]', cid);
                  cb({
                    success: true,
                    applicationsList: appList,
                    pdaMembers: getPdaMember(data.profile, dependantProfiles),
                    clientChoiceFinished: isClientChoiceCompleted,
                    quotCheckedList: quotCheckedList,
                    agentChannel: agentChannel,
                    isFNACompleted: true,
                    productInvalidList: productInvalidList
                  });
                }).catch(function (error) {
                  logger.error('ERROR: getAppListView - getAllDependantsProfile', cid, error);
                });
              } else {
                checkFNACompleted(_.get(data, 'profile.cid')).then(function (isFNACompleted) {
                  var needData = data;
                  needData.cid = data.profile.cid;
                  needData.action = 'initNeeds';
                  needsHandler.initNeeds(needData, session, function (resp) {
                    logger.log('INFO: getAppListView - end [RETURN=2]', cid);
                    cb({
                      success: true,
                      applicationsList: appList,
                      pdaMembers: getPdaMember(data.profile, resp.dependantProfiles, resp.pda),
                      clientChoiceFinished: isClientChoiceCompleted,
                      quotCheckedList: quotCheckedList,
                      agentChannel: agentChannel,
                      isFNACompleted: isFNACompleted,
                      productInvalidList: productInvalidList
                    });
                  });
                }).catch(function (error) {
                  logger.error('ERROR: getAppListView - checkFNACompleted', cid, error);
                });
              }
            });
            //Add by Alex Tse for CR49 End****
          }).catch(function (err) {
            logger.error('ERROR: getAppListView - getInfoForAppList', cid, err);
          });
        }).catch(function (getProductVersionError) {
          logger.error('ERROR: getAppListView :: getProductVersionError', cid, getProductVersionError);
        });
      }).catch(function (err) {
        logger.error('ERROR: getAppListView', cid, err);
      });
    } else {
      logger.log('INFO: getAppListView - CANNOT GET BUNDLE');
      cb({ success: false });
    }
  });
};
/**FUNCTION ENDS: getAppListView */

var getPdaMember = function getPdaMember(profile, dependantProfiles, pda) {

  // let {profile, dependantProfiles} = client;
  // let {pda} = needs;

  var result = [];
  if (profile) {
    result.push({
      cid: profile.cid,
      fullName: profile.fullName,
      relationship: "OWNER"
    });
  }

  if (pda) {
    var _deps = pda.dependants && pda.dependants.split(",");
    var _jointSpo = "";

    if (pda.applicant === "joint") {
      profile.dependants.filter(function (dependant) {
        if (dependant.relationship == "SPO") {
          var _obj = JSON.parse(JSON.stringify(dependant));
          // let _obj = _.cloneDeep(dependant);
          _obj.relationship = "SPOUSE";
          _obj.fullName = dependantProfiles[_obj.cid].fullName;
          _jointSpo = _obj.cid;
          result.push(_obj);
        }
      });
    }

    if (_deps) {
      profile.dependants && profile.dependants.filter(function (dependant) {
        if (_deps.indexOf(dependant.cid) > -1 && dependant.cid != _jointSpo) {
          var _obj = JSON.parse(JSON.stringify(dependant));
          // let _obj = _.cloneDeep(dependant);
          _obj.filter = _obj.relationship;
          _obj.relationship = "DEPENDANTS";
          _obj.fullName = dependantProfiles[_obj.cid].fullName;
          // _obj.fullName = _.at(dependantProfiles, `${_obj.cid}.fullName`)[0];
          result.push(_obj);
        }
      });
    }
  } else {
    profile.dependants && profile.dependants.filter(function (dependant) {
      if (dependantProfiles[dependant.cid]) {
        var _obj = _.cloneDeep(dependant);
        _obj.filter = _obj.relationship;
        _obj.relationship = 'DEPENDANTS';
        _obj.fullName = dependantProfiles[_obj.cid].fullName;
        // _obj.fullName = _.at(dependantProfiles, `${_obj.cid}.fullName`)[0];
        result.push(_obj);
      }
    });
  }

  return result;
};

//MOVE to ./application/common.js
// return a array of policy number
var getPolicyNumber = function getPolicyNumber(numOfPolicyNumber, cb) {
  commonApp.getPolicyNumberFromApi(numOfPolicyNumber, false, cb);
};
module.exports.getPolicyNumber = getPolicyNumber;

module.exports.saveForm = function (data, session, cb) {
  defaultApplication.saveAppForm(data, session, cb);
};

// let savePersonalDetailsToClientProfile = function(cid, appFormInfo){
//   logger.log('INFO: savePersonalDetailsToClientProfile starts');
//   const whiteList = ['A', 'B'];
//   return new Promise((resolve)=>{
//     clientDao.getProfile(cid, true).then((clientProfile)=>{
//       _.forEach(whiteList, (id)=>{
//         clientProfile[id] = _.get(appFormInfo, id) || _.get(clientProfile, id);
//       });
//       clientDao.updateProfile(cid, clientProfile).then(()=>{
//         logger.log('INFO: savePersonalDetailsToClientProfile ends');
//         resolve();
//       });
//     });
//   });
// };

// let getPersonalDetailsFromClientProfile = function(){

// };

module.exports.addOtherDocument = function (data, session, cb) {
  var rootValues = data.rootValues;
  var tabId = data.tabId;
  var values = void 0;
  var changedValues = rootValues[tabId];
  var docName = data.docName ? data.docName : data.docNameOption;
  var docNameOption = data.docNameOption ? data.docNameOption : 'Other';
  if (docName && typeof docName === 'string') {
    docName = docName.trim();
  }
  var docNameTitle = docNameOption !== 'Other' ? docNameOption : docName;

  if (docNameTitle && typeof docNameTitle === 'string') {
    docNameTitle = docNameTitle.trim();
  }
  var docId = '';
  var docTitleId = 'otherDoc';
  if (docNameTitle && typeof docNameTitle === 'string') {
    if (docNameTitle.replace(/\s+/g, '') !== '') {
      docTitleId = docNameTitle.replace(/\s+/g, '');
    }
  }
  docId = docTitleId + '_' + new Date().getTime();
  var appId = data.appId;

  logger.log('INFO: addOtherDocument - start', appId);
  applicationDao.getApplication(data.appId, function (app) {
    defaultSupportDocument.validateOtherDocumentName(docName, app, data.tabId).then(function (isDuplicated) {
      if (!isDuplicated) {
        if (!app.error) {
          if (changedValues['otherDoc']) {
            changedValues.otherDoc.template.push({
              'type': 'subSection',
              'id': docId,
              'title': docNameTitle,
              'docNameOption': docNameOption,
              'docName': docName
            });
            _.set(changedValues, "otherDoc.values['docId']", []);
          } else {
            changedValues['otherDoc'] = {};
            changedValues.otherDoc['template'] = [];
            changedValues.otherDoc.template.push({
              "type": "subSection",
              "id": docId,
              "title": docNameTitle,
              'docNameOption': docNameOption,
              'docName': docName
            });
            _.set(changedValues, "otherDoc['values']", {});
            _.set(changedValues, "otherDoc.values['docId']", []);
          }
          var suppDoc = app['supportDocuments'];
          // The support documents values will be updated after Submit button clicked
          bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then(function (bApp) {
            if (_.get(bApp, 'appStatus') === 'SUBMITTED') {
              values = rootValues;
              values[tabId] = changedValues;

              logger.log('INFO: addOtherDocument - end [RETURN=1]', appId);
              cb({
                success: true,
                values: values
              });
            } else {
              values = _.get(app, 'supportDocuments.values');
              values[tabId] = changedValues;

              suppDoc['values'] = values;
              app['supportDocuments'] = suppDoc;
              applicationDao.upsertApplication(app._id, app, function (resp) {
                if (resp) {
                  logger.log('INFO: addOtherDocument - end [RETURN=2]', appId);
                  cb({
                    success: true,
                    values: values
                  });
                } else {
                  logger.error('ERROR: addOtherDocument - end [RETURN=-3]', appId);
                  cb({ success: false });
                }
              });
            }
          }).catch(function (err) {
            logger.error('ERROR: addOtherDocument - end [RETURN=-2]', appId, err);
            cb({ success: false });
          });
        } else {
          logger.error('ERROR: addOtherDocument - end [RETURN=-1]', appId);
          cb({ success: false });
        }
      } else {
        logger.log('INFO: addOtherDocument - end [RETURN=3] - documentName is duplicated', appId);
        cb({
          success: true,
          duplicated: true
        });
      }
    }).catch(function (error) {
      logger.error('Error in addOtherDocument - validateOtherDocumentName: ', error);
    });
  });
};

module.exports.updateOtherDocumentName = function (data, session, cb) {
  defaultSupportDocument.updateOtherDocumentName(data, session, cb);
};

module.exports.submitSuppDocsFiles = function (data, session, cb) {
  logger.log('INFO: submitSuppDocsFiles - start' + data.appId);
  var localTime = CommonFunctions.getLocalTime();
  var timeLabel = localTime.format("dd/mm/yyyy HH:MM:ss");
  var sendHealthDeclarationNoti = void 0;

  var updateViewedList = function updateViewedList(viewedList, pendingSubmitList) {
    _.forEach(viewedList, function (viewedListObj, agentCode) {
      _.forEach(pendingSubmitList, function (fileId) {
        if (data.isSupervisorChannel && session.agentCode === agentCode) {
          viewedListObj[fileId] = true;
        } else {
          viewedListObj[fileId] = false;
        }
      });
    });
  };

  var hasHealthDeclarationFile = function hasHealthDeclarationFile(supportDocumentValues, pendingSubmitList) {
    var uploadedFiles = _.get(supportDocumentValues, 'policyForm.mandDocs.healthDeclaration');
    var isContain = false;

    for (var index in uploadedFiles) {
      if (_.includes(pendingSubmitList, uploadedFiles[index].id)) {
        isContain = true;
        break;
      }
    }

    return isContain;
  };

  var setSysDocsTime = function setSysDocsTime(app) {
    if (app.supportDocuments.values.policyForm && app.supportDocuments.values.policyForm.sysDocs) {
      var docs = app.supportDocuments.values.policyForm.sysDocs;
      for (var i in docs) {
        docs[i]["uploadDate"] = timeLabel;
      }
    }
  };

  applicationDao.getApplication(data.appId, function (app) {
    if (!app.error) {
      if (!data.isDocsFromApproval) {
        app.supportDocuments.values = data.values;
      } else {
        if (data.uploadAtt && data.uploadAtt === 'coe') {
          logger.log('INFO: submitSuppDocsFiles - coe');
          if (_.get(app, 'supportDocuments.values.policyForm.mandDocs')) {
            app.supportDocuments.values.policyForm.mandDocs.pCertOfEndorsement = data.pCertOfEndorsement;
          }
        }
      }
      if (app.supportDocuments.pendingSubmitList) {
        sendHealthDeclarationNoti = hasHealthDeclarationFile(data.values, app.supportDocuments.pendingSubmitList);
        // update all agents' viewedList
        updateViewedList(app.supportDocuments.viewedList, app.supportDocuments.pendingSubmitList);
        delete app.supportDocuments['pendingSubmitList'];
      }
      // app.supportDocuments.viewedList[session.agentCode] = data.viewedList;
      app.supportDocuments.isAllViewed = false;
      setSysDocsTime(app);
      applicationDao.upsertApplication(data.appId, app, function (resp) {
        if (resp) {
          logger.log('INFO: submitSuppDocsFiles - end [RETURN=1]' + data.appId);
          commonApp.updateChildSupportDocumentValue(app, function (error, parentApplication) {
            if (error === null) {
              logger.log('INFO: submitSuppDocsFiles - After updateChildSupportDocumentValue - end [RETURN=1] :: Success' + data.appId);
              cb({
                success: true,
                pendingSubmitList: [],
                sendHealthDeclarationNoti: sendHealthDeclarationNoti
              });
            } else {
              logger.error('INFO: submitSuppDocsFiles - After updateChildSupportDocumentValue - end [RETURN=1] :: Failed' + data.appId + error);
              cb({
                success: false
              });
            }
          });
        } else {
          logger.error('ERROR: submitSuppDocsFiles - end [RETURN=-2]' + data.appId);
          cb({ success: false });
        }
      });
    } else {
      logger.error('ERROR: submitSuppDocsFiles - end [RETURN=-1]' + data.appId);
      cb({ success: false });
    }
  });
};

var _checkUserUploadSuppDocFileSize = function _checkUserUploadSuppDocFileSize(data, session) {
  return new Promise(function (resolve, reject) {
    // Get current upload file size
    var totalFileSize = _.get(data, 'attachmentValues.length', 0);
    applicationDao.getApplication(data.applicationId, function (application) {
      var attachments = _.get(application, '_attachments');
      _.each(attachments, function (individualAtt, key) {
        // Identify this is upload by user
        if (key && key.length === 22 && key.indexOf('suppDocs') === 0 && _.isNumber(individualAtt.length)) {
          totalFileSize += individualAtt.length;
        }
      });
      var isShield = _.get(application, 'quotation.quotType') === 'SHIELD';
      var noOfProposer = 1;
      var noOfLA = isShield ? _.keys(_.get(application, 'iCidMapping')).length : 1;

      totalFileSize <= global.config.SUPPORT_DOCUMENTS_MAX_FILE_SIZE_IN_BYTE * (noOfProposer + noOfLA) ? resolve(true) : resolve(false);
    });
  });
};

module.exports.upsertSuppDocsFile = function (data, session, cb) {
  var rootValues = data.rootValues;

  _checkUserUploadSuppDocFileSize(data, session).then(function (allowUpload) {
    if (allowUpload) {
      logger.log('INFO: upsertSuppDocsFile - start', data.applicationId);
      var isSubmittedApp = false;

      var genFileProps = function genFileProps(attachmentValues) {
        var fileName = attachmentValues.fileName,
            fileSize = attachmentValues.fileSize,
            fileType = attachmentValues.fileType,
            length = attachmentValues.length;

        var serverTime = new Date();
        var localTime = CommonFunctions.getLocalTime();
        var fileProps = {
          "id": 'suppDocs' + localTime.format("yyyymmddHHMMss"),
          "title": localTime.format("yyyymmdd") + "_" + fileName,
          "fileSize": fileSize,
          "uploadDate": localTime.format('dd/mm/yyyy HH:MM:ss'),
          'backendUploadDate': localTime.format('yyyy-mm-dd\'T\'HH:MM:ss\'Z\''),
          "fileType": fileType,
          "length": length
        };
        return fileProps;
      };

      var updateViewedList = function updateViewedList(viewedList, attId) {
        if (data.isSupervisorChannel) {
          viewedList[attId] = true;
        } else {
          viewedList[attId] = false;
        }
      };

      var updateSuppDocs = function updateSuppDocs(isSubmittedApp, application, fileProps, valueLocation, tabId, suppDocsValues, viewedList, cb) {
        addSupportDocuments(isSubmittedApp, application, fileProps, valueLocation, tabId, suppDocsValues, session.agentCode, rootValues, function (resp) {
          if (resp.success == true) {
            // if (isSubmittedApp) {
            updateViewedList(viewedList, fileProps.id);
            // }

            var tokens = [createPdfToken(application.id, fileProps.id, Date.now(), session.loginToken)];
            var tokensMap = {};
            tokensMap[fileProps.id] = tokens[0].token;
            if (session.platform === "ios") {
              commonApp.updateChildSupportDocumentValue(application, function () {
                logger.log('INFO: upsertSuppDocsFile - end [RETURN=1]', data.applicationId);
                cb({
                  success: true,
                  values: resp.values,
                  viewedList: viewedList,
                  pendingSubmitList: resp.pendingSubmitList,
                  tokensMap: tokensMap
                });
              });
            } else {
              setPdfTokensToRedis(tokens, function () {
                commonApp.updateChildSupportDocumentValue(application, function () {
                  logger.log('INFO: upsertSuppDocsFile - end [RETURN=1]', data.applicationId);
                  cb({
                    success: true,
                    values: resp.values,
                    viewedList: viewedList,
                    pendingSubmitList: resp.pendingSubmitList,
                    tokensMap: tokensMap
                  });
                });
              });
            }
          } else {
            logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-4]', data.applicationId);
            cb({ success: false });
          }
        });
      };

      var attachments = {};
      var fileProps = genFileProps(data.attachmentValues);
      var subStrPattern = "base64,";
      var index = data.attachment.indexOf(subStrPattern);

      attachments[fileProps.id] = data.attachment.substring(index + subStrPattern.length);
      applicationDao.getApplication(data.applicationId, function (application) {
        if (!application.error) {
          bDao.getApplicationByBundleId(_.get(application, 'bundleId'), application.id).then(function (bApp) {
            if (bApp) {
              if (_.get(bApp, 'appStatus') === 'SUBMITTED') {
                isSubmittedApp = true;
              }
              applicationDao.upsertAppAttachments(data.applicationId, application._rev,
              // Convert attachment into attachments(array) for upsertAppAttachments inparm
              attachments, function (res) {
                if (res.success == true) {
                  updateSuppDocs(isSubmittedApp, application, fileProps, data.valueLocation, data.tabId, data.suppDocsValues, data.viewedList, cb);
                }
              });
            } else {
              logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-3]', data.applicationId);
              cb({ success: false });
            }
          }).catch(function (err) {
            logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-2]', data.applicationId, err);
            cb({ success: false });
          });
        } else {
          logger.error('ERROR: upsertSuppDocsFile - end [RETURN=-1]', data.applicationId);
          cb({ success: false });
        }
      });
    } else {
      cb({
        success: true,
        showTotalFilesSizeLargeDialog: true
      });
    }
  }).catch(function (error) {
    logger.error('ERROR: upsertSuppDocsFile ' + error);
  });
};
/**FUNCTION ENDS: upsertSuppDocsFile */

var addSuppDocsPendingReviewFiles = function addSuppDocsPendingReviewFiles(app, agentCode, toAddFiles) {
  if (app.supportDocuments.viewedList) {
    if (!app.supportDocuments.viewedList[agentCode]) {
      app.supportDocuments.viewedList[agentCode] = {};
    }
    _.forEach(toAddFiles, function (file) {
      app.supportDocuments.viewedList[agentCode][file] = false;
    });
  }
};

module.exports.updateSuppDocsViewedList = function (data, session, cb) {
  var agentCode = session.agentCode;
  var appId = data.appId;
  var viewedList = data.viewedList;
  logger.log('INFO: updateSuppDocsViewedList - start', appId);

  applicationDao.getApplication(appId, function (application) {
    if (!application.error) {
      if (application.supportDocuments.viewedList[agentCode]) {
        application.supportDocuments.viewedList[agentCode] = viewedList;
        applicationDao.upsertApplication(appId, application, function (resp) {
          if (resp) {
            logger.log('INFO: updateSuppDocsViewedList - end [RETURN=1]', appId);
            cb({ success: true });
          } else {
            logger.error('ERROR: updateSuppDocsViewedList - end [RETURN=-3]', appId);
            cb({ success: false });
          }
        });
      } else {
        logger.error('ERROR: updateSuppDocsViewedList - end [RETURN=-2]', appId);
        cb({ success: false });
      }
    } else {
      logger.error('ERROR: updateSuppDocsViewedList - end [RETURN=-1]', appId);
      cb({ success: false });
    }
  });
};

var getSuppDocsPolicyFormTemplate = function getSuppDocsPolicyFormTemplate(app, session, pProfile, pCid, eApprovalCase, appStatus) {
  logger.log('INFO: getSuppDocsPolicyFormTemplate');
  var sysDocsDocuments = [];
  var mandDocsDocuments = [];
  var optDocsDocuments = [];
  // let payerName = payerProfile.fullName;
  var pName = pProfile.fullName;

  var _commonApp$getPayerIn = commonApp.getPayerInfo(app),
      isThirdPartyPayer = _commonApp$getPayerIn.isThirdPartyPayer,
      payerName = _commonApp$getPayerIn.payerName;

  if (app.appStep && app.appStep > 0) {
    sysDocsDocuments.push({
      "type": "file",
      "id": "appPdf"
    });
  }
  if (session.agent.channel.type != "FA") {
    sysDocsDocuments.push({
      "type": "file",
      "id": "fnaReport"
    });
  }

  sysDocsDocuments.push({
    "type": "file",
    "id": "proposal"
  });

  if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(_.get(app, 'payment.initPayMethod')) > -1 && app.isSubmittedStatus) {
    sysDocsDocuments.push({
      "type": "file",
      "id": eCpdPdfName
    });
  }
  var showSupervisorStatus = ['A', 'R', 'PFAFA'];
  if (showSupervisorStatus.indexOf(eApprovalCase.approvalStatus) > -1 && eApprovalCase.isFACase === true) {
    //Show when the approver is not the director
    if (eApprovalCase.agentId !== eApprovalCase.approveRejectManagerId && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined) {
      sysDocsDocuments.push({
        "type": "file",
        "id": "eapproval_supervisor_pdf"
      });
    }

    //Show when it is approved by FA Admin
    if (eApprovalCase.approver_FAAdminCode !== '' && _.get(eApprovalCase, '_attachments.faFirm_Comment') !== undefined) {
      sysDocsDocuments.push({
        "type": "file",
        "id": "faFirm_Comment"
      });
    }
  } else if ((eApprovalCase.approvalStatus === 'A' || eApprovalCase.approvalStatus === 'R') && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined) {
    sysDocsDocuments.push({
      "type": "file",
      "id": "eapproval_supervisor_pdf"
    });
  }

  // mandDocsDocuments
  // Application is in FA channel
  if (session.agent.channel.type === 'FA') {
    mandDocsDocuments.push({
      "type": "subSection",
      "id": "cAck",
      "title": "Client acknowledgement/ FNA"
    });
  }
  // proposer is singaporean of singapore pr, N1 stands for singapore
  if (pProfile.nationality == 'N1' || pProfile.prStatus == 'Y') {
    mandDocsDocuments.push({
      "type": "subSection",
      "id": "pNric",
      "title": "Proposer's Singapore NRIC "
    });
  } else {
    if (pProfile.pass == 'ep' || pProfile.pass == 'wp' || pProfile.pass == 'dp' || pProfile.pass == 's' || pProfile.pass == 'lp' || pProfile.pass === 'sp') {
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "pPass",
        "title": "Proposer's Copy of valid pass in Singapore ",
        "toolTips": "Copy of valid Employment Pass / S Pass / Work Permit / Dependant / Student / Long Term Visit Pass. (both sides)"
      });
    } else {
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "pPassport",
        "title": "Proposer's Copy of valid passport (first page) ",
        "toolTips": "Passport cover page which contain all personal details "
      });
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "pPassportWStamp",
        "title": "Proposer's Copy of valid passport pages with entry stamps issued by Singapore immigration ",
        "toolTips": "Passport entry stamp is required as proof of entry into Singapore. "
      });
    }
  }
  mandDocsDocuments.push({
    "type": "subSection",
    "id": "pAddrProof",
    "title": "Proposer's Proof of Residential Address "
  });
  // If payer is third party payer
  if (isThirdPartyPayer) {
    mandDocsDocuments.push({
      "type": "subSection",
      "id": "thirdPartyID",
      "title": "Third Party Payor's ID copy ",
      "toolTips": "Example of ID copy for third party payor: NRIC / Valid Pass in Singapore / Passport "
    });
    mandDocsDocuments.push({
      "type": "subSection",
      "id": "thirdPartyAddrProof",
      "title": "Third Party Payor's Proof of Residential Address "
    });
  }
  var pMailingAddrCountry = _.get(app, 'applicationForm.values.proposer.personalInfo.mAddrCountry', '');
  // proposer is resided in either China - Major City or China - other than Major City and proposer possesses type of pass is others or social visit pass
  if (pProfile.nationality !== 'N1' && pProfile.prStatus === 'N' && (pMailingAddrCountry === 'R51' || pMailingAddrCountry === 'R52' || pProfile.residenceCountry === 'R51' || pProfile.residenceCountry === 'R52') && (pProfile.pass === 'o' || pProfile.pass === 'svp' || pProfile.pass === 'lp')) {
    mandDocsDocuments.push({
      'type': 'subSection',
      'id': 'pChinaVisit',
      'title': "Proposer's Mainland China Visitor Declaration Form ",
      'toolTips': 'Mainland China Visitor Declaration Form is compulsory for applicants who are China residents. '
    });
    mandDocsDocuments.push({
      'type': 'subSection',
      'id': 'pBoardingPass',
      'title': "Proposer's Copy of Boarding Pass ",
      'toolTips': 'Copy of Boarding Pass is compulsory for applicants who are China residents. '
    });
  }
  var hasCoe = false;
  var pCertOfEndorsement = _.get(app, 'supportDocuments.values.policyForm.mandDocs.pCertOfEndorsement');
  if (_.isArray(pCertOfEndorsement) && pCertOfEndorsement.length > 0) {
    hasCoe = true;
  }
  if (_.get(session.agent, 'channel.type', '') !== 'FA' && (appStatus === 'APPLYING' && session.agent.agentCode === session.agent.managerCode && app.isCrossBorder || hasCoe)) {
    mandDocsDocuments.push({
      'type': 'subSection',
      'id': 'pCertOfEndorsement',
      'title': 'Certificate of Endorsement (COE)',
      'toolTips': 'Certificate of Endorsement is Mandatory for Cross Border Business '
    });
  }

  // optDocsDocuments
  // proposer is Japanese nationality and All product except Shield
  if (pProfile.nationality === 'N81') {
    optDocsDocuments.push({
      "type": "subSection",
      "id": "pJapaneseNotShield",
      "title": "Declaration Form Status (Japanese)"
    });
  }

  if (appStatus == 'SUBMITTED' || _.get(session, 'agent.agentCode') === _.get(session, 'agent.managerCode')) {
    // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
    if (app.payment && app.payment.initPayMethod) {
      if (app.payment.initPayMethod == 'axasam') {
        mandDocsDocuments.push({
          "type": "subSection",
          "id": "axasam",
          "title": "AXS/SAM Receipt "
        });
      } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
        mandDocsDocuments.push({
          "type": "subSection",
          "id": "chequeCashierOrder",
          "title": "Cheque / Cashier Order Copy "
        });
      } else if (app.payment.initPayMethod == 'teleTransfter') {
        mandDocsDocuments.push({
          "type": "subSection",
          "id": "teleTransfer",
          "title": "Telegraphic Transfer Receipt "
        });
      } else if (app.payment.initPayMethod == 'cash') {
        mandDocsDocuments.push({
          "type": "subSection",
          "id": "cash",
          "title": "Conditional Receipt of Cash "
        });
      }
    }
  } else {
    // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
    if (app.payment && app.payment.initPayMethod) {
      if (app.payment.initPayMethod == 'axasam') {
        optDocsDocuments.push({
          "type": "subSection",
          "id": "axasam",
          "title": "AXS/SAM Receipt "
        });
      } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
        optDocsDocuments.push({
          "type": "subSection",
          "id": "chequeCashierOrder",
          "title": "Cheque / Cashier Order Copy "
        });
      } else if (app.payment.initPayMethod == 'teleTransfter') {
        optDocsDocuments.push({
          "type": "subSection",
          "id": "teleTransfer",
          "title": "Telegraphic Transfer Receipt "
        });
      } else if (app.payment.initPayMethod == 'cash') {
        optDocsDocuments.push({
          "type": "subSection",
          "id": "cash",
          "title": "Conditional Receipt of Cash "
        });
      }
    }
  }

  var template = [];

  template.push({
    "id": "sysDocs",
    "title": "System Document",
    "hasSubLevel": "false",
    "disabled": "true",
    "type": "section",
    "items": sysDocsDocuments
  });

  template.push({
    "id": "mandDocs",
    "hasSubLevel": "false",
    "disabled": "false",
    "title": "Mandatory Document",
    "type": "section",
    "items": mandDocsDocuments
  });

  if (optDocsDocuments.length > 0) {
    template.push({
      "id": "optDoc",
      "hasSubLevel": "false",
      "disabled": "false",
      "title": "Optional Document",
      "type": "section",
      "items": optDocsDocuments
    });
  }

  template.push({
    "id": "otherDoc",
    "hasSubLevel": "true",
    "disabled": "false",
    "title": "Other Document",
    "type": "section",
    "items": []
  });

  return template;
};

var getSuppDocsProposerTemplate = function getSuppDocsProposerTemplate(app, session, pProfile) {
  logger.log('INFO: getSuppDocsProposerTemplate');
  var pName = pProfile.fullName;
  var optDocsDocuments = [];

  optDocsDocuments.push({
    "type": "subSection",
    "id": "pMedicalTest",
    "title": "Copy of medical test / investigation / check up report ",
    "toolTips": "Please submit copy of medical report as declared in Medical and Health information"
  });

  var template = [{
    "id": "optDoc",
    "hasSubLevel": "false",
    "disabled": "false",
    "title": "Optional Document",
    "type": "section",
    "items": optDocsDocuments
  }, {
    "id": "otherDoc",
    "hasSubLevel": "true",
    "disabled": "false",
    "title": "Other Document",
    "type": "section",
    "items": []
  }];
  return template;
};

var getSuppDocsInsuredTemplate = function getSuppDocsInsuredTemplate(app, session, iProfile) {
  logger.log('INFO: getSuppDocsInsuredTemplate');
  var firstInsuredInsurabilityValues = _.get(app, 'applicationForm.values.insured[0].insurability', {});
  var iName = iProfile.fullName;
  var mandDocsDocuments = [];
  var optDocsDocuments = [];
  // mandDocsDocuments
  // proposer is singaporean of singapore pr, N1 stands for singapore
  if (iProfile.nationality == 'N1' || iProfile.prStatus == 'Y') {
    mandDocsDocuments.push({
      "type": "subSection",
      "id": "iNric",
      "title": "Singapore NRIC /Birth Certificate "
    });
  } else {
    if (iProfile.pass == 'ep' || iProfile.pass == 'wp' || iProfile.pass == 'dp' || iProfile.pass == 's' || iProfile.pass == 'lp' || iProfile.pass === 'sp') {
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "iPass",
        "title": "Copy of valid pass in Singapore ",
        "toolTips": "Copy of valid Employment Pass / S Pass / Work Permit / Dependant / Student / Long Term Visit Pass. (both sides)"
      });
    } else {
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "iPassport",
        "title": "Copy of valid passport (first page) ",
        "toolTips": "Passport cover page which contain all personal details "
      });
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "iPassportWStamp",
        "title": "Copy of passport pages with entry stamps issued by Singapore immigration ",
        "toolTips": "Passport entry stamp is required as proof of entry into Singapore. "
      });
    }
    // insured is China nationality and type of pass is others
    if (iProfile.nationality !== 'N1' && iProfile.prStatus === 'N' && (iProfile.residenceCountry === 'R52' || iProfile.residenceCountry === 'R51') && (iProfile.pass === 'o' || iProfile.pass === 'svp' || iProfile.pass === 'lp')) {
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "iChinaVisit",
        "title": "Mainland China Visitor Declaration Form ",
        "toolTips": "Mainland China Visitor Declaration Form is compulsory for applicants who are China residents. "
      });
      mandDocsDocuments.push({
        "type": "subSection",
        "id": "iBoardingPass",
        "title": "Copy of Boarding Pass ",
        "toolTips": "Copy of Boarding Pass is compulsory for applicants who are China residents. "
      });
    }
  }

  // optDocsDocuments
  // insured is Japanese nationality and All product except Shield
  if (iProfile.nationality === 'N81') {
    optDocsDocuments.push({
      "type": "subSection",
      "id": "iJapaneseNotShield",
      "title": "Declaration Form Status (Japanese)"
    });
  }

  // [Medical & Health Info.] If answer for "In the PAST FIVE YEARS, have you had any tests done such as X-ray, ultrasound, CT scan, biopsy, electrocardiogram (ECG), blood or urine test?" is YES
  if (_.get(firstInsuredInsurabilityValues, 'HEALTH11') === 'Y') {
    optDocsDocuments.push({
      "type": "subSection",
      "id": "iMedicalTest",
      "title": "Copy of medical test / investigation / check up report ",
      "toolTips": "Please submit copy of medical report as declared in Medical and Health information."
    });
  }

  if (_.find(
  // find parm 0
  _.at(firstInsuredInsurabilityValues, ['HEALTH14', 'HEALTH15', 'HEALTH16', 'HEALTH17']),
  // find parm 1
  function (value) {
    return value === 'Y';
  })) {
    optDocsDocuments.push({
      "type": "subSection",
      "id": "iJuvenileQuestions",
      "title": "Copy of latest Child Health Booklet including all assessments done to date ",
      "toolTips": " Child Health Booklet is required due to declaration in Juvenile Question in Medical and Health Information. "
    });
  }

  var template = [{
    "id": "mandDocs",
    "hasSubLevel": "false",
    "disabled": "false",
    "title": "Mandatory Document",
    "type": "section",
    "items": mandDocsDocuments
  }];

  if (optDocsDocuments.length > 0) {
    template.push({
      "id": "optDoc",
      "hasSubLevel": "false",
      "disabled": "false",
      "title": "Optional Document",
      "type": "section",
      "items": optDocsDocuments
    });
  }

  template.push({
    "id": "otherDoc",
    "hasSubLevel": "true",
    "disabled": "false",
    "title": "Other Document",
    "type": "section",
    "items": []
  });
  return template;
};

var getSupportDocumentsTemplate = function getSupportDocumentsTemplate(app, session, pProfile, iProfile, appStatus, pCid, iCid, eApprovalCase) {
  logger.log('INFO: getSupportDocumentsTemplate');
  // TODO: Remove appStatus passed from FrontEnd
  var template = {
    policyForm: getSuppDocsPolicyFormTemplate(app, session, pProfile, pCid, eApprovalCase, appStatus)

    // [Medical & Health Info.] If answer for "In the PAST FIVE YEARS, have you had any tests done such as X-ray, ultrasound, CT scan, biopsy, electrocardiogram (ECG), blood or urine test?" is YES
  };if (_.get(app, 'applicationForm.values.proposer.insurability.HEALTH11') === 'Y') {
    template['proposer'] = getSuppDocsProposerTemplate(app, session, pProfile);
  }

  if (pCid != iCid) {
    template['insured'] = getSuppDocsInsuredTemplate(app, session, iProfile);
  }

  return template;
};

var genSuppDocsPolicyFormValues = function genSuppDocsPolicyFormValues() {
  var values = {
    "sysDocs": {
      "fnaReport": {
        "id": "fnaReport",
        "title": "e-FNA",
        "fileType": "application/pdf",
        "fileSize": ""
      },
      "proposal": {
        "id": "proposal",
        "title": "e-PI",
        "fileType": "application/pdf",
        "fileSize": ""
      },
      "appPdf": {
        "id": "appPdf",
        "title": "e-App",
        "fileType": "application/pdf",
        "fileSize": ""
      },
      "eCpdPdf": {
        "id": eCpdPdfName,
        "title": "e-CPD",
        "fileType": "application/pdf",
        "fileSize": ""
      },
      "eapproval_supervisor_pdf": {
        "id": "eapproval_supervisor_pdf",
        "title": "Supervisor Validation",
        "fileType": "application/pdf",
        "fileSize": ""
      },
      "faFirm_Comment": {
        "id": "faFirm_Comment",
        "title": "Comments by FA Firm Admin",
        "fileType": "application/pdf",
        "fileSize": ""
      }
    },
    "mandDocs": {
      "cAck": [],
      "pNric": [],
      "pPass": [],
      "pPassport": [],
      "pPassportWStamp": [],
      "pAddrProof": [],
      "thirdPartyID": [],
      "thirdPartyAddrProof": [],
      "axasam": [],
      "chequeCashierOrder": [],
      "teleTransfer": [],
      "cash": [],
      "pChinaVisit": [],
      "pBoardingPass": [],
      "pCertOfEndorsement": []
    },
    "optDoc": {
      "axasam": [],
      "chequeCashierOrder": [],
      "teleTransfer": [],
      "cash": [],
      "pJapaneseNotShield": []
    }
  };
  return values;
};

var genSuppDocsProposerValues = function genSuppDocsProposerValues() {
  var values = {
    "mandDocs": {},
    "optDoc": {
      "pMedicalTest": []
    }
  };
  return values;
};

var genSuppDocsInsuredValues = function genSuppDocsInsuredValues() {
  var values = {
    "mandDocs": {
      "iNric": [],
      "iPass": [],
      "iPassport": [],
      "iPassportWStamp": [],
      "iChinaVisit": [],
      "iBoardingPass": []
    },
    "optDoc": {
      "iJapaneseNotShield": [],
      "iMedicalTest": [],
      "iJuvenileQuestions": []
    }
  };
  return values;
};
var genSupportDocumentsValues = function genSupportDocumentsValues() {
  var values = {
    policyForm: genSuppDocsPolicyFormValues(),
    proposer: genSuppDocsProposerValues(),
    insured: genSuppDocsInsuredValues()
  };
  return values;
};

var deleteSuppDocsFile = function deleteSuppDocsFile(app, attachmentId, valueLocation, tabId, cb) {
  logger.log('INFO: deleteSuppDocsFile - start', app._id);
  var _attachments = app._attachments,
      supportDocuments = app.supportDocuments;
  var sectionId = valueLocation.sectionId,
      subSectionId = valueLocation.subSectionId;
  // Modify _attachments Json
  // if (_attachments){
  //   delete _attachments[attachmentId];
  //   app._attachments = _attachments;
  // }
  // Modify supportDocuments Json

  var tabValues = supportDocuments.values[tabId];
  var sectionValue = tabValues[sectionId];
  var subSectionValue = void 0;
  if (sectionId == 'otherDoc') {
    subSectionValue = sectionValue.values[subSectionId];
  } else {
    subSectionValue = sectionValue[subSectionId];
  }
  if (subSectionValue) {
    var changedSubSectionValue = subSectionValue.filter(function (item) {
      return item.id != attachmentId;
    });
  }

  if (sectionId == 'otherDoc') {
    sectionValue.values[subSectionId] = changedSubSectionValue;
  } else {
    sectionValue[subSectionId] = changedSubSectionValue;
  }
  tabValues[sectionId] = sectionValue;
  supportDocuments.values[tabId] = tabValues;
  app.supportDocuments = supportDocuments;
  applicationDao.upsertApplication(app._id, app, function (resp) {
    if (resp) {
      commonApp.updateChildSupportDocumentValue(app, function () {
        logger.log('INFO: deleteSuppDocsFile - end [RETURN=1]', app._id);
        cb({
          success: true,
          supportDocuments: supportDocuments
        });
      });
    } else {
      logger.error('ERROR: deleteSuppDocsFile - end  [RETURN=-1]', app._id);
      cb({ success: false });
    }
  });
};

var addSupportDocuments = function addSupportDocuments() {
  var isSubmittedApp = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var app = arguments[1];
  var fileProps = arguments[2];
  var valueLocation = arguments[3];
  var tabId = arguments[4];
  var suppDocsValues = arguments[5];
  var agentCode = arguments[6];
  var rootValues = arguments[7];
  var cb = arguments[8];

  logger.log('INFO: addSupportDocuments - start', app._id);
  var sectionId = valueLocation.sectionId,
      subSectionId = valueLocation.subSectionId;

  var values = isSubmittedApp ? rootValues : app.supportDocuments.values;
  var tabValues = isSubmittedApp ? suppDocsValues : values[tabId];
  var sectionValue = tabValues[sectionId];
  var submittedApp = isSubmittedApp ? _.cloneDeep(app) : null;

  if (sectionId == 'otherDoc') {
    if (_.isEmpty(sectionValue.values) || _.isEmpty(sectionValue.values[subSectionId])) {
      _.set(sectionValue, 'values[' + subSectionId + ']', []);
    }
    sectionValue.values[subSectionId].push({
      "id": fileProps.id,
      "title": fileProps.title,
      "fileSize": fileProps.fileSize,
      "uploadDate": fileProps.backendUploadDate,
      "feUploadDate": fileProps.uploadDate,
      "fileType": fileProps.fileType,
      "length": fileProps.length
    });
    // let subSectionValue = sectionValue.values[subSectionId];
    //   subSectionValue.push({
    //     "id": fileProps.id,
    //     "title": fileProps.title,
    //     "fileSize": fileProps.fileSize,
    //     "uploadDate": fileProps.backendUploadDate,
    //     "fileType": fileProps.fileType,
    //     "length": fileProps.length
    //   });
  } else {
    if (_.isEmpty(sectionValue[subSectionId])) {
      _.set(sectionValue, '[' + subSectionId + ']', []);
    }
    sectionValue[subSectionId].push({
      "id": fileProps.id,
      "title": fileProps.title,
      "fileSize": fileProps.fileSize,
      "uploadDate": fileProps.backendUploadDate,
      "feUploadDate": fileProps.uploadDate,
      "fileType": fileProps.fileType,
      "length": fileProps.length
    });
    // let subSectionValue = sectionValue[subSectionId];
    //   subSectionValue.push({
    //     "id": fileProps.id,
    //     "title": fileProps.title,
    //     "fileSize": fileProps.fileSize,
    //     "uploadDate": fileProps.backendUploadDate,
    //     "fileType": fileProps.fileType,
    //     "length": fileProps.length
    //   });
    // sectionValue[subSectionId] = subSectionValue;
  }
  // Don't remove/comment out this line
  values[tabId] = tabValues;

  if (isSubmittedApp) {
    // upsert a pending to update list
    if (!submittedApp.supportDocuments.pendingSubmitList) {
      submittedApp.supportDocuments['pendingSubmitList'] = [];
    }
    submittedApp.supportDocuments.pendingSubmitList.push(fileProps.id);
    applicationDao.upsertApplication(submittedApp._id, submittedApp, function (resp) {
      if (resp) {
        logger.log('INFO: addSupportDocuments - end [RETURN=1]', app._id);
        cb({
          success: true,
          values: values,
          pendingSubmitList: _.get(submittedApp, 'supportDocuments.pendingSubmitList')
        });
      } else {
        logger.error('ERROR: addSupportDocuments - end [RETURN=-1]', app._id);
        cb({ success: false });
      }
    });
  } else {
    applicationDao.upsertApplication(app._id, app, function (resp) {
      if (resp) {
        logger.log('INFO: addSupportDocuments - end [RETURN=2]', app._id);
        cb({
          success: true,
          values: values,
          pendingSubmitList: _.get(submittedApp, 'supportDocuments.pendingSubmitList', [])
        });
      } else {
        logger.error('ERROR: addSupportDocuments - end [RETURN=-2]', app._id);
        cb({ success: false });
      }
    });
  }
};
/**FUNCTION ENDS: addSupportDocuments */

// Disabled
// var getSuppDocsAppView = function(app) {
//   let suppDocsAppView = {
//     id: app.id,
//     iCid: app.iCid,
//     pCid: app.pCid,
//     iFullName: app.quotation.iFullName,
//     pFullName: app.quotation.pFullName
//   };
//   return suppDocsAppView;
// }

var checkMandDocsStatus = function checkMandDocsStatus(template, values) {
  logger.log('INFO: checkMandDocsStatus');
  var result = {};
  var allUploaded = true;

  _.forEach(template, function (tabTemplate, tabId) {
    var tabContent = tabTemplate.tabContent || tabTemplate;
    var mandDocsTemplate = _.get(_.find(tabContent, function (section) {
      return section.id === 'mandDocs';
    }), 'items');
    var mandDocsValues = _.get(values[tabId], 'mandDocs', {});

    _.forEach(mandDocsTemplate, function (fileTemplate) {
      if (_.isEmpty(_.get(mandDocsValues, _.get(fileTemplate, 'id')))) {
        result[tabId] = false;
        allUploaded = false;
        return false;
      }
    });
  });

  result['allUploaded'] = allUploaded;
  return result;
};

module.exports.closeSuppDocs = function (data, session, cb) {
  logger.log('INFO: closeSuppDocs - start', data.appId);
  var mandDocsAllUploaded = true;

  var prepareSubmissionTemplateValues = function prepareSubmissionTemplateValues(currApp) {
    getSubmissionTemplate(currApp, function (tmpl) {
      getSubmissionValues(currApp, session, '', '', function (resValues) {
        logger.log('INFO: closeSuppDocs - end [RETURN=1]', data.appId);
        cb({
          success: true,
          template: tmpl,
          values: resValues.values,
          isMandDocsAllUploaded: mandDocsAllUploaded,
          isSubmitted: false
        });
      });
    });
  };

  applicationDao.getApplication(data.appId, function (app) {
    // if (_.get(app, 'type') === 'masterApplication') {
    //   data.appDoc = app;
    //   shieldHandler.closeSuppDocs(data, session, cb);
    // } else {
    mandDocsAllUploaded = _.get(checkMandDocsStatus(data.template, _.get(app, 'supportDocuments.values', {})), 'allUploaded');
    commonApp.deletePendingSubmitList(app).then(function (resp) {
      if (resp.success) {
        var application = resp.application;
        application.isMandDocsAllUploaded = mandDocsAllUploaded;
        applicationDao.upsertApplication(application.id, application, function (resp) {
          if (resp) {
            prepareSubmissionTemplateValues(application);
          } else {
            logger.error('ERROR: closeSuppDocs - end [RETURN=-3]', data.appId);
            cb({ success: false });
          }
        });
      } else {
        logger.error('ERROR: closeSuppDocs - end [RETURN=-2]', data.appId);
        cb({ success: false });
      }
    }).catch(function (error) {
      logger.error('ERROR: closeSuppDocs - deletePendingSubmitList [RETURN=-1]', data.appId, error);
    });
    // }
  });
};
/**FUNCTION ENDS: closeSuppDocs*/

module.exports.getSupportDocuments = function (data, session, cb) {
  logger.log('INFO: getSupportDocuments - start', data.applicationId);
  var agentCode = session.agentCode;

  var initViewedList = function initViewedList(app, template) {
    // let diffWithSysDocs = function(app, template){
    var idList = [];
    // let newViewedList = [];

    if (agentCode === app.agentCode) {
      idList = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        idList.push('fnaReport');
      }
    } else {
      idList = _.keys(_.get(app, 'supportDocuments.viewedList.' + app.agentCode, {}));

      var sysDocsIds = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        sysDocsIds.push('fnaReport');
      }
      idList = _.union(idList, sysDocsIds);
    }

    if (session.agent.channel.type === "FA") {
      // if in FA Channel, need to add cAck's files into ViewedList, as the documents of FNA in FA Channel
      _.forEach(_.get(app, 'supportDocuments.values.policyForm.mandDocs.cAck', []), function (cAckFile) {
        idList.push(cAckFile.id);
      });
    }
    _.forEach(idList, function (id) {
      if (!_.includes(_.keys(_.get(app, 'supportDocuments.viewedList.' + agentCode)), id)) {
        _.set(app, 'supportDocuments.viewedList.' + agentCode + '.' + id, false);
      }
    });
  };

  var updateViewedList = function updateViewedList(app, template, eApprovalCase) {
    if (!app.supportDocuments.viewedList[agentCode]) {
      app.supportDocuments.viewedList[agentCode] = {};
      initViewedList(app, template);
      // diffWithSysDocs(app, template);
    } else {
      if (agentCode !== app.agentCode) {
        var loginAgentViewedObj = app.supportDocuments.viewedList[agentCode];
        if (_.get(eApprovalCase, 'approvalStatus') && !_.includes(['A', 'R', 'E'], _.get(eApprovalCase, 'approvalStatus'))) {
          if (_.isEmpty(loginAgentViewedObj)) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - non Shield - empty object :', app.id);

            loginAgentViewedObj = {
              'appPdf': false,
              'proposal': false
            };
            if (session.agent.channel.type !== 'FA') {
              loginAgentViewedObj['fnaReport'] = false;
            }
            app.supportDocuments.viewedList[agentCode] = loginAgentViewedObj;
          } else if (!loginAgentViewedObj.hasOwnProperty('appPdf')) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - non Shield - missed eApp : ', app.id);

            loginAgentViewedObj['appPdf'] = false;
            app.supportDocuments.viewedList[agentCode] = loginAgentViewedObj;
          }
        }
      }
    }
  };

  var getDefaultDocNameList = function getDefaultDocNameList() {
    var defaultDocNameListId = 'suppDocsDefaultDocNames';
    return new Promise(function (resolve) {
      dao.getDoc(defaultDocNameListId, function (list) {
        resolve(list);
      });
    });
  };

  var getOtherDocNameList = function getOtherDocNameList(docNamesList, iIsJapaneseAndNotShield, pIsJapaneseAndNotShield) {
    var otherDocNameListId = 'otherDocNames';
    var ikey = iIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    var pkey = pIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    return new Promise(function (resolve) {
      dao.getDoc(otherDocNameListId, function (obj) {
        var otherDocNames = obj ? {
          'la': obj[ikey].la,
          'ph': obj[pkey].ph
        } : { 'la': [], 'ph': [] };
        docNamesList.otherDocNames = otherDocNames;
        resolve(docNamesList);
      });
    });
  };

  var genTemplateValues = function genTemplateValues(app, session, pProfile, iProfile, appStatus, pCid, iCid, isReadOnly, cb, eApprovalCase) {
    logger.log('INFO: getSupportDocuments - genTemplateValues', data.applicationId);
    var template = getSupportDocumentsTemplate(app, session, pProfile, iProfile, appStatus, pCid, iCid, eApprovalCase);
    var suppDocsAppView = {
      id: app.id,
      iCid: app.iCid,
      pCid: app.pCid,
      iFullName: app.quotation.iFullName,
      pFullName: app.quotation.pFullName,
      dFullName: app.quotation.agent.name
    };
    var tokensMap = {};

    var iIsJapaneseAndNotShield = false;
    var pIsJapaneseAndNotShield = false;
    if (app.type && app.type === 'application') {
      if (iProfile.nationality === 'N81') {
        iIsJapaneseAndNotShield = true;
      }
      if (pProfile.nationality === 'N81') {
        pIsJapaneseAndNotShield = true;
      }
    }
    // Default DOC NAME LIST for checking duplication of document name
    getDefaultDocNameList().then(function (docNamesList) {
      //get other doc selction list for non-Shield,update docNamesList
      return getOtherDocNameList(docNamesList, iIsJapaneseAndNotShield, pIsJapaneseAndNotShield);
    }).then(function (docNamesList) {
      // Init Support Documents in DB
      if (!app.supportDocuments) {
        var values = genSupportDocumentsValues();
        app['supportDocuments'] = {};
        app.supportDocuments["values"] = values;
        app.supportDocuments["viewedList"] = {};
        app.supportDocuments["isAllViewed"] = false;
        updateViewedList(app, template, eApprovalCase);

        var supportDocDetails = {
          otherDocNameList: docNamesList.otherDocNames
        };
        applicationDao.upsertApplication(app._id, app, function (resp) {
          if (resp) {
            logger.log('INFO: getSupportDocuments - end [RETURN=1]', data.applicationId);
            cb({
              success: true,
              template: template,
              values: values,
              suppDocsAppView: suppDocsAppView,
              viewedList: app.supportDocuments.viewedList[agentCode],
              isReadOnly: isReadOnly,
              defaultDocNameList: docNamesList.defaultDocumentNames,
              supportDocDetails: supportDocDetails,
              tokensMap: tokensMap,
              application: app
            });
          } else {
            logger.error('ERROR: getSupportDocuments - end [RETURN=-2]', data.applicationId);
            cb({ success: false });
          }
        });
      } else {
        if (!app.supportDocuments.viewedList) {
          app.supportDocuments["viewedList"] = {};
        }
        updateViewedList(app, template, eApprovalCase);
        var _supportDocDetails = {
          otherDocNameList: docNamesList.otherDocNames
        };
        applicationDao.upsertApplication(app._id, app, function (resp) {
          if (resp) {

            //generate token for image in mandDocs & optDoc
            var now = new Date().getTime();
            var tokens = [];
            _.forEach(SuppDocsUtils.getNonSysDocs(app, ['application/pdf']), function (doc) {
              var token = createPdfToken(app._id, doc.id, now, session.loginToken);
              tokens.push(token);
              tokensMap[doc.id] = token.token;
            });
            // no need to set pdf token if its IOS
            if (session.platform === "ios") {
              logger.log('INFO: getSupportDocuments - end [RETURN=2]', data.applicationId);
              cb({
                success: true,
                template: template,
                values: app.supportDocuments.values,
                suppDocsAppView: suppDocsAppView,
                viewedList: app.supportDocuments.viewedList[agentCode],
                isReadOnly: isReadOnly,
                defaultDocNameList: docNamesList.defaultDocumentNames,
                supportDocDetails: _supportDocDetails,
                tokensMap: tokensMap,
                application: app
              });
            } else {
              setPdfTokensToRedis(tokens, function () {
                logger.log('INFO: getSupportDocuments - end [RETURN=3]', data.applicationId);
                cb({
                  success: true,
                  template: template,
                  values: app.supportDocuments.values,
                  suppDocsAppView: suppDocsAppView,
                  viewedList: app.supportDocuments.viewedList[agentCode],
                  isReadOnly: isReadOnly,
                  defaultDocNameList: docNamesList.defaultDocumentNames,
                  supportDocDetails: _supportDocDetails,
                  tokensMap: tokensMap,
                  application: app
                });
              });
            }
          } else {
            logger.error('ERROR: getSupportDocuments - end [RETURN=-4]', data.applicationId);
            cb({ success: false });
          }
        });
      }
    });
  };

  applicationDao.getApplication(data.applicationId, function (appDoc) {
    if (!appDoc.error) {
      commonApp.deletePendingSubmitList(appDoc).then(function (resp) {
        var app = resp.application || appDoc;
        var pCid = app.quotation.pCid;
        var iCid = app.quotation.iCid;
        // let payerName = getPayerName(app);
        // if (payerCid == '') payerCid = pCid;
        ApprovalHandler.searchApprovalCaseById({ id: app.policyNumber }, session, function (resp) {
          commonApp.isSuppDocsReadOnly(app, session.agent, session.agentCode).then(function (isReadOnly) {
            logger.log('isReadOnly :' + isReadOnly);
            clientDao.getProfileById(pCid, function (pProfile) {
              bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then(function (bApp) {
                if (pCid == iCid) {
                  genTemplateValues(app, session, pProfile, pProfile, bApp.appStatus, pCid, iCid, isReadOnly, cb, resp.foundCase);
                } else {
                  clientDao.getProfileById(iCid, function (iProfile) {
                    genTemplateValues(app, session, pProfile, iProfile, bApp.appStatus, pCid, iCid, isReadOnly, cb, resp.foundCase);
                  });
                }
              });
            });
          });
        });
      });
    } else {
      logger.error('ERROR: getSupportDocuments - end [RETURN=-1]', data.applicationId);
      cb({ success: false });
    }
  });
};
/**FUNCTION ENDS: getSupportDocuments*/

module.exports.checkSuppDocsProperties = function (data, session, cb) {
  logger.log('INFO: checkSuppDocsProperties - start', data.appId);
  var currentAgentCode = session.agentCode;
  var showHealthDeclarationPrompt = false;
  var isShieldApp = false;
  var approvalDocId = void 0;
  var agentViewedList = void 0;

  applicationDao.getApplication(data.appId, function (app) {
    if (!app.error) {
      isShieldApp = app.type === 'masterApplication';

      // health declaration check
      if (isShieldApp && commonApp.isShowHealthDeclaration(app)) {
        if (_.isEmpty(_.get(app, 'supportDocuments.values.policyForm.mandDocs.healthDeclaration', []))) {
          showHealthDeclarationPrompt = true;
        }
      }

      if (_.get(app, 'supportDocuments.viewedList')) {
        var reviewDone = true;

        if (isShieldApp) {
          approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);
        } else {
          approvalDocId = app.policyNumber;
        }

        dao.getDocFromCacheFirst(approvalDocId, function (approvalDoc) {
          if (_.includes(['A', 'R'], _.get(approvalDoc, 'approvalStatus', ''))) {
            agentViewedList = app.supportDocuments.viewedList[_.get(approvalDoc, 'approveRejectManagerId')];
          } else {
            agentViewedList = app.supportDocuments.viewedList[currentAgentCode];
          }

          if (_.isEmpty(agentViewedList) || !agentViewedList.hasOwnProperty('appPdf')) {
            reviewDone = false;
          } else {
            for (var i in agentViewedList) {
              if (!agentViewedList[i]) {
                reviewDone = false;
                break;
              }
            }
          }

          /**
           * Clear Supp Docs Cached and useless data, Notify AXA - START
           */
          clearCacheSuppDocData(app, isShieldApp, session, _.get(approvalDoc, 'approvalStatus'), function (clearCacheResult) {
            if (clearCacheResult.notifyAXA && !app.notifyAbnormalCase) {
              app.notifyAbnormalCase = true;
              applicationDao.upsertApplication(app._id, app, function (resp) {
                if (resp && !resp.error) {
                  logger.log('INFO:: Successfully updated the application and ready to send email to AXA:: ' + app._id);
                  aEmaillHandler.notifyAXAwithAbnormalApprovalCase(app, clearCacheResult.failedReason);
                } else {
                  logger.error('ERROR:: Failed to Update Application:: ' + app._id);
                }
              });
            };
            logger.log('INFO: checkSuppDocsProperties - end [RETURN=1]', data.appId);
            cb({
              success: true,
              reviewDone: clearCacheResult.allowToApprove || !clearCacheResult.missingEapp && reviewDone,
              showMissingEappPrompt: clearCacheResult.missingEapp ? true : false,
              showHealthDeclarationPrompt: showHealthDeclarationPrompt
            });
          });
          /**
           * Clear Supp Docs Cached and useless data, Notify AXA - END
           */
        });
      } else {
        logger.log('INFO: checkSuppDocsProperties - end [RETURN=2]', data.appId);
        cb({
          success: true,
          reviewDone: false,
          showHealthDeclarationPrompt: showHealthDeclarationPrompt
        });
      }
    } else {
      logger.error('ERROR: checkSuppDocsProperties - end [RETURN=-1]', data.appId);
      cb({ success: false });
    }
  });
};
/**FUNCTION ENDS: checkSuppDocsProperties*/

var clearCacheSuppDocData = function clearCacheSuppDocData(app, isShieldApp, session, approvalStatus, cb) {
  var result = void 0;
  var template = void 0;
  var eApprovalCase = {};
  var pProfile = {};
  var iProfile = {};
  async.waterfall([function (callback) {
    /**  // Get all related docs for generating template
        let promises = [];
        let docIds = [];
        if (isShieldApp) {
          docIds.push(ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id));
        } else {
          docIds.push(_.get(app, 'pCid'));
          docIds.push(_.get(app, 'iCid'));
          docIds.push(_.get(app, 'policyNumber'));
        }
        _.each(docIds, id => {
          promises.push(new Promise(resolve => {
            dao.getDoc(id, (doc) => {
              resolve(doc);
            });
          }))
        });
        Promise.all(promises).then((docs) => {
          callback(null, docs);
        }).catch((error) => {
            logger.error(`ERROR: Get Docs in clearCacheSuppDocData: ${error}`);
            callback(error);
        });
    },
    (allDocs, callback) => {
      // Prepare Supporting Docs UI Template for checking mandatory docs and system docs
      if (isShieldApp) {
        let eApprovalCase = allDocs[0];
        template = shieldSupportDocument.getSupportDocumentsTemplate(app, 'SUBMITTED', session.agent, eApprovalCase);
      } else {
        let pProfile = allDocs[0];
        let iProfile = allDocs[1];
        let eApprovalCase = allDocs[2];
        template = getSupportDocumentsTemplate(app, session, pProfile, iProfile, 'SUBMITTED', pProfile.cid, iProfile.cid, eApprovalCase);
      }
      callback(null, template);
    },
    (template, callback) => {
      let mandDocIds = {};
      _.each(template, (tabTemplate, tabKey) => {
        mandDocIds[tabKey] = {};
        _.each(tabTemplate, (sectionTemplate, sectionKey) => {
          if (sectionTemplate && (sectionTemplate.id === 'mandDocs' || sectionTemplate.id === 'sysDocs')) {
            mandDocIds[tabKey][sectionTemplate.id] = [];
            _.each(sectionTemplate.items, item => {
              //if ((sectionKey === 'sysDocs' && item.id.indexOf('appPdf') > -1)|| sectionKey === 'mandDocs') {
              if (sectionKey === 'mandDocs') {
                mandDocIds[tabKey][sectionTemplate.id].push(item.id);
              }
            })
          }
        });
      });
      callback(null, mandDocIds);
    },
    (mandDocIds, callback) => {
      let result = {};
      // Check are all mandatory files displayed and able to review
      let successfullyDisplayAllMandDocs = true;
      _.each(mandDocIds, (tab, tabKey) => {
        _.each(tab, (section, sectionKey) => {
          _.each(section, id => {
            let mandObj = _.get(app, `supportDocuments.values.${tabKey}.${sectionKey}.${id}`);
            if(mandObj !== undefined && _.isEmpty(mandObj)) {
              successfullyDisplayAllMandDocs = false;
            }
          });
        });
      });
       // Missing e-App
      let missingeApp = false;
       const noteAppAttachmentIds = ['fnaReport', 'proposal'];
      // Check any cached useless data in supporting docs
      let attachmentsKeys = _.keys(app._attachments);
      let viewedList =_.get(app, `supportDocuments.viewedList.${session.agent.agentCode}`);
      let viewedListKeys = _.filter(_.keys(viewedList), id => noteAppAttachmentIds.indexOf(id) === -1 );
      // Empty - All viewedListKeys has been uploaded succesffully and has attachmentId
      // let allMandUploaded = _.isEmpty(_.difference(viewedListKeys, attachmentsKeys));
       // Check UI and ViewedListMappng
      _.each(_.get(app, 'supportDocuments.values'), tabObj => {
        _.each(tabObj, sectionObj => {
          _.each(sectionObj, docsObj => {
            viewedListKeys = _.remove(viewedListKeys, (id) => {
              // Prepare the list the do datapatch the viewedList, do datapatch when there is any remaining items
              return docsObj.id === id && (viewedListKeys.indexOf(docsObj.id) > -1 || viewedList[docsObj.id] === true);
            });
          });
        });
      });
       let uiViewedListMapped = _.isEmpty(viewedListKeys);
       if (missingeApp) {
        result = {
          success: true,
          notifyAXA: true,
          missingEapp: true,
          allowToApprove: false
        }
      } else if (successfullyDisplayAllMandDocs) {
        result = {
          success: true,
          notifyAXA: true,
          allowToApprove: uiViewedListMapped ? false : true
        }
      } else {
        result ={
          success: true,
          notifyAXA: false
        };
      }*/
    var eAppShowing = true;
    if (isShieldApp) {
      // Check Proposer Show the eapp
      eAppShowing = _.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated');
      // Check Insured show the eApp
      _.each(_.get(app, 'iCids', []), function (iCid) {
        var foundInsuredObj = _.find(_.get(app, 'applicationForm.values.insured'), function (insured) {
          return insured.personalInfo.cid === iCid;
        });
        if (!_.get(foundInsuredObj, 'extra.isPdfGenerated')) {
          eAppShowing = false;
        }
      });
    } else {
      eAppShowing = app.appStep && app.appStep > 0;
    }

    if (!approvalStatus || ['A', 'R', 'E'].indexOf(approvalStatus) > -1) {
      // not yet submitted case and the approval status is approve /reject /expire
      result = {
        success: true,
        notifyAXA: false
      };
    } else if (app && app.isMandDocsAllUploaded && eAppShowing) {
      var viewedList = _.get(app, 'supportDocuments.viewedList.' + session.agent.agentCode);
      var supportDocumnetsValues = _.get(app, 'supportDocuments.values');

      // remove all viewedList when it appears in UI, remain the not matched case
      var viewedAllDisplayedSuppDocs = true;
      _.each(supportDocumnetsValues, function (tab) {
        _.each(tab, function (section) {
          _.each(section, function (docsArray) {
            var docs = void 0;
            if (section.values) {
              // Other docs go 1 more level deeper
              _.each(section.values, function (otherDocsArray) {
                _.each(otherDocsArray, function (obj) {
                  if (_.findKey(viewedList, function (value, key) {
                    return key === obj.id;
                  }) && _.get(viewedList, obj.id) === false) {
                    viewedAllDisplayedSuppDocs = false;
                  }
                  viewedList = _.omit(viewedList, [obj.id]);
                });
              });
            } else {
              _.each(docsArray, function (obj) {
                // Check any supporting docs (matched the UI values) not yet viewed
                if (_.findKey(viewedList, function (value, key) {
                  return key === docsArray.id;
                }) && _.get(viewedList, docsArray.id) === false || _.findKey(viewedList, function (value, key) {
                  return key === obj.id;
                }) && _.get(viewedList, obj.id) === false) {
                  viewedAllDisplayedSuppDocs = false;
                }
                // Remove the supporting docs that Matched the UI
                viewedList = _.omit(viewedList, [obj.id]);
              });
            }
            // Remove System Docs Id
            viewedList = _.omit(viewedList, [docsArray.id]);
          });
        });
      });

      var notMatchedUIandViewedList = !_.isEmpty(viewedList);

      if (notMatchedUIandViewedList && viewedAllDisplayedSuppDocs) {
        logger.error('clearCacheSuppDocData:: UI and viewedList are not matched:: ' + (app && app.id));
        result = {
          success: true,
          notifyAXA: true,
          allowToApprove: true,
          failedReason: 'Agent has approved the case, please extract the case to check the root cause'
        };
      } else {
        result = {
          success: true,
          notifyAXA: false
        };
      }
    } else if (!eAppShowing) {
      logger.error('clearCacheSuppDocData:: e-App is not showing in the UI:: ' + (app && app.id));
      result = {
        success: true,
        notifyAXA: true,
        missingEapp: true,
        allowToApprove: false,
        failedReason: 'Manager cannot approve the case, please extract the case to check the root cause'
      };
    } else {
      result = {
        success: true,
        notifyAXA: false
      };
    }

    callback(null, result);
  }], function (err, result) {
    if (err) {
      logger.error("Error in clearCacheSuppDocData: ", err);
      cb({
        success: false,
        notifyAXA: false
      });
    } else {
      cb(result);
    }
  });
};

var createNewViewedList = function createNewViewedList(app, agentCode) {
  _.forEach(app.supportDocuments.viewedList, function (item) {
    if (!_.isEmpty(item)) {
      app.supportDocuments.viewedList[agentCode] = item;
      _.forEach(Object.keys(app.supportDocuments.viewedList[agentCode]), function (file) {
        app.supportDocuments.viewedList[agentCode][file] = false;
      });
    }
  });
};

module.exports.viewedSuppDocsFile = function (data, session, cb) {
  logger.log('INFO: viewedSuppDocsFile - start', data.applicationId);
  var agentCode = session.agentCode;

  var now = Date.now();
  var signDocConfig = {
    // pdfUrl: '',
    attUrl: global.config.signdoc.getPdf,
    postUrl: global.config.signdoc.postUrl,
    resultUrl: global.config.signdoc.resultUrl,
    dmsId: global.config.signdoc.dmsid,
    docid: data.attachmentId,
    auth: genSignDocAuth(data.attachmentId, now),
    docts: now
  };

  var checkAllFilesViewed = function checkAllFilesViewed(supportDocuments) {
    var allViewed = true;
    var files = supportDocuments.viewedList[agentCode];
    for (var i in files) {
      if (!files[i]) {
        allViewed = false;
      }
    }
    if (allViewed) {
      supportDocuments.isAllViewed = true;
    }
  };

  var updateViewedList = function updateViewedList(appId, attachmentId) {
    logger.log('INFO: viewedSuppDocsFile - updateViewedList', data.applicationId);
    if (data.isSupervisorChannel && data.updateReviewList) {
      applicationDao.getApplication(appId, function (app) {
        if (!app.supportDocuments.viewedList[agentCode]) {
          createNewViewedList(app, agentCode);
        }
        if (!_.isUndefined(app.supportDocuments.viewedList[agentCode][attachmentId])) {
          app.supportDocuments.viewedList[agentCode][attachmentId] = true;
        }
        // _.set(app, 'supportDocuments.viewedList[' + agentCode + '][' + attachmentId + ']', true);

        if (!app.supportDocuments.isAllViewed) {
          checkAllFilesViewed(app.supportDocuments);
        }
        applicationDao.upsertApplication(appId, app, function (resp) {
          if (!resp) {
            logger.error('ERROR: viewedSuppDocsFile - updateViewedList [RETURN=-1]', data.applicationId);
          }
        });
      });
    }
  };

  var uploadFNAReport = function uploadFNAReport(app, bundle) {
    return new Promise(function (resolve) {
      needsHandler.generateFNAReport({ cid: app.pCid }, session, function (pdfResult) {
        if (!pdfResult.fnaReport) {
          logger.error('ERROR: viewedSuppDocsFile - end cannot gen FNA Report [RETURN=-30]', app.id);
          resolve(false);
        } else {
          dao.uploadAttachmentByBase64(bundle.id, data.attachmentId, bundle._rev, pdfResult.fnaReport, 'application/pdf', function (res) {
            resolve(true);
          });
        }
      });
    });
  };

  if (data.applicationId == null) {
    logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-1]');
    cb({ success: false });
  } else {
    if (data.attachmentId === appPdfName) {
      applicationDao.getApplication(data.applicationId, function (app) {
        if (!app.error) {
          updateViewedList(data.applicationId, data.attachmentId);

          var tokens = [createPdfToken(data.applicationId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(tokens, function () {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=8]', data.applicationId);
            cb({
              success: true,
              docId: data.applicationId,
              token: tokens[0].token,
              isSigned: app.isApplicationSigned,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-8]', data.applicationId);
          cb({ success: false });
        }
      });
    } else if (data.attachmentId === proposalPdfName) {
      applicationDao.getApplication(data.applicationId, function (app) {
        if (!app.error) {
          updateViewedList(data.applicationId, data.attachmentId);

          var tokens = [createPdfToken(app.quotationDocId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(tokens, function () {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=2]', data.applicationId);
            cb({
              success: true,
              docId: app.quotationDocId,
              token: tokens[0].token,
              isSigned: app.isProposalSigned,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-2]', data.applicationId);
          cb({ success: false });
        }
      });
    } else if (data.attachmentId === fnaPdfName) {
      applicationDao.getApplication(data.applicationId, function (app) {
        if (!app.error) {
          var bundleId = _.get(app, 'bundleId');
          bDao.getBundle(app.pCid, bundleId, function (bundle) {
            var isFnaReportSigned = _.get(bundle, 'isFnaReportSigned', false);
            updateViewedList(data.applicationId, data.attachmentId);

            var tokens = [createPdfToken(bundleId, data.attachmentId, now, session.loginToken)];
            setPdfTokensToRedis(tokens, function () {
              logger.log('INFO: viewedSuppDocsFile - end [RETURN=3]', data.applicationId);
              if (!isFnaReportSigned && bundle.status < bDao.BUNDLE_STATUS.START_GEN_PDF) {
                uploadFNAReport(app, bundle).then(function (success) {
                  if (success) {
                    cb({
                      success: true,
                      docId: bundleId,
                      token: tokens[0].token,
                      isSigned: isFnaReportSigned,
                      signDocConfig: signDocConfig
                    });
                  } else {
                    logger.error('ERROR: uploadFNAReport unsuccessfully - end [RETURN=-3]', data.applicationId);
                    cb({ success: false });
                  }
                });
              } else {
                cb({
                  success: true,
                  docId: bundleId,
                  token: tokens[0].token,
                  isSigned: isFnaReportSigned,
                  signDocConfig: signDocConfig
                });
              }
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-3]', data.applicationId);
          cb({ success: false });
        }
      });
    } else if (data.attachmentId === eCpdPdfName) {
      updateViewedList(data.applicationId, data.attachmentId);
      var tokens = [createPdfToken(data.applicationId, data.attachmentId, now, session.loginToken)];
      setPdfTokensToRedis(tokens, function () {
        logger.log('INFO: viewedSuppDocsFile - end [RETURN=4]', data.applicationId);
        cb({
          success: true,
          docId: data.applicationId,
          token: tokens[0].token,
          isSigned: false,
          signDocConfig: signDocConfig
        });
      });
    } else if (data.attachmentId == 'eapproval_supervisor_pdf') {
      applicationDao.getApplication(data.applicationId, function (app) {
        if (!app.error) {
          var approvalDocId = void 0;
          if (_.get(app, 'quotation.quotType') === 'SHIELD') {
            approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(data.applicationId);
          } else {
            approvalDocId = app.policyNumber;
          }
          var _tokens = [createPdfToken(approvalDocId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(_tokens, function () {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=5]', data.applicationId);
            cb({
              success: true,
              docId: app.policyNumber,
              token: _tokens[0].token,
              isSigned: false,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-5]', data.applicationId);
          cb({ success: false });
        }
      });
    } else if (data.attachmentId == 'faFirm_Comment') {
      applicationDao.getApplication(data.applicationId, function (app) {
        if (!app.error) {
          var approvalDocId = void 0;
          if (_.get(app, 'quotation.quotType') === 'SHIELD') {
            approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(data.applicationId);
          } else {
            approvalDocId = app.policyNumber;
          }
          var _tokens2 = [createPdfToken(approvalDocId, data.attachmentId, now, session.loginToken)];
          setPdfTokensToRedis(_tokens2, function () {
            logger.log('INFO: viewedSuppDocsFile - end [RETURN=6]', data.applicationId);
            cb({
              success: true,
              docId: app.policyNumber,
              token: _tokens2[0].token,
              isSigned: false,
              signDocConfig: signDocConfig
            });
          });
        } else {
          logger.error('ERROR: viewedSuppDocsFile - end [RETURN=-6]', data.applicationId);
          cb({ success: false });
        }
      });
    } else {
      updateViewedList(data.applicationId, data.attachmentId);
      var _tokens3 = [createPdfToken(data.applicationId, data.attachmentId, now, session.loginToken)];
      setPdfTokensToRedis(_tokens3, function () {
        logger.log('INFO: viewedSuppDocsFile - end [RETURN=7]', data.applicationId);
        cb({
          success: true,
          docId: data.applicationId,
          token: _tokens3[0].token,
          isSigned: false,
          signDocConfig: signDocConfig
        });
      });
    }
  }
};

// This function is used in 'Other Document' type
module.exports.deleteSupportDocument = function (data, session, cb) {
  logger.log('INFO: deleteSupportDocument - start', data.applicationId);
  applicationDao.getApplication(data.applicationId, function (application) {
    if (!application.error) {
      var supportDocuments = application.supportDocuments;
      var tabValues = supportDocuments.values[data.tabId];
      // let otherDoc = supportDocuments.values.otherDoc;
      var otherDoc = tabValues.otherDoc;
      var values = otherDoc.values;
      var template = otherDoc.template;
      var attachmentIdsArr = [];
      var successList = [];
      var failList = [];
      if (values[data.documentId]) {
        values[data.documentId].forEach(function (item) {
          attachmentIdsArr.push(item.id);
        });
      }
      if (attachmentIdsArr.length == 0) {
        template.splice(template.findIndex(function (item) {
          return item.id == data.documentId;
        }), 1);
        delete values[data.documentId];
        tabValues.otherDoc['template'] = template;
        tabValues.otherDoc['values'] = values;
        supportDocuments.values[data.tabId] = tabValues;
        application.supportDocuments = supportDocuments;
        applicationDao.upsertApplication(data.applicationId, application, function (upsertAppResp) {
          logger.log('INFO: deleteSupportDocument - end [RETURN=1]', data.applicationId);
          cb({
            values: supportDocuments.values
          });
        });
      } else {
        applicationDao.getApplication(data.applicationId, function (app) {
          if (app && !app.error) {
            var attachments = app['_attachments'];
            attachmentIdsArr.forEach(function (docId) {
              delete attachments[docId];
            });
            app['_attachments'] = attachments;

            dao.updDoc(data.applicationId, app, function (result) {
              if (!result.error || result.success) {
                // delete success
                dao.updateViewIndex("main", "summaryApps");
                template.splice(template.findIndex(function (item) {
                  return item.id == data.documentId;
                }), 1);
                delete values[data.documentId];
                tabValues.otherDoc['template'] = template;
                tabValues.otherDoc['values'] = values;
                supportDocuments.values[data.tabId] = tabValues;
                application.supportDocuments = supportDocuments;
                applicationDao.upsertApplication(data.applicationId, application, function (upsertAppResp) {
                  // update fail
                  if (upsertAppResp.success === false) {
                    logger.log('INFO: deleteSupportDocument - end [RETURN=2]', data.applicationId);
                    cb({
                      values: supportDocuments.values,
                      success: false,
                      error: upsertAppResp.error
                    });
                  }
                  logger.log('INFO: deleteSupportDocument - end [RETURN=3]', data.applicationId);
                  cb({
                    success: true,
                    values: supportDocuments.values
                  });
                });
              } else {
                // delete fail
                logger.log('INFO: deleteSupportDocument - end [RETURN=4]', data.applicationId);
                cb({
                  success: false,
                  values: supportDocuments.values,
                  error: result.error
                });
              }
            });
          } else {
            // can't get application
            logger.log('INFO: deleteSupportDocument - end [RETURN=5]', data.applicationId);
            cb({
              success: false,
              values: supportDocuments.values,
              error: app.error
            });
          }
        });
      }
    } else {
      logger.error('INFO: deleteSupportDocument - end [RETURN=-1]', data.applicationId);
      cb({ success: false });
    }
  });
};

module.exports.editSupportDocument = function (data, session, cb) {
  logger.log('INFO: editSupportDocument - start', data.applicationId);
  applicationDao.getApplication(data.applicationId, function (application) {
    if (!application.error) {
      var supportDocuments = application.supportDocuments;
      var tabValues = supportDocuments.values[data.tabId];
      // let otherDoc = supportDocuments.values.otherDoc;
      var otherDoc = tabValues.otherDoc;
      var values = otherDoc.values;
      var template = otherDoc.template;
      var attachmentIdsArr = [];
      var successList = [];
      var failList = [];
      if (values[data.documentId]) {
        values[data.documentId].forEach(function (item) {
          attachmentIdsArr.push(item.id);
        });
      }

      var changeData = otherDoc.template.find(function (temp) {
        return temp.id === data.documentId;
      });
      changeData.docName = data.docName;
      changeData.docNameOption = data.docName;
      changeData.title = data.docName;

      applicationDao.upsertApplication(data.applicationId, application, function (upsertAppResp) {
        logger.log('INFO: editSupportDocument - end [RETURN=1]', data.applicationId);
        cb({
          values: supportDocuments.values
        });
      });
    } else {
      logger.error('INFO: deleteSupportDocument - end [RETURN=-1]', data.applicationId);
      cb({ success: false });
    }
  });
};

module.exports.deleteSupportDocumentFile = function (data, session, cb) {
  logger.error('INFO: deleteSupportDocumentFile - start', data.applicationId);
  applicationDao.deleteAppAttachment(data.applicationId, data.attachmentId, session, function (resp) {
    if (resp.success == true) {
      applicationDao.getApplication(data.applicationId, function (application) {
        if (!application.error) {
          deleteSuppDocsFile(application, data.attachmentId, data.valueLocation, data.tabId, function (delResp) {
            if (delResp.success == true) {
              logger.log('INFO: deleteSupportDocumentFile - end [RETURN=1]', data.applicationId);
              cb({
                success: true,
                supportDocuments: delResp.supportDocuments
              });
            } else {
              logger.error('ERROR: deleteSupportDocumentFile - end [RETURN=-3]', data.applicationId);
              cb({ success: false });
            }
          });
        } else {
          logger.error('ERROR: deleteSupportDocumentFile - end [RETURN=-2]', data.applicationId);
          cb({ success: false });
        }
      });
    } else {
      logger.error('ERROR: deleteSupportDocumentFile - end [RETURN=-1]', data.applicationId);
      cb({ success: false });
    }
  });
};

// set the signExpiryShown as true
module.exports.setSignExpiryShown = function (data, session, cb) {
  if (data.appId) {
    applicationDao.getApplication(data.appId, function (app) {
      if (app && !app.error) {
        app.signExpiryShown = true;
        applicationDao.upsertApplication(data.appId, app, function (upsertResult) {
          if (upsertResult && !upsertResult.error) {
            cb({
              success: true
            });
          } else {
            cb({
              success: false
            });
          }
        }, true);
      } else {
        cb({
          success: false
        });
      }
    });
  } else {
    cb({
      success: false
    });
  }
};

module.exports.goApplication = function (data, session, cb) {
  var appId = data.id;
  var result = {};
  var crossAgeData = { appId: appId };
  var isFaChannel = session.agent.channel.type === 'FA';

  logger.log('INFO: goApplication - start', appId);

  async.waterfall([function (callback) {
    applicationDao.getApplication(appId, function (application) {
      if (application && !application.error) {
        bDao.getBundle(application.pCid, application.bundleId, function (bundle) {
          if (bundle && !bundle.error) {
            callback(null, application, bundle);
          } else {
            callback('Fail to get bundle ' + _.get(bundle, 'error'));
          }
        });
      } else {
        callback('Fail to get application ' + _.get(application, 'error'));
      }
    });
  }, function (application, bundle, callback) {
    defaultApplication.getAppFormTemplate(application.quotation, function (template, sections) {
      if (bundle.isFnaReportSigned) {
        logger.log('INFO: goApplication - frozenAppFormTemplateFnaQuestions', appId);
        commonApp.frozenAppFormTemplateFnaQuestions(template);
      }
      commonApp.getOptionsList(application, template, function () {
        // if start signature, frozen the application value
        if (application.isStartSignature) {
          result.application = application;
          logger.log('INFO: goApplication - getApplication frozenTemplate', appId);
          if (session.platform) {
            commonApp.frozenTemplateMobile(template);
          } else {
            commonApp.frozenTemplate(template);
          }
          result.template = template;
          callback(null, result.application, false);
        } else {
          result.template = template;
          // if it is not frozen, populate form values from Bundle and any update is make
          // if (isFaChannel) {
          //   result.application = application;
          //   getPNumber(result.application);
          // } else {
          commonApp.populateFromBundle(application, sections, isFaChannel).then(function (fvApp) {
            result.application = fvApp;
            callback(null, result.application, true);
          });
          // }
        }
      });
    });
  }, function (application, checkPolicyNumber, callback) {
    if (checkPolicyNumber) {
      var values = application.applicationForm.values;

      logger.log('INFO: goApplication - getPNumber has PNumber', appId, application.policyNumber ? true : false);
      if (application.policyNumber) {
        callback(null);
      } else {
        //determinate if next menu is insurability
        //if no menu insurability, check declaration
        var menus = values.menus || [];
        var checkedMenu = values.checkedMenu || [];
        var completedMenus = values.completedMenus || [];
        var targetMenu = menus.indexOf("menu_insure") > -1 ? "menu_insure" : "menu_declaration";
        var toGetPyNo = false;
        for (var mIndex = 0; mIndex < menus.length; mIndex++) {
          var curMenu = menus[mIndex];
          if (checkedMenu.indexOf(curMenu) == -1 || completedMenus.indexOf(curMenu) == -1) {
            if (curMenu == targetMenu) {
              toGetPyNo = true;
            }
            break;
          }
        }
        logger.log('INFO: goApplication - getPNumber get', toGetPyNo, 'for', appId);
        if (toGetPyNo) {
          getPolicyNumber(1, function (pNumbers) {
            var pNumber = pNumbers ? pNumbers[0] : null;
            application.policyNumber = pNumber;
            callback(null);
          });
        } else {
          callback(null);
        }
      }
    } else {
      callback(null);
    }
  }, function (callback) {
    logger.log('INFO: goApplication - checkCrossAge/SignatureExpiry', appId);

    // delete appPdf if the values changed
    var signExpireWarningDay = global.config.signExpireWarningDay;
    logger.debug('DEBUG: check signature expiry for showing warning: ', result.application.biSignedDate, result.application.appStatus, result.application.signExpiryShown, appId);
    // check signature expiry for showing warning
    if (result.application.biSignedDate && !result.application.isSubmittedStatus && !result.application.signExpiryShown) {
      var expiryTime = new Date();
      var biSignDate = new Date(result.application.biSignedDate);
      expiryTime.setDate(expiryTime.getDate() - signExpireWarningDay);
      if (biSignDate.getTime() < expiryTime.getTime()) {
        result.showSignExpiryWarning = true;
      }
    }

    // checkCrossAge(crossAgeData, session, (crossAgeResp) => {
    defaultApplication.checkCrossAge(result.application, function (crossAgeResp) {

      result.crossAgeObj = {};
      if (crossAgeResp.success && (crossAgeResp.insuredStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED || crossAgeResp.proposerStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED)) {
        result.crossAgeObj = {
          showCrossedAgeSignedMsg: true,
          insuredCrossed: crossAgeResp.insuredStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED,
          proposerCrossed: crossAgeResp.proposerStatus === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED
        };
      }

      // TODO: deprecate crossAgeObj, use crossAge
      if (crossAgeResp.success) {
        result.crossAge = crossAgeResp;
      }

      if (crossAgeResp.success && crossAgeResp.crossedAge) {
        result.application.isCrossAge = true;
      }

      logger.debug('DEBUG: goApplication - result', appId, result.showSignExpiryWarning, result.crossAgeObj.showCrossedAgeSignedMsg, result.application.isCrossAge);

      applicationDao.upsertApplication(result.application.id, result.application, function (resp) {
        if (resp && !resp.error) {
          result.success = true;
          callback(null);
        } else {
          result.success = false;
          callback('Fail to update appliation ' + _.get(resp, 'error'));
        }
      });
    });
  }], function (err) {
    if (err) {
      logger.error('ERROR: goApplication - end [RETURN=-1]', appId);
    } else {
      logger.log('INFO: goApplication - end [RETURN=1]', appId);
    }
    cb(result);
  });
};

//generate a new application
module.exports.apply = function (data, session, cb) {
  var isFaChannel = session.agent.channel.type === 'FA';
  var quotationId = data.id;

  logger.log('INFO: apply application - start', quotationId);
  if (!quotationId) {
    logger.log('INFO: apply application - end [RETURN=-1]');
    cb({ success: true, error: "please use the app properly" });
  }

  //get quotation
  var result = { success: true };

  quotDao.getQuotation(quotationId, function (quotation) {
    // let cid = _.get(quotation, 'pCid');
    var pCid = _.get(quotation, 'pCid');
    var iCid = _.get(quotation, 'iCid');
    var bundleId = _.get(quotation, 'bundleId');
    bDao.getDocByQuotId(pCid, quotationId).then(function (bQuotation) {
      if (_.get(bQuotation, 'appStatus') === 'INVALIDATED') {
        logger.log('INFO: apply action, document has been invalidated', quotationId);
        applicationDao.getAppListView('01', pCid, bundleId).then(function (appList) {
          //Modify by Alex Tse for CR49 Start**************
          getLabel(appList, function () {
            cb({
              success: true,
              isInvalidated: true,
              appList: appList
            });
          });
          //Modify by Alex Tse for CR49 End****************
        });
      } else {
        new Promise(function (resolve) {
          bDao.getCurrentBundle(pCid).then(function (bundle) {
            defaultApplication.getAppFormTemplate(quotation, function (template, sections, dynTemplate) {
              if (bundle.isFnaReportSigned) {
                logger.log('INFO: apply application - frozenAppFormTemplateFnaQuestions', quotationId);
                commonApp.frozenAppFormTemplateFnaQuestions(template);
              }
              //getOptionsList is done in genApplicationDoc
              genApplicationDoc(session, quotation, bundle, sections, template, function (app) {
                _.set(app, 'applicationForm.values.appFormTemplate.dynTemplate', dynTemplate);
                initCrossBorder(app);
                result.template = template;
                // if (isFaChannel) {
                //   resolve(app);
                // } else {
                commonApp.populateFromBundle(app, sections, isFaChannel).then(function (fvApp) {
                  resolve(fvApp);
                }).catch(function (error) {
                  logger.error('ERROR: populateFromBundle', _.get(app, 'id'), error);
                });;
                // }
              });
            });
          });
        }).then(function (application) {
          commonApp.getApplicationId(session, function (id) {
            application.id = id;
            applicationDao.updApplication(id, application, function () {
              dao.updateViewIndex("main", "summaryApps");
              dao.updateViewIndex("main", "quotationByAgent");

              bDao.onCreateApplication(pCid, quotation.id, application.id).then(function () {
                result.application = application;
                // dao.updateViewIndex("main", "contacts");
                // cb(result);
                clientDao.getProfile(pCid, true).then(function (profile) {
                  bDao.getApplyingApplicationsCount(application.bundleId).then(function (count) {
                    profile.applicationCount = count;
                    clientDao.updateProfile(pCid, profile).then(function () {
                      // result.profile = profile;
                      dao.updateViewIndex("main", "contacts");

                      logger.log('INFO: apply application - end', quotationId, id);
                      cb(result);
                    });
                  }).catch(function (error) {
                    logger.error('ERROR: getApplyingApplicationsCount', error);
                  });;
                });
              });
            });
          });
        }).catch(function (err) {
          logger.error('ERROR: apply application [Fail]', quotationId, err);
          cb({ success: false });
        });
      }
    });
  });
};

var getPlanDetailType = function getPlanDetailType(planCode) {
  var typeMap = {
    "SAV": 1
  };

  return typeMap[planCode];
};

var getQuotCheckedList = function getQuotCheckedList(applicationsList) {
  var bundle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var quotation = {};
  var quotCheckedList = [];
  var invalidStatuses = ['INVALIDATED', 'INVALIDATED_SIGNED'];

  if (_.get(bundle, 'isFnaReportSigned')) {
    quotCheckedList = _.get(bundle, 'clientChoice.quotCheckedList');
  } else {
    // bundle is not exists, or isFnaReportSigned is false
    for (var i in applicationsList) {
      if (!_.includes(invalidStatuses, _.get(applicationsList[i], 'appStatus'))) {
        if (applicationsList[i].type === 'application' || applicationsList[i].type === 'masterApplication') {
          quotation = applicationsList[i].quotation;
        } else {
          quotation = applicationsList[i];
        }

        quotCheckedList.push({
          id: quotation.id,
          checked: _.get(quotation, 'clientChoice.isClientChoiceSelected', false)
        });
      }
    }
  }

  return {
    isClientChoiceCompleted: _.get(bundle, 'clientChoice.isClientChoiceCompleted', true),
    quotCheckedList: quotCheckedList
  };
};

var deleteApplications = function deleteApplications(data, session, cb) {
  var applicationIds = data.deleteItemsSelectedArray;
  var pCid = _.get(data, 'applicationsList[0].pCid'); // consider getting the pCid from request
  // Bundle ID of applications that going to delete
  var exBundleId = _.get(data, 'applicationsList[0].bundleId');
  var currentBundleId = void 0;
  if (exBundleId) {
    var isFaChannel = session.agent.channel.type == 'FA';
    logger.log('INFO: deleteApplications - start', pCid);

    /** DO NOT REMOVE start */
    bDao.getCurrentBundle(pCid).then(function (currBundle) {
      var applications = _.get(currBundle, "applications", []);
      var applyingAppIds = _.filter(_.map(applications, function (app) {
        return app.appStatus === "APPLYING" ? app.applicationDocId : "";
      }), function (id) {
        return id.length > 0;
      });
      var deleteApplyingIdCount = 0;
      _.forEach(applyingAppIds, function (id) {
        if (applicationIds.indexOf(id) >= 0) {
          deleteApplyingIdCount += 1;
        }
      });
      var rollbackBundle = data.rollbackBundle || deleteApplyingIdCount === applyingAppIds.length;
      /** DO NOT REMOVE end */

      return Promise.all(_.map(applicationIds, function (docId) {
        return new Promise(function (resolve) {
          applicationDao.getApplication(docId, function (application) {
            if (application && !application.error) {
              if (_.get(application, 'childIds', []).length > 0) {
                //for shield
                var deletePromises = application.childIds.map(function (childId) {
                  return new Promise(function (cResolve, cReject) {
                    applicationDao.getApplication(childId, function (childApp) {
                      if (childApp && !childApp.error) {
                        applicationDao.deleteApplication(childId, childApp._rev, function (dRes) {
                          cResolve(dRes.success && childId);
                        });
                      } else if (childApp.error === 'not_found') {
                        //deleted
                        cResolve(childId);
                      } else {
                        cReject(new Error('Fail to delete child application for' + docId + _.get(childApp, 'error')));
                      }
                    });
                  });
                });
                Promise.all(deletePromises).then(function (dResults) {
                  logger.log('INFO: deleteApplications - delete child', docId, dResults);
                  applicationDao.deleteApplication(docId, application._rev, function (res) {
                    resolve(res.success && docId);
                  });
                });
              } else {
                applicationDao.deleteApplication(docId, application._rev, function (res) {
                  resolve(res.success && docId);
                });
              }
            } else {
              resolve(null);
            }
          });
        });
      })).then(function (ids) {
        var deletedIds = _.filter(ids, function (id) {
          return id;
        });
        return bDao.getCurrentBundle(pCid).then(function (bundle) {
          // No need to update bundle Status when the on deleting applications are not in current bundle
          currentBundleId = bundle.id;
          var hasCasesOtherThanInvalidate = false;
          // If the delete cases only contain invalidated cases
          // no need to rollback bundle status
          _.each(bundle.applications, function (app) {
            if (app.appStatus !== 'INVALIDATED' && (deletedIds.indexOf(app.applicationDocId) > -1 || deletedIds.indexOf(app.quotationDocId) > -1)) {
              hasCasesOtherThanInvalidate = true;
            }
          });
          if (bundle.id === exBundleId && hasCasesOtherThanInvalidate) {
            /** DO NOT REMOVE rollbackBundle, don't use data.rollbackBundle */
            if (rollbackBundle && bundle.status < bDao.BUNDLE_STATUS.FULL_SIGN) {
              return bDao.rollbackStatus(pCid, bDao.BUNDLE_STATUS.HAS_PRE_EAPP).then(function () {
                return { ids: ids, bundleId: bundle.id };
              });
            } else {
              return { ids: ids, bundleId: bundle.id };
            }
          } else {
            return { ids: ids, bundleId: bundle.id };
          }
        });
      }).then(function (result) {
        var deletedIds = _.filter(_.get(result, 'ids'), function (id) {
          return id;
        });
        // Seperate to delete invalidate Applications only
        // No bundle Status Update
        return bDao.onDeleteInvalidateApplications(pCid, deletedIds, isFaChannel, exBundleId, currentBundleId).then(function (bundleId) {
          return result;
        });
      }).then(function (result) {
        // Deleting invalidated cases in old bundle
        // No need to update bundle again, handled in onDeleteInvalidateApplications
        if (exBundleId === currentBundleId) {
          var deletedIds = _.filter(_.get(result, 'ids'), function (id) {
            return id;
          });
          return bDao.onDeleteApplications(pCid, deletedIds, isFaChannel, exBundleId, currentBundleId).then(function (bundleId) {
            if (bundleId) {
              return bundleId;
            } else {
              throw new Error('Fail to get bundle in onDeleteApplications');
            }
          });
        } else {
          return exBundleId;
        }
      }).then(function (bundleId) {
        // update contacts applying count
        if (currentBundleId === exBundleId) {
          clientDao.getProfile(pCid, true).then(function (profile) {
            bDao.getApplyingApplicationsCount(bundleId).then(function (count) {
              profile.applicationCount = count;
              clientDao.updateProfile(pCid, profile).then(function () {
                dao.updateViewIndex("main", "contacts");
                applicationDao.getAppListView('01', pCid, bundleId).then(function (appList) {
                  //Modify by Alex Tse for CR49 Start**********
                  getLabel(appList, function () {
                    var _getQuotCheckedList2 = getQuotCheckedList(appList),
                        quotCheckedList = _getQuotCheckedList2.quotCheckedList;

                    logger.log('INFO: deleteApplications - end [RETURN=1]', pCid);
                    cb({ success: true, result: appList, quotCheckedList: quotCheckedList });
                  });
                  //Modify by Alex Tse for CR49 End ***********
                });
              });
            });
          });
        } else {
          applicationDao.getAppListView('01', pCid, exBundleId).then(function (appList) {
            //Modify by Alex Tse for CR49 Start**********
            getLabel(appList, function () {
              var _getQuotCheckedList3 = getQuotCheckedList(appList),
                  quotCheckedList = _getQuotCheckedList3.quotCheckedList;

              logger.log('INFO: deleteApplications - end [RETURN=1]', pCid);
              cb({ success: true, result: appList, quotCheckedList: quotCheckedList });
            });
            //Modify by Alex Tse for CR49 End ***********
          });
        }
      });
    }).catch(function (err) {
      logger.error('ERROR: deleteApplications - end [RETURN=-1]', pCid, err);
      cb({ success: false });
    });
  } else {
    logger.error('ERROR: deleteApplications - end [RETURN=-1] CANNOT find bundleId in applications that going to delete', pCid, applicationIds);
    cb({ success: false });
  }
};
module.exports.deleteApplications = deleteApplications;

// confirm applicatino paid after invalidated (signed)
module.exports.confirmAppPaid = function (data, session, cb) {
  var updateApplication = function updateApplication(application) {
    applicationDao.updApplication(application.id, application, function (result) {
      if (result && !result.error) {
        dao.updateViewIndex("main", "summaryApps");
        cb({ success: true });
      } else {
        cb({ success: false, error: 'Update Application failure:' + application.id });
      }
    });
  };

  if (data && data.appId) {
    applicationDao.getApplication(data.appId, function (application) {
      if (application && !application.error) {
        application.isInvalidatedPaidToRLS = false;
        application.isInitialPaymentCompleted = true;
        application.lastUpdateDate = new Date().getTime();
        if (!application.isSubmittedStatus) {
          application.isSubmittedStatus = true;
          application.applicationSubmittedDate = new Date().getTime();
          logger.debug('DEBUG: Submit invalidated paid application (paid after invalidated):', application.id);

          if (session.platform) {
            updateApplication(application);
          } else {
            callApiComplete("/submitInvalidApp/" + application.id, "GET", {}, null, true, function (resp) {
              logger.log('INFO: Submit invalidated paid application (paid after invalidated):', application.id, resp);
              if (resp && resp.success) {
                updateApplication(application);
              }
            });
          }
        } else {
          updateApplication(application);
        }
      } else {
        cb({ success: false, error: 'Invalid Application:' + data.appId });
      }
    });
  } else {
    cb({ success: false, error: 'Invalid Application' });
  }
};

var genApplicationDoc = function genApplicationDoc(session, quotation, bundle, sections, template, cb) {
  var lang = 'en';
  var quotationId = quotation.id;
  logger.log('INFO: genApplicationDoc - starts', quotationId);
  var exFormValue = _.get(bundle, 'formValues');
  var now = new Date().getTime();
  var application = {
    type: "application",
    // appStatus: 'APPLYING', // TODO: still store appStatus in application?
    quotationDocId: quotation._id,
    applicationForm: {
      values: {}
    },
    isSubmittedStatus: false,
    isValid: true,
    isFailSubmit: false,
    isApplicationSigned: false,
    isProposalSigned: false,
    isStartSignature: false,
    isFullySigned: false,
    isBackDate: false,
    isMandDocsAllUploaded: false,
    applicationStartedDate: moment().toISOString(),
    applicationSignedDate: 0,
    applicationSubmittedDate: 0,
    applicationInforceDate: 0,
    appStep: 0,
    sameAs: quotation.sameAs,
    lastUpdateDate: now,
    createDate: now,
    quotation: quotation,
    bundleId: bundle.id || bundle._id,
    compCode: session.agent.compCode,
    agentCode: session.agent.agentCode,
    dealerGroup: session.agent.channel.code
  };

  var channel = session.agent ? session.agent.channel.code : '';
  var oneyearpremium = quotation.premium;
  var payMode = quotation.paymentMode;
  switch (payMode) {
    case 'S':
      oneyearpremium = quotation.premium * 2;
      break;
    case 'Q':
      oneyearpremium = quotation.premium * 4;
      break;
    case 'M':
      oneyearpremium = quotation.premium * 12;
      break;
    default:
      oneyearpremium = quotation.premium;
  }
  var extra = {
    isPhSameAsLa: quotation.pCid == quotation.iCid ? "Y" : "N",
    ccy: quotation.ccy,
    channel: commonApp.getChannelType(channel),
    channelName: channel,
    premium: oneyearpremium,
    baseProductCode: quotation.baseProductCode
  };
  var propHasTrustedIndividual = "";
  var laHasTrustedIndividual = [];

  var formValues = application.applicationForm.values;
  var phCid = "";
  var laCids = [];

  if (quotation.insured) {
    for (var laIndex = 0; laIndex < quotation.insured.length; laIndex++) {
      laCids.push(quotation.insured[laIndex]['cid']);
    }
  } else {
    laCids.push({ cid: quotation.iCid });
  }

  if (quotation.proposer) {
    phCid = quotation.proposer.cid;
  } else {
    phCid = quotation.pCid;
  }

  //todo: iCid would be totally different for multiple LA
  application.iCid = quotation.iCid;
  application.pCid = quotation.pCid;

  //set extra props for LA, such as channel, premium...
  var setExtraProp = function setExtraProp(fValues) {
    logger.log('INFO: genApplicationDoc - setExtraProp', quotationId);

    fValues.proposer.extra = _.cloneDeep(extra);
    fValues.proposer.extra.hasTrustedIndividual = propHasTrustedIndividual;

    if (fValues.insured && fValues.insured.length > 0) {
      for (var __laIndex = 0; __laIndex < fValues.insured.length; __laIndex++) {
        fValues.insured[__laIndex].extra = _.cloneDeep(extra);

        //set relations
        var _la = fValues.insured[__laIndex];
        var dps = fValues.proposer.personalInfo.dependants;
        for (var dpIndex = 0; dpIndex < dps.length; dpIndex++) {
          var dp = dps[dpIndex];
          if (dp.cid == _la.personalInfo.cid) {
            //let rship = (dp.relationship == "OTH") ? dp.relationshipOther : dp.relationship;
            fValues.insured[__laIndex].personalInfo.relationship = dp.relationship;
            fValues.insured[__laIndex].personalInfo.relationshipOther = dp.relationshipOther;
          }
        }
      }
    }
  };

  // set up plans values
  var handlePlans = function handlePlans() {
    logger.log("INFO: genApplicationDoc - handlePlans", quotationId);

    var transformPlanSumAssured = function transformPlanSumAssured(plans) {
      var changedPlans = _.cloneDeep(plans);
      _.forEach(changedPlans, function (plan) {
        if (plan.saViewInd !== "Y") {
          plan.sumInsured = " - ";
        }
        if (_.has(plan, 'multiplyBenefit') && plan.othSaInd !== 'Y') {
          plan.multiplyBenefit = ' - ';
        }
      });
      return changedPlans;
    };

    var planDetails = {
      planList: transformPlanSumAssured(quotation.plans),
      type: getPlanDetailType(quotation.baseProductCode),
      ccy: quotation.ccy,
      isBackDate: quotation.isBackDate,
      paymentMode: quotation.paymentMode,
      riskCommenDate: quotation.riskCommenDate,
      totYearPrem: quotation.premium,
      premium: quotation.premium,
      sumInsured: quotation.sumInsured,
      paymentTerm: quotation.paymentMode === 'L' ? 'Single' : 'Regular'
    };

    planDetails.displayPlanList = _.take(_.cloneDeep(planDetails.planList));
    planDetails.displayPlanList[0].totalPremium = planDetails.premium;
    planDetails.riderList = _.drop(_.cloneDeep(planDetails.planList));
    planDetails.hasRiders = _.isEmpty(planDetails.riderList) ? 'N' : 'Y';

    planDetails.insuranceCharge = _.get(quotation, 'policyOptionsDesc.coi[' + lang + ']');
    planDetails.rspSelect = _.get(quotation, 'policyOptions.rspSelect') || _.get(quotation, 'policyOptions.rspInd');

    if (quotation.plans[0].covClass) {
      planDetails.covClass = quotation.plans[0].covClass;
    }

    if (quotation.fund && quotation.fund.funds) {
      planDetails.fundList = quotation.fund.funds;
    }

    if (quotation.policyOptions) {
      planDetails = Object.assign(planDetails, quotation.policyOptions);
      if (quotation.policyOptionsDesc && quotation.policyOptionsDesc.multiFactor) {
        planDetails.multiFactorDesc = quotation.policyOptionsDesc.multiFactor.en;
      }
    }

    var biName = null;
    var baseProductCode = quotation.baseProductCode;
    var plans = quotation.plans;
    for (var pIndex = 0; pIndex < plans.length; pIndex++) {
      var plan = plans[pIndex];
      if (plan.covCode == baseProductCode) {
        biName = plan.covName;
      }
    }
    planDetails.planTypeName = biName;
    formValues.planDetails = planDetails;

    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function (mapping) {
      // set extra value such as channel
      formValues.appFormTemplate = mapping[quotation.baseProductCode].appFormTemplate;
      setExtraProp(formValues);
      application.applicationForm.values = formValues;

      // populateFromBundle(application, sections).then((app)=>{
      // REMOVE the code 'let app = application;' below if uncomment 'populateFromBundle'
      var app = application;
      commonApp.getOptionsList(app, template, function () {

        //Remove generated values which is not found in template
        var appTemplate = template.items[0].items[0]; //contain items of menuSection
        var appValuesOrg = app.applicationForm.values;
        var appValuesTemp = _.cloneDeep(app.applicationForm.values);
        var appValuesNew = _.cloneDeep(app.applicationForm.values);
        var trigger = {};

        //for create trigger object using template
        logger.log('INFO: genApplicationDoc - replaceAppFormValuesByTemplate start', quotationId);
        commonApp.replaceAppFormValuesByTemplate(appTemplate, appValuesOrg, appValuesTemp, trigger, [], 'en', null);
        logger.log('INFO: genApplicationDoc - replaceAppFormValuesByTemplate end', quotationId);

        //remove appForm values which is not found in template + trigger
        for (var key in appValuesOrg) {
          if (key == 'proposer') {
            logger.log('INFO: genApplicationDoc - removeAppFormValuesByTrigger: handle', key, quotationId);
            commonApp.removeAppFormValuesByTrigger(key, appValuesOrg[key], appValuesNew[key], appValuesOrg[key], trigger[key], false, appValuesOrg);
          } else if (key == 'insured') {
            for (var i = 0; i < appValuesOrg[key].length; i++) {
              var iOrgValues = appValuesOrg[key][i];
              var iNewValues = appValuesNew[key][i];
              var iTrigger = trigger[key][i];
              logger.log('INFO: genApplicationDoc - removeAppFormValuesByTrigger: handle', key, i, quotationId);
              commonApp.removeAppFormValuesByTrigger(key, iOrgValues, iNewValues, iOrgValues, iTrigger, false, appValuesOrg);
            }
          }
        }

        app.applicationForm.values = appValuesNew;

        logger.log("INFO: genApplicationDoc - end", quotationId);
        cb(app);
      });
      // });
    });
  };

  var handlePhRop = function handlePhRop(ePolicies, exFormValue) {
    logger.log("INFO: genApplicationDoc - handlePhRop", quotationId);

    var policies = Object.assign({}, {
      existLife: CommonFunctions.toNumber(ePolicies.existLife),
      existTpd: CommonFunctions.toNumber(ePolicies.existTpd),
      existCi: CommonFunctions.toNumber(ePolicies.existCi),
      existPaAdb: CommonFunctions.toNumber(ePolicies.existPaAdb),
      existTotalPrem: CommonFunctions.toNumber(ePolicies.existTotalPrem),
      replaceLife: 0,
      replaceTpd: 0,
      replaceCi: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0,
      existLifeAXA: 0,
      existTpdAXA: 0,
      existCiAXA: 0,
      existPaAdbAXA: 0,
      existTotalPremAXA: 0,
      existLifeOther: 0,
      existTpdOther: 0,
      existCiOther: 0,
      existPaAdbOther: 0,
      existTotalPremOther: 0,
      replaceLifeAXA: 0,
      replaceTpdAXA: 0,
      replaceCiAXA: 0,
      replacePaAdbAXA: 0,
      replaceTotalPremAXA: 0,
      replaceLifeOther: 0,
      replaceTpdOther: 0,
      replaceCiOther: 0,
      replacePaAdbOther: 0,
      replaceTotalPremOther: 0,
      replacePolTypeAXA: '-',
      replacePolTypeOther: '-'
    });
    if (quotation.clientChoice && quotation.clientChoice.recommendation) {
      var ccRop = quotation.clientChoice.recommendation.rop;
      policies.replaceLife = CommonFunctions.toNumber(ccRop.replaceLife);
      policies.replaceTpd = CommonFunctions.toNumber(ccRop.replaceTpd);
      policies.replaceCi = CommonFunctions.toNumber(ccRop.replaceCi);
      policies.replacePaAdb = CommonFunctions.toNumber(ccRop.replacePaAdb);
      policies.replaceTotalPrem = CommonFunctions.toNumber(ccRop.replaceTotalPrem);
    }

    if (extra.channel == "FA") {
      policies.havExtPlans = "";
      policies.havAccPlans = "";
      policies.havPndinApp = "";
      policies.isProslReplace = "";
    } else {
      //set up in fe
      policies.havExtPlans = policies.existLife > 0 || policies.existTpd > 0 || policies.existCi > 0 ? "Y" : "N";
      policies.havAccPlans = policies.existPaAdb > 0 ? "Y" : "N";

      //only display for LA = PH
      if (extra.isPhSameAsLa) {
        policies.isProslReplace = policies.replaceLife > 0 || policies.replaceTpd > 0 || policies.replaceCi > 0 || policies.replacePaAdb > 0 || policies.replaceTotalPrem > 0 ? "Y" : "N";
      } else {
        policies.isProslReplace = "";
      }
    }

    //retrieve from bundle
    if (exFormValue && exFormValue.policies) {
      logger.log("INFO: genApplicationDoc - pop pending policies from bundle", quotationId);
      policies.havPndinApp = exFormValue.policies.havPndinApp ? exFormValue.policies.havPndinApp : "";
      policies.pendingLife = CommonFunctions.toNumber(exFormValue.policies.pendingLife);
      policies.pendingTpd = CommonFunctions.toNumber(exFormValue.policies.pendingTpd);
      policies.pendingCi = CommonFunctions.toNumber(exFormValue.policies.pendingCi);
      policies.pendingPaAdb = CommonFunctions.toNumber(exFormValue.policies.pendingPaAdb);
      policies.pendingTotalPrem = CommonFunctions.toNumber(exFormValue.policies.pendingTotalPrem);
    }

    formValues.proposer.policies = policies;
    handlePlans();
  };

  var handleLaRop = function handleLaRop(sLA, ePolicies) {
    logger.log("INFO: genApplicationDoc - handleLaRop", quotationId);

    //avoid string format
    var policies = Object.assign({}, {
      existLife: CommonFunctions.toNumber(ePolicies.existLife),
      existTpd: CommonFunctions.toNumber(ePolicies.existTpd),
      existCi: CommonFunctions.toNumber(ePolicies.existCi),
      existPaAdb: CommonFunctions.toNumber(ePolicies.existPaAdb),
      existTotalPrem: CommonFunctions.toNumber(ePolicies.existTotalPrem),
      replaceLife: 0,
      replaceTpd: 0,
      replaceCi: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0,
      existLifeAXA: 0,
      existTpdAXA: 0,
      existCiAXA: 0,
      existPaAdbAXA: 0,
      existTotalPremAXA: 0,
      existLifeOther: 0,
      existTpdOther: 0,
      existCiOther: 0,
      existPaAdbOther: 0,
      existTotalPremOther: 0,
      replaceLifeAXA: 0,
      replaceTpdAXA: 0,
      replaceCiAXA: 0,
      replacePaAdbAXA: 0,
      replaceTotalPremAXA: 0,
      replaceLifeOther: 0,
      replaceTpdOther: 0,
      replaceCiOther: 0,
      replacePaAdbOther: 0,
      replaceTotalPremOther: 0,
      replacePolTypeAXA: '-',
      replacePolTypeOther: '-'
    });
    if (quotation.clientChoice && quotation.clientChoice.recommendation) {
      var ccRop = quotation.clientChoice.recommendation.rop;
      policies.replaceLife = CommonFunctions.toNumber(ccRop.replaceLife);
      policies.replaceTpd = CommonFunctions.toNumber(ccRop.replaceTpd);
      policies.replaceCi = CommonFunctions.toNumber(ccRop.replaceCi);
      policies.replacePaAdb = CommonFunctions.toNumber(ccRop.replacePaAdb);
      policies.replaceTotalPrem = CommonFunctions.toNumber(ccRop.replaceTotalPrem);
    }

    if (extra.channel == "FA") {
      policies.havExtPlans = "";
      policies.havAccPlans = "";
      policies.havPndinApp = "";
      policies.isProslReplace = "";
    } else {
      policies.havExtPlans = policies.existLife > 0 || policies.existTpd > 0 || policies.existCi > 0 ? "Y" : "N";
      policies.havAccPlans = policies.existPaAdb > 0 ? "Y" : "N";
      policies.isProslReplace = policies.replaceLife > 0 || policies.replaceTpd > 0 || policies.replaceCi > 0 || policies.replacePaAdb > 0 || policies.replaceTotalPrem > 0 ? "Y" : "N";
    }
    sLA.policies = policies;
  };

  //la
  var newLA = [];

  //get previous ph values or default one
  var getPHProfile = function getPHProfile() {
    logger.log("INFO: genApplicationDoc - getPHProfile", quotationId);

    formValues.insured = newLA;
    clientDao.getClientById(phCid, function (phDoc) {
      var hasTrustedIndividual = 'N';
      dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], function (pdaDoc) {
        if (pdaDoc && !pdaDoc.error) {
          logger.log('INFO: genApplicationDoc - getPHProfile - populate PDA data', quotationId);
          hasTrustedIndividual = _.get(pdaDoc, 'trustedIndividual', 'N');
        }

        commonApp.removeInvalidProfileProp(phDoc);
        phDoc.cid = phDoc._id || phDoc.id;
        //push personal info
        var newPH = {
          personalInfo: phDoc,
          declaration: {
            trustedIndividuals: hasTrustedIndividual == "Y" ? phDoc.trustedIndividuals : {},
            ccy: quotation.ccy
          },
          residency: {},
          extra: extra,
          insurability: {
            "LIFESTYLE01": phDoc.isSmoker
          }
        };
        propHasTrustedIndividual = hasTrustedIndividual;
        formValues.proposer = newPH;

        //policies
        clientChoiceHandler.getExistingPolicies(phCid, phCid).then(function (ePolicies) {
          handlePhRop(ePolicies);
        }).catch(function (error) {
          logger.error('ERROR: getExistingPolicies', error);
          cb(null);
        });
      });
    });
  };

  //multiple LA
  var getLAProfile = function getLAProfile(laIndex) {
    logger.log("INFO: genApplicationDoc - getLAProfile", quotationId, 'laIndex', laIndex);

    if (laIndex < laCids.length) {
      var cid = laCids[laIndex].cid;
      clientDao.getClientById(cid, function (laDoc) {
        if (laDoc && !laDoc.error) {
          clientChoiceHandler.getExistingPolicies(phCid, cid).then(function (ePolicies) {
            commonApp.removeInvalidProfileProp(laDoc);
            laDoc.cid = laDoc._id;

            var sLA = {
              personalInfo: laDoc,
              declaration: {},
              residency: {},
              extra: extra,
              insurability: {
                "LIFESTYLE01": laDoc.isSmoker
              }
            };
            handleLaRop(sLA, ePolicies);
            newLA.push(sLA);

            getLAProfile(laIndex + 1);
          }).catch(function (error) {
            logger.error('ERROR: getExistingPolicies', quotationId, error);
          });
        } else {
          logger.error('ERROR: genApplicationDoc - getLAProfile', quotationId, _.get(laDoc, 'error'));
          cb(null);
        }
      });
    } else {
      getPHProfile();
    }
  };

  //check if it has only 1 person for the app: ph = la
  //todo: check if PH is one of LA in batch 2
  //laCids.length == 1 && laCids[0].cid == phCid
  if (quotation.sameAs == 'Y') {
    getPHProfile();
  } else {
    getLAProfile(0);
  }
};

var getRunningSeqNo = function getRunningSeqNo(agentCode, cb) {
  var docId = "RUN_SEQ_" + agentCode;
  var seq = -1;

  var update = function update(result) {
    if (result && !result.error) {
      cb(seq);
    } else {
      cb(seq);
    }
  };

  var afterGet = function afterGet(result) {
    if (result) {
      delete result.error;
      delete result.reason;
      seq = result.seq || 0;
      result.seq = seq + 1;
      dao.updDoc(docId, result, update);
    } else {
      seq = 1;
      var newDoc = {
        seq: 1
      };
      dao.updDoc(docId, newDoc, update);
    }
  };

  dao.getDoc(docId, afterGet);
};

module.exports.getRunningSeqNo = getRunningSeqNo;

//NO-USD
// var getDynFormId = function(planCode, planCat){
//   return "appform_" + planCode + "_DYN_" + planCat;
// }

// var getAppFormSections = function(sections){
//  let signSection = {
//    "id": "stepSign",
//    "type": "section",
//    "title": "Signature",
//    "detailSeq": 2,
//    "items": [
//      {
//        "id": "sign",
//        "type": "signature"
//      }
//    ]
//  }

//  let paymentSection = {
//    "id": "stepPay",
//    "type": "section",
//    "title": "Payment",
//    "detailSeq": 3,
//    "items": [
//      {
//        "id": "pay",
//        "type": "payment"
//      }
//    ]
//  }

//  let submitSection = {
//    "id": "stepSubmit",
//    "type": "section",
//    "title": "Submit",
//    "detailSeq": 4,
//    "items": [
//      {
//        "id": "submit",
//        "type": "submission"
//      }
//    ]
//  }

//  sections.push(signSection);
//  sections.push(paymentSection);
//  sections.push(submitSection);
// }

//Function: for retrieving values in Payment Page
var getQuotationPremiumDetails = function getQuotationPremiumDetails(docId, cb) {
  logger.log('INFO: getQuotationPremiumDetails - start', docId);

  var values = null;
  var initBasicPrem = 0;
  var backdatingPrem = 0;
  var topupPrem = 0;
  var initRspPrem = 0;
  var initTotalPrem = 0;
  var quot = null;
  var backdatedDay = 0;

  var signedDate = null;
  var backdate = null;

  applicationDao.getApplication(docId, function (app) {
    if (app._id) {
      quot = app.quotation;
      var retValues = function retValues() {
        logger.log('INFO: getQuotationPremiumDetails - end', docId);
        cb({
          values: values,
          paymentMethod: quot.policyOptions && quot.policyOptions.paymentMethod ? quot.policyOptions.paymentMethod : "",
          covCode: quot.baseProductCode,
          paymentMode: quot.paymentMode,
          isMandDocsAllUploaded: app.isMandDocsAllUploaded,
          isSubmitted: app.isSubmittedStatus,
          notPaybySGD: quot.ccy !== 'SGD' ? true : false
        });
      };

      var wrapUp = function wrapUp() {
        applicationDao.upsertApplication(app._id, app, function (upApp) {
          if (upApp && !upApp.error) {
            retValues();
          } else {
            logger.error('ERROR: getQuotationPremiumDetails - end [RETURN=-3]', _.get(upApp, 'error'));
            cb({ success: false, result: upApp });
          }
        });
      };

      //Retrieve stored values
      if (app.payment) {
        values = app.payment;
        if (app.payment.trxNo && ['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(app.payment.initPayMethod) >= 0) {
          var status = app.payment.trxStatus;
          var trxStatusRemark = '';
          if (status == 'I') {
            trxStatusRemark = 'O';
          } else {
            trxStatusRemark = status;
          }
          if (trxStatusRemark != app.payment.trxStatusRemark) {
            app.payment.trxStatusRemark = trxStatusRemark;
            values = app.payment;
            wrapUp();
          } else {
            retValues();
          }
        } else {
          retValues();
        }
      } else {
        values = {
          proposalNo: '',
          policyCcy: '',
          initBasicPrem: 0,
          backdatingPrem: 0,
          topupPrem: 0,
          initRspPrem: 0,
          initTotalPrem: 0,
          initPayMethod: '',
          initPayMethodButton: false,
          ttRemittingBank: '',
          ttDOR: '',
          ttRemarks: '',
          subseqPayMethod: '',
          paymentMode: '',
          rspSelect: false,
          covCode: '',
          paymentMethod: '',
          cpfisoaBlock: {
            idCardNo: ''
          },
          cpfissaBlock: {
            idCardNo: ''
          },
          srsBlock: {
            idCardNo: ''
          }
        };

        clientDao.getProfileById(quot.pCid, function (pProfile) {
          if (pProfile && !pProfile.error) {
            values.initPayMethodButton = app.hasOwnProperty('isInitialPaymentCompleted') ? app.isInitialPaymentCompleted : false;
            values.proposalNo = app.policyNumber || app._id;
            values.policyCcy = quot.ccy;
            values.baseProductCode = quot.baseProductCode;
            values.paymentMode = quot.paymentMode;
            values.rspSelect = _.get(quot, 'policyOptions.rspSelect') || _.get(quot, 'policyOptions.rspInd') ? 'Y' : 'N';
            values.covCode = quot.baseProductCode;
            values.paymentMethod = quot.policyOptions && quot.policyOptions.paymentMethod ? quot.policyOptions.paymentMethod : "";
            values.cpfisoaBlock.idCardNo = pProfile.idCardNo;
            values.cpfissaBlock.idCardNo = pProfile.idCardNo;
            values.srsBlock.idCardNo = pProfile.idCardNo;
            // if payment mode === "Single Premium"
            if (values.paymentMode === "L" || app.quotation.ccy !== "SGD") {
              values.subseqPayMethod = undefined;
            } else {
              values.subseqPayMethod = quot.paymentMode === "M" ? "Y" : "N";
            }

            // Calculate All Premium
            initBasicPrem = quot.premium * (quot.paymentMode === 'M' ? 2 : 1);

            //- Backdating Premium
            if (quot.isBackDate == 'Y' || app.isBackDate) {
              signedDate = new Date(app.applicationSignedDate.substring(0, 10) + 'T00:00:00.000Z');
              backdate = new Date(quot.riskCommenDate);
              backdatedDay = Math.ceil((signedDate.getTime() - backdate.getTime()) / (1000 * 3600 * 24));
              var backdatedMonth = 0;
              if (backdatedDay == 0) {
                backdatedMonth = 0;
              } else if (backdatedDay > 0 && backdatedDay <= 30) {
                backdatedMonth = 1;
              } else if (backdatedDay > 30 && backdatedDay <= 61) {
                backdatedMonth = 2;
              } else if (backdatedDay > 61 && backdatedDay <= 91) {
                backdatedMonth = 3;
              } else if (backdatedDay > 91 && backdatedDay <= 122) {
                backdatedMonth = 4;
              } else if (backdatedDay > 122 && backdatedDay <= 152) {
                backdatedMonth = 5;
              } else if (backdatedDay > 152) {
                backdatedMonth = 6;
              }

              backdatingPrem = 0;
              if (quot.paymentMode == 'M') {
                backdatingPrem = quot.premium * backdatedMonth;
              } else if (quot.paymentMode == 'Q') {
                if (backdatedMonth > 1 && backdatedMonth <= 4) {
                  backdatingPrem = quot.premium;
                } else if (backdatedMonth > 4 && backdatedMonth <= 6) {
                  backdatingPrem = quot.premium * 2;
                }
              } else if (quot.paymentMode == 'S') {
                backdatingPrem = quot.premium * (backdatedMonth > 4 && backdatedMonth <= 6 ? 1 : 0);
              }
            }

            // Calculate RSP Premium
            initRspPrem = 0;
            var rspAmount = _.get(quot, 'policyOptions.rspAmount') || _.get(quot, 'policyOptions.rspAmt', 0);
            if (rspAmount > 0) {
              // rspPayFreq is quotation field from SAV, IND, SHIELD. rspFrequency is quotation field from FLEXI
              var rspPayFreq = _.get(quot, 'policyOptions.rspPayFreq');

              // Init RSP Premium should be two times of RSPAmount for monthly mode, and equals to RSPAmount for Annaul/Semi-Annual/Quarter modes
              if (rspPayFreq === "monthly" || rspPayFreq === "M") {
                initRspPrem = rspAmount * 2;
              } else {
                initRspPrem = rspAmount;
              }
            }

            //TODO: Update the folloiwng premium logic
            topupPrem = _.get(quot, 'policyOptions.topUpAmt', 0);

            initTotalPrem = initBasicPrem + backdatingPrem + topupPrem + initRspPrem;

            values.initBasicPrem = initBasicPrem;
            if (backdatingPrem > 0) {
              values.backdatingPrem = backdatingPrem;
            }
            if (topupPrem > 0) {
              values.topupPrem = topupPrem;
            }
            if (initRspPrem > 0) {
              values.initRspPrem = initRspPrem;
            }
            values.initTotalPrem = initTotalPrem;

            app.payment = _.cloneDeep(values);
            wrapUp();
          } else {
            logger.error('ERROR: getQuotationPremiumDetails - end [RETURN=-2]', docId, quot.pCid);
            cb(false);
          }
        });
      }
    } else {
      logger.error('ERROR: getQuotationPremiumDetails - end [RETURN=-1]', docId);
      cb(false);
    }
  });
};

var setupPaymentOptions = function setupPaymentOptions(paymentTemplate, payOptionsTmpl, premiumValues, isFaChannel) {

  var payMethod = paymentTemplate.items[1];
  var payMethod12Box = payMethod.items[0];
  var payOptions = payMethod12Box.items[0].options;
  var subseqPayCheckbox = payMethod.items[payMethod.items.length - 1].items[0];

  var getOptionsAvailable = function getOptionsAvailable(payOptionsTmpl, covCode, paymentMethod, paymentMode, notPaybySGD) {
    var foundObj = {};
    var optionsByCovCode = _.filter(payOptionsTmpl.items, function (item) {
      return item.covCode === covCode;
    });

    _.forEach(optionsByCovCode, function (item) {
      if (notPaybySGD) {
        if (item.notPaybySGD) {
          foundObj = item;
          return false;
        }
      } else {
        if (item.paymentMethod === paymentMethod && _.includes(item.paymentMode, paymentMode)) {
          foundObj = item;
          return false;
        }
      }
    });

    return foundObj.payOptions || [];
  };

  var optionsAvailable = getOptionsAvailable(payOptionsTmpl, premiumValues.covCode, premiumValues.paymentMethod, premiumValues.paymentMode, premiumValues.notPaybySGD) || [];

  if (isFaChannel && premiumValues.paymentMethod !== 'cpfisoa' && premiumValues.paymentMethod !== 'cpfissa' && premiumValues.paymentMethod !== 'srs') {
    optionsAvailable.push("payLater");
  }

  // If Initial Payment includes topup, then dont display credit card and dbs creadit card
  // If it is single premium mode, then dont display credit card and dbs creadit card
  if (_.get(premiumValues, 'values.topupPrem') > 0) {
    _.remove(optionsAvailable, function (item) {
      return item === 'crCard' || item === 'dbsCrCardIpp';
    });
  } else if (premiumValues.paymentMode === 'L') {
    _.remove(optionsAvailable, function (item) {
      return item === 'crCard';
    });
  }

  if (_.get(premiumValues, 'values.initTotalPrem') >= 5000) {
    _.remove(optionsAvailable, function (item) {
      return item === 'cash';
    });
  }

  payOptions = _.filter(payOptions, function (item) {
    return _.includes(optionsAvailable, item.value);
  });

  _.set(paymentTemplate, 'items[1].items[0].items[0].options', payOptions);

  //Set default Subsequent Payment Method for monthly
  if (_.get(premiumValues, 'values.paymentMode') === "M") {
    subseqPayCheckbox.value = "Y";
    subseqPayCheckbox["disabled"] = true;
  }
};

var _getPaymentTemplateValues = function _getPaymentTemplateValues(data, session, cb) {
  var payOptionsId = 'payOptions';
  var docId = data.docId;
  var isFaChannel = session.agent.channel.type === 'FA';

  var template = {};
  var values = {};

  logger.log('INFO: getPaymentTemplateValues - start', docId);

  var callBackFalse = function callBackFalse(number) {
    logger.error('ERROR: getPaymentTemplateValues - end [RETURN=-' + number + ']', docId);
    cb(false);
  };

  dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.PAYMENT, function (template) {
    if (template && !template.error) {
      var retTemplate = _.cloneDeep(template);

      getQuotationPremiumDetails(docId, function (premiumValues) {
        if (premiumValues) {
          dao.getDoc(payOptionsId, function (payOptionsTmpl) {
            if (payOptionsTmpl && !payOptionsTmpl.error) {

              setupPaymentOptions(retTemplate, payOptionsTmpl, premiumValues, isFaChannel);

              values = _.cloneDeep(premiumValues.values);
              if (premiumValues.isSubmitted) {
                logger.log('INFO: getPaymentTemplateValues - frozenTemplate', docId);
                if (session.platform) {
                  commonApp.frozenTemplateMobile(retTemplate);
                } else {
                  commonApp.frozenTemplate(retTemplate);
                }
              }

              logger.log('INFO: getPaymentTemplateValues - end [RETURN=1]', docId);
              cb({
                success: true,
                template: retTemplate,
                values: values,
                isMandDocsAllUploaded: premiumValues.isSubmitted || premiumValues.isMandDocsAllUploaded,
                isSubmitted: premiumValues.isSubmitted
              });
            } else {
              callBackFalse(3);
            }
          });
        } else {
          callBackFalse(2);
        }
      });
    } else {
      callBackFalse(1);
    }
  });
};
module.exports.getPaymentTemplateValues = _getPaymentTemplateValues;

var _getTransactionNoForShield = function _getTransactionNoForShield(appId) {
  var raw = appId.substr(2, 12) + '-' + parseInt(new Date().getTime() / 1000);
  var strs = raw.split('-');

  var trxNo = "";

  for (var i in strs) {
    var str = strs[i];
    var n = Math.floor(Number(str));
    if (String(n) === str && n >= 0) {
      trxNo += n.toString(16);
    } else {
      trxNo += n;
    }
  }
  return trxNo;
};

var _getTransactionNo = function _getTransactionNo(policyNo) {
  var raw = policyNo + '-' + parseInt(new Date().getTime() / 1000);
  var strs = raw.split('-');

  var trxNo = "";

  for (var i in strs) {
    var str = strs[i];
    var n = Math.floor(Number(str));
    if (String(n) === str && n >= 0) {
      trxNo += n.toString(16);
    } else {
      trxNo += n;
    }
  }
  return trxNo;
};

var _getPolicyNumberByMapping = function _getPolicyNumberByMapping(iCidMapping) {
  var policyNumbers = [];
  _.each(iCidMapping, function (cidObj) {
    _.each(cidObj, function (obj) {
      if (obj.policyNumber) {
        policyNumbers.push(obj.policyNumber);
      }
    });
  });
  return policyNumbers;
};

module.exports.getPaymentUrl = function (data, session, cb) {
  var docId = data.docId,
      authToken = data.authToken,
      webServiceUrl = data.webServiceUrl;

  logger.log('INFO: getPaymentUrl - start', docId);
  _updatePaymentMethods(data, session, function (app) {

    if (app && !app.error && app._id) {
      var isShield = _.get(app, 'quotation.quotType') === 'SHIELD';
      var trxNo = isShield ? _getTransactionNoForShield(app.id) : _getTransactionNo(app.policyNumber);
      var amount = isShield ? app.payment.totCashPortion : app.payment.initTotalPrem;
      var quotationCcy;
      if (isShield) {
        _.each(_.get(app, 'quotation.insureds'), function (obj) {
          quotationCcy = obj.ccy;
          return false;
        });
      }
      var currency = app.payment.policyCcy || _.get(app, 'applicationForm.values.insured[0].extra.ccy') || _.get(app, 'applicationForm.values.proposer.extra.ccy') || quotationCcy;

      (0, _axiosWrapper2.default)({
        url: "/payment/url",
        data: {
          trxNo: trxNo,
          appId: app._id,
          policyNo: isShield ? app.id : app.policyNumber,
          amount: amount,
          currency: currency,
          method: data.values.initPayMethod,
          isShield: isShield,
          lob: isShield ? 'Health' : 'Life',
          relatedPolicyNumber: isShield ? _getPolicyNumberByMapping(_.get(app, 'iCidMapping')) : [],
          relatedApplicationNumber: isShield ? _.get(app, 'childIds') : []
        },
        method: "POST",
        headers: {
          Authorization: 'Bearer ' + authToken
        },
        webServiceUrl: webServiceUrl
      }).then(function (_ref) {
        var result = _ref.data.result;

        if (result && !result.error) {
          var url = result.url;

          var params = result.params;
          var paramStr = "";
          for (var p in params) {
            if (p) {
              paramStr += (paramStr ? "&" : "?") + (p + "=" + params[p]);
            }
          }

          app.payment.trxNo = trxNo;
          app.payment.trxStatus = result.status;
          app.payment.trxStartTime = result.timestamp;
          app.payment.trxMethod = app.payment.initPayMethod = data.values.initPayMethod;
          app.payment.trxStatusRemark = result.status;
          app.payment.trxEnquiryStatus = "No";

          applicationDao.upsertApplication(app._id, app, function (upApp) {
            if (upApp) {
              logger.log('INFO: getPaymentUrl - end [RETURN=1]', docId);
              if (isShield) {
                payment.saveParentPaymenttoChild(app, function (error, parentApplication) {
                  cb({
                    success: true,
                    url: url + paramStr,
                    application: app
                  });
                });
              } else {
                cb({
                  success: true,
                  url: url + paramStr,
                  application: app
                });
              }
            } else {
              logger.error('ERROR: getPaymentUrl - end [RETURN=-2]', docId, _.get(upApp, 'error'));
              cb({
                success: false,
                error: "field to upsert application"
              });
            }
          });
        } else {
          logger.error('ERROR: getPaymentUrl - end [RETURN=-1]', docId, _.get(result, 'error'));
          cb({
            success: false,
            error: "field to generate url"
          });
        }
      }).catch(function (e) {
        console.log(e);
        cb({
          success: false,
          error: e
        });
      });
    }
  });
};

module.exports.checkPaymentStatus = function (data, session, cb) {
  var trxNo = data.trxNo;
  var appId = data.appId;

  logger.log('INFO: checkPaymentStatus - start', appId);
  applicationDao.getApplication(appId, function (app) {
    if (app && !app.error && app.payment) {
      var status = app.payment.trxStatus;
      var trxStatusRemark = '';
      if (status == 'I') {
        trxStatusRemark = 'O';
      } else {
        trxStatusRemark = status;
      }
      if (trxStatusRemark != app.payment.trxStatusRemark) {
        app.payment.trxStatusRemark = trxStatusRemark;
        applicationDao.upsertApplication(appId, app, function (upsertResult) {
          if (upsertResult && !upsertResult.error) {
            logger.log('INFO: checkPaymentStatus - end [RETURN=1]', appId);
            cb({
              success: true,
              paymentValues: app.payment
            });
          } else {
            logger.error('ERROR: checkPaymentStatus - end [RETURN=-2]', appId, _.get(upsertResult, 'error'));
            cb({
              success: false
            });
          }
        });
      } else {
        logger.log('INFO: checkPaymentStatus - end [RETURN=2]', appId);
        cb({
          success: true,
          paymentValues: app.payment
        });
      }
    } else {
      logger.error('ERROR: checkPaymentStatus - end [RETURN=-1]', appId, _.get(app, 'error'));
      cb({
        success: false
      });
    }
  });
};

var getSubmissionTemplate = function getSubmissionTemplate(app, cb) {
  logger.log('INFO: getSubmissionTemplate');
  dao.getDoc(commonApp.TEMPLATE_NAME.SUBMISSION, function (res) {
    if (res && !res.error) {
      cb(res);
    } else {
      cb(false);
    }
  });
};

var getSubmissionValues = function getSubmissionValues(app, session, submitStatus, failMsg, cb) {
  logger.log('INFO: getSubmissionValues');
  var quot = null;
  var values = {
    agentChannelType: session.agent.channel.type == 'FA' ? 'Y' : 'N',
    submitStatus: submitStatus,
    receiveEmailFa: app.submission && app.submission.receiveEmailFa ? app.submission.receiveEmailFa : "",
    mandDocsAllUploaded: submitStatus == 'SUCCESS' ? 'Y' : app.isMandDocsAllUploaded ? 'Y' : 'N'
  };
  var covName = void 0;

  quot = app.quotation;
  if (_.get(quot, 'quotType') === 'SHIELD') {
    covName = _.get(app, 'quotation.covName');
  } else {
    var basicPlan = quot.plans.find(function (plan) {
      return plan.covCode == quot.baseProductCode;
    });
    covName = _.get(basicPlan, 'covName');
  }

  if (submitStatus == 'FAIL') {
    values.failMsg = failMsg;
    values.lastSubmitDateTime = CommonFunctions.getFormattedDateTime(new Date());
  } else if (submitStatus == 'SUCCESS') {
    values.eSubmitDateTime = CommonFunctions.getFormattedDateTime(app.applicationSubmittedDate);
    values.proposerName = quot.pFullName;
    values.planName = covName;
    values.policyNumber = app.policyNumber ? app.policyNumber : "";
  }
  cb({ values: values });
};

var appFormSubmission = function appFormSubmission(data, session, cb) {
  logger.log('INFO: appFormSubmission', data.docId);
  var docId = data.docId;
  var template = {};
  var values = data.values;
  var agentCode = session.agentCode;
  var agentProfile = data.agentProfile;

  var errorCB = function errorCB(app, msg) {
    logger.error('ERROR: appFormSubmission - end [RETURN=-2]', docId, msg);
    getSubmissionValues(app, session, 'FAIL', 'Submission Fail. Please try again later', function (res) {
      cb({
        success: false,
        values: res.values,
        isMandDocsAllUploaded: true,
        isSubmitted: false
      });
    });
  };

  var updateViewedList = function updateViewedList(app) {
    if (_.get(app, 'payment.initialPayment.paymentMethod') && _.get(_.get(app, 'supportDocuments.viewedList'), agentCode)) {
      app.supportDocuments.viewedList[agentCode][app.payment.initialPayment.paymentMethod] = false;
    }
  };

  //Step 1
  var genPdfBeforeSubmission = function genPdfBeforeSubmission(app, lang, callback) {
    logger.log('INFO: appFormSubmission - (1) genPdfBeforeSubmission', docId);
    genAppFormPdfByPaymentData(app, lang, function (newApp) {
      if (newApp && !newApp.error) {
        getECpdPdfByAppFormData(newApp, lang, function (newApp2) {
          if (newApp2 && !newApp2.error) {
            callback(newApp2);
          } else {
            callback(false);
          }
        });
      } else {
        callback(false);
      }
    });
  };

  //Step 2
  var doSubmission = function doSubmission(app, callback) {
    logger.log('INFO: appFormSubmission - (2) doSubmission', docId);
    var clientProfileId = _.get(app, 'quotation.pCid');
    //Prepare Submission Email
    clientDao.getProfile(clientProfileId, true).then(function (clientProfile) {
      applicationDao.getSubmissionEmailTemplate(session.agent.channel.type == 'FA', function (result) {
        if (result && !result.error) {
          // Check whether need to send email
          var now = Date.now();
          var filteredTemplate = _.filter(result.eSubmissionEmails, function (template) {
            return template.productCode.indexOf(app.quotation.baseProductCode) != -1;
          });

          if (filteredTemplate && filteredTemplate.length != 0) {
            logger.log('INFO: appFormSubmission - (2) doSubmission filteredTemplate', app.quotation.baseProductCode, session.agent.channel.type, docId);
            var emailValues = Object.assign({}, data);
            var attachmentKeys = filteredTemplate[0].attachmentKeys;

            var eCpdIndex = attachmentKeys.indexOf('eCpd');
            if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(_.get(app, 'payment.initPayMethod')) < 0 && eCpdIndex > -1) {
              attachmentKeys.splice(eCpdIndex, 1);
            }
            emailValues['agentName'] = _.get(data, 'agentProfile.name');
            emailValues['agentContactNum'] = _.get(data, 'agentProfile.mobile');
            emailValues['agentEmail'] = _.get(data, 'agentProfile.email');
            emailValues['agentTitle'] = '';
            emailValues['clientName'] = _.get(data, 'clientProfile.fullName');
            emailValues['clientEmail'] = _.get(data, 'clientProfile.email');
            emailValues['clientId'] = _.get(data, 'clientProfile.cid');
            emailValues['clientTitle'] = _.get(data, 'clientProfile.title');
            emailValues["emailContent"] = filteredTemplate[0].emailContent;
            emailValues["attKeys"] = attachmentKeys;
            emailValues["policyNumber"] = app.policyNumber ? app.policyNumber : "";
            emailValues["submissionDate"] = CommonFunctions.getFormattedDate(now);
            emailValues["quotId"] = app.quotation.id;
            emailValues["appId"] = app._id;
            emailValues["receiveEmailFa"] = session.agent.channel.type == 'FA' ? values.receiveEmailFa ? values.receiveEmailFa : "N" : "Y";

            //Create Approval Case and update applciation status in bundle
            ApprovalHandler.createApprovalCase({ ids: [app._id] }, session, function (eAppResult) {
              dao.updateViewIndex("main", "applicationsByAgent");
              dao.updateViewIndex("main", "approvalDetails");
              bDao.onSubmitApplication(app.pCid, app._id).then(function (result) {
                if (result.ok && eAppResult.success) {
                  //Send email
                  aEmaillHandler.SubmittedCaseNotification({ id: app.policyNumber }, session);
                  commonApp.prepareSubmissionEmails(emailValues, session, function (resp) {
                    if (resp && resp.success) {
                      app.isSubmittedStatus = true;
                      // app.appStatus = 'SUBMITTED'; // TODO: still store appStatus inside application?
                      app.applicationSubmittedDate = new Date().toISOString();
                      if (session.agent.channel.type == 'FA') {
                        app.submission = {};
                        app.submission.receiveEmailFa = values.receiveEmailFa;
                      }
                      if (_.get(session, 'agent.agentCode') !== _.get(session, 'agent.managerCode')) {
                        commonApp.transformSuppDocsPolicyFormValues(app);
                      }
                      updateViewedList(app);
                      applicationDao.upsertApplication(app._id, app, function (res) {
                        if (res && !res.error) {
                          callback(res);
                        } else {
                          callback(new Exception.UpdateDocException('Fail to update status'));
                        }
                      });
                    } else {
                      app.isFailSubmit = false;
                      applicationDao.upsertApplication(app._id, app, function (res) {
                        if (res && !res.error) {
                          callback(res);
                        } else {
                          callback(new Exception.EmailException('Fail to send email'));
                          //errorCB(false, 'Fail to send email');
                        }
                      });
                    }
                  });
                } else {
                  callback(new Exception.CreateDocException('Fail to create/ update Bundle'));
                }
              }).catch(function (Err) {
                logger.error('ERROR: appFormSubmission', Err);
                callback(new Exception.CreateDocException('Fail to create/ update Bundle'));
              });
            });
          } else {
            callback(new Exception.GetTemplateException('Fail to get email template from product'));
            //errorCB(false, 'Fail to get email template from product');
          }
        } else {
          callback(new Exception.GetTemplateException('Fail to get email template'));
        }
      });
    }).catch(function (error) {
      logger.error('ERROR :: doSubmission :: Step 2 ' + error);
      callback(new Exception.GetTemplateException('Fail to get email'));
    });
  };

  //Step 3
  var prepareSubmissionTemplateValues = function prepareSubmissionTemplateValues(app, callback) {
    logger.log('INFO: appFormSubmission - (3) prepareSubmissionTemplateValues');
    getSubmissionTemplate(app, function (tmpl) {
      if (tmpl) {
        template = _.cloneDeep(tmpl);
        if (session.agent.channel.type == 'FA') {
          if (session.platform) {
            commonApp.frozenTemplateMobile(template);
          } else {
            commonApp.frozenTemplate(template);
          }
        }
        getSubmissionValues(app, session, 'SUCCESS', '', function (resValues) {
          if (resValues) {
            callback({
              template: template,
              values: resValues.values
            });
          } else {
            callback(new Exception.EmailException('Fail to get submission values to display'));
            //errorCB(resValues)
          }
        });
      } else {
        callback(new Exception.GetTemplateException('Fail to get template'));
        //errorCB(tmpl)
      }
    });
  };

  //Starting function call
  applicationDao.getApplication(docId, function (app) {
    var backupApp = _.cloneDeep(app);
    logger.log('INFO: appFormSubmission - getApplication', docId);
    if (app._id && !app.error) {
      var lang = 'en';
      try {
        if (agentProfile.role != '04' && !agentProfile.managerCode) {
          //No supervisor is found
          getSubmissionTemplate(app, function (tmpl) {
            if (tmpl) {
              getSubmissionValues(app, session, 'FAIL', 'Supervisor detail not available. Case cannot be submitted.', function (resValues) {
                if (resValues) {
                  logger.log('INFO: appFormSubmission - end [RETURN=-1]', docId);
                  cb({
                    success: false,
                    template: _.cloneDeep(tmpl),
                    values: resValues.values,
                    isMandDocsAllUploaded: true,
                    isSubmitted: false
                  });
                } else {
                  handleRollBackAppEapp(new Exception.EmailException('Fail to get submission values to display'), backupApp, session, errorCB);
                }
              });
            } else {
              handleRollBackAppEapp(new Exception.GetTemplateException('Fail to get template'), backupApp, session, errorCB);
            }
          });
        } else {
          genPdfBeforeSubmission(app, lang, function (res) {
            if (res && !res.error) {
              doSubmission(app, function (submitRes) {
                if (submitRes instanceof Error) {
                  handleRollBackAppEapp(submitRes, backupApp, session, errorCB);
                } else if (submitRes) {
                  // Case: Successfully go to Submission
                  prepareSubmissionTemplateValues(app, function (res) {
                    if (res instanceof Error) {
                      handleRollBackAppEapp(res, backupApp, session, errorCB);
                    } else {
                      bDao.getCurrentBundle(app.pCid).then(function (bundle) {
                        // bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.SUBMIT_APP, function(newBundle) {
                        bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.SUBMIT_APP).then(function (newBundle) {
                          logger.log('INFO: appFormSubmission - end [RETURN=1]', docId);
                          cb({
                            success: true,
                            template: res.template,
                            values: res.values,
                            isMandDocsAllUploaded: true,
                            isSubmitted: true
                          });
                        }).catch(function (error) {
                          logger.error('ERROR: appFormSubmission - end [RETURN=1] fails :', docId, error);
                          cb({ success: false });
                        });
                      });
                    }
                  });
                } else {
                  handleRollBackAppEapp(new Exception.UnknownException('Unknown Exception in doSubmission'), backupApp, session, errorCB);
                }
              });
            } else {
              handleRollBackAppEapp(new Exception.UnknownException('Fail to generate eApp Form payment page in doSubmission'), backupApp, session, errorCB);
            }
          });
        }
      } catch (e) {
        logger.error('ERROR: appFormSubmission - catch error:', e);
        handleRollBackAppEapp(new Exception.UnknownException('Unexpected error:' + e), backupApp, session, errorCB);
      }
    } else {
      handleRollBackAppEapp(new Exception.UnknownException('No application ID.'), backupApp, session, errorCB);
    }
  });
};
module.exports.appFormSubmission = appFormSubmission;
/**FUNCTION ENDS: appFormSubmission*/

var handleRollBackAppEapp = function handleRollBackAppEapp(exception, appDoc, session, callback) {
  logger.log('INFO: handleRollBackAppEapp', appDoc._id, exception.code);
  if (exception.code === Exception.ROLL_BACK) {
    var eApprovalDocId = _.get(appDoc, 'policyNumber');
    bDao.onApplyApplication(appDoc.pCid, appDoc._id).then(function (resp) {
      applicationDao.upsertApplication(appDoc._id, appDoc, function (res) {
        dao.getDoc(eApprovalDocId, function (exDoc) {
          dao.delDocWithRev(eApprovalDocId, exDoc._rev, session, function (resp) {
            callback(appDoc, exception.message);
          });
        });
      });
    });
  } else {
    callback(appDoc, exception.message);
  }
};

module.exports.getSubmissionTemplateValues = function (data, session, cb) {
  var docId = data.docId;
  var template = {};
  var values = {};

  logger.log('INFO: getSubmissionTemplateValues - start', docId);

  var errorCB = function errorCB(res) {
    logger.error('ERROR: getSubmissionTemplateValues - end [RETURN=-1]', _.get(res, 'error'));
    cb(false);
  };

  applicationDao.getApplication(docId, function (app) {
    if (app._id) {
      getSubmissionTemplate(app, function (tmpl) {
        if (tmpl) {
          getSubmissionValues(app, session, app.isSubmittedStatus ? 'SUCCESS' : '', '', function (resValues) {
            if (resValues) {
              values = _.cloneDeep(resValues.values);
              if (app.isSubmittedStatus) {
                logger.log('INFO: getSubmissionTemplateValues - getFrozenTemplate', docId);
                if (session.platform) {
                  commonApp.frozenTemplateMobile(tmpl);
                } else {
                  commonApp.frozenTemplate(tmpl);
                }
              }
              logger.log('INFO: getSubmissionTemplateValues - end', docId);
              cb({
                success: true,
                template: tmpl,
                values: values,
                isMandDocsAllUploaded: app.isMandDocsAllUploaded,
                isSubmitted: app.isSubmittedStatus
              });
            } else {
              errorCB(resValues);
            }
          });
        } else {
          errorCB(tmpl);
        }
      });

      if (!app.isMandDocsAllUploaded) {
        app.isFailSubmit = true;
        applicationDao.upsertApplication(app._id, app, function (res) {
          if (!res) {
            errorCB(res);
          }
        });
      }
    } else {
      errorCB(app);
    }
  });
};

//MOVE to ./application/common.js
//Function: for Signature, SignDoc Auth
var genSignDocAuth = function genSignDocAuth(docid, docts) {
  var secretKey = global.config.signdoc.secretKey;
  var input = docid + docts + secretKey;
  var iconv = new Iconv('UTF-8', 'ISO-8859-1');
  var buffer = iconv.convert(input);
  var hash = crypto.createHash('sha1');
  hash.update(buffer);
  var md = hash.digest();
  var auth = md.toString('base64');
  return auth;
};
module.exports.generateSignDocAuth = genSignDocAuth;

//Function: for Signature, prepare SignDoc parameter from web
var getSignatureStatusFromCb = function getSignatureStatusFromCb(data, session, cb) {
  if (session.platform) {
    defaultSignature.getSignatureInitMobile(data, session, cb);
  } else {
    defaultSignature.getSignatureInitUrl(data, session, cb);
  }
};
module.exports.getSignatureStatusFromCb = getSignatureStatusFromCb;

//Function: for Signature, return urls with new token for signed pdf and next signing pdf
module.exports.getUpdatedAttachmentUrl = function (data, session, cb) {
  if (session.platform) {
    defaultSignature.getSignatureUpdatedPdfString(data, session, cb);
  } else {
    defaultSignature.getSignatureUpdatedUrl(data, session, cb);
  }
};

//COMMON FUNCTION
//Function: for Signature, receive POST from SignDoc, with PDF base64 data
module.exports.getSignedPdfFromSignDocPost = function (sdDocid, pdfData, tiffData, cb) {
  var ids = commonApp.getIdFromSignDocId(sdDocid);
  var docId = ids.docId;
  var attId = ids.attId;

  logger.log('INFO: getSignedPdfFromSignDocPost - start', docId);

  //Test if the data is validated tiff
  //CommonFunctions.base64ToFile(tiffData, 'temp.tiff', function(res) {
  //});

  if (docId.startsWith('SA')) {
    //Shield application
    shieldSignature.saveSignedPdfFromSignDoc(docId, attId, pdfData, cb);
  } else {
    defaultSignature.saveSignedPdfFromSignDoc(docId, attId, pdfData, cb);
  }
};

module.exports.saveSignedPdf = function (data, session, cb) {
  var docId = data.docId,
      attId = data.attId,
      pdfData = data.pdfData;

  if (docId.startsWith('SA')) {
    //Shield application
    shieldSignature.saveSignedPdf(docId, attId, pdfData, cb);
  } else {
    defaultSignature.saveSignedPdf(docId, attId, pdfData, cb);
  }
};

module.exports.updateAppStep = function (data, session, cb) {
  var docId = data.docId;
  var stepperIndex = data.stepperIndex;

  logger.log('INFO: updateAppStep - start', docId);

  applicationDao.getApplication(docId, function (app) {
    if (app._id) {
      if (app.appStep == stepperIndex) {
        app.appStep = stepperIndex + 1;
        applicationDao.upsertApplication(app._id, app, function (resp) {
          if (resp && !resp.error) {
            logger.log('INFO: updateAppStep - end [RETURN=1]', docId);
            cb({ success: true, appStep: app.appStep });
          } else {
            logger.error('ERROR: updateAppStep - end [RETURN=-2]', docId);
            cb({ success: false, result: 'Fail to update Application' });
          }
        });
      } else {
        logger.log('INFO: updateAppStep - end [RETURN=2]', docId);
        cb({ success: true });
      }
    } else {
      logger.error('ERROR: updateAppStep - end [RETURN=-1]', _.get(app, 'error'));
      cb({ success: false, result: 'Fail to get Application' });
    }
  });
};

//TO-DO: fade out this function and MOVE to ./application/common.js
var mergePdfBase64ToAppFormPdf = function mergePdfBase64ToAppFormPdf(app, pdfStr, callback) {
  logger.log('INFO: mergePdfBase64ToAppFormPdf - start', app._id);
  //get eApp form and merge with premium payment
  applicationDao.getAppAttachment(app._id, appPdfName, function (appPdf) {
    if (appPdf.success) {
      if (pdfStr && pdfStr.length > 0) {
        logger.log('INFO: mergePdfBase64ToAppFormPdf - pdfString length', pdfStr.length, app._id);
        //merge Pdf
        CommonFunctions.mergePdfs([appPdf.data, pdfStr], function (mergedPdf) {
          if (mergedPdf) {
            logger.log('INFO: mergePdfBase64ToAppFormPdf - mergedPdf not null', app._id);
            dao.uploadAttachmentByBase64(app._id, appPdfName, app._rev, mergedPdf, 'application/pdf', function (res) {
              if (res && res.rev && !res.error) {
                app._rev = res.rev;
                logger.log('INFO: mergePdfBase64ToAppFormPdf - end [RETURN=1]', app._id);
                callback(app);
              } else {
                logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-4]', app._id);
                callback(false);
              }
            });
          } else {
            logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-3]', app._id);
            callback(false);
          }
        });
      } else {
        logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-2]', app._id);
        callback(app);
      }
    } else {
      logger.error('ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-1]', app._id);
      callback(false);
    }
  });
};

//COMMON FUNCTION
//batch 2 report
var genAppFormPdfByPaymentData = function genAppFormPdfByPaymentData(app, lang, callback) {
  logger.log('INFO: genAppFormPdfByPaymentData - start', app._id);
  var trigger = {};
  var paymentValues = _.cloneDeep(app.payment);
  var isShowDollarSign = _.get(app, 'applicationForm.values.appFormTemplate.properties.dollarSignForAllCcy', true);
  var ccy = isShowDollarSign ? null : _.get(app, 'quotation.ccy');

  dao.getDoc(commonApp.TEMPLATE_NAME.PAYMENT, function (paymentTmpl) {
    logger.log('INFO: genAppFormPdfByPaymentData - replaceAppFormValuesByTemplate start', app._id);
    commonApp.replaceAppFormValuesByTemplate(paymentTmpl, app.payment, paymentValues, trigger, [], lang, ccy);
    logger.log('INFO: genAppFormPdfByPaymentData - replaceAppFormValuesByTemplate end', app._id);

    var reportData = {
      root: {
        payment: paymentValues,
        originalData: app.payment,
        policyOptions: app.quotation.policyOptions
      }
    };

    var tplFiles = [];
    commonApp.getReportTemplates(tplFiles, ['appform_report_payment_main', 'appform_report_common', 'appform_report_premium_payment'], 0, function () {
      if (tplFiles.length) {
        PDFHandler.getPremiumPaymentPdf(reportData, tplFiles, lang, function (pdfStr) {
          //get eApp form and merge with premium payment
          mergePdfBase64ToAppFormPdf(app, pdfStr, function (app) {
            if (app && !app.error) {
              logger.log('INFO: genAppFormPdfByPaymentData - end [RETURN=1]', app._id);
              callback(app);
            } else {
              logger.error('ERROR: genAppFormPdfByPaymentData - end [RETURN=-2]', _.get(app, 'error'));
              callback(false);
            }
          });
        });
      } else {
        logger.error('ERROR: genAppFormPdfByPaymentData - end [RETURN=-1]', app._id);
        callback(false);
      }
    });
  });
};

//COMMON FUNCTION
//eCPD report
var getECpdPdfByAppFormData = function getECpdPdfByAppFormData(app, lang, callback) {
  var initPayMethod = _.get(app, 'payment.initPayMethod');
  logger.log('INFO: getECpdPdfByAppFormData - start', app._id);

  if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(initPayMethod) > -1) {
    var formValues = _.cloneDeep(app.applicationForm.values);
    formValues.receiptDate = CommonFunctions.getFormattedDate(app.payment.trxTime, 'dd/MMMM/yyyy');
    formValues.policyNumber = app.policyNumber || app._id;
    formValues.agent = app.quotation.agent;
    var reportData = {
      root: formValues
    };

    var tplFiles = [];
    commonApp.getReportTemplates(tplFiles, ['appform_report_ecpd_main'], 0, function () {
      if (tplFiles.length) {
        PDFHandler.getECpdPdf(reportData, tplFiles, lang, function (pdfStr) {
          dao.uploadAttachmentByBase64(app._id, eCpdPdfName, app._rev, pdfStr, 'application/pdf', function (res) {
            if (res && res.rev && !res.error) {
              app._rev = res.rev;
              logger.log('INFO: getECpdPdfByAppFormData - end [RETURN=1]', app._id);
              callback(app);
            } else {
              logger.error('ERROR: getECpdPdfByAppFormData - end [RETURN=-2]', app._id, tplFiles);
              callback(false);
            }
          });
        });
      } else {
        logger.error('ERROR: getECpdPdfByAppFormData - end [RETURN=-1]', app._id, tplFiles);
        callback(false);
      }
    });
  } else {
    logger.log('INFO: getECpdPdfByAppFormData - end [RETURN=2]', app._id);
    callback(app);
  }
};

var _updatePaymentMethods = function _updatePaymentMethods(data, session, cb) {
  var docId = data.docId;
  var values = data.values;
  var stepperIndex = data.stepperIndex;
  var now = Date.now();
  var updateStepper = data.updateStepper;

  logger.log('INFO: _updatePaymentMethods - start', docId);

  applicationDao.getApplication(docId, function (app) {
    if (app && !app.error && app._id) {
      // if (app.appStep == stepperIndex) {
      if (!app.hasOwnProperty('payment')) {
        app['payment'] = {};
      }
      if (!app.hasOwnProperty('isInitialPaymentCompleted')) {
        app['isInitialPaymentCompleted'] = false;
      }
      app.payment.initPayMethod = values.initPayMethod;
      app.payment.subseqPayMethod = values.subseqPayMethod;

      if (app.payment.trxNo && ['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(app.payment.initPayMethod) >= 0) {
        // updated from api already
      } else {
        app.payment.trxNo = "";
        app.payment.trxTime = 0;
        app.payment.trxStartTime = 0;
        app.payment.trxStatus = "";
        app.payment.trxRemark = "";
        app.payment.trxAmount = 0;
        app.payment.trxCcy = "";
        app.payment.trxPayType = "";
        app.payment.trxMethod = "";
        app.payment.ccNo = "";
        app.payment.trxStatusRemark = "";
      }

      for (var v in values) {
        if (["covCode", "paymentMode", "axsOnline", "samOnline"].indexOf(v) == -1) {
          if (["ttRemittingBank", "ttDOR", "ttRemarks"].indexOf(v) == -1) {
            app.payment[v] = values[v];
          } else {
            if (values.initPayMethod == 'teleTransfter') {
              if (v == 'ttDOR') {
                app.payment[v] = values.ttDOR ? new Date(values.ttDOR).toISOString() : "";
              } else {
                app.payment[v] = values[v];
              }
            }
          }
        }
      }

      // app.payment.initBasicPrem = values.initBasicPrem || 0;
      // app.payment.backdatingPrem = values.backdatingPrem || 0;
      // app.payment.initRspPrem = values.initRspPrem || 0;
      // app.payment.initTotalPrem = values.initTotalPrem || 0;
      // app.payment.topupPrem = values.topupPrem || 0;

      if (updateStepper && !data.isShield) {
        app.appStep = stepperIndex + 1;
      }
      logger.log('INFO: _updatePaymentMethods - end [RETURN=1]', docId);
      cb(app);
      // } else {
      //   cb(app);
      // }
    } else {
      logger.error('ERROR: _updatePaymentMethods - end [RETURN=-1]', docId, _.get(app, 'error'));
      cb(app);
    }
  });
};

var resetMandDocsStatusForDirector = function resetMandDocsStatusForDirector(app, session) {
  if (_.get(session, 'agent.agentCode') === _.get(session, 'agent.managerCode')) {
    var paymentMethod = _.get(app, 'payment.initPayMethod');
    var mandDocs = _.get(app, 'supportDocuments.values.policyForm.mandDocs');
    if (paymentMethod && mandDocs) {
      if (_.get(mandDocs, '[' + paymentMethod + ']') && _.isEmpty(mandDocs[paymentMethod])) {
        _.set(app, 'isMandDocsAllUploaded', false);
      }
    }
  }
};

module.exports.updatePaymentMethods = function (data, session, cb) {
  var updateStepper = data.updateStepper;
  var docId = data.docId;

  logger.log('INFO: updatePaymentMethods - start', docId);

  _updatePaymentMethods(data, session, function (app) {
    resetMandDocsStatusForDirector(app, session);
    if (app && !app.error && app._id) {
      applicationDao.upsertApplication(app._id, app, function (upApp) {
        if (upApp && !upApp.error) {
          var lang = session.lang || 'en';
          logger.log('INFO: updatePaymentMethods - end [RETURN=1]', docId);
          cb({ success: true, application: app });
        } else {
          logger.error('ERROR: updatePaymentMethods - end [RETURN=-2]', docId, _.get(upApp, 'error'));
          cb({ success: false, result: upApp });
        }
      });
    } else {
      logger.error('ERROR: updatePaymentMethods - end [RETURN=-1]', docId, _.get(app, 'error'));
      cb({ success: false, result: app });
    }
  });
};

module.exports.getAppSumEmailAttKeys = function (data, session, callback) {
  applicationDao.getAppSumEmailAttKeys(session, function (result) {
    if (result && !result.error) {
      callback({
        success: true,
        attKeysMap: result.items
      });
    } else {
      callback({
        success: false
      });
    }
  });
};

module.exports.getEmailTemplate = function (data, session, callback) {
  applicationDao.getEmailTemplate(function (result) {
    if (result && !result.error) {
      callback({
        success: true,
        clientSubject: result.clientSubject,
        clientContent: result.clientContent,
        agentSubject: result.agentSubject,
        agentContent: result.agentContent,
        embedImgs: result.embedImgs
      });
    } else {
      callback({
        success: false
      });
    }
  });
};

var sendApplicationEmail = function sendApplicationEmail(data, session, cb) {
  commonApp.sendApplicationEmail(data, session, cb);
};
module.exports.sendApplicationEmail = sendApplicationEmail;

module.exports.updateBIBackDateTrue = function (data, session, callback) {
  var appId = data.appId;

  var signatureData = { docId: appId };
  logger.log('INFO: updateBIBackDateTrue - start', appId);

  applicationDao.getApplication(appId, function (app) {
    if (app && !app.error) {
      _.set(app, 'quotation.isBackDate', 'Y');
      _.set(app, 'applicationForm.values.planDetails.isBackDate', 'Y');
      _.set(app, 'isBackDate', true);
      _.set(app, 'applicationForm.values.planDetails.riskCommenDate', _.get(app, 'quotation.riskCommenDate'));
      applicationDao.upsertApplication(app.id, app, function (result) {
        defaultApplication.genApplicationPDF(app, session, function (genAppCb) {
          getSignatureStatusFromCb(signatureData, session, function (signCb) {
            if (genAppCb.success) {
              //update view
              dao.updateViewIndex("main", "summaryApps");
              if (result) {
                logger.log('INFO: updateBIBackDateTrue - end [RETURN=1]', app.id);
                callback({
                  success: true,
                  isBackDate: 'Y',
                  changedValues: _.get(app, 'applicationForm.values'),
                  application: signCb.application.application,
                  attachments: signCb.application.attachments
                });
              } else {
                logger.error('ERROR: updateBIBackDateTrue - end [RETURN=-3]', appId);
                callback({ success: false });
              }
            } else {
              logger.error('ERROR: updateBIBackDateTrue - end [RETURN=-2]', appId);
              callback({ success: false });
            }
          });
        });
      });
    } else {
      logger.error('ERROR: updateBIBackDateTrue - end [RETURN=-1]', appId, _.get(app, 'error'));
      callback(app);
    }
  });
};

module.exports.invalidateApplication = function (data, session, callback) {
  logger.log('INFO: invalidateApplication in appHandler');
  applicationDao.getApplication(data.appId, function (app) {
    bDao.onInvalidateApplicationById(app.quotation.pCid, data.appId).then(function (resp) {
      callback(resp);
    });
  });
};

module.exports.applicationSubmit = function (data, session, callback) {
  defaultApplication.applicationSubmit(data, session, function (response) {
    callback(response);
  });
};

module.exports.genFNA = function (data, session, cb) {
  defaultApplication.genFNA(data, session, cb);
};