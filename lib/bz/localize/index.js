'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var langs = require('./language.json');
var fs = require('fs');
var logger = global.logger || console;

module.exports.init = function (data) {
  var lang = data.lang;
  var root = global.rootPath;
  var language = null;
  if (!lang) {
    language = langs.languages[0];
  } else {
    for (var l in langs.languages) {

      var lgg = langs.languages[l];
      if (lgg.id == lang.toLowerCase()) {
        language = lgg;
        break;
      }
    }
  }
  var langMap = null;
  if (language) {
    var lmStr = "";
    // var currPath = fs.realpathSync('.')
    // logger.log('init language: current path:', root, fs.realpathSync('.'), process.env , __dirname);

    var filePath = '';
    // if (global.MODE == 'PROD' || global.MODE == 'SIT') {
    filePath = root + '/localize/' + language.filename;
    // } else {
    //   filePath = __dirname + '/../../localize/' + language.filename;
    // }
    lmStr = fs.readFileSync(filePath, 'utf8');

    lmStr = "{" + lmStr.replace(/"\s*=\s*"/g, '\":\"').replace(/"\s*;\s*/g, '",') + "\"success\":true }";
    langMap = JSON.parse(lmStr);
  }
  var initData = _defineProperty({
    langs: langs,
    langMap: langMap }, 'langMap', langMap);

  return initData;
};