'use strict';

var logger = global.logger || console;

var nDao = require("./cbDao/needs");
var cDao = require("./cbDao/client");
var bDao = require("./cbDao/bundle");
var dao = require('./cbDaoFactory').create();
var EmailUtils = require('./utils/EmailUtils');
var DateUtils = require('../common/DateUtils');
var AppHandler = require("./handler/ApplicationHandler");
var PDFHandler = require('./PDFHandler');
var CommonFunctions = require('./CommonFunctions');
var companyDao = require('./cbDao/company');

var nwIds = ['assets', 'savAcc', 'fixDeposit', 'invest', 'cpfOa', 'cpfSa', 'cpfMs', 'srs', 'car', 'property', 'otherAsset', 'liabilities', 'mortLoan', 'motLoan', 'eduLoan', 'otherLiabilites', 'netWorth'];
var cfIds = ['mDisposeIncome', 'aDisposeIncome', 'netMIcncome', 'aBonus', 'totAExpense', 'totMExpense', 'totAIncome'];
var cf12Ids = ['mIncome', 'lessMCPF', 'householdExpenses', 'insurancePrem', 'otherExpense', 'regSav', 'personExpenses'];
var eikeys = ['aInsPrem', 'lifeInsProt', 'retirePol', 'ednowSavPol', 'eduFund', 'invLinkPol', 'disIncomeProt', 'ciPort', 'personAcc', 'caInsPrem', 'csInsPrem', 'mdInsPrem', 'fpInsPrem'];
var pks = ['aInsPrem', 'pAInsPrem', 'sAInsPrem', 'oAInsPrem'];
var hpckeys = ['provideHeadstart', 'typeofWard'];

var _ = require('lodash');

module.exports.savePDA = function (data, session, cb) {
  var cid = data.cid,
      pda = data.pda,
      tiInfo = data.tiInfo,
      confirm = data.confirm;
  var aid = session.agent.agentCode;

  delete pda.tiInfo;
  bDao.onSaveNeedItem(cid, pda, nDao.ITEM_ID.PDA, { tiInfo: tiInfo }).then(function (result) {
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      bDao.invalidNeedItem(cid, session.agent, pda, nDao.ITEM_ID.PDA, result.errObj).then(function (resp) {
        var newId = _.get(resp, nDao.ITEM_ID.PDA);
        if (newId) {
          pda.newId = newId;
        }
        nDao.savePDA(session.agent, cid, pda, tiInfo).then(function (result2) {
          var hasTi = pda.lastStepIndex > 1 && pda.trustedIndividual === "Y";
          bDao.updateApplicationTrustedIndividual(cid, hasTi, tiInfo).then(function () {
            // bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA, ()=>{
            bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA).then(function () {
              nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
                cb({
                  success: true,
                  pda: result2.pda,
                  fe: result2.fe,
                  fna: result2.fna,
                  profile: result2.profile,
                  showFnaInvalidFlag: showFnaInvalidFlag
                });
              });
            }).catch(function (error) {
              logger.error('ERROR: savePDA :', error);
              cb({ success: false });
            });
          });
        });
      });
    } else {
      cb({ success: true, code: result.code });
    }
  });
};

module.exports.getPDA = function (data, session, cb) {
  var cid = data.cid;
  nDao.getItem(cid, nDao.ITEM_ID.PDA).then(function (pda) {
    delete pda.tiInfo;
    cb({
      success: pda ? true : false,
      pda: pda
    });
  });
};

module.exports.saveFE = function (data, session, cb) {
  var cid = data.cid,
      fe = data.fe,
      confirm = data.confirm;
  var aid = session.agent.agentCode;

  bDao.onSaveNeedItem(cid, fe, nDao.ITEM_ID.FE).then(function (result) {
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      bDao.invalidNeedItem(cid, session.agent, fe, nDao.ITEM_ID.FE, result.errObj).then(function (resp) {
        var newId = _.get(resp, nDao.ITEM_ID.FE);
        if (newId) {
          fe.newId = newId;
        }
        nDao.saveFE(session.agent, cid, fe).then(function (result2) {
          // bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA, ()=>{
          bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA).then(function () {
            cDao.getProfile(cid).then(function (profile) {
              nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
                cb({
                  success: true,
                  fe: result2.fe,
                  fna: result2.fna,
                  showFnaInvalidFlag: showFnaInvalidFlag,
                  profile: profile
                });
              });
            });
          }).catch(function (error) {
            logger.error('ERROR: saveFE :', error);
            cb({ success: false });
          });
        });
      });
    } else {
      cb({ success: true, code: result.code });
    }
  });
};

module.exports.getFE = function (data, session, cb) {
  var cid = data.cid;
  nDao.getItem(cid, nDao.ITEM_ID.FE).then(function (fe) {
    cb({
      success: fe ? true : false,
      fe: fe
    });
  });
};

module.exports.saveFNA = function (data, session, cb) {
  var cid = data.cid,
      fna = data.fna,
      confirm = data.confirm;
  var aid = session.agent.agentCode;

  bDao.onSaveNeedItem(cid, fna, nDao.ITEM_ID.NA).then(function (result) {
    //remove warning message if not invalidate bundle
    if (result.code != bDao.CHECK_CODE.INVALID_BUNDLE && result.code != bDao.CHECK_CODE.INVALID_FNA_REPORT) {
      result.code = bDao.CHECK_CODE.VALID;
    }
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      bDao.invalidNeedItem(cid, session.agent, fna, nDao.ITEM_ID.NA).then(function (resp) {
        var newId = _.get(resp, nDao.ITEM_ID.NA);
        if (newId) {
          fna.newId = newId;
        }
        nDao.saveFNA(session.agent, cid, fna).then(function (result2) {
          // bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA, ()=>{
          bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA).then(function () {
            cDao.getProfile(cid).then(function (profile) {
              nDao.showFnaInvalidFlag(cid).then(function (showFnaInvalidFlag) {
                cb({
                  success: true,
                  fna: result2.fna,
                  showFnaInvalidFlag: showFnaInvalidFlag,
                  profile: profile
                });
              });
            });
          }).catch(function (error) {
            logger.error('ERROR: saveFNA :', error);
            cb({ success: false });
          });
        }).catch(function (e) {
          logger.error(e);
        });
      });
    } else {
      cb({ success: true, code: result.code });
    }
  });
};

module.exports.getFNA = function (data, session, cb) {
  var cid = data.cid;
  nDao.getItem(cid, nDao.ITEM_ID.NA).then(function (fna) {
    cb({
      success: fna ? true : false,
      fna: fna
    });
  });
};

var _getNeedForm = function _getNeedForm() {
  return new Promise(function (resolve) {
    var promises = [];
    promises.push(nDao.getCNAForm());
    promises.push(nDao.getPdaForm());
    promises.push(nDao.getFeForm());
    promises.push(nDao.getFnaForm());
    Promise.all(promises).then(function (args) {
      resolve({
        cnaForm: args[0],
        pdaForm: args[1],
        feForm: args[2],
        fnaForm: args[3]
      });
    });
  });
};

module.exports.getNeedForm = function (data, session, cb) {
  _getNeedForm().then(function (result) {
    cb(result);
  });
};
var _initNeeds = function _initNeeds(data, session, cid) {
  return new Promise(function (resolve) {
    var promises = [];
    promises.push(nDao.getItem(cid, nDao.ITEM_ID.PDA));
    promises.push(nDao.getItem(cid, nDao.ITEM_ID.FE));
    promises.push(nDao.getItem(cid, nDao.ITEM_ID.NA));
    promises.push(new Promise(function (resolve3) {
      nDao.getNeedsSummary(resolve3);
    }));
    promises.push(cDao.getAllDependantsProfile(cid));
    promises.push(nDao.showFnaInvalidFlag(cid));

    Promise.all(promises).then(function (args) {
      resolve({
        success: true,
        pda: args[0],
        fe: args[1],
        fna: args[2],
        needsSummary: args[3],
        dependantProfiles: args[4],
        showFnaInvalidFlag: args[5]
      });
    });
  });
};

module.exports.initNeeds = function (data, session, cb) {
  _initNeeds(data, session, data.cid).then(function (result) {
    cb(result);
  });
};

var handleProfileData = function handleProfileData(profile) {
  profile.name = profile.fullName;
  profile.mobile = profile.mobileNo ? profile.mobileCountryCode + " " + profile.mobileNo : '';
  profile.othMobile = profile.otherNo ? profile.otherMobileCountryCode + " " + profile.otherNo : '';
  profile.idDocType = profile.idDocType === 'nric' ? 'NRIC' : profile.idDocType === 'bcNo' ? "Birth Certificate No." : profile.idDocType === 'passport' ? 'Passport' : profile.idDocType === 'fin' ? 'FIN No.' : profile.idDocType === 'other' ? profile.idDocType + " - " + profile.idDocTypeOther : '';
  var dob = profile.dob ? new Date(profile.dob) : null;
  if (dob) {
    profile.dob = {
      day: dob.getDate(),
      month: dob.getMonth() + 1,
      year: dob.getFullYear()
    };
  }
  if (profile.occupation === 'O921' && !!profile.occupationOther) {
    profile.occupation = getTitle('occupation', profile.occupation) + '-' + profile.occupationOther;
  } else {
    profile.occupation = getTitle('occupation', profile.occupation);
  }
  profile.nationality = getTitle('nationality', profile.occupation);
};

var getTitle = function getTitle(map, value) {
  return _.get(_.find(_.get(global.optionsMap, map + ".options"), function (n) {
    return n.value === value;
  }), 'title.en');
};

var handleCka = function handleCka(cka) {
  if (cka.course == "notApplicable") {
    cka.institution = "";
    cka.studyPeriodEndYear = "";
  }
  if (cka.collectiveInvestment != "Y") {
    cka.transactionType = "";
  }
  if (cka.insuranceInvestment != "Y") {
    cka.insuranceType = "";
  }
  if (!cka.profession == "notApplicable") {
    cka.years = null;
  }

  var course = cka.course,
      collectiveInvestment = cka.collectiveInvestment,
      insuranceInvestment = cka.insuranceInvestment,
      profession = cka.profession;


  cka.isValid = course && collectiveInvestment && insuranceInvestment && profession && (course != 'notApplicable' ? 1 : 0) + (collectiveInvestment == 'Y' ? 1 : 0) + (insuranceInvestment == 'Y' ? 1 : 0) + (profession != 'notApplicable' ? 1 : 0) >= 1 ? "Y" : "N";
};

var handleDate = function handleDate(_d) {
  if (!_d) return "";

  var d = new Date(_d);
  var addZero = function addZero(value) {
    return (value.toString().length == 1 ? "0" : "") + value.toString();
  };
  return addZero(d.getDate()) + "/" + addZero(d.getMonth() + 1) + "/" + d.getFullYear();
};

var replaceNewLine = function replaceNewLine() {
  var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  return str.split('\n').join('[[@newLine]]');
};

var genFnaReportData = function genFnaReportData(session, agent, profile, dependantProfiles, pda, fe, fna, skipEmailInReport, template, callback) {

  bDao.getCurrentBundle(profile.cid).then(function (_bundle) {
    try {
      var now = new Date();
      var printDate = _bundle.firstBiCreateTime;
      if (!printDate) {
        printDate = pda.lastUpd;
        if (printDate < fe.lastUpd) {
          printDate = fe.lastUpd;
        }
        if (printDate < fna.lastUpd) {
          printDate = fna.lastUpd;
        }
      }
      printDate = new Date(printDate);
      var printDateStr = printDate.getDate() + "/" + (printDate.getMonth() + 1) + "/" + printDate.getFullYear();
      var hasSpouse = pda.applicant === 'joint' ? true : false;
      var authorised = _.map(_.split(agent.authorisedDes, ","), _.trim);
      var hasILP = _.find(["Investment Linked Plan"], function (_id) {
        return authorised.indexOf(_id) > -1;
      }) ? "Y" : "N";
      var hasLI = _.find(["Life Insurance"], function (_id) {
        return authorised.indexOf(_id) > -1;
      }) ? "Y" : "N";
      var hasHI = _.find(["Health Insurance"], function (_id) {
        return authorised.indexOf(_id) > -1;
      }) ? "Y" : "N";
      var hasUT = _.find(["Unit Trusts"], function (_id) {
        return authorised.indexOf(_id) > -1;
      }) ? "Y" : "N";

      if (skipEmailInReport) {
        profile.email = '';
      }

      var reportData = {
        root: {
          printDate: printDateStr,
          owner: profile,
          hasSpouse: hasSpouse ? "Y" : "N",
          agent: {
            name: agent.name,
            code: agent.agentCode,
            mobile: agent.mobile,
            hasILP: hasILP,
            hasLI: hasLI,
            hasHI: hasHI,
            hasUT: hasUT
          },
          personalData: {
            applicant: pda.applicant,
            ownerConsentMethod: pda.ownerConsentMethod,
            spouseConsentMethod: pda.spouseConsentMethod,
            dependants: []
          }
        }
      },
          root = reportData.root;
      handleProfileData(root.owner);

      var spouse = _.find(profile.dependants, function (d) {
        return d.relationship === 'SPO';
      });
      var spouseProfile = void 0;
      if (spouse) {
        spouseProfile = _.cloneDeep(dependantProfiles[spouse.cid]);
        if (hasSpouse) {
          root.spouse = _.cloneDeep(dependantProfiles[spouse.cid]);
          handleProfileData(root.spouse);
        }
      }

      //handle personal data acknowledgement
      var childs = [],
          sd = [];
      if (pda.dependants) {
        root.hasDependants = "Y";
        sd = pda.dependants.split(",");
        _.forEach(sd, function (d) {
          var dependant = _.cloneDeep(dependantProfiles[d]) || {};
          //add into childs for later validation
          if (['SON', 'DAU'].indexOf(dependant.relationship) > -1) {
            childs.push(dependant.cid);
          }
          var targetSupYear = dependant.relationship === 'SON' ? 25 : dependant.relationship === 'DAU' ? 22 : dependant.gender === 'M' ? 80 + 8 : 85 + 8;
          var dob = new Date(dependant.dob);

          var dAge = DateUtils.getNearestAge(printDate, dob);
          var supYrs = targetSupYear - dAge;
          if (1 >= dAge) {
            var pmon = printDate.getMonth(),
                dmon = dob.getMonth();
            if (printDate.getFullYear() > dob.getFullYear()) {
              pmon += 12;
            }
            if (pmon - dmon <= 6) {
              if (pmon - dmon === 6) {
                if (printDate.getDate() >= dob.getDate()) {
                  dAge = 1;
                } else {
                  dAge = "<1";
                }
              } else {
                dAge = "<1";
              }
            } else {
              dAge = 1;
            }
          }
          var dobStr = dob.getDate() + "/" + (dob.getMonth() + 1) + "/" + dob.getFullYear();

          var relationshipOther = '';
          var relationship = getRelationship(dependant.relationship);
          if (dependant.relationship === 'OTH') {
            var relationshipDependant = _.find(profile.dependants, function (d) {
              return d.cid === dependant.cid;
            });
            if (relationshipDependant && relationshipDependant.relationshipOther) {
              relationshipOther = relationshipDependant.relationshipOther;
              relationship += '-' + relationshipOther;
            }
          }
          root.personalData.dependants.push({
            name: dependant.fullName,
            dob: dobStr,
            age: dAge,
            supYrs: supYrs > 0 ? supYrs : 0,
            relationship: relationship
          });
        });
      }
      for (var aa = 3 - _.size(root.personalData.dependants); aa > 0; aa--) {
        root.personalData.dependants.push({});
      }

      //handle prority
      var priorityValid = true;
      var _asts = _.split(fna.aspects, ",");
      var _fna$sfAspects = fna.sfAspects,
          sfAspects = _fna$sfAspects === undefined ? [] : _fna$sfAspects,
          _fna$spAspects = fna.spAspects,
          spAspects = _fna$spAspects === undefined ? [] : _fna$spAspects;

      var priority = { otherSav: [], otherPro: [], psGoals: [] };
      var pArr = _.concat(sfAspects, spAspects);
      //insert index
      _.forEach(pArr, function (pObj, pIdx) {
        pObj.pIdx = pIdx + 1;
      });
      var _pArr = _.chain(pArr).groupBy("aid").toPairs().map(function (currentItem) {
        return _.zipObject(["aid", "aspects"], currentItem);
      }).value();
      _.forEach(_pArr, function (pObj) {
        if (!_.get(fna, pObj.aid + ".isValid")) {
          priorityValid = false;
        } else if (priorityValid) {
          if ('psGoals' === pObj.aid) {
            var _aArr = _.chain(pObj.aspects).groupBy("title").toPairs().map(function (currentItem) {
              return _.zipObject(["title", "aspects"], currentItem);
            }).value();
            _.forEach(_aArr, function (_aObj) {
              var a = '',
                  ai = 1;
              _.forEach(_aObj.aspects, function (_a) {
                var an = _a.cid === profile.cid ? 'Myself' : hasSpouse && _a.cid === _.get(spouseProfile, 'cid') ? 'Spouse' : _.get(dependantProfiles[_a.cid], 'fullName');
                a += (a ? ", " : "") + (_a.pIdx + " - " + an);
              });
              priority.psGoals.push({
                title: _aObj.title,
                value: a,
                level: _.findIndex(pArr, function (_pObj) {})
              });
            });
          } else if ('other' === pObj.aid) {
            var _aArr2 = _.chain(pObj.aspects).groupBy("type").toPairs().map(function (currentItem) {
              return _.zipObject(["type", "aspects"], currentItem);
            }).value();

            _.forEach(_aArr2, function (_aObj) {
              var _tArr = _.chain(_aObj.aspects).groupBy("title").toPairs().map(function (currentItem) {
                return _.zipObject(["title", "aspects"], currentItem);
              }).value();

              _.forEach(_tArr, function (_tObj) {
                var a = '',
                    ai = 1;
                _.forEach(_tObj.aspects, function (_t) {
                  var an = _t.cid === profile.cid ? 'Myself' : hasSpouse && _t.cid === spouseProfile.cid ? 'Spouse' : _.at(dependantProfiles[_t.cid], 'fullName')[0];
                  a += (a ? ", " : "") + (_t.pIdx + " - " + an);
                });
                if (_aObj.type === "SAVINGS") {
                  priority.otherSav.push({
                    title: _tObj.title,
                    value: a
                  });
                } else {
                  priority.otherPro.push({
                    title: _tObj.title,
                    value: a
                  });
                }
              });
            });
          } else if ('pcHeadstart' === pObj.aid) {
            _.forEach(['protection', 'critical'], function (aspectType) {
              var aspects = _.filter(pObj.aspects, function (aspect) {
                return _.toLower(aspect.title).indexOf(aspectType) > -1;
              });
              var a = '',
                  ai = 1;
              _.forEach(aspects, function (aspect) {
                var cids = _.split(aspect.cid, ',');
                _.forEach(cids, function (cid, index) {
                  cids[index] = cids[index] === profile.cid ? 'Myself' : hasSpouse && cids[index] === spouseProfile.cid ? 'Spouse' : _.get(dependantProfiles[cids[index]], 'fullName');
                });
                var _cid = cids.join(", ");
                a += (a ? ', ' : '') + (aspect.pIdx + " - " + _cid);
              });
              priority[(aspectType === 'protection' ? 'P' : 'C') + pObj.aid] = a;
            });
          } else {
            var a = '',
                ai = 1;
            _.forEach(pObj.aspects, function (_a) {
              var cids = _.split(_a.cid, ",");
              for (var i in cids) {
                cids[i] = cids[i] === profile.cid ? 'Myself' : hasSpouse && cids[i] === spouseProfile.cid ? 'Spouse' : _.get(dependantProfiles[cids[i]], 'fullName');
              }
              var _cid = cids.join(", ");
              a += (a ? ", " : "") + (_a.pIdx + " - " + _cid);
            });
            priority[pObj.aid] = a;
          }
        }
      });

      if (!_.size(priority.psGoals)) {
        priority.psGoals.push({});
      }
      if (!_.size(priority.otherSav)) {
        priority.otherSav.push({});
      }
      if (!_.size(priority.otherPro)) {
        priority.otherPro.push({});
      }

      if (priorityValid) {
        root.priority = priority;
      } else {
        root.priority = { otherSav: [{}], otherPro: [{}], psGoals: [{}] };
      }

      //handle networth
      var cnws = _.at(fe, ['owner.assets', 'owner.liabilities', 'owner.noALReason', 'owner.otherLiabilites', 'owner.otherLiabilitesTitle', 'spouse.otherLiabilites', 'spouse.otherLiabilitesTitle', 'owner.init', 'spouse.init', 'owner.otherAsset', 'owner.otherAssetTitle', 'owner.otherAsset', 'owner.otherAssetTitle']);
      var hasNoAlReason = !cnws[0] && !cnws[1];
      if (!(hasNoAlReason && !cnws[2] || cnws[3] && !cnws[4] || !cnws[3] && cnws[4] || cnws[9] && !cnws[10] || !cnws[9] && cnws[10] || cnws[7] || hasSpouse && (cnws[5] && !cnws[6] || !cnws[5] && cnws[6] || cnws[11] && !cnws[12] || !cnws[12] && cnws[11] || cnws[8]))) {
        var netWorth = root.netWorth = { owner: {}, spouse: {}, valid: 'Y' };
        if (hasNoAlReason) {
          netWorth.noALReason = cnws[2];
        }

        if (hasSpouse) {
          _handleCurrency(netWorth.spouse, fe.spouse, nwIds, 0, true);
          netWorth.spouse.otherLiabilitesTitle = fe.spouse.otherLiabilitesTitle;
          netWorth.spouse.otherAssetTitle = fe.spouse.otherAssetTitle;

          if (fe.spouse.assets && fe.spouse.liabilities) {
            if (fe.spouse.assets > 0 && fe.spouse.liabilities > 0) {
              _handleCurrency(netWorth.spouse, fe.spouse, ['netWorth'], 0, false);
            }
          }
        }
        _handleCurrency(netWorth.owner, fe.owner, nwIds, 0, true);
        netWorth.owner.otherLiabilitesTitle = fe.owner.otherLiabilitesTitle;
        netWorth.owner.otherAssetTitle = fe.owner.otherAssetTitle;

        if (fe.owner.assets && fe.owner.liabilities) {
          if (fe.owner.assets > 0 && fe.owner.liabilities > 0) {
            _handleCurrency(netWorth.owner, fe.owner, ['netWorth'], 0, false);
          }
        }
      }

      //handle cash flow
      //check valid
      var valid = true;
      var incomeIds = ['mIncome', 'aBonus', 'netMIcncome'];
      var expenseIds = ['personExpenses', 'householdExpenses', 'insurancePrem', 'regSav', 'otherExpense'];
      var hasNoCFReason =
      // no income provided
      _.findIndex(incomeIds, function (_s) {
        return Number(_.get(fe.owner, _s, 0));
      }) === -1 ||
      // no exspense provided
      _.findIndex(expenseIds, function (_s) {
        return Number(_.get(fe.owner, _s, 0));
      }) === -1;

      if (
      // input Other expense without other expense title
      _.get(fe, 'owner.otherExpense') && !_.get(fe, 'owner.otherExpenseTitle') || _.get(fe, 'spouse.otherExpense') && !_.get(fe, 'spouse.otherExpenseTitle') ||
      // input Other expense title without other expense
      !_.get(fe, 'owner.otherExpense') && _.get(fe, 'owner.otherExpenseTitle') || !_.get(fe, 'spouse.otherExpense') && _.get(fe, 'spouse.otherExpenseTitle') ||
      // no visit before
      _.get(fe, 'owner.init') || hasSpouse && _.get(fe, 'spouse.init') ||
      // without income / expense and reason
      hasNoCFReason && !_.get(fe, 'owner.noCFReason')) {
        valid = false;
      }

      if (valid) {
        var cashFlow = root.cashFlow = { owner: {}, spouse: {}, valid: 'Y' };
        // check is it no input in UI
        if (hasNoCFReason) {
          root.cashFlow.noCFReason = _.get(fe, 'owner.noCFReason', '');
        }

        root.cashFlow.additionComment = _.get(fe, 'owner.additionComment', '');

        if (hasSpouse) {
          _handleCurrency(cashFlow.spouse, fe.spouse, cfIds, 0, true);
          cashFlow.spouse.otherExpenseTitle = fe.spouse.otherExpenseTitle;
        }
        _handleCurrency(cashFlow.owner, fe.owner, cfIds, 0, true);
        cashFlow.owner.otherExpenseTitle = fe.owner.otherExpenseTitle;

        _.forEach(cf12Ids, function (_id) {
          if (hasSpouse) {
            cashFlow.spouse[_id] = getCurrency(Number(_.get(fe, "spouse." + _id)) * 12, 0, true);
          }
          cashFlow.owner[_id] = getCurrency(Number(_.get(fe, "owner." + _id)) * 12, 0, true);
        });

        // show zero income in FNA report if user input 0 for income field in client profile
        if (hasSpouse) {
          if (fe.spouse && fe.spouse.mIncome === 0) {
            _handleCurrency(cashFlow.spouse, fe.spouse, ['mIncome', 'totAIncome'], 0, false);
          }
        }
        if (fe.owner && fe.owner.mIncome === 0) {
          _handleCurrency(cashFlow.owner, fe.owner, ['mIncome', 'totAIncome'], 0, false);
        }
      }

      //handle existing insurance portfolio
      var checkEiPorf = function checkEiPorf() {
        var values = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var isOwner = arguments[1];
        var eiPorf = values.eiPorf,
            hospNSurg = values.hospNSurg,
            aInsPrem = values.aInsPrem,
            sInsPrem = values.sInsPrem,
            noEIReason = values.noEIReason;

        return isOwner && !eiPorf && !hospNSurg && !aInsPrem && !sInsPrem && !noEIReason || // no existing insurance Porfolio, annual insurance Premium and reason
        (values.eiPorf || values.hospNSurg) && !values.aInsPrem && !values.sInsPrem || // has existing insurance Porfolio but no annual insurance Premium
        !values.eiPorf && !values.hospNSurg && (values.aInsPrem || values.sInsPrem) || // has annual insurance premium but no existing insurance porfolio
        values.init ? false : true;
      };

      var eiValid = true;
      if (!checkEiPorf(fe.owner, true) || hasSpouse && !checkEiPorf(fe.spouse)) {
        eiValid = false;
      }
      _.forEach(fe.dependants, function (d) {
        if (childs.indexOf(d.cid) > -1 && !checkEiPorf(d)) {
          eiValid = false;
        }
      });

      if (eiValid) {
        var eo = { name: 'Myself' };
        var es = { name: 'Spouse', notShown: hasSpouse ? 'N' : 'Y' };
        _handleCurrency(eo, fe.owner, eikeys, 0, true);
        _handleString(eo, fe.owner, ['hospNSurg']);
        if (hasSpouse) {
          _handleCurrency(es, fe.spouse, eikeys, 0, true);
          _handleString(es, fe.spouse, ['hospNSurg']);
        }

        var oPayer = '';
        var oPayers = _.at(fe, ["owner." + pks[0], "owner." + pks[1], "owner." + pks[2], "owner." + pks[3]]);
        if (oPayers[1]) oPayer = 'Proposer';
        if (profile.marital === 'M' && oPayers[2]) oPayer += (oPayer ? ", " : "") + 'Spouse';
        if (oPayers[3]) oPayer += (oPayer ? ", " : "") + 'Others';
        eo.payer = oPayer;

        if (hasSpouse) {
          var sPayer = '';
          var sPayers = _.at(fe, ["spouse." + pks[1], "spouse." + pks[2], "spouse." + pks[3]]);
          if (sPayers[0]) sPayer = 'Proposer';
          if (sPayers[1]) sPayer += (sPayer ? ", " : "") + 'Spouse';
          if (sPayers[2]) sPayer += (sPayer ? ", " : "") + 'Others';
          es.payer = sPayer;
        }

        var eiPorf = root.eiPorf = { person: [{ items: [eo, es] }] };
        if (!_.get(fe, "owner.eiPorf") && !_.get(fe, "owner.hospNSurg")) {
          eiPorf.noEIReason = _.get(fe, "owner.noEIReason");
        }
        _.forEach(childs, function (c, cidx) {
          var dependant = dependantProfiles[c];
          var d = _.find(fe.dependants, function (_d) {
            return _d.cid === c;
          });
          var dv = { name: dependant.fullName };
          _handleCurrency(dv, d, eikeys, 0, true);
          _handleString(dv, d, ['hospNSurg']);

          var otPayer = '';
          var otPayers = _.at(d, [pks[1], pks[2], pks[3]]);
          if (otPayers[0]) otPayer = 'Proposer';
          if (otPayers[1]) otPayer += (otPayer ? ", " : "") + 'Spouse';
          if (otPayers[2]) otPayer += (otPayer ? ", " : "") + 'Others';
          dv.payer = otPayer;
          if (eiPorf.person[eiPorf.person.length - 1].items.length == 3) {
            eiPorf.person.push({ items: [] });
          }
          eiPorf.person[eiPorf.person.length - 1].items.push(dv);
        });
        if (eiPorf.person[0].items.length === 2) {
          eiPorf.person[0].items.push({ name: 'Child', notShown: "Y" });
        }
      } else {
        root.eiPorf = { person: [{ items: [{ name: 'Myself' }, { name: 'Spouse', notShown: hasSpouse ? 'N' : 'Y' }, { name: 'Child', notShown: 'Y' }] }] };
      }

      var aspects = _.split(_.get(fna, "aspects"), ",");

      //retire planning
      root.rPlanning = [{ name: 'Myself', isActive: false }, { name: 'Spouse', isActive: false }];
      var rpac = 0;
      if (_.get(fna, 'rPlanning.isValid') && aspects.indexOf("rPlanning") > -1) {
        if (_.get(fna, 'rPlanning.owner.isActive')) {
          root.rPlanning[0] = handleRp(root, fna.rPlanning.owner, 'Myself', profile, template);
        }
        if (hasSpouse && spouseProfile && _.get(fna, 'rPlanning.spouse.isActive')) {
          root.rPlanning[1] = handleRp(root, fna.rPlanning.spouse, 'Spouse', spouseProfile, template);
        }
        _.forEach(root.rPlanning, function (_rp) {
          rpac += _.size(_rp.assets);
        });
      }

      if (rpac === 0 && root.rPlanning[0]) {
        root.rPlanning[0].assets = [{}];
      }

      //income protection
      root.fiProtection = [{ name: 'Myself' }, { name: 'Spouse' }];
      if (_.get(fna, 'fiProtection.isValid') && aspects.indexOf("fiProtection") > -1) {
        if (_.get(fna, 'fiProtection.owner.isActive')) {
          root.fiProtection[0] = handleFip(root, fna.fiProtection.owner, 'Myself', profile, template);
        }
        if (hasSpouse && spouseProfile && _.get(fna, 'fiProtection.spouse.isActive')) {
          root.fiProtection[1] = handleFip(root, fna.fiProtection.spouse, 'Spouse', spouseProfile, template);
        }
      }

      //critical illness protection
      var ccip = _.at(fna, ['ciProtection.isValid', 'ciProtection.owner.isActive', 'ciProtection.spouse.isActive']);
      var ciProtection = root.ciProtection = [];
      if (ccip[0] && aspects.indexOf("ciProtection") > -1) {
        if (ccip[1]) {
          addCiData(ciProtection, fna.ciProtection.owner, 'Myself');
        } else {
          ciProtection[0] = { items: [{ name: 'Myself' }] };
        }

        if (hasSpouse && ccip[2]) {
          addCiData(ciProtection, fna.ciProtection.spouse, 'Spouse');
        } else {
          ciProtection[0].items.push({ name: 'Spouse' });
        }

        if (ciProtection.length == 1 && ciProtection[0].items.length == 2) {
          ciProtection[0].items.push({ name: 'Child' });
        }
      }
      if (!_.size(ciProtection)) {
        ciProtection[0] = { items: [{ name: 'Myself' }, { name: 'Spouse' }, { name: 'Child' }] };
      }

      root.iarRateNotMatchReason = fe.isCompleted && fna.lastStepIndex > 0 && (fna.iarRate < 0 || fna.iarRate > 6) ? fna.iarRateNotMatchReason : '';

      //Disability income protection
      var cdip = _.at(fna, ['diProtection.isValid', 'diProtection.owner.isActive', 'diProtection.spouse.isActive']);
      var diProtection = root.diProtection = [];
      if (cdip[0] && aspects.indexOf("diProtection") > -1) {
        if (cdip[1]) {
          addDiData(diProtection, fna.diProtection.owner, 'Myself');
        } else {
          diProtection[0] = { items: [{ name: 'Myself' }] };
        }

        if (hasSpouse && cdip[2]) {
          addDiData(diProtection, fna.diProtection.spouse, 'Spouse');
        } else {
          diProtection[0].items.push({ name: 'Spouse' });
        }

        _.forEach(childs, function (ch) {
          var didp = _.find(fna.diProtection.dependants, function (fd) {
            return fd.cid === ch;
          });
          if (didp && didp.isActive) {
            addDiData(diProtection, didp, dependantProfiles[ch].fullName);
          }
        });
        if (_.size(diProtection[0].items) == 2) {
          diProtection[0].items.push({ name: 'Child' });
        }
      }

      if (!_.size(root.diProtection)) {
        root.diProtection[0] = { items: [{ name: 'Myself' }, { name: 'Spouse' }, { name: 'Child' }] };
      }

      //Personal accident protection
      var cpaip = _.at(fna, ['paProtection.isValid', 'paProtection.owner.isActive', 'paProtection.spouse.isActive']);
      var paProtection = root.paProtection = [];
      if (cpaip[0] && aspects.indexOf("paProtection") > -1) {
        if (cpaip[1]) {
          addPaData(paProtection, fna.paProtection.owner, 'Myself');
        } else {
          paProtection[0] = { items: [{ name: 'Myself' }] };
        }

        if (hasSpouse && cpaip[2]) {
          addPaData(paProtection, fna.paProtection.spouse, 'Spouse');
        } else {
          paProtection[0].items.push({ name: 'Spouse' });
        }

        _.forEach(childs, function (ch) {
          var cadp = _.find(fna.paProtection.dependants, function (fd) {
            return fd.cid === ch;
          });
          if (cadp && cadp.isActive) {
            addPaData(paProtection, cadp, dependantProfiles[ch].fullName);
          }
        });
        if (_.size(paProtection[0].items) == 2) {
          paProtection[0].items.push({ name: 'Child' });
        }
      }
      if (!_.size(paProtection)) {
        paProtection[0] = { items: [{ name: 'Myself' }, { name: 'Spouse' }, { name: 'Child' }] };
      }

      //Planning for Children's Headstart
      var pcHeadstart = root.pcHeadstart = [];
      if (_.get(fna, 'pcHeadstart.isValid') && aspects.indexOf("pcHeadstart") > -1) {
        _.forEach(childs, function (ch) {
          var pchdp = _.find(fna.pcHeadstart.dependants, function (fd) {
            return fd.cid === ch;
          });
          if (pchdp && pchdp.isActive) {
            addPchData(pcHeadstart, pchdp, dependantProfiles[ch].fullName);
          }
        });
      }
      if (!_.size(pcHeadstart)) {
        pcHeadstart[0] = {};
      }

      //education planning
      var ePlanning = root.ePlanning = [];
      if (_.get(fna, 'ePlanning.isValid') && aspects.indexOf("ePlanning") > -1) {
        _.forEach(childs, function (ch) {
          var ep = _.find(fna.ePlanning.dependants, function (epd) {
            return epd.cid === ch;
          });
          if (ep && ep.isActive) {
            var dependant = dependantProfiles[ch];
            addEpData(ePlanning, ep, dependant.fullName, template);
          }
        });
      }
      if (!_.size(root.ePlanning)) {
        root.ePlanning[0] = {};
      }
      _.forEach(root.ePlanning, function (epi) {
        if (!_.size(epi.assets)) {
          epi.assets = [{}];
        }
      });

      var hcProtection = root.hcProtection = [];
      var chcp = _.at(fna, ['hcProtection.isValid', 'hcProtection.owner.isActive', 'hcProtection.spouse.isActive']);
      if (chcp[0] && aspects.indexOf("hcProtection") > -1) {
        if (chcp[1]) {
          addHcpData(hcProtection, fna.hcProtection.owner, 'Myself');
        } else {
          hcProtection[0] = { items: [{ name: 'Myself' }] };
        }

        var addHcSpuseData = false;
        if (hasSpouse && chcp[2]) {
          addHcpData(hcProtection, fna.hcProtection.spouse, 'Spouse');
        } else if (spouse && spouse.cid) {
          // add spouse data if spouse is dependant, not myself and spouse
          var hcpSData = _.find(fna.hcProtection.dependants, function (hcpd) {
            return _.find(sd, function (d) {
              return d === hcpd.cid;
            }) && hcpd.cid === spouse.cid && hcpd.isActive;
          });
          if (hcpSData) {
            addHcSpuseData = true;
            addHcpData(hcProtection, hcpSData, 'Spouse');
          } else {
            hcProtection[0].items.push({ name: 'Spouse' });
          }
        }

        _.forEach(sd, function (d) {
          var hcpe = _.find(fna.hcProtection.dependants, function (hcpd) {
            return hcpd.cid === d;
          });
          var hcpDep = dependantProfiles[d];
          if (hcpe && hcpe.isActive) {
            if (addHcSpuseData && spouse && hcpe.cid === spouse.cid) {
              //don't add data if spouse data add before
            } else {
              addHcpData(hcProtection, hcpe, _.get(hcpDep, 'fullName'));
            }
          }
        });
        if (_.size(hcProtection[0].items) === 2) {
          hcProtection[0].items.push({ name: 'Child' });
        }
      }
      if (!_.size(root.hcProtection)) {
        root.hcProtection[0] = { items: [{ name: 'Myself' }, { name: 'Spouse' }, { name: 'Child' }] };
      }

      var psGoals = root.psGoals = [];
      if (_.get(fna, 'psGoals.isValid') && aspects.indexOf("psGoals") > -1) {
        addPsgData(psGoals, fna.psGoals.owner, 'Myself', template);
        if (!psGoals[0]) {
          psGoals[0] = { name: 'Myself' };
        }
        if (hasSpouse) {
          addPsgData(psGoals, fna.psGoals.spouse, 'Spouse', template);
        };
        if (!psGoals[1]) {
          psGoals[1] = { name: 'Spouse' };
        }
      } else {
        root.psGoals = [{ name: 'Myself' }, { name: 'Spouse' }];
      }
      _.forEach(root.psGoals, function (_psg) {
        if (!_.size(_psg.assets)) {
          _psg.assets = [{}];
        }
      });

      var otherNeed = root.otherNeed = [];
      if (_.get(fna, 'other.isValid') && aspects.indexOf("other") > -1) {
        if (_.get(fna, 'other.owner.isActive')) {
          addOData(otherNeed, fna.other.owner, 'Myself');
        }
        if (!_.size(otherNeed)) {
          otherNeed.push({ items: [{ name: 'Myself' }], goals: [] });
        }

        if (hasSpouse && _.get(fna, 'other.spouse.isActive')) {
          addOData(otherNeed, fna.other.spouse, 'Spouse');
        }
        if (_.size(otherNeed[0].items) == 1) {
          otherNeed[0].items.push({ name: 'Spouse' });
        }

        _.forEach(childs, function (ch) {
          var fon = _.find(fna.other.dependants, function (fod) {
            fod.cid === ch;
          });
          if (fon && fon.isActive) {
            addOData(otherNeed, fon, dependantProfiles[ch].fullName);
          }
        });
        if (_.size(otherNeed[0].items) === 2) {
          otherNeed[0].items.push({ name: 'Child' });
        }
      } else {
        root.otherNeed = [{ items: [{ name: 'Myself' }, { name: 'Spouse' }, { name: 'Child' }], goals: [{}] }];
      }

      _.forEach(otherNeed, function (o) {
        if (!_.size(o.goals)) {
          o.goals = [{}];
        }
      });

      //client choice budget

      if (fna.isCompleted && _.get(_bundle, "clientChoice.isBudgetCompleted") && !_.get(_bundle, "clientChoice.isAppListChanged")) {
        var clientChoiceBudget = {};
        var _ccbIds = [];
        var bundleChoice = _.get(_bundle, "clientChoice.budget") || {};
        _.forEach(["cpfMs", "cpfOa", "cpfSa", "rp", "sp", "srs"], function (_id) {
          var compareId = _id + "Compare";
          _handleCurrency(clientChoiceBudget, bundleChoice, [_id + "Budget", compareId, _id + "TotalPremium"]);
          // if (_.get(bundleChoice, compareId) < 0) {
          //   clientChoiceBudget[compareId] = "(" + clientChoiceBudget[compareId].replace("-", "") + ")";
          // }
        });
        _.forEach(["Less", "More"], function (_id) {
          if (bundleChoice["budget" + _id + "Choice"] === "Y") {
            clientChoiceBudget["budget" + _id + "Reason"] = replaceNewLine(bundleChoice["budget" + _id + "Reason"]);
          }
        });
        root.clientChoiceBudget = clientChoiceBudget;
      }

      //budget
      var bgValid = true;
      var hasBudget = false;
      var budget = {};
      if (fe.owner && !fe.owner.init) {
        var owner = fe.owner;
        _handleCurrency(budget, owner, ["singPrem", "aRegPremBudget", "cpfOaBudget", "cpfSaBudget", "cpfMsBudget", "srsBudget"], 2, true);
        if (owner.aRegPremBudget) {
          budget.hasRegPrem = "Y";
          hasBudget = true;
          if (!owner.aRegPremBudgetSrc || owner.confirmBudget === 'Y' && !owner.confirmBudgetReason) {
            bgValid = false;
          } else {
            _handleString(budget, owner, ["aRegPremBudgetSrc", "confirmBudget"]);
            if (budget.confirmBudget === 'Y') {
              _handleString(budget, owner, ["confirmBudgetReason"]);
            }
          }
        }

        if (owner.singPrem) {
          hasBudget = true;
          if (!owner.singPremSrc || owner.confirmSingPremBudget === 'Y' && !owner.confirmSingPremBudgetReason) {
            bgValid = false;
          } else {
            _handleString(budget, owner, ["singPremSrc", "confirmSingPremBudget"]);
            if (budget.confirmSingPremBudget === 'Y') {
              _handleString(budget, owner, ["confirmSingPremBudgetReason"]);
            }
          }
        }

        if (owner.cpfOaBudget) {
          hasBudget = true;
          if (!owner.cpfOa || owner.cpfOaBudget > owner.cpfOa) {
            bgValid = false;
          }
        }

        if (owner.cpfSaBudget) {
          hasBudget = true;
          if (!owner.cpfSa || owner.cpfSaBudget > owner.cpfSa) {
            bgValid = false;
          }
        }

        if (owner.cpfMsBudget) {
          hasBudget = true;
          if (!owner.cpfMs || owner.cpfMsBudget > owner.cpfMs) {
            bgValid = false;
          }
        }

        if (owner.srsBudget) {
          hasBudget = true;
          if (!owner.srs || owner.srsBudget > owner.srs) {
            bgValid = false;
          }
        }

        if (!owner.forceIncome || owner.forceIncome === 'Y' && !owner.forceIncomeReason) {
          bgValid = false;
        }
        if (owner.forceIncome) {
          _handleString(budget, owner, ["forceIncome"]);
          if (owner.forceIncome === 'Y') {
            _handleString(budget, owner, ["forceIncomeReason"]);
          }
        }
      } else {
        bgValid = false;
      }

      if (hasBudget && bgValid) {
        root.budget = budget;
      }

      //trusted individual
      if (pda && pda.isCompleted && (checkHasTi(profile) || hasSpouse && checkHasTi(spouseProfile))) {
        root.hasTi = pda.trustedIndividual;
        if (root.hasTi === 'Y') {
          var language = {
            "en": "English",
            "mandarin": "Mandarin",
            "malay": "Malay",
            "tamil": "Tamil"
          };
          var _idDocType = {
            "nric": "NRIC",
            "bcNo": "Birth Certificate No.",
            "fin": "FIN",
            "passport": "Passport"
          };

          var tiLang = _.get(_bundle, "formValues." + profile.cid + ".declaration.TRUSTED_IND01");
          tiLang = tiLang ? tiLang == "other" ? _.get(_bundle, "formValues." + profile.cid + ".declaration.TRUSTED_IND02") : language[tiLang] : "";
          root.ti = _.cloneDeep(profile.trustedIndividuals);
          root.ti.relationship = getRelationship(root.ti.relationship);
          root.ti.language = tiLang || "";
          handleProfileData(root.ti);
        }
      }

      if (!root.ti) {
        root.ti = { idDocType: 'NRIC/Passport' };
      }

      var prodType = _.get(fna, "productType.prodType") || "";
      var hasCka = fe.isCompleted && fna.lastStepIndex >= 4 && _.get(fna, "ckaSection.isValid");
      if (hasCka && prodType.indexOf("investLinkedPlans") > -1) {
        root.cka = {};
        root.cka.owner = fna.ckaSection.owner || {};
        handleCka(root.cka.owner);
      }

      var hasRa = fe.isCompleted && fna.lastStepIndex >= 4 && _.get(fna, "raSection.isValid");
      if (hasRa && (prodType.indexOf("participatingPlans") > -1 || prodType.indexOf("investLinkedPlans") > -1)) {
        root.ra = {};
        root.ra.owner = fna.raSection.owner || {};
        var haveSelfSectLv = _.get(fna, "ckaSection.isValid") && _.get(fna, "ckaSection.owner.passCka") == "Y" ? true : false;
        handleRa(root.ra.owner, haveSelfSectLv);
      }

      AppHandler.getAppListView({ profile: profile }, session, function (aList) {
        var clientChoiceFinished = aList.clientChoiceFinished,
            quotCheckedList = aList.quotCheckedList;

        var declaration = _.get(_bundle, "formValues." + profile.cid + ".declaration");
        if (declaration && clientChoiceFinished && fna.isCompleted) {
          var tickActivity = _.get(declaration, "ROADSHOW01");
          if (tickActivity === "Y") {
            root.tickActivity = _.get(declaration, "ROADSHOW04");
            var tickDate = _.get(declaration, "ROADSHOW02");
            if (tickDate) {
              tickDate = new Date(tickDate);
              root.tickDate = tickDate.getDate() + "/" + (tickDate.getMonth() + 1) + "/" + tickDate.getFullYear();
              root.tickVenue = _.get(declaration, "ROADSHOW03") || "";
            }
          }
        }
        var quots = [],
            qidx = 1;
        var addQuot = function addQuot(quotId, isSelected, callback) {
          dao.getDoc(quotId, function (quot) {
            quot.isSelected = isSelected ? "Y" : "N";
            quots.push(quot);
            if (_.size(quots) === _.size(quotCheckedList)) {
              callback(quots);
            } else {
              addQuot(quotCheckedList[qidx].id, quotCheckedList[qidx++].checked, callback);
            }
          });
        };
        root.cmdProd = [];
        root.selectedPlan = [];
        root.unselectedPlan = [];
        root.choiceCompleted = clientChoiceFinished && fna.isCompleted ? "Y" : "N";
        if (aList.clientChoiceFinished && _.size(quotCheckedList) && fna.isCompleted) {
          //recommendation
          root.recommendProd = [];
          _.forEach(fna.sfAspects, function (sfa) {
            var cids = _.split(sfa.cid, ",");
            for (var i in cids) {
              cids[i] = cids[i] === profile.cid ? "Myself" : hasSpouse && spouseProfile && spouseProfile.cid === cids[i] ? "Spouse" : _.at(dependantProfiles[cids[i]], 'fullName')[0];
            }
            var cmdProd = {
              name: cids.join(", "),
              // special handling for disability benefit since the value changed
              aspect: _.toLower(sfa.title).indexOf('disability') > -1 ? 'Disability benefit' : sfa.title
            };
            if (_.size(cids) === 1) {
              var aValues = _.at(fna, [sfa.aid + ".owner", sfa.aid + ".spouse", sfa.aid + ".dependants"]);
              var aValue = profile.cid === sfa.cid ? aValues[0] : hasSpouse && spouseProfile.cid === sfa.cid ? aValues[1] : _.find(aValues[2], function (_av) {
                return _av.cid === sfa.cid;
              });
              if (aValue) {
                if (['psGoals', 'other'].indexOf(sfa.aid) > -1) {
                  var _sValue = _.get(aValue, "goals[" + (sfa.index - 1) + "]", {});
                  cmdProd.totShortfall = _sValue.totShortfall;
                  cmdProd.timeHorizon = _sValue.timeHorizon;
                } else if (['pcHeadstart'].indexOf(sfa.aid) > -1) {
                  cmdProd.totShortfall = _.toLower(sfa.title).indexOf('protection') > -1 ? aValue.pTotShortfall : aValue.ciTotShortfall;
                } else {
                  cmdProd.totShortfall = aValue.totShortfall;
                  cmdProd.timeHorizon = aValue.timeHorizon;
                }
              }
            }
            root.recommendProd.push(cmdProd);
          });

          addQuot(quotCheckedList[0].id, quotCheckedList[0].checked, function (_quots) {
            _.forEach(_quots, function (_qu, quIdx) {
              var funds = _.get(_qu, "fund.funds") || [{}];
              var quotItems = [];
              var shieldInfo = {};
              if (_qu.quotType == 'SHIELD') {
                _handleCurrency(shieldInfo, _qu, ['totCashPortion', 'totMedisave', 'totPremium']);

                var _loop = function _loop() {
                  handleQuot(_qu.insureds[i], root, _qu.quotType);

                  var _$get = _.get(_qu, "insureds[" + i + "]", {}),
                      _$get$plans = _$get.plans,
                      plans = _$get$plans === undefined ? [] : _$get$plans,
                      _$get$iFullName = _$get.iFullName,
                      iFullName = _$get$iFullName === undefined ? '' : _$get$iFullName;

                  var planNames = _.map(plans, 'covName.en', []);
                  var covCodes = _.map(plans, 'covCode', []);
                  var covClasses = _.map(plans, 'covClass', []);
                  quotItems = _.concat(quotItems, plans);

                  if (_qu.isSelected === "Y") {
                    //find if there is any plans selected with same plans and rider
                    //AXA Shield (Plan A) and AXA SHield (Plan B) will have same covCode but different covClass
                    var sameIdx = _.findIndex(root.selectedPlan, function (sPlan) {
                      return sPlan.quIdx === quIdx && !_.difference(sPlan.covCodes, covCodes).length && !_.difference(covCodes, sPlan.covCodes).length && !_.difference(sPlan.covClasses, covClasses).length && !_.difference(covClasses, sPlan.covClasses).length;
                    });
                    if (sameIdx === -1) {
                      root.selectedPlan.push({
                        quotType: _qu.quotType,
                        value: _.join(planNames, ', '),
                        name: iFullName,
                        covCodes: covCodes,
                        covClasses: covClasses,
                        quIdx: quIdx
                      });
                    } else {
                      root.selectedPlan[sameIdx].name += ' / ' + iFullName;
                    }
                  } else if (_qu.isSelected === "N") {
                    //find if there is any plans selected with same plans and rider
                    var _sameIdx = _.findIndex(root.unselectedPlan, function (sPlan) {
                      return sPlan.quIdx === quIdx && !_.difference(sPlan.covCodes, covCodes).length && !_.difference(covCodes, sPlan.covCodes).length;
                    });
                    if (_sameIdx === -1) {
                      root.unselectedPlan.push({
                        quotType: _qu.quotType,
                        value: _.join(planNames, ', '),
                        name: iFullName,
                        covCodes: covCodes,
                        covClasses: covClasses,
                        quIdx: quIdx
                      });
                    } else {
                      root.unselectedPlan[_sameIdx].name += ' / ' + iFullName;
                    }
                  }
                };

                for (var i in _qu.insureds) {
                  _loop();
                }

                var ropBlock = _.get(_qu, "clientChoice.recommendation.rop_shield.ropBlock");
                if (ropBlock) {
                  var hasRop = false;
                  _qu.clientChoice.Q1 = "N";
                  _qu.clientChoice.Q2 = "N";
                  _qu.clientChoice.Q3 = "N";
                  for (var i in ropBlock.iCidRopAnswerMap) {
                    if (ropBlock[ropBlock.iCidRopAnswerMap[i]] === "Y") {
                      _qu.clientChoice.Q1 = "Y";
                      _qu.clientChoice.Q2 = ropBlock.ropQ2;
                      _qu.clientChoice.Q3 = ropBlock.ropQ3;
                    }
                  }
                }
              } else {
                handleQuot(_qu, root);
                quotItems = _qu.plans;
                if (_qu.isSelected === "Y") {
                  root.selectedPlan.push({
                    value: _.join(_.map(_qu.plans, 'covName.en', []), ','),
                    name: _qu.iFullName
                  });
                } else if (_qu.isSelected === "N") {
                  root.unselectedPlan.push({
                    value: _.join(_.map(_qu.plans, 'covName.en', []), ','),
                    name: _qu.iFullName
                  });
                }
              }

              // replace \n to br
              var recommendation = _.get(_qu, "clientChoice.recommendation");
              var isShield = _.get(_qu, 'quotType') === 'SHIELD';
              if (recommendation) {
                _.forEach(["benefit", "reason", "limitation"], function (rId) {
                  recommendation[rId] = replaceNewLine(recommendation[rId]);
                });
                var rop = recommendation.rop;
                var shieldRop = recommendation.rop_shield;
                if (rop && !isShield) {
                  _.forEach(["choiceQ1Sub3"], function (rId) {
                    rop[rId] = replaceNewLine(rop[rId]);
                  });
                } else if (isShield && shieldRop) {
                  rop['isConsultantExplanedRopinShield'] = _.get(recommendation, 'rop_shield.ropBlock.ropQ3', 'N');
                  rop['choiceQ1Sub3'] = replaceNewLine(_.get(recommendation, 'rop_shield.ropBlock.ropQ1sub3', ''));
                }
              }

              root.cmdProd.push({
                type: _qu.quotType,
                isSelected: _qu.isSelected,
                items: quotItems,
                funds: funds,
                choice: _qu.clientChoice,
                shieldInfo: shieldInfo
              });
            });

            if (!_.size(root.cmdProd)) {
              root.cmdProd = [{}];
            }
            for (var i = 3 - _.size(root.selectedPlan); i > 0; i--) {
              root.selectedPlan.push({});
            }
            for (var _i = 3 - _.size(root.unselectedPlan); _i > 0; _i--) {
              root.unselectedPlan.push({});
            }
            callback(reportData);
          });
        } else {
          root.cmdProd = [{}];
          for (var i = 3 - _.size(root.selectedPlan); i > 0; i--) {
            root.selectedPlan.push({});
          }
          for (var _i2 = 3 - _.size(root.unselectedPlan); _i2 > 0; _i2--) {
            root.unselectedPlan.push({});
          }
          callback(reportData);
        }
      });
    } catch (e) {
      logger.error("ERROR IN GENERATE FNA REPORT: ", e);
      callback(false);
    }
  });
};

var handleQuot = function handleQuot() {
  var quot = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var root = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var quotType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";

  var baseProductCode = quot.baseProductCode || "";
  _.forEach(quot.plans, function (plan) {
    var mode = quotType == "SHIELD" ? _.get(plan, "payFreq") : _.get(quot, "paymentMode");
    var frequency = mode === "A" ? "Annual" : mode === "S" ? "Semi Annual" : mode === "Q" ? "Quarterly" : mode === "M" ? "Monthly" : "-";
    plan.productLineTitle = _.get(global, "optionsMap.productLine.options." + quot.plans[0].productLine + ".title.en");
    plan.frequency = plan.covCode === 'IND' ? '' : frequency;
    plan.mode = handleQuotPlanPremiumMode(quot);
    plan.policyTerm = quotType == "SHIELD" ? "-" : plan.policyTerm === "999" ? baseProductCode === "BAA" ? "To Age 75" : "To Age 99" : plan.polTermDesc;
    // plan.policyTermYr ? plan.policyTermYr : plan.policyTerm;
    plan.premTermDesc = plan.premTermDesc || '-';
    plan.sumInsured = handleQuotPlanSumInsured(quotType, plan, baseProductCode);
    plan.premium = getCurrency(plan.premium);
    plan.iName = quot.iFullName;
    plan.ccy = quot.ccy;
    if (['LTT', 'LVT', 'ADBR', 'EPB', 'WPSR', 'WPTN', 'WPPT', 'LARB', 'CARB'].indexOf(plan.covCode) > -1) {
      plan.premium = 'unit deduction';
      plan.mode = '-';
      plan.frequency = '-';
    }
  });
};

var handleQuotPlanPremiumMode = function handleQuotPlanPremiumMode(quot) {
  var rpPaymentMethod = ['A', 'S', 'Q', 'M'];
  var spPaymentMethod = ['L'];
  var paymentMode = _.get(quot, 'paymentMode');
  if (quot.quotType === 'SHIELD') {
    return 'RP';
  } else if (rpPaymentMethod.indexOf(paymentMode) > -1) {
    return 'RP';
  } else if (spPaymentMethod.indexOf(paymentMode) > -1) {
    var lang = 'en';
    return _.get(quot, "policyOptionsDesc.paymentMethod." + lang, 'SP');
  } else {
    return 'RP';
  }
};

var handleQuotPlanSumInsured = function handleQuotPlanSumInsured(quotType, plan, baseProductCode) {
  var isShield = quotType === 'SHIELD';
  if (isShield) {
    switch (_.get(plan, 'covClass')) {
      case 'A':
        return 'Plan A';
      case 'B':
        return 'Plan B';
      case 'C':
        return 'Standard Plan';
      default:
        return '-';
    }
  } else if (['IND', 'AWICA', 'AWICP'].indexOf(baseProductCode) > -1) {
    var planSumInsured = _.get(plan, 'sumInsured', 0);
    return planSumInsured === 0 ? '-' : getCurrency(planSumInsured);
  } else if (['AWT', 'PUL'].indexOf(baseProductCode) > -1) {
    return '-';
  } else if (['RHP'].indexOf(baseProductCode) > -1) {
    var planRetireInCome = Math.round(_.get(plan, 'gteedAnnualRetireIncome', 0));
    return planRetireInCome === 0 ? '-' : getCurrency(planRetireInCome);
  } else if (['NPE'].indexOf(baseProductCode) > -1) {
    var incomePayout = _.get(plan, 'incomePayout', 0);
    return incomePayout === 0 ? '-' : getCurrency(incomePayout);
  } else if (plan.saViewInd == 'Y') {
    return getCurrency(_.get(plan, 'sumInsured', 0));
  } else {
    return '-';
  }
};

var handleRp = function handleRp(root) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];
  var profile = arguments[3];
  var template = arguments[4];

  var result = { name: name, assets: [], hasData: 'Y' };
  _.forEach(value.assets, function (g, gi) {
    var aItem = { title: getAssetName(g.key, template) };
    _handleCurrency(aItem, g, ["calAsset", "usedAsset"]);
    _handleDemical(aItem, g, ["return"]);
    result.assets.push(aItem);
  });
  _handleInteger(result, value, ["retireAge", "timeHorizon", "retireDuration"]);
  _handleDemical(result, value, ["avgInflatRate", "iarRate2"]);
  _handleCurrency(result, value, ["annualInReqRetirement", "compFv", "othRegIncome", "firstYrPMT", "compPv", "maturityValue"]);
  _handleString(result, value, ["unMatchPmtReason"]);
  result.inReqRetirement = getCurrency((value.inReqRetirement || 0) * 12);
  _handleShortfall(result, value);
  if (value.inReqRetirement > profile.allowance) {
    if (name === 'Myself') {
      root.oUnmatchIncome = value.unMatchPmtReason;
    } else {
      root.sUnmatchIncome = value.unMatchPmtReason;
    }
  }
  return result;
};

var _handleInteger = function _handleInteger(data, oValue, ids) {
  _.forEach(ids, function (id) {
    var value = Number(oValue[id] || 0);
    data[id] = value < 0 ? "(" + Math.abs(value) + ")" : value.toString();
  });
};

var _handleCurrency = function _handleCurrency(data, oValue, ids) {
  var decimal = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 2;
  var showBlankIfZero = arguments[4];

  _.forEach(ids, function (id) {
    var value = oValue[id] || 0;
    data[id] = getCurrency(value, decimal, showBlankIfZero);
  });
};

var getCurrency = function getCurrency() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var decimal = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
  var showBlankIfZero = arguments[2];

  value = Number(value);

  // don't show 0 if user not input data
  if (showBlankIfZero && (value === 0 || !value)) {
    return '';
  }

  var text = Math.abs(value).toFixed(decimal).toString();
  var temp = text.split(",");
  temp[0] = temp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return value < 0 ? "(" + temp.join(".") + ")" : temp.join(".");
};

var _handleDemical = function _handleDemical(data, oValue, ids) {
  _.forEach(ids, function (id) {
    var value = Number(oValue[id] || 0);
    var text = Math.abs(value).toFixed(2).toString();
    data[id] = value < 0 ? "(" + text + ")" : text;
  });
};

var _handleString = function _handleString(data, oValue, ids) {
  _.forEach(ids, function (id) {
    data[id] = oValue[id] || "";
  });
};

var _handleShortfall = function _handleShortfall(data, value) {
  var id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'totShortfall';

  data[id] = getCurrency(Math.abs(value[id] || 0));
  if (value[id] < 0) {
    data[id] = "(" + data[id] + ")";
  }
};

var handleFip = function handleFip(root) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];
  var profile = arguments[3];
  var template = arguments[4];

  var result = { name: name };
  var sum = 0;
  _.forEach(value.assets, function (g, gi) {
    sum += g.calAsset;
  });
  result.totAsset = getCurrency(sum);
  _handleInteger(result, value, ["requireYrIncome", "timeHorizon", "retireDuration"]);
  _handleCurrency(result, value, ["pmt", "annualRepIncome", "lumpSum", "totLiabilities", "finExpenses", "totRequired", "existLifeIns", "maturityValue", "othFundNeeds"]);
  _handleDemical(result, value, ["iarRate2"]);
  _handleString(result, value, ["othFundNeedsName"]);
  if (result.othFundNeedsName == "") {
    delete result.othFundNeedsName;
  }
  _handleShortfall(result, value);

  if (value.pmt > profile.allowance) {
    if (name === 'Myself') {
      root.oUnmatchPmt = value.unMatchPmtReason;
    } else {
      root.sUnmatchPmt = value.unMatchPmtReason;
    }
  }

  return result;
};

var handleRa = function handleRa(values, hasSelectRLv) {
  var questions = _.at(values, ["riskPotentialReturn", "avgAGReturn", "smDroped", "alofLosses", "expInvTime", "invPref"]),
      totSource = 0,
      valid = true;
  _.forEach(questions, function (q) {
    var source = Number(q);
    if (!source) {
      valid = false;
    } else {
      totSource += source;
    }
  });
  if (valid) {
    values.totSource = totSource;
  }
  if (!values.selfSelectedriskLevel || values.selfSelectedriskLevel == values.assessedRL || !hasSelectRLv && valid) {
    values.selfSelectedriskLevel = "";
    values.selfRLReasonRP = "";
  }
};
var checkHasTi = function checkHasTi() {
  var profile = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var cnt = 0;

  if (profile.age > 62) cnt++;
  if (profile.education === 'below') cnt++;
  if (profile.language && profile.language.indexOf("en") == -1) cnt++;
  return cnt > 1 ? true : false;
};

var getAspectName = function getAspectName(aid, template) {
  var fnaForm = template.fnaForm;

  return _.at(_.find(_.at(fnaForm, 'items[0].items[0].options')[0], function (opt) {
    return opt.value === aid;
  }), 'title.en')[0];
};

var getAssetName = function getAssetName(aid, template) {
  var feForm = template.feForm;

  return _.at(_.find(_.at(feForm, 'items[0].items[0].items[0].items[0].items')[0], function (opt) {
    return opt.id === aid;
  }), 'title.en')[0];
};

var addOData = function addOData(oArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];

  var item = { name: name, isActive: value.isActive ? "Y" : "N" };
  var position = oArr.length === 0 || oArr[oArr.length - 1].items.length === 3 ? 1 : oArr[oArr.length - 1].items.length + 1;
  if (value.isActive) {
    if (position === 1) {
      oArr.push({ items: [], goals: [] });
    }
    oArr[oArr.length - 1].items.push(item);
    var goalNo = value.goalNo,
        goals = value.goals;

    var index = Number(goalNo);
    for (var i = 0; i < index; i++) {
      var goal = goals[i];
      var oItem = { position: position };
      _handleCurrency(oItem, goal, ["needsValue", "extInsuDisplay"]);
      _handleString(oItem, goal, ["goalName"]);
      _handleShortfall(oItem, goal);
      oArr[oArr.length - 1].goals.push(oItem);
    }
  }
};

var addPsgData = function addPsgData(psgArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];
  var template = arguments[3];

  var item = { name: name };
  if (value.isActive) {
    (function () {
      var items = [];
      var assets = [];
      var _value$goalNo = value.goalNo,
          goalNo = _value$goalNo === undefined ? 0 : _value$goalNo,
          _value$goals = value.goals,
          goals = _value$goals === undefined ? [] : _value$goals;

      var index = Number(goalNo);

      var _loop2 = function _loop2(i) {
        var goal = goals[i] || {};
        var spgData = { name: goal.goalName };
        _handleCurrency(spgData, goal, ["compFv", "projMaturity"]);
        _handleInteger(spgData, goal, ["timeHorizon"]);
        _handleShortfall(spgData, goal);
        _.forEach(goal.assets, function (ga) {
          var aItem = { title: getAssetName(ga.key, template), position: i + 1 };
          _handleCurrency(aItem, ga, ["calAsset", "usedAsset"]);
          _handleDemical(aItem, ga, ["return"]);
          assets.push(aItem);
        });
        items.push(spgData);
      };

      for (var i = 0; i < index; i++) {
        _loop2(i);
      }
      item.assets = assets;
      item.items = items;
    })();
  }
  psgArr.push(item);
};

var getRelationship = function getRelationship(key) {
  var options = global.optionsMap.relationship.options;
  return _.at(_.find(options, function (opt) {
    return opt.value === key;
  }), 'title.en')[0];
};

var addCiData = function addCiData(ciArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];

  var ciData = { name: name };
  _handleCurrency(ciData, value, ["pmt", "annualLivingExp", "lumpSum", "mtCost", "totCoverage", "ciProt"]);
  _handleDemical(ciData, value, ["iarRate2"]);
  _handleInteger(ciData, value, ["requireYrIncome"]);
  _handleShortfall(ciData, value);

  var totAsset = 0;
  _.forEach(value.assets, function (ca) {
    totAsset += ca.usedAsset;
  });
  ciData.totAsset = getCurrency(totAsset);

  if (ciArr.length === 0 || ciArr[ciArr.length - 1].items.length === 3) {
    ciArr.push({ items: [ciData] });
  } else {
    ciArr[ciArr.length - 1].items.push(ciData);
  }
};

var addDiData = function addDiData(diArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];

  var diData = { name: name };
  _handleCurrency(diData, value, ["pmt", "annualPmt", "disabilityBenefit", "othRegIncome"]);
  _handleInteger(diData, value, ["requireYrIncome"]);
  _handleShortfall(diData, value);

  if (diArr.length === 0 || diArr[diArr.length - 1].items.length === 3) {
    diArr.push({ items: [diData] });
  } else {
    diArr[diArr.length - 1].items.push(diData);
  }
};

var addPaData = function addPaData(paArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];

  var paData = { name: name };
  _handleCurrency(paData, value, ["pmt", "extInsurance"]);
  _handleShortfall(paData, value);

  if (paArr.length === 0 || paArr[paArr.length - 1].items.length === 3) {
    paArr.push({ items: [paData] });
  } else {
    paArr[paArr.length - 1].items.push(paData);
  }
};

var addPchData = function addPchData(pchArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];

  var pchData = { name: name };
  if (_.indexOf(value.providedFor, 'P') > -1) {
    pchData.showProtection = 'Y';
    _handleCurrency(pchData, value, ["sumAssuredProvided", "extInsurance"]);
    _handleShortfall(pchData, value, 'pTotShortfall');
  }
  if (_.indexOf(value.providedFor, 'C') > -1) {
    pchData.showCriticalIllness = 'Y';
    _handleCurrency(pchData, value, ["sumAssuredCritical", "ciExtInsurance"]);
    _handleShortfall(pchData, value, 'ciTotShortfall');
  }

  if (pchArr.length === 0 || pchArr[pchArr.length - 1].items.length === 3) {
    pchArr.push({ items: [pchData] });
  } else {
    pchArr[pchArr.length - 1].items.push(pchData);
  }
};

var addEpData = function addEpData(epArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];
  var template = arguments[3];

  var epPosition = epArr.length === 0 || epArr[epArr.length - 1].items.length === 3 ? 1 : epArr.length + 1;
  var epData = { name: name };
  _handleInteger(epData, value, ["curAgeofChild", "yrtoSupport", "timeHorizon"]);
  _handleCurrency(epData, value, ["costofEdu", "estTotFutureCost", "maturityValue"]);
  _handleDemical(epData, value, ["avgEduInflatRate"]);
  _handleShortfall(epData, value);
  if (epPosition === 1) {
    epArr.push({ items: [epData], assets: [] });
  } else {
    epArr[epArr.length - 1].items.push(epData);
  }

  _.forEach(value.assets, function (g, gi) {
    var _g$calAsset = g.calAsset,
        calAsset = _g$calAsset === undefined ? 0 : _g$calAsset,
        _g$return = g.return,
        _r = _g$return === undefined ? 0 : _g$return,
        _g$usedAsset = g.usedAsset,
        usedAsset = _g$usedAsset === undefined ? 0 : _g$usedAsset;

    var eItem = { title: getAssetName(g.key, template), position: epArr[epArr.length - 1].items.length };
    _handleCurrency(eItem, g, ["calAsset", "usedAsset"]);
    _handleDemical(eItem, g, ["return"]);
    epArr[epArr.length - 1].assets.push(eItem);
  });
};

var addHcpData = function addHcpData(hcpArr) {
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var name = arguments[2];

  var hcpData = { name: name };
  _.forEach(hpckeys, function (hcpk) {
    hcpData[hcpk] = value[hcpk];
  });

  if (hcpArr.length === 0 || hcpArr[hcpArr.length - 1].items.length === 3) {
    hcpArr.push({ items: [hcpData] });
  } else {
    hcpArr[hcpArr.length - 1].items.push(hcpData);
  }
};

var generateFNAReportForSignDocDemo = function generateFNAReportForSignDocDemo(data, session, cb) {
  var cid = data.cid;
  var reportData = {
    root: {
      id: data.profile.cid,
      name: data.profile.fullName
    }
  };

  nDao.getFNAReportTemplate().then(function (reportTemplate) {
    PDFHandler.getFnaReportPdf(reportData, reportTemplate, function (pdf) {
      cb({
        success: true,
        fnaReport: pdf
      });
    });
  });
};

module.exports.generateFNAReportForSignDocDemo = generateFNAReportForSignDocDemo;

var _generateFNAReport = function _generateFNAReport(data, session) {
  var cid = data.cid;
  var DOC_ID = 'fnaReport';
  return new Promise(function (resolve) {
    bDao.getCurrentBundle(cid).then(function (bundle) {
      // if (bundle.status >= bDao.BUNDLE_STATUS.SIGN_FNA) {
      //   //generate token for image in mandDocs & optDoc
      //   let now = new Date().getTime();
      //   let tokensMap = {};
      //   let tokens = [];
      //   let token = createPdfToken(bundle.id, DOC_ID, now, session.loginToken);
      //   tokens.push(token);
      //   tokensMap[DOC_ID] = token.token;

      //   setPdfTokensToRedis(tokens, () => {
      //     dao.getAttachment(bundle.id, 'fnaReport', resp => { resolve({pdf: resp.data, token: token.token}); });
      //   });
      // } else {
      var promises = [];
      promises.push(cDao.getProfile(cid));
      promises.push(cDao.getAllDependantsProfile(cid));
      promises.push(nDao.getItem(cid, nDao.ITEM_ID.PDA));
      promises.push(nDao.getItem(cid, nDao.ITEM_ID.FE));
      promises.push(nDao.getItem(cid, nDao.ITEM_ID.NA));
      promises.push(nDao.getFeForm());
      promises.push(nDao.getFnaForm());
      promises.push(nDao.getFNAReportTemplate());
      Promise.all(promises).then(function (args) {
        var p = args[0],
            dp = args[1],
            pda = args[2],
            fe = args[3],
            fna = args[4],
            feForm = args[5],
            fnaForm = args[6],
            reportTemplates = args[7];
        genFnaReportData(session, session.agent, p, dp, pda, fe, fna, data.skipEmailInReport, { feForm: feForm, fnaForm: fnaForm }, function (reportData) {
          if (!reportData) {
            resolve({ pdf: null });
          } else {
            PDFHandler.getFnaReportPdf(reportData, reportTemplates, function (pdf) {
              resolve({ pdf: pdf });
            });
          }
        });
      });
      //}
    });
  });
};

module.exports.generateFNAReport = function (data, session, callback) {
  _generateFNAReport(data, session).then(function (resp) {
    callback({
      success: resp.pdf ? true : false,
      fnaReport: resp.pdf,
      token: resp.token
    });
  });
};

module.exports.getFNAEmailTemplate = function (data, session, callback) {
  nDao.getFNAEmailTemplate(function (result) {
    if (result && !result.error) {
      callback({
        success: true,
        agentSubject: result.agentSubject,
        agentContent: result.agentContent,
        clientSubject: result.clientSubject,
        clientContent: result.clientContent,
        embedImgs: result.embedImgs
      });
    } else {
      callback({
        success: false
      });
    }
  });
};

var prepareFnaReportAtt = function prepareFnaReportAtt(pdf, emailId, clientProfile, agentProfile, callback) {
  var password = void 0;
  if (emailId === 'agent') {
    var agentCode = agentProfile.agentCode;
    if (agentCode.length >= 6) {
      password = agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
    } else {
      password = agentCode.replace(/ /g, '').substr(0, agentCode.length).toLowerCase();
    }
    //password = agentCode.substr('000124' - 6, 6) + agentProfile.name.substr(0, 5).toLowerCase();
  } else {
    var dob = new Date(clientProfile.dob);
    password = dob.format('ddmmmyyyy').toUpperCase();
  }
  CommonFunctions.protectBase64Pdf(pdf, password, function (data) {
    callback({
      isProtectBase: true,
      fileName: 'FNA_Report.pdf', // TODO file name
      data: data
    });
  });
  /**callback({
    fileName: 'FNA_Report.pdf', // TODO file name
    data: pdf
  });*/
};

module.exports.emailFnaReport = function (data, session, callback) {
  var emails = data.emails,
      profile = data.profile,
      agentProfile = data.agentProfile,
      skipEmailInReport = data.skipEmailInReport,
      authToken = data.authToken,
      webServiceUrl = data.webServiceUrl;

  data.cid = profile.cid;
  if (emails) {
    companyDao.getCompanyInfo(function (companyInfo) {
      _generateFNAReport(data, session).then(function (attachment) {
        var fnaReportpdfStr = attachment.pdf;
        var promises = [];
        _.each(emails, function (email, index, emails) {
          promises.push(new Promise(function (resolve) {
            prepareFnaReportAtt(fnaReportpdfStr, email.id, profile, agentProfile, function (att) {
              var attArr = [];
              attArr.push(att);
              _.each(email.embedImgs, function (value, key) {
                attArr.push({
                  fileName: key,
                  cid: key,
                  data: value
                });
              });
              EmailUtils.sendEmail({
                from: companyInfo.fromEmail, // TODO
                userType: email.id,
                agentCode: email.agentCode,
                dob: email.dob,
                to: _.join(email.to, ','),
                title: email.title,
                content: email.content,
                attachments: attArr
              }, resolve, authToken, webServiceUrl);
            });
          }));
        });
        Promise.all(promises).then(function () {
          callback({ success: true });
        });
      });
    });
  } else {
    callback({ success: true });
  }
};

/*
  tid: target id, can be proposer, spouse id or dependant's id
  p: profile, can get from client store
  pda: personal data acknowledgement, can get from needs store
  fna: need analysis data, can get from needs store
*/
module.exports.getSelectedNeeds = function (tid, p, pda, fna) {
  var dt = "";
  var hasSpouse = _.get(pda, "applicant") === "joint" ? true : false;
  var sp = _.find(_.get(p, "dependants"), function (d) {
    return d.relationship === "SPO";
  });
  var sid = _.get(sp, "cid");

  if (tid === p.cid) {
    dt = "owner";
  } else if (hasSpouse && sid === tid) {
    dt = "spouse";
  } else if (_.find(_.get(pda, "dependants") && _.get(pda, "dependants").split(','), function (dCid) {
    return dCid === tid;
  })) {
    dt = "dependants";
  }

  //if dt has no value, cid is invalid
  if (!dt) {
    return;
  }

  var aspects = _.split(_.get(fna, "aspects"), ",");
  var result = [];
  _.forEach(aspects, function (a) {
    var v = _.get(fna, a + "." + dt);
    if (dt === "dependants") {
      v = _.find(v, function (_v) {
        return _v.cid === tid;
      });
    }
    if (!_.get(v, "init") && _.get(v, "isActive")) {
      result.push(a);
    }
  });
  return result;
};