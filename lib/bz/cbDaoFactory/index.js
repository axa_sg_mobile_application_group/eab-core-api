'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var db = null;

var create = exports.create = function create() {
  // the file is included from main project
  // e.g. /axa-sg-app/app/cbDaoFactory/index.js
  db = require('../../../../../app/cbDaoFactory');
  return db;
};

exports.default = {
  create: create
};