'use strict';

var dao = require('./cbDaoFactory').create();
var nUtil = require('./utils/NotificationsUtils');
var _isEqual = require('lodash/fp/isEqual');
var _isEmpty = require('lodash/fp/isEmpty');
var _getOr = require('lodash/fp/getOr');
var approvalStatusModel = require('./model/approvalStatus');
var emailHandler = require('./EmailHandler');
var approvalhandler = require('./ApprovalHandler');
var smsHandler = require('./SMSHandler');

var _get = require('lodash/fp/get');
var logger = global.logger || console;
var moment = require('moment');
var ConfigConstant = require('../app/constants/ConfigConstants');
var EmailUtils = require('./utils/EmailUtils');
var SmsUtils = require('./utils/SmsUtils');
var _ = require('lodash');

var DefaultEmailSender = void 0;
var DefaultEmailRecipients = void 0;

module.exports.notifyAXAwithAbnormalApprovalCase = function (app, failedReason) {
    new Promise(function (resolve) {
        dao.getDoc('companyInfo', function (companyInfo) {
            DefaultEmailSender = _get('fromEmail', companyInfo);
            DefaultEmailRecipients = _get('config.abnormalSuppDocsCaseToEmail', global, 'application3.desk@axa365.onmicrosoft.com');
            resolve();
        });
    }).then(function () {
        try {
            var policyNumber = _get('policyNumber', app) || _.flatten(_.map(_.get(app, 'iCidMapping'), function (cidObj) {
                return _.map(cidObj, function (obj) {
                    return obj.policyNumber;
                });
            }));
            var email = nUtil.getEmailObject(nUtil.TYPE.ABNORMAL_APPROVAL_CASE_NOTIFICATION, {
                sender: DefaultEmailSender,
                policyNumber: policyNumber,
                ENV_NAME: _get('config.ENV_NAME', global, 'DEV'),
                FAILED_REASON: failedReason,
                recipients: DefaultEmailRecipients
            });
            emailHandler.sendEmail({ email: email }, {}, function () {});
        } catch (e) {
            logger.error('FAILED to notify AXA with abnormal Approval Case | Get Email Object and Send: ' + e);
        }
    }).catch(function (error) {
        logger.error('FAILED to notify AXA with abnormal Approval Case: ' + error);
    });
};

module.exports.pendingDocNotification = function (data, session, cb) {
    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            var approvalCase = doc;
            dao.getDoc('companyInfo', function (companyInfo) {
                DefaultEmailSender = _get('fromEmail', companyInfo);
                resolve(approvalCase);
            });
        });
    }).then(function (tempAppCase) {
        var agentId = _get('agentId', tempAppCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, tempAppCase).then(function (rolesObj) {
            if (_isEqual(approvalStatusModel.APPRVOAL_STATUS_PDOC, _get('approvalStatus', tempAppCase))) {
                var currentUserCode = _get('agentCode', session.agent);
                var recipients = _get('agent.email', rolesObj);
                var assignedMangerCode = _get('assignedManager.agentCode', rolesObj);
                var mangerCode = _get('manager.agentCode', rolesObj);
                var cc = '';
                if (!_isEqual(currentUserCode, mangerCode)) {
                    cc = _get('manager.email', rolesObj);
                }

                // const caseNumber = _get('policyId', tempAppCase);
                var caseNumber = _get('proposalNumber', tempAppCase) || _get('policyId', tempAppCase);
                var productName = _get('productName', tempAppCase);
                var agentName = _get('agentName', tempAppCase);
                var proposerName = _get('customerName', tempAppCase);

                var submissionDate = moment(_get('submittedDate', tempAppCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                var expiryDate = moment(_get('submittedDate', tempAppCase)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);

                try {
                    var email = nUtil.getEmailObject(nUtil.TYPE.PENDING_FOR_DOCUMENT_NOTIFICATION, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        caseNumber: caseNumber,
                        productName: productName,
                        agentName: agentName,
                        proposerName: proposerName,
                        submissionDate: submissionDate,
                        expiryDate: expiryDate,
                        cc: cc
                    });
                    emailHandler.sendEmail({ email: email }, session, function () {});
                } catch (e) {
                    // ignore
                    logger.error('Pending Doc Email Exception: ' + e);
                    cb({ success: false });
                }
            }
            cb({ success: true });
        });
    }).catch(function (e) {
        logger.error('Pending Doc Email Exception: ' + e);
        cb({ success: false });
    });
};

module.exports.ApproveNotification = function (data, session) {
    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            var approvalCase = doc;
            dao.getDoc('companyInfo', function (companyInfo) {
                DefaultEmailSender = _get('fromEmail', companyInfo);
                resolve(approvalCase);
            });
        });
    }).then(function (updatedCase) {
        var agentId = _get('agentId', updatedCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, updatedCase).then(function (rolesObj) {
            var managerCode = _get('manager.agentCode', rolesObj);
            var cc = '';
            var managerEmail = _get('manager.email', rolesObj);
            var assignedManagerEmail = _get('assignedManager.email', rolesObj);

            if (!_isEqual(_getOr('', 'approveRejectManagerId', updatedCase), managerCode)) {
                cc = managerEmail;
            }

            var approvedManagerEmail = _get('approveRejectManagerEmail', updatedCase);
            var agentEmail = _get('agent.email', rolesObj);
            var recipients = agentEmail;

            var mobileNo = _get('agent.mobile', rolesObj);

            var productName = _get('productName', updatedCase);
            // const proposalNumber = _get('policyId', updatedCase);
            var proposalNumber = _get('proposalNumber', updatedCase) || _get('policyId', updatedCase);
            var proposerName = _get('customerName', updatedCase);
            var supervisorName = _get('approveRejectManagerName', updatedCase);

            var approvalDate = moment(_get('approveRejectDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
            var adviserName = _get('agentName', updatedCase);
            var approvalRemarksText = _isEmpty(_getOr('', 'accept.approveComment', updatedCase)) ? '-' : _get('accept.approveComment', updatedCase);

            if (data.approverLevel === approvalStatusModel.ONELEVEL_SUBMISSION) {
                try {
                    var email = nUtil.getEmailObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        productName: productName,
                        proposalNumber: proposalNumber,
                        proposerName: proposerName,
                        supervisorName: supervisorName,
                        approvalDate: approvalDate,
                        adviserName: adviserName,
                        approvalRemarksText: approvalRemarksText,
                        cc: cc
                    });
                    emailHandler.sendEmail({ email: email }, session, function () {});

                    var sms = nUtil.getSMSObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT, {
                        mobileNo: mobileNo,
                        productName: productName,
                        proposalNumber: proposalNumber,
                        proposerName: proposerName,
                        supervisorName: supervisorName,
                        approvalDate: approvalDate
                    });
                    smsHandler.sendSMS({ sms: sms }, session, function () {});
                } catch (e) {
                    // ignore
                    logger.error('Prepare Approved Notification Error' + e);
                }
            } else if (data.approverLevel === approvalStatusModel.FAAGENT) {
                try {
                    var agentName = _get('agentName', updatedCase);
                    var submissionDate = moment(_get('submittedDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                    var expiryDate = moment(_get('submittedDate', updatedCase)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                    approvalDate = moment(_get('supervisorApproveRejectDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                    var _email = nUtil.getEmailObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        productName: productName,
                        proposalNumber: proposalNumber,
                        proposerName: proposerName,
                        supervisorName: supervisorName,
                        approvalDate: approvalDate,
                        adviserName: adviserName,
                        approvalRemarksText: approvalRemarksText,
                        agentName: agentName,
                        cc: cc
                    });
                    emailHandler.sendEmail({ email: _email }, session, function () {});

                    var _agentId = _get('agentId', updatedCase);
                    var actionLink = _getOr('', 'config.host.path', global) + '/goApproval';
                    approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: _agentId }, session, function (resp) {
                        if (resp && resp.success) {
                            var resultArr = [];
                            ['faAdminCode'].forEach(function (obj) {
                                resultArr.push(_getOr('', 'result.' + obj, resp));
                            });
                            approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: resultArr[0] }, session, function (resp) {
                                if (resp && resp.success) {
                                    var emailResult = [];
                                    ['email'].forEach(function (obj) {
                                        emailResult.push(_getOr('', 'result.' + obj, resp));
                                    });
                                    recipients = emailResult[0];
                                    try {
                                        var faEmail = nUtil.getEmailObject(nUtil.TYPE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM, {
                                            sender: DefaultEmailSender,
                                            recipients: recipients,
                                            productName: productName,
                                            proposalNumber: proposalNumber,
                                            proposerName: proposerName,
                                            supervisorName: supervisorName,
                                            adviserName: adviserName,
                                            submissionDate: submissionDate,
                                            expiryDate: expiryDate,
                                            agentName: agentName,
                                            actionLink: actionLink
                                        });
                                        emailHandler.sendEmail({ email: faEmail }, session, function () {});
                                    } catch (faExp) {
                                        logger.error('Prepare FA Channel Approve Email: ' + faExp);
                                    }
                                }
                            });
                        }
                    });
                } catch (e) {
                    //ignore
                    logger.error('Prepare FA Channel Approve Email: ' + e);
                }
            } else if (data.approverLevel === approvalStatusModel.FAADMIN) {
                try {
                    recipients = _isEqual(managerCode, _getOr('', 'approveRejectManagerId', updatedCase)) ? agentEmail + ',' + managerEmail : agentEmail + ',' + managerEmail + ',' + assignedManagerEmail;
                    var faAdminApprovalRemarksText = _isEmpty(_getOr('', 'accept_FAAdmin.approveComment', updatedCase)) ? '-' : _get('accept_FAAdmin.approveComment', updatedCase);
                    var _agentName = _get('agentName', updatedCase);
                    var _email2 = nUtil.getEmailObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_FAFIRM, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        productName: productName,
                        adviserName: adviserName,
                        proposalNumber: proposalNumber,
                        agentName: _agentName,
                        proposerName: proposerName,
                        supervisorName: supervisorName,
                        approvalDate: approvalDate,
                        faAdminApprovalRemarksText: faAdminApprovalRemarksText
                    });

                    emailHandler.sendEmail({ email: _email2 }, session, function () {});
                } catch (e) {
                    //ignore
                    logger.error('Prepare FA Admin Approve Email: ' + e);
                }
            }
        });
    }).catch(function (e) {
        logger.error('Prepare Approve notification Error: ' + e);
    });
};

module.exports.RejectNotification = function (data, session) {
    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            var approvalCase = doc;
            dao.getDoc('companyInfo', function (companyInfo) {
                DefaultEmailSender = _get('fromEmail', companyInfo);
                resolve(approvalCase);
            });
        });
    }).then(function (updatedCase) {
        var agentId = _get('agentId', updatedCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, updatedCase).then(function (rolesObj) {
            var managerCode = _get('manager.agentCode', rolesObj);
            var cc = '';
            var managerEmail = _get('manager.email', rolesObj);
            var assignedManagerEmail = _get('assignedManager.email', rolesObj);
            if (!_isEqual(_getOr('', 'approveRejectManagerId', updatedCase), managerCode)) {
                cc = managerEmail;
            }

            var agentEmail = _get('agent.email', rolesObj);
            var recipients = agentEmail;

            var mobileNo = _get('agent.mobile', rolesObj);

            var productName = _get('productName', updatedCase);
            var adviserName = _get('agentName', updatedCase);
            // const proposalNumber = _get('policyId', updatedCase);
            var proposalNumber = _get('proposalNumber', updatedCase) || _get('policyId', updatedCase);
            var proposerName = _get('customerName', updatedCase);
            var supervisorName = _get('approveRejectManagerName', updatedCase);
            var reasonForRejection = _get('reject.rejectReason', updatedCase);
            var rejectionCommentText = _get('reject.rejectComment', updatedCase);
            var rejectionDate = moment(_get('approveRejectDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);;
            if (data.approverLevel === approvalStatusModel.ONELEVEL_SUBMISSION) {
                try {
                    var email = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        productName: productName,
                        adviserName: adviserName,
                        proposalNumber: proposalNumber,
                        proposerName: proposerName,
                        supervisorName: supervisorName,
                        reasonForRejection: reasonForRejection,
                        rejectionCommentText: rejectionCommentText,
                        rejectionDate: rejectionDate,
                        cc: cc
                    });
                    emailHandler.sendEmail({ email: email }, session, function () {});

                    if (_getOr('', 'channel.type', session.agent) !== 'FA') {
                        var sms = nUtil.getSMSObject(nUtil.TYPE.REJECTED_NOTIFICATION, {
                            mobileNo: mobileNo,
                            productName: productName,
                            proposalNumber: proposalNumber,
                            proposerName: proposerName,
                            supervisorName: supervisorName,
                            rejectionDate: rejectionDate
                        });
                        smsHandler.sendSMS({ sms: sms }, session, function () {});
                    }
                } catch (e) {
                    // ignore
                    logger.error('Prepare Reject Notification Agency Channel: ' + e);
                }
            } else if (data.approverLevel === approvalStatusModel.FAAGENT) {
                try {
                    var _email3 = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        productName: productName,
                        adviserName: adviserName,
                        proposalNumber: proposalNumber,
                        proposerName: proposerName,
                        supervisorName: supervisorName,
                        reasonForRejection: reasonForRejection,
                        rejectionCommentText: rejectionCommentText,
                        rejectionDate: rejectionDate,
                        cc: cc
                    });
                    emailHandler.sendEmail({ email: _email3 }, session, function () {});

                    var _agentId2 = _get('agentId', updatedCase);
                    approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: _agentId2 }, session, function (resp) {
                        if (resp && resp.success) {
                            var resultArr = [];
                            ['faAdminCode'].forEach(function (obj) {
                                resultArr.push(_getOr('', 'result.' + obj, resp));
                            });
                            approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: resultArr[0] }, session, function (resp) {
                                if (resp && resp.success) {
                                    var emailResult = [];
                                    ['email'].forEach(function (obj) {
                                        emailResult.push(_getOr('', 'result.' + obj, resp));
                                    });
                                    recipients = emailResult[0];
                                    try {
                                        var faEmail = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM, {
                                            sender: DefaultEmailSender,
                                            recipients: recipients,
                                            productName: productName,
                                            adviserName: adviserName,
                                            proposalNumber: proposalNumber,
                                            proposerName: proposerName,
                                            supervisorName: supervisorName,
                                            reasonForRejection: reasonForRejection,
                                            rejectionCommentText: rejectionCommentText,
                                            rejectionDate: rejectionDate
                                        });
                                        emailHandler.sendEmail({ email: faEmail }, session, function () {});
                                    } catch (faExp) {
                                        logger.error('Prepare Reject Notification FA Channel: ' + faExp);
                                    }
                                }
                            });
                        }
                    });
                } catch (e) {
                    //ignore
                    logger.error('Prepare Reject Notification FA Channel: ' + e);
                }
            } else if (data.approverLevel === approvalStatusModel.FAADMIN) {
                try {
                    recipients = _isEqual(managerCode, _getOr('', 'approveRejectManagerId', updatedCase)) ? agentEmail + ',' + managerEmail : agentEmail + ',' + managerEmail + ',' + assignedManagerEmail;
                    var faAdminRejectionCommentText = _isEmpty(_getOr('', 'reject_FAAdmin.rejectComment', updatedCase)) ? '-' : _get('reject_FAAdmin.rejectComment', updatedCase);
                    var faAdminreasonForRejection = _get('reject_FAAdmin.rejectReason', updatedCase);
                    var _email4 = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION_FAFIRM, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        productName: productName,
                        adviserName: adviserName,
                        proposalNumber: proposalNumber,
                        proposerName: proposerName,
                        faAdminreasonForRejection: faAdminreasonForRejection,
                        faAdminRejectionCommentText: faAdminRejectionCommentText,
                        rejectionDate: rejectionDate
                    });
                    emailHandler.sendEmail({ email: _email4 }, session, function () {});
                } catch (e) {
                    //ignore
                    logger.error('Prepare Reject Notification FA Admin: ' + e);
                }
            }
        });
    }).catch(function (e) {
        logger.error('Prepare Reject Notification Error: ' + e);
    });
};

module.exports.AdditionalDocumentNotification = function (data, session, cb) {
    var isHealthDeclaration = data.isHealthDeclaration,
        id = data.id,
        authToken = data.authToken,
        webServiceUrl = data.webServiceUrl;

    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            var approvalCase = doc;
            dao.getDoc('companyInfo', function (companyInfo) {
                DefaultEmailSender = _get('fromEmail', companyInfo);
                resolve(approvalCase);
            });
        });
    }).then(function (approvalCase) {
        var agentId = data.agentId;
        // let agentId = _get('agentId', approvalCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, session.agent.profileId, approvalCase).then(function (rolesObj) {
            var agentProfile = session.agent;

            var managerEmail = _get('manager.email', rolesObj);
            var proxyManagerEmail = _get('assignedManager.email', rolesObj);
            var managerMobile = _get('manager.mobile', rolesObj);
            var proxyManagerMobile = _get('assignedManager.mobile', rolesObj);

            var recipients = _isEqual(managerEmail, proxyManagerEmail) ? managerEmail : proxyManagerEmail;
            if (!recipients) {
                recipients = approvalCase.managerEmail;
            }
            var cc = _isEqual(managerEmail, proxyManagerEmail) ? '' : managerEmail;
            var mobileNo = _isEqual(managerMobile, proxyManagerMobile) ? managerMobile : proxyManagerMobile;
            if (!mobileNo) {
                mobileNo = approvalCase.managerMobile;
            }

            // const proposalNumber = _get('policyId', approvalCase);
            var proposalNumber = _get('proposalNumber', approvalCase) || _get('policyId', approvalCase);
            var productName = _get('productName', approvalCase);

            // const caseNumber = _get('policyId', approvalCase);
            var caseNumber = _get('proposalNumber', approvalCase) || _get('policyId', approvalCase);
            var proposerName = _get('proposerName', approvalCase);
            var expiryDate = moment(_get('submittedDate', approvalCase)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);

            var agentName = _get('agentName', approvalCase);

            try {
                if ((_isEqual(session.agent.agentCode, _get('manager.agentCode', rolesObj)) || _isEqual(session.agent.agentCode, _get('assignedManager.agentCode', rolesObj))) && isHealthDeclaration) {
                    // Health Declaration is uploaded by supervisor
                    _healthDeclarationIsUploadedBySupervisorNotification(id, session);
                }

                var pendingFAFirmStatus = [approvalStatusModel.APPRVOAL_STATUS_PDOCFAF, approvalStatusModel.APPRVOAL_STATUS_PFAFA, approvalStatusModel.APPRVOAL_STATUS_PDISFAF];
                // Pending in FA Firm state
                if (approvalCase && pendingFAFirmStatus.indexOf(_get('approvalStatus', approvalCase)) > -1) {
                    var _agentId3 = _get('agentId', approvalCase);
                    if (_agentId3 === session.agent.agentCode) {
                        cc = _isEqual(managerEmail, proxyManagerEmail) ? '' + managerEmail : managerEmail + ',' + proxyManagerEmail;
                    } else {
                        cc = '';
                    }
                    agentName = session.agent.name;
                    approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: _agentId3 }, session, function (resp) {
                        if (resp && resp.success) {
                            var resultArr = [];
                            ['faAdminCode'].forEach(function (obj) {
                                resultArr.push(_getOr('', 'result.' + obj, resp));
                            });
                            approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: resultArr[0] }, session, function (resp) {
                                if (resp && resp.success) {
                                    var emailResult = [];
                                    ['email'].forEach(function (obj) {
                                        emailResult.push(_getOr('', 'result.' + obj, resp));
                                    });
                                    recipients = emailResult[0];
                                    try {
                                        var email = nUtil.getEmailObject(nUtil.TYPE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION, {
                                            sender: DefaultEmailSender,
                                            recipients: recipients,
                                            proposalNumber: proposalNumber,
                                            productName: productName,
                                            agentName: agentName,
                                            caseNumber: caseNumber,
                                            proposerName: proposerName,
                                            expiryDate: expiryDate,
                                            cc: cc
                                        });
                                        email.userType = "agent";
                                        email.agentCode = session.agent.agentCode;
                                        if (session.platform === "ios") {
                                            EmailUtils.sendEmail(email, function () {}, authToken, webServiceUrl);
                                        } else {
                                            emailHandler.sendEmail({ email: email }, session, function () {});
                                        }
                                    } catch (additionalExp) {
                                        logger.error('Prepare Additional Doc Email Exception: ' + additionalExp);
                                    }
                                }
                            });
                        }
                    });
                    cb({ success: true });
                } else {
                    // Agency Channel -- Supervisor will not receive addtional doc email
                    if (_isEqual(session.agent.agentCode, _get('manager.agentCode', rolesObj)) || _isEqual(session.agent.agentCode, _get('assignedManager.agentCode', rolesObj))) {
                        cb({ success: true });
                    } else {
                        try {
                            var email = nUtil.getEmailObject(nUtil.TYPE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION, {
                                sender: DefaultEmailSender,
                                recipients: recipients,
                                proposalNumber: proposalNumber,
                                productName: productName,
                                agentName: agentName,
                                caseNumber: caseNumber,
                                proposerName: proposerName,
                                expiryDate: expiryDate,
                                cc: cc
                            });
                            email.userType = "agent";
                            email.agentCode = session.agent.agentCode;
                            if (session.platform === "ios") {
                                EmailUtils.sendEmail(email, function () {}, authToken, webServiceUrl);
                            } else {
                                emailHandler.sendEmail({ email: email }, session, function () {});
                            }
                            if (_getOr('', 'channel.type', agentProfile) !== 'FA') {
                                var sms = nUtil.getSMSObject(nUtil.TYPE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION, {
                                    mobileNo: mobileNo,
                                    agentName: agentName,
                                    caseNumber: caseNumber,
                                    proposerName: proposerName,
                                    expiryDate: expiryDate
                                });
                                if (session.platform === "ios") {
                                    SmsUtils.sendSms(sms, function () {}, authToken, webServiceUrl);
                                } else {
                                    smsHandler.sendSMS({ sms: sms }, session, function () {});
                                }
                            }
                            cb({ success: true });
                        } catch (addExp) {
                            logger.error('Prepare Additional Doc Email Exception: ' + addExp);
                            cb({ success: false });
                        }
                    }
                }
            } catch (e) {
                // ignore
                logger.error('Prepare Additional Doc Email Exception: ' + e);
                cb({ success: false });
            }
        });
    }).catch(function (e) {
        logger.error('Additional Doc Email Exception: ' + e);
        cb({ success: false });
    });
};

var _healthDeclarationIsUploadedBySupervisorNotification = function _healthDeclarationIsUploadedBySupervisorNotification(approvalId, session) {
    new Promise(function (resolve) {
        dao.getDoc(approvalId, function (doc) {
            var approvalCase = doc;
            dao.getDoc('companyInfo', function (companyInfo) {
                DefaultEmailSender = _get('fromEmail', companyInfo);
                resolve(approvalCase);
            });
        });
    }).then(function (doc) {
        var agentId = _get('agentId', doc);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, doc).then(function (rolesObj) {
            if (doc && _get('approvalStatus', doc) !== approvalStatusModel.APPRVOAL_STATUS_APPROVED) {
                var agentProfile = session.agent;
                var agentEmail = _get('agent.email', rolesObj);
                var managerEmail = _get('manager.email', rolesObj);
                var assignedManagerEmail = _get('assignedManager.email', rolesObj);
                var cc = '';
                if (!_isEqual(managerEmail, assignedManagerEmail)) {
                    cc = managerEmail;
                }
                var recipients = agentEmail;
                var mobileNo = _get('agent.mobile', rolesObj);

                var adviserName = _get('agentName', doc);
                // const proposalNumber = _get('policyId', doc);
                var proposalNumber = _get('proposalNumber', doc) || _get('policyId', doc);
                var proposerName = _get('customerName', doc);

                try {
                    var email = nUtil.getEmailObject(nUtil.TYPE.ADDITIONALDOC_HEALTHDECLARATION, {
                        sender: DefaultEmailSender,
                        recipients: recipients,
                        adviserName: adviserName,
                        proposalNumber: proposalNumber,
                        proposerName: proposerName,
                        cc: cc
                    });
                    emailHandler.sendEmail({ email: email }, session, function () {});
                } catch (e) {
                    // ignore
                    logger.error('Prepare Additional Document in Health Declaration email: ' + e);
                }

                try {
                    if (_getOr('', 'channel.type', agentProfile) !== 'FA') {
                        var sms = nUtil.getSMSObject(nUtil.TYPE.ADDITIONALDOC_HEALTHDECLARATION, {
                            mobileNo: mobileNo,
                            adviserName: adviserName,
                            proposalNumber: proposalNumber,
                            proposerName: proposerName
                        });
                        smsHandler.sendSMS({ sms: sms }, session, function () {});
                    }
                } catch (ex) {
                    //ignore
                    logger.error('Prepare Additional Document in Health Declaration sms: ' + ex);
                }
            }
        });
    }).catch(function (e) {
        logger.error('Additional Document in Health Declaration: ' + e);
    });
};

module.exports.SubmittedCaseNotification = function (data, session) {
    new Promise(function (resolve) {
        dao.getDoc(data.id, function (doc) {
            var approvalCase = doc;
            dao.getDoc('companyInfo', function (companyInfo) {
                DefaultEmailSender = _get('fromEmail', companyInfo);
                resolve(approvalCase);
            });
        });
    }).then(function (doc) {
        var agentId = _get('agentId', doc);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, doc).then(function (rolesObj) {
            if (doc && _get('approvalStatus', doc) !== approvalStatusModel.APPRVOAL_STATUS_APPROVED) {
                var agentProfile = session.agent;
                var managerEmail = _get('manager.email', rolesObj);
                var assignedManagerEmail = _get('assignedManager.email', rolesObj);
                var recipients = _isEqual(managerEmail, assignedManagerEmail) ? managerEmail : assignedManagerEmail;

                var managerMobile = _get('manager.mobile', rolesObj);
                var assignedManagerMobile = _get('assignedManager.mobile', rolesObj);
                var mobileNo = _isEqual(managerMobile, assignedManagerMobile) ? managerMobile : assignedManagerMobile;

                var agentName = _get('agentName', doc);
                var productName = _get('productName', doc);
                var adviserName = _get('agentName', doc);
                var submissionDate = moment(_get('submittedDate', doc)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                // const proposalNumber = _get('policyId', doc);
                var proposalNumber = _get('proposalNumber', doc) || _get('policyId', doc);
                var proposerName = _get('customerName', doc);
                var expiryDate = moment(_get('submittedDate', doc)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                var actionLink = _getOr('', 'config.host.path', global) + '/goApproval';

                try {
                    if (doc && _get('approvalStatus', doc) !== approvalStatusModel.APPRVOAL_STATUS_PFAFA) {
                        var email = nUtil.getEmailObject(nUtil.TYPE.SUBMISSION_NOTIFICATION, {
                            sender: DefaultEmailSender,
                            recipients: recipients,
                            agentName: agentName,
                            productName: productName,
                            adviserName: adviserName,
                            submissionDate: submissionDate,
                            proposalNumber: proposalNumber,
                            proposerName: proposerName,
                            expiryDate: expiryDate,
                            actionLink: actionLink
                        });
                        emailHandler.sendEmail({ email: email }, session, function () {});
                    } else {
                        approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: agentId }, session, function (resp) {
                            var supervisorName = adviserName;
                            if (resp && resp.success) {
                                var resultArr = [];
                                ['faAdminCode'].forEach(function (obj) {
                                    resultArr.push(_getOr('', 'result.' + obj, resp));
                                });
                                approvalhandler.getAgentProfileId({ compCode: session.agent.compCode, id: resultArr[0] }, session, function (resp) {
                                    if (resp && resp.success) {
                                        var emailResult = [];
                                        ['email'].forEach(function (obj) {
                                            emailResult.push(_getOr('', 'result.' + obj, resp));
                                        });
                                        recipients = emailResult[0];
                                        try {
                                            var faEmail = nUtil.getEmailObject(nUtil.TYPE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM, {
                                                sender: DefaultEmailSender,
                                                recipients: recipients,
                                                productName: productName,
                                                proposalNumber: proposalNumber,
                                                proposerName: proposerName,
                                                supervisorName: supervisorName,
                                                adviserName: adviserName,
                                                submissionDate: submissionDate,
                                                expiryDate: expiryDate,
                                                agentName: agentName,
                                                actionLink: actionLink
                                            });
                                            emailHandler.sendEmail({ email: faEmail }, session, function () {});
                                        } catch (faExp) {
                                            logger.error('Prepare Application submitted approval email: ' + faExp);
                                        }
                                    }
                                });
                            }
                        });
                    }
                } catch (e) {
                    // ignore
                    logger.error('Prepare Application submitted approval email: ' + e);
                }

                try {
                    if (_getOr('', 'channel.type', agentProfile) !== 'FA') {
                        var sms = nUtil.getSMSObject(nUtil.TYPE.SUBMISSION_NOTIFICATION, {
                            mobileNo: mobileNo,
                            productName: productName,
                            adviserName: adviserName,
                            submissionDate: submissionDate,
                            proposalNumber: proposalNumber,
                            proposerName: proposerName,
                            expiryDate: expiryDate
                        });
                        smsHandler.sendSMS({ sms: sms }, session, function () {});
                    }
                } catch (ex) {
                    //ignore
                    logger.error('Prepare Application submitted approval sms: ' + ex);
                }
            }
        });
    }).catch(function (e) {
        logger.error('Application Submitted approval Notifiation: ' + e);
    });
};