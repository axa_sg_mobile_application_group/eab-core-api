'use strict';

var _ = require('lodash');
var moment = require('moment');
var dao = require('./cbDaoFactory').create();
var bundleDao = require('./cbDao/bundle/index');
var appDao = require('./cbDao/application');
var logger = global.logger || console;

/**
 * @description this file implemented the batch job that is running in mobile only
 */

var sysParameter = "sysParameter";
var BATCH_SIGN_EXPIRY_TRX = "BATCH_SIGN_EXPIRY_TRX";

var addTrxLogs = function addTrxLogs(agentCode, runTime, docType, logs) {
  return new Promise(function (resolve) {
    var runTimeString = runTime.toISOString();
    var docId = docType + '_' + agentCode;
    var updateLogs = function updateLogs(docId, newDoc) {
      dao.updDoc(docId, newDoc, function (result) {
        newDoc._rev = result.rev;
        resolve(newDoc);
      });
    };

    dao.getDoc(docId, function (doc) {
      if (doc && !doc.error) {
        var newDoc = _.cloneDeep(doc);
        newDoc.rows = doc.rows.concat(logs);

        logger.log('INFO: InvaildationHandler - addTrxLogs :: update doc ' + docType + ' - runTime=' + runTimeString);
        updateLogs(docId, newDoc);
      } else if (doc && doc.error === 'not_found') {
        var _newDoc = {
          rows: logs,
          type: docType
        };
        logger.log('INFO: InvaildationHandler - addTrxLogs :: new doc ' + docType + ' - runTime=' + runTimeString);
        updateLogs(docId, _newDoc);
      } else {
        logger.error('ERROR: InvaildationHandler - addTrxLogs :: error=' + doc.error + ' runTime=' + runTimeString);
        resolve(false);
      }
    });
  });
};

var signatureExpiry = function signatureExpiry(data, session, cb) {
  var cid = data.cid,
      applicationId = data.applicationId;

  var now = moment();
  var logs = [];
  var defaultLog = {
    EAPP_NO: "",
    BATCH_NO: "",
    POL_NO: "",
    BUNDLE_ID: "",
    SIGNATURE_DATE: 0,
    EXPIRY_DAY: 0,
    CREATE_DATE: "",
    PREMIUM_IND: "",
    REMARK: ""
  };
  var agentCode = "";
  logger.log('INFO: signatureExpiry - start', cid, applicationId);

  return new Promise(function (resolve, reject) {
    dao.getDocFromCacheFirst(sysParameter, function (param) {
      if (param && !param.error) {
        resolve(param);
      } else {
        reject(new Error("Fail to get sysParameter"));
      }
    });
  }).then(function (sysParameter) {
    return bundleDao.getCurrentBundle(cid).then(function (bundle) {
      if (!bundle) {
        throw new Error('Bundle is null');
      }
      agentCode = bundle.agentCode;
      return {
        bundle: bundle,
        sysParameter: sysParameter
      };
    });
  }).then(function (_ref) {
    var sysParameter = _ref.sysParameter,
        bundle = _ref.bundle;
    var signExpireDay = sysParameter.signExpireDay;

    var expLimit = signExpireDay || 21;
    var expSignDate = moment().startOf("day").subtract(expLimit - 1, "days");

    // get signatureExpire View
    return new Promise(function (resolve) {
      dao.getViewRange("main", "signatureExpire", '["01",0]', '["01",' + expSignDate.valueOf() + ']', null, resolve);
    }).then(function (result) {
      var rows = result.rows;

      var appRows = [];
      var found = false;

      var filterRows = _.filter(rows, function (row) {
        return row.value.bundleId === bundle.id;
      });
      if (applicationId) {
        appRows = _.filter(filterRows, function (row) {
          return row.value.appId === applicationId;
        });
      } else {
        appRows = filterRows;
      }

      var appIds = _.map(appRows, function (row) {
        return row.value.appId;
      });
      var appIdsString = "";

      _.forEach(bundle.applications, function (app) {
        var applicationDocId = _.get(app, "applicationDocId", "");
        var isFullySigned = _.get(app, "isFullySigned", false);

        var row = _.find(appRows, function (row) {
          return row.value.appId === applicationDocId;
        });

        if (appIds.includes(applicationDocId)) {
          if (isFullySigned) {
            _.set(app, "appStatus", "INVALIDATED_SIGNED");
          } else {
            _.set(app, "appStatus", "INVALIDATED");
          }
          _.set(app, "invalidateReason", "Signature Expired");
          _.set(app, "invalidateDate", moment().valueOf());

          var newLog = _.cloneDeep(defaultLog);
          newLog.BATCH_NO = bundle.agentCode + '_' + now.toISOString();
          newLog.EAPP_NO = applicationDocId;
          newLog.POL_NO = _.get(row, "value.polNo", "");
          newLog.BUNDLE_ID = bundle.id;
          newLog.SIGNATURE_DATE = _.get(row, "value.signDate", now.valueOf());
          newLog.EXPIRY_DAY = expLimit;
          newLog.CREATE_DATE = now.toISOString();
          newLog.PREMIUM_IND = _.get(row, "value.payMethod", "") + '(' + _.get(row, "value.payStatus", "") + ')';

          logs.push(newLog);
          appIdsString += applicationDocId + ";";

          found = true;
        }
      });

      if (found) {
        //update bundle
        return bundleDao.updateBundlePromise(bundle).then(function (res) {
          var bundleUpdResult = !!res ? "Update Success" : "Cannot save document";
          logger.log("INFO: signatureExpiry - Signature expired: " + appIdsString + " on bundle = " + bundle.id + " : result " + bundleUpdResult);

          return _.map(logs, function (log) {
            log.REMARK += "Signature expired: Bundle " + bundleUpdResult + ".";
            return log;
          });
        }).then(function (logs) {
          //update applications
          var promises = _.map(appIds, function (appId) {
            return appDao.getApplicationPromise(appId);
          });
          return Promise.all(promises).then(function (apps) {
            var upPromise = _.map(apps, function (app) {
              _.set(app, "isInvalidated", true);
              return appDao.upsertApplicationPromise(app.id, app);
            });
            return Promise.all(upPromise);
          }).then(function (upApps) {
            _.forEach(upApps, function (app) {
              var appUpdResult = !!app.error ? "Cannot save document" : "Update Success";
              var index = _.findIndex(logs, function (log) {
                return log.EAPP_NO === app.id;
              });
              if (index > -1) {
                logs[index].REMARK += " Application " + appUpdResult + ".";
              }
            });
            return logs;
          });
        });
      }
      return logs;

      // TODO: 
      // 1. handle Signed + Paid case in batch
      // 2. handle Signed + Paid case
    });
  }).then(function (logs) {
    if (logs.length > 0) {
      return addTrxLogs(agentCode, now, BATCH_SIGN_EXPIRY_TRX, logs);
    } else {
      return [];
    }
  }).then(function (result) {
    logger.log("INFO: signatureExpiry - end", cid, applicationId);
    cb({ success: true, logs: result });
  }).catch(function (error) {
    logger.error('ERROR: signatureExpiry - end', cid, error);
    var errorLogs = _.map(logs, function (log) {
      log.REMARK += ' > Exception: ' + error.stack;
      return log;
    });
    return addTrxLogs(agentCode, now, BATCH_SIGN_EXPIRY_TRX, errorLogs).then(function (result) {
      cb({ success: false, logs: result });
    }).catch(function (logError) {
      logger.error('ERROR: signatureExpiry - addTrxLogs', cid, elogErrorrror);
      cb({ success: false });
    });
  });
};

module.exports.signatureExpiry = signatureExpiry;