'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var driver = new Object();

if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object' && (typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object') {
  module.exports = driver;

  var utils = require('../Quote/utils');
  var runCalc = utils.runCalc,
      getCurrency = utils.getCurrency,
      returnResult = utils.returnResult,
      math = utils.math,
      debug = utils.debug;
}

driver.evalMandatoryFunc = function (data) {
  var result = runCalc(data.fnStr, data.needStr, data.qForm);
  return result;
};

driver.calRiskFunc = function (data) {
  return runCalc(data.fnStr, data.rAns, data.rQuests);
};